<?php
switch(true){
	case($role_no == 1 || $role_no == 2):
		switch($status){
			case 0:
			?>
			<button href="#" class="btn btn-default btn-sm" disabled>
	        	<i class="fa fa-times"></i>
	        </button>
			<?php
			break;
			case 1:
			?>
			<button class="btn btn-info btn-sm" onclick="deliveryDetail(<?php echo $id ?>)">
	        	<i class="fa fa-eye"></i>
	        </button>
			<?php
			break;
		}
	break;
	case($role_no == 3):
		switch($status){
			case 0:
			?>
			<a href="<?php echo base_url().'slot/set_schedule/'.$po_number.'/'.$id; ?>" class="btn btn-success btn-sm">
	            <i class="fa fa-pencil"></i>
	          </a>
			<?php
			break;
			case 1:
			?>
			<button class="btn btn-info btn-sm" onclick="deliveryDetail(<?php echo $id ?>)">
	        	<i class="fa fa-eye"></i>
	        </button>
			<?php
			break;
		}
	break;
}
?>