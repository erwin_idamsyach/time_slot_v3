<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists('getHTML')){
	function getHTML($view = "", $data = ""){
		$CI = &get_instance();
		$CI->load->view('includes/header', $data);
		$CI->load->view('includes/sidebar', $data);
		$CI->load->view($view, $data);
		$CI->load->view('includes/footer', $data);
	}
}


if(! function_exists('getUnplannedSlot')){
	function getUnplannedSlot($role = 1, $vendor_code = 0){
		$CI = &get_instance();
		if($role == 1 || $role == 2){
			$count = $CI->db->query("SELECT * FROM tb_rds_detail WHERE status='0' GROUP BY po_number")->num_rows();
		}else{
			$count = $CI->db->query("SELECT * FROM tb_rds_detail WHERE vendor_code='$vendor_code' AND status='0' GROUP BY po_number")->num_rows();
		}
		/*echo $role." ".$vendor_code;*/
		return $count;
	}
}

if(! function_exists('getButton')){
	function getButton($role = 1, $status = 0, $po_number, $id){
		$CI = &get_instance();
		$data['role_no'] = $role;
		$data['status'] = $status;
		$data['po_number'] = $po_number;
		$data['id'] = $id;
		$CI->load->view('button_setter', $data);
	}
}

if(! function_exists('truckIcon')){
	function truckIcon($status = 0, $vendor = "SKL", $adt = 0, $tmat = 0,$other = 0){
		/*
		0 --> NOT_PLANNED
		1 --> PLANNED
		2 --> ARRIVED
		3 --> RECEIVED
		*/
		$color = '';
		
		if($other == 1){
			$color = "color:#110e9b;";
		}else if($adt == 1 ){
			$color = "color:red";
		}
		
		if($status == 0 && $adt == 1){
			$tmatz = '';
			if($tmat == 0){
				$tmatz = "opacity:0.5;";
			}

			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:20px;'.$tmatz.';" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        
			      </div>';
		}else if($status == 1 && $adt == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:20px;" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-lock fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>';
		}else if($status == 2 && $adt == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:20px;" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-arrow-down fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>';	
		}else if($status == 3 && $adt == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:20px;" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-check fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>';
		}else if($status == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:20px;'.$color.'" class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-lock fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>';
		}else if($status == 2){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:20px;'.$color.'" class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-arrow-down fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>';
			
		}else if($status == 3){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:20px;'.$color.'" class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-check fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>';

		}else{
			$tmatz = '';
			if($tmat == 0){
				$tmatz = "color:#ACACAC;";
			}
			echo "<b style='position:absolute;margin-top:-17px;margin-left:20px;font-weight:800'>".$vendor."</b>".'<div style="'.$tmatz.'margin-left:20px;'.$color.'" class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-3x fa-stack-1x"></i>
			      </div>';

		}
	}
}

if(! function_exists('LegendIcon')){
	function LegendIcon($status = 0){
		/*
		0 --> NOT_PLANNED
		1 --> PLANNED
		2 --> ARRIVED
		3 --> RECEIVED
		*/
		if($status == 1){
			echo '<div class="fa-stack fa-1x text-center">
			        <i class="fa fa-truck fa-stack-2x"></i>
			        <i class="fa fa-lock fa-stack-1x" style="color: #36FA49;margin-left:-3px;margin-top:-2px"></i>
			      </div><br><b><p style="font-size:8px">PLANNED</p></b>';
		}else if($status == 2){
			echo '<div class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-stack-2x"></i>
			        <i class="fa fa-arrow-down fa-stack-1x" style="color: #36FA49;margin-left:-3px;margin-top:-2px"></i>
			      </div><br><b><p style="font-size:8px">ARRIVED</p></b>';	
		}else if($status == 3){
			echo '<div class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-stack-2x"></i>
			        <i class="fa fa-check fa-stack-1x" style="color: #36FA49;margin-left:-3px;margin-top:-2px"></i>
			      </div><br><b><p style="font-size:8px">RECEIVED</p></b>';
		}else if($status == 4){
			echo '<div class="fa-stack fa-1x text-center">
					<i style="color:red" class="fa fa-truck fa-2x"></i>
					</div><br><b><p style="font-size:8px">ADDITIONAL</p></b>';
		}else if($status == 5){
			echo '<div class="fa-stack fa-1x text-center">
					<i style="color:blue" class="fa fa-truck fa-2x"></i>
					</div><br><b><p style="font-size:8px">OTHER SUPPLIER</p></b>';
		}else{
			echo '<div class="fa-stack fa-1x text-center">
					<i style="text-align:center" class="fa fa-truck fa-2x"></i>
					</div><br><b><p style="font-size:8px">NOT PLANNED</p></b>';
		}
	}
}

if(! function_exists('isLogin')){
	function isLogin(){
		$CI = &get_instance();
		if($CI->session->userdata('sess_role_no') != ""){
			return true;
		}else{
			redirect('login');
		}
	}
}

if(! function_exists('isRole')){
	function isRole(){
		$year = date('Y');
		$week = date('W');
		$date = date('Y-m-d');
		$CI = &get_instance();
		if($CI->session->userdata('sess_role_no') == "3"){
			redirect('schedule?year='.$year.'&week='.$week.'&date='.$date);
		}
	}
}

if(! function_exists('history')){

	function history($role = 0, $vendor_code = 0, $limit = 0, $id =''){
		$CI = &get_instance();
		if($CI->session->userdata('sess_role_no') != '' ){
			$role = $CI->session->userdata('sess_role_no');
			if($limit == 0){
				if($role == 3){
					
					$data = $CI->db->query("SELECT a.action,a.date,a.time, a.description,a.table_join,a.id_join,a.select_join,b.username FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE b.id = '$id' ORDER BY a.id desc LIMIT $limit")->result();
				}else{
					
				}
				
			}else{
				if($role == 3){

					$vendor_code = $CI->session->userdata('sess_vendor_code');
					$data = $CI->db->query("SELECT a.*,b.username FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id  WHERE  a.vendor_code = '$vendor_code' ORDER BY id desc LIMIT $limit")->result();
					

				}else if($role == 4){
					$data = $CI->db->query("SELECT a.*,b.username FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE a.author != 1 ORDER BY a.id desc LIMIT $limit")->result();
				}else if($role == 1){
					$data = $CI->db->query("SELECT a.*,b.username FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id ORDER BY a.id desc LIMIT $limit")->result();
				}else if($role == 2){
					$data = $CI->db->query("SELECT a.*,b.username FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE a.author != 1 ORDER BY a.id desc LIMIT $limit")->result();
				}
			}
			/*echo $role." ".$vendor_code;*/
			return $data;
		}
	}
}

if(! function_exists('history_count')){
	
	function history_count($id){
			$CI = &get_instance();
			$id_user = $CI->session->userdata('sess_id');
			$vendor_code = $CI->session->userdata('sess_vendor_code');
			if($CI->session->userdata('sess_role_no') != '' ){
			$role = $CI->session->userdata('sess_role_no');
			if($role == 1){
				$data = $CI->db->query("SELECT a.id FROM tb_history AS a INNER JOIN ms_user AS b WHERE a.id > b.last_history AND b.id = '$id_user'")->num_rows();
			}else if($role == 2){
				$data = $CI->db->query("SELECT a.id FROM tb_history AS a INNER JOIN ms_user AS b WHERE a.id > b.last_history AND b.id = '$id_user' AND a.author != 1 ")->num_rows();
			}else if($role == 4){
				$data = $CI->db->query("SELECT a.id FROM tb_history AS a INNER JOIN ms_user AS b WHERE a.id > b.last_history AND b.id = '$id_user' AND a.author != 1")->num_rows();
			}else if($role == 3){
				$data = $CI->db->query("SELECT a.id FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id INNER JOIN skin_master.ms_supplier AS c ON b.vendor_code = c.vendor_code  WHERE a.id > b.last_history AND a.author = 3 AND a.vendor_code = '$vendor_code'")->num_rows();
			}
			
			return $data;
		}
	}
}

if(! function_exists('getShift')){
	function getShift(){
		$CI = &get_instance();
		$c_time = date('H:i:s');
		$getShift = $CI->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		
		return $getShift->shift;
	}
}
?>