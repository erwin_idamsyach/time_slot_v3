<style>

  .card {
    
    padding: 0;
    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 100%;
    margin: 0 auto;
  }

  .fc-content{
    font-size:13px;
  }

  .centered {
   text-align: center;
   font-size: 0;

  }
  .centered > div {
     float: none;
     display: inline-block;
     text-align: left;
     font-size: 13px;
  }

  @media screen and (min-width: 400px) {
  
}

@media screen and (min-width: 800px){
  #filter{
    margin-top:28px;
  }
}
</style>

<?php if (isset($_GET['vendor_code'])): ?>
  <?php  $vendor = $_GET['vendor_code']; ?>
<?php else: ?>
  <?php  $vendor = "ALL"; ?>
<?php endif ?>

<?php if (isset($_GET['GET'])): ?>
  <?php  $type = $_GET['GET']; ?>
<?php else: ?>
  <?php  $type = "Weekly"; ?>
<?php endif ?>

<?php if (isset($_GET['category'])): ?>
  <?php  $cat = $_GET['category']; ?>
<?php else: ?>
  <?php  $cat = "FACE"; ?>
<?php endif ?>

<?php if (isset($_GET['type_search_item'])): ?>
  <?php  $type_search_item = $_GET['type_search_item']; ?>
<?php else: ?>
  <?php  $type_search_item = ""; ?>
<?php endif ?>

<?php if (isset($_GET['type_search_value'])): ?>
  <?php  $type_search_value = $_GET['type_search_value']; ?>
<?php else: ?>
  <?php  $type_search_value = ""; ?>
<?php endif ?>

<div class="content">
  <div class="col-md-12">
      <div class="card" style="margin-bottom: 0">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="fas fa-th" style="font-size:30px"></i>
          </div>
          <h4 class="card-title">RDS Plan</h4>
        </div>
        <div class="card-body">
           <div class="row">
            <div class="col-md-2">
              <?php $year=$_GET['year']; $week=$_GET['week']; ?>
                     <div class="form-group">
                  <label style="margin-left:70px">Year</label>
                  <nav aria-label="Page navigation example">
                      <ul class="pagination pagination-primary">
                        
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $year-1; ?></a>
                        </li>
                        
                        <li class="page-item active">
                          <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $year; ?></a>
                        </li>

                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $year+1; ?></a>
                        </li>

                      </ul>
                    </nav>
                  </div>
          </div>

          <div class="col-md-3 col-xl-3 col-sm-6">
          <div class="form-group">
            <label style="margin-left:85px">Week</label>
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-primary">
                  
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']-2; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $week-2; ?></a>
                  </li>
                   <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $week-1; ?></a>
                  </li>
                  
                  <li class="page-item active">
                    <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $week; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $week+1; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>&category=<?php echo $_GET['category'];?>&GET=Daily"><?php echo $week+2; ?></a>
                  </li>
                  

                </ul>
              </nav>
            </div>
          </div>
          <div class="col" id="filter">
            <form action="<?php echo base_url() ?>rds" method="GET">
            <div class="row">
            <input type="hidden" name="year" value="<?php echo $this->input->get('year'); ?>">
            <input type="hidden" name="week" value="<?php echo $this->input->get('week'); ?>"> 
            <input type="hidden" name="category" value="<?php echo $this->input->get('category'); ?>">
            <input type="hidden" name="Date" value="<?php echo $this->input->get('Date'); ?>">
            <input type="hidden" name="GET" value="<?php echo $this->input->get('GET'); ?>">            
                <div class="col-md-4 col-sm-12" id="supplier">
                    <select <?php if($this->session->userdata('sess_role_no') == 3){echo "style='display:none'";} ?> name="supplier" class="form-control">
                      <option value="">-- SELECT ALL SUPPLIER --</option>
                      <?php foreach ($supplier as $get) {
                          ?>
                          <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code.' - '.$get->vendor_alias; ?></option>
                          <?php
                      } ?>


                    </select>
                </div>

                <div class="col-md-3">
                    <select name="menu" id="#menu" class="form-control text-center menux" onchange="select_menu(this.value)">
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == '' ){ echo "selected"; } ?> value="">-- SELECT ROWS --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'material_code' ){ echo "selected"; } ?> value="material_code">-- ITEM CODE --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'po_line_item' ){ echo "selected"; } ?> value="po_line_item">-- LINE --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'qty' ){ echo "selected"; } ?> value="qty">-- ORDER QTY --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'uom' ){ echo "selected"; } ?> value="uom">-- UOM --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'uom_plt' ){ echo "selected"; } ?> value="uom_plt">-- QTY/PLT --</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <input type="text" <?php if($this->input->get('menu') == ''){echo 'disabled="true"';} ?> id="search_value"  class="form-control" name="search_value" value="<?php echo $this->input->get('search_value'); ?>" placeholder="Search Item">
                </div>

                <div div class="col-md-2">
                    <button class="btn btn-primary btn-sm">Filter</button>
                </div>           
            </div>
            </form>
          </div>

          </div>

            <style>
              .nav-pills .nav-item .nav-link{min-width:0px}
            </style>
            <style>.category-4 .btn-group .btn.btn-info{padding : 15px;}</style>
            <div class="row">
               <div class="col-sm-12 col-md-4 category-4">
                <div class="btn-group" data-toggle="buttons">
                  <button type="button" class="btn  btn-info <?php if($cat == 'FACE'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=FACE&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">FACE</button>
                  <button type="button" class="btn  btn-info <?php if($cat == 'BODY'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=BODY&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">BODY</button>
                  <button type="button" class="btn  btn-info <?php if($cat == 'PWL'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=PWL&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">PWL</button>
                  <button type="button" class="btn  btn-info <?php if($cat == 'DEO'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=DEO&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">DEO</button>
                </div>
            </div>
              <div class="col-sm-12 col-md-4" style="margin-top:10px">
                <ul class="nav nav-pills">
                  <?php if (isset($_GET['GET'])): ?>
                    <?php $class_nav =  $_GET['GET'] == 'Weekly'?"active":""; ?>
                    <li class="nav-item" >
                      <a class="nav-link <?php echo $class_nav ?>" href="?&category=<?php echo $cat ?>&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&GET=Weekly&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>">Weekly</a>
                    </li>
                    <?php $class_nav =  $_GET['GET'] == 'Daily'?"active":""; ?>
                    <li class="nav-item">
                      <a class="nav-link <?php echo $class_nav ?>" href="?&category=<?php echo $cat ?>&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&GET=Daily&vendor_code=<?php echo $vendor ?>&Date=<?php echo date('Y-m-d')?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>">Daily</a>
                    </li>
                    <?php $class_nav =  $_GET['GET'] == 'List'?"active":""; ?>
                    <li class="nav-item">
                      <a class="nav-link <?php echo $class_nav ?>" href="?&category=<?php echo $cat ?>&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&GET=List&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>">List</a>
                    </li>
                  <?php else: ?>
                    <li class="nav-item">
                      <a class="nav-link" href="?GET=Weekly&vendor_code=<?php echo $vendor ?>">Weekly</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="?GET=Daily&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>">Daily</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="?GET=List&&vendor_code=<?php echo $vendor ?>">List</a>
                    </li>
                  <?php endif ?>
                </ul>
              </div>
              <div class="col" style="margin-top:10px">
                <div class="row">
                  <div class="col-3" style="padding: 0;">
                    <div style="height: 30px;background: green;"></div>
                    <p style="text-align: center;">Received</p>
                  </div>
                  <div class="col-3" style="padding: 0;">
                    <div style="height: 30px;background: red;"></div>
                    <p style="text-align: center;">Not Planned</p>
                  </div>
                  <div class="col-3" style="padding: 0;">
                    <div style="height: 30px;background: #b7b7b7;"></div>
                    <p style="text-align: center;">Planned</p>
                  </div>
                </div>
              </div>
              </div>

            <div class="row">
              <div class="col-sm-12">
                  <?php
                  $zz = 0;
                  while($zz < count($list_date)){
                    $dc = date_create($list_date[$zz]);
                    $df = date_format($dc, "D d M Y");
                    $dl = date_format($dc, "Y-m-d");
                    
                    if (isset($_GET['Date'])){
                      $c_date = $_GET['Date'];
                    }
                    else{
                      $c_date = "";
                    }
                    ?>
                    <button type="button" onclick="window.location=('?year=<?php echo $year; ?>&category=<?php echo $cat ?>&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&week=<?php echo $week; ?>&Date=<?php echo $dl; ?>&GET=<?php echo "Daily" ?>&vendor_code=<?php echo $vendor ?>')" style="width: 130px;font-size:12px" class="btn btn-primary btn-sm <?php  if($dl == $c_date){echo "active";} ?>"><?php echo $df; ?></button>
                    <?php
                    $zz++;
                  }
                  ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="card" style="margin-top: 5px;"> 
          <div class="card-body">
            <div class="row table-fix" style="overflow: scroll;max-height: 500px;
            max-width: 100%;">
            <div class="table" style="margin-top: -1px;">
            <table class="table table-bordered table-striped table-hover" style="white-space: nowrap; 
              position: relative; border-collapse: separate;"> 
              <thead style="color:white; background-color:#31559F;font-weight: 650">
                <tr>
                  <th width="50"></th>
                  <?php
                  foreach($list_date as $d){
                    if($d != $c_date){
                      continue;
                    }else{
                    ?>
                  <th class="text-center">
                    <?php
                    $cd = date_create($d);
                    $df = date_format($cd, "M Y");
                    echo $df;
                    ?>
                  </th>
                    <?php
                    }
                  }
                  ?>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($arr_shift as $sh){
                  ?>
                <tr>
                  <td class="bg-primary"><?php echo $sh['shift_alias']; ?></td>
                  <?php
                  
                  foreach ($list_date as $get) {
                    $ard = $data[$get][$sh['shift_alias']];
                    if($get != $c_date){
                      continue;
                    }else{

                          $ad = json_encode($ard);
                          $dd = json_decode($ad, true);
                          if(count($dd) > 0){
                            echo "<td>";
                            echo "<div class='container-fluid'>";
                            
                            foreach($dd as $val){?>

                              <?php 
                                if ($val['status'] == 1) {
                                  $color = "#b7b7b7;color:black";
                                }elseif ($val['status'] == 0) {
                                  $color = "red;color:white";
                                }elseif ($val['status'] == 2) {
                                  $color = "#b7b7b7;color:black";
                                }elseif ($val['status'] == 3 && $val['quantity'] > $val['receive_amount'] ) {
                                  $color = "#b7b7b7;color:black";
                                }elseif ($val['status'] == 3) {
                                  $color = "green;color:white";
                                }

                              ?>

                              <?php if ($val['sisa'] > 0): ?>
                                <div class="row">
                                
                                  <div class="col-md-12">
                                    <div style="width : 100%;min-height: 20px;background: <?php echo $color; ?>;border-radius: 5px; font-size: 10px ;margin-top: 5px;padding: 5px;font-weight: 700">
                                      <?php echo $val['vendor_alias']." - ".$val['material_code']." - ".$val['material_name']." : ".$val['material_terscedule']." ".$val['uom']; ?>
                                    </div>
                                  </div>
                                  
                                </div>
                              <?php else: ?>
                                <?php if ($val['material_terscedule'] > 0 && $val['material_terscedule'] != null): ?>

                                  <div style="width : 100%;min-height: 20px;background: <?php echo $color; ?>;border-radius: 5px;margin-top: 5px;font-weight: 700;padding: 5px;font-size: 10px;">
                                  <?php echo $val['vendor_alias']." - ".$val['material_code']." - ".$val['material_name']." : ".$val['material_terscedule']." ".$val['uom']; ?>
                                  </div>

                                <?php else: ?>

                                  <div style="width : 100%;min-height: 20px;background: <?php echo $color; ?>;border-radius: 5px;margin-top: 5px;font-weight: 700;padding: 5px;font-size: 10px;">
                                  <?php echo $val['vendor_alias']." - ".$val['material_code']." - ".$val['material_name']." : ". $val['req_pallet'] * $val['uom_plt'] ." ".$val['uom']; ?>
                                  </div>

                                <?php endif ?>
                              <?php endif ?>

                              <?php
                                
                              }
                            }
                            echo "</div></div>";
                            echo "</td>";
                      }
                    }
                  }
                  ?>
                </tr>
              </tbody>
            </table>
            </div>
          </div>
          </div>
        </div>
      </div>
</div>