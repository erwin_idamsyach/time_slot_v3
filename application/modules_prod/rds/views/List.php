<style>

  .card {
    
    padding: 0;
    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 100%;
    margin: 0 auto;
  }

  .fc-content{
    font-size:13px;
  }

  .centered {
   text-align: center;
   font-size: 0;

  }
  .centered > div {
     float: none;
     display: inline-block;
     text-align: left;
     font-size: 13px;
  }

@media screen and (min-width: 400px) {
  
}

@media screen and (min-width: 800px){
  #filter{
    margin-top:28px;
  }
}
</style>

<?php if (isset($_GET['vendor_code'])): ?>
  <?php  $vendor = $_GET['vendor_code']; ?>
<?php else: ?>
  <?php  $vendor = "ALL"; ?>
<?php endif ?>

<?php if (isset($_GET['GET'])): ?>
  <?php  $type = $_GET['GET']; ?>
<?php else: ?>
  <?php  $type = "Weekly"; ?>
<?php endif ?>

<?php if (isset($_GET['category'])): ?>
  <?php  $cat = $_GET['category']; ?>
<?php else: ?>
  <?php  $cat = "FACE"; ?>
<?php endif ?>

<?php if (isset($_GET['type_search_item'])): ?>
  <?php  $type_search_item = $_GET['type_search_item']; ?>
<?php else: ?>
  <?php  $type_search_item = ""; ?>
<?php endif ?>

<?php if (isset($_GET['type_search_value'])): ?>
  <?php  $type_search_value = $_GET['type_search_value']; ?>
<?php else: ?>
  <?php  $type_search_value = ""; ?>
<?php endif ?>

<div class="content">
  <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-success card-header-icon">
          <div class="card-icon">
            <i class="fas fa-th" style="font-size:30px"></i>
          </div>
          <h4 class="card-title">RDS Plan</h4>
        </div>
        <div class="card-body">
           <div class="row">
            <div class="col-md-2">
              <?php $year=$_GET['year']; $week=$_GET['week']; ?>
                     <div class="form-group">
                  <label style="margin-left:70px">Year</label>
                  <nav aria-label="Page navigation example">
                      <ul class="pagination pagination-primary">
                        
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $year-1; ?></a>
                        </li>
                        
                        <li class="page-item active">
                          <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $year; ?></a>
                        </li>

                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $year+1; ?></a>
                        </li>

                      </ul>
                    </nav>
                  </div>
          </div>

          <div class="col-md-3 col-xl-3 col-sm-6">
          <div class="form-group">
            <label style="margin-left:85px">Week</label>
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-primary">
                  
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']-2; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $week-2; ?></a>
                  </li>
                   <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $week-1; ?></a>
                  </li>
                  
                  <li class="page-item active">
                    <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $week; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $week+1; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>&category=<?php echo $_GET['category'];?>&GET=List"><?php echo $week+2; ?></a>
                  </li>
                  

                </ul>
              </nav>
            </div>
          </div>
          <div class="col" id="filter">
            <form action="<?php echo base_url() ?>rds" method="GET">
            <div class="row">
            <input type="hidden" name="year" value="<?php echo $this->input->get('year'); ?>">
            <input type="hidden" name="week" value="<?php echo $this->input->get('week'); ?>"> 
            <input type="hidden" name="category" value="<?php echo $this->input->get('category'); ?>">
            <input type="hidden" name="GET" value="<?php echo $this->input->get('GET'); ?>">            
                <div class="col-md-4 col-sm-12" id="supplier">
                    <select <?php if($this->session->userdata('sess_role_no') == 3){echo "style='display:none'";} ?> name="supplier" class="form-control">
                      <option value="">-- SELECT ALL SUPPLIER --</option>
                      <?php foreach ($supplier as $get) {
                          ?>
                          <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code.' - '.$get->vendor_alias; ?></option>
                          <?php
                      } ?>


                    </select>
                </div>

                <div class="col-md-3">
                    <select name="menu" id="#menu" class="form-control text-center menux" onchange="select_menu(this.value)">
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == '' ){ echo "selected"; } ?> value="">-- SELECT ROWS --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'material_code' ){ echo "selected"; } ?> value="material_code">-- ITEM CODE --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'po_line_item' ){ echo "selected"; } ?> value="po_line_item">-- LINE --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'qty' ){ echo "selected"; } ?> value="qty">-- ORDER QTY --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'uom' ){ echo "selected"; } ?> value="uom">-- UOM --</option>
                      <option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'uom_plt' ){ echo "selected"; } ?> value="uom_plt">-- QTY/PLT --</option>
                    </select>
                </div>

                <div class="col-md-3">
                    <input type="text" <?php if($this->input->get('menu') == ''){echo 'disabled="true"';} ?> id="search_value"  class="form-control" name="search_value" value="<?php echo $this->input->get('search_value'); ?>" placeholder="Search Item">
                </div>

                <div div class="col-md-2">
                    <button class="btn btn-primary btn-sm">Filter</button>
                </div>           
            </div>
            </form>
          </div>

          </div>

            <style>
              .nav-pills .nav-item .nav-link{min-width:0px}
            </style>
            <style>.category-4 .btn-group .btn.btn-info{padding : 15px;}</style>
            <div class="row">
              <div class="col-sm-12 col-md-4 category-4">
                <div class="btn-group" data-toggle="buttons">
                  <button type="button" class="btn  btn-info <?php if($cat == 'FACE'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=FACE&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">FACE</button>
                  <button type="button" class="btn  btn-info <?php if($cat == 'BODY'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=BODY&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">BODY</button>
                  <button type="button" class="btn  btn-info <?php if($cat == 'PWL'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=PWL&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">PWL</button>
                  <button type="button" class="btn  btn-info <?php if($cat == 'DEO'){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>/rds?&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&vendor_code=<?php  echo $vendor ?>&category=DEO&GET=<?php echo $type ?>&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>')">DEO</button>
                </div>
            </div>
              <div class="col-sm-12 col-md-4" style="margin-top:10px">
                <ul class="nav nav-pills">
                  <?php if (isset($_GET['GET'])): ?>
                    <?php $class_nav =  $_GET['GET'] == 'Weekly'?"active":""; ?>
                    <li class="nav-item" >
                      <a class="nav-link <?php echo $class_nav ?>" href="?&category=<?php echo $cat ?>&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&GET=Weekly&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>">Weekly</a>
                    </li>
                    <?php $class_nav =  $_GET['GET'] == 'Daily'?"active":""; ?>
                    <li class="nav-item">
                      <a class="nav-link <?php echo $class_nav ?>" href="?&category=<?php echo $cat ?>&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&GET=Daily&vendor_code=<?php echo $vendor ?>&Date=<?php echo date('Y-m-d')?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>">Daily</a>
                    </li>
                    <?php $class_nav =  $_GET['GET'] == 'List'?"active":""; ?>
                    <li class="nav-item">
                      <a class="nav-link <?php echo $class_nav ?>" href="?&category=<?php echo $cat ?>&type_search_value=<?php echo $type_search_value ?>&type_search_item=<?php echo $type_search_item ?>&GET=List&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>">List</a>
                    </li>
                    
                  <?php else: ?>
                    <li class="nav-item">
                      <a class="nav-link" href="?GET=Weekly&vendor_code=<?php echo $vendor ?>&year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category']; ?>">Weekly</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="?GET=Daily&vendor_code=<?php echo $vendor ?>&Date=<?php echo $list_date[0]?>&year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category']; ?>">Daily</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="?GET=List&&vendor_code=<?php echo $vendor ?>&year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category']; ?>">List</a>
                    </li>
                  <?php endif ?>
                </ul>
              </div>
              <div class="col" style="margin-top:10px">
                <div class="row">
                  <div class="col-3" style="padding: 0;">
                    <div style="height: 30px;background: green;"></div>
                    <p style="text-align: center;">Received</p>
                  </div>
                  <div class="col-3" style="padding: 0;">
                    <div style="height: 30px;background: red;"></div>
                    <p style="text-align: center;">Not Planned</p>
                  </div>
                  <div class="col-3" style="padding: 0;">
                    <div style="height: 30px;background: #b7b7b7;"></div>
                    <p style="text-align: center;">Planned</p>
                  </div>
                </div>
              </div>
              </div>
          <div class="row">
            <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover"> 
              <thead>
                <tr><th>List Schedule</th></tr>
              </thead>
              <tbody>
                <?php foreach ($data as $key => $value): ?>

                  <?php $condition = false ?>

                  <?php foreach ($value as $keyx => $x): ?>

                    <?php if (!empty($x)): ?>

                      <?php $condition = true?>

                    <?php endif ?>
                    
                  <?php endforeach ?>

                  <?php if ($condition): ?>

                    <tr class="btn-default" style="font-size: large;font-weight: 700;">
                      <td>
                        <?php
                          $cd = date_create($key);
                          $df = date_format($cd, "d M");
                          echo $df;
                        ?>
                        
                      </td>
                    </tr>

                    <?php foreach ($value as $key1 => $x1): ?>

                      <?php if (!empty($x1)): ?>

                        <?php foreach ($x1 as $result): ?>

                          <?php 
                            if ($result->status == 1) {
                              $color = "#b7b7b7;color:black";
                            }elseif ($result->status == 0) {
                              $color = "red;color:white";
                            }elseif ($result->status == 2) {
                              $color = "#b7b7b7;color:black";
                            }elseif ($result->status == 3 && $result->quantity > $result->receive_amount ) {
                              $color = "#b7b7b7;color:black";
                            }elseif ($result->status == 3) {
                              $color = "green;color:white";
                            }
                          ?>

                          <?php if (floatval($result->sisa) > 0 && $result->status != 3 && $result->status == 0): ?>

                            <tr style="background: red;font-weight: 700;font-size:10px">
                              <td><?php echo "$result->vendor_alias "."{$result->material_code} - {$result->material_name} : {$result->sisa} {$result->uom}" ?></td>
                            </tr>

                          <?php else: ?>

                              <?php if ($result->material_terscedule > 0 && $result->material_terscedule != null): ?>

                                <tr style="background: <?php echo $color ?>;font-weight:700;padding:5px;font-size:10px">
                                  <td><?php echo "$result->vendor_alias "."{$result->material_code} - {$result->material_name} : {$result->material_terscedule} {$result->uom}" ?></td>
                                </tr>

                              <?php else: ?>

                                <tr style="background: <?php echo $color ?>;font-weight:700;padding:5px;font-size:10px">
                                  <?php $qty = $result->req_pallet * $result->uom_plt ?>
                                  <td><?php echo "$result->vendor_alias "." - "."{$result->material_code} - {$result->material_name} : {$qty} {$result->uom}" ?></td>
                                </tr>

                              <?php endif ?>

                          <?php endif ?>

                        <?php endforeach ?>

                      <?php endif ?>


                    <?php endforeach ?>
                    
                  <?php endif ?>
                  
                <?php endforeach ?>
              </tbody>
            </table>
            </div>
          </div>
          </div>
        </div>
      </div>
    </section>
</div>