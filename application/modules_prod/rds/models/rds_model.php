<?php

class Rds_Model extends CI_Model{

	public function Rds_Data(){
		if($this->session->userdata('sess_role_no') != 3){
			$getData = $this->db->query("SELECT * FROM tb_schedule_detail as tsd 
				INNER JOIN tb_scheduler as ts ON tsd.schedule_number = ts.schedule_number 
				INNER JOIN tb_rds_detail as trd ON ts.id_schedule = trd.id
				INNER JOIN skin_master.ms_supplier as ms ON tsd.vendor_code = ms.vendor_code
				INNER JOIN ms_time_slot as mts ON tsd.id_time_slot = mts.id");
		}else{
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$getData = $this->db->query("SELECT * FROM tb_schedule_detail as tsd 
				INNER JOIN tb_scheduler as ts ON tsd.schedule_number = ts.schedule_number 
				INNER JOIN tb_rds_detail as trd ON ts.id_schedule = trd.id
				INNER JOIN skin_master.ms_supplier as ms ON tsd.vendor_code = ms.vendor_code
				INNER JOIN ms_time_slot as mts ON tsd.id_time_slot = mts.id
				WHERE tsd.vendor_code = '$vendor_code' ");
		}
		
		return $getData;
	}

	public function get_date($week, $year){
		$dto = new DateTime();
		  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
		  $s = 1;
		  while($s <= 6){
		  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
		  	$s++;
		 }
		 
		 return $ret;
	}


	public function get_data_dm($week, $year, $arrDate){
		$arr = array();
		foreach ($arrDate as $get) {
			$getData = $this->db->query("SELECT a.material_code, b.material_name, a.qty, a.shift, a.requested_delivery_date as rdd FROM tb_rds_detail AS a 
			INNER JOIN skin_master.ms_material AS b ON a.material_code = b. material_sku 
			WHERE a.requested_delivery_date = '$get' AND a.shift = 'DM' ")->result();
		}
		
		dump($getData);
		return $getData;
	}

	public function get_data_dp($week, $year){
		$getData = $this->db->query("SELECT a.material_code, b.material_name, a.qty, a.shift, a.requested_delivery_date as rdd FROM tb_rds_detail AS a 
			INNER JOIN skin_master.ms_material AS b ON a.material_code = b. material_sku 
			WHERE YEAR(a.requested_delivery_date) = '$year' AND a.week ='$week' AND a.shift = 'DP' ");
		
		return $getData;
	}

	public function get_data_ds($week, $year){
		$getData = $this->db->query("SELECT a.material_code, b.material_name, a.qty, a.shift, a.requested_delivery_date as rdd FROM tb_rds_detail AS a 
			INNER JOIN skin_master.ms_material AS b ON a.material_code = b. material_sku 
			WHERE YEAR(a.requested_delivery_date) = '$year' AND a.week ='$week' AND a.shift = 'DS' ");
		
		return $getData;
	}
}

?>