<?php
class Rds extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('Rds_Model');
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		date_default_timezone_set("Asia/Bangkok");
		isLogin();
	}

	public function index(){
		$type_search = $this->input->get("type_search_item");
		$type_value = $this->input->get("type_search_value" );
		$category = $this->input->get('category');

		if($this->session->userdata('sess_role_no') != 3){
			$sup = $this->input->get('supplier');
		}else{
			$sup = $this->session->userdata('sess_vendor_code');
		}

		$menu = $this->input->get('menu');
		if($menu == ''){
			$menu = "material_code";
		}
		$search_value = $this->input->get('search_value');
		// dump();
		// dump($this->input->get());
		
		if ($category == "" || $category == null) {
			$category = '';
		}else{
			$category = $category;
		}

		$rds_data = $this->Rds_Model->Rds_Data();
		$data['data'] = $rds_data;

		$arr = array();
		$arr_shift = array(
			array(
				'shift_name' => "Dinas Malam",
				"shift_alias" => "DM"
			),
			array(
				'shift_name' => "Dinas Pagi",
				"shift_alias" => "DP"
			),
			array(
				'shift_name' => "Dinas Sore",
				"shift_alias" => "DS"
			)
		);
		
		$yearnow = date('Y');
		$ddate = date('Y-m-d');
		$date = new DateTime($ddate);
		$weeks = $date->format("W");
		$week = (null === $this->input->get('week'))?$weeks:$this->input->get('week');
		$year = (null === $this->input->get('year'))?$yearnow:$this->input->get('year');
		$arrDate = $this->getDateList($week, $year);
		$role = $this->session->userdata('sess_role_no');
		
		foreach($arrDate as $date){
			foreach($arr_shift as $shift){
				$arr[$date][$shift['shift_alias']] = array();
				$sa = $shift['shift_alias'];
				if($role != 3){
					$getDataz = $this->db->query("SELECT  d.vendor_alias, 
						c.material_code,
						c.qty,
						a.status, 
						c.req_pallet, 
						c.uom_plt, 
						e.material_name, 
						c.uom, 
						(c.uom_plt * b.quantity) 
						as material_terscedule, (c.qty - (c.uom_plt * b.quantity) ) AS sisa,
						b.receive_amount,
						c.shift, b.quantity 
						FROM tb_schedule_detail AS a 
						INNER JOIN tb_Scheduler AS b 
						ON a.schedule_number = b.schedule_number 
						INNER JOIN tb_rds_detail AS c ON b.id_schedule = c.id 
						LEFT JOIN skin_master.ms_supplier AS d ON d.vendor_code = c.vendor_code 
						LEFT JOIN skin_master.ms_material AS e ON e.material_sku = c.material_code
						WHERE 
							c.requested_delivery_date = '$date'
							AND c.shift = '$sa' 
							AND a.vendor_code LIKE '%$sup%'
							AND c.category LIKE '%$category%'
							AND c.$menu LIKE '%$search_value%'
						GROUP BY b.id
						")->result();
					
					$getData2 = $this->db->query("SELECT  d.vendor_alias, 
						c.material_code,
						c.qty,
						0 as status, 
						c.req_pallet, 
						c.uom_plt, 
						e.material_name, 
						c.uom, 
						(c.qty - (c.uom_plt * SUM(b.quantity)) ) 
						as material_terscedule, (c.qty - (c.uom_plt * b.quantity) ) AS sisa,
						b.receive_amount,
						c.shift, b.quantity 
						FROM tb_schedule_detail AS a 
						INNER JOIN tb_Scheduler AS b 
						ON a.schedule_number = b.schedule_number 
						INNER JOIN tb_rds_detail AS c ON b.id_schedule = c.id 
						LEFT JOIN skin_master.ms_supplier AS d ON d.vendor_code = c.vendor_code 
						LEFT JOIN skin_master.ms_material AS e ON e.material_sku = c.material_code
						WHERE 							
							c.requested_delivery_date = '$date'
							AND c.shift = '$sa' AND a.vendor_code LIKE '%$sup%'
							AND c.category LIKE '%$category%'
							AND c.$menu LIKE '%$search_value%'
						GROUP BY b.id_schedule HAVING material_terscedule > 0
						")->result();
					
					$getDatapool = $this->db->query("SELECT b.vendor_alias, 
						a.material_code, 
						a.qty, 
						0 as status,
                        a.status,
						a.req_pallet, 
						a.uom_plt, 
						c.material_name, 
						a.uom,
						a.qty as material_terscedule, 
						0 as sisa,
						0 as receive_amount,
						a.shift, 
						a.req_pallet as quantity 
						FROM tb_rds_detail AS a
						INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code 
						INNER JOIN skin_master.ms_material AS c ON a.material_code = c.material_sku
                        LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
                        WHERE a.status = '0' AND d.id_schedule is null
							AND a.requested_delivery_date = '$date'
							AND a.shift = '$sa' AND a.vendor_code LIKE '%$sup%'
							AND a.category LIKE '%$category%'  AND a.status = '0'
							AND a.$menu LIKE '%$search_value%'
						")->result();
					$getData = array_merge($getDataz, $getDatapool, $getData2 );
					
				}else{
					$getDataz = $this->db->query("SELECT  c.id, d.vendor_alias, 
						c.material_code,
						c.qty,
						a.status, 
						c.req_pallet, 
						c.uom_plt, 
						e.material_name, 
						c.uom, 
						(c.uom_plt * b.quantity) 
						as material_terscedule, (c.qty - (c.uom_plt * b.quantity) ) AS sisa,
						b.receive_amount,
						c.shift, b.quantity 
						FROM tb_schedule_detail AS a 
						LEFT JOIN tb_Scheduler AS b 
						ON a.schedule_number = b.schedule_number 
						LEFT JOIN tb_rds_detail AS c ON b.id_schedule = c.id 
						LEFT JOIN skin_master.ms_supplier AS d ON d.vendor_code = c.vendor_code 
						LEFT JOIN skin_master.ms_material AS e ON e.material_sku = c.material_code
						WHERE 
							c.requested_delivery_date = '$date'
							AND c.shift = '$sa' AND a.vendor_code = '$sup'
							AND c.category LIKE '%$category%' 
							AND c.$menu LIKE '%$search_value%'
						GROUP BY b.id
						")->result();

					$getData2 = $this->db->query("SELECT  d.vendor_alias, 
						c.material_code,
						c.qty,
						0 as status, 
						c.req_pallet, 
						c.uom_plt, 
						e.material_name, 
						c.uom, 
						(c.qty - (c.uom_plt * SUM(b.quantity)) ) 
						as material_terscedule, (c.qty - (c.uom_plt * b.quantity) ) AS sisa,
						b.receive_amount,
						c.shift, b.quantity 
						FROM tb_schedule_detail AS a 
						INNER JOIN tb_Scheduler AS b 
						ON a.schedule_number = b.schedule_number 
						INNER JOIN tb_rds_detail AS c ON b.id_schedule = c.id 
						LEFT JOIN skin_master.ms_supplier AS d ON d.vendor_code = c.vendor_code 
						LEFT JOIN skin_master.ms_material AS e ON e.material_sku = c.material_code
						WHERE 							
							c.requested_delivery_date = '$date'
							AND c.shift = '$sa' AND a.vendor_code = '$sup'
							AND c.category LIKE '%$category%'
							AND c.$menu LIKE '%$search_value%' 
						GROUP BY b.id_schedule HAVING material_terscedule > 0
						")->result();
					


					$getDatapool = $this->db->query("SELECT a.id, b.vendor_alias, 
						a.material_code, 
						a.qty, 
						0 as status,
                        a.status,
						a.req_pallet, 
						a.uom_plt, 
						c.material_name, 
						a.uom,
						a.qty as material_terscedule, 
						0 as sisa,
						0 as receive_amount,
						a.shift, 
						a.req_pallet as quantity 
						FROM tb_rds_detail AS a
						INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code 
						INNER JOIN skin_master.ms_material AS c ON a.material_code = c.material_sku
                        LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
                        WHERE a.status = '0' AND d.id_schedule is null
							AND 
							a.requested_delivery_date = '$date'
							AND a.shift = '$sa' AND a.vendor_code = '$sup'
							AND a.category LIKE '%$category%'  AND a.status = '0'
							AND a.$menu LIKE '%$search_value%'
						")->result();
											
					$getData = array_merge($getDataz, $getDatapool, $getData2);
					
				}
				$arr[$date][$shift['shift_alias']] = $getData;
									
				}
		}

		/*dump($arr);*/
		// dump($arr);
		
		$get_date = $this->Rds_Model->get_date($week, $year);
		$getSupplier = $this->db->query("SELECT id, vendor_code, vendor_name, vendor_alias FROM skin_master.ms_supplier")->result();
		/*echo "<pre>".json_encode($arr, JSON_PRETTY_PRINT)."</pre>"; die();*/
		$data['list_date'] = $arrDate;
		$data['data'] = $arr;
		
		$data['arr_shift'] = $arr_shift;
		$data['year'] = $year;
		$data['week'] = $week;
		$data['supplier'] = $getSupplier;

		//dump($data);

		if(isset($_GET['GET'])){

			if($_GET['GET'] == 'Daily'){
				getHTML('rds/Daily',$data);	
			}else if($_GET['GET'] == 'List'){
				getHTML('rds/List',$data);
			}else{
				getHTML('rds/index',$data);	
			}

		}else{
			getHTML('rds/index',$data);	
		}		

	}

	public function index2()
	{
		$supplier = $this->input->get('vendor_code');
		$rds_data = $this->Rds_Model->Rds_Data();
		$data['data'] = $rds_data;

		$arr = array();

		$arr_shift = array(
			array(
				'shift_name' => "Dinas Malam",
				"shift_alias" => "DM"
			),
			array(
				'shift_name' => "Dinas Pagi",
				"shift_alias" => "DP"
			),
			array(
				'shift_name' => "Dinas Sore",
				"shift_alias" => "DS"
			)
		);

		$yearnow = date('Y');
		$ddate = date('Y-m-d');
		$date = new DateTime($ddate);
		$weeks = $date->format("W");
		$week = (null === $this->input->get('week'))?$weeks:$this->input->get('week');
		$year = (null === $this->input->get('year'))?$yearnow:$this->input->get('year');
		$arrDate = $this->getDateList($week, $year);
		$role = $this->session->userdata('sess_role_no');
		$vendor_code = $this->session->userdata('sess_vendor_code');
	}

	public function getDateList($week, $year){
		$dto = new DateTime();
		$ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
		
		$s = 1;
		while($s <= 6){
			$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
		  	$s++;
		}
		return $ret;
	}

public function download_rds()
    { 	
    	$week = $this->input->get('week');
    	$year = $this->input->get('year');
    	$category = $this->input->get('category');

    	$getDataz = $this->db->query("SELECT  d.vendor_alias, 
						c.material_code,
						c.qty,
						c.requested_delivery_date,
						a.status, 
						c.req_pallet, 
						c.uom_plt, 
						e.material_name, 
						c.uom, 
						(c.uom_plt * b.quantity) 
						as material_terscedule, (c.qty - (c.uom_plt * b.quantity) ) AS sisa, 
						c.shift, b.quantity 
						FROM tb_schedule_detail AS a 
						INNER JOIN tb_Scheduler AS b 
						ON a.schedule_number = b.schedule_number 
						INNER JOIN tb_rds_detail AS c ON b.id_schedule = c.id 
						LEFT JOIN skin_master.ms_supplier AS d ON d.vendor_code = c.vendor_code 
						LEFT JOIN skin_master.ms_material AS e ON e.material_sku = c.material_code
						WHERE c.week LIKE '%$week%' AND YEAR(c.requested_delivery_date) LIKE '%$year%' AND c.category LIKE '%$category%'
						GROUP BY b.id
						")->result();
					
					$getData2 = $this->db->query("SELECT  d.vendor_alias, 
						c.material_code,
						c.qty,
						0 as status, 
						c.req_pallet,
						c.requested_delivery_date, 
						c.uom_plt, 
						e.material_name, 
						c.uom, 
						(c.qty - (c.uom_plt * SUM(b.quantity)) ) 
						as material_terscedule, (c.qty - (c.uom_plt * b.quantity) ) AS sisa, 
						c.shift, b.quantity 
						FROM tb_schedule_detail AS a 
						INNER JOIN tb_Scheduler AS b 
						ON a.schedule_number = b.schedule_number 
						INNER JOIN tb_rds_detail AS c ON b.id_schedule = c.id 
						LEFT JOIN skin_master.ms_supplier AS d ON d.vendor_code = c.vendor_code 
						LEFT JOIN skin_master.ms_material AS e ON e.material_sku = c.material_code
						WHERE c.week LIKE '%$week%' AND YEAR(c.requested_delivery_date) LIKE '%$year%' AND c.category LIKE '%$category%'
						GROUP BY b.id_schedule HAVING material_terscedule > 0
						")->result();
					
					$getDatapool = $this->db->query("SELECT b.vendor_alias, 
						a.material_code, 
						a.qty, 
						0 as status,
                        a.status,
						a.req_pallet, 
						a.uom_plt, 
						a.requested_delivery_date,
						c.material_name, 
						a.uom,
						a.qty as material_terscedule, 
						0 as sisa, 
						a.shift, 
						a.req_pallet as quantity 
						FROM tb_rds_detail AS a
						INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code 
						INNER JOIN skin_master.ms_material AS c ON a.material_code = c.material_sku
                        LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
                        WHERE a.status = '0' AND d.id_schedule is null AND
						a.week LIKE '%$week%' AND YEAR(a.requested_delivery_date) LIKE '%$year%' AND a.category LIKE '%$category%'
						")->result();
					$getData = array_merge($getDataz, $getDatapool, $getData2 );
					

        $filePath = "./assets/templates/template_rds.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $no = 0;
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', date('d-m-Y'));
        $objPHPExcel->getActiveSheet()->SetCellValue('C2', $week);
        $objPHPExcel->getActiveSheet()->SetCellValue('C3', $category);
        foreach ($getData as $get) {
        	$objPHPExcel->getActiveSheet()->SetCellValue('A'.($no + 6), $no + 1);
        	$objPHPExcel->getActiveSheet()->SetCellValue('B'.($no + 6), $get->requested_delivery_date);
        	$objPHPExcel->getActiveSheet()->SetCellValue('C'.($no + 6), $get->shift);
        	$objPHPExcel->getActiveSheet()->SetCellValue('D'.($no + 6), $get->vendor_alias);
        	$objPHPExcel->getActiveSheet()->SetCellValue('E'.($no + 6), $get->material_code);
        	$objPHPExcel->getActiveSheet()->SetCellValue('F'.($no + 6), $get->material_name);
        	$objPHPExcel->getActiveSheet()->SetCellValue('G'.($no + 6), $get->material_terscedule);
        	$objPHPExcel->getActiveSheet()->SetCellValue('H'.($no + 6), $get->uom);
        	$no++;
        }

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="master_data_template.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');
    }

}
?>