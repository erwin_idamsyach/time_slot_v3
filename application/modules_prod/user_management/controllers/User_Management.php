<?php

class User_Management extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		isLogin();
	}

	public function index(){
		$getUser_uni = $this->db->query("SELECT a.id,a.nama,a.email,a.phone_number, a.username, a.vendor_code, b.role, c.category FROM ms_user a INNER JOIN ms_role b ON a.role = b.id
			LEFT JOIN ms_category c ON a.category = c.id WHERE a.role != 3 ");
		$getUser_sup = $this->db->query("SELECT a.id,a.nama,a.email,a.phone_number, a.username, a.vendor_code, b.role, c.vendor_name, c.vendor_alias FROM ms_user a INNER JOIN ms_role b ON a.role = b.id
			 LEFT JOIN skin_master.ms_supplier c ON a.vendor_Code = c.vendor_code WHERE a.role = 3 ");
		$data['data_user_uni'] = $getUser_uni;
		$data['data_user_sup'] = $getUser_sup;
		getHTML('user_management/index', $data);
	}

	public function add(){
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$getVendor = $this->db->query("SELECT vendor_code FROM skin_master.ms_supplier");
		$data['data_role'] = $getRole;
		$data['data_vendor'] = $getVendor;
		$data['data_cat']  = $getCate;
		getHTML('user_management/add', $data);
	}

	public function add_supplier(){
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$data['data_role'] = $getRole;
		$data['data_cat']  = $getCate;
		getHTML('user_management/add_vendor', $data);
	}

	public function select_vendor(){
		$vendor_code = $this->input->get('vendor_code');
		$data = $this->db->query("SELECT vendor_name FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->row()->vendor_name;

		echo json_encode($data);
	}

	public function edit(){
	 	$id = $_GET['id'];
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$getData = $this->db->query("SELECT a.*, b.vendor_alias, c.role as rolez, b.vendor_name FROM ms_user AS a LEFT JOIN skin_master.ms_supplier AS b ON b.vendor_code = a.vendor_code INNER JOIN ms_role AS c ON a.role = c.id where a.id='$id' ")->row();
		$getVendor = $this->db->query("SELECT vendor_code FROM skin_master.ms_supplier");
		$data['data_role'] = $getRole;
		$data['data_vendor'] = $getVendor;
		$data['data_cat']  = $getCate;
		$data['data_user'] = $getData;
		getHTML('user_management/edit', $data);
	}

	public function add_action(){
		$name = $this->input->post('name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email		= $this->input->post('email');
		$phone_number = $this->input->post('phone');
		$role = $this->input->post('role');
		if($role == 2){$category = $this->input->post('category');}else{$category = "";}
		if($role == 3){$vendor_code = $this->input->post('vendor_code');}else{$vendor_code = "";}

		$data = array(
			'nama' 			=> $name,
			"username" 		=> $username,
			"password" 		=> $password,
			"role" 			=> $role,
			"category" 		=> $category,
			"vendor_code" 	=> $vendor_code,
			"email"			=> $email,
			"phone_number"  => $phone_number,
			"created_at"	=> date('Y-m-d H:i:s')
			
		);
		$this->db->insert("ms_user", $data);

		$last_id = $this->db->query("SELECT id FROM ms_user ORDER BY id DESC")->row()->id;
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Add",
			"table_join"	=> 'ms_user',
			"id_join"		=> 	$last_id,
			"description" 	=> "New ".$role_user.",Name :".$name." Username : ".$username.$vendor." Email :".$email." Phone Number :".$phone_number,
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);

		$this->session->set_flashdata("SUCCESS", "User :".$username." berhasil ditambahkan!");
		redirect('user_management');
	}

	public function add_action_vendor(){
		
		$vendor_code = $this->input->post('vendor_code');
		$vendor_name = $this->input->post('vendor_name');
		$vendor_alias = $this->input->post('vendor_alias');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');

		$data = array(
			"vendor_code" 		=> $vendor_code,
			"vendor_name"		=> $vendor_name,
			"vendor_alias"		=> $vendor_alias,
			"email"				=> $email,
			"phone_number"		=> $phone
		);
		$this->db->insert('skin_master.ms_supplier', $data);

		$last_id = $this->db->query("SELECT id FROM skin_master.ms_supplier ORDER BY id DESC")->row()->id;
		$history = array(
				"date"			=> date('Y-m-d'),
				"time"			=> date('H:i:s'),
				"action"		=> "Add",
				"by_who"		=> $this->session->userdata('sess_id'),
				"table_join" 	=> "skin_master.ms_supplier",
				"id_join"		=> $last_id,
				"description"	=> "Add new Vendor, Vendor Code :".$vendor_code." Vendor Name :".$vendor_name."(".$vendor_alias.")",
				"value"			=> '',
				"value2"		=> '',
				"author"		=> 1
			);
		$this->db->insert('tb_history',$history);

		redirect('user_management');
	}

	public function edit_action(){		
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email		= $this->input->post('email');
		$phone_number = $this->input->post('phone');
		$role = $this->input->post('role');
		if($role == 2){$category = $this->input->post('category');}else{$category = "";}
		if($role == 3){$vendor_code = $this->input->post('vendor_code');}else{$vendor_code = "";}

		$data = array(
			'nama' 			=> $name,
			"username" 		=> $username,
			"password" 		=> $password,
			"role" 			=> $role,
			"category" 		=> $category,
			"vendor_code" 	=> $vendor_code,
			"email"			=> $email,
			"phone_number"	=> $phone_number,
			"update_at"	=> date('Y-m-d H:i:s')

		);
		$this->db->where('id', $id);
		$this->db->update("ms_user", $data);

		$last_id = $this->db->query("SELECT id FROM ms_user WHERE id='$id' ")->row()->id;
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Add",
			"table_join"	=> 'ms_user',
			"id_join"		=> 	$last_id,
			"description" 	=> "New ".$role_user.",Name :".$name." Username : ".$username.$vendor." Email :".$email." Phone Number :".$phone_number,
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);

		$this->session->set_flashdata("UPDATED", "User ".$username." berhasil diedit!");
		redirect('user_management');
	}

	public function delete(){
		$id = $this->input->get('id');
		$user = $this->db->query("SELECT a.*, b.role as role_name FROM ms_user as a INNER JOIN ms_role as b WHERE a.id ='$id' ")->row();
		$role_user = $user->role_name;
		$name = $user->nama;
		$username = $user->username;
		$email = $user->email;
		$phone_number = $user->phone_number;
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Delete",
			"table_join"	=> 'ms_user',
			"id_join"		=> 	$id,
			"description" 	=> ", Nama :".$name.", Username : ".$username.", Email :".$email.", Phone Number:".$phone_number,
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);
		
		$this->db->where('id', $id);
		$this->db->delete("ms_user");
		$this->session->set_flashdata("DELETED", "User :".$username." berhasil dihapus!");
		redirect('user_management');
	}

	public function ajax_vendor(){
		$vendor_code = $this->input->get('vendor_code');
		$data = $this->db->query("SELECT vendor_name, vendor_alias FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->result();
		echo json_encode($data);
	}

	public function delete_vendor(){
		$id = $this->input->post('id');
		$data = $this->db->query("SELECT vendor_code, vendor_name, vendor_alias FROM skin_master.ms_supplier WHERE id = '$id' ")->row();
		
		$vendor_code = $data->vendor_code;
		$vendor_name = $data->vendor_name;
		$vendor_alias = $data->vendor_alias;
		$this->db->query("DELETE FROM skin_master.ms_supplier WHERE id='$id'  ");
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Delete",
			"table_join"	=> 'ms_supplier',
			"id_join"		=> 	$id,
			"description"	=> "Add new Vendor, Vendor Code :".$vendor_code." Vendor Name :".$vendor_name."(".$vendor_alias.")",
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);

	}

	public function vendor_edit(){
		$id = $this->input->get('id');
		$getData = $this->db->query("SELECT * FROM skin_master.ms_supplier WHERE id = '$id' ")->row();
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$data['data_role'] = $getRole;
		$data['data_cat']  = $getCate;
		$data['data'] = $getData;
		getHTML('user_management/edit_vendor', $data);
	}

	public function vendor_update(){
		$id= $this->input->post('id');
		$vendor_code = $this->input->post('vendor_code');
		$vendor_name = $this->input->post('vendor_name');
		$vendor_alias = $this->input->post('vendor_alias');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');

		$data = array(
			"vendor_code" 		=> $vendor_code,
			"vendor_name"		=> $vendor_name,
			"vendor_alias"		=> $vendor_alias,
			"email"				=> $email,
			"phone_number"				=> $phone
		);

		$this->db->where('id', $id);
		$this->db->update('skin_master.ms_supplier', $data);
		$last_id = $this->db->query("SELECT id FROM skin_master.ms_supplier WHERE id='$id' ")->row()->id;

		$history = array(
				"date"			=> date('Y-m-d'),
				"time"			=> date('H:i:s'),
				"action"		=> "Update",
				"by_who"		=> $this->session->userdata('sess_id'),
				"table_join" 	=> "skin_master.ms_supplier",
				"id_join"		=> $last_id,
				"description"	=> "Update new Vendor, V.Code :".$vendor_code." V.Name :".$vendor_name."(".$vendor_alias.")",
				"value"			=> '',
				"value2"		=> '',
				"author"		=> 1
			);
		$this->db->insert('tb_history',$history);
		redirect('user_management');
	}

	public function vendor_list(){
		$this->load->view('user_management/vendor_list');
		return true;
	}

}

?>