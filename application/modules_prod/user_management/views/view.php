<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i class="fas fa-user" style="font-size:30px"></i>
                    </div>
                    <h4 class="card-title">View User</h4>
                    </div>
                    <div class="card-body ">
                      <form action="<?php echo base_url(); ?>user_management/add_action" method="post">
                      <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Name</label>
                                  <input type="text" disabled class="form-control" name="name" value="<?php echo $data_user->nama; ?>" required="">
                                </div>
                          </div> 
                          <div class="col-md-6">
                          </div>
                          <div class="col-md-3">
                            <span id="add_plus" style="position: absolute;margin-top:25px;margin-left:76px;font-size:50px;color:green"><i class="fas fa-plus"></i></span>
                            <img src="<?php echo base_url()."assets/upload/foto/".$data_user->foto; ?>" width="150" height="150" style="position: absolute;">
                            <!-- <input hidden type="file" id="imagez" name="foto"> -->
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Username</label>
                                  <input type="text" disabled class="form-control" value="<?php echo $data_user->username; ?>" name="username" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Password</label>
                                  <input type="password" disabled class="form-control" value="<?php echo $data_user->password; ?>" name="password" required="">
                                </div>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                  <label>Email</label>
                                  <input type="email" disabled value="<?php echo $data_user->emailz; ?>" class="form-control" name="email" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Phone Number</label>
                                  <input type="Number" disabled value="<?php echo $data_user->phone; ?>" class="form-control" name="phone" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <div class="form-group">
                                  <label>Role</label>
                                  <input type="text" disabled value="<?php echo $data_user->role_name; ?>" class="form-control" name="phone" required="">
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3" <?php if($data_user->role != 'Progressor'){echo "hidden='true'";} ?> id="category">
                            <div class="form-group">
                                  <div class="form-group">
                                  <label>Category</label>
                                  <input type="text" disabled value="<?php echo $data_user->category; ?>" class="form-control" name="phone" required="">
                                </div>
                            </div>
                          </div>
                      </div>

                       <div class="row" <?php if($data_user->role != 'Supplier'){echo "hidden='true'";} ?> id="vendor_form">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Code</label>
                                  <input type="text" disabled class="form-control" value="<?php echo $data_user->vendor_code; ?>" name="vendor_code" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Name</label>
                                  <input type="text" disabled class="form-control" value="<?php echo $data_user->vendor_name; ?>" name="vendor_name" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Alias</label>
                                  <input type="text" disabled class="form-control" name="vendor_alias" value="<?php echo $data_user->vendor_alias; ?>" required="">
                                </div>
                          </div>
                      </div>

                      
                      </form>
                        
</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  