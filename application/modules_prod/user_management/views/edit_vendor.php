<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i class="fas fa-user-cog" style="font-size:30px"></i>
                    </div>
                    <h4 class="card-title">Edit Vendor</h4>
                    </div>
                    <div class="card-body ">

          <form action="<?php echo base_url(); ?>user_management/vendor_update" method="post">
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>Vendor Code</label>
                <input type="text" hidden name="id" value="<?php echo $data->id; ?>">
                <input required="true" value="<?php echo $data->vendor_code; ?>" type="text" class="form-control" name="vendor_code">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Vendor Name</label>
                <input required="true" type="text" value="<?php echo $data->vendor_name; ?>" class="form-control" name="vendor_name">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>Vendor Alias</label>
                <input required="true" type="text" value="<?php echo $data->vendor_alias; ?>" class="form-control" name="vendor_alias">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Email</label>
                <input required="true" type="text" value="<?php echo $data->email; ?>" class="form-control" name="email">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>Phone Number</label>
                <input required="true" type="text" value="<?php echo $data->phone_number; ?>" class="form-control" name="phone">
              </div>
            </div>

          </div>
          <div class="row">
            <div class="col-md-12">
              <button required="true" type="text" class="btn btn-primary"> Submit </button>
            </div>
          </div>

          </form>
        </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

