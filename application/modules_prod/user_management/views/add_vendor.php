<style type="text/css">
  .form-control{
    padding: 0.4375rem 0;
  }
</style>
<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i class="fas fa-user-friends" style="font-size:30px"></i>
                    </div>
                    <h4 class="card-title">Add Supplier</h4>
                    </div>
                    
                    <div class="card-body ">
                      <form action="<?php echo base_url(); ?>user_management/add_action_vendor" method="POST">
                        <div class="container-fluid">
                          <div class="row">
                            <div class="col-md-2">
                              <div class="form-group">
                                <label>Supplier Code</label>
                              </div>
                              <input type="text" class="form-control" name="vendor_code">
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Supplier Name</label>
                              </div>
                              <input type="text" class="form-control" name="vendor_name">
                            </div>

                            <div class="col-md-2">
                              <div class="form-group">
                                <label>Supplier Alias</label>
                              </div>
                              <input type="text" class="form-control" name="vendor_alias">
                            </div>

                             <div class="col-md-3">
                              <div class="form-group">
                                <label>Telephone</label>
                              </div>
                              <input type="number" class="form-control" name="telephone">
                            </div>

                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <button onclick='loading()' class="btn btn-primary" style="margin:20px"> Add Supplier</button>
                        </div>
                      </form>
                        
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  