<style type="text/css">
  /*table.dataTable.compact tbody th, table.dataTable.compact tbody td {
    padding: 10px!important;
}

.btn.btn-sm, .btn-group-sm>.btn, .btn-group-sm .btn {
    padding: 0.40625rem 1.25rem;
    font-size: 0.675rem;
    line-height: 1.5;
    border-radius: 1.2rem;
}*/

/*.dataTable>thead>tr>th, .dataTable>tbody>tr>th, .dataTable>tfoot>tr>th, .dataTable>thead>tr>td, .dataTable>tbody>tr>td, .dataTable>tfoot>tr>td {
    padding: 2px !important;
    font-size: 12px;
    font-weight: 600;
}*/


/*.btn.btn-sm, .btn-group-sm>.btn, .btn-group-sm .btn {
    padding: 1 1.5rem;
    font-size: 4px;
    line-height: 1.5;
    border-radius: 6.2rem;
}*/
@media screen and (min-width: 400px){

}

@media screen and (min-width: 800px){
  .action-button{
    position: absolute;
    display: inline-block;
  }
}

td{
  white-space: nowrap;
}
</style>

<div class="content">
  <div class="content">
   
    <div class="container-fluid">
       
            <div class="row">
               <div class="col" style="font-size:13px;font-weight: 650">
              <?php
                if($this->session->flashdata('SUCCESS') != ""){
                  ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?php echo $this->session->flashdata('SUCCESS'); ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                  <?php
                }else if($this->session->flashdata('FAILED') != ""){
                  ?>
                <div class="alert alert-error" style="display: block !important;"><?php echo $this->session->flashdata('FAILED'); ?></div>
                  <?php
                }else if($this->session->flashdata('DELETED') != ""){
                  ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <?php echo $this->session->flashdata('DELETED'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                  <?php
                } else if($this->session->flashdata('UPDATED') != ""){
                  ?>
                  <div class="alert alert-warning alert-dismissible fade show" role="alert">
                      <?php echo $this->session->flashdata('UPDATED'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                  <?php
                }
                ?>
            </div>
            
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-primary card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-users-cog"></i>
                    </div>
                    <h4 class="card-title">User Management Supplier</h4>
                  </div>
                  <div class="card-body ">
                <b style="font-size:14px">Data PIC Supplier</b><br>
                <div class="col-6 action-button" >
                  <a href="<?php echo base_url(); ?>user_management/add?sup" class="btn btn-sm btn-primary">
                    <i class="fa fa-plus"></i> ADD PIC SUPPLIER
                  </a> 

                  <a href="<?php echo base_url(); ?>user_management/add_supplier" class="btn btn-warning btn-sm">
                    <i class="fa fa-plus"></i> ADD SUPLIER
                  </a>
              </div>
            

            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="tables">
                <thead style="color:white; background-color:#31559F;font-weight: 650; white-space: nowrap;">
                  <tr>
                    <th class="text-center" width="50">No</th>
                    <th class="text-center">PIC</th>
                    <th class="text-center">Username</th>
                    <th class="text-center">Supplier Code</th>
                    <th class="text-center">Supplier Name</th>
                    <th class="text-center">Supplier Alias</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">No Telepon</th>
                   
                    <?php
                    if($this->session->userdata('sess_role_no') == 1){
                      ?>
                    <th width="200" class="text-center">Action</th>
                      <?php
                    }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $a = 1;
                  foreach($data_user_sup->result() as $get){
                    ?>
                    <tr>
                      <td><?php echo $a++; ?></td>
                      <td><?php echo $get->nama; ?></td>
                      <td><?php echo $get->username; ?></td>
                      <td><?php echo $get->vendor_code; ?></td>
                      <td><?php echo $get->vendor_name; ?></td>
                      <td><?php echo $get->vendor_alias; ?></td>
                      <td><?php echo $get->email; ?></td>
                      <td><?php echo $get->phone_number; ?></td>
                     
                      <?php
                      if($this->session->userdata('sess_role_no') == 1){
                        ?>
                      <td class="text-center" width="100">
                        <?php
                        if($this->session->userdata('sess_role_no') == 1){
                          ?>
                           <a onclick='loading()' href="<?php echo base_url(); ?>profile?id=<?php echo $get->id; ?>"><button class="btn btn-info btn-sm">
                          <i class="fa fa-eye"></i>
                        </button></a>
                        <a onclick='loading()' href="<?php echo base_url(); ?>user_management/edit?id=<?php echo $get->id; ?>"><button class="btn btn-warning btn-sm">
                          <i class="fa fa-edit"></i>
                        </button></a>
                        <button class="btn btn-danger btn-sm" onclick="deletez(<?php echo $get->id; ?>)">
                          <i class="fa fa-trash"></i>
                        </button>
                          <?php
                        }
                        ?>
                      </td>
                        <?php
                      }
                      ?>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <hr style="border-color:grey">
            <b style="position: absolute; margin-top:15px;font-size:14px">Data Supplier</b>
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="tables3">
              <thead style="color:white; background-color:#31559F;font-weight: 650;">
                <tr align="center">
                  <th style="font-size:13px">No</th>
                  <th style="font-size:13px">Vendor Code</th>
                  <th style="font-size:13px">Vendor Name</th>
                  <th style="font-size:13px">Vendor Alias</th>
                  <th style="font-size:13px">Email</th>
                  <th style="font-size:13px">Telepon</th>
                  <th style="font-size:13px" width="100">Action</th>
                </tr>
              </thead>
              <tbody id="vendor_list">
                
              </tbody>

            </table>
          </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

    
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-users-cog"></i>
                    </div>
                    <h4 class="card-title">User Management Unilever</h4>
                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->

          <div class="row">
            <div class="col-md-6 action-button" >
              
                <a href="<?php echo base_url(); ?>user_management/add" class="btn btn-primary btn-sm">
                  <i class="fa fa-plus"></i> ADD USER
                </a>      
            </div>

          </div>

          <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="tables2" >
                <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
                  <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">PIC</th>
                    <th class="text-center">Username</th>
                    <th class="text-center">Role</th>
                    <th class="text-center">Category</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">No Telepon</th>
                    
                    <?php
                    if($this->session->userdata('sess_role_no') == 1){
                      ?>
                    <th class="text-center">Action</th>
                      <?php
                    }
                    ?>
                  </tr>
                </thead>
                <tbody >
                  <?php
                  $a = 1;
                  foreach($data_user_uni->result() as $get){
                    ?>
                    <tr >
                      <td><?php echo $a++; ?></td>
                      <td><?php echo $get->nama; ?></td>
                      <td><?php echo $get->username; ?></td>
                      <td  align="center"><?php echo $get->role; ?></td>
                      <td  align="center"><?php echo ($get->role=="Progressor")?$get->category:"-"; ?></td>
                      <td><?php echo $get->email; ?></td>
                      <td><?php echo $get->phone_number; ?></td>
                      <?php
                      if($this->session->userdata('sess_role_no') == 1){
                        ?>
                      <td class="text-center">
                        <?php
                        if($this->session->userdata('sess_role_no') == 1){
                          ?>
                          <a onclick='loading()' href="<?php echo base_url(); ?>profile?id=<?php echo $get->id; ?>"><button class="btn btn-info btn-sm">
                          <i class="fa fa-eye"></i>
                        </button></a>
                        <a onclick='loading()' href="<?php echo base_url(); ?>user_management/edit?id=<?php echo $get->id; ?>"><button class="btn btn-warning btn-sm">
                          <i class="fa fa-edit"></i>
                        </button></a>
                        <button class="btn btn-danger btn-sm" onclick="deletez(<?php echo $get->id; ?>)">
                          <i class="fa fa-trash"></i>
                        </button>
                          <?php
                        }
                        ?>
                      </td>
                        <?php
                      }
                      ?>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
        </div>
      
</div>

</div>
</div>
</div>


</div>
</div>
</div>