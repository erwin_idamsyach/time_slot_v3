<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i class="fas fa-user-cog" style="font-size:30px"></i>
                    </div>
                    <h4 class="card-title">Edit User</h4>
                    </div>
                    <div class="card-body ">
                      <form action="<?php echo base_url(); ?>user_management/edit_action" method="post" enctype='multipart/form-data'>
                        <input type="hidden" name="id" value="<?php echo $data_user->id; ?>">
                      <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Name</label>
                                  <input type="text" class="form-control" name="name" value="<?php echo $data_user->nama; ?>" required="">
                                </div>
                          </div>
                          <div class="col-md-6">
                          </div>
                          <div class="col-md-3">
                            <?php 
                            $foto = base_url()."assets/upload/foto/".$data_user->foto;
                            if(@getimagesize($foto)){
                               
                            }else{
                              $foto = base_url()."assets/images/dummy-profile.png";
                            }
                            ?>

                            <span id="add_plus" style="position: absolute;margin-top:25px;margin-left:76px;font-size:50px;color:green"><i class="fas fa-plus"></i></span>
                            <img id="foto" src="<?php echo $foto; ?>" onclick="$('#imagez').click()" onmouseout="$(this).css('opacity',1),$('#add_plus').css('opacity',1)" onmouseover="$(this).css('opacity',0.5),$('#add_plus').css('opacity',0.5)" src="#" width="200" height="200" style="position: absolute;margin-top:-50px">
                            <input hidden type="file" id="imagez" name="foto">
                          </div> 
                      </div>

                      <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Username</label>
                                  <input value="<?php echo $data_user->username; ?>" type="text" class="form-control" name="username" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Password</label>
                                  <input value="<?php echo $data_user->password; ?>" type="password" class="form-control" name="password" required="">
                                </div>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                  <label>Email</label>
                                  <input value="<?php echo $data_user->email; ?>" type="email" class="form-control" name="email" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Phone Number</label>
                                  <input value="<?php echo $data_user->phone_number; ?>" type="Number" class="form-control" name="phone" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <!-- <label>Role</label> -->

                                    <select required class="form-control" name="role" onchange="if(this.value == 3){$('#vendor_form').attr('hidden',false);$('#category').attr('hidden',true);}else if(this.value == 2){$('#category').attr('hidden',false);$('#vendor_form').attr('hidden',true);}else{$('#vendor_form').attr('hidden',true);$('#category').attr('hidden',true);}">
                                      <?php if(!isset($_GET['sup'])){ ?>
                                          <option value="">Select Role</option>
                                          <?php
                                        foreach($data_role->result() as $data){
                                          if($data->id != '3'){
                                          ?>
                                        <option <?php if($data_user->rolez == $data->role){ echo "selected";} ?> value="<?php echo $data->id; ?>"><?php echo $data->role ?></option>
                                          <?php
                                          }
                                        }
                                      }else{
                                        ?>
                                      <option value="3">Supplier</option>
                                        <?php
                                      }
                                    ?>
                                    </select>
                                </div>
                          </div>

                          
                      </div>

                      <div class="row" >
                        <div class="col-md-3" style="height: 70px" <?php if($data_user->role != '2'){echo "hidden='true'"; } ?> id="category">
                            <div class="form-group">
                                   <!--  <label>Category</label> -->
                                    <select class="form-control" name="category">
                                      <option value="">Select Category</option>
                                      <?php
                                    foreach($data_cat->result() as $data){
                                      ?>
                                    <option <?php if($data_user->category == $data->id){echo "selected";} ?> value="<?php echo $data->id; ?>"><?php echo $data->category ?></option>
                                      <?php
                                    }
                                    ?>
                                    </select>
                                </div>
                          </div>

                      </div>

                       <div class="row" <?php if($data_user->role != '3'){echo "hidden='true' ";} ?> id="vendor_form">

                          <div class="col-md-3">
                            <div class="form-group">
                                  <!-- <label>Vendor Code</label> -->
                                  <select class="form-control" name="vendor_code" onchange="vendor_complete(this.value)" style="color:black">
                                    <option value="" style="color:black">-- SELECT SUPPLIER --</option>
                                    <?php 
                                    foreach ($data_vendor->result() as $get) {
                                      ?>
                                      <option <?php if($data_user->vendor_code == $get->vendor_code){echo "selected";} ?> value="<?php echo $get->vendor_code;?>" style="color:black"><?php echo $get->vendor_code;?></option>
                                      <?php 
                                    }
                                    ?>
                                  </select>
                                </div>
                          </div>
                          
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Name</label>
                                  <input type="text" id="vendor_name" value="<?php echo $data_user->vendor_name; ?>" disabled class="form-control" name="vendor_name">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Alias</label>
                                  <input type="text" value="<?php echo $data_user->vendor_alias; ?>" id="vendor_alias" disabled class="form-control" name="vendor_alias">
                                </div>
                          </div>

                          
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                          <button class="btn btn-primary">SUBMIT</button>
                        </div>
                      </div>
                      </form>
                        
</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  