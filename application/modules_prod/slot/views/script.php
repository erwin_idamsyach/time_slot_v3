<script>
	
	function category_select(category){
		var year = "<?php echo $_GET['year']; ?>";
		var week = "<?php echo $_GET['week']; ?>";
		
		window.location = ('<?php base_url() ?>slot?year='+year+'&week='+week+'&category='+category);
	}

	$("#table").DataTable({
		"scrollX": true,
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
		
	});

	$("#table_info").css('padding',0);

	$(document).ready(function(){
		// datatablez();
	});
	
		// function datatablez(){
		// $('#datatables').DataTable({
  //       "pagingType": "full_numbers",
  //       "lengthMenu": [
  //         [5, -1],
  //         [5]
  //       ]
  //     });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });
	

	function viewPODetail(po_number){
		$.ajax({
			"type" : "GET",
			"url"  : "<?php echo base_url(); ?>slot/get_data_detail",
			"data" : "po_number="+po_number,
			success:function(resp){
				openModal("DETAIL PO NUMBER : "+po_number, resp);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		});
	}

	function setWeek(week, state){
		/*alert(week+" "+state);*/
		var category = $('#category').val();
		var year = $('#year').val();
		if(state == "INC"){
			week = parseInt(week)+1;
			$('.week').val(week);
		}else{
			week = parseInt(week)-1;
			$('.week').val(week);
		}
		window.location=('<?php base_url() ?>slot?year='+year+'&week='+week+'&category='+category);
	}


	function setYear2(year, state){
		var category = $('#category').val();
		var year = $('#year').val();
		var week = $('.week').val();
		window.location=('<?php base_url() ?>slot?year='+year+'&week='+week+'&category='+category);
	}


	function setYear(year, state){
		/*alert(week+" "+state);*/
		var year = $('#year').val();
		var week = $('.week').val();
		var category = $('#category').val();
		if(state == "INC"){
			year = parseInt(year)+1;
			$('#year').val(year);
		}else{
			year = parseInt(year)-1;
			$('#year').val(year);
		}
		window.location=('<?php base_url() ?>slot?year='+year+'&week='+week+'&category='+category);
	}

	function getTruckDetail(truck_no, dudate){
		$("#title-truck").html('<label>Truck No <i class="fa fa-spinner fa-spin fa-temp"></i></label>');

		$.ajax({
			type : "GET",
			url : "<?php echo base_url() ?>slot/get_truck_detail",
			data : {
				'truck_no' : truck_no,
				'date'	   : dudate
			},
			success:function(resp){
				$(".area-slot_ava").html("<h5>Slot Available : <b>"+resp+"</b></h5>")
				$(".fa-temp").remove();
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
				$(".fa-temp").remove();
			}
		})
	}

	function deliveryDetail(id){
		$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>slot/get_delivery_detail",
			data : {
				"id" : id
			},
			success:function(resp){
				openModal("Delivery Detail", resp);
			},
			error:function(e){
				console.log(e);
			}
		})
	}

	$("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 	});

 	// $('#checkitem').on('click', function(){
 	// 	$('#deletex').attr('disabled', false);
 	// });

 	function check_delete(){
 		if(($('input:checkbox:checked').length) == 1){
 			var confirmz = confirm('Are you sure you want to delete this?');
 		}else{
 			var confirmz = confirm('Are you sure you want to delete this all?');
 		}
 		
 		var check_item = [];
 		var a = 0;
            $.each($("input[name='check_item']:checked"), function(){            
                check_item.push({"check_item" : $(this).val()});
            	// check_item[a] = $(this).val();
            	// a += 1;
            });
            // alert("Item yang di check adalah: " + check_item[0].check_item);
            // console.log(check_item);

 		$.ajax({
 			type : "GET",
 			url  : "<?php echo base_url(); ?>slot/check_delete",
 			data : {
 				"check_item" : check_item,
 			},
 			dataType : "JSON",
 			success:function(resp){
 				//location.reload();
 			},
 			error: function(e){
 				//location.reload();
 				console.log(e);
 			}
 		});
 		
 	}

</script>