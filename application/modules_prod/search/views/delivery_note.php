<style type="text/css">
  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 0px 8px;
    /*font-size: 13px;*/
    vertical-align: middle;
    border-color: #ddd;
}
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-shipping-fast"></i>
                    </div>
                    <h4 class="card-title">Search Receipt</h4>
                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->
        <div class="box">
            <div class="row">
            <div class="col-md-2">
              <?php $year=$_GET['year']; $week=$_GET['week']; ?>
                     <div class="form-group">
                  <label style="margin-left:70px">Year</label>
                  <nav aria-label="Page navigation example">
                      <ul class="pagination pagination-primary">
                        
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>"><?php echo $year-1; ?></a>
                        </li>
                        
                        <li class="page-item active">
                          <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>"><?php echo $year; ?></a>
                        </li>

                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>"><?php echo $year+1; ?></a>
                        </li>

                      </ul>
                    </nav>
                  </div>
          </div>

          <div class="col-md-3 col-xl-3 col-sm-6">
          <div class="form-group">
            <label style="margin-left:85px">Week</label>
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-primary">
                  
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>&category=<?php echo $_GET['category'];?>"><?php echo $week-2; ?></a>
                  </li>
                   <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>&category=<?php echo $_GET['category'];?>"><?php echo $week-1; ?></a>
                  </li>
                  
                  <li class="page-item active">
                    <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $_GET['category'];?>"><?php echo $week; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>&category=<?php echo $_GET['category'];?>"><?php echo $week+1; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>&category=<?php echo $_GET['category'];?>"><?php echo $week+2; ?></a>
                  </li>
                  

                </ul>
              </nav>
            </div>
          </div>
      </div>

            <div class="box-body">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="table">
                    <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px;white-space: nowrap;">
                        <tr>
                            <th style="font-size:14px" class="text-center">Receipt</th>
                            <th style="font-size:14px" class="text-center">Supplier</th>
                            <th style="font-size:14px" class="text-center">Deliver To</th>
                            <th style="font-size:14px" class="text-center">No Truck</th>
                            <th style="font-size:14px" class="text-center">RDD</th>
                            <th style="font-size:14px" class="text-center">Time Slot</th>
                            <th style="font-size:14px" class="text-center">Status</th>
                            <th style="font-size:14px" class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($dn_list->result() as $get){
                            ?>
                        <tr>
                            <td style="font-size:12px"><?php echo $get->receipt; ?></td>
                            
                            <td style="font-size:12px"><?php echo $get->vendor_name." ( ".$get->vendor_alias." )"; ?></td>
                            <td style="font-size:12px">9003</td>
                            <td style="font-size:12px"><?php echo $get->id_truck; ?></td>
                            <td style="font-size:12px"><?php
                            $rdd = date_create($get->delivery_date);
                            $df = date_format($rdd, "D, d M Y");
                            echo $df;
                            ?></td>
                            <td style="font-size:12px"><?php echo $get->TIME_SLOT; ?></td>
                            <td  style="font-size:10px"class="text-center" style="font-size:11px">
                                <?php LegendIcon($get->status); ?>
                            </td>
                            <td style="font-size:12px" class="text-right">
                                <button class="btn btn-info btn-sm" onclick="getDetailedInformation(<?php echo $get->sn;?>, <?php echo $get->status;  ?>)">
                                    <i class="fa fa-eye"></i>
                                </button>
                            </td>
                        </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
              </div>
            </div>
        </div>
    </section>
</div>

</div>
</div>
</div>
</div>
</div>
</div>