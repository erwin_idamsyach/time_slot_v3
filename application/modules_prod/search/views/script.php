<script>
	
	$(document).ready(function(){
		<?php if($this->uri->segment(2) == 'delivery_note'){ ?>
		datatablez();
		<?php }else{ ?>
		searchByMaterial();
	<?php } ?>
    });

	var link = "<?php $this->uri->segment('2') ?>";
	

	if(link == 'material'){
		$("#table").DataTable({
			"scrollX": true,
			ordering: false,
			responsive: true,
			"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'		
		});
	}else{
		$("#table").DataTable({		
			ordering: false,
			responsive: true,
			"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
		});
	}

	function datatablez(){
		$('#datatables').DataTable({
        
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        "scrollX": true,
		ordering: false,
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search Delivery",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

	}

	var BASE_URL = "<?php echo base_url(); ?>";
	function searchByDn(){
		var keyword = $('.keyword').val();
		
		$.ajax({
			type : 'GET',
			url : BASE_URL+"search/dn_action",
			data :{
				'keyword' : keyword
			},
			success:function(resp){
				$(".area-result").html(resp);
				datatablez();
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})
	}

	function searchByMaterial(){
		
		var keyword = "<?php echo (isset($_GET['material']))?$_GET['material']:''; ?>";
		var supplier = "<?php echo (isset($_GET['supplier']))?$_GET['supplier']:''; ?>";
		var calender = $('#calender').val();
		var category = "<?php echo $this->input->get('category'); ?>";
		var check = 0;
		var year = "<?php echo (isset($_GET['year']))?$_GET['year']:''; ?>";
		var week = "<?php echo (isset($_GET['week']))?$_GET['week']:''; ?>";
		$.ajax({
			type : 'GET',
			url : BASE_URL+"search/material_action",
			data :{
				'keyword' : keyword,
				'supplier' :supplier,
				'calender' : calender,
				'category' : category,
				'year'		: year,
				'week'		: week
			},
			success:function(resp){
				check = 1;
				$("#area-result").html(resp);
				$(".data-table").DataTable();
				
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			},async:false
		})
		
		if(check == 1){
			
		}
	}

	function select_material(supplier){

		if(supplier == ''){
			$('#materialz').attr('disabled', true);
		}else{
			$('#materialz').attr('disabled', false);
			$('#materialz').empty();
			$.ajax({
				type : "GET",
				url : "<?php echo base_url() ?>search/ajax_get_material",
				data : {
					'supplier' : supplier
				},
				dataType: "html",
				success:function(resp){
					$('#materialz').append(resp);
				},
				error:function(e){
					alert("Something wrong!");
					console.log(e);
				}
			});
		}

	}

	function getDetailedInformation(key,status){
		$(".modal").modal('hide');
		setTimeout(function(){
			$.ajax({
				type : "GET",
				url : "<?php echo base_url() ?>schedule/get_detailed_information",
				data : {
					'key' : key,
					'status' : status
				},
				success:function(resp){
					openModal("Information", resp, "lg");
				},
				error:function(e){
					alert("Something wrong!");
					console.log(e);
				}
			})
			
		}, 200);	
	}

	
</script>