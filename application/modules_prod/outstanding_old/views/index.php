<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-dolly-flatbed"></i>
                    </div>
                    <h4 class="card-title">Outstanding PO</h4>

                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->
        <div class="box">
           
            <div class="box-body">
                <div class="row">
                <div class="col-md-2 col-xl-2 col-sm-6">
                      <div class="form-group">
                        <?php
                            $year = $_GET['year'];
                            $week = $_GET['week'];
                         ?>
                        <label style="margin-left:70px">Year</label>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination pagination-primary">
                              
                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>"><?php echo $year-1; ?></a>
                              </li>
                              
                              <li class="page-item active">
                                <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $year; ?></a>
                              </li>

                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>"><?php echo $year+1; ?></a>
                              </li>

                            </ul>
                          </nav>
                        </div>
                      </div>

          <div class="col-md-3 col-xl-3 col-sm-6">
          <div class="form-group">
            <label style="margin-left:85px">Week</label>
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-primary">
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>"><?php echo $week-2; ?></a>
                  </li>
                   <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>"><?php echo $week-1; ?></a>
                  </li>
                  <li class="page-item active">
                    <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $week; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>"><?php echo $week+1; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>"><?php echo $week+2; ?></a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
            </div>
        </div>

                <div class="table-responsive" >
                      <table class="table table-striped table-bordered table-hover" id="table">
                    <thead style="color:white; background-color:#31559F;font-weight: 650">
                        <tr>
                            <th style="font-size:14px" class="text-center">Category</th>
                            <th style="font-size:14px" class="text-center">Material Code</th>
                            <th style="font-size:14px" class="text-center">Material Name</th>
                            <th style="font-size:14px"  class="text-center">Vendor Code</th>
                            <th style="font-size:14px" class="text-center">RDD</th>
                            <th style="font-size:14px" class="text-center">Shift</th>
                            <th style="font-size:14px" class="text-center">Req. Qty</th>
                            <th style="font-size:14px" class="text-center">Qty. (Pallet)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach($pool_list as $get){
                            ?>
                        <tr>
                            <td style="font-size:12px" align="center" style="height: 35px"><?php echo $get->category; ?></td>
                            <td style="font-size:12px" align="center"><?php echo $get->material_code; ?></td>
                            <td style="font-size:12px"><?php echo $get->material_name; ?></td>
                            <td style="font-size:12px" align="center"><?php echo $get->vendor_code; ?></td>
                            <td style="font-size:12px" align="center" width="120"><?php
                            $rdd = date_create($get->requested_delivery_date);
                            $df = date_format($rdd, 'D, d M Y');
                            echo $df;
                            ?></td>
                            <td style="font-size:12px" align="center"><?php echo $get->shift; ?></td>
                            <td style="font-size:12px">
                                <?php 
                                echo ($get->sisa != null)?$get->sisa*$get->uom_plt:$get->req_pallet*$get->uom_plt; 
                                echo " ".$get->uom;?>
                            </td>
                            <td style="font-size:12px" class="text-center"><?php echo ($get->sisa != null)?$get->sisa:$get->req_pallet; ?></td>
                        </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </section>
</div>

</div>
</div>
</div>
</div>
</div>
</div>