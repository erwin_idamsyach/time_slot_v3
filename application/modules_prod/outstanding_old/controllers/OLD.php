<?php

class Outstanding extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('OS_Model');
		isLogin();
	}

	public function index(){
		$shift = getShift();
		$arr_vendor = array();
		$arr_id = array();
		$ven_list = $this->OS_Model->getVendorList();
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$sess_no = $this->session->userdata('sess_role_no');
		$pl = [];
		if($sess_no != 3){
			foreach ($ven_list->result() as $g) {
				array_push($arr_vendor, $g->vendor_code);
			}

			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialPool($imp, $year, $week);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model->getDateLess($year,$week, $imp);
		}else{
			$vencode = $this->session->userdata('sess_vendor_code');
			$dataPool = $this->OS_Model->getMaterialVendor($vencode, $year, $week);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model->getDateLessVen($year,$week,$imp,$vencode);
		}
		/*dump($dateLess->result());*/
		if($dataPool->num_rows() > 0 && $dateLess->num_rows() > 0){
			$pl = array_merge($dataPool->result(), $dateLess->result());
		}else{
			if($dataPool->num_rows() > 0 && $dateLess->num_rows() == 0){
				$pl = $dataPool->result();	
			}else if($dataPool->num_rows() == 0 && $dateLess->num_rows() > 0){
				$pl = $dateLess->result();	
			}
		}
		$data['pool_list'] = $pl;
		getHTML('outstanding/index', $data);
	}

	public function check(){
		$vendor = $this->input->GET('vendor');
		$check = $this->OS_Model->os_check($vendor);
	
		echo json_encode($check);
	}
}

?>