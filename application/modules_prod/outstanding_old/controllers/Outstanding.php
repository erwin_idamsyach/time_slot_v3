<?php

class Outstanding extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('OS_Model');
		isLogin();
	}

	public function index(){
		date_default_timezone_set('Asia/Jakarta');
		$shift = getShift();
		$role = $this->session->userdata('sess_role_no');
		$arr_vendor = array();
		$arr_id = array();
		$today  = date('Y-m-d');
		$ven_list = $this->OS_Model->getVendorList();
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$sess_no = $this->session->userdata('sess_role_no');
		$pl = [];
		if($sess_no != 3){
			foreach ($ven_list->result() as $g) {
				array_push($arr_vendor, $g->vendor_code);
			}

			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialPool($imp, $year, $week);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model->getDateLess($year,$week, $imp);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLate($today, $shift, $imp, $year, $week, $role);
		}else{
			$vc = $this->session->userdata('sess_vendor_code');
			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialVendor($year, $week, $sess_no, $vc);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			
			$dateLess = $this->OS_Model->getDateLessVen($year,$week, $imp, $sess_no, $vc);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLateVen($today, $shift, $imp, $year, $week, $sess_no, $vc);
		}
		/*dump($dateLess->result());*/
		$pl = array_merge($dataPool->result(), $dateLess->result(), $shiftLate->result());
		/*if($dataPool->num_rows() > 0 && $dateLess->num_rows() > 0 && $shiftLate->num_rows() > 0){
			
		}else{
			if($dataPool->num_rows() > 0 && $dateLess->num_rows() == 0){
				$pl = $dataPool->result();	
			}else if($dataPool->num_rows() == 0 && $dateLess->num_rows() > 0){
				$pl = $dateLess->result();	
			}
		}*/
		$data['pool_list'] = $pl;
		getHTML('outstanding/index', $data);
	}


	public function check(){
		$shift = getShift();
		$role = $this->session->userdata('sess_role_no');
		if($role == 1){
			$vendor_code = $this->input->get('vendor_code');
		}else{
			$vendor_code = $this->session->userdata('sess_vendor_code');
		}

		$data = [];
		$today  = date('Y-m-d');
		$sess_no = $this->session->userdata('sess_role_no');

		$os_check = $this->OS_Model->adt_os_check($shift, $sess_no, $vendor_code);
		
		echo json_encode($os_check);
		
	}
}

?>