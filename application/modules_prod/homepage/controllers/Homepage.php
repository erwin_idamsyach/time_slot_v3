<?php 
require_once APPPATH.'third_party/spout-master/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Homepage extends CI_Controller{

	function __Construct(){
		parent::__Construct();
	}

	public function index(){
		/*$getData = ;*/
		$date = date('d/m/Y');
		$date_find = date('Y-m-d');

		$getTimeSlot = $this->db->query("SELECT * FROM ms_time_slot");

		$data['curr_date'] = $date;
		$data['date_find'] = $date_find;
		$data['time_slot'] = $getTimeSlot;

		$this->load->view('homepage/index', $data);
	}

	public function search_schedule(){
		$date = $this->input->get('date');

		$df = str_replace("/", "-", $date);
		$df = date_create($df);
		$df = date_format($df, "Y-m-d");

		$getTimeSlot = $this->db->query("SELECT * FROM ms_time_slot");

		$data['date_find'] = $df;
		$data['time_slot'] = $getTimeSlot;

		$this->load->view('homepage/search_result', $data);
	}

}
?>