<link href='<?php echo base_url() ?>/assets/plugins/rds_plan/core/main.css' rel='stylesheet' />
<link href='<?php echo base_url() ?>/assets/plugins/rds_plan/daygrid/main.css' rel='stylesheet' />
<link href='<?php echo base_url() ?>/assets/plugins/rds_plan/timegrid/main.css' rel='stylesheet' />
<link href='<?php echo base_url() ?>/assets/plugins/rds_plan/list/main.css' rel='stylesheet' />
<script src='<?php echo base_url() ?>/assets/plugins/rds_plan/core/main.js'></script>
<script src='<?php echo base_url() ?>/assets/plugins/rds_plan/interaction/main.js'></script>
<script src='<?php echo base_url() ?>/assets/plugins/rds_plan/daygrid/main.js'></script>
<script src='<?php echo base_url() ?>/assets/plugins/rds_plan/timegrid/main.js'></script>
<script src='<?php echo base_url() ?>/assets/plugins/rds_plan/list/main.js'></script>
<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var no = 0;
    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'dayGrid','interaction', 'timeGrid', 'list' ],
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'timeGridWeek,timeGridDay,listMonth'
      },
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      navLinks: true, // can click day/week names to navigate views
      businessHours: true, // display business hours
      editable: true,
      eventLimit: true,
      view: {
        dayGrid: {
          eventLimit: 6
        }
      },
      events: [
      <?php foreach ($data->result() as $get) {
        ?>
        {
          id : "<?php echo $get->schedule_number; ?>",
          title: '<?php echo $get->vendor_alias; ?>',
          start: '<?php echo $get->requested_delivery_date.'T'.$get->start_time; ?>',
          end:   '<?php echo $get->requested_delivery_date.'T'.$get->end_time; ?>',
          color: '#257e4a',
          overlap: false,

        },

      <?php } ?>
        
      ],
      eventClick: function(info) {
        info.jsEvent.preventDefault(); // don't let the browser navigate

        if (info.event.title) {
          getDI(info.event.id);
        }
      }

    });

    calendar.render();
  });

 function getDI(key){
    $(".modal").modal('hide');
    setTimeout(function(){
      $.ajax({
        type : "GET",
        url : "<?php echo base_url() ?>schedule/get_DI",
        data : {
          'key' : key
        },
        success:function(resp){
          openModal("Information", resp, "lg");
          editTime($("#created_at").val());
        },
        error:function(e){
          alert("Something wrong!");
          console.log(e);
        }
      })
      
    }, 200);  
  }

</script>
<style>

  .card {
    
    padding: 0;
    font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 100%;
    margin: 0 auto;
  }

  .fc-content{
    font-size:13px;
  }
</style>

<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        RDS Plan
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
      <div class="box">
        <div class="box-header">
          <label>RDS Plan</label>
        </div>
        <div class="box-body">
          <div class="row">

          </div>
            <div class="row">
            <div class="col-md-12">
              <div id='calendar' style="font-size:15px"></div>
            </div>
          </div>
          
        </div>
      </div>
    </section>
</div>