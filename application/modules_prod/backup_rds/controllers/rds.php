<?php

class Rds extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('Rds_Model');
		isLogin();
	}

	public function index(){
		$rds_data = $this->Rds_Model->Rds_Data();
		$data['data'] = $rds_data;
		
		getHTML('rds/index',$data);
	}

}

?>