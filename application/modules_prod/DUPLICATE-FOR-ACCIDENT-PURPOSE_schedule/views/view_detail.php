<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        View Schedule Detail<br>
        <a href="<?php echo base_url()."schedule/index/".$this->uri->segment(4); ?>" class="btn btn-info">
          <i class="fa fa-arrow-left"></i> BACK
        </a>
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
      <div class="row">
        <div class="col-md-8">
          <div class="box">
            <div class="box-header">
              <label>Material List</label>
              <h4 id="area-cnt-pallet">Slot Occupancy : .../32</h4>
            </div>
            <div class="box-body">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th class="text-center">PO Number</th>
                    <th class="text-center">Material Code</th>
                    <th class="text-center">Material Name</th>
                    <th class="text-center">Req. Pallet</th>
                    <th class="text-center">*</th>
                  </tr>
                </thead>
                <tbody id="area-list">
                  <?php
                  foreach($data_list->result() as $get){
                    ?>
                  <tr>
                    <td class="text-center"><?php echo $get->po_number ?></td>
                    <td><?php echo $get->material_code ?></td>
                    <td><?php echo $get->material_name ?></td>
                    <td width="25px">
                      <input type="text" class="form-control inp-pallet" value="<?php echo $get->req_pallet ?>" readonly>
                    </td>
                    <td class="text-center">
                      <button class="btn btn-link clr-red" onclick="moveMaterial(<?php echo $get->id; ?>, <?php echo $sn; ?>)">
                        <i class="fa fa-times"></i>
                      </button>
                    </td>
                  </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <label>Material Pool</label>
                </div>
                <div class="box-body" id="area-pool">
                  
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <label>Find From Same RDD</label>
                </div>
                <div class="box-body" id="area-same-rdd"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <label>Delivery Form</label>
            </div>
            <div class="box-body">
              <form action="<?php echo base_url(); ?>schedule/save_schedule" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label>RDD</label><br>
                  <label>
                    <?php
                  $date = date_create($this->uri->segment(4));
                  $dfns = date_format($date, "D, d M Y");
                  echo $dfns;
                    ?>
                  </label>
                </div>
                <div class="form-group">
                  <label>Time Slot</label><br>
                  <label>
                    <?php
                    $this->Schedule_model->getTimeSlotById($this->uri->segment(5));
                    ?>
                  </label>
                </div><hr>
                <div class="form-group">
                  <label>Truck No.</label>
                  <input type="text" name="truck_no" class="form-control" required="">
                </div>
                <div class="form-group">
                  <label>Driver Name</label>
                  <input type="text" name="driver_name" class="form-control" required="">
                </div>
                <div class="form-group">
                  <label>Phone Number</label>
                  <input type="text" name="phone_number" class="form-control" required="">
                </div>
                <div class="form-group">
                  <label>Delivery Orders</label>
                  <input type="file" name="file" class="form-control" required="">
                </div>
                <div class="form-group">
                  <label>Delivery Orders Number</label>
                  <input type="text" name="do_number" class="form-control" required="">
                </div>
                <div class="form-group">
                  <input type="hidden" name="id_schedule" value="<?php echo $this->uri->segment(3) ?>">
                  <input type="hidden" name="rdd" value="<?php echo $this->uri->segment(4) ?>">
                  <input type="hidden" name="id_time_slot" value="<?php echo $this->uri->segment(5) ?>">
                  <button class="btn btn-primary">SUBMIT</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>