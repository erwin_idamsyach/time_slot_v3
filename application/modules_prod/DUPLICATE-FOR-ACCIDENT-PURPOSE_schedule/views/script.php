<script>
	$(document).ready(function(){
		callOffPallet();
	});
	var c_minus = 0;
	var vendor_code = "<?php echo $vendor_code ?>";
	var sn = "<?php echo (isset($sn))?$sn:'-'; ?>";
	var rdd = "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):'' ?>";

	function callOffPallet(){
		var a = 0;
		$(".inp-pallet").each(function(){
			var as = parseInt(this.value);
			a = a+as;
		});
		c_minus = 32-a;
		$("#area-cnt-pallet").html("Slot Occupancy : <b>"+a+" / 32</b>");

		getSuggestion(c_minus, sn);
	}

	function moveMaterial(id, sn){
		$.ajax({
			type : "GET",
			url  : "<?php echo base_url() ?>schedule/move_schedule",
			data : {
				'id' : id
			},
			success:function(resp){
				getNewScheduleList(sn);
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}

	function getNewScheduleList(sn){
		$.ajax({
			type : "GET",
			url  : "<?php echo base_url() ?>schedule/get_schedule_list",
			data : {
				'sn' : sn
			},
			success:function(resp){
				$("#area-list").html(resp);
				callOffPallet();
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}

	function getMaterialPool(){
		$.ajax({
			type : "GET",
			url  : "<?php echo base_url() ?>schedule/get_pool_schedule_list",
			data : {
				'vendor_code' : vendor_code,
				'rdd' : "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):"123" ?>"
			},
			success:function(resp){
				$("#area-pool").html(resp);
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}

	function getDetailedInformation(key){
		$.ajax({
			type : "GET",
			url : "<?php echo base_url() ?>schedule/get_detailed_information",
			data : {
				'key' : key
			},
			success:function(resp){
				openModal("Information", resp, "lg");
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})
		preventDefault();
	}

	function getSuggestion(c_min, sn){
		$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>schedule/get_suggestion",
			data : {
				'pallet_minus' : c_min,
				'sn' : sn,
				'rdd' : "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):"123" ?>"
			},
			success:function(resp){
				$("#area-same-rdd").html(resp);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})
	}

	function addMaterial(id, sn){
		$.ajax({
			type : 'GET',
			url : "<?php echo base_url() ?>schedule/add_material",
			data : {
				'id' : id,
				'sn' : sn
			},
			success:function(resp){
				getNewScheduleList(sn);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		});
	}

	function changeURL(){
		var dapick = $(".dapick").val();
		window.location=("<?php echo base_url() ?>schedule?date_start="+dapick);
	}
</script>