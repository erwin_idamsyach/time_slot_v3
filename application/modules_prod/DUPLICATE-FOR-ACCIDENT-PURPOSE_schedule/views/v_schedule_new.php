<?php
foreach($data_list->result() as $get){
  ?>
<tr>
  <td class="text-center"><?php echo $get->po_number ?></td>
  <td><?php echo $get->material_code ?></td>
  <td><?php echo $get->material_name ?></td>
  <td width="25px">
    <input type="text" class="form-control inp-pallet" value="<?php echo $get->req_pallet ?>" readonly>
  </td>
  <td class="text-center">
    <button class="btn btn-link clr-red" onclick="moveMaterial(<?php echo $get->id; ?>, <?php echo $sn; ?>)">
      <i class="fa fa-times"></i>
    </button>
  </td>
</tr>
  <?php
}
?>