<script>
	function viewPODetail(po_number){
		$.ajax({
			"type" : "GET",
			"url"  : "<?php echo base_url(); ?>slot/get_data_detail",
			"data" : "po_number="+po_number,
			success:function(resp){
				openModal("DETAIL PO NUMBER : "+po_number, resp);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		});
	}


	function getTruckDetail(truck_no, dudate){
		$("#title-truck").html('<label>Truck No <i class="fa fa-spinner fa-spin fa-temp"></i></label>');

		$.ajax({
			type : "GET",
			url : "<?php echo base_url() ?>slot/get_truck_detail",
			data : {
				'truck_no' : truck_no,
				'date'	   : dudate
			},
			success:function(resp){
				$(".area-slot_ava").html("<h5>Slot Available : <b>"+resp+"</b></h5>")
				$(".fa-temp").remove();
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
				$(".fa-temp").remove();
			}
		})
	}

	function deliveryDetail(id){
		$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>slot/get_delivery_detail",
			data : {
				"id" : id
			},
			success:function(resp){
				openModal("Delivery Detail", resp);
			},
			error:function(e){
				console.log(e);
			}
		})
	}

	$("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 	});

 	function check_delete(){
 		if(($('input:checkbox:checked').length) == 1){
 			var confirmz = confirm('Are you sure you want to delete this?');
 		}else{
 			var confirmz = confirm('Are you sure you want to delete this all?');
 		}
 		
 		var check_item = [];
 		var a = 0;
            $.each($("input[name='check_item']:checked"), function(){            
                check_item.push({"check_item" : $(this).val()});
            	// check_item[a] = $(this).val();
            	// a += 1;
            });
            // alert("Item yang di check adalah: " + check_item[0].check_item);
            // console.log(check_item);

 		$.ajax({
 			type : "GET",
 			url  : "<?php echo base_url(); ?>slot/check_delete",
 			data : {
 				"check_item" : check_item,
 			},
 			dataType : "JSON",
 			success:function(resp){
 				location.reload();
 			},
 			error: function(e){
 				location.reload();
 				console.log(e);
 			}
 		});
 		
 	}

</script>