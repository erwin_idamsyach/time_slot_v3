<?php
require_once APPPATH.'third_party/spout-master/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
class Slot extends CI_Controller{
	function __Construct(){
		parent::__Construct();		
	}

	public function index(){
		date_default_timezone_set("Asia/Bangkok");
		$datenow = date('Y-m-d 00:00:01', strtotime('-1 days'));
		if($this->session->userdata('sess_role_no') != 3){
			$getData = $this->db->query("SELECT
							a.*, b.vendor_alias,
							c.material_name,
							e. STATUS AS statz,
							(SELECT SUM(d.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND `status`='1') AS TOTAL
						FROM
							tb_rds_detail a
						LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
						LEFT JOIN tb_schedule_detail AS e ON d.schedule_number = e.schedule_number
						LEFT JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
						LEFT JOIN skin_master.ms_material c ON a.material_code = c.material_sku
						GROUP BY
							a.id
						ORDER BY
							a.requested_delivery_date,
							a.shift ASC");
		}else{
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$getData = $this->db->query("SELECT 
				a.*,
				b.vendor_alias,
				c.material_name,
				e.status as statz,
				SUM(d.quantity) AS TOTAL
				FROM 
				tb_rds_detail a
				LEFT JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code 
				LEFT JOIN skin_master.ms_material c ON a.material_code = c.material_sku
				LEFT JOIN  tb_scheduler as d ON a.id = d.id_schedule
				RIGHT JOIN tb_schedule_detail as e ON d.schedule_number = e.schedule_number
				WHERE a.vendor_code = '$vendor_code'
				AND a.requested_delivery_date >= '$datenow'
				 
				ORDER BY a.requested_delivery_date,a.shift ASC");
		}

		$data['rds'] = $getData;
		$data['sess_role'] = $this->session->userdata('sess_role_no');
		getHTML('slot/index', $data);
	}

	public function upload_excel(){
		date_default_timezone_set("Asia/Bangkok");
		$file = $this->input->post('file', TRUE);
		$arr_vl = array();
		$date_c='';
		$config['upload_path'] = './assets/upload/excel'; 
  		$config['file_name'] = $file;
  		$config['allowed_types'] = 'xlsx|csv|xls';
  		$config['max_size'] = 0;
  		$this->load->library('upload', $config);
	  	$this->upload->initialize($config);
	  	if (!$this->upload->do_upload('file')) {
	  		echo $this->upload->display_errors();
	   		$this->session->set_flashdata('ERR', $this->upload->display_errors());
	   		redirect('slot?status=error_f');
	  	} else {
	  		$media = $this->upload->data();
	   		$inputFileName = 'C:\xampp\htdocs\time_slot\assets\upload/excel/'.$media['file_name'];
	   		$fn = $media['file_name'];
	   		$exp = explode(".", $fn);
	   		$ext = $exp[1];
	   		$sheetname = 'Processor';
	   		if($ext == "xlsx"){
	   			$reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
	   		}else if($ext == "xls"){
	   			$reader = ReaderFactory::create(Type::XLS); //set Type file xls
	   		}
	   		$no = 1;
	   		$reader->open($inputFileName);
	        foreach ($reader->getSheetIterator() as $sheet) {
	        	if($sheet->getName() == $sheetname){
		        	foreach ($sheet->getRowIterator() as $rn => $get) {
		        		
		        		if($rn > 4){
		        			$rdd = $get[37];
		        			if(is_object($rdd)){
			        			 	$datenew = $rdd->format('Y-m-d');
			        			 }else{
			        			 	continue;
			        			 }

		        			$ars = array("DP","DS","DM");
		        			if(!empty($get[29]) && $datenew >= date('Y-m-d 00:00:01', strtotime('-1 days'))){
		        				 
			        			 $material_code = $get[1];
			        			 $MR = $get[5];
			        			 $get_category = explode(' ', $get[6]);
			        			 $category = $get_category[0];
			        			 $get_vendor = $get[23];
			        			 $po_number = $get[29];
			        			 $line_item = $get[30];
			        			 $vendor = explode(' ',$get_vendor);
			        			 $vendor_code = $vendor[0];
			        			 $qty = $get[31];
			        			 $uom = $get[21];
			     				 $shift = $get[38];
			        			 $uom_plt = $get[26];
			        			 $plt_truck = '32';
			        			 $wk = $get[38];
			        			 /*echo $wk." ".!in_array($wk, $ars)."<br>";*/
			        			 $req_plt = round($qty / $uom_plt);
			        			 $get_po_number = '';
			        			 $get_rdt = '';
			        			 $get_week = '';
			        			 $get_material = '';
			        			 $get_qty = '';
			        			 $get_material_name = $get[25];

			        			 $material_checker = $this->db->query("SELECT material_sku FROM skin_master.ms_material WHERE material_sku = '$material_code' ")->num_rows();
			        			 if($material_checker == 0){
			        			 	$array = array(
							   			"material_sku" => $material_code,
							   			"material_name" => $get_material_name,
							   			"material_uom"	=> $uom
							   		);

							   		$this->db->insert('skin_master.ms_material', $array);
			        			 }

			        			 $data_checker = $this->db->query("SELECT id, po_number,material_code, requested_delivery_date,shift, qty FROM tb_rds_detail where po_number='$po_number' AND requested_delivery_date = '$datenew' AND material_code ='$material_code' AND shift='$shift'  ")->row();

			        			 if(!empty($data_checker)){
			        			 	$get_po_number = $data_checker->po_number;
			        				$get_rdt = $data_checker->requested_delivery_date;
			        				$get_material = $data_checker->material_code;
			        				$get_shift = $data_checker->shift;
			        				$get_qty = $data_checker->qty;
			        			 }		   
			        			$data = array(
			        			 		"category" 	=> $category,
				        				"po_number" => $po_number,
				        				"po_line_item" => $line_item,
				        				"material_code" => $material_code,
				        				"vendor_code" => $vendor_code,
				        				"qty" => $qty,
				        				"uom" => $uom,
				        				"requested_delivery_date" => $datenew,
				        				"uom_plt" => $uom_plt,
				        				"plt_truck" => $plt_truck,
				        				"req_pallet" => $req_plt,
				        				"shift" => $shift,
				        				"week" => substr($wk,-2),
				        				'is_schedule'=> '0',
		        				);
			        			 if($get_po_number == $po_number && $datenew == $get_rdt && $get_material == $material_code){
			        			 	
								  }
			         			 else if($get_po_number == $po_number && $datenew == $get_rdt && $get_material == $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew <> $get_rdt && $get_material == $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew <> $get_rdt && $get_material == $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew <> $get_rdt && $get_material <> $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew == $get_rdt && $get_material <> $material_code){
		         					$this->db->insert('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == '' && $get_rdt == '' && $get_material == ''){
		         					$this->db->insert('tb_rds_detail', $data);
			        			 	
			         			}
		        			}else{
		        				continue;
		        			}
		        		}
		        	}
	        	}
	        }
	    }
	      $this->session->set_flashdata('SCS', 'Upload Success');
	      redirect('slot?status=success');
	}


	public function detail(){
		$po_number = $this->uri->segment(3);
		$material_code = $this->uri->segment(4);
		$id = $this->uri->segment(5);
		$getInfo = $this->db->query("SELECT 
			a.category, 
			a.po_number, 
			a.vendor_code, 
			a.requested_delivery_date,
			b.vendor_name,
			b.vendor_alias,
			a.week,
			a.shift
			FROM 
			tb_rds_detail a 
			INNER JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
			WHERE a.id='$id' ")->row();

		$getList = $this->db->query("SELECT 
			a.id, a.category, 
			a.po_number, 
			a.material_code, 
			a.vendor_code, 
			a.qty, 
			a.uom,
			a.uom_plt, 
			a.requested_delivery_date, 
			a.status, 
			b.material_name,
			c.vendor_name
			FROM 
			tb_rds_detail a 
			LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku 
			INNER JOIN skin_master.ms_supplier c ON a.vendor_code = c.vendor_code
			WHERE a.id='$id'");

		$data['po_number'] = $po_number;
		$data['info'] = $getInfo;
		$data['item_list'] = $getList;
		getHTML('slot/detail_po', $data);
	}

	public function delete(){
		$po_number = $this->uri->segment(3);
		$this->db->query("DELETE FROM tb_rds_detail WHERE po_number = '$po_number' ");
		redirect('slot?delete='.$po_number);
	}

	public function set_schedule(){
		$po_number = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$getTimeSlot = $this->db->query("SELECT * FROM ms_time_slot");
		$getData = $this->db->query("SELECT 
								a.*, 
								b.material_name,
								c.vendor_name
								FROM tb_rds_detail a 
								INNER JOIN skin_master.ms_material b ON a.material_code = b.material_sku 
								INNER JOIN skin_master.ms_supplier c ON a.vendor_code = c.vendor_code
								WHERE a.id='$id'");

		$data['po_number'] = $po_number;
		$data['data_po'] = $getData;
		$data['time_slot'] = $getTimeSlot;
		$data['id_schedule'] = $id;
		getHTML('slot/set_schedule', $data);
	}

	public function get_truck_detail(){
		$truck_no = $this->input->get('truck_no');
		$date = $this->input->get('date');
		
		$date = str_replace("/", "-", $date);
		$date_n = date_create($date);
		$date_new = date_format($date_n, 'Y-m-d');

		$cData = $this->db->query("SELECT SUM(pallet_count) as COUNT FROM tb_delivery_detail WHERE id_truck='$truck_no' AND delivery_date='$date_new'")->row();
		$count = $cData->COUNT;

		$available = 32-$count;
		echo $available;
	}

	public function save_schedule(){
		$id_schedule = $this->input->post('id_schedule');
		$delivery_date = $this->input->post('date');
		$time_slot = $this->input->post('time_slot');
		$truck_no = $this->input->post('truck_no');
		$f_invoice = $this->input->post('inv_file', TRUE);
		$po_number = $this->input->post('po_number');
		$slot_usage = $this->input->post('slot_usage');

		$date = str_replace("/", "-", $delivery_date);
		$date_n = date_create($date);
		$date_new = date_format($date_n, 'Y-m-d');

		$config['upload_path'] = './assets/upload/invoice'; 
  		$config['file_name'] = $po_number."_".$id_schedule."_".$f_invoice;
  		$config['allowed_types'] = 'docx|doc|pdf';
  		$config['max_size'] = 0;

  		$this->load->library('upload', $config);
	  	$this->upload->initialize($config);

	  	if (!$this->upload->do_upload('inv_file')) {
	   		$this->session->set_flashdata('ERR', $this->upload->display_errors()); 
	   		redirect('slot/set_schedule/'.$po_number."/".$id_schedule);
	  	} else {
	  		$media = $this->upload->data();
	   		$fileName = 'assets\upload/invoice/'.$media['file_name'];

	   		$array = array(
	   			"id_schedule" => $id_schedule,
	   			"pallet_count" => $slot_usage,
	   			"delivery_date" => $date_new,
	   			"id_time_slot" => $time_slot,
	   			"id_truck" => $truck_no,
	   			"invoice" => $fileName
	   		);

	   		$this->db->insert('tb_delivery_detail', $array);

	   		$this->db
	   			->where('id', $id_schedule)
	   			->update('tb_rds_detail', array(
	   				"status" => '1'
	   			));
	  	}
	  	redirect('slot/detail/'.$po_number);
	}

	public function check_delete(){
		$data = $this->input->get('check_item');
		$no = 0;
		foreach($data as $get){
			$check_item = $data[$no];
			$data_item = $check_item['check_item'];
			$this->db->query("DELETE FROM tb_rds_detail WHERE id = '$data_item' ");
			$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule = '$data_item' ");
			$no++;
		}
		echo "success";
		return true;
	}

	public function get_delivery_detail(){
		$id = $this->input->get('id');

		$getData = $this->db->query("SELECT
						a.delivery_date,
						a.id_time_slot,
						a.id_truck,
						a.invoice,
						a.pallet_count,
						b.po_number,
						b.material_code,
						b.vendor_code,
						b.qty,
						b.requested_delivery_date,
						c.material_name,
						d.start_time,
						d.end_time,
						e.vendor_name
					FROM
						tb_delivery_detail a
					INNER JOIN tb_rds_detail b ON a.id_schedule = b.id
					INNER JOIN skin_master.ms_material c ON b.material_code = c.material_sku
					INNER JOIN ms_time_slot d ON a.id_time_slot = d.id
					INNER JOIN skin_master.ms_supplier e ON b.vendor_code = e.vendor_code
					WHERE
						b.id='$id'");
		$data['data_list'] = $getData;
		$this->load->view('slot/get_list_delivery_data', $data);
	}

}
?>