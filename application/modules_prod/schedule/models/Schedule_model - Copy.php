<?php

class Schedule_model extends CI_Model{
	

	public function getSchedule($date, $vendor_code, $user, $shift){
	if($user == '3'){
		$data = $this->db->query("SELECT
							tb_rds_detail.id,
							tb_rds_detail.shift,
							material_code,
							tb_rds_detail.vendor_code,
							shift,
							qty,
							requested_delivery_date,
							req_pallet,
							IF(@rownum + req_pallet <= 32, @rownum := @rownum + req_pallet, @rownum := 0 + req_pallet) as row_number
						FROM
							tb_rds_detail
						LEFT JOIN tb_scheduler ON tb_rds_detail.id = tb_scheduler.id_schedule
						CROSS JOIN (SELECT @rownum := 0) r
						WHERE
							tb_rds_detail.vendor_code = '$vendor_code'
							AND
							tb_rds_detail.shift='$shift'
						AND 
						tb_scheduler.id_schedule IS NULL
						AND requested_delivery_date = '$date'
						AND tb_rds_detail.is_schedule = '0'");
	}else{
		$data = $this->db->query("SELECT
							tb_rds_detail.id,
							tb_rds_detail.shift,
							material_code,
							tb_rds_detail.vendor_code,
							qty,
							requested_delivery_date,
							req_pallet,
							IF(@rownum + req_pallet <= 32, @rownum := @rownum + req_pallet, @rownum := 0 + req_pallet) as row_number
						FROM
							tb_rds_detail
						LEFT JOIN tb_scheduler ON tb_rds_detail.id = tb_scheduler.id_schedule
						LEFT JOIN tb_schedule_pool ON tb_rds_detail.id = tb_schedule_pool.id_schedule
						CROSS JOIN (SELECT @rownum := 0) r
						WHERE 
						tb_scheduler.id_schedule IS NULL
						AND tb_schedule_pool.id_schedule IS NULL
						AND requested_delivery_date = '$date'
						AND tb_rds_detail.status = '0'");
	}
		return $data;
	}

	public function Download_Excel(){
		date_default_timezone_set("Asia/Bangkok");
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle("Week ".$_GET['Week']."-".$_GET['Year']);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Download date : '.date('Y-m-d H:i:s'));
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		$this->excel->getActiveSheet()->mergeCells('A3:C3');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A3:B3')->getAlignment('Left')->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->setCellValue('A4', 'No');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Receipt');
		$this->excel->getActiveSheet()->setCellValue('C4', 'No Truck');				
		$this->excel->getActiveSheet()->setCellValue('D4', 'Driver Name');
		$this->excel->getActiveSheet()->setCellValue('E4', 'Request Del.Date');
		$this->excel->getActiveSheet()->setCellValue('F4', 'Shift');
		$this->excel->getActiveSheet()->setCellValue('G4', 'Time Slot');
		$this->excel->getActiveSheet()->setCellValue('H4', 'Actual Del.Date');
		$this->excel->getActiveSheet()->setCellValue('I4', 'Actual Shift');
		$this->excel->getActiveSheet()->setCellValue('J4', 'PO Number');
		$this->excel->getActiveSheet()->setCellValue('K4', 'Line');
		$this->excel->getActiveSheet()->setCellValue('L4', 'Material Code');
		$this->excel->getActiveSheet()->setCellValue('M4', 'Material Name');
		$this->excel->getActiveSheet()->setCellValue('N4', 'Vendor Code');
		$this->excel->getActiveSheet()->setCellValue('O4', 'Quantity');
		$this->excel->getActiveSheet()->setCellValue('P4', 'UOM');
		$this->excel->getActiveSheet()->setCellValue('Q4', 'UOM Plt');
		$this->excel->getActiveSheet()->setCellValue('R4', 'Plt Truck');
		$this->excel->getActiveSheet()->setCellValue('S4', 'Req.Pallet');
		$this->excel->getActiveSheet()->setCellValue('T4', 'Status');
		$this->excel->getActiveSheet()->setCellValue('U4', 'Arrived');
		$this->excel->getActiveSheet()->setCellValue('V4', 'Received');
		
		$styleArray = array(
      	'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
		          )
		      )
		 );

		$this->excel->getActiveSheet()->setCellValue('A3', $_GET['Year']." Week : ".$_GET['Week']);
		$get_date = $_GET['DateStart'];	
		$start_date = date('Y-m-d', strtotime($get_date));
		$end_date = date('Y-m-d',strtotime($get_date . "+6 days"));
		$getData = $this->db->query("SELECT a.requested_delivery_date, a.po_number, a.po_line_item, a.material_code, a.vendor_code, 						  				 a.qty, a.uom, a.uom_plt, a.plt_truck, a.req_pallet,
									a.shift as shift_rdd,
									 b.material_name,
									 c.vendor_alias,
									 d.id_schedule,
									 d.schedule_number,
									 d.quantity,
									 e.receipt,
									 e.id_truck,
									 e.driver_name,
									 e.arrive_time,
									 e.receive_time,
									 e.arrive_date,
									 e.receive_date,
									 f.rdd as rdd_actual,
									 g.shift as shift_actual,
									 f.status,
									 g.shift,
									 g.start_time, g.end_time
									 FROM tb_rds_detail as a
									 LEFT JOIN skin_master.ms_material as b  ON a.material_code = b.material_sku
									 LEFT JOIN skin_master.ms_supplier as c ON a.vendor_code = c.vendor_code
									 LEFT JOIN tb_scheduler as d ON a.id = d.id_schedule
									 LEFT JOIN tb_delivery_detail as e ON d.schedule_number = e.id_schedule_group
									 LEFT JOIN tb_schedule_detail as f ON d.schedule_number = f.schedule_number
									 
									 LEFT JOIN ms_time_slot as g ON f.id_time_slot = g.slot_number 
									 WHERE requested_delivery_date BETWEEN '$start_date' AND '$end_date'
									 OR f.rdd BETWEEN '$start_date' AND '$end_date'
		 							 ORDER BY a.requested_delivery_date ASC");
		
		foreach (range('A', $this->excel->getActiveSheet()->getHighestDataColumn()) as $col) {
	    $this->excel->getActiveSheet()
	             ->getColumnDimension($col)
	             ->setAutoSize(true);
	    }

	    $do_number = '';
	    $count_do_number = 0;
	    $no=1;
	    $checker = '';
	    $data_do_number = $getData->row()->schedule_number;
	    foreach ($getData->result() as $data) {
	    	if($data->status == '1'){
		 			$plan = 'Planned';
		 		}elseif($data->status == '2'){
		 			$plan = 'Arrived';
		 		}elseif($data->status == '3'){
		 			$plan = 'Received';
		 		}else{
		 			$plan = 'Not Planned';
		 	}

		 	// $get_do_number = $this->db->query("SELECT do_number from tb_delivery_invoice WHERE id_schedule = '$data->schedule_number' ");
		 	// $count_do_number = $get_do_number->num_rows();
		 	// $i = -1;
		 	// if($count_do_number >=1){
		 	// 	if($data_do_number == $checker){
		 	// 		$do_number = '';
		 	// 	}else{
		 	// 		foreach($get_do_number->result() as $get){
		 	// 			$do_number = $get->do_number;
		 	// 			$this->excel->getActiveSheet()->setCellValue('B'.(4+$no+$i), $do_number);
		 	// 			$i++;
		 	// 		}
		 	// 	}	
		 	// }else{
		 	// 	$do_number = '';
		 	// }		 	
		 	
	     	$this->excel->getActiveSheet()->setCellValue('A'.(4+$no), $no);
	     	$this->excel->getActiveSheet()->setCellValue('B'.(4+$no), $data->receipt);
			$this->excel->getActiveSheet()->setCellValue('C'.(4+$no), $data->id_truck);
			$this->excel->getActiveSheet()->setCellValue('D'.(4+$no), $data->driver_name);
			$rdd = '';
			if($data->requested_delivery_date != ''){
				$rdd = Date('d-m-Y',strtotime($data->requested_delivery_date));
			}			
	     	$this->excel->getActiveSheet()->setCellValue('E'.(4+$no), $rdd);
			$this->excel->getActiveSheet()->setCellValue('F'.(4+$no), $data->shift_rdd);
		 	$this->excel->getActiveSheet()->setCellValue('G'.(4+$no), $data->start_time." - ".$data->end_time);
		 	$actual = '';
		 	if($data->rdd_actual != ''){
		 		$actual = Date('d-m-Y',strtotime($data->rdd_actual));
		 	}
		 	$this->excel->getActiveSheet()->setCellValue('H'.(4+$no), $actual);
		 	$this->excel->getActiveSheet()->setCellValue('I'.(4+$no), $data->shift_actual);				
		 	$this->excel->getActiveSheet()->setCellValue('J'.(4+$no), $data->po_number);
		 	$this->excel->getActiveSheet()->setCellValue('K'.(4+$no), $data->po_line_item);
		 	$this->excel->getActiveSheet()->setCellValue('L'.(4+$no), $data->material_code);
		 	$this->excel->getActiveSheet()->setCellValue('M'.(4+$no), $data->material_name);
		 	$this->excel->getActiveSheet()->setCellValue('N'.(4+$no), $data->vendor_code);
			$quantity = '';
			if($data->quantity != 0){
				$quantity = $data->quantity * $data->uom_plt;
			}else{
				$quantity = $data->req_pallet * $data->uom_plt;
			}
		 	$this->excel->getActiveSheet()->setCellValue('O'.(4+$no), $quantity);
		 	$this->excel->getActiveSheet()->setCellValue('P'.(4+$no), $data->uom);
		 	$this->excel->getActiveSheet()->setCellValue('Q'.(4+$no), $data->uom_plt);
		 	$this->excel->getActiveSheet()->setCellValue('R'.(4+$no), $data->plt_truck);
			if($data->quantity != 0){
				$quantity = $data->quantity;
			}else{
				$quantity = $data->req_pallet;
			}
		 	$this->excel->getActiveSheet()->setCellValue('S'.(4+$no), $quantity);
		 	$this->excel->getActiveSheet()->setCellValue('T'.(4+$no), $plan);
			$this->excel->getActiveSheet()->setCellValue('U'.(4+$no), $plan);
			$this->excel->getActiveSheet()->setCellValue('V'.(4+$no), $plan);
			$plank = ' ';
			$arrive = '';
		 	if($data->arrive_date != ''){
		 		$plank =" | ";
		 		$arrive = Date('d-m-Y',strtotime($data->arrive_date));
		 	}
		 	$this->excel->getActiveSheet()->setCellValue('U'.(4+$no), $arrive.$plank.$data->arrive_time);
		 	$plank = ' ';
		 	$receive = '';
		 	if($data->receive_date != ''){
		 		$plank =" | ";
		 		$receive = Date('d-m-Y',strtotime($data->receive_date));
		 	}
		 	$this->excel->getActiveSheet()->setCellValue('V'.(4+$no), $receive.$plank.$data->receive_time);
			
			$checker = $data->schedule_number;
		 	$no++;
	     }

	     //BORDER
		$this->excel->getActiveSheet()->getStyle('A4:V'.(3+$no))->applyFromArray($styleArray);
		//
		
		$this->excel->getActiveSheet()->setCellValue('B'.(5+$no), 'Receipt');
		$this->excel->getActiveSheet()->setCellValue('C'.(5+$no), 'DO Number');
		$no++;
		$do_awal = $no;
		$total_do = 0;
		 $get_data_do = $this->db->query("SELECT a.do_number, b.receipt FROM tb_delivery_invoice as a LEFT JOIN tb_delivery_detail as b ON b.id_schedule_group = a.id_schedule");
	     if($get_data_do->num_rows() >= 1){
	     	foreach ($get_data_do->result() as $get) {
	     		$this->excel->getActiveSheet()->setCellValue('B'.(5+$no), $get->receipt);
	     	$this->excel->getActiveSheet()->setCellValue('C'.(5+$no), $get->do_number);
	     	$no++;
	     	$total_do++;
	     	}	
	     }

	     $this->excel->getActiveSheet()->getStyle('B'.(4+$do_awal).':C'.(4+$no))->applyFromArray($styleArray);

		$filename='Schedule_Y'.$_GET['Year'].'_W'.$_GET['Week'].'_T'.date('H:i:s').'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	public function receipt_pdf(){
		$time_slot = $this->db->query("SELECT start_time, end_time 
									   FROM ms_time_slot "
									);

		$sn = $this->input->get('Schedule');
		$del = $this->db->query("SELECT a.*,d.shift,d.start_time,d.end_time, a.receipt FROM tb_delivery_detail as a
								 INNER JOIN tb_scheduler as b ON a.id_schedule_group = b.schedule_number
								 INNER JOIN tb_schedule_detail as c ON c.schedule_number = a.id_schedule_group
								 INNER JOIN ms_time_slot as d ON c.id_time_slot = d.slot_number
								 
							     WHERE id_schedule_group ='$sn' ")->row();

		$date = substr($del->delivery_date,0,10);
		$data_supplier = explode('_',$del->invoice);
		$do_number = explode('.',$data_supplier[4]);
		$supplier = $data_supplier[3];
		$shift = '';
		if($del->shift == 'DP'){
			$shift = 'Dinas Pagi';
		}elseif($del->shift == 'DS'){
			$shift = 'Dinas Siang';
		}else{
			$shift = 'Dinas Malam';
		}

		if(!file_exists('assets/dist/img/'.$del->receipt.".png")){
			$this->load->library('infiQr');
			QRcode::png($del->receipt, 'assets/dist/img/'.$del->receipt.".png");
		}

		$fpdf = new FPDF('P', 'mm', 'A4');
		$fpdf->AddFont('Helvetica');
		$fpdf->SetFillColor(200, 200, 200);
		$fpdf->AddPage();
		$fpdf->SetMargins(5, 1, 5);
		/* TITLE */

		$fpdf->SetFont('Helvetica', 'B', '15');
		$fpdf->SetX(5);
		$fpdf->Cell(0, 0, "Delivery Receipt : ".$del->receipt, 0, 1, "R");
		$fpdf->SetX(5);
		$fpdf->SetFont('Helvetica', '', '12');
		$fpdf->Cell(0, 0, "Date : $date", 0, 1, "L");
		$fpdf->Image(base_url('assets/dist/img/'.$del->receipt.'.png'),170,20,35,35,'PNG');
		//logo
		$fpdf->Image(base_url('assets/dist/img/unilever_logo.png'), 5, 23, 28, 27);
		$fpdf->SetX(5);
		//$fpdf->Image(base_url()."assets/dist/img/unilever_logo.PNG", 10, 25, 23, 23, 'PNG');
		
		$fpdf->Ln(14);
		$fpdf->SetY(23);
		$fpdf->SetX(122);
		$fpdf->SetFont('Helvetica', '', '8');
		$fpdf->Cell(20, 6, "ID Truck", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->id_truck", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Vendor", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $supplier", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Driver Name ", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->driver_name", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Phone Number", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->phone_number", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Shift", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $shift", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Time Slot", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->start_time - $del->end_time", 0, 1, "L");
		$fpdf->SetFont('Helvetica', '', '9');
		$fpdf->Ln(5);
		
		$fpdf->SetFont('Helvetica', '', '8');
		
		$fpdf->SetY(23);
		$fpdf->SetX(35);
		$fpdf->MultiCell(60, 5, "PT. Unilever \nJl. Jababeka v Blok U No.14-16, Karangbaru, Cikarang Utara, Bekasi,\nJawa Barat 17530\nIndonesia", 0, 'L', 0);
		$fpdf->SetY(32);
		$fpdf->SetFont('Helvetica', 'B', '10');
		$fpdf->SetFont('Helvetica', 'B', '10');

		$fpdf->Ln(10);

		/* TABLE */
		$no = 1;
		$fpdf->SetY(65);
		$fpdf->SetFont('Helvetica', '', '7');
		$data_po = $this->db->query("SELECT b.po_number,b.material_code,c.material_name,b.qty,b.uom,b.uom_plt,b.req_pallet FROM tb_scheduler as a 
									 INNER JOIN tb_rds_detail as b ON a.id_schedule = b.id
									 LEFT JOIN skin_master.ms_material as c ON c.material_sku = b.material_code
									 
									 WHERE a.schedule_number = '$del->id_schedule_group' "); 
		$fpdf->Cell(10, 12, "NO", 1, 0, "C", 0);
		$fpdf->Cell(30, 12, "PO Number", 1, 0, "C", 0);
		$fpdf->Cell(30, 12, "Material Code", 1, 0, "C", 0);
		$fpdf->Cell(70, 12, "Material Name", 1, 0, "C", 0);
		$fpdf->Cell(15, 12, "Uom", 1, 0, "C", 0);
		$fpdf->Cell(25, 12, "Qty", 1, 0, "C", 0);
		$fpdf->Cell(20, 12, "Req. Pallet", 1, 1, "C", 0);
		$total_qty = 0;
		$total_rpallet = 0;
		foreach ($data_po->result() as $data) {
			$fpdf->Cell(10, 12, $no, 1, 0, "C", 0);
			$fpdf->Cell(30, 12, ' '.$data->po_number, 1, 0, "L", 0);
			$fpdf->Cell(30, 12, ' '.$data->material_code, 1, 0, "L", 0);
			$fpdf->Cell(70, 12, ' '.$data->material_name, 1, 0, "L", 0);			
			$fpdf->Cell(15, 12, $data->uom, 1, 0, "C", 0);
			$fpdf->Cell(25, 12, $data->qty, 1, 0, "C", 0);
			$fpdf->Cell(20, 12, $data->req_pallet, 1, 1, "C", 0);
			$total_qty = $total_qty + $data->qty;
			$total_rpallet = $total_rpallet + $data->req_pallet;
			$no++;
		}
		$fpdf->Ln(5);
		$fpdf->SetFont('Helvetica', 'B', '10');
		$fpdf->Cell(20, 6, "DO NUMBER : ", 0, 0, "L");
		$fpdf->SetFont('Helvetica', 'U', '8');
		$data_do = $this->db->query("SELECT do_number FROM tb_delivery_invoice WHERE id_schedule = '$del->id_schedule_group' ");
		$fpdf->Ln(5);
		$a = 1;
		foreach($data_do->result() as $get){
			if(($a % 5) == 0){
				$fpdf->Cell(38, 12, $get->do_number, 0, 1, "L", 0);
			}else{
				$fpdf->Cell(38, 12, $get->do_number, 0, 0, "L", 0);
			}
			$a++;
		}

		$fpdf->Ln(15);
		$fpdf->SetFont('Helvetica', 'B', '10');
		$fpdf->MultiCell(185, 5, " Signature ", 0, 'R', 0);
		$fpdf->Ln(15);
		$fpdf->MultiCell(195, 5, "___________________", 0, 'R', 0);
		$fpdf->Output();
	}

	public function getScheduleX($date, $vendor_code){

		$data = $this->db->query("SELECT
							tb_rds_detail.id,
							material_code,
							tb_rds_detail.vendor_code,
							qty,
							requested_delivery_date,
							req_pallet,
							IF(@rownum + req_pallet <= 32, @rownum := @rownum + req_pallet, @rownum := 0 + req_pallet) as row_number
						FROM
							tb_rds_detail
						CROSS JOIN (SELECT @rownum := 0) r
						WHERE
							tb_rds_detail.vendor_code = '$vendor_code'
						AND requested_delivery_date = '$date'");
		return $data;
	}

	public function getTimeSlot(){
		$data = $this->db->query("SELECT * FROM ms_time_slot ORDER BY ordering ASC");		

		return $data;
	}

	public function getScheduleNumber(){
		$data = $this->db->query("SELECT schedule_number FROM tb_schedule_detail ORDER BY schedule_number DESC LIMIT 0,1")->row();
		$sn = (!isset($data->schedule_number))?0:$data->schedule_number;
		return $sn+1;
	}

	public function getScheduleNew($date, $vendor_code, $user){
		if($user == '3'){
			$data = $this->db->query("SELECT
								a.*, b.vendor_name,
								b.vendor_alias,
								e.shift,
								(SELECT COUNT(*) FROM tb_scheduler WHERE schedule_number=a.schedule_number) as TOTAL_MATERIAL
							FROM
								tb_schedule_detail AS a
							INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
							LEFT JOIN tb_scheduler d ON a.schedule_number = d.schedule_number
							LEFT JOIN tb_rds_detail c ON d.id_schedule = c.id
							INNER JOIN ms_time_slot e ON a.id_time_slot = e.id
							WHERE
								a.vendor_code = '$vendor_code'
							AND a.rdd = '$date'
							GROUP BY
								a.schedule_number");

		}else{

			$data = $this->db->query("SELECT
								a.*, b.vendor_name,
								b.vendor_alias,
								e.shift,
								(SELECT COUNT(*) FROM tb_scheduler WHERE schedule_number=a.schedule_number) as TOTAL_MATERIAL
							FROM
								tb_schedule_detail AS a
							INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
							LEFT JOIN tb_scheduler d ON a.schedule_number = d.schedule_number
							LEFT JOIN tb_rds_detail c ON d.id_schedule = c.id
							INNER JOIN ms_time_slot e ON a.id_time_slot = e.id
							WHERE
								a.rdd = '$date'
							GROUP BY
								a.schedule_number");
		}
		return $data;
	}

	public function checkSlotAva($id_slot, $date){
		$cek = $this->db->query("SELECT COUNT(*) as COUNT FROM tb_schedule_detail WHERE id_time_slot='$id_slot' AND rdd='$date'")->row();
		return $cek->COUNT;
	}

	public function getScheduleDetail($id){
		if($this->session->userdata('sess_role_no')== '3'){
		$data = $this->db->query("SELECT
							c.id,
							c.id_schedule,
							a.po_number,
							c.quantity,
							c.schedule_number,
							c.vendor_code,
							d.vendor_name,
							a.material_code,
							b.material_name,
							a.id as idz,
							a.qty,
							a.uom,
							a.uom_plt,
							a.req_pallet,
							a.requested_delivery_date,
							a.shift,
							(c.quantity*a.uom_plt) as SEND_AMOUNT
						FROM
							tb_scheduler c
						INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
						LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
						INNER JOIN skin_master.ms_supplier d ON c.vendor_code = d.vendor_code
						WHERE
							schedule_number='$id'
						ORDER BY a.po_number ASC");
		}else{
			$data = $this->db->query("SELECT
							c.id,
							c.id_schedule,
							a.po_number,
							c.quantity,
							c.schedule_number,
							a.material_code,
							b.material_name,
							a.id as idz,
							a.requested_delivery_date,
							a.shift,
							a.qty,
							a.uom,
							a.uom_plt,
							a.req_pallet,
							(c.quantity*a.uom_plt) as SEND_AMOUNT
						FROM
							tb_scheduler c
						INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
						LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
						WHERE
							schedule_number='$id'
						ORDER BY a.po_number ASC");
		}
		$get_arr = $this->db->query("SELECT id_schedule FROM tb_scheduler WHERE schedule_number='$id'");
		return array(
			"data_list" => $data,
			"array_id" => $get_arr
		);
	}

	public function getDeliveryInfo($id){
		
		$data = $this->db->query("SELECT
									e.*,
									g.status as STATE,
									concat(
										f.start_time,
										' - ',
										f.end_time
									) AS time_slot
								FROM
									tb_scheduler c
								LEFT JOIN tb_delivery_detail e ON c.schedule_number = e.id_schedule_group
								LEFT JOIN tb_schedule_detail g ON e.id_schedule_group = g.schedule_number
								LEFT JOIN ms_time_slot f ON e.id_time_slot = f.slot_number
								LEFT JOIN tb_delivery_invoice z ON e.id_schedule_group = z.id_schedule
								WHERE
									g.schedule_number = '$id'
								LIMIT 0, 1");
		return $data;

	}

	public function getdnDownload($id){
		
		$data = $this->db->query("SELECT
									b.*
								FROM
									tb_delivery_detail as a
								INNER JOIN tb_delivery_invoice as b ON b.id_schedule = a.id_schedule_group
								WHERE a.id_schedule_group = '$id'
								");
		return $data;

	}

	public function moveSchedule($id){
		$this->db->query("INSERT INTO tb_schedule_pool SELECT '' as ids,id_schedule, vendor_code, schedule_number, rdd, status FROM tb_scheduler where id='$id' ");
		$this->db->query("DELETE FROM tb_scheduler WHERE id='$id'");

		
	}

	public function getTimeSlotById($id){
		$data = $this->db->query("SELECT * FROM ms_time_slot WHERE slot_number=$id")->row();
		$st = date_create($data->start_time);
		$st = date_format($st, "H:i");
		$et = date_create($data->end_time);
		$et = date_format($et, "H:i");
		echo $st." - ".$et;
	}

	public function getSuggestion($find, $sn, $rdd){
		$data = $this->db->query("SELECT
									c.id,
									c.id_schedule,
									a.po_number,
									c.schedule_number,
									c.vendor_code,
									d.vendor_name,
									a.material_code,
									b.material_name,
									a.req_pallet,
									a.uom_plt,
									a.qty,
									a.shift,
									a.requested_delivery_date
									-- c.quantity as req_pallet
								FROM
									tb_scheduler c
								INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
								LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
								INNER JOIN skin_master.ms_supplier d ON c.vendor_code = d.vendor_code
								WHERE
									schedule_number !='$sn'
								AND
									c.rdd = '$rdd'
								AND
									a.status = '0'
								AND
									a.req_pallet <= $find
								ORDER BY a.po_number ASC");
		return $data;
	}

	public function material_pool($role, $rdd){

		if($role == '3'){
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$getData = $this->db->query("SELECT
											*
										FROM
											(
												SELECT
													trd.*, mm.material_name,
													(
														trd.req_pallet - SUM(tb.quantity)
													) AS sisa
												FROM
													tb_rds_detail AS trd
												INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
												LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
												WHERE
													trd.vendor_code = '$vendor_code'
												AND trd.is_schedule = '1'
												GROUP BY
													trd.id
											) AS TB_1
										WHERE sisa > 0 OR sisa IS NULL");
		}else{
			$getData = $this->db->query("SELECT trd.*, mm.material_name,tsp.id_schedule, tsp.schedule_number FROM tb_schedule_pool as tsp INNER JOIN tb_rds_detail as trd ON  tsp.id_schedule = trd.id INNER JOIN skin_master.ms_material as mm ON mm.material_sku= trd.material_code");
		}

		return $getData;
	}

	public function getDateLess($rdd, $sn){
		$getData = "";
		if($sn != ""){
			$getData = $this->db->query("SELECT
										trd.*, mm.material_name, null as sisa
									FROM
										tb_rds_detail AS trd
									INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
									LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
									WHERE trd.requested_delivery_date < '$rdd' AND trd.status='0' AND tb.schedule_number != $sn");
		}
		return $getData;
	}

	public function getTimeSlot2(){
        $data = $this->db->query("SELECT * FROM ms_time_slot ");        
        return $data;
    }

    public function setStatus($stat, $id){
    	$data = $this->db->query("UPDATE tb_schedule_detail SET status='?' WHERE id='?'", [$stat, $id]);
    	return $data;
    }

}

?>