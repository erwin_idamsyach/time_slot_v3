<?php

class Schedule_model extends CI_Model{
	

	public function getSchedule($date, $vendor_code, $user, $shift){
	if($user == '3'){
		$data = $this->db->query("SELECT
							tb_rds_detail.id,
							tb_rds_detail.shift,
							material_code,
							tb_rds_detail.vendor_code,
							shift,
							qty,
							requested_delivery_date,
							req_pallet,
							IF(@rownum + req_pallet <= 32, @rownum := @rownum + req_pallet, @rownum := 0 + req_pallet) as row_number
						FROM
							tb_rds_detail
						LEFT JOIN tb_scheduler ON tb_rds_detail.id = tb_scheduler.id_schedule
						CROSS JOIN (SELECT @rownum := 0) r
						WHERE
							tb_rds_detail.vendor_code = '$vendor_code'
							AND
							tb_rds_detail.shift='$shift'
						AND 
						tb_scheduler.id_schedule IS NULL
						AND requested_delivery_date = '$date'
						AND tb_rds_detail.is_schedule = '0'");
	}else{
		$data = $this->db->query("SELECT
							tb_rds_detail.id,
							tb_rds_detail.shift,
							material_code,
							tb_rds_detail.vendor_code,
							qty,
							requested_delivery_date,
							req_pallet,
							IF(@rownum + req_pallet <= 32, @rownum := @rownum + req_pallet, @rownum := 0 + req_pallet) as row_number
						FROM
							tb_rds_detail
						LEFT JOIN tb_scheduler ON tb_rds_detail.id = tb_scheduler.id_schedule
						LEFT JOIN tb_schedule_pool ON tb_rds_detail.id = tb_schedule_pool.id_schedule
						CROSS JOIN (SELECT @rownum := 0) r
						WHERE 
						tb_scheduler.id_schedule IS NULL
						AND tb_schedule_pool.id_schedule IS NULL
						AND requested_delivery_date = '$date'
						AND tb_rds_detail.status = '0'");
	}
		return $data;
	}

	public function Download_Excel($vc = ''){
		$history = array(
				"date"			=> date('Y-m-d'),
				"time"			=> date('H:i:s'),
				"action"		=> "Download",
				"by_who"		=> $this->session->userdata('sess_id'),
				"table_join" 	=> "tb_schedule_detail",
				"id_join"		=> '',
				"description"	=> " Schedule Week : ".$_GET['Week'],
				"value"			=> '',
				"value2"		=> '',
				"vendor_code"	=> $this->session->userdata('sess_vendor_code')
		);

		$this->db->insert('tb_history',$history);

		date_default_timezone_set("Asia/Bangkok");
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle("Week ".$_GET['Week']."-".$_GET['Year']);
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Download date : '.date('Y-m-d H:i:s'));
		$this->excel->getActiveSheet()->setCellValue('B2', $this->session->userdata('sess_nama') );
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		$this->excel->getActiveSheet()->mergeCells('A3:C3');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A3:B3')->getAlignment('Left')->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->setCellValue('A4', 'No');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Receipt');
		$this->excel->getActiveSheet()->setCellValue('C4', 'No Truck');				
		$this->excel->getActiveSheet()->setCellValue('D4', 'Driver Name');
		$this->excel->getActiveSheet()->setCellValue('E4', 'Request Del.Date');
		$this->excel->getActiveSheet()->setCellValue('F4', 'Shift');
		$this->excel->getActiveSheet()->setCellValue('G4', 'Time Slot');
		$this->excel->getActiveSheet()->setCellValue('H4', 'Actual Del.Date');
		$this->excel->getActiveSheet()->setCellValue('I4', 'Actual Shift');
		$this->excel->getActiveSheet()->setCellValue('K4', 'Category');
		$this->excel->getActiveSheet()->setCellValue('L4', 'PO Number');
		$this->excel->getActiveSheet()->setCellValue('M4', 'Line');
		$this->excel->getActiveSheet()->setCellValue('N4', 'Material Code');
		$this->excel->getActiveSheet()->setCellValue('O4', 'Material Name');
		$this->excel->getActiveSheet()->setCellValue('P4', 'Vendor Code');
		$this->excel->getActiveSheet()->setCellValue('Q4', 'Quantity');
		$this->excel->getActiveSheet()->setCellValue('R4', 'UOM');
		$this->excel->getActiveSheet()->setCellValue('S4', 'UOM Plt');
		$this->excel->getActiveSheet()->setCellValue('T4', 'Plt Truck');
		$this->excel->getActiveSheet()->setCellValue('U4', 'Req.Pallet');
		$this->excel->getActiveSheet()->setCellValue('V4', 'Status');
		$this->excel->getActiveSheet()->setCellValue('W4', 'Arrived Date Time');
		$this->excel->getActiveSheet()->setCellValue('X4', 'Arrived By');
		$this->excel->getActiveSheet()->setCellValue('Y4', 'Received Date Time');
		$this->excel->getActiveSheet()->setCellValue('Z4', 'Received By');
		$this->excel->getActiveSheet()->setCellValue('AA4', 'Reason');
		$this->excel->getActiveSheet()->setCellValue('AB4', 'Received Uom plt');
		$this->excel->getActiveSheet()->setCellValue('AC4', 'Received Amount *pallet');
		$this->excel->getActiveSheet()->setCellValue('AD4', 'Plan Created By');
		$this->excel->getActiveSheet()->setCellValue('AE4', 'Plan Created Time');
		$this->excel->getActiveSheet()->setCellValue('AF4', 'Last Edit By');
		$this->excel->getActiveSheet()->setCellValue('AG4', 'Plan Edit Time');

		$styleArray = array(
      	'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
		          )
		      )
		 );

		$styleDate = array(
      	'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_NumberFormat::FORMAT_DATE_XLSX14
		          )
		      )
		 );

		$this->excel->getActiveSheet()->setCellValue('A3', $_GET['Year']." Week : ".$_GET['Week']);
		$get_date = $_GET['DateStart'];	
		$start_date = date('Y-m-d', strtotime($get_date));
		$end_date = date('Y-m-d',strtotime($get_date . "+6 days"));
		
		$data1 = $this->db->query("SELECT a.requested_delivery_date,
	        									 a.shift as shift_rdd,
	        									 a.category,
	        									 a.po_number,
	        									 a.po_line_item,
	        									 a.material_code,
	        									 a.vendor_code,
	        									 a.uom,
	        									 a.uom_plt,
	        									 a.plt_truck,
	        									 a.req_pallet,
	        									 a.created_by,
	        									 b.material_name,
	        									 (a.req_pallet - c.quantity) as sisa,
	        									 c.quantity,
	        									 c.receive_amount,
	        									 d.status,
	        									 d.rdd as rdd_actual,
	        									 e.receipt,
	        									 e.id_truck,
	        									 e.driver_name,
	        									 f.start_time,
	        									 f.end_time,
	        									 f.shift as shift_actual,
	        									 e.arrive_date,
	        									 e.arrive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.arrived_by ) AS arrived_by,
	        									 e.receive_date,
	        									 e.receive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.received_by ) AS received_by,
	        									 e.reason,
	        									 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS nama,
	        									 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name,
	        									 (SELECT created_at FROM ms_user WHERE id = a.created_by) AS created_at,
	        									 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name,
	        									 (SELECT created_at FROM ms_user WHERE id = a.update_by) AS update_at
	        									 FROM tb_rds_detail AS a 
	        									 LEFT JOIN skin_master.ms_material AS b ON a.material_code = b.material_sku
	        									 LEFT JOIN tb_scheduler AS c ON a.id = c.id_schedule
	        									 LEFT JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number
	        									 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = d.schedule_number
	        									 LEFT JOIN ms_time_slot AS f ON d.id_time_slot = f.id
	        									 WHERE
	        									 
		 										 a.vendor_code LIKE '%$vc%' AND
		 										 
		 										 (
		 										 a.requested_delivery_date 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 OR d.rdd 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 )
		 			 							 GROUP BY c.id
		 			 							 ORDER BY a.requested_delivery_date ASC

	        									 ")->result();


	        	$data2 = $this->db->query("SELECT a.requested_delivery_date,
	        									 a.shift as shift_rdd,
	        									 a.category,
	        									 a.po_number,
	        									 a.po_line_item,
	        									 a.material_code,
	        									 a.vendor_code,
	        									 a.uom,
	        									 a.uom_plt,
	        									 a.plt_truck,
	        									 a.req_pallet,
	        									 a.created_by,
	        									 b.material_name,
	        									 (a.req_pallet - c.quantity) as sisa,
	        									 (a.req_pallet - SUM(c.quantity) ) AS quantity,
	        									 c.receive_amount,
	        									 0 AS status,
	        									 '' AS rdd_actual,
	        									 '' AS receipt,
	        									 '' AS id_truck,
	        									 '' AS driver_name,
	        									 f.start_time,
	        									 f.end_time,
	        									 '' as shift_actual,
	        									 e.arrive_date,
	        									 e.arrive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.arrived_by ) AS arrived_by,
	        									 e.receive_date,
	        									 e.receive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.received_by ) AS received_by,
	        									 e.reason,
	        									 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS nama,
	        									 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name,
	        									 (SELECT created_at FROM ms_user WHERE id = a.created_by) AS created_at,
	        									 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name,
	        									 (SELECT created_at FROM ms_user WHERE id = a.update_by) AS update_at
	        									 FROM tb_rds_detail AS a 
	        									 INNER JOIN skin_master.ms_material AS b ON a.material_code = b.material_sku
	        									 LEFT JOIN tb_scheduler AS c ON c.id_schedule = a.id
	        									 INNER JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number
	        									 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = d.schedule_number
	        									 LEFT JOIN ms_time_slot AS f ON d.id_time_slot = f.id
	        									  WHERE	        									 
		 										 a.vendor_code LIKE '%$vc%' AND
		 										 
		 										 (
		 										 a.requested_delivery_date 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 OR d.rdd 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 )		 			 							 
	        									 GROUP BY a.id HAVING quantity > 0
	        									 ")->result();
	        		
	        		$getData = array_merge($data1, $data2);
		
		foreach (range('A', $this->excel->getActiveSheet()->getHighestDataColumn()) as $col) {
	    $this->excel->getActiveSheet()
	             ->getColumnDimension($col)
	             ->setAutoSize(true);
	    }

	    $do_number = '';
	    $count_do_number = 0;
	    $no=1;
	    $checker = '';
	    //$data_do_number = $getData->row()->schedule_number;
	    foreach ($getData as $data) {
	    	if($data->status == '1'){
		 			$plan = 'Planned';
		 		}elseif($data->status == '2'){
		 			$plan = 'Arrived';
		 		}elseif($data->status == '3'){
		 			$plan = 'Received';
		 		}else{
		 			$plan = 'Not Planned';
		 	}


		     	$this->excel->getActiveSheet()->setCellValue('A'.(4+$no), $no);
		     	$this->excel->getActiveSheet()->setCellValue('B'.(4+$no), $data->receipt);
				$this->excel->getActiveSheet()->setCellValue('C'.(4+$no), $data->id_truck);
				$this->excel->getActiveSheet()->setCellValue('D'.(4+$no), $data->driver_name);

				$rdd = '';
				if($data->requested_delivery_date != ''){
					$rdd = Date('d-m-Y',strtotime($data->requested_delivery_date));
				}			
				
				$date_rdd = date('d/m/Y', strtotime($rdd));
				$rdd = PHPExcel_Shared_Date::PHPToExcel($date_rdd);
				
		     	$this->excel->getActiveSheet()->setCellValue('E'.(4+$no),  $rdd);
		     	$this->excel->getActiveSheet()->getStyle('E'.(4+$no))->getNumberFormat()->setFormatCode('dd/mm/yyyy');
				$this->excel->getActiveSheet()->setCellValue('F'.(4+$no), $data->shift_rdd);
			 	$this->excel->getActiveSheet()->setCellValue('G'.(4+$no), $data->start_time." - ".$data->end_time);
			 	$actual = '';
			 	if($data->rdd_actual != ''){
			 		$get_actual = Date('d-m-Y',strtotime($data->rdd_actual));
			 		$actual = PHPExcel_Shared_DAte::PHPTOExcel($get_actual);
			 	}
			 	$this->excel->getActiveSheet()->setCellValue('H'.(4+$no), $actual);
			 		$this->excel->getActiveSheet()->getStyle('H'.(4+$no))->getNumberFormat()->setFormatCode('dd/mm/yyyy');

			 	$this->excel->getActiveSheet()->setCellValue('I'.(4+$no), $data->shift_actual);
			 	$this->excel->getActiveSheet()->setCellValue('J'.(4+$no), $data->nama);	
			 	$this->excel->getActiveSheet()->setCellValue('K'.(4+$no), $data->category);			
			 	$this->excel->getActiveSheet()->setCellValue('L'.(4+$no), $data->po_number);
			 	$this->excel->getActiveSheet()->setCellValue('M'.(4+$no), $data->po_line_item);
			 	$this->excel->getActiveSheet()->setCellValue('N'.(4+$no), $data->material_code);
			 	$this->excel->getActiveSheet()->setCellValue('O'.(4+$no), $data->material_name);
			 	$this->excel->getActiveSheet()->setCellValue('P'.(4+$no), $data->vendor_code);
				$quantity = 0;
			 	if($data->sisa > 0){
			 		$quantity = $data->quantity * $data->uom_plt;
			 	}else{
			 		$quantity = $data->req_pallet * $data->uom_plt;
			 	}						
			 	$this->excel->getActiveSheet()->setCellValue('Q'.(4+$no), $quantity);
			 	$this->excel->getActiveSheet()->setCellValue('R'.(4+$no), $data->uom);
			 	$this->excel->getActiveSheet()->setCellValue('S'.(4+$no), $data->uom_plt);
			 	$this->excel->getActiveSheet()->setCellValue('T'.(4+$no), $data->plt_truck);


			 	$this->excel->getActiveSheet()->setCellValue('U'.(4+$no), $data->quantity);
			 	$this->excel->getActiveSheet()->setCellValue('V'.(4+$no), $plan);
								
				$arrive = '';
			 	if($data->arrive_date != ''){
			 	$get_arrive = Date('d/m/Y H:i:s',strtotime($data->arrive_date.' '.$data->arrive_time));
				$arrive = PHPExcel_Shared_Date::PHPToExcel($get_arrive);				 	
			 	}

			 	$this->excel->getActiveSheet()->setCellValue('W'.(4+$no), $arrive);
			 	$this->excel->getActiveSheet()->getStyle('W'.(4+$no))->getNumberFormat()->setFormatCode('dd/mm/yyyy hh:mm:ss');	
			 	$this->excel->getActiveSheet()->setCellValue('X'.(4+$no), $data->arrived_by);
			 	
			 	$receive = '';
			 	
			 	if($data->receive_date != ''){
			 		
			 		$get_receive = Date('d/m/Y H:i:s',strtotime($data->receive_date.' '.$data->receive_time));
			 		$receive = PHPExcel_Shared_Date::PHPToExcel($get_receive);
			 	}
			 	$this->excel->getActiveSheet()->setCellValue('Y'.(4+$no), $receive);
			 	$this->excel->getActiveSheet()->getStyle('Y'.(4+$no))->getNumberFormat()->setFormatCode('dd/mm/yyyy hh:mm:ss');
				$this->excel->getActiveSheet()->setCellValue('Z'.(4+$no), $data->received_by);
				$this->excel->getActiveSheet()->setCellValue('AA'.(4+$no), $data->reason);
				$this->excel->getActiveSheet()->setCellValue('AB'.(4+$no), $data->receive_amount);
				$this->excel->getActiveSheet()->setCellValue('AC'.(4+$no), $data->receive_amount * $data->uom_plt);
				$this->excel->getActiveSheet()->setCellValue('AD'.(4+$no), $data->created_name);
				
				$get_created_at = Date('d/m/Y H:i:s',strtotime($data->created_at));
			 	$created_at = PHPExcel_Shared_Date::PHPToExcel($get_created_at);
				
				$this->excel->getActiveSheet()->setCellValue('AE'.(4+$no), $created_at);
				$this->excel->getActiveSheet()->getStyle('AE'.(4+$no))->getNumberFormat()->setFormatCode('dd/mm/yyyy hh:mm:ss');

				$this->excel->getActiveSheet()->setCellValue('AF'.(4+$no), $data->edit_name);
				$update_at = '';
				if($data->update_at != '0000-00-00 00:00:00'){
					$get_update_at = Date('d/m/Y H:i:s',strtotime($data->update_at));
			 		$update_at = PHPExcel_Shared_Date::PHPToExcel($get_update_at);
				}
				
				$this->excel->getActiveSheet()->setCellValue('AG'.(4+$no), $update_at);
				$this->excel->getActiveSheet()->getStyle('AG'.(4+$no))->getNumberFormat()->setFormatCode('dd/mm/yyyy hh:mm:ss');
				
			 	$no++;
		     }

		    
		


		foreach(range('B'.(3+$no),'Z'.(3+$no)) as $columnID) {
		    $this->excel->getActiveSheet()->getColumnDimension($columnID)
		        ->setAutoSize(true);
		}

		foreach(range('AA'.(3+$no),'AF'.(3+$no)) as $columnID) {
		    $this->excel->getActiveSheet()->getColumnDimension($columnID)
		        ->setAutoSize(true);
		}
		

	     //BORDER
		$this->excel->getActiveSheet()->getStyle('A4:AG'.(3+$no))->applyFromArray($styleArray);
		//
		
		$this->excel->getActiveSheet()->setCellValue('B'.(5+$no), 'Receipt');
		$this->excel->getActiveSheet()->setCellValue('C'.(5+$no), 'DO Number');
		$no++;
		$do_awal = $no;
		$total_do = 0;
	
			 $get_data_do = $this->db->query("SELECT a.do_number, b.receipt FROM tb_delivery_invoice as a LEFT JOIN tb_delivery_detail as b ON b.id_schedule_group = a.id_schedule WHERE DATE(b.delivery_date) BETWEEN '$start_date' AND '$end_date'");
		     if($get_data_do->num_rows() >= 1){
		     	foreach ($get_data_do->result() as $get) {
		     		$this->excel->getActiveSheet()->setCellValue('B'.(5+$no), $get->receipt);
		     	$this->excel->getActiveSheet()->setCellValue('C'.(5+$no), $get->do_number);
		     	$no++;
		     	$total_do++;
		     	}	
		     }
		     $this->excel->getActiveSheet()->getStyle('B'.(4+$do_awal).':C'.(4+$no))->applyFromArray($styleArray);
	     
	     

		$filename='Schedule_Y'.$_GET['Year'].'_W'.$_GET['Week'].'_T'.date('H:i:s').'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	public function material_movement($year, $week){
		$dto = new DateTime();
		$dto->setISODate($year, $week);
		$start_date = $dto->format('Y-m-d');
		$dto->modify('+6 days');
		$end_date = $dto->format('Y-m-d');
		$filePath = "./assets/templates/template_material_movement.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $sheetNames = $objPHPExcel->getSheetNames();
        $sheetNo = 0;
        $number = 0;
		foreach ($sheetNames as $sheetNameIndex => $sheetName)
	    {
	        $sheet =   $objPHPExcel->setActiveSheetIndexByName($sheetName); //sets and returns active
	        $no = 0;
	        if($sheetNo == 0){
	        	//header
	        	$sheet->setCellValue('C'.(2), ": ".date('d-m-Y H:i:s'));
	        	$sheet->setCellValue('C'.(3), ": ".$this->session->userdata('sess_nama'));
	        	$sheet->setCellValue('C'.(4), ": ".$year);
	        	$sheet->setCellValue('C'.(5), ": ".$week);
	        	//

	        	//table material_movement

	        	$data1 = $this->db->query("SELECT a.requested_delivery_date,
	        									 a.shift,
	        									 a.category,
	        									 a.po_number,
	        									 a.po_line_item,
	        									 a.material_code,
	        									 a.vendor_code,
	        									 a.uom,
	        									 a.uom_plt,
	        									 a.plt_truck,
	        									 a.req_pallet,
	        									 
	        									 b.material_name,
	        									 (a.req_pallet - c.quantity) as sisa,
	        									 c.quantity,
	        									 c.receive_amount,
	        									 d.status,
	        									 d.rdd as rdd,
	        									 e.receipt,
	        									 e.id_truck,
	        									 e.driver_name,
	        									 f.start_time,
	        									 f.end_time,
	        									 f.shift as actual_shift,
	        									 e.arrive_date,
	        									 e.arrive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.arrived_by ) AS arrived_by,
	        									 e.receive_date,
	        									 e.receive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.received_by ) AS received_by,
	        									 e.reason,
	        									 
	        									 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_by,
	        									 (SELECT created_at FROM ms_user WHERE id = a.created_by) AS created_at,
	        									 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS update_by,
	        									 (SELECT created_at FROM ms_user WHERE id = a.update_by) AS update_at
	        									 FROM tb_rds_detail AS a 
	        									 LEFT JOIN skin_master.ms_material AS b ON a.material_code = b.material_sku
	        									 LEFT JOIN tb_scheduler AS c ON a.id = c.id_schedule
	        									 LEFT JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number
	        									 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = d.schedule_number
	        									 LEFT JOIN ms_time_slot AS f ON d.id_time_slot = f.id
	        									 WHERE
	        									 
		 										
		 										 (
		 										 a.requested_delivery_date 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 OR d.rdd 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 )
		 			 							 GROUP BY c.id_schedule, c.schedule_number
		 			 							 ORDER BY a.requested_delivery_date ASC

	        									 ")->result();
			

	        	$data2 = $this->db->query("SELECT a.requested_delivery_date,
	        									 a.shift,
	        									 a.category,
	        									 a.po_number,
	        									 a.po_line_item,
	        									 a.material_code,
	        									 a.vendor_code,
	        									 a.uom,
	        									 a.uom_plt,
	        									 a.plt_truck,
	        									 a.req_pallet,
	        									 
	        									 b.material_name,
	        									 (a.req_pallet - c.quantity) as sisa,
	        									 (a.req_pallet - SUM(c.quantity) ) AS quantity,
	        									 c.receive_amount,
	        									 0 AS status,
	        									 '' AS rdd,
	        									 '' AS receipt,
	        									 '' AS id_truck,
	        									 '' AS driver_name,
	        									 
	        									 f.start_time,
	        									 f.end_time,
	        									 '' as actual_shift,
	        									 e.arrive_date,
	        									 e.arrive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.arrived_by ) AS arrived_by,
	        									 e.receive_date,
	        									 e.receive_time,
	        									 (SELECT nama FROM ms_user WHERE id = e.received_by ) AS received_by,
	        									 e.reason,
	        									 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_by,
	        									 (SELECT created_at FROM ms_user WHERE id = a.created_by) AS created_at,
	        									 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS update_by,
	        									 (SELECT created_at FROM ms_user WHERE id = a.update_by) AS update_at
	        									 FROM tb_rds_detail AS a 
	        									 INNER JOIN skin_master.ms_material AS b ON a.material_code = b.material_sku
	        									 LEFT JOIN tb_scheduler AS c ON c.id_schedule = a.id
	        									 INNER JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number
	        									 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = d.schedule_number
	        									 LEFT JOIN ms_time_slot AS f ON d.id_time_slot = f.id
	        									  WHERE	        		
	        									  							 
		 										 
		 										 (
		 										 a.requested_delivery_date 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 OR d.rdd 
		 										 BETWEEN '$start_date' AND '$end_date'
		 										 )		 			 							 
	        									 GROUP BY a.id HAVING quantity > 0
	        									 ")->result();

	        		

	        		$data = array_merge($data1, $data2);
	        		
	        	foreach ($data as $get) {
	        		$sheet->setCellValue('A'.(8+$no), $no+1);
	        		$sheet->setCellValue('B'.(8+$no), $get->receipt);
	        		$sheet->setCellValue('C'.(8+$no), $get->id_truck);
	        		$sheet->setCellValue('D'.(8+$no), $get->driver_name);
	        		$sheet->setCellValue('E'.(8+$no), $get->requested_delivery_date);
	        		$sheet->setCellValue('F'.(8+$no), $get->shift);
	        		$sheet->setCellValue('G'.(8+$no), $get->start_time." - ".$get->end_time);
	        		$sheet->setCellValue('H'.(8+$no), $get->rdd);
	        		$sheet->setCellValue('I'.(8+$no), $get->actual_shift);
	        		$sheet->setCellValue('J'.(8+$no), "");
	        		$sheet->setCellValue('K'.(8+$no), $get->category);
	        		$sheet->setCellValue('L'.(8+$no), $get->po_number);
	        		$sheet->setCellValue('M'.(8+$no), $get->po_line_item);
	        		$sheet->setCellValue('N'.(8+$no), $get->material_code);
	        		$sheet->setCellValue('O'.(8+$no), $get->material_name);
	        		$sheet->setCellValue('P'.(8+$no), $get->vendor_code);
	        		$sheet->setCellValue('Q'.(8+$no), $get->quantity);
	        		$sheet->setCellValue('R'.(8+$no), $get->uom);
	        		$sheet->setCellValue('S'.(8+$no), $get->uom_plt);
	        		$sheet->setCellValue('T'.(8+$no), $get->plt_truck);
	        		$sheet->setCellValue('U'.(8+$no), $get->req_pallet);

	        		if($get->status == '1'){
		 					$plan = 'Planned';
				 		}elseif($get->status == '2'){
				 			$plan = 'Arrived';
				 		}elseif($get->status == '3'){
				 			$plan = 'Received';
				 		}else{
				 			$plan = 'Not Planned';
				 	}
	        		
	         		$sheet->setCellValue('V'.(8+$no), $plan);
	         		$sheet->setCellValue('W'.(8+$no), $get->arrive_date);
	         		$sheet->setCellValue('X'.(8+$no), $get->arrived_by);
	         		$sheet->setCellValue('Y'.(8+$no), $get->receive_date);
	         		$sheet->setCellValue('Z'.(8+$no), $get->received_by);
	         		$sheet->setCellValue('AA'.(8+$no), $get->reason);
	         		$receive_amount = '';
	         		$uom_receive = '';
	         		if($get->receive_amount != 0){
	         			$uom_receive = $get->uom_plt*$get->receive_amount;
	         			$receive_amount = $get->receive_amount;
	         		}
	        		$sheet->setCellValue('AB'.(8+$no), $uom_receive);
	        		$sheet->setCellValue('AC'.(8+$no), $receive_amount);
	        		$sheet->setCellValue('AD'.(8+$no), $get->created_by);
	        		$sheet->setCellValue('AE'.(8+$no), $get->created_at);
	        		$sheet->setCellValue('AF'.(8+$no), $get->update_by);
	        		$sheet->setCellValue('AG'.(8+$no), $get->update_at);
	        		$no++;
	        		$number++;
	        	}
	        	//

	        }else{
	        	$data = $this->db->query("SELECT a.po_number,
	        									 a.po_line_item,
	        									 a.material_code,
	        									 a.qty,
	        									 b.receive_amount,
	        									 d.material_name,
	        									 c.status,
	        									 a.uom_plt
	        									 
	        									 FROM tb_rds_detail AS a 
	        									 LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
	        									 LEFT JOIN tb_schedule_detail AS c ON b.schedule_number = c.schedule_number
	        									 INNER JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku
	        									 WHERE 
	        									 YEAR(a.requested_delivery_date) = '$year' AND a.week = '$week'
	        									 GROUP BY a.id
	        									 ")->result();

	        	foreach ($data as $get) {
	        		$po_number = $get->po_number;
	        		$po_line_item = $get->po_line_item;
	        		$material_code = $get->material_code;	
	        		$sheet->setCellValue('A'.(2+$no), $no+1);
	        		$sheet->setCellValue('B'.(2+$no), $po_number);
	        		$sheet->setCellValue('C'.(2+$no), $po_line_item);
	        		$sheet->setCellValue('D'.(2+$no), $material_code);
	        		$sheet->setCellValue('E'.(2+$no), $get->material_name);
	        		$sheet->setCellValue('F'.(2+$no), $get->qty);
	        		$sheet->setCellValue('M'.(2+$no), $get->uom_plt);
	        		$not_planned = '';
	        		
	        // 		$check_data = $this->check_qty(0,$po_number,$po_line_item,$material_code);

	     			// $sheet->setCellValue('G'.(2+$no), $check_data);

	     			// $check_data = $this->check_qty(1,$po_number,$po_line_item,$material_code);
	     			// $sheet->setCellValue('H'.(2+$no), $check_data);

	     			// $check_data = $this->check_qty(2,$po_number,$po_line_item,$material_code);
	     			// $sheet->setCellValue('I'.(2+$no), $check_data);

	     			// $check_data = $this->check_qty(3,$po_number,$po_line_item,$material_code);
	     			// $sheet->setCellValue('J'.(2+$no), $check_data);
	     			

	        		$no++;
	        	}
	        	
	        }
	        
	        $sheetNo++;
	    }
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="material_movement_'.date('d_m_Y').'.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');
	}

	public function receipt_pdf(){
		$time_slot = $this->db->query("SELECT start_time, end_time 
									   FROM ms_time_slot "
									);

		$sn = $this->input->get('Schedule');
		$del = $this->db->query("SELECT a.*,c.vendor_code,e.vendor_name,e.vendor_alias,d.shift,d.start_time,d.end_time, a.receipt FROM tb_delivery_detail as a
								 INNER JOIN tb_scheduler as b ON a.id_schedule_group = b.schedule_number
								 INNER JOIN tb_schedule_detail as c ON c.schedule_number = a.id_schedule_group
								 LEFT JOIN skin_master.ms_supplier as e ON e.vendor_code = c.vendor_code
								 INNER JOIN ms_time_slot as d ON c.id_time_slot = d.slot_number
								 
							     WHERE id_schedule_group ='$sn' ")->row();

		$get_date = substr($del->delivery_date,0,10);
		$date = date('d M Y', strtotime($get_date));
		$data_supplier = explode('_',$del->invoice);
		$do_number = explode('.',$data_supplier[4]);
		$vendor_name = $del->vendor_name;
		$vendor_alias = $del->vendor_alias;
		$shift = '';
		if($del->shift == 'DP'){
			$shift = 'Dinas Pagi';
		}elseif($del->shift == 'DS'){
			$shift = 'Dinas Siang';
		}else{
			$shift = 'Dinas Malam';
		}

		if(!file_exists('assets/dist/img/'.$del->receipt.".png")){
			$this->load->library('infiQr');
			QRcode::png($del->receipt, 'assets/dist/img/'.$del->receipt.".png");
		}

		$fpdf = new FPDF('P', 'mm', 'A4');
		$fpdf->AddFont('Helvetica');
		$fpdf->SetFillColor(200, 200, 200);
		$fpdf->AddPage();
		$fpdf->SetMargins(5, 1, 5);
		/* TITLE */

		$fpdf->SetFont('Helvetica', 'B', '10');
		$fpdf->SetX(5);
		$fpdf->Cell(0, 0, "Delivery Receipt : ".$del->receipt, 0, 1, "R");
		$fpdf->SetX(5);
		$fpdf->SetFont('Helvetica', '', '10');
		$fpdf->Cell(0, 0, "Date : $date", 0, 1, "L");
		$fpdf->Image(base_url('assets/dist/img/'.$del->receipt.'.png'),170,20,35,35,'PNG');
		//logo
		$fpdf->Image(base_url('assets/dist/img/unilever_logo.png'), 5, 23, 28, 27);
		$fpdf->SetX(5);
		//$fpdf->Image(base_url()."assets/dist/img/unilever_logo.PNG", 10, 25, 23, 23, 'PNG');
		
		$fpdf->Ln(14);
		$fpdf->SetY(23);
		$fpdf->SetX(122);
		$fpdf->SetFont('Helvetica', '', '8');
		$fpdf->Cell(20, 6, "ID Truck", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->id_truck", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Vendor", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $vendor_alias", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Driver Name ", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->driver_name", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Phone Number", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->phone_number", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Shift", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $shift", 0, 1, "L");
		$fpdf->SetX(122);
		$fpdf->Cell(20, 6, "Time Slot", 0, 0, "L");
		$fpdf->Cell(40, 6, ": $del->start_time - $del->end_time", 0, 1, "L");
		$fpdf->SetFont('Helvetica', '', '9');
		$fpdf->Ln(5);
		
		$fpdf->SetFont('Helvetica', '', '8');
		
		$fpdf->SetY(23);
		$fpdf->SetX(35);
		$fpdf->MultiCell(60, 5, "PT. Unilever Plant : 9003 - SKIN FACTORY \nJl. Jababeka v Blok U No.14-16, Karangbaru, Cikarang Utara, Bekasi,\nJawa Barat 17530\nIndonesia", 0, 'L', 0);
		$fpdf->SetY(32);
		$fpdf->SetFont('Helvetica', 'B', '10');
		$fpdf->SetFont('Helvetica', 'B', '10');

		$fpdf->Ln(10);

		/* TABLE */
		$no = 1;
		$fpdf->SetY(65);
		$fpdf->SetFont('Helvetica', '', '7');
		$data_po = $this->db->query("SELECT b.po_number, b.material_code, c.material_name, b.qty, b.uom, b.uom_plt, b.req_pallet, a.quantity, (uom_plt*quantity) as SENT FROM tb_scheduler as a 
									 INNER JOIN tb_rds_detail as b ON a.id_schedule = b.id
									 LEFT JOIN skin_master.ms_material as c ON c.material_sku = b.material_code
									 
									 WHERE a.schedule_number = '$del->id_schedule_group' "); 
		$fpdf->Cell(10, 12, "NO", 1, 0, "C", 0);
		$fpdf->Cell(30, 12, "PO Number", 1, 0, "C", 0);
		$fpdf->Cell(30, 12, "Material Code", 1, 0, "C", 0);
		$fpdf->Cell(70, 12, "Material Name", 1, 0, "C", 0);
		$fpdf->Cell(15, 12, "Uom", 1, 0, "C", 0);
		$fpdf->Cell(25, 12, "Qty", 1, 0, "C", 0);
		$fpdf->Cell(20, 12, "Send Pallet", 1, 1, "C", 0);
		$total_qty = 0;
		$total_rpallet = 0;
		foreach ($data_po->result() as $data) {
			$fpdf->Cell(10, 12, $no, 1, 0, "C", 0);
			$fpdf->Cell(30, 12, ' '.$data->po_number, 1, 0, "L", 0);
			$fpdf->Cell(30, 12, ' '.$data->material_code, 1, 0, "L", 0);
			$fpdf->Cell(70, 12, ' '.$data->material_name, 1, 0, "L", 0);			
			$fpdf->Cell(15, 12, $data->uom, 1, 0, "C", 0);
			$fpdf->Cell(25, 12, $data->SENT, 1, 0, "C", 0);
			$fpdf->Cell(20, 12, $data->quantity, 1, 1, "C", 0);
			$total_qty = $total_qty + $data->qty;
			$total_rpallet = $total_rpallet + $data->req_pallet;
			$no++;
		}
		$fpdf->Ln(5);
		$fpdf->SetFont('Helvetica', 'B', '10');
		$fpdf->Cell(20, 6, "DO NUMBER : ", 0, 0, "L");
		$fpdf->SetFont('Helvetica', 'U', '10');
		$data_do = $this->db->query("SELECT do_number FROM tb_delivery_invoice WHERE id_schedule = '$del->id_schedule_group' ");
		$fpdf->Ln(5);
		$a = 1;
		foreach($data_do->result() as $get){
			if(($a % 5) == 0){
				$fpdf->Cell(38, 12, $get->do_number, 0, 1, "L", 0);
			}else{
				$fpdf->Cell(38, 12, $get->do_number, 0, 0, "L", 0);
			}
			$a++;
		}
		$username  = $this->db->query("SELECT nama FROM ms_user WHERE vendor_code = '$del->vendor_code' ")->row()->nama;
		$fpdf->Ln(25);
		$fpdf->SetFont('Helvetica', 'B', '10');
		$fpdf->MultiCell(173, 5, " Signature ", 0, 'R', 0);
		$fpdf->Ln(5);
		$fpdf->SetFont('Helvetica', 'B', '8');
		$fpdf->Cell(130, 5, "", 0, 0, "C", 0);
		$fpdf->Cell(65, 5, "$username", 0, 1, "C", 0);
		$fpdf->Cell(130, 5, "", 0, 0, "C", 0);
		$fpdf->Cell(65, 5, "$del->vendor_name", 0, 1, "C", 0);
		$fpdf->SetFont('Helvetica', '', '10');
		$fpdf->Cell(130, 5, "", 0, 0, "C", 0);
		$date = date('d-m-Y H:i', strtotime($del->delivery_date));
		$fpdf->Cell(65, 5, "$date", 0, 1, "C", 0);
		$fpdf->MultiCell(190, 5, "________________________________", 0, 'R', 0);
		$fpdf->Output();
	}

	public function getScheduleX($date, $vendor_code){

		$data = $this->db->query("SELECT
							tb_rds_detail.id,
							material_code,
							tb_rds_detail.vendor_code,
							qty,
							requested_delivery_date,
							req_pallet,
							IF(@rownum + req_pallet <= 32, @rownum := @rownum + req_pallet, @rownum := 0 + req_pallet) as row_number
						FROM
							tb_rds_detail
						CROSS JOIN (SELECT @rownum := 0) r
						WHERE
							tb_rds_detail.vendor_code = '$vendor_code'
						AND requested_delivery_date = '$date'");
		return $data;
	}

	public function getTimeSlot(){
		$data = $this->db->query("SELECT * FROM ms_time_slot ORDER BY ordering ASC");		

		return $data;
	}

	public function getScheduleNumber(){
		$data = $this->db->query("SELECT schedule_number FROM tb_schedule_detail ORDER BY schedule_number DESC LIMIT 0,1")->row();
		$sn = (!isset($data->schedule_number))?0:$data->schedule_number;
		return $sn+1;
	}

	public function getScheduleNew($date, $vendor_code, $user){
		if($user == '3'){
			$data = $this->db->query("SELECT
								a.*, b.vendor_name,
								b.vendor_alias,
								e.shift,
								(SELECT COUNT(*) FROM tb_scheduler WHERE schedule_number=a.schedule_number) as TOTAL_MATERIAL
							FROM
								tb_schedule_detail AS a
							LEFT JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
							INNER JOIN tb_scheduler d ON a.schedule_number = d.schedule_number
							INNER JOIN tb_rds_detail c ON d.id_schedule = c.id
							INNER JOIN ms_time_slot e ON a.id_time_slot = e.id
							WHERE
								 a.rdd = '$date'
							GROUP BY
								a.schedule_number");

		}else{

			$data = $this->db->query("SELECT
								a.*, b.vendor_name,
								b.vendor_alias,
								e.shift,
								(SELECT COUNT(*) FROM tb_scheduler WHERE schedule_number=a.schedule_number) as TOTAL_MATERIAL
							FROM
								tb_schedule_detail AS a
							INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
							INNER JOIN tb_scheduler d ON a.schedule_number = d.schedule_number
							INNER JOIN tb_rds_detail c ON d.id_schedule = c.id
							INNER JOIN ms_time_slot e ON a.id_time_slot = e.id
							WHERE
								a.rdd = '$date'
							GROUP BY
								a.schedule_number");
		}
		return $data;
	}

	public function checkSlotAva($id_slot, $date){
		$cek = $this->db->query("SELECT COUNT(*) as COUNT FROM tb_schedule_detail AS a WHERE (SELECT count(a.id) FROM tb_scheduler AS a INNER JOIN tb_Schedule_detail AS b ON a.schedule_number = b.schedule_number) > 0 AND a.id_time_slot= '$id_slot' AND a.rdd='$date' AND a.status != 0")->row();

		return $cek->COUNT;
	}

	public function checkSlotAva_not_plan($id_slot, $date){
		$cek = $this->db->query("SELECT COUNT(*) as COUNT FROM tb_schedule_detail AS a WHERE (SELECT count(a.id) FROM tb_scheduler AS a INNER JOIN tb_Schedule_detail AS b ON a.schedule_number = b.schedule_number) > 0 AND a.id_time_slot= '$id_slot' AND a.rdd='$date' AND a.status = 0")->row();

		return $cek->COUNT;
	}

	public function checkSlotAva_change($id_slot, $date, $shift){
		$cek = $this->db->query("SELECT Count(a.id_time_slot) AS total, a.id_time_slot FROM tb_schedule_detail AS a WHERE (SELECT count(a.id) FROM tb_scheduler AS a INNER JOIN tb_Schedule_detail AS b ON a.schedule_number = b.schedule_number) > 0 AND status != 0 GROUP BY a.id_time_Slot HAVING total >= 2")->result();

		$no = 0;
		$not = array();
		foreach($cek as $get){
			array_push($not, $get->id_time_slot); 
		}
		
		$not_time = implode(",", $not);
		
		$c_time = date('H:i:s');	
    	$order = $this->db->query("SELECT ordering FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")
    	->row()->ordering;

		if($not_time != ""){

			$get_schedule = $this->db->query("SELECT id FROM ms_time_slot WHERE id NOT IN($not_time) AND shift = '$shift' AND ordering >= '$order' ORDER BY id DESC")->row();
		
		}else{
			
			$get_schedule = $this->db->query("SELECT id FROM ms_time_slot WHERE shift = '$shift' AND ordering >= '$order' ORDER BY id DESC")->row();
		}
		
		
		return $get_schedule;
	}

	public function get_another_schedule($id_slot, $date, $id, $shift){
		$getData = $this->db->query("SELECT id FROM tb_schedule_detail WHERE (SELECT count(a.id) FROM tb_scheduler AS a INNER JOIN tb_Schedule_detail AS b ON a.schedule_number = b.schedule_number) > 0 AND id != '$id' AND rdd = '$date' AND id_time_slot = '$id_slot' AND status = '0' ORDER BY id DESC")->row()->id;
		
		return $getData;
	}

	public function getScheduleDetail($id){

			$data = $this->db->query("SELECT
							c.id,
							c.id_schedule,
							a.po_number,
							a.po_line_item,
							c.quantity,
							c.schedule_number,
							c.receive_amount,
							a.material_code,
							b.material_name,
							a.id as idz,
							a.requested_delivery_date,
							a.shift,
							a.qty,
							a.uom,
							a.uom_plt,
							a.req_pallet,
							(c.quantity*a.uom_plt) as SEND_AMOUNT,
							(SELECT amount FROM tb_return_form WHERE id_scheduler = c.id) AS return_receipt
						FROM
							tb_scheduler c
						INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
						LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
						WHERE
							schedule_number='$id'
						ORDER BY a.po_number ASC");
		
		$get_arr = $this->db->query("SELECT id_schedule FROM tb_scheduler WHERE schedule_number='$id'");
		return array(
			"data_list" => $data,
			"array_id" => $get_arr
		);
	}

	public function getDeliveryInfo($id){
		
		$data = $this->db->query("SELECT
									e.*,
									(SELECT nama FROM ms_user WHERE id = e.arrived_by) AS arrived_name,
									(SELECT nama FROM ms_user WHERE id = e.received_by) AS received_name,
									h.reason as adt_reason,
									g.status as STATE,
									g.rdd,
									concat(
										f.start_time,
										' - ',
										f.end_time
									) AS time_slot
								FROM
									tb_scheduler c
								LEFT JOIN tb_delivery_detail e ON c.schedule_number = e.id_schedule_group
								LEFT JOIN tb_schedule_detail g ON e.id_schedule_group = g.schedule_number
								LEFT JOIN tb_additional_schedule h ON e.id_schedule_group = h.schedule_number
								LEFT JOIN ms_time_slot f ON e.id_time_slot = f.slot_number
								LEFT JOIN tb_delivery_invoice z ON e.id_schedule_group = z.id_schedule
								WHERE
									g.schedule_number = '$id'
								LIMIT 0, 1");
		return $data;

	}

	public function getdnDownload($id){
		
		$data = $this->db->query("SELECT
									b.*
								FROM
									tb_delivery_detail as a
								INNER JOIN tb_delivery_invoice as b ON b.id_schedule = a.id_schedule_group
								WHERE a.id_schedule_group = '$id'
								");
		return $data;

	}

	public function moveSchedule($id){
		$data = $this->db->query("SELECT a.vendor_code,a.po_number, a.material_code, a.requested_delivery_date, a.shift, b.quantity FROM tb_rds_detail as a INNER JOIN tb_scheduler as b ON a.id = b.id_schedule WHERE b.id = '$id' ")->row();

		$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Take Out",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_scheduler",
	  			"id_join"		=> $id,
	  			"description"	=> "FROM RDD :".$data->requested_delivery_date.", Shift :".$data->shift.", PO :".$data->po_number.", Material Code :".$data->material_code.", Req Pallet :".$data->quantity,
	  			"select_join"	=> "receipt",
	  			"author"		=> 3,
	  			"vendor_code"	=> $data->vendor_code
	  		);
	  	$this->db->insert('tb_history', $history);
		
		$this->db->query("INSERT INTO tb_schedule_pool SELECT '' as ids,id_schedule, vendor_code, schedule_number, rdd, status FROM tb_scheduler where id='$id' ");
		$this->db->query("DELETE FROM tb_scheduler WHERE id='$id'");

		
	}
public function getTimeSlotById($id){
		$data = $this->db->query("SELECT * FROM ms_time_slot WHERE slot_number=$id")->row();
		$st = date_create($data->start_time);
		$st = date_format($st, "H:i");
		$et = date_create($data->end_time);
		$et = date_format($et, "H:i");
		echo $st." - ".$et;
	}
	public function getSuggestion($find, $sn, $rdd, $vendor_codez){
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$c_time = date('H:i:s');
		$getShift = $this->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$shift = $getShift->shift;
		$get_Shift = '';
		
		if($shift == 'DP'){
			$get_Shift = "AND a.shift != 'DM' ";
		}else if($shift == 'DS'){
			$get_Shift = "AND a.shift = 'DS' ";
		}

			$data = $this->db->query("SELECT
									c.id,
									c.id_schedule,
									a.po_number,
									c.schedule_number,
									c.vendor_code,
									d.vendor_name,
									a.material_code,
									b.material_name,
									a.req_pallet,
									a.uom_plt,
									a.qty,
									a.shift,
									a.requested_delivery_date
									-- c.quantity as req_pallet
								FROM
									tb_scheduler c
								INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
								LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
								INNER JOIN skin_master.ms_supplier d ON c.vendor_code = d.vendor_code
								WHERE
									schedule_number !='$sn'
								AND
									c.rdd = '$rdd'
								AND
									a.status = '0'
								AND
									a.vendor_code = '$vendor_codez'
								AND
									a.req_pallet <= '$find'
								$get_Shift
								ORDER BY a.po_number ASC");
		
		
		return $data;
	}

	
	public function material_pool($role, $rdd, $vendor_codez, $cminus){
		
		if($role == '3'){
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$getData = $this->db->query("SELECT
											*
										FROM
											(
												SELECT
													trd.*, mm.material_name,
													IF(tb.receive_amount = 0,
														(
															trd.req_pallet - SUM(tb.quantity)
														), (
															trd.req_pallet - SUM(tb.receive_amount)
														)
													) AS sisa
												FROM
													tb_rds_detail AS trd
												INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
												LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
												WHERE
													trd.vendor_code = '$vendor_code'
												AND trd.is_schedule = '1'
												AND trd.req_pallet <= '$cminus'

												GROUP BY
													trd.id
											) AS TB_1
										WHERE sisa > 0 AND sisa <= '$cminus' OR sisa IS NULL");
		}else{
			$getData = $this->db->query("SELECT
											*
										FROM
											(
												SELECT
													trd.*, mm.material_name,
													(
														trd.req_pallet - SUM(tb.quantity)
													) AS sisa
												FROM
													tb_rds_detail AS trd
												INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
												LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
												WHERE
													trd.vendor_code = '$vendor_codez'
												AND trd.is_schedule = '1'
												AND trd.req_pallet <= '$cminus'
												GROUP BY
													trd.id 
											) AS TB_1
										WHERE sisa > 0 AND sisa <= '$cminus' OR sisa IS NULL");
		}
		
		return $getData;
	}

	public function getDateLess($rdd, $sn, $vendor_codez, $cminus){
		$getData = "";
		$vendor_code = $this->session->userdata('sess_vendor_code');
		if($sn != ""){
			$role = $this->session->userdata('sess_role');
			if($role == '3'){
			$getData = $this->db->query("SELECT
										trd.*, mm.material_name, null as sisa
									FROM
										tb_rds_detail AS trd
									INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
									LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
									WHERE trd.requested_delivery_date < '$rdd' 
									
									AND trd.status='0' 
									AND trd.vendor_code = '$vendor_code' 
									AND tb.schedule_number != $sn");
			}else{
				$getData = $this->db->query("SELECT
										trd.*, mm.material_name, null as sisa
									FROM
										tb_rds_detail AS trd
									INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
									LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
									WHERE trd.requested_delivery_date < '$rdd'
									 
									AND trd.status='0' 
									AND trd.vendor_code = '$vendor_codez' 
									AND tb.schedule_number != $sn");
			}
		}
		
		return $getData;
	}

	public function getTimeSlot2(){
        $data = $this->db->query("SELECT * FROM ms_time_slot");
        
        return $data;
    }

  public function getAdtTimeSlotWithSisa($date)
    {
    	$cdate = date('Y-m-d');
    	$c_time = ($date == $cdate)?date('H:i:s'):"00:01";
		
    	
    	$get_order = $this->db->query("SELECT ordering, id FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
    	$order = $get_order->id;
    	$ordering = $get_order->ordering;

    	$ts = $this->db->query("SELECT start_time, end_time, id, ordering, (3) AS sisa FROM ms_time_slot WHERE id >= '$order' ")->result();
    	
    	$get_data = [];
    	$no = 0;
    	foreach ($ts as $get) {
    		//dump($ts_sisa[$no]->id);
    		

    		$sisa = $this->db->query("SELECT (3-COUNT(a.id_time_slot)) as sisa FROM tb_schedule_detail AS a INNER JOIN ms_time_slot AS b ON a.id_time_slot = b.id 
    		WHERE a.status != 0
    		AND a.id_time_slot = '$get->id' 
    		AND a.rdd = '$date' 
    		AND (SELECT count(c.id) FROM tb_scheduler AS c INNER JOIN tb_schedule_detail as d ON c.schedule_number = d.schedule_number) > 0 
    		GROUP BY a.id_time_slot")->row();
    		if(!empty($sisa)){
    			$sisax = $sisa->sisa;
    		}else{
    			$sisax = "3";
    		}

    		if($ordering > 1 && $get->ordering > 1 ){
    			$get_data[$no] = Array(
	    			"id" 			=> $get->id,
	    			"start_time" 	=> $get->start_time,
	    			"end_time"		=> $get->end_time,
	    			"sisa"			=> $sisax
	    		);
    		}
    		
    		$no++;
    	}
    	
    	$data = json_decode(json_encode($get_data, true));

    	// $ts_sisa = array_combine($sisa, $ts);
    	return $data;
    }

    public function getTimeSlot_adt($date){

    	date_default_timezone_set("Asia/Bangkok");
    	$c_time = date('H:i:s');
		$order = $this->db->query("SELECT ordering FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row()->ordering; 

        $data = $this->db->query("SELECT a.id, COUNT(b.id_time_slot) FROM ms_time_slot AS a LEFT JOIN tb_schedule_detail AS b ON b.id_time_slot = a.id WHERE b.rdd = '$date' GROUP BY b.id_time_slot HAVING count(b.id_time_slot) > 2")->result();
        $not = array();

        foreach ($data as $get) {
        	array_push($not, $get->id);	
        }

        $get_time = [];
        $not_time = implode(",", $not);
        if($not_time != ''){
       	 $get_time = $this->db->query("SELECT id, start_time, end_time FROM ms_time_slot WHERE id NOT IN($not_time) AND id >= $order ")->result();
        }else{
         $get_time = $this->db->query("SELECT id, start_time, end_time FROM ms_time_slot WHERE ordering >= $order ")->result();
        }
       	
        return $get_time;
    }

    public function setStatus($stat, $id){
    	$data = $this->db->query("UPDATE tb_schedule_detail SET status='?' WHERE id='?'", [$stat, $id]);
    	return $data;
    }

    public function getShiftLate($today, $shift, $imp, $year, $week){
		if($imp != ""){
			$add_whereimp = "AND trd.id NOT IN($imp)";
		}else{
			$add_whereimp = "";
		}

		if($shift == "DM"){
			$add_where = "";
		}else if($shift == "DP"){
			$add_where = "AND trd.shift IN('DM')";
		}else{
			$add_where = "AND trd.shift IN('DM', 'DP')";
		}
		$getData = $this->db->query("SELECT
											*
										FROM
											(
												SELECT
													trd.*, mm.material_name,
													trd.req_pallet AS sisa
												FROM
													tb_rds_detail AS trd
												INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
												LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
												WHERE
													trd.requested_delivery_date= '$today'
												AND trd.is_schedule = '1'
												AND YEAR(trd.requested_delivery_date) ='$year'
												AND trd.week='$week'
												$add_whereimp
												$add_where
												GROUP BY
													trd.id
											) AS TB_1");
		return $getData;
	}

    public function mail($body='', $vendor){

			// Create the Transport
    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', '465', 'ssl')
        ->setUsername('radenadenuradisa@gmail.com')
        ->setPassword('ZxyUi..Pq*6643');

		$sendto = $this->db->query("SELECT email FROM ms_user WHERE role != '3' ")->result();
		$vendor = $this->db->query("SELECT email FROM ms_user WHERE vendor_code = '$vendor' ")->result();

		$email_to = array_merge($sendto, $vendor);
			foreach($email_to as $get){
		    // Create the Mailer using your created Transport
		    $mailer = Swift_Mailer::newInstance($transport);

		    // Create a message
		    $message = (new Swift_Message('Additional Truck - No Reply'))
		      ->setFrom(['radenadenuradisa@gmail.com' => 'Time Slot Booking'])
		      ->setTo([ $get->email => "Time Slot Users" ])
		      ->setBody($body, 'text/html');

				// Send the message
				$result = $mailer->send($message);
		}
	}

    public function total_ourstanding($shift){
    	date_default_timezone_set("Asia/Bangkok");
    	$time = date('H:i');
		$today = date('Y-m-d');
		$getDataShift = 0;
		$getDataShift = 0;
		$getDataless = 0;

		if($shift == 'DS'){
			$getDataShift = $this->db->query("SELECT IF((a.req_pallet - b.quantity) = 0, a.req_pallet, (a.req_pallet - b.quantity)) AS req_pallet  FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE shift != 'DS' AND requested_delivery_date = '$today' ");
			if($getDataShift->num_rows() > 0){
				$getDataShift = $getDataShift->row()->req_pallet;
			}else{
				$getDataShift = 0;
			}

		}else if($shift == 'DP'){
			$getDataShift = $this->db->query("SELECT IF((a.req_pallet - b.quantity) = 0, a.req_pallet, (a.req_pallet - b.quantity)) AS req_pallet  FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE shift = 'DM' AND requested_delivery_date = '$today' ");
			if($getDataShift->num_rows() > 0){
				$getDataShift = $getDataShift->row()->req_pallet;
			}else{
				$getDataShift = 0;
			}
		}

		$getDataSisa = $this->db->query("SELECT SUM(a.req_pallet - b.quantity) as total_pallet FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule ")->row()->total_pallet;

		$getDataless = $this->db->query("SELECT SUM(req_pallet) as total_pallet FROM tb_rds_detail WHERE requested_delivery_date < '$today' AND status = '0' ")->row()->total_pallet;


		if(empty($getDataless) ){
			$getDataless = 0;
		}

		if(empty($getDataSisa) ){
			$getDataSisa = 0;
		}

		return ($getDataShift + $getDataSisa + $getDataless);
	}
    

    public function ourstanding_day($shift){
    	date_default_timezone_set("Asia/Bangkok");
    	$time = date('H:i');
		$today = date('Y-m-d');
		$getDataShift = "";

		if($shift == 'DS'){
			$getDataShift = $this->db->query("SELECT IF((a.req_pallet - b.quantity) = 0, a.req_pallet, (a.req_pallet - b.quantity)) AS req_pallet  FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE shift != 'DS' AND requested_delivery_date = '$today' ");
			if($getDataShift->num_rows() > 0){
				$getDataShift = $getDataShift->row()->req_pallet;
			}else{
				$getDataShift = 0;
			}
		}else if($shift == 'DP'){
			$getDataShift = $this->db->query("SELECT IF((a.req_pallet - b.quantity) = 0, a.req_pallet, (a.req_pallet - b.quantity)) AS req_pallet  FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE shift = 'DM' AND requested_delivery_date = '$today' ");
			if($getDataShift->num_rows() > 0){
				$getDataShift = $getDataShift->row()->req_pallet;
			}else{
				$getDataShift = 0;
			}
			
		}

		$getDataSisa = $this->db->query("SELECT SUM(a.req_pallet - b.quantity) as total_pallet FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule ")->row()->total_pallet;
		
		if(empty($getDataShift) ){
			$getDataShift = 0;
		}

		if(empty($getDataless) ){
			$getDataless = 0;
		}

		if(empty($getDataSisa) ){
			$getDataSisa = 0;
		}
		
		return ($getDataShift + $getDataSisa);

    }

    public function total_received($date1, $date2){
    	$received = $this->db->query("SELECT SUM(receive_amount) AS total_received FROM tb_scheduler WHERE rdd BETWEEN '$date1' AND '$date2' ")->row()->total_received;
    	return $received;
    }

    public function received_day($date1){
    	$received = $this->db->query("SELECT SUM(receive_amount) AS total_received FROM tb_scheduler WHERE rdd = '$date1' ")->row()->total_received;
    	return $received;
    }

}

?>