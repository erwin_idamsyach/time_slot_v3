<style type="text/css">
  .box-header {
    color: #ffffff !important;
    background-color: #31559F !important;
    text-transform: uppercase;
}
.box-header {
    
    display: block;
    padding: 10px;
    position: relative;
}

/*.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 3px 8px;
    vertical-align: middle;
    border-color: #ddd;
}

.table>thead>tr>th {
    border: 1px solid #6A6A6A !important;
    background-color: rgb(255,204,102);
    color: black;
}*/

.box .box-body {
    background: #FFF;
}

.box-body {
    max-width: 100%;
    overflow: auto;
    padding: 15px;
}
.box-body {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    padding: 10px;
}

.form-check, label {
    font-size: 14px;
    line-height: 0;
    color: #ffffff;
    font-weight: 400;
}
.fa-times{
  color:red;
}

.fa-arrow-up{
  color:green;
}

b{
  font-size:12px;
}

</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="far fa-address-card"></i>
                    </div>
                    <h4 class="card-title">Delivery Form</h4>
                  </div>
                  <div class="card-body ">

                    <form action="<?php echo base_url(); ?>schedule/save_schedule" method="post" enctype="multipart/form-data">
                <input type="hidden" name="slot_number" value="<?php echo $this->uri->segment(3); ?>">
                
                <div class="row">
                  <div class="col-md-2">
                    <div class="form-group">
                      <b>RDD</b><br>
                      <b>
                        <?php
                      $date = date_create($this->uri->segment(4));
                      $dfns = date_format($date, "D, d M Y");
                      echo $dfns;
                        ?>
                      </b>
                    </div>
                  </div>
                  
                  <div class="col-md-2">
                    <div class="form-group">
                      <b>Time Slot</b><br>
                        <b>
                          <?php
                            $this->Schedule_model->getTimeSlotById($this->uri->segment(5));
                          ?>
                        </b>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <b>Date Time</b><br>
                        <p id='jam'></p>
                    </div>
                  </div>

                  <div class="col-md-5">
                    <button class="btn btn-primary btn-sm" style="margin-top:20px" type="button" data-toggle='modal' data-target='#modalDN'>ADD DN</button>
                    <button class="btn btn-warning btn-sm" style="margin-top:20px" type="button" data-toggle='modal' data-target='#modalCOA'>ADD COA</button>
                  </div>

                </div>

                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Truck No.</label>
                      <input type="text" name="truck_no" value="" class="form-control" required="">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Driver Name</label>
                      <input type="text" name="driver_name" value="" class="form-control" required="">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Phone Number</label>
                      <input type="number" name="phone_number" value="" class="form-control" required="">
                    </div>
                  </div>
                </div>

 <!-- Modal DN-->
<div id="modalDN" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width:700px">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">DN Data</h4>
      </div>
      <div class="modal-body" >
        
        <div id="delivery_notes">
                  <p class="help-block" style="color:blue">* Pastikan format file Delivery Note adalah .DOCX atau .PDF</p>
                  <?php echo "<b style='color:red'>".$this->session->flashdata('ERR')."</b>"; ?>
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="table">
                    <thead style="color:white; background-color:#31559F;font-weight: 650">
                        <tr>
                          <th class="text-center">#</th>
                          <th class="text-center">No</th>
                          <th class="text-center">DN</th>
                          <th class="text-center">Input DN</th>
                        </tr>  
                      </thead>
                      <tbody id="body_dn">
                          <tr id='dn_number1'>
                            <td><span class="btn btn-danger btn-sm" onclick="removeDN('1')" id='delete_dn1'><i class="fas fa-minus"></i></span></td>
                            <td style="text-align:center"><b class="no_dn">1</b></td>
                            <td><input type="text" class="dn" style="width: 100" name="do_number1"></td>
                            <td><input type="file" class="dn_input" required class="btn btn-primary btn-sm" style="" name="file1"></td>
                          </tr>

                      </tbody>
                      <tr>
                          <td><span onclick="addDN()" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i></span></td>
                        </tr>
                    </table>
                  </div>
                  <!-- <div class="row">
                      <div class="col-md-3">
                        
                              <input type="text" name="do_number1" width="50" placeholder="Delivery Note Name" class="form-control" required="">

                              <span readonly onclick="$('#file1').click()" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Choose File</span>
                              <span readonly class="btn btn-danger btn-sm"><i class="fas fa-minus"></i></span>
                              <input type="file" name="file1" hidden="" id="file1" class="input-group-addon form-control" required="">
                      </div>

                      <div class="col-md-3">
                        <button type="button" style="margin:5px;margin-bottom:5px" class="btn btn-primary btn-sm" onclick="addDN()"><span class="fa fa-plus"></span></button>
                      </div>
                  </div> -->
                </div>
                <input hidden type="text" id="dn_count" value="1" name="dn_count">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

 <!-- Modal COA-->
<div id="modalCOA" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width:700px">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">COA Data</h4>
      </div>
      <div class="modal-body" >
        
        <p class="help-block" style="color:blue">* Pastikan format file COA adalah .DOCX atau .PDF</p>
                    <?php echo "<b style='color:red'>".$this->session->flashdata('ERR2')."</b>"; ?>
                    <div id="CoA">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table">
                    <thead style="color:white; background-color:#31559F;font-weight: 650">
                            <tr>
                              <th class="text-center">#</th>
                              <th class="text-center">No</th>
                              <th class="text-center">Input COA</th>
                            </tr> 
                          </thead>
                          <tbody id="body_coa">
                           
                              <tr id='no_coa1'>
                                <td><span class="btn btn-danger btn-sm" onclick="removeCOA(1)"><i class="fas fa-minus"></i></span></td>
                                <td style="text-align:center"><b class="no_coa">1</b></td>
                                <td><input type="file" required class="btn btn-warning btn-sm coa_input" style="" name="coa1"></td>
                              </tr>
                           

                              
                          </tbody>
                          <tr>
                                <td><span onclick='addCOA()' class="btn btn-warning btn-sm"><i class="fas fa-plus"></i></span></td>
                              </tr>
                        </table>
                      </div>
                </div>
                
                <input hidden type="text" id="coa_count" value="1" name="coa_count">


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



              
                <div class="form-group">
                  <input type="hidden" name="id_schedule" value="<?php echo $this->uri->segment(3) ?>">
                  <input type="hidden" name="rdd" value="<?php echo $this->uri->segment(4) ?>">
                  <input type="hidden" name="id_time_slot" value="<?php echo $this->uri->segment(5) ?>">
                  <label id="check-error"></label>
                  <input type="hidden" value="" id="count-pallet">
                  <button type="button" class="btn btn-primary" onclick="check_detail($('#dn_count').val(), $('#coa_count').val() )">SUBMIT</button>
                  <button hidden type="submit" id="submitz"></button>
                </div>
              </form>


                  </div>
                </div>
              </div>
          </div>
    </div>
  
  <div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-pallet"></i>
                    </div>
                    <h4 class="card-title">Material List</h4>
                    <h4 id="area-cnt-pallet" style="color:black">Slot Occupancy : .../32</h4>
                  </div>
                  <div class="card-body ">
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                <thead style="color:white; background-color:#31559F;font-weight: 650">
                  <tr>
                    <th class="text-center">PO Number</th>
                    <th class="text-center">Material Code</th>
                    <th class="text-center">Material Name</th>
                    <th class="text-center">Date / Shift</th>
                    <th class="text-center">Req. Pallet</th>
                    <th class="text-center">Send Qty</th>
                    <th class="text-center">*</th>
                  </tr>
                </thead>
                <tbody id="area-list">
                  <?php
                  $no = 0;
                  foreach($data_list->result() as $get){
                    ?>
                  <tr>
                    <td class="text-center"><?php echo $get->po_number ?><input type="text" hidden value="<?php echo $get->idz; ?>" id="idz<?php echo $no; ?>" ></td>
                    <td><?php echo $get->material_code ?><input type="text" hidden value="<?php echo $get->material_code; ?>" id="material_code" ></td>
                    <td><?php echo $get->material_name ?></td>
                    <td>
                      <?php
                      $r_date = date_create($get->requested_delivery_date);
                      $df = date_format($r_date, "d-m-Y");
                      echo $df." / ".$get->shift
                      ?>
                    </td>
                    <td width="35px">
                      <input type="text" class="form-control inp-pallet" value="<?php echo $get->quantity ?>" onchange="cutPallet(<?php echo $get->id ?>, this.value, <?php echo $get->idz ?>, <?php echo $get->quantity; ?>)">
                      <input type="text" hidden value="<?php echo $get->quantity; ?>" id="qty_material<?php echo $no++; ?>" >
                    </td>
                    <td><?php echo ($get->uom_plt * $get->quantity)." ".$get->uom; ?></td>
                    <td class="text-center">
                      <button class="btn btn-link clr-red" onclick="moveMaterial(<?php echo $get->id; ?>, <?php echo $sn; ?>)">
                        <i class="fa fa-times"></i>
                      </button>
                    </td>
                    
                  </tr>
                    <?php
                  }
                  ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-danger card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-dolly-flatbed"></i>
                    </div>
                    <h4 class="card-title">Material Pool</h4>
                  </div>
                  <div class="card-body ">

                    <div class="box-body" id="area-pool">
                  
                    </div>

                  </div>
                </div>
              </div>
            </div>
    </div>
  </div>
</div>


<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i style="font-size:30px" class="fas fa-boxes"></i>
              </div>
              <h4 class="card-title">Find Material From Same RDD</h4>
            </div>
            <div class="card-body ">
                <div <?php echo ($this->uri->segment(7) == 'adt')?"hidden":'' ?> class="col-md-12">
                  <div class="box"> 
                    <div class="box-body" id="area-same-rdd"></div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>