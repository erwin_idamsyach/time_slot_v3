<style type="text/css">
	.information{
		color:black;
	}
	.form-check, label {
    font-size: 14px;
    line-height: 0;
    color: black;
    font-weight: 400;

}
label{
	font-size:12px;
}


.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 3px 8px;
    vertical-align: middle;
    border-color: #ddd;
}

/*.table>thead>tr>th {
    border: 1px solid #6A6A6A !important;
    background-color: rgb(255,204,102);
    color: black;
}*/


	.alert{
		padding: 10px 20px;
	}


</style>


<div class="row">
	<div class="col-md-4">
		<div>
		<div class="form-group" style="display:none" id="timer_edit">
			<table <?php if($this->session->userdata('sess_role_no') != '3'){ echo 'hidden';} ?>>
				<tr>
					<td width="50" align="center"><p id="editp">Edit</p></td>
					<td width="100" align="center">Time Remaining</td>
				</tr>
				<tr>
					<?php $id = $del->id; ?>
					<td>
						<div id="EditBtn">
							<a onclick="editModal()">
								<button class='btn btn-warning btn-sm form-control' disabled="true" id='btnbtn'><i class='fa fa-edit'></i>
								</button></a>
						</div>
						<div style="display: none" id="Cancel">
							<button id="btnbtncancel" class="btn btn-danger form-control" onclick="Cancel()"><span class="fas fa-times"></span>
							</button>
						</div>
					</td>
					<?php $getcreatedat =  $del->created_at; 
						  $newCreatedat =  Date('M d, Y H:i:s', strtotime($getcreatedat));
					?>
					<td><input style="display: none" value="<?php echo $newCreatedat; ?>" id="created_at"><p align="center" id="remain"></p></td>
				</tr>
			</table> 
		</div>
		<table>
				<tr class="">
					<td width="185"><label style='font-size:12px;font-weight:700'>RECEIPT</label></td>
					<td><label style='font-size:12px;font-weight:700'>RDD</label></td>
				</tr>
				<tr>
					<td><label style='font-size:12px;font-weight:700'><?php echo $del->receipt; ?></label></td>
					<td><?php 
						$date = date_create($del->rdd);
						$df = date_format($date, "D, d M Y");
						echo $df; ?>
					</td>
				</tr>
			</table>
			<br>
		<table>
				<tr class="">
					<td width="185"><label style='font-size:12px;font-weight:700'>Time</label></td>
					<td><label style='font-size:12px;font-weight:700'>Status</label></td>
				</tr>
				<tr>
					<td><p style="font-size:12px" id="#remain"><?php echo $del->time_slot; ?></p></td>
					<td><label style='font-size:12px;font-weight:700'>
					<p id="#remain">
						<?php
						if($del->STATE == 0){
							?>
						<div class="alert alert-default">NOT PLANNED</div>
							<?php
						}else if($del->STATE == 1){
							?>
						<div class="alert alert-warning">PLANNED</div>
							<?php
						}else if($del->STATE == 2){
							?>
						<div class="alert alert-warning">ARRIVED</div>
							<?php
						}else{
							?>
						<div class="alert alert-success">RECEIVED</div>
							<?php
						}
						?>
					</p>
				</label></td>
				</tr>
			</table>
		
			<table >
				<tr>
					<td width="185">
						<div class="form-group">
							<label style='font-size:12px;font-weight:700'>Time Arrived</label><br>
							
								<p style="font-size:12px" id="#remain" style="white-space: nowrap;">
									<?php
									if($del->arrive_date == ""){
										echo "-";
									}else{
										$ad = date_create($del->arrive_date." ".$del->arrive_time);
										$ad = date_format($ad, "D, d M Y H:i:s");
										echo $ad;
									}
									?>
								</p>
							
						</div>
					</td>
					<td>
						<div class="form-group">
							<label style='font-size:12px;font-weight:700'>Arrived By</label><br>
							
								<p style="font-size:12px" id="#remain" style="white-space: nowrap;">
									<?php
									if($del->arrived_by == ""){
										echo "-";
									}else{
										echo $del->arrived_name;
									}
									?>
								</p>
							
						</div>
					</td>
				</tr>
			</table>

		
			<table>
				<tr>
					<td width="185">
						<div class="form-group">
							<label style='font-size:12px;font-weight:700'>Time Received</label><br>
							
								<p style="font-size:12px" id="#remain" style="white-space: nowrap;">
									<?php
									if($del->receive_date == ""){
										echo "-";
									}else{
										$rd = date_create($del->receive_date." ".$del->receive_time);
										$rd = date_format($rd, "D, d M Y H:i:s");
										echo $rd;
									}
									?>
								</p>
							
						</div>
					</td>

					<td>
						<div class="form-group">
							<label style='font-size:12px;font-weight:700'>Received By</label><br>
							
								<p style="font-size:12px" id="#remain">
									<?php
									if($del->received_by == ""){
										echo "-";
									}else{
										echo $del->received_name;
									}
									?>
								</p>
							
						</div>
					</td>
				</tr>

			</table>

			<table>
				<tr>
					<td width="185">
						<div class="form-group">
							<label style='font-size:12px;font-weight:700'>Received Reason</label><br>
							<p style='font-size:12px;font-weight:700'><?php 
							echo $del->reason; ?></p>
						</div>
					</td>
					<td>
						<div class="form-group">
							<label style='font-size:12px;font-weight:700'>Foto</label><br>
							
								<?php
								if($del->foto != ''){
									?> 
										<a target="#__Blank" href="<?php echo base_url() ?>assets/upload/return/<?php echo $del->foto; ?>"><img width="100" height="100" src="<?php echo base_url() ?>assets/upload/return/<?php echo $del->foto; ?>"></a>
									<?php
								}else{
									echo " - ";
								} 
							?>
						</div>
					</td>
				</tr>
			</table>
		

		

		<?php if($del->adt_reason != ''){ ?>
			<div class="form-group">
				<label style='font-size:12px;font-weight:700'>Additional Reason</label><br>
				<p class="unedit"><?php echo $del->adt_reason; ?></p>
				<input type="text" class="edit" style="width: 100%; height: 30px;display: none" id="truck_no" name="truck_no" value="<?php echo $del->id_truck; ?>">
				<input type="text" style="display: none" value="<?php echo $del->id_time_slot; ?>" name="id_time_slot">
			</div>
		<?php } ?>
		
		
		<form action="<?php echo base_url().'schedule/edit_schedule/'.$id; ?>" method="post" enctype="multipart/form-data">
			<table>
					<tr>
						<td width="185"><label style='font-size:12px;font-weight:700'>Truck No</label></td>
						<td><label style='font-size:12px;font-weight:700'>Driver Name</label></td>
					</tr>
					<tr>
						<td>
							<label class="unedit"><?php echo $del->id_truck; ?></label><input type="text" class="edit form-control" style="width: 100%; height: 30px;display: none" id="truck_no" name="truck_no" value="<?php echo $del->id_truck; ?>">

							<input type="text" style="display: none; text-align: left" class="form-control" value="<?php echo $del->id_time_slot; ?>" name="id_time_slot">
						</td>
						<td>
							<label class="unedit"><?php echo $del->driver_name; ?></label>
							<input type="text" class="edit form-control" style="width: 100%; height: 30px; display: none" id="driver_name" name="driver_name" value="<?php echo $del->driver_name; ?>">
						</td>
					</tr>
				</table>
				
				<div class="form-group">
					<label style='font-size:12px;font-weight:700'>Phone Number</label><br>
					<label class="unedit"><?php echo $del->phone_number; ?></label>
					<input type="text" class="edit form-control" style="width: 100%; height: 30px; display: none" id="phone_number" name="phone_number" value="<?php echo $del->phone_number; ?>">
				</div>

	        <!-- <div class="form-group edit" style="display: none">
	          <label style='font-size:12px;font-weight:700'>Delivery Note</label>
	          <div id="delivery_notes">
                  
                      <div class="input-group">                      
                          <input type="text" name="do_number1" class="form-control" required="">
                          <input type="file" name="file1" class="input-group-addon form-control" required="">
                          <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                      </div>
                  
                </div>
               <button type="button" style="margin:5px;margin-bottom:5px" class="btn btn-primary" onclick="addDN()"><span class="fa fa-plus"></span></button>
                <input type="text" style="display: none" id="dn_count" value="1" name="dn_count">
	        </div>
	            
	        <div class="form-group edit" style="display: none">
	          <div id="CoA">
                  <div class="form-group">
                    <label style='font-size:12px;font-weight:700'>Upload CoA</label>
                    <input type="file" name="coa1" class="form-control" required="">
                  </div>
                </div>
                <button type="button" style="margin:5px;margin-bottom:5px" class="btn btn-warning" onclick="addCOA()"><span class="fa fa-plus"></span></button>
                <input style="display: none" type="text" id="coa_count" value="1" name="coa_count">
	        </div> -->

	        <div class="edit" style="display: none">
	        	<button class="btn btn-primary edit">Update</button>
	    	</div>
	    </form>

		<!-- <div class="unedit">
			<table class="table table-bordered">
				<tr>

					<td style="white-space: nowrap;"><label style='font-size:12px;font-weight:700'>Delivery Note</label></td>
					<?php $i=0; foreach($dn_download->result() as $get){ 
												
						?>
					<td ><a href="<?php echo base_url().$get->invoice; ?>" class="btn btn-primary btn-sm" target="__Blank">
						<i class="fa fa-download"></i> 
						</a>
					</td>
				<?php $i++; } ?>
				</tr>
				<tr>
					<td style="white-space: nowrap;"><label style='font-size:12px;font-weight:700'>Coa File</label></td>
					<?php $i=0; foreach ($coa_download->result() as $get) {
					?>
					<td><a href="<?php echo base_url().$get->coa; ?>" class="btn btn-success btn-sm" target="__Blank">
						<i class="fa fa-download"></i> 
						</a>
					</td>
					<?php 
					} ?>
				</tr>
			</table>
		</div> -->

	</div>
</div>


	<div class="col-md-8" style="border-left: 1px solid #BDBDBD">

		<div class="form-group" <?php if($del->STATE == 0){echo 'hidden';} ?>>
			<a href="<?php echo base_url('schedule/receipt'); ?>?Schedule=<?php echo $del->id_schedule_group; ?>" target="_blank"><button class="btn btn-primary btn-sm" ><i class="fa fa-download"></i> Download Receipt</button></a>

			<button onclick="$('#dn_toggle').toggle()" class="btn btn-info btn-sm" target="__Blank">
						<i class="fa fa-download"></i> Download DN 
						</button>

			<button onclick="$('#coa_toggle').toggle()" href=""  class="btn btn-warning btn-sm" target="__Blank">
						<i class="fa fa-download"></i> Download COA 
				</button>

		</div>

		<div class="container-fluid" style="display: none" id="dn_toggle">
			<div class="row" style="border:1px solid;">
				<?php $i=0; foreach($dn_download->result() as $get){ ?>
					<div class="col-sm-1" style="margin:5px">
						<a href="<?php echo base_url().$get->invoice; ?>" class="btn btn-info btn-sm" target="__Blank">
							<i class="fa fa-download"></i> 
						</a>
					</div>
				<?php } ?>
			</div>
			<br>
		</div>

		<div class="container-fluid" style="display: none" id="coa_toggle">
			<div class="row" style="border:1px solid;">
				<?php $i=0; foreach($coa_download->result() as $get){ ?>
					<div class="col-sm-1" style="margin:5px">
						<a href="<?php echo base_url().$get->coa; ?>" class="btn btn-warning btn-sm" target="__Blank">
							<i class="fa fa-download"></i> 
						</a>
					</div>
				<?php } ?>
			</div>
			<br>
		</div>

		<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
           <thead style="color:white; background-color:#31559F;font-weight: 650; font-size:10px!important">
              <tr>
                <th style="font-size:12px;white-space:nowrap;" class="text-center">PO Number</th>
                <th style="font-size:12px;white-space:nowrap;" class="text-center">Material Code</th>
                <th style="font-size:12px;white-space:nowrap;" class="text-center">Material Name</th>
                <th style="font-size:12px;white-space:nowrap;" class="text-center">Qty</th>
                <th style="font-size:12px;white-space:nowrap;" width="35" class="text-center">Uom</th>
                <th style="font-size:12px;white-space:nowrap;" width="35" class="text-center">Req. Pallet</th>
                <th style="font-size:12px;white-space:nowrap;" class="text-center">Rdd / Shift</th>
                <th style="font-size:12px;white-space:nowrap;" class="text-center">Receive Amount</th>
                
              </tr>
            </thead>
            <tbody id="area-list">
              <?php
              foreach($sch_detail->result() as $get){
                ?>
              <tr>
                <td style="font-size:12px;white-space:nowrap;" class="text-center"><?php echo $get->po_number ?></td>
                <td style="font-size:12px;white-space:nowrap;"><?php echo $get->material_code ?></td>
                <td style="font-size:12px;white-space:nowrap;" width="200"><?php echo $get->material_name ?></td>
                <td style="font-size:12px;white-space:nowrap;">
                  <?php echo $get->SEND_AMOUNT; ?>
                </td>
                <td style="font-size:12px;white-space:nowrap; text-align: center"><?php echo $get->uom; ?></td>
                <td style="font-size:12px;white-space:nowrap;" width="25px" align="center"><?php echo ($get->receive_amount != 0)?$get->quantity-($get->quantity-$get->receive_amount):$get->quantity; ?>                	
                </td>
              	<td style="font-size:12px;white-space:nowrap;;white-space: nowrap" width="100"><?php echo $get->requested_delivery_date." / ".$get->shift; ?></td>
              	<td style="font-size:12px;white-space:nowrap;" class="text-center"><?php echo $get->receive_amount; ?></td>              	
              </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
      </div>
	</div>
</div>
