<style type="text/css">
	table th{
		background-color: #96b5ff!important;
		text-align: center;
		font-size:12px;
		justify-content: center;
  		flex-direction: column;
	}

table td 
{
	white-space: nowrap;
	margin: 0px;
	padding: 0px!important;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: middle;
    border-top: 1px solid #ddd;
}

.table-responsive{
	margin-top:-20px;
}

	input {
	font-size:12px;
    width: 100%;
    margin:0px;
    padding:0px;
    display: inline-table;
    
		}

}
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-file-invoice"></i>
                    </div>
                    <h4 class="card-title">Schedule Table </h4>
                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->

	<section class="content" style="padding:10px;">
		<div class="box">
		<div class="box-header">
		  

		</div>
		<div class="box-body" style="overflow: unset;">

		<div class="container-fluid">
			<div class="row" style="margin-left:-30px">
				<div class="col-md-6">               
					<a href="<?php echo base_url(); ?>schedule/schedule2?category=FACE&year=<?php echo $year; ?>&week=<?php echo $week; ?>" class="btn btn-info btn-sm"><i class="fa fa-arrow-left"></i> BACK</a>
					<button class="btn btn-primary btn-sm" onclick="sumbitForm();"><i class="fa fa-book"></i> SAVE</button>
					<button class="btn btn-success btn-sm" onclick="showAllInput();"><i class="fa fa-eye"></i> SHOW ALL</button>
					<img hidden="true" id="loading" src="<?php echo base_url()?>assets/images/loading.gif" style="width: 30px;height: 30px">
				</div>
			</div>
			<br>
			<?php
			if($this->session->flashdata('DELETED') != ""){
				?>
			<div class="alert alert-danger alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				<b><?php echo $this->session->flashdata('DELETED'); ?></b>
			</div>
				<?php
			}
			?>

			<?php
			if($this->session->flashdata('SAVED') != ""){
				?>
			<div class="alert alert-info" style="font-size:14px">
				 <b><?php echo $this->session->flashdata('SAVED'); ?> <a href="#" class="alert-link" onclick="filterInput('dash-green')">TAMPILKAN DATA</a></b>
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_ERROR') != ""){
				?>
			<div class="alert alert-danger" style="font-size:14px;color:black!important">
				Error! Perhatikan lagi data yang diupload dengan nomor PO <b><?php echo $this->session->flashdata('LIST_ERROR') ?> <a href="#" class="alert-link" onclick="filterInput('dash-red')" style="color:#245269!important">TAMPILKAN DATA</a></b>
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_NOACT') != ""){
				?>
			<div class="alert alert-warning" style="font-size:12px;color:black!important">
				Jadwal pengiriman dengan nomor PO <b><?php echo $this->session->flashdata('LIST_NOACT') ?></b> tidak berubah, material sedang dikirim / sudah diterima. <a href="#" style=";color:#245269!important" class="alert-link" onclick="filterInput('dash-yellow')">TAMPILKAN DATA</a>
			</div>
				<?php
			}
			?>
	
			<?php
			if($this->session->flashdata('LIST_DATA_LACK') != ""){
				?>
			<div class="alert alert-danger" style="font-size:12px;color:black!important">
				Material <b><?php echo $this->session->flashdata('LIST_DATA_LACK') ?></b> belum memiliki master data standar pallet, mohon penuhi terlebih dahulu <a href="#" class="alert-link" onclick="filterInput('dash-red')" style="color:#245269!important">TAMPILKAN DATA</a>
			</div>
				<?php
			}
			?>

			<?php
			if($this->session->flashdata('LIST_DATA_ORDER') != ""){
				?>
			<div class="alert alert-danger" style="font-size:12px;color:black!important">
				Material <b><?php echo $this->session->flashdata('LIST_DATA_LACK') ?></b> order quantity tidak sesuai dengan master data standar pallet, mohon untuk diperhatikan lagi data yang di upload <a href="#" class="alert-link" onclick="filterInput('dash-red')" style="color:#245269!important">TAMPILKAN DATA</a>
			</div>
				<?php
			}
			?>

		</div>
	</div>
		
		<div>
			<div class="container-fluid">
			
			
			<div class="row">
				<form id="idSchTbl" action="<?php echo base_url(); ?>schedule/save_schedule2?category=<?php echo $_GET['category'] ?>" method="post">
						
				
			<div class="table-responsive">
			<table border="0" id="schedule_table" class="table table-hover" style="">
				
					<tr>
								<th rowspan="2" width="30" style="vertical-align : middle;text-align:center;"><input <?php if($this->session->userdata('sess_role_no') == 3){ echo "style='display:none'";} ?> id="check_all" style="width: 15px;height: 15px" type="checkbox" name="check_all"></th>
								<th rowspan="2" width="40"><b disabled value="No" style="height: 50px;" />No</th>
								<th rowspan="2" width="45"><b disabled value="Category" style="height: 50px" />Category</th>
								<th rowspan="2" width="80"><b disabled value="Vendor/Supplier" style="height: 50px" />Vendor</th>
								<th rowspan="2" width="100"><b disabled value="Item Code" style="height: 50px;" />Item Code</th>
								<th rowspan="2" width="300"><b disabled value="Item Description" style="height: 50px;"/>Item Description</th>
								
								<th rowspan="2" width="60"><b disabled value="Plant" style="height: 50px"/>Plant</th>
								<th rowspan="2" width="100"><b disabled value="Purchase Order Doc." style="height: 50px"/>Purchase Order Doc.</th>
								<th rowspan="2" width="45"><b disabled value="Line" style="height: 50px"/>Line</th>
								<th rowspan="2" width="60"><b disabled value="Order Qty" style="height: 50px"/>Order Qty</th>
								<th rowspan="2" width="20"><b disabled value="UOM" style="height: 50px"/>UOM</th>
								<th colspan="2"><b disabled value="Revision Del.Plan" style="width: 260px;height: 25px"/>Revision Del.Plan</th>
								<th rowspan="2" width="60"><b disabled value="Qty per Pallet" style="height: 50px"/>Qty Per Pallet</th>
							</tr>
							<tr>
								<th width="110"><b disabled/>Date</th>
								<th width="40"><b disabled/>Shift</th>
							</tr>
				
				<tbody>
				
					<?php $row=0;
					$i=1;
					/*echo json_encode($arr_data); die();*/
					foreach ($arr_data as $get) {
					if($get['data_state'] == "1"){
						$class = "table-input dash-green";
					}else if($get['data_state'] == "2"){
						$class = "table-input dash-yellow";
					}else if($get['data_state'] == "3"){
						$class = "table-input dash-red";
					}else{
						$class = "";
					}
					 $col = 0;
					?>
				<tr class="<?php echo $class; ?>">
					<td style="text-align: center" width="30">
						<?php
						if($get['data_state'] == "1"){
							?>
						<i class="fa fa-check" style="color: #3CAE29"></i>
							<?php
						}else if($get['data_state'] == "2"){
							?>
						<i class="fa fa-exclamation-triangle" style="color: #F2F02A"></i>
							<?php
						}else if($get['data_state'] == "3"){
							?>
						<i class="fa fa-times" style="color: #F91D1D"></i>
							<?php
						}else{}
						?>
					</td>
					<td width="40"><input disabled value="<?php echo $i++; ?>" id="no<?php echo $row;?>"/></td>

					<td width="65"><input style="text-align:center" value="<?php echo $get['category']; ?>" class="inputz <?php echo $row; ?> " name="category[]" id="2_<?php echo $row; ?>" /></td>

					<td width="80"><input value="<?php echo $get['vendor']; ?>" class="inputz <?php echo $row; ?> "  name="vendor[]" id="3_<?php echo $row; ?>" /></td>

					<td width="100"><input value="<?php echo $get['item_code']; ?>" class="inputz <?php echo $row; ?> " name="item_code[]" id="4_<?php echo $row; ?>" /></td>

					<td width="300"><input value="<?php echo $get['material_name']; ?>" class="inputz <?php echo $row; ?> " name="material_name[]" id="5_row<?php echo $row; ?>col<?php echo $col++; ?>" /></td>

					

					<td width="60"><input value="<?php echo $get['plant']; ?>" class="inputz <?php echo $row; ?> " name="plan[]"  id="7_<?php echo $row; ?>" /></td>

					<td width="100"><input value="<?php echo $get['po_number']; ?>" class="inputz <?php echo $row; ?> " name="purchase_order[]" id="8_<?php echo $row; ?>" /></td>

					<td width="45"><input style="text-align: center!important;" value="<?php echo $get['line']; ?>" class="inputz <?php echo $row; ?> " name="line[]" id="9_<?php echo $row; ?>" /></td>

					<td width="60"><input value="<?php echo $get['order_qty']; ?>" class="inputz <?php echo $row; ?> " name="order_qty[]" id="10_<?php echo $row; ?>" /></td>

					<td width="35" align="center"><input style="text-align: center!important;" value="<?php echo $get['uom']; ?>" class="inputz <?php echo $row; ?> "name="uom[]" id="11_<?php echo $row; ?>" /></td>

					<td width="110"><input style="text-align:center" value="<?php echo Date('d-m-Y',strtotime($get['date'])); ?>"  class="inputz <?php echo $row; ?> " name="date[]" id="12_<?php echo $row; ?>"/></td>

					<td  width="40"><input style="text-align: center!important;" value="<?php echo $get['shift']; ?>" class="inputz <?php echo $row; ?> " name="shift[]" id="13_<?php echo $row; ?>"/></td>
					<td width="60"><input value="<?php echo $get['uom_plt']; ?>" class="inputz <?php echo $row; ?> " name="qty_per_palet[]" id="6_<?php echo $row; ?>" /></td>
					</form>
				</tr>
				<?php
			}
				?>
			</tbody>
		</table>
			</div>
		</div>
		</div>
	</div>

		<table id="header-fixed"></table>
		<img/><div id="json"></div>          
		</div>
	</section>


</div>
</div>
</div>
</div>
</div>
</section>
</div>
</div>