  <div class="table-responsive">
 <table class="table table-striped table-bordered table-hover">
           <thead style="color:white; background-color:#31559F;font-weight: 650; font-size:12px!important">
                              <tr>
                                <th class="text-center">PO Number</th>
                                <th class="text-center">Material Code</th>
                                
                                <th class="text-center">Del. Qty</th>
                                <th class="text-center">Uom</th>
                                <th class="text-center">Del. Pallet</th>
                                <th class="text-center">Rdd / Shift</th>
                                <th class="text-center">Received</th>
                                <th class="text-center" width="70">Outstanding</th>                
                              </tr>
                            </thead>
                            <tbody >
                              <?php
                              $no = 0;
                                  foreach($sch_detail->result() as $get){
                                    ?>
                                  <tr>
                                    <td style="font-size:12px" class="text-center">
                                        <input type="text" value="<?php echo $get->po_number; ?>" hidden name="po_number[]">
                                      <input type="text" value="<?php echo $get->po_line_item; ?>" hidden name="po_line_item[]">
                                      <input type="text" value="<?php echo $get->material_code; ?>" hidden name="material_code[]">
                                      <input type="text" value="<?php echo $get->id; ?>" hidden name="id_return[]"><?php echo $get->po_number ?></td>
                                    <td style="font-size:12px"><?php echo $get->material_code ?></td>
                                    
                                    <td style="font-size:12px" align="center">
                                      <?php echo $get->SEND_AMOUNT; ?>
                                    </td>
                                    <td style="font-size:12px" align="center"><?php echo $get->uom; ?></td>
                                    <td style="font-size:12px" width="25px" align="center"><?php echo $get->quantity ?></td>
                                    <td style="font-size:12px" width="100" align="center"><?php echo $get->requested_delivery_date." / ".$get->shift; ?></td>
                                    <td style="font-size:12px" align="center"><input style="border:1px solid" onkeyup="return_func(this.value,<?php echo $get->quantity ?>, <?php echo $no; ?>)" onclick="return_func(this.value,<?php echo $get->quantity ?>, <?php echo $no; ?>)" type="number" max="<?php echo $get->quantity;  ?>" min="0" name="received_amount[]" id="received_value" value="<?php echo $get->quantity ?>"></td>
                                    <td style="font-size:12px" align="center"><input type="text" name="return_value[]" id="return_value<?php echo $no++; ?>" readonly value="0"></b></td>
                                  </tr>
                                    <?php
                                  }
                                  ?>
                            </tbody>
                          </table>
                        </div>
             

