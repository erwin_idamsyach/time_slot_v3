<style type="text/css">
  th{
    white-space: nowrap;
  }

  td{
    white-space: nowrap;
  }

</style>
<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
           <thead style="color:white; background-color:#31559F;font-weight: 650; font-size:10px!important">
             <tr>
                <th class="text-center">PO Number</th>
                <th class="text-center">Material Code</th>
                <th class="text-center">Material Name</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Uom</th>
                <th class="text-center">Send. Pallet</th>
                <th class="text-center">Rdd / Shift</th>                
              </tr>
            </thead>
            <tbody id="area-list">
              <?php
              foreach($sch_detail->result() as $get){
                ?>
              <tr>
                <td class="text-center"><?php echo $get->po_number ?></td>
                <td align="center"><?php echo $get->material_code ?></td>
                <td><?php echo $get->material_name ?></td>
                <td align="center">
                  <?php echo $get->SEND_AMOUNT; ?>
                </td>
                <td align="center"><?php echo $get->uom; ?></td>
                <td width="25px" align="center"><?php echo $get->quantity ?></td>
                <td width="100" align="center"><?php echo $get->requested_delivery_date." / ".$get->shift; ?></td>
              </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
		  </div>
	</div>
</div>