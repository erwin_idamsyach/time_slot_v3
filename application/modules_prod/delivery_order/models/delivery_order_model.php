<?php

class delivery_order_model extends CI_Model{

	public function get_do($do){
		$data = $this->db->query("SELECT a.*, b.start_time, b.end_time, b.shift FROM tb_delivery_detail as a 
									 INNER JOIN ms_time_slot as b ON a.id_time_slot = b.slot_number
									 where do_number ='$do' ");

		return $data;
	}

	public function get_data_do($do){
		$data = $this->db->query("SELECT a.*, b.*, c.material_name from tb_scheduler as a
								  INNER JOIN tb_rds_detail as b ON a.id_schedule = b.id
								  INNER JOIN skin_master.ms_material as c ON c.material_sku = b.material_code
								  WHERE a.schedule_number = '$do'
								");

		return $data;

	}

	public function download_do(){
		date_default_timezone_set("Asia/Bangkok");
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle("Delivery Order");
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Download date : '.date('Y-m-d'));
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:C1');
		$this->excel->getActiveSheet()->mergeCells('A3:C3');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A3:B3')->getAlignment('Left')->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->setCellValue('A4', 'DO Number');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Delivery Date');
		$this->excel->getActiveSheet()->setCellValue('C4', 'Shift');
		$this->excel->getActiveSheet()->setCellValue('D4', 'Time Slot');
		$this->excel->getActiveSheet()->setCellValue('E4', 'ID Truck');
		$this->excel->getActiveSheet()->setCellValue('F4', 'Driver Name');
		$this->excel->getActiveSheet()->setCellValue('G4', 'Phone Number');
		$this->excel->getActiveSheet()->setCellValue('H4', 'Arrived');
		$this->excel->getActiveSheet()->setCellValue('I4', 'Received');		

		$getData = $this->db->query("SELECT a.*, b.start_time, b.end_time,b.shift FROM tb_delivery_detail as a
									 INNER JOIN ms_time_slot as b ON a.id_time_slot = b.slot_number ");
		$no=5;
		$no2=1;
		$no3=1;

		foreach($getData->result() as $data){

			$this->excel->getActiveSheet()->setCellValue('A'.$no, $data->do_number);
			$this->excel->getActiveSheet()->setCellValue('B'.$no, Date('Y-m-d',strtotime($data->delivery_date)));
			$this->excel->getActiveSheet()->setCellValue('C'.$no, $data->shift);
			$this->excel->getActiveSheet()->setCellValue('D'.$no, $data->start_time." - ".$data->end_time);
			$this->excel->getActiveSheet()->setCellValue('E'.$no, $data->id_truck);
			$this->excel->getActiveSheet()->setCellValue('F'.$no, $data->driver_name);
			$this->excel->getActiveSheet()->setCellValue('G'.$no, $data->phone_number);
			$this->excel->getActiveSheet()->setCellValue('H'.$no, $data->arrived);
			$this->excel->getActiveSheet()->setCellValue('I'.$no, $data->received);
			$no++;$no2++;

		}

		
		// $styleArray = array(
		//     'borders' => array(
  		//         'allborders' => array(
  		//             'style' => PHPExcel_Style_Border::BORDER_THIN
		//           )
		//       )
		//   );
			
		//$this->excel->getActiveSheet()->getStyle('A4:P'.(3))->applyFromArray($styleArray);
		$datenow = Date('Y-m-d');
		$filename='Deivery_order_'.$datenow.'.xls';
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');

	}

}

?>