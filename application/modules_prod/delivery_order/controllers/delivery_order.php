<?php
require_once APPPATH.'third_party/spout-master/src/Spout/Autoloader/autoload.php';

class delivery_order extends CI_Controller{
	function __Construct(){
		parent::__Construct();	
		$this->load->model('delivery_order_model');
		isLogin();	
	}

	public function index(){
		$getData = $this->db->query("SELECT * FROM tb_delivery_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.slot_number");
		
		$data['data'] = $getData;
		$data['sess_role'] = $this->session->userdata('sess_role_no');
		getHTML('delivery_order/index', $data);
	}

	public function detail_order(){

		$getDo = $this->input->get('Do');
		$getData_information = $this->delivery_order_model->get_do($getDo)->row();
		$sn = $getData_information->id_schedule_group;
		$getData_list = $this->delivery_order_model->get_data_do($sn);

		$data['data'] = $getData_information;
		$data['list'] = $getData_list->result();
		getHTML('delivery_order/detail_order', $data);
	}	  		

	public function download_do(){
		$this->delivery_order_model->download_do();
		$this->index();
	}

	public function ajax_search_do(){
		$getData = $this->input->get('DO');
		if($getData == null){
			$data = $this->db->query("SELECT * FROM tb_delivery_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.slot_number")->result();
		}else{
			$data = $this->db->query("SELECT * FROM tb_delivery_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.slot_number WHERE a.do_number LIKE '%$getData%' ")->result();
		}

		header('Content-Type: application/json');
		echo $output = json_encode($data);
	}
}
?>