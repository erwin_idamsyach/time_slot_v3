<script>
	
	
	$(document).ready(function() {
		var table;
 		
        //datatables
        table = $('#datatables').DataTable({ 
 			
            "processing": true, 
            "serverSide": true, 
            responsive: true,
			"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>',
            "order": [], 
             
            "ajax": {
                "url": "<?php echo base_url('master_data/get_data_master')?>",
                "type": "POST"
            },
            "columns":[
            	{"id":"no"},
            	{"data":"material_sku"},
            	{"data":"material_name"},
            	{"data":"material_type"},
            	{"data":"material_uom"},
            	{"data":"uom_pallet"},
            	
                ]
        
    });

});

	$("#table").DataTable({
		
		
	});


	function deletez(id){

		var confirmz = confirm("Apakah anda ingin menghapus data ini ?");

		if(confirmz){
			$.ajax({
			url: "<?php echo base_url('master_data/delete_data_material')  ?>",
			type: "POST",
			data: { 'id' :id },
		success: function(a){
			location.reload();
		},error: function(a){
			alert("Something Wrong");
			}

			});
			
		}
	}


	function openModal(id,status = 0){
		
		var material_code = '';
		var material_name = '';
		var material_type = '';
		var material_uom = '';
		var uom_pallet = '';
		var session = "<?php $this->session->userdata('sess_role_no'); ?>";
		if(status == 1){
		$.ajax({
			url: "<?php echo base_url('master_data/get_data_material')  ?>",
			type: "POST",
			data: { 'id' :id },
			dataType: "JSON",
		success: function(a){
			material_code = a[0].material_sku;
			material_name = a[0].material_name;
			material_type = a[0].material_type;
			material_uom = a[0].material_uom;
			uom_pallet = a[0].uom_pallet;
		},error: function(a){
			alert("Something Wrong");
		},async : false

		});
		
			$('.modal-title').html(material_code + ' - ' + material_name);
			$('#material_code').val(material_code);
			$('#material_name').val(material_name);
			$('#type').val(material_type);
			$('#uom').val(material_uom);
			$('#uom_pallet').val(uom_pallet);
			$('#button').attr('class', "btn btn-warning").html("Update");
			$('#idz').val(id);
			$('#openModal').click();
		}else{
			$('.modal-title').html("Add New Master");
			$('#material_code').val("");
			$('#material_name').val("");
			$('#type').val("");
			$('#uom').val("");
			$('#uom_pallet').val("");
			$('#button').attr('class', "btn btn-primary").html("Save");
		}
	}
	</script>