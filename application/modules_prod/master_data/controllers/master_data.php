<?php
require_once APPPATH.'third_party/spout-master/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;

class master_data extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('master_model');
        $this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		isLogin();
	}

	var $table = 'skin_master.ms_material';
    var $column_order = array(null, 'material_sku','material_name','material_type','material_uom','uom_pallet');
    var $column_search = array('material_sku','material_name','material_type','material_uom','uom_pallet');
    var $order = array('id' => 'asc');

		public function index(){
			$getMaterial = $this->db->query("SELECT * FROM skin_master.ms_material ORDER BY id DESC");
			$data['data_material'] = $getMaterial;
			getHTML('master_data/index', $data);
		}

		function get_data_master()
    {
        $list = $this->master_model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row["material_sku"] = $field->material_sku;
            $row["material_name"] = $field->material_name;
            $row["material_type"] = $field->material_type;
            $row["material_uom"] = $field->material_uom;
            $row["uom_pallet"] = $field->uom_pallet;
            $row["id"] = $field->id;
            $row["DT_Row_Index"] = $field->id;
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->master_model->count_all(),
            "recordsFiltered" => $this->master_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }

    public function get_data_material(){
    	$id = $this->input->post('id');
    	$material = $this->master_model->get_material($id);
    	echo json_encode($material);

    }

    public function download_excel()
    {
        $filePath = "./assets/templates/template_ms_material.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $getMaterial = $this->db->query("SELECT * FROM skin_master.ms_material ORDER BY id DESC")
            ->result();

        foreach ($getMaterial as $key => $value) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.($key + 2), $value->material_sku);
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.($key + 2), $value->material_name);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.($key + 2), $value->uom_pallet);
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.($key + 2), $value->material_uom);
        }

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="master_data.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');
    }

    public function download_template()
    {
        $filePath = "./assets/templates/template_ms_material.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="master_data_template.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');
    }

     public function add_material()
    {	
    	$material_code = $this->input->post('material_code');
    	$material_name = $this->input->post('material_name');
    	$material_type = $this->input->post('type');
    	$material_uom = $this->input->post('uom');
    	$uom_pallet = $this->input->post('uom_pallet');
    	$data = array(
    		"material_sku" 		=> $material_code,
    		"material_name" 	=> $material_name,
    		"material_type"		=> $material_type,
    		"material_uom"		=> $material_uom,
    		"uom_pallet"		=> $uom_pallet 
    	);

    	$this->db->insert('skin_master.ms_material',$data);
        $last_id = $this->db->query("SELECT id FROM skin_master.ms_material ORDER BY id DESC")->row()->id;
        $history = Array(
            "by_who"        => $this->session->userdata('sess_id'),
            "date"          => Date('Y-m-d'),
            "time"          => Date('H:i:s'),
            "action"        => "Add",
            "table_join"    => 'skin_master.ms_material',
            "id_join"       =>  $last_id,
            "description"   => "New Master Material, Material Code ".$material_code.", Material Name :".$material_name.", Material Type : ".$material_type.", UOM :".$material_uom." Uom Pallet :".$uom_pallet,
            "select_join"   => "id"
        );
        $this->db->insert('tb_history',$history);

        redirect('master_data');
    	
    }

    public function update_material()
    {	
    	$material_code 	= $this->input->post('material_code');
    	$material_name 	= $this->input->post('material_name');
    	$material_type 	= $this->input->post('type');
    	$material_uom 	= $this->input->post('uom');
    	$uom_pallet 	= $this->input->post('uom_pallet');
    	$idz 			= $this->input->post('idz');
    	$data = array(
    		"material_sku" 		=> $material_code,
    		"material_name" 	=> $material_name,
    		"material_type"		=> $material_type,
    		"material_uom"		=> $material_uom,
    		"uom_pallet"		=> $uom_pallet 
    	);
    	$this->db->where('id', $idz);
    	$this->db->update('skin_master.ms_material', $data);

         $last_id = $this->db->query("SELECT id FROM skin_master.ms_material WHERE id = '$idz' ")->row()->id;
        $history = Array(
            "by_who"        => $this->session->userdata('sess_id'),
            "date"          => Date('Y-m-d'),
            "time"          => Date('H:i:s'),
            "action"        => "Update",
            "table_join"    => 'skin_master.ms_material',
            "id_join"       =>  $last_id,
            "description"   => "New Master Material, Material Code ".$material_code.", Material Name :".$material_name.", Material Type : ".$material_type.", UOM :".$material_uom." Uom Pallet :".$uom_pallet,
            "select_join"   => "id"
        );
        $this->db->insert('tb_history',$history);
    	redirect('master_data');
    }

    public function delete_data_material(){

    	$id = $this->input->post('id');
        $data = $this->db->query("SELECT id,material_sku, material_name, material_type, material_uom, uom_pallet FROM skin_master.ms_material WHERE id = '$id' ")->row();
    	$this->db->query("DELETE FROM skin_master.ms_material WHERE id = '$id' ");
         $material_code = $data->material_sku;
         $material_name = $data->material_name;
         $material_type = $data->material_type;
         $material_uom = $data->material_uom;
         $uom_pallet  =  $data->uom_pallet;
        $history = Array(
            "by_who"        => $this->session->userdata('sess_id'),
            "date"          => Date('Y-m-d'),
            "time"          => Date('H:i:s'),
            "action"        => "Delete",
            "table_join"    => 'skin_master.ms_material',
            "id_join"       =>  $data->id,
            "description"   => " Master Material, Material Code ".$material_code.", Material Name :".$material_name.", Material Type : ".$material_type.", UOM :".$material_uom." Uom Pallet :".$uom_pallet,
            "select_join"   => "username"
        );
        $this->db->insert('tb_history',$history);
    	return true;
    }

    public function upload_master(){
        date_default_timezone_set("Asia/Bangkok");
        $file = $this->input->post('upload_master', TRUE);
        $arr_vl = array();
        $date_c='';
        $today = date('Y-m-d');
        $config['upload_path'] = './assets/upload/excel'; 
        $config['file_name'] = $file;
        $config['allowed_types'] = 'xlsx|csv|xls';
        $config['max_size'] = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('upload_master')) {
            echo $this->upload->display_errors();
            $this->session->set_flashdata('ERR', $this->upload->display_errors());
            //redirect('master_data?status=error_f');
        } else {
            $media = $this->upload->data();
            $inputFileName = '.\assets/upload/excel/'.$media['file_name'];
            $fn = $media['file_name'];
            $exp = explode(".", $fn);
            $ext = $exp[1];
            $sheetname = $exp[0];
           
            if($ext == "xlsx"){
                $reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
            }else if($ext == "xls"){
                $reader = ReaderFactory::create(Type::XLS); //set Type file xls
            }
            
            $no = 1;
            $reader->open($inputFileName);
            foreach ($reader->getSheetIterator() as $sheet) {
                // if($sheet->getName() == $sheetname){
                    foreach ($sheet->getRowIterator() as $rn => $get) {
                       if($rn > 1){ 
                            $check = $this->db->query("SELECT material_sku FROM skin_master.ms_material WHERE material_sku = '$get[0]' ")->num_rows();

                            $data = array(
                                "material_sku"  => $get[0],
                                "material_name" => $get[1],
                                "uom_pallet"    => $get[2],
                                "material_uom"  => $get[3],
                                "material_type" => "VERP"
                            );

                       if($check > 0){
                                $this->db->where('material_sku', $get[0]);
                                $this->db->update('skin_master.ms_material', $data);
                            }else{
                                $this->db->insert('skin_master.ms_material', $data);
                            }
                            
                            $check_material = $this->db->query("SELECT id, uom_plt, req_pallet, qty FROM tb_rds_detail WHERE requested_delivery_date >= '$today' AND material_code = '$get[0]' AND status='0'");
                            if($check_material->num_rows() > 0){
                                foreach($check_material->result() AS $geta ){
                                    $id = $geta->id;
                                    $req_pallet = $geta->req_pallet;
                                    $qty = $geta->qty;
                                    $uom_plt = $geta->uom_plt;
                                    $array = array(
                                        "uom_plt"       => $get[2],
                                        "req_pallet"    => $qty / $get[2]
                                        
                                    );
                                    
                                    $this->db->where('id', $geta->id);
                                    $this->db->update('tb_rds_detail', $array);

                                    $get_scheduler = $this->db->query("SELECT id, id_schedule, quantity, rdd FROM tb_scheduler WHERE id_schedule = $id");
                                    
                                    if($get_scheduler->num_rows() > 0){
                                        
                                        foreach($get_scheduler->result() as $d){
                                            $this->db->query("DELETE FROM tb_scheduler WHERE id_schedule=$d->id_schedule");
                                            $this->db->query("DELETE FROM tb_schedule_detail WHERE rdd='$d->rdd' AND status=0");
                                            $this->db->query("UPDATE tb_rds_detail SET is_schedule=0 WHERE id=$d->id_schedule");
                                        }
                                        $this->db->query("UPDATE ms_user SET data_checker =1 WHERE vendor_code != '' ");
                                    }

                                    
                                }
                            }
                            

                        }
                    }
                    $reader->close();

            }
            
        }
        
          $this->session->set_flashdata('SCS', 'Upload Success');
         //redirect('master_data?status=success');
    }

    public function test(){
        $today = date('Y-m-d');
        $check_material = $this->db->query("SELECT id, uom_plt, req_pallet, qty FROM tb_rds_detail WHERE requested_delivery_date >= '$today' AND material_code = '$get[0]' AND status='0'");
                            if($check_material->num_rows() > 0){
                                foreach($check_material->result() AS $geta ){
                                    $id = $geta->id;
                                    $req_pallet = $geta->req_pallet;
                                    $qty = $geta->qty;
                                    $uom_plt = $geta->uom_plt;
                                    $array = array(
                                        "uom_plt"       => $get[2],
                                        "req_pallet"    => $qty / $get[2]
                                        
                                    );
                                    
                                    $this->db->where('id', $geta->id);
                                    $this->db->update('tb_rds_detail', $array);

                                    $get_scheduler = $this->db->query("SELECT id, id_schedule, quantity FROM tb_scheduler WHERE id_schedule = $id");
                                    dump("disini nih");
                                    if($get_scheduler->num_rows() > 0){
                                        dump("wkwkkw");
                                        foreach($get_scheduler->result() as $d){
                                            $this->db->query("DELETE FROM tb_scheduler WHERE id_schedule=$d->id_schedule");
                                            $this->db->query("UPDATE tb_rds_detail SET is_schedule=0 WHERE id=$d->id_schedule");
                                        }
                                        $this->db->query("UPDATE ms_user SET data_checker =1 WHERE vendor_code != '' ");
                                    }

                                    
                                }
                            }
    }

}
?>