<?php

class Profile extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('Profile_Model');
		isLogin();
	}

	public function index(){
		if($this->input->get('id') != null){
			$id = $this->input->get('id');
		}else{
			$id = $this->session->userdata('sess_id');
		}
		$dataz = $this->Profile_Model->profile($id);
		$history_data = $this->Profile_Model->history($id);
		$data['data'] = $dataz;
		$data['history'] = $history_data;
		getHTML('profile/index',$data);
	}

	public function edit_profile(){
		$id = $this->session->userdata('sess_id');
		$data = $this->Profile_Model->profile($id);
		getHTML('profile/edit_profile',$data);
	}

	public function update_profile(){
		date_default_timezone_set("Asia/Bangkok");
		$id = $this->session->userdata('sess_id');
		$role = $this->session->userdata('sess_role_no');
		$rolename = $this->input->post('role_name');
		$category = $this->input->post('category');
		$username = $this->input->post('username');
		$email	  = $this->input->post('email');
		$phone_number = $this->input->post('phone_number');
		$name 			=$this->input->post('name');
		$password	 =$this->input->post('password');

		$foto = '';
	  	$config['allowed_types'] = 'jpg|jpeg|png|bmp';
	  	$config['max_size'] = 0;
		$config['upload_path'] = 'assets/upload/foto'; 
  		$config['file_name'] = $username;
  		$this->load->library('upload', $config);
	  	if ($this->upload->do_upload('foto')) {
	  		$get_foto = $this->db->query("SELECT foto FROM ms_user WHERE id = '$id' ")->row()->foto;
	  		@unlink("assets/upload/foto/".$get_foto);
	  		$data = $this->upload->data();
		  	$foto = $this->upload->data('file_name');
		  	
		}else{
		  	print_r($this->upload->display_errors());
		}
		
		$data = array(
			"username"		=> $username,			
			"password"		=> $password,
			"update_at"		=> date('Y-m-d H:i:s'),
			
		);
		if($foto != ''){
			$data['foto'] = $foto;
		}
		$this->db->where('id',$id);
		$this->db->update("ms_user", $data);

		$data = array(
			"date"			=> date('Y-m-d'),
			"time"			=> date('H:i:s'),
			"by_who"			=> $id,
			"action"		=> "Update",
			"table_join"	=> "ms_user",
			"id_join"		=> $id,
			"description"	=> "Profile",
			"value"			=> "",
			"value2"		=> "",
			"author"		=> 0
		);
		// dump($data);
		$this->db->insert('tb_history', $data);
		
		$data = $this->Profile_Model->profile($id);
		redirect('profile/edit_profile');
	}

	public function settings(){
		
		getHTML('profile/setting');
	}

}

?>