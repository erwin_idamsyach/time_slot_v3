<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
               <i class="fas fa-id-card" style="font-size:30px"></i>
                  </div>
                    <h4 class="card-title">Profile</h4>
                      </div>
                        <div class="card-body ">
                          <div class="row">
                            <div class="col-md-3">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Role</label>
                                <input disabled type="text" id="description" class="form-control" value="<?php echo $data->role_name; ?>">
                              </div>
                            </div>
                            
                            <div class="col-md-3">

                              <div <?php if($data->role != '2'){echo "hidden"; } ?> class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Category</label>
                                <input disabled type="text" id="description" class="form-control" value="<?php echo $data->category_name;   ?>">
                              </div>
                            
                            </div>
                            <div class="col-md-3">
                            </div>
                            <?php 
                            $foto = base_url()."assets/upload/foto/".$data->foto;
                            if(@getimagesize($foto)){
                               
                            }else{
                              $foto = base_url()."assets/images/dummy-profile.png";
                            }
                            ?>

                            <div class="col-md-3">
                              <img src="<?php echo $foto; ?>" width="150" height="150" style="position: absolute;">
                            </div>
                          </div>

                          <div id="vendor_form" <?php echo ($data->role != 3)?'hidden':'' ?>>
                          <div class="row">
                            <div class="col-md-3">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Vendor Code</label>
                                <input disabled type="text" id="description" class="form-control" value="<?php echo $data->vendor_code; ?>">
                            </div>
                            </div>

                            <div class="col-md-3">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Vendor Name</label>
                                <input disabled type="text" id="description" class="form-control" value="<?php echo $data->vendor_name; ?>">
                            </div>
                            </div>

                            <div class="col-md-2">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Vendor Alias</label>
                                <input  disabled type="text" id="description" class="form-control" value="<?php echo $data->vendor_alias; ?>">
                            </div>
                            </div>

                            <!-- <div class="col-md-1">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Truck</label>
                                <input  disabled type="text" id="description" class="form-control" value="<?php echo $data->no_truck; ?>">
                            </div>
                            </div> -->

                           </div>
                          </div>

                          <div class="row">
                            <div class="col-md-3">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Username</label>
                                  <input disabled type="text" id="description" class="form-control" value="<?php echo $data->username; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Name</label>
                                  <input disabled type="text" id="description" class="form-control" value="<?php echo $data->nama; ?>">
                                </div>
                            </div>
                            <div class="col-md-3">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Email</label>
                                <input disabled type="text" id="description" class="form-control" value="<?php echo $data->email; ?>">
                            </div>
                            </div>
                            
                          </div>

                          <div class="row">
                            <div class="col-md-3">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Phone Number</label>
                                <input disabled type="text" id="description" class="form-control" value="<?php echo $data->phone_number; ?>">
                            </div>
                            </div>
                            <?php if($this->session->userdata('sess_role_no') == '1'){ ?>
                            <div class="col-md-3">
                              <div class="form-group bmd-form-group">
                                <label class="bmd-label-floating">Password</label>
                                <input disabled type="text" class="form-control" value="<?php echo $data->password; ?>">
                            </div>
                            </div>
                          <?php } ?>
                          </div>

                          <hr>
                            <label style="color:white;text-align: center" class="btn btn-primary form-control">HISTORY</label>
                            <a style="" href="<?php echo base_url()?>history/download_history_profile?id=<?php if(isset($_GET['id'])){echo $_GET['id'];}else{ echo $this->session->userdata('sess_id');} ?>"><button class="btn btn-primary">Download History</button></a>
                            
                          <div class="row">
                            <div class="col-md-12">
                              <div class="table-responsive">
                                  <div class="table-responsive" style="margin-top:15px">
                                     <table id="table" class="table table-striped table-bordered table-hover data-table">
                                        <thead class="" style="color:white; background-color:#31559F;font-weight: 650">
                                      
                                        <tr style="text-align: center">
                                          <td >No</td>
                                          <td >Date</td>
                                          <td >Action Time</td>
                                          
                                          <td >Description</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                      <?php $a=1; foreach ($history as $get) {
                                
                               ?>
                              <tr>
                                <!-- <td><input value="<?php echo $get->id; ?>" type="checkbox" name="check_item"></td> -->
                                <td><?php echo $a++; ?></td>
                                <td width="100"><?php echo $get->date; ?></td>
                                <td><?php echo $get->time; ?></td>
                                <?php
                                 $history = '' ;
                                  $table_join = $get->table_join;
                                  $id_join = $get->id_join;
                                  $select_join = $get->select_join;
                                 
                                  if($get->action == 'Arrive' || $get->action == 'Received'){
                                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->user."</a>"."</b>"."&nbsp;"."&nbsp; <b style='color:green'>".$get->value.'</b>&nbsp;<b style="color:orange">'.$get->action."</b>&nbsp;".$get->description.'&nbsp'.$get->value2."</b>";
                                      $class = "fa fa-truck";
                                    }
                                    elseif($get->action == 'Update' || $get->action == 'Take Out' || $get->action == 'Take' ){
                                            $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->user."</a>"."</b>"."&nbsp;".$get->action."&nbsp;".$get->description."&nbsp; <b style='color:blue'>".$get->value.'&nbsp;'.$get->value2."</b>";        
                                            $class = "fa fa-edit";
                                    }else if($get->action == 'Return Vendor'){
                                      if($this->session->userdata('sess_role_no') != 3 ){
                                        $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->user."</a>"."</b>"."&nbsp;".$get->action."&nbsp;".$get->description."&nbsp; <b style='color:blue'> Actual Receive :".$get->value.'&nbsp;=> '.$get->value2."</b>";
                                      }else{
                                        
                                        if($vendor_code == $get->id_join){
                                          $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->user."</a>"."</b>"."&nbsp;".$get->action."&nbsp;".$get->description."&nbsp; <b style='color:blue'> Actual Receive :".$get->value.'&nbsp;=> '.$get->value2."</b>";
                                        }

                                      }
                                  
                                  }
                                    else{
                                      
                                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->user."</a>"."</b>"."&nbsp;".$get->action." ".$get->description;
                                      $class = "fa fa-user";
                                     }
                                 ?>
                                <td><?php echo $history; ?></td>
                              </tr>
                            <?php } ?>
                                    </tbody>
                                  </table>
                                
                              </div>
                            </div>
                          </div>
                          
                    </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>