<style type="text/css">
  img:hover{
    opacity: 0.5;
    filter: alpha(opacity=50);
  }

</style>
<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i class="fas fa-user-edit" style="font-size:30px"></i>
                    </div>
                    <h4 class="card-title">Edit Profile</h4>
                    </div>
                    <div class="card-body ">
                      <div class="container-fluid">
                        <form action="profile/update_profile" method="POST" enctype="multipart/form-data">
                        <div class="row">
                              <div class="col-md-3">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Role</label>
                                  <input  type="text" disabled name="role_name" id="description" class="form-control" value="<?php echo $role_name; ?>">
                              </div>
                              </div>

                              <div class="col-md-3" >
                                <div class="form-group bmd-form-group" <?php echo ($this->session->userdata('sess_role_no') != 2)?'hidden':'' ?>>
                                  <label class="bmd-label-floating">Role</label>
                                  <select id="category" disabled name="category" class="form-control select2-init" style="width: 100%;padding:50px;margin-top:15px!important;position: relative!important;">
                                      <option value="FACE">FACE</option>
                                          <option value="BODY">BODY</option>
                                          <option value="PWL">PWL</option>
                                          <option value="DEO">DEO</option>
                                      </select>
                                    </div>
                                 
                              </div>
                              <div class="col-md-3">
                              </div>

                              <div class="col-md-3">
                                <span id="add_plus" style="position: absolute;margin-top:25px;margin-left:76px;font-size:50px;color:green"><i class="fas fa-plus"></i></span>
                                <?php 
                          $id = $this->session->userdata('sess_id');
                          $get_foto = $this->db->query("SELECT foto FROM ms_user where id = '$id' ")->row()->foto;

                           ?>
                           <?php 
                            $foto = base_url()."assets/upload/foto/".$get_foto;
                            if(@getimagesize($foto)){
                               
                            }else{
                              $foto = base_url()."assets/images/dummy-profile.png";
                            }
                            ?>

                                            

                                <img id="foto" src="<?php echo $foto; ?>" onclick="$('#imagez').click()" onmouseout="$(this).css('opacity',1),$('#add_plus').css('opacity',1)" onmouseover="$(this).css('opacity',0.5),$('#add_plus').css('opacity',0.5)" src="#" width="200" height="200" style="position: absolute;margin-top:-50px">
                                <input hidden type="file" id="imagez" name="foto">
                              </div> 
                            </div>

                            
                            
                              <div disabled class="row" id="vendor_form" <?php echo ($this->session->userdata('sess_role_no') != 3)?'hidden':'' ?>>
                              <div class="col-md-3">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Vendor Code</label>
                                  <input  type="text" disabled name="vendor_code" id="description" class="form-control" value="<?php echo $vendor_code; ?>">
                              </div>
                              </div>

                              <div disabled class="col-md-3">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Vendor Name</label>
                                  <input  type="text" disabled name="vendor_name" id="description" class="form-control" value="<?php echo $vendor_name; ?>">
                              </div>
                              </div>

                              <div class="col-md-2">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Vendor Alias</label>
                                  <input  type="text" disabled name="vendor_alias" id="description" class="form-control" value="<?php echo $vendor_alias; ?>">
                              </div>
                              </div>

                              <!-- <div class="col-md-1">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">No Truck</label>
                                  <input  type="text" disabled name="vendor_alias" id="description" class="form-control" value="<?php echo $truck; ?>">
                              </div>
                              </div> -->

                             </div>
                            

                            <div class="row">

                              <div disabled class="col-md-3">
                                  <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Name</label>
                                    <input disabled type="text" name="name" id="description" class="form-control" value="<?php echo $nama; ?>">
                                  </div>
                              </div>

                              <div disabled class="col-md-3">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Email</label>
                                  <input disabled type="text" name="email" id="description" class="form-control" value="<?php echo $email; ?>">
                                </div>
                              </div>

                              <div  class="col-md-3">
                                <div class="form-group bmd-form-group">
                                  <label class="bmd-label-floating">Phone Number</label>
                                  <input disabled type="text" name="phone_number" id="description" class="form-control" value="<?php echo $phone_number; ?>">
                              </div>
                              </div>
                              
                            </div>

                              <div class="row">
                                <div class="col-md-3">
                                  <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Username</label>
                                    <input  type="text" name="username" id="description" class="form-control" value="<?php echo $username; ?>">
                                  </div>
                              </div>

                              <div class="col-md-3">
                                  <div class="form-group bmd-form-group">
                                    <label class="bmd-label-floating">Password</label>
                                    <input  type="text" name="password" id="description" class="form-control" value="<?php echo $password; ?>">
                                  </div>
                              </div>

                            </div>

                            <button class="btn btn-primary" type="submit">Save</button>
                          </form>
                        </div>
                        <br><br>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div

