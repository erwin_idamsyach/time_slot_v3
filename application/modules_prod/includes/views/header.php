<?php
define("APP_NAME", "Time Slot Management");
define("APP_SHORT_NAME", "TSB");
?>
<!DOCTYPE html>
<html>
<head>
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <title><?php echo APP_NAME; ?></title>
	  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/unilever.png" />
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	 <title><?php echo APP_NAME; ?></title>
	 <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/google/google_icon.css" />
	 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> -->
   <script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery.min.js"></script>
	 <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fa/css/fa.css">
	 <link href="<?php echo base_url(); ?>assets/plugins/material_dashboard/material-dashboard.css" rel="stylesheet" />
	 <link href="<?php echo base_url(); ?>assets/plugins/demo/demo.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/spin.css">
  <!-- DataTable -->


</head>

<body>


<!-- Modal -->
<div class="modal fade" id="modal-dynamic" tabindex="-1" style="z-index: 999999;" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#31559F;color:white!important">
        <h5 class="modal-title" id="area-modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="area-modal-content">
        
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        
      </div> -->
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="modal-dynamic" style="z-index: 9999;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#31559F;color:white!important">
        <button class="close" id="modal_close" data-dismiss="modal">
          <i class="fa fa-times" style="color: white !important"></i>
        </button>
        <h4 class="modal-title" id="area-modal-title"></h4>
      </div>
      <div class="modal-body" id="area-modal-content"></div>
    </div>
  </div>
</div> -->

<div class="se-pre-con"></div>

	<div class="wrapper">
