<?php 
$img = "assets/dist/img/admin.PNG";
?>
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url().$img ?>" class="img-circle img-responsive" style="width: 35px; height: 35px;" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('sess_nama'); ?></p>
          <i class="fa fa-circle text-success"></i> Online
        </div>
      </div>

      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li>
          <?php
          $year = date('Y');
          $week = date('W');
          $date = date('Y-m-d');
          ?>
          <a href="<?php date_default_timezone_set("Asia/Bangkok"); echo base_url() ?>schedule?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo $date; ?>">
            <i class="fa fa-dashboard"></i> <span>SCHEDULE DASHBOARD</span>
          </a>
        </li>
        <li <?php if($this->session->userdata('sess_role_no') != 1 && $this->session->userdata('sess_role_no') != 2  ){ echo "style='display: none'";} ?> >
          <a href="<?php echo base_url() ?>schedule/schedule2?category=FACE&year=<?php echo Date('Y'); ?>&week=<?php echo  Date('W'); ?>">
            <i class="fa fa-desktop"></i> <span>SCHEDULE TABLE</span>
          </a>
        </li>
        <li <?php echo ($this->session->userdata('sess_role_no') == 3)?'style="display: none"':'' ?>>
          <a href="<?php echo base_url() ?>slot">
            <i class="fa fa-calendar"></i> <span>RAW RDS DATA</span>
          </a>
        </li>
        <li <?php echo ($this->session->userdata('sess_role_no') == 3)?'style="display: none"':'' ?>>
          <a href="<?php echo base_url() ?>search/delivery_note">
            <i class="fa fa-calendar"></i> <span>SEARCH DN</span>
          </a>
        </li>
        <li <?php echo ($this->session->userdata('sess_role_no') == 3)?'style="display: none"':'' ?>>
          <a href="<?php echo base_url() ?>search/material">
            <i class="fa fa-calendar"></i> <span>SEARCH MATERIAL</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url() ?>outstanding">
            <i class="fa fa-file-o"></i> <span>OUTSTANDING PO</span>
          </a>
        </li>
        <li <?php echo ($this->session->userdata('sess_role_no') == 3 || $this->session->userdata('sess_role_no') == 4)?'style="display: none"':'' ?>>
          <a href="<?php echo base_url(); ?>user_management">
            <i class="fa fa-user-plus"></i> <span>USER MANAGEMENT</span>
          </a>
        </li>
        <li>
          <a href="<?php echo base_url() ?>login/logout">
            <i class="fa fa-sign-out"></i> <span>LOGOUT</span>
          </a>
        </li>
      </ul>
    
      <!-- MENU -->

    </section>
    <!-- /.sidebar -->
  </aside>