<?php 
$img = "assets/dist/img/admin.PNG";
?>
<style type="text/css">
  .main-panel>.content {
    margin-top: 40px;
    padding: 20px 0px;
    min-height: calc(100vh - 123px);
}

.sidebar .nav p {
    margin: 0;
    line-height: 15px;
    font-size: 13px;
    position: relative;
    display: block;
    height: auto;
    white-space: nowrap;
}

.sidebar .nav i {
    font-size: 20px;
    float: left;
    margin-right: 15px;
    line-height: 16px;
    width: 30px;
    text-align: center;
    color: #a9afbb;
}

</style>
<!-- sidebar -->
      <div class="sidebar" data-color="rose" data-background-color="black" data-image="<?php echo base_url()?>assets/images/unilever_bg2.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo text-center">
        
          <img src="<?php echo base_url().$img ?>" class="img-circle img-responsive" style="width: 55px; height: 55px;" alt="User Image">
        
        <a href="#" class="simple-text logo-normal">
          Time Slot Booking
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <?php 
          $id = $this->session->userdata('sess_id');
          $get_foto = $this->db->query("SELECT foto FROM ms_user where id = '$id' ")->row()->foto;

           ?>
          <?php 
                            $foto = base_url()."assets/upload/foto/".$get_foto;
                            if(@getimagesize($foto)){
                               
                            }else{
                              $foto = base_url()."assets/images/dummy-profile.png";
                            }
                            ?>

            <img src="<?php echo $foto; ?>" />
          </div>
          <!-- Informasi User -->
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
                <?php echo $this->session->userdata('sess_nama'); ?>
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>profile">
                    <span class="sidebar-mini"> MP </span>
                    <span class="sidebar-normal"> My Profile </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>profile/edit_profile">
                    <span class="sidebar-mini"> EP </span>
                    <span class="sidebar-normal"> Edit Profile </span>
                  </a>
                </li>
                <li class="nav-item" hidden>
                  <a class="nav-link" href="<?php echo base_url()?>profile/settings">
                    <span class="sidebar-mini"> S </span>
                    <span class="sidebar-normal"> Settings </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- End informasi User -->
        </div>

        <ul class="nav">
          <?php
          date_default_timezone_set("Asia/Bangkok");
          $year = date('Y');
          $week = date('W');
          $date = date('Y-m-d');
          ?>
          <li class="nav-item <?php if($this->uri->segment('1') == 'schedule' AND $this->uri->segment('2') == '' ){echo "active";}?>">
            <a class="nav-link" href="<?php  echo base_url() ?>schedule?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo $date; ?>">
              <i class="fas fa-th-large"></i>
              <p>Schedule Dashboard </p>
            </a>
          </li>

           <li <?php if($this->session->userdata('sess_role_no') != 1 && $this->session->userdata('sess_role_no') != 2  ){ echo "style='display: none'";} ?> class="nav-item <?php if($this->uri->segment('1') == 'schedule' AND $this->uri->segment('2') != '' AND $this->uri->segment('2') != 'view_schedule'){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>schedule/schedule2?category=FACE&year=<?php echo Date('Y'); ?>&week=<?php echo  Date('W'); ?>">
              <i class="fas fa-file-invoice"></i>
              <p>Schedule Table </p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment('1') == 'slot' AND $this->uri->segment('2') == '' ){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>slot?year=<?php echo $year; ?>&week=<?php echo $week;?>&category=">
              <i class="fas fa-boxes"></i>
              <p>RAW RDS Data</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment('1') == 'rds_plan' ){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>rds?year=<?php echo $year; ?>&week=<?php echo $week;?>&category=FACE&GET=Weekly">
              <i class="fas fa-th"></i>
              <p>RDS PLAN</p>
            </a>
          </li>


          <li <?php echo ($this->session->userdata('sess_role_no') == 3)?'style="display: none"':'' ?> class="nav-item <?php if($this->uri->segment('1') == 'master_data' ){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>master_data">
              <i class="fas fa-file-archive"></i>
              <p>Master Data</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment('1') == 'return_vendor' AND $this->uri->segment('2') == '' ){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>return_vendor?year=<?php echo date('Y'); ?>&week=<?php echo date('W'); ?>&category=FACE">
              <i class="fas fa-people-carry"></i>
              <p>Received Management</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment('1') == 'search'  AND $this->uri->segment('2') == 'delivery_note' ){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>search/delivery_note?week=<?php echo date('W'); ?>&year=<?php echo date('Y'); ?>&category=FACE">
              <i class="fas fa-shipping-fast"></i>
              <p>Search Receipt</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment('1') == 'search'  AND $this->uri->segment('2') == 'material' ){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>search/material?week=<?php echo date('W'); ?>&year=<?php echo date('Y'); ?>&category=FACE">
              <i class="fas fa-pallet"></i>
              <p>Search Material</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment('1') == 'outstanding'){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url() ?>outstanding?year=<?php echo $year; ?>&week=<?php echo $week;?>">
              <i class="fas fa-dolly-flatbed"></i>
              <p>Outstanding PO</p>
            </a>
          </li>

          <li <?php echo ($this->session->userdata('sess_role_no') != 1)?'style="display: none"':'' ?> class="nav-item <?php if($this->uri->segment('1') == 'user_management'){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url(); ?>user_management">
              <i class="fas fa-users-cog"></i>
              <p>User Management</p>
            </a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() ?>login/logout">
              <i class="fas fa-sign-out-alt"></i>
              <p>Log-Out</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- end sidebar -->

    <div class="main-panel">

        <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
               <i class="fas fa-ellipsis-h" style="font-size:18px"></i>
                
              </button>
            </div>
            <?php if( !empty($this->uri->segment('2')) && $this->uri->segment('1') != 'search' && $this->uri->segment('2') != 'schedule2' ){ ?>
            <div class="navbar-minimize">
              <button onclick='window.history.back()' id="minimizeSidebar" class="btn btn-just-icon btn-info btn-fab btn-round">
                <i class="material-icons text_align-center visible-on-sidebar-regular">arrow_back</i>               
              </button>
            </div>
          <?php } ?>
            <!-- <a class="navbar-brand" href="#pablo"></a> -->
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <!-- <span style="font-size:40px;color:blue;cursor: pointer;" onclick="history.back()"><i class="fas fa-arrow-circle-left"></i></span>
          <span style="font-size:40px;color:blue;cursor: pointer;" onclick="history.forward()"><i class="fas fa-arrow-circle-right"></i></span> -->
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form" style="display:none">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <style type="text/css">
                @media screen and (max-width:800px){
                  .statz{
                    display: none!important;
                  }
                }

                

              </style>
              <li class="nav-item statz">
                <a class="nav-link" href="<?php date_default_timezone_set("Asia/Bangkok"); echo base_url() ?>schedule?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo $date; ?>">
                  <i class="fas fa-th-large" style="font-size:18px"></i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <?php if($this->session->userdata('sess_role_no') != '' ){ ?> 
              <li class="nav-item dropdown">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php $data_history = history($this->session->userdata('sess_role_no'), $this->session->userdata('sess_vendor_code'),10, $this->session->userdata('sess_id')); 
                    
                    $data_count = history_count($this->session->userdata('sess_id'));
                  ?>
                  <i class="fas fa-bell" style="font-size:18px"></i>
                  <span class="notification"><?php echo $data_count; ?></span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                
                  <?php foreach ($data_history as $get) {
                    $history = '' ;
                    $table_join = $get->table_join;
                    $id_join = $get->id_join;
                    $select_join = $get->select_join;
                   
                    if($get->action == 'Arrive' || $get->action == 'Received'){
                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->username."</a>"."</b>"."&nbsp;"."&nbsp; ".$get->value.'&nbsp;<b style="color:orange">'.$get->action."</b>&nbsp;".$get->description.'&nbsp'.$get->value2."</b>";
                    }
                    elseif($get->action == 'Update' || $get->action == 'Take Out' || $get->action == 'Take' ){
                            $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->username."</a>"."</b>"."&nbsp;".$get->action."&nbsp;".$get->description."&nbsp; <b style='color:blue'>".$get->value.'&nbsp;'.$get->value2."</b>";        
                    }
                    else{
                      
                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->username."</a>"."</b>"."&nbsp;".$get->action." ".$get->description;
                    }

                    ?>
                  <b class="dropdown-item" href="#"><?php echo $history ?></b>
                  <?php } ?>
                  <hr style="padding:0px;margin:0px">
                  <a class="dropdown-item" href="<?php echo base_url() ?>history?year=<?php echo date('Y');?>&week=<?php echo date('W'); ?>" style="color:blue">Click Here to See All History</a>
                </div>
              </li>
            <?php } ?>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user" style="font-size:18px"></i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?php echo base_url()?>profile">Profile</a>
                  <a class="dropdown-item" href="<?php echo base_url()?>profile/edit_profile">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url()?>login/logout">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <script type="text/javascript">
          function open_menu(){
            
          }
      </script>