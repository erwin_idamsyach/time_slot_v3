<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url() ?>assets/dist/js/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url() ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/dist/js/demo.js"></script>

<!-- Sweet Alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="<?php echo base_url() ?>assets/dist/js/bootstrap-select.min.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- My Style -->
<script src="<?php echo base_url() ?>assets/dist/js/dropify.min.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/function-style_cms.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/liquidmetal.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/jquery.flexselect.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/style_cms.js"></script>
<link rel="manifest" href="<?php echo base_url(); ?>manifest.json" />
<script>
	$(document).ready(function(){
		$(".select2-init").select2();
	});
	$('table.data-table').dataTable();
	$("[data-toggle='tooltip']").tooltip({
		container : 'table'
	});
	$("[data-toggle='popover']").popover({
		html : true,
		container : 'body',
		viewport : {
			"x-index" : 100
		}
	});
	$("[data-toggle='popover']").on('show.bs.popover', function(){
		$("[data-toggle='popover']").not(this).popover('hide');
	});
	$("#datepicker").datepicker({
		autoclose : true
	});
	/*$("[data-toggle='popover']").popover({
		html : true,
		container : 'body',
		viewport : {
			"x-index" : 100
		}
	});

	$("[data-toggle='popover']").on('show.bs.popover', function(){
		$("[data-toggle='popover']").not(this).popover('hide');
	});

	$("#modal-dynamic").on('show.bs.modal', function(){
		$("[data-toggle='popover']").popover('hide');
	})*/

	function openModal(title, body_content, modal_size = "md"){
		$("#area-modal-title").html(title);
		$("#area-modal-content").html(body_content);

		if(modal_size == "sm"){
			$("#modal-dynamic .modal-dialog").addClass("modal-sm");
		}else if(modal_size == "lg"){
			$("#modal-dynamic .modal-dialog").addClass("modal-lg");
		}else{
			$("#modal-dynamic .modal-dialog").addClass("modal-md");
		}

		$("#modal-dynamic").modal('show');
	}
</script>
<?php
	$uri = $this->load->view('script');
?>
</body>
</html>