<?php 
define("APP_NAME", "Time Slot Management");
define("APP_SHORT_NAME", "TSB");
?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo APP_NAME ?></title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/unilever.png" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap4.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/skin-blue.css">

  <!-- My Css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style_cms.css">

  <!-- DataTable -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/bootstrap-select.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/dropify.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/flexselect.css">

</head>


<body class="hold-transition skin-blue sidebar-mini" id="body">

<div class="modal fade" id="modal-dynamic">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" data-dismiss="modal">
          <i class="fa fa-times" style="color: white !important"></i>
        </button>
        <h4 class="modal-title" id="area-modal-title"></h4>
      </div>
      <div class="modal-body" id="area-modal-content"></div>
    </div>
  </div>
</div>

<div class="se-pre-con"></div>

<div class="wrapper">

<?php 
  $img = '/assets/images/blank_profil.png';
?>
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url() ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><?php echo APP_SHORT_NAME ?></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg" style="text-align:center;"><b><?php echo APP_NAME ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url()."assets/dist/img/admin.PNG"; ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata('sess_nama'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url()."assets/dist/img/admin.PNG"; ?>" class="img-circle" alt="User Image">
                <p class="color-white">
                    <?php echo $this->session->userdata('sess_nama') ?>
                    <small><?php echo $this->session->userdata('sess_role_name') ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href=""><button class="btn btn-default btn-flat">Profile</button></a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url() ?>login/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>