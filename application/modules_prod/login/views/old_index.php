<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Time Slot Booking</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/unilever.png" />
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	  <!-- Bootstrap 3.3.6 -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
	  <!-- Ionicons -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/ionicons.min.css">
	  <!-- daterange picker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
	  <!-- bootstrap datepicker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
	  <!-- iCheck for checkboxes and radio inputs -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
	  <!-- Bootstrap Color Picker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
	  <!-- Bootstrap time Picker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
	  <!-- Select2 -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
	  <!-- AdminLTE Skins. Choose a skin from the css/skins
	       folder instead of downloading all of them to reduce the load. -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/skin-blue.css">

	  <!-- My Css -->
	  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style_cms.css"> -->

	  <!-- DataTable -->
	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">

	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/bootstrap-select.min.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/dropify.min.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/flexselect.css">
</head>
<body class="hold-transition login-page" style="background-image:url('assets/images/login_bg.jpg');background-repeat:no-repeat;background-size:100%">
	<div class="login-box" style="width: 350px;">
	  <!-- /.login-logo -->
	  <div class="login-box-body" style="margin-top: 50px;background-color:white;opacity: 1">
	    <p class="login-box-msg"></p>
	    <center> 
	    	<img src="<?php echo base_url() ?>assets/dist/img/admin.PNG" class="img-responsive" style="width: 100px; height:100px; background-color:transparent;">
	    </center>
	    <div class="login-logo" style="font-size:20px;margin-top:15px">
		    <b>Time Slot</b> Booking
		</div>
	    <form action="<?php echo base_url(); ?>login/function_login" method="post">
	      <div class="form-group has-feedback">
	        <input type="text" name="username" class="form-control" placeholder="Username">
	        <span class="glyphicon glyphicon-user form-control-feedback"></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input type="password" name="password" class="form-control" placeholder="Password">
	        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	      </div>
	      <div class="row">
	        <!-- /.col -->
	        <div class="col-xs-12">
	        	<?php
	        	if($this->session->flashdata('STATUS') == "ERROR"){
	        		?>
	        	<div class="alert alert-error">
	        		Username atau Password salah!
	        	</div>
	        		<?php
	        	}
	        	?>
	          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
	          <a href="<?php echo base_url(); ?>" type="button" class="btn btn-success btn-block btn-flat">
	          	<i class="fa fa-arrow-left"></i> Back To Homepage
	          </a>
	        </div>
	        <!-- /.col -->
	      </div>
	    </form>
	  </div>
	  <!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->
</body>
<footer style="position: absolute;bottom:0;width: 100%;height: 75px;background-color:blue;">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2" style="background-color:white;height: 75px">
				
				<p style="font-size:20px"><b><?php 
				date_default_timezone_set("Asia/Bangkok");
				$day = Date("D");
				$hari = '';
				if($day == 'Sun'){
					$hari = "Minggu";
				}else if($day == 'Mon'){
					$hari = "Senin";
				}else if($day == 'Tue'){
					$hari = "Selasa";
				}else if($day == 'Wed'){
					$hari = "Rabu";
				}else if($day == 'Thu'){
					$hari = "Kamis";
				}else if($day == 'Fri'){
					$hari = "Jumat";
				}else if($day == 'Sat'){
					$hari = "Sabtu";
				}
				echo $hari.", ";
				echo Date("d-m-Y"); 
				?></b>

				</p>
				
			<b><p id="jam" style="font-size:25px;margin: 0;margin-top:-10px" align="center"></p></b>
			
					
				

			</div>
			<div class="col-md-10" style="color:white;font-size:30px;margin-top:15px">
				<marquee behavior="scroll" direction="left">
		          <p><b>SELAMAT DATANG DI TIME SLOT BOOKING</b></p>
		        </marquee>
			</div>
		</div>
	</div>
</footer>

<script type="text/javascript">
	
	
	window.setTimeout("waktu()", 1000);

	function waktu() {
		var waktu = new Date();
		setTimeout("waktu()", 1000);
		document.getElementById("jam").innerHTML = waktu.getHours()+ ":"+waktu.getMinutes()+":"+waktu.getSeconds();
	}
</script>
</html>