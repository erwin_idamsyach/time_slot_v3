<?php

class Login extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		date_default_timezone_set("Asia/Bangkok");
		
	}

	public function index(){
		$date_now = date('Y-m-d');
		$data = array();
		$data['DM'] = $this->db->query("SELECT start_time, end_time, id FROM ms_time_slot WHERE shift ='DM' ORDER BY ordering ");
		$data['DP'] = $this->db->query("SELECT start_time, end_time, id FROM ms_time_slot WHERE shift ='DP' ORDER BY ordering ");
		$data['DS'] = $this->db->query("SELECT start_time, end_time, id FROM ms_time_slot WHERE shift ='DS' ORDER BY ordering ");
		
		$this->load->view('index',$data);
	}

	public function function_login(){
		$username = $this->db->escape_str($this->input->post('username'));
		$password = $this->db->escape_str($this->input->post('password'));
		$pass_md  = $password;
		/*echo $pass_md; die();*/
		$arr 	  = array();

		$cek_login = $this->db->query("SELECT ms_user.*, ms_role.role as ROLE_NAME, c.vendor_name as vendor_names, c.vendor_alias, c.email as vendor_email, c.phone_number as vendor_phone, c.vendor_category as vendor_category FROM ms_user INNER JOIN ms_role ON ms_user.role = ms_role.id LEFT JOIN skin_master.ms_supplier as c ON ms_user.vendor_code  = c.vendor_code WHERE username='$username' AND password='$pass_md'");
		if($cek_login->num_rows() == 1){
			$data = $cek_login->row();

			$nama = $data->nama;
			$username = $data->username;
			$role_no = $data->role;
			$role_name = $data->ROLE_NAME;

			$vendor_code = $data->vendor_code;
			$vendor_name = $data->nama;

			$array = array(
				"sess_id" => $data->id,
				"sess_nama" => $nama,
				"sess_user" => $username,
				"sess_role_no" => $role_no,
				"sess_role_name" => $role_name,
				"sess_vendor_code" => $vendor_code,
				"sess_vendor_name" => $vendor_name,
				"sess_vendor_alias" => $data->vendor_alias,
				"sess_foto"			=> $data->foto
				
			);
			$this->session->set_userdata($array);
			date_default_timezone_set("Asia/Bangkok");
			$year = date('Y');
          	$week = date('W');
          	$dt = date('Y-m-d');

			redirect('schedule?year='.$year.'&week='.$week."&date=".$dt);
			
		}else{
			$this->session->set_flashdata('STATUS', "ERROR");
			redirect('login');
		}

	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}

?>