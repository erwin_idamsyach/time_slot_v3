<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(! function_exists('getHTML')){
	function getHTML($view = "", $data = ""){
		$CI = &get_instance();
		$CI->load->view('includes/header', $data);
		$CI->load->view('includes/sidebar', $data);
		$CI->load->view($view, $data);
		$CI->load->view('includes/footer', $data);
	}
}


if(! function_exists('getUnplannedSlot')){
	function getUnplannedSlot($role = 1, $vendor_code = 0){
		$CI = &get_instance();
		if($role == 1 || $role == 2){
			$count = $CI->db->query("SELECT * FROM tb_rds_detail WHERE status='0' GROUP BY po_number")->num_rows();
		}else{
			$count = $CI->db->query("SELECT * FROM tb_rds_detail WHERE vendor_code='$vendor_code' AND status='0' GROUP BY po_number")->num_rows();
		}
		/*echo $role." ".$vendor_code;*/
		return $count;
	}
}

if(! function_exists('getButton')){
	function getButton($role = 1, $status = 0, $po_number, $id){
		$CI = &get_instance();
		$data['role_no'] = $role;
		$data['status'] = $status;
		$data['po_number'] = $po_number;
		$data['id'] = $id;
		$CI->load->view('button_setter', $data);
	}
}

if(! function_exists('truckIcon')){
	function truckIcon($doid = 1, $status = 0, $vendor = "SKL", $adt = 0, $tmat = 1,$other = 0, $no_police = '', $gr =''){
		/*
		0 --> NOT_PLANNED
		1 --> PLANNED
		2 --> ARRIVED
		3 --> RECEIVED
		*/
		$color = '';
		if($other == 1){
			$color = "color:#110e9b;";
		}else if($adt == 1 ){
			$color = "color:red";
		}else if($doid == 2 AND $other != 1){
			$color = "color:#FC46B6";
		}else{
			$color = "black";
		}
		if($no_police != '' && $other != 1){
			$show_police_number = "<b style='position:absolute;margin-top:15px;margin-left:-26px;font-weight:1000'>".$no_police."</b>";
		}else{
			$show_police_number = '';
		}

		if($gr != ''){
			$gr_color = '#36FA49';
			$gr_icon = "fa fa-check fa-stack-1x";
		}else{
			if($doid == 2 AND $other != 1){
				$gr_color = '#FFFFFF';
				$gr_icon = "fa fa-clock fa-stack-1x";
			}else{
				$gr_color = '#fb8c00';
				$gr_icon = "fa fa-clock fa-stack-1x";
			}
		}
		
		if($status == 0 && $adt == 1){
			$tmatz = '';
			// if($tmat == 0){
			// 	$tmatz = "opacity:0.5;";
			// }

			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:15px;'.$tmatz.';" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        
			      </div>'.$show_police_number;
		}else if($status == 1 && $adt == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:15px;" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-lock fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>'.$show_police_number;
		}else if($status == 2 && $adt == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:15px;" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-arrow-down fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>'.$show_police_number;	
		}else if($status == 3 && $adt == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:15px;" class="fa-stack fa-1x">
			        <i style="'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="'.$gr_icon.'" style="color: '.$gr_color.';margin-top:-3px"></i>
			      </div>'.$show_police_number;
		}else if($status == 1){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:15px;'.$color.'" class="fa-stack fa-1x">
			        <i style="color:'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-lock fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>'.$show_police_number;
		}else if($status == 2){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:15px;'.$color.'" class="fa-stack fa-1x">
			        <i style="color:'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="fa fa-arrow-down fa-stack-1x" style="color: #36FA49;margin-top:-3px"></i>
			      </div>'.$show_police_number;
			
		}else if($status == 3){
			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="margin-left:15px;'.$color.'" class="fa-stack fa-1x">
			        <i style="color:'.$color.'" class="fa fa-truck fa-3x fa-stack-1x"></i>
			        <i class="'.$gr_icon.'" style="color: '.$gr_color.';margin-top:-3px"></i>
			      </div>'.$show_police_number;

		}else{
			$tmatz = '';
			echo "<b style='position:absolute;margin-top:-17px;margin-left:15px;font-weight:800'>".$vendor."</b>".'<div style="'.$tmatz.'margin-left:15px;'.$color.'" class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-3x fa-stack-1x"></i>
			      </div>'.$show_police_number;

		}
	}
}

if(! function_exists('LegendIcon')){
	function LegendIcon($status = 0){
		/*
		0 --> NOT_PLANNED
		1 --> PLANNED
		2 --> ARRIVED
		3 --> RECEIVED
		*/
		if($status == 1){
			echo '<div class="fa-stack fa-1x text-center">
			        <i class="fa fa-truck fa-stack-2x"></i>
			        <i class="fa fa-lock fa-stack-1x" style="color: #36FA49;margin-left:-5px;margin-top:-2px"></i>
			      </div><br><b><p style="font-size:8px">PLANNED</p></b>';
		}else if($status == 2){
			echo '<div class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-stack-2x"></i>
			        <i class="fa fa-arrow-down fa-stack-1x" style="color: #36FA49;margin-left:-5px;margin-top:-3px"></i>
			      </div><br><b><p style="font-size:8px">ARRIVED</p></b>';	
		}else if($status == 3){
			echo '<div class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-stack-2x"></i>
			        <i class="fa fa-clock fa-stack-1x" style="color: #fb8c00;margin-left:-5px;margin-top:-2px"></i>
			      </div><br><b><p style="font-size:8px">PENDING GR</p></b>';
		}else if($status == 4){
			echo '<div class="fa-stack fa-1x">
			        <i class="fa fa-truck fa-stack-2x"></i>
			        <i class="fa fa-check fa-stack-1x" style="color: #36FA49;margin-left:-3px;margin-top:-2px"></i>
			      </div><br><b><p style="font-size:8px">RECEIVED</p></b>';
		}else if($status == 5){
			echo '<div class="fa-stack fa-1x text-center">
					<i style="color:red" class="fa fa-truck fa-2x"></i>
					</div><br><b><p style="font-size:8px">ADDITIONAL</p></b>';
		}else if($status == 6){
			echo '<div class="fa-stack fa-1x text-center">
					<i style="color:blue" class="fa fa-truck fa-2x"></i>
					</div><br><b><p style="font-size:8px">OTHER SUPPLIER</p></b>';
		}else if($status == 7){
			echo '<div class="fa-stack fa-1x text-center">
					<i style="color:#FC46B6" class="fa fa-truck fa-2x"></i>
					</div><br><b><p style="font-size:8px">CONTAIN ZARA 2 MATERIAL</p></b>';
		}else{
			echo '<div class="fa-stack fa-1x text-center">
					<i style="text-align:center" class="fa fa-truck fa-2x"></i>
					</div><br><b><p style="font-size:8px">NOT PLANNED</p></b>';
		}
	}
}

if(! function_exists('isLogin')){
	function isLogin(){
		$CI = &get_instance();
		if($CI->session->userdata('sess_role_no') != ""){
			return true;
		}else{
			redirect('login');
		}
	}
}


if(! function_exists('isRole')){
	function isRole(){
		$year = date('Y');
		$week = date('W');
		$date = date('Y-m-d');
		$CI = &get_instance();
		redirect('schedule?year='.$year.'&week='.$week.'&date='.$date);
	}
}

if(! function_exists('history')){

	function history($role = 0, $vendor_code = 0, $limit = 10, $id ='',$year='',$week=''){
		$CI = &get_instance();
				if($limit != 0){
					$cek_limit = "LIMIT $limit";
				}else{
					$cek_limit = '';
				}

				if($year != ''){
					$dto = new DateTime();
				  	$dto->setISODate($year, $week);
				  	$week_start = $dto->format('Y-m-d');
				  	$dto->modify('+6 days');
				  	$week_end = $dto->format('Y-m-d');
					$cek_date = "AND a.date BETWEEN '$week_start' AND '$week_end' ";
				}else{
					$cek_date ='';
				}
				$data = [];
				$check_menu = [];
				if(check_sub_menu(32)){
					$vendor_code ='';
				}else{
					$vendor_code = "AND a.vendor_code LIKE '%".$CI->session->userdata('sess_vendor_code')."%'";
				}

				if(check_sub_menu(28)){
					array_push($check_menu,1);
				}
				if(check_sub_menu(29)){
					array_push($check_menu,2);
				}
				if(check_sub_menu(30)){
					array_push($check_menu,3); 
				}
				
				if(check_sub_menu(31)){
					array_push($check_menu,4);
				}

				$author = implode(",", $check_menu);
				
				$data = $CI->db->query("SELECT a.*,b.username FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE a.author IN ($author) $vendor_code $cek_date ORDER BY a.id DESC $cek_limit")->result();				
				
				return $data;
			}
}

if(! function_exists('history_count')){
	
	function history_count($id){
			$CI = &get_instance();
			$id_user = $CI->session->userdata('sess_id');
			$vendor_code = $CI->session->userdata('sess_vendor_code');
			if($CI->session->userdata('sess_role_no') != '' ){
			$role = $CI->session->userdata('sess_role_no');
			$data = 0;
			if($role == 3){

				$data = $CI->db->query("SELECT a.id FROM tb_history AS a INNER JOIN tb_data_checker AS b WHERE a.id > b.last_history AND b.id_user = '$id_user' AND a.vendor_code = '$vendor_code' ")->num_rows();
			}
			else if(check_sub_menu(25)){
				$data = $CI->db->query("SELECT a.id FROM tb_history AS a INNER JOIN tb_data_checker AS b WHERE a.id > b.last_history AND b.id = '$id_user'")->num_rows();
			}else if(check_sub_menu(26)){
				$data = $CI->db->query("SELECT a.id FROM tb_history AS a INNER JOIN tb_data_checker AS b WHERE a.id > b.last_history AND b.id = '$id_user' AND a.author != 1 ")->num_rows();
			}else if(check_sub_menu(27)){
				$data = $CI->db->query("SELECT a.id FROM tb_history AS a INNER JOIN tb_data_checker AS b WHERE a.id > b.last_history AND b.id = '$id_user' AND a.author != 1")->num_rows();
			}else if(check_sub_menu(28)){
				$data = $CI->db->query("SELECT a.id FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id INNER JOIN skin_master.ms_supplier AS c ON b.vendor_code = c.vendor_code INNER JOIN tb_data_checker AS d ON b.id = d.id_user  WHERE a.id > d.last_history AND a.author = 3 AND a.vendor_code = '$vendor_code'")->num_rows();
			}
			return $data;
		}
	}
}

if(! function_exists('getShift')){
	function getShift(){
		$CI = &get_instance();
		$c_time = date('H:i:s');
		$getShift = $CI->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		
		return $getShift->shift;
	}
}

if(! function_exists('add_material_movement')){
	function add_material_movement($data){
		$CI = &get_instance();
		$data['created_at'] = date('Y-m-d h:i:s');
		$CI->db->insert('tb_material_movement',$data);
	}
}

if(! function_exists('sub_menu')){
	function sub_menu($menu){
		$CI = &get_instance();
		$data = $CI->db->query("SELECT a.*, b.menu FROM ms_sub_menu AS a INNER JOIN ms_menu AS b ON a.id_menu = b.id WHERE id_menu = '$menu' ");
		return $data;
	}
}

if(! function_exists('get_menu')){
	function get_menu(){
		$CI = &get_instance();
		$data = $CI->db->query("SELECT a.sub_menu, a.id, b.menu, b.icon, b.link, b.link_type FROM ms_sub_menu AS a INNER JOIN ms_menu AS b ON a.id_menu = b.id WHERE a.sub_menu = 'Menu' ")->result();
		return $data;
	}
}

if(! function_exists('check_sub_menu')){
	function check_sub_menu($id_menu){
		$CI = &get_instance();
		$role = $CI->session->userdata('sess_role_no');
		$data = $CI->db->query("SELECT menu FROM ms_role WHERE id = $role ")->row()->menu;
		$data = explode(",", $data);
		if(in_array($id_menu, $data)){
			return true;
		}else{
			return false;
		}
		
	}
}

if(!function_exists('changeUsage')){
	function changeUsage($po, $line, $dif, $status){
		$CI = &get_instance();
		$sap_db = $CI->load->database('sap', true);
		$cur_db = $sap_db->database;
		if($status == "MINUS"){
			$CI->db->query("UPDATE $cur_db.tb_po_subpo SET qty_acc = qty_acc-$dif WHERE po_number=$po AND item_number=$line");
		}else{
			$CI->db->query("UPDATE $cur_db.tb_po_subpo SET qty_acc = qty_acc+$dif WHERE po_number=$po AND item_number=$line");
		}
	}
}

if(!function_exists('getDateList')){
	function getDateList($week, $year) {
	  $dto = new DateTime();
	  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
	  $s = 1;
	  while($s <= 6){
	  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
	  	$s++;
	  }
	  return $ret;
	}
}

if(!function_exists('getDateListText')){
	function getDateListText($week, $year){
		$dto = new DateTime();
	  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
	  $s = 1;
	  while($s <= 6){
	  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
	  	$s++;
	  }
	  $ld = json_encode($ret);
	  $ld = str_replace("[", "", $ld);
	  $ld = str_replace("]", "", $ld);
	  return $ld;
	}
}

?>