<style type="text/css">
	td {
		white-space: nowrap;
	}

	.table>thead>tr>th,
	.table>tbody>tr>th,
	.table>tfoot>tr>th,
	.table>thead>tr>td,
	.table>tbody>tr>td,
	.table>tfoot>tr>td {
		padding: 0px 8px;
		/*font-size: 13px;*/
		vertical-align: middle;
		border-color: #ddd;
	}
</style>

<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover data-table" id="table" width="100%">
		<thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px;white-space: nowrap">
			<tr>
				<th style="font-size:14px" class="text-center">Category</th>
				<th style="font-size:14px" class="text-center">PO Number#</th>
				<th style="font-size:14px" class="text-center">Material Code</th>
				<th style="font-size:14px" class="text-center">Line</th>
				<th style="font-size:14px" class="text-center">Quantity</th>
				<th style="font-size:14px" class="text-center">Outstanding Qty</th>
				<th style="font-size:14px" class="text-center" width="100">Vendor Name</th>
				<th style="font-size:14px" class="text-center">RDD</th>
				<th style="font-size:14px" class="text-center">Shift</th>
				<th style="font-size:14px" class="text-center">Status</th>
				<th style="font-size:14px" class="text-center" width="50">Req. Qty (Pallet)</th>
				<th style="font-size:14px" class="text-center" width="50">Send (Pallet)</th>
				<th style="font-size:14px" class="text-center" width="10px">Outs. (Pallet)</th>
				<th style="font-size:14px">Created By</th>
				<th style="font-size:14px">Update By</th>
			</tr>
		</thead>
		<tbody>
			<?php
			foreach ($search_result->result() as $get) {
				$plt_kurang = $get->req_pallet - $get->receive_amount;
			?>
				<tr>
					<td style="font-size:12px"><?php echo $get->category; ?></td>
					<td style="font-size:12px"><?php echo $get->po_number; ?></td>
					<td style="font-size:12px"><?php echo $get->material_code; ?></td>
					<!-- <td><?php echo $get->material_name; ?></td> -->
					<td style="font-size:12px"><?php echo $get->po_line_item; ?></td>
					<td style="font-size:12px"><?php echo $get->qty; ?> T</td>
					<td style="font-size:12px" class="dataz"><?php echo $plt_kurang * $get->uom_plt;
																echo " " . $get->uom ?></td>
					<td style="font-size:12px"><?php echo $get->vendor_name . " (" . $get->vendor_alias . ")"; ?></td>
					<td style="font-size:12px">
						<?php
						$dc = date_create($get->requested_delivery_date);
						$df = date_format($dc, "D, d M Y");
						echo $df;
						?>
					</td>
					<td style="font-size:12px"><?php echo ($get->shift === null) ? "-" : $get->shift; ?></td>
					<td style="font-size:8px; text-align:left" ondblclick="getDetailedInformation('<?php echo $get->schedule_number ?>','<?php echo $get->C_STATUS; ?>')" class="text-center" style="font-size:11px">
						<div style="margin-left:-20px;">
							<?php truckIcon($get->dock_id, $get->C_STATUS, '', 0, 0, 0, 0, $get->gr_material_doc); ?>
						</div>
					</td>
					<td style="font-size:12px"><?php echo $get->req_pallet; ?></td>
					<td style="font-size:12px"><?php echo ($get->total === null) ? 0 : $get->total ?></td>
					<td style="font-size:12px" class="text-center dataz" align="center"><?php echo $plt_kurang ?></td>
					<?php if ($get->created_at == "0000-00-00 00:00:00") {
						$created_at = '';
					} else {
						$created_at = Date('d/m H:i', strtotime($get->created_at));
					} ?>
					<td style="font-size:12px" class="text-center dataz" width="60"><?php echo $get->created_name; ?><br><?php echo $created_at; ?></td>
					<?php if ($get->update_at == "0000-00-00 00:00:00") {
						$update_at = '';
					} else {
						$update_at = Date('d/m H:i', strtotime($get->update_at));
					} ?>
					<td style="font-size:12px" class="text-center dataz" width="60"><?php echo $get->edit_name ?><br><?php echo $update_at; ?></td>
				</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$("#table").DataTable({
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>',
		scrollX: true
	});
</script>