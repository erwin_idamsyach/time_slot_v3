<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i style="font-size:30px" class="fas fa-history"></i>
                    </div>
                    <h4 class="card-title">History</h4>
                    </div>
                    <div class="card-body ">
                      <div class="row">
                         <?php $year = $_GET['year']; $week = $_GET['week']; ?>
                    <div class="col-md-8">
                    <table>
                      <tr>
                        <td>
                          <div class="form-group">
                            <label style="margin-left:70px">Year</label>
                            <nav aria-label="Page navigation example">
                                <ul class="pagination pagination-primary">
                                  
                                  <li class="page-item">
                                    <a class="page-link" href="?year=<?php echo $year-1; ?>&week=52"><?php echo $year-1; ?></a>
                                  </li>
                                  
                                  <li class="page-item active">
                                    <a class="page-link " id='year' href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $year; ?></a>
                                  </li>

                                  <li class="page-item">
                                    <a class="page-link" href="?year=<?php echo $year+1; ?>&week=1"><?php echo $year+1; ?></a>
                                  </li>

                                </ul>
                              </nav>
                            </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <label style="margin-left:85px">Week</label>
                            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-primary" >
                <li class="page-item" <?php echo ($week < 3)?"style='display:none'":"" ?>>
                  <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>"><?php echo $week-2; ?></a>
                </li>
                 <li class="page-item" <?php echo ($week < 2)?"style='display:none'":"" ?>>
                  <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>"><?php echo $week-1; ?></a>
                </li>
                
                <li class="page-item active">
                  <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $week; ?></a>
                </li>
                <li class="page-item" <?php echo ($week == 52)?"style='display:none'":"" ?>>
                  <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>"><?php echo $week+1; ?></a>
                </li>
                <li class="page-item" <?php echo ($week == 52)?"style='display:none'":"" ?>>
                  <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>"><?php echo $week+2; ?></a>
                </li>
                </ul>
              </nav>

                            </div>
                        </td>
                      </tr>
                    </table>
                </div>
                    
                    <div class="col" style="text-align:right">
                      <div class="row">
                        <div class="col-6">
                          <input style="margin-top:30px" type="text" onchange="filter_download(this.value)" id="filter" class="form-control" placeholder="Filter download history" name="filter">
                        </div>
                        <div class="col">
                          <!-- <button class="btn btn-danger" onclick="check_delete()">Delete</button> -->
                          <a id="link" style="visibility: hidden;" href="<?php echo base_url()?>history/download_history?year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>"></a>
                          <button style="margin-top:30px" class="btn btn-primary" onclick="download_history()">Download History</button>
                        </div>
                        
                      </div>
                      
                    </div>

                      </div>

                      <div class="row">
                        <div class="table-responsive">
                         <div class="table-responsive" style="margin-top:15px">
                            <table id="table" class="table table-striped table-bordered table-hover data-table">
            <thead class="" style="color:white; background-color:#31559F;font-weight: 650">
                              <tr>
                                <!-- <th style="width: 25px"><input type="checkbox" name="checkall" id="checkAll"></th> -->
                                <th style="width: 30px">No</th>
                                <th style="width: 100px">Date</th>
                                <th style="width: 50px">Time</th>
                                <th>Description</th>

                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              if($this->input->get('year') == '2019' && $this->input->get('week') > 33 ){
                                $a=1; 
                                $data_history = history($this->session->userdata('sess_role_no'), $this->session->userdata('sess_vendor_code'),0, $this->session->userdata('sess_id'), $year,$week); 

                                 foreach ($data_history as $get) {
                                    $update = '';
                                    $history = '' ;
                                    $table_join = $get->table_join;
                                    $id_join = $get->id_join;
                                    $select_join = $get->select_join;
                                    
                                    if($get->action == 'Arrive' || $get->action == 'Received'){
                                      $history = "<a href='#' style='color:green;font-weight:600'>".$get->username."</a>"."&nbsp;<b style='color:rgb(234, 131, 4)'>".$get->action."</b>&nbsp;<b> Recipt: ".$get->value.", ".$get->description."<b style='color:rgb(234, 131, 4)'>&nbsp;".$update."</b><b style='color:rgb(234, 131, 4)'>".$get->value2."</b></b>";
                                    }
                                    else{
                                      if(!empty($get->value) && !empty($get->value2) ){
                                        $update = $get->value." => ".$get->value2;
                                      }
                                      $history = "<a href='#' style='color:green;font-weight:600'>".$get->username."</a>"."&nbsp;<b style='color:rgb(234, 131, 4)'>".$get->action."</b>&nbsp;<b>".$get->description."<b style='color:rgb(234, 131, 4)'>&nbsp;".$update."</b>"."</b>";
                                    }

                                    ?>
                                <tr>
                                  <!-- <td><input value="<?php echo $get->id; ?>" type="checkbox" name="check_item"></td> -->
                                  <td><?php echo $a++; ?></td>
                                  <td><?php echo $get->date; ?></td>
                                  <td><?php echo $get->time; ?></td>
                                  <td><?php echo $history; ?></td>
                                </tr>
                              <?php }
                                }else if($this->input->get('year') > '2019'){
                                $a=1; 
                                $data_history = history($this->session->userdata('sess_role_no'), $this->session->userdata('sess_vendor_code'),0, $this->session->userdata('sess_id'), $year,$week); 

                                 foreach ($data_history as $get) {
                                    $update = '';
                                    $history = '' ;
                                    $table_join = $get->table_join;
                                    $id_join = $get->id_join;
                                    $select_join = $get->select_join;
                                    
                                    if($get->action == 'Arrive' || $get->action == 'Received'){
                                      $history = "<a href='#' style='color:green;font-weight:600'>".$get->username."</a>"."&nbsp;<b style='color:rgb(234, 131, 4)'>".$get->action."</b>&nbsp;<b> Recipt: ".$get->value.", ".$get->description."<b style='color:rgb(234, 131, 4)'>&nbsp;".$update."</b><b style='color:rgb(234, 131, 4)'>".$get->value2."</b></b>";
                                    }
                                    else{
                                      if(!empty($get->value) && !empty($get->value2) ){
                                        $update = $get->value." => ".$get->value2;
                                      }
                                      $history = "<a href='#' style='color:green;font-weight:600'>".$get->username."</a>"."&nbsp;<b style='color:rgb(234, 131, 4)'>".$get->action."</b>&nbsp;<b>".$get->description."<b style='color:rgb(234, 131, 4)'>&nbsp;".$update."</b>"."</b>";
                                    }

                                    ?>
                                <tr>
                                  <!-- <td><input value="<?php echo $get->id; ?>" type="checkbox" name="check_item"></td> -->
                                  <td><?php echo $a++; ?></td>
                                  <td><?php echo $get->date; ?></td>
                                  <td><?php echo $get->time; ?></td>
                                  <td><?php echo $history; ?></td>
                                </tr>
                              <?php }
                                }
                              else{
                               ?>
                                
                               <?php

                if($segmen == 1){
                  $gofor = $data->result();
                  }else{
                  $gofor = $data;
                  }
                //dump($gofor);
                 $a=1;
                 foreach ($gofor as $get) {
                                
                               ?>
                              <tr>
                                <!-- <td><input value="<?php echo $get->id; ?>" type="checkbox" name="check_item"></td> -->
                                <td><?php echo $a++; ?></td>
                                <td><?php echo $get->date; ?></td>
                                <td><?php echo $get->time; ?></td>
                                <?php
                                 $history = '' ;
                                  $table_join = $get->table_join;
                                  $id_join = $get->id_join;
                                  $select_join = $get->select_join;
                                 
                                  if($get->action == 'Arrive' || $get->action == 'Received'){
                                    $history = '<b style="color:blue">'.$get->username.'</b> '."&nbsp; <b style='color:green'>".$get->value.'</b>&nbsp;<b style="color:orange">'.$get->action."</b>&nbsp;".$get->description.'&nbsp'.$get->value2."</b>";
                                  }
                                  elseif($get->action == 'Update' ){
                                          $history = '<b style="color:blue">'.$get->username.'</b> '.$get->action." ".$get->description." ".$get->value." ".$get->value2;        
                                  }
                                  else{
                                    $history = '<b style="color:blue">'.$get->username.'</b> '.$get->action." ".$get->description;
                                  }
                                 ?>
                                <td><?php echo $history; ?></td>
                              </tr>
                            <?php }
                            } ?>
                            </tbody>
                          </table>
                        </div>
                      
                    </div>
                         
                      </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>