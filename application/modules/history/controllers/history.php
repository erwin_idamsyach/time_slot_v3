<?php

class History extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('history_model');
		isLogin();
	}

	public function index(){
		
		if($this->input->get('year') == '2019' && $this->input->get('week') <= 33){
			$year = $this->input->get('year');
			$week = $this->input->get('week');
			$role = $this->session->userdata('sess_role_no');
			$id = $this->session->userdata('sess_id');
			$vendor_code = $this->session->userdata('sess_vendor_code');
			if($role == 1){
				$author = "";
			}else if($role == 2){
				$author = "WHERE author = '2' ";
			}else if($role == 3){
				$author = "WHERE author = '3' ";
			}else if($role == 4){
				$author = "WHERE author = '4' ";
			}

			if($role != 3){
				$get_last_id = $this->db->query("SELECT id FROM tb_history $author ORDER BY id DESC")->row();
			}else{
				//$get_last_id = $this->db->query("SELECT a.id FROM tb_history AS a INNER JOIN ")->row();
				$get_last_id = $this->db->query("SELECT a.id FROM tb_history AS a WHERE a.vendor_code = '$vendor_code' ORDER BY a.id DESC")->row();
			}

			if(!empty($get_last_id)){
				$last_id = $get_last_id->id;
				
				$this->db->where('id', $id);
				$this->db->update('ms_user', array("last_history" => $last_id));	 
			}
			$data['segmen'] = 1;
			$data['data'] = $this->history_model->get_all_history($role, $id, $week, $year);
		
		}else{
			$year = $this->input->get('year');
			$week = $this->input->get('week');
			$role = $this->session->userdata('sess_role_no');
			$id = $this->session->userdata('sess_id');
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$author = "";
			$cek = [];
			if(check_sub_menu(25)){
				array_push($cek,"1");
			}

			if(check_sub_menu(26)){
				array_push($cek,"2");
			}

			if(check_sub_menu(27)){
				array_push($cek,"3");
			}

			if(check_sub_menu(28)){
				array_push($cek,"4");
			}
			$cek = implode(",",$cek);

			if($role == 3){
				//$get_last_id = $this->db->query("SELECT a.id FROM tb_history AS a INNER JOIN ")->row();
				$get_last_id = $this->db->query("SELECT a.id FROM tb_history AS a WHERE a.vendor_code = '$vendor_code' ORDER BY a.id DESC")->row();
			}else if(check_sub_menu(32)){
				$get_last_id = $this->db->query("SELECT id FROM tb_history WHERE author IN ($cek) ORDER BY id DESC")->row();
			}

			if(!empty($get_last_id)){
				$last_id = $get_last_id->id;
				
				$this->db->where('id_user', $id);
				$this->db->update('tb_data_checker', array("last_history" => $last_id));	 
			}

			$data['segmen'] = 2;
			$data['data'] = history($this->session->userdata('sess_role_no'), $this->session->userdata('sess_vendor_code'),0, $this->session->userdata('sess_id'), $year, $week);	
		}
		
		
		getHTML('history/index', $data);
	}

	public function check_delete(){
		$data = $this->input->get('check_item');

		$no = 0;
		
			$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H-i-s'),
	  			"action"		=> "Delete",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_rds_detail",
	  			"select_join"	=> ""
	  	);
		
		foreach($data as $get){
			$check_item = $data[$no];
			$data_item = $check_item['check_item'];
			
			$this->db->query("DELETE FROM tb_history WHERE id = '$data_item' ");
			$no++;
		}
		
		return true;
	}

	public function sidebar_history(){
		$this->load->view('sidebar_history');
	}

	public function download_history(){
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$dto = new DateTime();
	  	$dto->setISODate($year, $week);
	  	$week_start = $dto->format('Y-m-d');
	  	$dto->modify('+6 days');
	  	$week_end = $dto->format('Y-m-d');
	  	$filter = $this->input->get('filter');
	  	if($filter != ''){
	  		$filterz = "AND (a.description LIKE '%Add%' OR a.action LIKE '%Add%' OR a.value LIKE '%%' OR a.value2 LIKE '%%')";
	  	}else{
	  		$filterz = "";
	  	}
		$vendor_code = $this->session->userdata('sess_vendor_code');
		if($vendor_code == ''){
			$filter_vendor = '';
		}else{
			$filter_vendor = "a.vendor_code = '$vendor_code' AND ";
		}

		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('wawa');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Download date : '.date('Y-m-d H:i:s'));
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		$this->excel->getActiveSheet()->mergeCells('A3:C3');

		//$this->excel->getActiveSheet()->setCellValue('A4', 'No');
		//$this->excel->getActiveSheet()->setCellValue('A4', 'No');

		$this->excel->getActiveSheet()->setCellValue('A4', 'No');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Date');
		$this->excel->getActiveSheet()->setCellValue('C4', 'Time');
		$this->excel->getActiveSheet()->setCellValue('D4', 'Description');
		$no = 5;
		$data = $this->db->query("SELECT a.action,a.date,a.time, a.description,a.table_join,a.id_join,a.select_join,a.select_join2, a.value, a.value2,b.username FROM tb_history as a LEFT JOIN ms_user as b ON a.by_who = b.id where $filter_vendor  a.date BETWEEN '$week_start' AND '$week_end' $filterz ORDER BY a.id desc");
		foreach ($data->result() as $get) {
		$this->excel->getActiveSheet()->setCellValue('A'.$no,($no-4));
		$this->excel->getActiveSheet()->setCellValue('B'.$no,$get->date);
		$this->excel->getActiveSheet()->setCellValue('C'.$no,$get->time);
		$history = '' ;
        $select_join = $get->select_join;
		 if($get->action == 'Delete'){
             $history = ''.$get->username.''.$get->action." ".$get->description;
          }
          else if($get->action == 'Arrive' || $get->action == 'Received'){
            $history = ''.$get->username.''." ".$get->value.' '.$get->action." ".$get->description.''.$get->value2."";
          }
          elseif($get->action == 'Update' ){
                  $history = ''.$get->username.' '.$get->action." ".$get->description." ".$get->value." ".$get->value2;        
          }
          else{
            $history = ' '.$get->username.' '.$get->action." ".$get->description;
          }

		$this->excel->getActiveSheet()->setCellValue('D'.$no, $history);

		$no++;
		}
		
		$styleArray = array(
      	'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
		          )
		      )
		 );

		foreach (range('A', $this->excel->getActiveSheet()->getHighestDataColumn()) as $col) {
	    $this->excel->getActiveSheet()
	             ->getColumnDimension($col)
	             ->setAutoSize(true);
	    }

	    $this->excel->getActiveSheet()->getStyle('A4:D'.($no))->applyFromArray($styleArray);

		$filename='History_W'.$week.'_Y'.$year.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

	public function download_history_profile(){
		$year =date('Y');
		$week = date('W');
		$id = $this->input->get('id');
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('wawa');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Download date : '.date('Y-m-d H:i:s'));
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(12);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		$this->excel->getActiveSheet()->mergeCells('A3:C3');

		//$this->excel->getActiveSheet()->setCellValue('A4', 'No');
		//$this->excel->getActiveSheet()->setCellValue('A4', 'No');

		$this->excel->getActiveSheet()->setCellValue('A4', 'No');
		$this->excel->getActiveSheet()->setCellValue('B4', 'Date');
		$this->excel->getActiveSheet()->setCellValue('C4', 'Action Time');
		$this->excel->getActiveSheet()->setCellValue('D4', 'Description');
		$no = 5;
		$data = $this->db->query("SELECT a.*,b.username FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE a.by_who = '$id' ORDER BY a.id desc");
		
		foreach ($data->result() as $get) {
		$this->excel->getActiveSheet()->setCellValue('A'.$no,($no-4));
		$this->excel->getActiveSheet()->setCellValue('B'.$no,$get->date);
		$this->excel->getActiveSheet()->setCellValue('C'.$no,$get->time);
		$history = '' ;
        $select_join = $get->select_join;
								 if($get->action == 'Arrive' || $get->action == 'Received'){
			                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , "."".$get->username." ".$get->value.' '.$get->action." ".$get->description.' '.$get->value2." ";
			                      
			                    }
			                    elseif($get->action == 'Update' || $get->action == 'Take Out' || $get->action == 'Take' ){
			                            $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." ,"." ".$get->username." ".$get->action." ".$get->description." ".$get->value.' '.$get->value2;        
			                            
			                    }
			                    else{
			                      
			                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , ".$get->username." ".$get->action." ".$get->description;
			                      
			                     }

		$this->excel->getActiveSheet()->setCellValue('D'.$no, $history);

		$no++;
		}
		
		$styleArray = array(
      	'borders' => array(
          'allborders' => array(
              'style' => PHPExcel_Style_Border::BORDER_THIN
		          )
		      )
		 );

		foreach (range('A', $this->excel->getActiveSheet()->getHighestDataColumn()) as $col) {
	    $this->excel->getActiveSheet()
	             ->getColumnDimension($col)
	             ->setAutoSize(true);
	    }

	    $this->excel->getActiveSheet()->getStyle('A4:D'.($no))->applyFromArray($styleArray);

		$filename='History_W'.$week.'_Y'.$year.'.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}

}

?>