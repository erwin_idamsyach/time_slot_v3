<script type="text/javascript">
	function searchDo(a){
		$.ajax({
			type : "GET",
			url  : "<?php echo base_url() ?>delivery_order/ajax_search_do",
			data : {
				'DO' : a
			},
			dataType: 'json',
			success:function(b){
				tableDo(b);
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}

	function tableDo(a){
		$('tr td').remove();
		var no=1;
		var arrived= '';
		var received='';
		for(i=0;i<a.length;i++){
			if(a[i].arrived == null){
				arrived = '-';
			}else{
				arrived = a[i].arrived;
			}

			if(a[i].received == null){
				received = '-';
			}else{
				received = a[i].received;
			}

			var table = "<tr><td align='center'>"+no+"</td><td>"+a[i].do_number+"</td><td align='center'>"+a[i].delivery_date.substring(0,10)+"</td><td align='center'>"+a[i].shift+"</td><td align='center'>"+a[i].start_time+" - "+a[i].end_time+"</td><td>"+a[i].id_truck+"</td><td>"+a[i].driver_name+"</td><td>"+a[i].phone_number+"</td><td align='center'>"+arrived+"</td><td align='center'>"+received+"</td><td><a href='delivery_order/detail_order?Do="+a[i].do_number+"'><span class='btn btn-primary'><i class='fa fa-eye'></i></span></a></td></tr>";
			$('#tabel_body').append(table);
		}
		
		
	}
</script>