<div class="content-wrapper" style="background:#EEEEEE;">
	<section class="content-header">
    <h1>
        Delivery Order
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
    	<div class="box">
        <div class="box-header">
          <label>Time Slotting</label>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-3" >
             
                  <div class="input-group">
                    <input type="text" onkeyup="searchDo($('#search_do').val())" class="form-control" id="search_do" placeholder="Search DO Number">
                    <div class="input-group-btn">
                      <span class="btn btn-primary form-control" onclick="searchDo($('#search_do').val())" type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                      </span>
                    </div>
                  </div>
            </div>
            <div class="col-md-9" >
              <div class="form-group pull-right">
                <a href="delivery_order/download_do"><button class="btn btn-success"><span class="fa fa-download"></span> Download DO</button></a>
              </div>
            </div>

          </div>
          <table class="table table-bordered table-bordered table-striped data-table" style="margin-top: 10px;">
            <thead>
              <tr>
                <th class="text-center">No</th>
                <th class="text-center">Do Number</th>
                <th class="text-center">Delivery Date</th>
                <th class="text-center" width="30">Shift</th>
                <th class="text-center">Time Slot</th>
                <th class="text-center">Id Truck</th>
                <th class="text-right">Driver Name</th>
                <th class="text-right">Phone Number</th>
                <th class="text-center">Arrived</th>
                <th class="text-center">Received</th>
                <th class="text-center">Action</th>
              </tr>
            </thead>
            <div class="tbody">
              <tbody id="tabel_body">
          
                   <?php $a = 1;
                   foreach ($data->result() as $get) {
                   	?>
                   	<tr>
                      <td align="center"><?php echo $a++; ?></td>
                      <td><?php echo $get->do_number; ?></td>
                   		<td align="center"><?php echo Date('Y-m-d',strtotime($get->delivery_date)); ?></td>
                   		<td align="center"><?php echo $get->shift; ?></td>
                   		<td align="center"><?php echo $get->start_time." - ".$get->end_time; ?></td>
                   		<td><?php echo $get->id_truck; ?></td>            		
                   		<td><?php echo $get->driver_name; ?></td>
                   		<td><?php echo $get->phone_number; ?></td>
                   		<td><?php echo $get->arrived; ?></td>
                      <td><?php echo $get->received; ?></td>
                      <td><a href="delivery_order/detail_order?Do=<?php echo $get->do_number; ?>"><span class="btn btn-primary"><i class="fa fa-eye"></i></span></a></td>			
                   	</tr>
                   <?php }
                   ?>
                 
              </tbody>
            </div>
          </table>
        </div>
      </div>
    </section>
</div>