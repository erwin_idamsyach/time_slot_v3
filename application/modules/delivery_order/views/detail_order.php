
<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
      <a href="<?php echo base_url()."delivery_order" ?>" class="btn btn-info">
        <i class="fa fa-arrow-left"></i> BACK
      </a><br><br>
        DETAIL DO : <?php if(isset($data->arrived)){echo "Arrived : ".$data->arrived;}elseif(isset($data->received)){echo "Received : ".$data->received;}else{echo "Planned";} ?>
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
      <div class="row">
        <div class="col-md-8">
          <div class="box">
            <div class="box-header">INFORMATION</div>
            <div class="box-body">
              <table border='1' class="table table-bordered">
                <tr>
                  <th>DO Number#</th>
                  <td><?php echo $data->do_number; ?></td>
                </tr>
                <tr>
                  <th>Request Del.Date</th>
                  <td><?php echo substr($data->delivery_date,0,-9); ?></td>
                </tr>
                <tr>
                  <th>Shift</th>
                  <td><?php echo $data->shift; ?></td>
                </tr>
                <tr>
                  <th>Time Slot</th>
                  <td><?php echo $data->start_time." - ".$data->end_time; ?></td>
                </tr>
                <tr>
                  <th>Arrived</th>
                  <td><?php echo $data->arrived; ?></td>
                </tr>
                <tr>
                  <th>Received</th>
                  <td><?php echo $data->received; ?></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="box">
        <div class="box-header">ITEM LIST</div>
        <div class="box-body">
          <div class="table responsive">
              <table class="table table-bordered table-bordered table-striped data-table">
                <thead>
                  <tr>
                    <th class="text-center" width="10">No</th>
                    <th class="text-center">PO Number</th>
                    <th class="text-center">Material Code</th>
                    <th class="text-center">Material Description</th>
                    <th Class="text-center">Vendor Code</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">UOM</th>
                    <th class="text-center">UOM Plt</th>
                    <th class="text-center">Plt Truck</th>
                   
                  </tr>
                </thead>
                <tbody>
              <?php $no=1; foreach($list as $data){ ?>
                <tr>
                  <td align="center"><?php echo $no; ?></td>
                  <td align="center"><?php echo $data->po_number; ?></td>
                  <td align="center"><?php echo $data->material_code; ?></td>
                  <td><?php echo $data->material_name; ?></td>
                  <td><?php echo $data->vendor_code; ?></td>
                  <td><?php echo $data->qty; ?></td>
                  <td><?php echo $data->uom; ?></td>
                  <td><?php echo $data->uom_plt; ?></td>
                  <td align="center"><?php echo $data->plt_truck; ?></td>
                  
                </tr> 
                  <?php $no++; } ?>
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </section>
</div>