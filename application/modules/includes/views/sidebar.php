<?php 
$img = "assets/dist/img/admin.PNG";
?>
<style type="text/css">
  .main-panel>.content {
    margin-top: 30px;
    padding: 20px 0px;
    min-height: calc(100vh - 123px);
}

.sidebar .sidebar-wrapper>.nav [data-toggle="collapse"]~div>ul>li>a .sidebar-mini, .sidebar .sidebar-wrapper .user .user-info [data-toggle="collapse"]~div>ul>li>a .sidebar-mini {
    text-transform: uppercase;
    width: 30px;
    margin-right: 40px;
    text-align: center;
    letter-spacing: 1px;
    position: relative;
    float: left;
    display: inherit;
}

.sidebar .sidebar-wrapper>.nav [data-toggle="collapse"]~div>ul>li>a i, .sidebar .sidebar-wrapper .user .user-info [data-toggle="collapse"]~div>ul>li>a i {
    font-size: 17px;
    line-height: 22px;
    width: 100px;
}

.sidebar .logo {
    padding: 5px 0px;
    margin: 0;
    display: block;
    position: relative;
    z-index: 4;
}

.sidebar .user {
    padding-bottom: 10px;
    margin: 5px auto 0;
    position: relative;
}

.sidebar .nav p {
    margin: 0;
    line-height: 8px;
    font-size: 12px;
    position: relative;
    display: block;
    height: auto;
    white-space: nowrap;
}

.sidebar .nav {
    margin-top: 0px;
    display: block;
}

.sidebar .nav i {
    font-size: 14px;
    float: left;
    margin-right: 14px;
    line-height: 10px;
    width: 30px;
    text-align: center;
    color: #a9afbb;
}

</style>
<!-- sidebar -->
      <div class="sidebar" data-color="rose" data-background-color="black" data-image="<?php echo base_url()?>assets/images/unilever_bg2.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo text-center">
        
          <img src="<?php echo base_url().$img ?>" class="img-circle img-responsive" style="width: 45px; height: 45px;" alt="User Image">
        
        <a href="#" class="simple-text logo-normal">
         
          Time Slot Booking
        </a>
      </div>
      <div class="sidebar-wrapper">
        <div class="user">
          <div class="photo">
            <?php 
          $id = $this->session->userdata('sess_id');
          $getData = $this->db->query("SELECT username, foto_type FROM ms_user where id = '$id' ")->row();
          $getUsername = $getData->username;
          $getFotoType = $getData->foto_type;

           ?>
          <?php 
              $foto = base_url()."assets/upload/foto/".$getUsername.$getFotoType;
              if(@getimagesize($foto)){
                 
              }else{
                $foto = base_url()."assets/images/dummy-profile.png";
              }
            ?>

            <img src="<?php echo $foto; ?>" />
          </div>
          <!-- Informasi User -->
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username">
              <span>
                <?php echo $this->session->userdata('sess_nama'); ?>
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample">
              <ul class="nav">
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>profile">
                    <span class="sidebar-mini"> MP </span>
                    <span class="sidebar-normal"> My Profile </span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?php echo base_url()?>profile/edit_profile">
                    <span class="sidebar-mini"> EP </span>
                    <span class="sidebar-normal"> Edit Profile </span>
                  </a>
                </li>
                <li class="nav-item" hidden>
                  <a class="nav-link" href="<?php echo base_url()?>profile/settings">
                    <span class="sidebar-mini"> S </span>
                    <span class="sidebar-normal"> Settings </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- End informasi User -->
        </div>

        <ul class="nav">
          <?php
          date_default_timezone_set("Asia/Bangkok");
          $year = date('Y');
          $week = date('W');
          $date = date('Y-m-d');
          ?>
          
          <?php 
            $id_user = $this->session->userdata('sess_id');
            $getMenu = get_menu(); 
            $getUserMenu = $this->db->query("SELECT a.menu FROM ms_role As a INNER JOIN ms_user AS b ON a.id = b.role WHERE b.id = '$id_user' ")->row()->menu;
            $get_menus = explode(",",$getUserMenu);
            foreach ($getMenu as $get) {
              if(in_array($get->id,$get_menus)){
                  if($get->id == 22){ ?>
                  <li class="nav-item ">
                    <a class="nav-link" data-toggle="collapse" href="#formsExamples">
                      <i class="<?php echo $get->icon; ?>"></i>
                      <p><?php echo $get->menu; ?> <b class="caret" style="margin-top: 2px;"></b></p>
                    </a>
                    <div class="collapse" id="formsExamples">
                      <ul class="nav">
                        <li class="nav-item " <?php if(!check_sub_menu(22)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url().$get->link; ?>">
                            <span class="sidebar-mini"> <i class="fas fa-users-cog"></i> </span>
                            <p>PIC Management</p>
                          </a>
                        </li>
                        <li class="nav-item" <?php if(!check_sub_menu(23)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url(); ?>/user_management/authorization">
                            <span class="sidebar-mini"> <i class="fas fa-user-lock"></i> </span>
                            <span class="sidebar-normal"> User Authorization </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>
                <?php 
                }else if($get->id == 8){ ?>
                  <li class="nav-item ">
                    <a class="nav-link collapsed" data-toggle="collapse" href="#schedule_table">
                      <i class="<?php echo $get->icon; ?>"></i>
                      <p>
                        <?php echo $get->menu; ?>
                        <b class="caret" style="margin-top: 2px;"></b>
                      </p>
                    </a>
                    <div class="collapse" id="schedule_table">
                      <ul class="nav">
                        <li class="nav-item " <?php if(!check_sub_menu(8)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url().$get->link; ?>?year=<?php echo date('Y'); ?>&week=<?php echo date('W'); ?>&category=ALL">
                            <span class="sidebar-mini"> <i class="fas fa-box"></i> </span>
                            <span class="sidebar-normal"> Non-Weighing Material </span>
                          </a>
                        </li>
                        <li class="nav-item" <?php if(!check_sub_menu(8)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url(); ?>schedule_table/schedule2?year=<?php echo date('Y'); ?>&week=<?php echo date('W'); ?>&category=WEIGHING">
                            <span class="sidebar-mini"> <i class="fas fa-box"></i> </span>
                            <span class="sidebar-normal"> Weighing Material </span>
                          </a>
                        </li>
                        <li class="nav-item" <?php if(!check_sub_menu(8)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url(); ?>schedule_table">
                            <span class="sidebar-mini"> <i class="fas fa-box"></i> </span>
                            <span class="sidebar-normal"> IBD Material </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>
                <?php 
                }else if($get->id == 33){ 
                ?>
                  <li class="nav-item ">
                    <a class="nav-link" data-toggle="collapse" href="#report">
                      <i class="<?php echo $get->icon; ?>"></i>
                      <p><?php echo $get->menu; ?> <b class="caret" style="margin-top: 2px;"></b></p>
                    </a>
                    <div class="collapse" id="report">
                      <ul class="nav">
                        <li class="nav-item " <?php if(!check_sub_menu(33)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url().$get->link; ?>?year=<?php echo date('Y'); ?>&week=<?php echo date('W'); ?>&category=ALL">
                            <span class="sidebar-mini"> <i class="fas fa-hourglass"></i> </span>
                            <span class="sidebar-normal"> OTIF Report </span>
                          </a>
                        </li>
                        <li class="nav-item" <?php if(!check_sub_menu(34)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url(); ?>report/on_time?year=<?php echo date('Y'); ?>&week=<?php echo date('W'); ?>">
                            <span class="sidebar-mini"> <i class="fas fa-stopwatch"></i> </span>
                            <span class="sidebar-normal"> Slot On Time Report </span>
                          </a>
                        </li>
                        <li class="nav-item" <?php if(!check_sub_menu(36)){echo "hidden";} ?>>
                          <a class="nav-link" href="<?php  echo base_url(); ?>report/unloading_time?year=<?php echo date('Y'); ?>&week=<?php echo date('W'); ?>">
                            <span class="sidebar-mini"> <i class="fas fa-truck-loading"></i> </span>
                            <span class="sidebar-normal"> Unloading Time Report </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>
                <?php 
                }
                  else if($get->id != 22){
                  $type = $get->link_type;

                  $date = date('Y-m-d');

                  if($date == '2019-12-30' || $date == '2019-12-31'){
                    $year = "2020";
                    $week = "1";
                  }else{
                    $year = date('Y');
                    $week = date('W');
                  }
                  if(strpos($week, "0") == 0){
                    $week = str_replace("0", "", $week);
                  }
                  ?>
                    <li class="nav-item">
                      <?php if($type == 1){ ?>
                        <a class="nav-link" href="<?php  echo base_url().$get->link; ?>?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo date('Y-m-d'); ?>">
                      <?php }else if($type == 2){ ?>
                        <a class="nav-link" href="<?php  echo base_url().$get->link; ?>?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo date('Y-m-d'); ?>&category=FACE">
                      <?php }else if($type == 3){ ?>
                        <a class="nav-link" href="<?php  echo base_url().$get->link; ?>?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo date('Y-m-d'); ?>&category=">
                      <?php }else if($type == 4){ ?>
                        <a class="nav-link" href="<?php  echo base_url().$get->link; ?>?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo date('Y-m-d'); ?>&category=FACE&GET=Weekly">
                       <?php }else if($type == 36){ ?>
                        <a class="nav-link" href="<?php echo $get->link; ?>">
                      <?php }else{ ?>
                        <a class="nav-link" href="<?php  echo base_url().$get->link; ?>">
                      <?php } ?>
                        <i class="<?php echo $get->icon; ?>"></i>
                        <p><?php echo $get->menu; ?></p>
                      </a>
                    </li>
                  <?php
                  }
                }
            }
          ?>
          
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url() ?>login/logout">
              <i class="fas fa-sign-out-alt"></i>
              <p>Log-Out</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <!-- end sidebar -->

    <div class="main-panel">

        <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
              <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
               <i class="fas fa-ellipsis-h" style="font-size:18px"></i>
                
              </button>
            </div>
            <div style="font-size: 12px; margin-top: 6px; margin-left: 10px">
              Ada pertanyaan terkait penggunaan Time Slot Booking? <a href="http://skin.timeslotbooking.com/support/kb/" target="_Blank">Klik disini</a>
            </div>
            <?php if( !empty($this->uri->segment('2')) && $this->uri->segment('1') != 'search' && $this->uri->segment('2') != 'schedule2' ){ ?>
            <div class="navbar-minimize">
              <button onclick='window.history.back()' id="minimizeSidebar" class="btn btn-just-icon btn-info btn-fab btn-round">
                <i class="fas fa-arrow-left"></i>              
              </button>
            </div>
          <?php } ?>
            <!-- <a class="navbar-brand" href="#pablo"></a> -->
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <!-- <span style="font-size:40px;color:blue;cursor: pointer;" onclick="history.back()"><i class="fas fa-arrow-circle-left"></i></span>
          <span style="font-size:40px;color:blue;cursor: pointer;" onclick="history.forward()"><i class="fas fa-arrow-circle-right"></i></span> -->
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form" style="display:none">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <style type="text/css">
                @media screen and (max-width:800px){
                  .statz{
                    display: none!important;
                  }
                }
              </style>
              <?php if($get_menus[0] == 1){ ?>
              <li class="nav-item statz">
                <a class="nav-link" href="<?php date_default_timezone_set("Asia/Bangkok"); echo base_url() ?>schedule?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo $date; ?>">
                  <i class="fas fa-th-large" style="font-size:18px"></i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
            <?php } ?>
              <div id="history_update">
              </div>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-user" style="font-size:18px"></i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?php echo base_url()?>profile">Profile</a>
                  <a class="dropdown-item" href="<?php echo base_url()?>profile/edit_profile">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url()?>login/logout">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->

      <script type="text/javascript">
          function open_menu(){
            
          }
      </script>