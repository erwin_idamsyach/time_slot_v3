<!-- footer -->
                 
                 <!--  End Footer -->

                 <!-- sidebar mini -->

    </div>

  </div>

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap-notify.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/scroll/perfect_scroll.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/material_dashboard/dashboard.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap-material-design.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/sweet_alert/sweetalert2.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/chart/chartist.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/select2/select2.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/spin.umd.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/tableHeadFixer.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap-selectpicker.js"></script>
<!-- bootstrap datepicker -->
<script>

  var x = new Spin.Spinner().spin();
  loading();
  function loading(){
    $('body').append(x.el);
  }

  function loading_stop(){
    x.stop();
  }

  var set_year = "<?php echo date('Y'); ?>";
  var set_week = "<?php echo date('W'); ?>";
  var set_date = "<?php echo date('Y-m-d'); ?>";
  $(document).ready(function(){
    var ps = $('.content').perfectScrollbar();
    loading_stop();
    setInterval(history_routine(), 1000);
    
    $(".select2-init").select2();
    <?php if($this->uri->segment('1') == 'schedule' AND $this->uri->segment('2') != 'schedule2' ){ ?>
    check_event_changing();
  <?php } ?>
    
    <?php
    if($this->session->userdata("sess_role_no") == 3){
      ?>
    check_data_changing();
    check_schedule_changing();
    
    //check_periode_changing();
      <?php
    }
    ?>
  });
  $('table.data-table').dataTable();
  $("[data-toggle='tooltip']").tooltip({
    container : 'table'
  });
  $("[data-toggle='popover']").popover({
    html : true,
    container : 'body',
    viewport : {
      "x-index" : 100
    }
  });
  $("[data-toggle='popover']").on('show.bs.popover', function(){
    $("[data-toggle='popover']").not(this).popover('hide');
  });
  $("#datepicker").datepicker({
    autoclose : true
  });
  /*$("[data-toggle='popover']").popover({
    html : true,
    container : 'body',
    viewport : {
      "x-index" : 100
    }
  });

  $("[data-toggle='popover']").on('show.bs.popover', function(){
    $("[data-toggle='popover']").not(this).popover('hide');
  });

  $("#modal-dynamic").on('show.bs.modal', function(){
    $("[data-toggle='popover']").popover('hide');
  })*/

  function openModal(title, body_content, modal_size = "md"){
    $("#area-modal-title").html(title);
    $("#area-modal-content").html(body_content);
    $("#modal-content").removeAttr("style");
    if(modal_size == "sm"){
      $("#modal-dynamic .modal-dialog").addClass("modal-sm");
    }else if(modal_size == "lg"){
      $("#modal-dynamic .modal-dialog").addClass("modal-lg");
    }else if(modal_size == "xl"){
      $("#modal-dynamic .modal-dialog").addClass("modal-xl");
    }else{
      $("#modal-dynamic .modal-dialog").addClass("modal-md");
    }

    $("#modal-dynamic").modal('show');
  }

  function check_event_changing(){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>routine/check_event_data_changed",
      success:function(resp){
        if(resp == 1){
          set_event_to_zero();
        }else{
          setTimeout(function(){
          check_event_changing();
        }, 1000);
        }
      },
      error:function(e){
        setTimeout(function(){
          check_event_changing();
        }, 1000);
        
      }
    });
}

function history_routine(){
  $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>history/sidebar_history",
      dataType:"HTML",
      success:function(resp){
        $('#history_update').html(resp);
      },
      error:function(e){
      }
    });
}

function set_event_to_zero(){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>routine/set_event_to_zero",
      success:function(resp){
        alert("Ada perubahan schedule, Halaman akan di reload");
        window.location=("<?php echo base_url(); ?>schedule?year="+set_year+"&week="+set_week+"&date="+set_date);
      },
      error:function(e){
      }
    });
}

  <?php
  if($this->session->userdata('sess_role_no') == 3){
    ?>
  function check_data_changing(){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>routine/check_data_changed",
      success:function(resp){
        if(resp == 1){
          set_to_zero();
        }else{
          setTimeout(function(){
          check_data_changing();
        }, 1000);
        }
      },
      error:function(e){
        setTimeout(function(){
          check_data_changing();
        }, 1000);
        
      }
    })
  }


  function check_schedule_changing(){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>routine/check_schedule_changed",
      success:function(resp){
        if(resp == 1){
          set_to_zero_schedule();
        }else{
          setTimeout(function(){
          check_schedule_changing();
        }, 1000);
        }
      },
      error:function(e){
        setTimeout(function(){
          check_schedule_changing();
        }, 1000);
        
      }
    })
  }

  function set_to_zero(){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>routine/set_to_zero",
      success:function(resp){
        alert("Progressor telah menambahkan/mengubah data, proses scheduling akan dilaksanakan");
        window.location=("<?php echo base_url(); ?>schedule?year="+set_year+"&week="+set_week+"&date="+set_date);
      },
      error:function(e){
      }
    })
  }

  function set_to_zero_schedule(){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>routine/set_to_zero_schedule",
      success:function(resp){
        alert("Ada perubahan schedule, data scheduling baru akan terload");
        window.location=("<?php echo base_url(); ?>schedule?year="+set_year+"&week="+set_week+"&date="+set_date);
      },
      error:function(e){
      }
    })
  }
    <?php 
  }
  ?>
</script>
<?php
  $uri = $this->load->view('script');
?>
</body>
</html>