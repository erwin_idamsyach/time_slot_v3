<script>
	function setWeek(week, state){
		/*alert(week+" "+state);*/
		var year = $('#year').val();
		if(state == "INC"){
			week = parseInt(week)+1;
			$('.week').val(week);
		}else{
			week = parseInt(week)-1;
			$('.week').val(week);
		}
		window.location=('<?php base_url() ?>outstanding?year='+year+'&week='+week);
	}

	$("#table").DataTable({
		
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>',
		responsive : true
		
	});


	function datatablez(){
		$('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [5, 10, 25, 50, -1],
          [5, 10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search RDS",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

	}

	function setYear2(year, state){
			var year = $('#year').val();
			var week = $('.week').val();
			window.location=('<?php base_url() ?>outstanding?year='+year+'&week='+week);
	}


	function setYear(year, state){
		/*alert(week+" "+state);*/
		var year = $('#year').val();
		var week = $('.week').val();
		if(state == "INC"){
			year = parseInt(year)+1;
			$('#year').val(year);
		}else{
			year = parseInt(year)-1;
			$('#year').val(year);
		}
		window.location=('<?php base_url() ?>outstanding?year='+year+'&week='+week);
	}
</script>