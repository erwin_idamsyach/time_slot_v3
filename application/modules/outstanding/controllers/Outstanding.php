<?php

class Outstanding extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('OS_Model');
		isLogin();
	}

	public function index(){
		
		date_default_timezone_set('Asia/Jakarta');
		$shift 			= getShift();
		$role 			= $this->session->userdata('sess_role_no');
		$arr_vendor 	= array();
		$arr_id 		= array();
		$today  		= date('Y-m-d');
		$ven_list 		= $this->OS_Model->getVendorList();
		$year 			= (null !== $this->input->get('year'))?$this->input->get('year'):date('Y');
		$week 			= (null !== $this->input->get('week'))?$this->input->get('week'):date('W');
		$sess_no 		= $this->session->userdata('sess_role_no');
		$pl 			= [];
		
		if($sess_no != 3){
			foreach ($ven_list->result() as $g) {
				array_push($arr_vendor, $g->vendor_code);
			}

			$impl = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialPool($impl, $year, $week);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model->getDateLess($year,$week, $imp);
			foreach($dateLess->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLate($today, $shift, $imp, $year, $week, $role);
			$returnMat = $this->OS_Model->getMaterialReturn($impl, $year, $week);
		}else{
			$vc = $this->session->userdata('sess_vendor_code');
			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialVendor($year, $week, $sess_no, $vc);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$dateLess = $this->OS_Model->getDateLessVen($year,$week, $imp, $sess_no, $vc);

			foreach($dateLess->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLateVen($today, $shift, $imp, $year, $week, $sess_no, $vc);

			$returnMat = $this->OS_Model->getReturnVendor($year, $week, $sess_no, $vc);

		}
		/*dump($dateLess->result());*/
		$pl = array_merge($dataPool->result(), $dateLess->result(), $shiftLate->result(), $returnMat->result());

		/*if($dataPool->num_rows() > 0 && $dateLess->num_rows() > 0 && $shiftLate->num_rows() > 0){

		}else{
			if($dataPool->num_rows() > 0 && $dateLess->num_rows() == 0){
				$pl = $dataPool->result();
			}else if($dataPool->num_rows() == 0 && $dateLess->num_rows() > 0){
				$pl = $dateLess->result();
			}
		}*/
		$data['pool_list'] = $pl;
		getHTML('outstanding/index', $data);
	}


	public function check(){
		
		$MonthAgo = date('Y-m-d',strtotime("-1 month"));
		$shift = getShift();
		$role = $this->session->userdata('sess_role_no');
		$arr_vendor = array();
		$arr_id = array();
		$today  = date('Y-m-d');
		if($this->session->userdata('sess_role_no') == 3){
			$ven_list = $this->session->userdata('sess_vendor_code');
		}else{
			$ven_list = $this->OS_Model->getVendorList();
		}
		
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$sess_no = $this->session->userdata('sess_role_no');
		$pl = [];
		if($sess_no != 3){
			foreach ($ven_list->result() as $g) {
				array_push($arr_vendor, $g->vendor_code);
			}

			$impl = implode(",", $arr_vendor);
			
			$dataPool = $this->OS_Model->getMaterialPool($impl, $year, $week);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model->getDateLess($year,$week, $imp);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLate($today, $shift, $imp, $year, $week, $role);
			$returnMat = $this->OS_Model->getMaterialReturn($impl, $year, $week);

		}else{
			$vc = $this->session->userdata('sess_vendor_code');
			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialVendor($year, $week, $sess_no, $vc);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$dateLess = $this->OS_Model->getDateLessVen($year,$week, $imp, $sess_no, $vc);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLateVen($today, $shift, $imp, $year, $week, $sess_no, $vc);

			$returnMat = $this->OS_Model->getReturnVendor($year, $week, $sess_no, $vc);

		}
		/*dump($dateLess->result());*/
		$pl = array_merge($dataPool->result(), $dateLess->result(), $shiftLate->result(), $returnMat->result());
		
		echo json_encode($pl, JSON_PRETTY_PRINT);

	}
}

?>
