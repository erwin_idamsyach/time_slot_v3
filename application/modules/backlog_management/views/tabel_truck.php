<?php  ?>
<div class="row">
  <?php 
  $header = array(
    0 => "primary",
    1 => "info", 
    2 => "success"
  );

  $bg_color = array(
    0 => "#ab47bc",
    1 => "#26c6da",
    2 => "#ffa726"
  );

  $shift = array(
    0 => "Malam",
    1 => "Pagi",
    2 => "Siang"
  );

  $shift2 = array(
    "DM",
    "DP",
    "DS"
  );
  ?>

<?php 
$no = 0;
$id_row = 0;
$id_baris = 0;
$id_drag = 0;
  foreach($data_schedule_detail as $dsd){
    ?>
    <div class="col-md-4">
        <div class="card card-chart">
          <div class="card-header card-header-<?php echo $header[$no]; ?> text-center">
            <b>Dinas <?php echo $shift[$no]; ?> </b>
          </div>
        <div class="card-body ">
          <div class="table-schedule table-responsive">      
            <table class="table table-hover">
              <thead>
                <tr align="center" style="padding:0px">
                    <th rowspan="2" style="padding:0px; font-weight: 500" width="120px">Time Slot</th>
                    <th colspan="5" style="vertical-align: middle;padding:5px; font-weight: 500" >Schedule</th>
                  </tr>
              </thead>
              <tbody>
                <?php foreach($dsd as $d){
                  $counter_truck =0;
                  ?>
                    <tr>
                      <td style="background-color: <?php echo $bg_color[$no]; ?>; text-align:center"><b><?php echo substr(array_values($d)[0]["start_time"], 0,5)." - ".substr(array_values($d)[0]["end_time"], 0,5); ?></b></td>
                      <td width="2"></td>
                      <?php $total_truck = count(array_values($d)[0]["detail"]) ?>
                      <?php
                      $b = 0;                      
                      $count_zara2 = 0;
                       foreach(array_values($d)[0]["detail"] as $detail_truck){
                        ?>
                          <td id="div<?php echo $id_row++; ?>" class="<?php echo $id_baris; ?>" style="width: 60px; font-size:7.5px; background-color:#cacbce"><div 
                          <?php
                              $zara2 = $detail_truck->zara2;
                              if($detail_truck->zara2 >0){                        
                                $count_zara2++;
                              }
                              
                              if($detail_truck->status == '2'){
                              
                            ?> 
                              ondrop="drop(event)" 
                              ondragover="allowDrop(event)"
                              draggable="true" 
                              ondragstart="drag(event)" 
                            <?php
                              }
                            ?>
                           id="drag<?php echo $id_drag++; ?>" ondblclick="getDetailedInformation(<?php echo $detail_truck->schedule_number;?>, <?php echo $detail_truck->status;  ?>)" value='<?php echo $detail_truck->id; ?>'>
                           
                           <?php 
                          
                          truckIcon($detail_truck->dock_id, $detail_truck->status, $detail_truck->vendor_alias, 0, 1, 0, $detail_truck->police_number, $detail_truck->gr_material_doc);
                          
                           ?>
                             
                           </div>
                         </td>
                        <?php
                        $counter_truck++;
                       }
                      ?>
                      
                      <?php 
                        while($counter_truck < 5){
                          ?>
                            <td id="div<?php echo $id_row; ?>" class="<?php echo $id_baris; ?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width: 60px;font-size:7.5px; background-color:#cacbce"></td>
                          <?php
                          $id_row++;
                          $counter_truck++;
                        }
                      ?>
                      
                    </tr>
                  <?php
                  $id_baris++;
                } ?>
                
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
    <?php
    $no++;
  }
?>
  
</div>