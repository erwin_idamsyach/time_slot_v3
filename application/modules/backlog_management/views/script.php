<script>
var link = "<?php $this->uri->segment('2') ?>";
if(link == 'material'){
	$("#table").DataTable({
		"scrollX": true,
		ordering: false,
		responsive: true,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'		
	});
}else{
	$("#table").DataTable({		
		ordering: false,
		responsive: true,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
	});
}

tabel_truck_move();
setInterval(function(){
	tabel_truck_move();	
}, 60000);
var date = "<?php echo $this->input->get('date'); ?>";
var week = "<?php echo $this->input->get('week'); ?>";
var year = "<?php echo $this->input->get('year'); ?>";

function tabel_truck(){
	date = "<?php echo $this->input->get('date'); ?>";
	week = "<?php echo $this->input->get('week'); ?>";
	year = "<?php echo $this->input->get('year'); ?>";
	$.ajax({
		url:"<?php echo base_url() ?>backlog_management/tabel_truck",
		dataType:"HTML",
		data : {	
			"rdd"  : date,
			"year" : year,
			"week" : week
		},
		success:function(resp){
			$('#tabel_truck').html(resp);
		},
		error:function(resp){
			alert("Tabel Schedule Error");
		}
	});
}

function tabel_truck_move(){
	date = "<?php echo $this->input->get('date'); ?>";
	week = "<?php echo $this->input->get('week'); ?>";
	year = "<?php echo $this->input->get('year'); ?>";
	$.ajax({
		url:"<?php echo base_url() ?>backlog_management/tabel_truck_move",
		dataType:"HTML",
		data : {	
			"rdd"  : date,
			"year" : year,
			"week" : week
		},
		success:function(resp){
			$('#tabel_truck').html(resp);
		},
		error:function(resp){
			alert("Tabel Schedule Error");
		}
	});
}

function datatablez(){
	$('#datatables').DataTable({
    "lengthMenu": [
      [10, 25, 50, -1],
      [10, 25, 50, "All"]
    ],
    "scrollX": true,
	ordering: false,
    responsive: true,
    language: {
      search: "_INPUT_",
      searchPlaceholder: "Search Delivery",
    }
  });

  var table = $('#datatable').DataTable();

  // Edit record
  table.on('click', '.edit', function() {
    $tr = $(this).closest('tr');
    var data = table.row($tr).data();
    alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
  });

  // Delete a record
  table.on('click', '.remove', function(e) {
    $tr = $(this).closest('tr');
    table.row($tr).remove().draw();
    e.preventDefault();
  });

  //Like record
  table.on('click', '.like', function() {
    alert('You clicked on Like button');
  });

}

function getDetailedInformation(key,status){
	$(".modal").modal('hide');
	setTimeout(function(){
		$.ajax({
			type : "GET",
			url : "<?php echo base_url() ?>schedule/get_detailed_information",
			data : {
				'key' : key,
				'status' : status
			},
			success:function(resp){
				openModal("Information", resp, "lg");
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})
		
	}, 200);	
}

function changeSchedule(sn){
	$.ajax({
		url : "<?php echo base_url(); ?>backlog_management/getSchedule",
		method : "GET",
		data :{
			"sn" :sn
		},
		dataType : "JSON",
		success:function(e){
			$('#sn').val(e.schedule_number);
			$('#schedule_now').val(e.start_time+' - '+e.end_time);
			$('#schedule_id').val(e.id);
		},error:function(e){
			alert("Something Error");
		}
	});
	$('#btn_modal').click();
}

var id_truck = 0;
var id_time_slot = 0;
function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
  id_truck = $('#'+ev.target.id).attr('value');
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
  id_time_slot = ev.target.id;
  id_time_slot = (id_time_slot).substring(3,5);
  truck_date =  $('#'+ev.target.id).attr("truck_date");

  $.ajax({
	type : "GET",
	url : "<?php echo base_url() ?>backlog_management/update_time_slot",
	data : {
		'id_truck' : id_truck,
		'id_time_slot' : id_time_slot,
		"rdd" : date,
		"truck_date" : truck_date
	},
	success:function(resp){
		if(resp == "Hari telah terlampui coba hubungi pihak Unilever"){
			Swal.fire(
			  'Gagal!',
			  'Hari telah terlampui coba hubungi pihak Unilever!',
			  'error'
			);
		}else if(resp == "Time slot telah terlampui coba hubungi pihak Unilever"){
			Swal.fire(
				'Gagal!',
				'Time slot telah terlampui coba hubungi pihak Unilever',
				'error'
			);
		}else if(resp == "Data berhasil terupdate"){
			Swal.fire(
				'Berhasil!',
				'Data Time Slot berhasil di update',
				'success'
			);
			location.reload();
		}
		tabel_truck_move();
	},
	error:function(e){
		alert("Something wrong!");
		console.log(e);
	}
});
}

function getDetailedInformation(key,status){
		if(status > 0){
			$(".modal").modal('hide');
			setTimeout(function(){
				$.ajax({
					type : "GET",
					url : "<?php echo base_url() ?>schedule/get_detailed_information",
					data : {
						'key' : key,
						'status' : status
					},
					success:function(resp){
						openModal("Information", resp, "lg");
					},
					error:function(e){
						alert("Something wrong!");
						console.log(e);
					}
				})
				
			}, 200);
		}else{
			$(".modal").modal('hide');
		setTimeout(function(){
			$.ajax({
				type : "GET",
				url : "<?php echo base_url() ?>schedule/get_DI",
				data : {
					'key' : key
				},
				success:function(resp){
					openModal("Information", resp, "lg");
					editTime($("#created_at").val());
				},
				error:function(e){
					alert("Something wrong!");
					console.log(e);
				}
			})
			preventDefault();
		}, 200);
			
	}
		}
</script>