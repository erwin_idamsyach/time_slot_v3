
<div class="row">
  <?php 
  $date_now = $this->input->get('rdd');
  if($data_shift == "DM" || $date_now != date('Y-m-d')){
    $header = array(
      0 => "primary",
      1 => "info", 
      2 => "warning"
    );

    $bg_color = array(
      0 => "#ab47bc",
      1 => "#26c6da",
      2 => "#ffa726"
    );

    $shift = array(
      0 => "Malam",
      1 => "Pagi",
      2 => "Siang"
    );

    $shift2 = array(
      "DM",
      "DP",
      "DS"
    );
  }else if($data_shift == "DP"){
    $header = array(
      0 => "info", 
      1 => "warning",
      2 => "primary"
    );

    $bg_color = array(
      0 => "#26c6da",
      1 => "#ffa726",
      2 => "#ab47bc",
    );

    $shift = array(
      0 => "Pagi",
      1 => "Siang",
      2 => "Malam"
    );

    $shift2 = array(
      "DP",
      "DS",
      "DM",
    );
  }else if($data_shift == "DS"){
    $header = array( 
      0 => "warning",
      1 => "primary",
      2 => "info",
    );

    $bg_color = array(
      0 => "#ffa726",
      1 => "#ab47bc",
      2 => "#26c6da"
    );

    $shift = array(
      0 => "Siang",
      1 => "Malam",
      2 => "Pagi",
    );

    $shift2 = array(
      "DS",
      "DM",
      "DP",
    );
  }
  
  ?>

<?php 
$no = 0;
$id_row = 0;
$id_baris = 0;
$id_drag = 0;
 
if($data_shift == "DM" || $date_now != date('Y-m-d')){
  foreach($data_schedule_detail[$date] as $key => $dsd){
      ?>
        <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-<?php echo $header[$no]; ?> text-center">
                <b>Dinas <?php echo $shift[$no]; ?> <?php echo $date; ?></b>
              </div>
            <div class="card-body ">
              <div class="table-schedule table-responsive">      
                <table class="table table-hover">
                  <thead>
                    <tr align="center" style="padding:0px">
                        <th rowspan="2" style="padding:0px; font-weight: 500" width="120px">Time Slot</th>
                        <th colspan="5" style="vertical-align: middle;padding:5px; font-weight: 500" >Schedule</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($dsd as $d){
                      $counter_truck =0;
                      ?>
                        <tr>
                          <td style="background-color: <?php echo $bg_color[$no]; ?>; text-align:center; font-size:11px"><b><?php echo substr(array_values($d)[0]["start_time"], 0,5)." - ".substr(array_values($d)[0]["end_time"], 0,5); ?></b></td>
                          <td width="2"></td>
                          <?php $total_truck = count(array_values($d)[0]["detail"]) ?>
                          <?php
                          $b = 0;                      
                          $count_zara2 = 0;

                           foreach(array_values($d)[0]["detail"] as $detail_truck){

                            ?>
                              <td id="div<?php echo $id_row++; ?>" truck_date="<?php echo $date; ?>" class="<?php echo $id_baris; ?>" style="width: 60px; font-size:7.5px; background-color:#cacbce"><div 
                              <?php
                                  $zara2 = $detail_truck->zara2;
                                  if($detail_truck->zara2 >0){                        
                                    $count_zara2++;
                                  }

                                  if($detail_truck->status == '2'){
                                  
                                ?> 
                                  truck_date="<?php echo $date; ?>"
                                  ondrop="drop(event)" 
                                  ondragover="allowDrop(event)"
                                  draggable="true" 
                                  ondragstart="drag(event)"
                                <?php
                                  }
                                ?>
                               id="drag<?php echo $id_drag++; ?>" ondblclick="getDetailedInformation('<?php echo $detail_truck->schedule_number;?>', <?php echo $detail_truck->status;  ?>)" value='<?php echo $detail_truck->id; ?>'>
                               
                               <?php 
                              
                              if($detail_truck->status > 0){
      
                                truckIcon($detail_truck->dock_id, $detail_truck->status, $detail_truck->vendor_alias, 0, 1, 0, $detail_truck->police_number, $detail_truck->gr_material_doc);
                              }
                              
                               ?>
                                 
                               </div>
                             </td>
                            <?php
                            $counter_truck++;
                           }
                          ?>
                          
                          <?php 
                            while($counter_truck < 5){
                              ?>
                                <td id="div<?php echo $id_row; ?>" truck_date="<?php echo $date; ?>" class="<?php echo $id_baris; ?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width: 60px;font-size:7.5px; background-color:#cacbce"></td>
                              <?php
                              $id_row++;
                              $counter_truck++;
                            }
                          ?>
                          
                        </tr>
                      <?php
                      $id_baris++;
                    } ?>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php
      $no++;
      }
  }else if($data_shift == 'DP'){
    $id_row = 25;
    foreach($data_schedule_detail[$date] as $key => $dsd){
      if($key != "DM"){

      ?>
        <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-<?php echo $header[$no]; ?> text-center">
                <b>Dinas <?php echo $shift[$no]; ?> <?php echo $date; ?></b>
              </div>
            <div class="card-body ">
              <div class="table-schedule table-responsive">      
                <table class="table table-hover">
                  <thead>
                    <tr align="center" style="padding:0px">
                        <th rowspan="2" style="padding:0px; font-weight: 500" width="120px">Time Slot</th>
                        <th colspan="5" style="vertical-align: middle;padding:5px; font-weight: 500" >Schedule</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($dsd as $d){
                      $counter_truck =0;
                      ?>
                        <tr>
                          <td style="background-color: <?php echo $bg_color[$no]; ?>; text-align:center; font-size:11px"><b><?php echo substr(array_values($d)[0]["start_time"], 0,5)." - ".substr(array_values($d)[0]["end_time"], 0,5); ?></b></td>
                          <td width="2"></td>
                          <?php $total_truck = count(array_values($d)[0]["detail"]) ?>
                          <?php
                          $b = 0;                      
                          $count_zara2 = 0;

                           foreach(array_values($d)[0]["detail"] as $detail_truck){

                            ?>
                              <td id="div<?php echo $id_row++; ?>" truck_date="<?php echo $date; ?>" class="<?php echo $id_baris; ?>" style="width: 60px; font-size:7.5px; background-color:#cacbce"><div 
                              <?php
                                  $zara2 = $detail_truck->zara2;
                                  if($detail_truck->zara2 >0){                        
                                    $count_zara2++;
                                  }
                                  if($detail_truck->status == '2'){
                                  
                                ?> 
                                  truck_date="<?php echo $date; ?>"
                                  ondrop="drop(event)" 
                                  ondragover="allowDrop(event)"
                                  draggable="true" 
                                  ondragstart="drag(event)"
                                <?php
                                  }
                                ?>
                               id="drag<?php echo $id_drag++; ?>" ondblclick="getDetailedInformation('<?php echo $detail_truck->schedule_number;?>', <?php echo $detail_truck->status;  ?>)" value='<?php echo $detail_truck->id; ?>'>
                               
                               <?php 
                              
                              if($detail_truck->status > 0){
                                
                                truckIcon($detail_truck->dock_id, $detail_truck->status, $detail_truck->vendor_alias, 0, 1, 0, $detail_truck->police_number, $detail_truck->gr_material_doc);

                              }
                              
                               ?>
                                 
                               </div>
                             </td>
                            <?php
                            $counter_truck++;
                           }
                          ?>
                          
                          <?php 
                            while($counter_truck < 5){
                              ?>
                                <td id="div<?php echo $id_row; ?>" truck_date="<?php echo $date; ?>" class="<?php echo $id_baris; ?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width: 60px;font-size:7.5px; background-color:#cacbce"></td>
                              <?php
                              $id_row++;
                              $counter_truck++;
                            }
                          ?>
                          
                        </tr>
                      <?php
                      $id_baris++;
                    } ?>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php
      $no++;
      }
    }

    foreach($data_schedule_detail[$rdd_future] as $key => $dsd){
      if($key == "DM"){
        $id_row=0;
      ?>
        <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-<?php echo $header[$no]; ?> text-center">
                <b>Dinas <?php echo $shift[$no]; ?> <?php echo $rdd_future; ?></b>
              </div>
            <div class="card-body ">
              <div class="table-schedule table-responsive">      
                <table class="table table-hover">
                  <thead>
                    <tr align="center" style="padding:0px">
                        <th rowspan="2" style="padding:0px; font-weight: 500" width="120px">Time Slot</th>
                        <th colspan="5" style="vertical-align: middle;padding:5px; font-weight: 500" >Schedule</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($dsd as $d){
                      $counter_truck =0;
                      ?>
                        <tr>
                          <td style="background-color: <?php echo $bg_color[$no]; ?>; text-align:center; font-size:11px"><b><?php echo substr(array_values($d)[0]["start_time"], 0,5)." - ".substr(array_values($d)[0]["end_time"], 0,5); ?></b></td>
                          <td width="2"></td>
                          <?php $total_truck = count(array_values($d)[0]["detail"]) ?>
                          <?php
                          $b = 0;                      
                          $count_zara2 = 0;
                           foreach(array_values($d)[0]["detail"] as $detail_truck){
                            ?>
                              <td id="div<?php echo $id_row++; ?>" truck_date="<?php echo $rdd_future; ?>"  class="<?php echo $id_baris; ?>" style="width: 60px; font-size:7.5px; background-color:#cacbce"><div 
                              <?php
                                  $zara2 = $detail_truck->zara2;
                                  if($detail_truck->zara2 >0){                        
                                    $count_zara2++;
                                  }
                                  if($detail_truck->status == '2'){
                                  
                                ?>
                                truck_date="<?php echo $date; ?>" 
                                  ondrop="drop(event)" 
                                  ondragover="allowDrop(event)"
                                  draggable="true" 
                                  ondragstart="drag(event)"
                                <?php
                                  }
                                ?>
                               id="drag<?php echo $id_drag++; ?>" ondblclick="getDetailedInformation('<?php echo $detail_truck->schedule_number;?>', <?php echo $detail_truck->status;  ?>)" value='<?php echo $detail_truck->id; ?>'>
                               
                               <?php 
                              
                              if($detail_truck->status > 0){
                                truckIcon($detail_truck->dock_id, $detail_truck->status, $detail_truck->vendor_alias, 0, 1, 0, $detail_truck->police_number, $detail_truck->gr_material_doc);
                              }
                              
                               ?>
                                 
                               </div>
                             </td>
                            <?php
                            $counter_truck++;
                           }
                          ?>
                          
                          <?php 
                            while($counter_truck < 5){
                              ?>
                                <td id="div<?php echo $id_row; ?>" truck_date="<?php echo $rdd_future; ?>" class="<?php echo $id_baris; ?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width: 60px;font-size:7.5px; background-color:#cacbce"></td>
                              <?php
                              $id_row++;
                              $counter_truck++;
                            }
                          ?>
                          
                        </tr>
                      <?php
                      $id_baris++;
                    } ?>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php
      $no++;
      }
      
    }
    // atas penutup else 1
}else if($data_shift == 'DS'){
  $id_row = 55;
    foreach($data_schedule_detail[$date] as $key => $dsd){
      if($key == "DS"){

      ?>
        <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-<?php echo $header[$no]; ?> text-center">
                <b>Dinas <?php echo $shift[$no]; ?> <?php echo $date; ?></b>
              </div>
            <div class="card-body ">
              <div class="table-schedule table-responsive">      
                <table class="table table-hover">
                  <thead>
                    <tr align="center" style="padding:0px">
                        <th rowspan="2" style="padding:0px; font-weight: 500" width="120px">Time Slot</th>
                        <th colspan="5" style="vertical-align: middle;padding:5px; font-weight: 500" >Schedule</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($dsd as $d){
                      $counter_truck =0;
                      ?>
                        <tr>
                          <td style="background-color: <?php echo $bg_color[$no]; ?>; text-align:center ; font-size:11px"><b><?php echo substr(array_values($d)[0]["start_time"], 0,5)." - ".substr(array_values($d)[0]["end_time"], 0,5); ?></b></td>
                          <td width="2"></td>
                          <?php $total_truck = count(array_values($d)[0]["detail"]) ?>
                          <?php
                          $b = 0;                      
                          $count_zara2 = 0;

                           foreach(array_values($d)[0]["detail"] as $detail_truck){

                            ?>
                              <td id="div<?php echo $id_row++; ?>" truck_date="<?php echo $date; ?>" class="<?php echo $id_baris; ?>" style="width: 60px; font-size:7.5px; background-color:#cacbce"><div 
                              <?php
                                  $zara2 = $detail_truck->zara2;
                                  if($detail_truck->zara2 >0){                        
                                    $count_zara2++;
                                  }
                                  if($detail_truck->status == '2'){
                                  
                                ?> 
                                  truck_date="<?php echo $date; ?>"
                                  ondrop="drop(event)" 
                                  ondragover="allowDrop(event)"
                                  draggable="true" 
                                  ondragstart="drag(event)"
                                <?php
                                  }
                                ?>
                               id="drag<?php echo $id_drag++; ?>" ondblclick="getDetailedInformation('<?php echo $detail_truck->schedule_number;?>', <?php echo $detail_truck->status;  ?>)" value='<?php echo $detail_truck->id; ?>'>
                               
                               <?php 
                              
                              if($detail_truck->status > 0){
                                
                                truckIcon($detail_truck->dock_id, $detail_truck->status, $detail_truck->vendor_alias, 0, 1, 0, $detail_truck->police_number, $detail_truck->gr_material_doc);

                              }
                              
                               ?>
                                 
                               </div>
                             </td>
                            <?php
                            $counter_truck++;
                           }
                          ?>
                          
                          <?php 
                            while($counter_truck < 5){
                              ?>
                                <td id="div<?php echo $id_row; ?>" truck_date="<?php echo $date; ?>" class="<?php echo $id_baris; ?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width: 60px;font-size:7.5px; background-color:#cacbce"></td>
                              <?php
                              $id_row++;
                              $counter_truck++;
                            }
                          ?>
                          
                        </tr>
                      <?php
                      $id_baris++;
                    } ?>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php
      $no++;
      }
    }
    $id_row = 0;
    foreach($data_schedule_detail[$rdd_future] as $key => $dsd){
      if($key != "DS"){
        
      ?>
        <div class="col-md-4">
            <div class="card card-chart">
              <div class="card-header card-header-<?php echo $header[$no]; ?> text-center">
                <b>Dinas <?php echo $shift[$no]; ?> <?php echo $rdd_future; ?> </b>
              </div>
            <div class="card-body ">
              <div class="table-schedule table-responsive">      
                <table class="table table-hover">
                  <thead>
                    <tr align="center" style="padding:0px">
                        <th rowspan="2" style="padding:0px; font-weight: 500" width="120px">Time Slot</th>
                        <th colspan="5" style="vertical-align: middle;padding:5px; font-weight: 500" >Schedule</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($dsd as $d){
                      $counter_truck =0;
                      ?>
                        <tr>
                          <td style="background-color: <?php echo $bg_color[$no]; ?>; text-align:center; font-size:11px"><b><?php echo substr(array_values($d)[0]["start_time"], 0,5)." - ".substr(array_values($d)[0]["end_time"], 0,5); ?></b></td>
                          <td width="2"></td>
                          <?php $total_truck = count(array_values($d)[0]["detail"]) ?>
                          <?php
                          $b = 0;                      
                          $count_zara2 = 0;
                           foreach(array_values($d)[0]["detail"] as $detail_truck){
                            ?>
                              <td id="div<?php echo $id_row++; ?>" truck_date="<?php echo $rdd_future; ?>" class="<?php echo $id_baris; ?>" style="width: 60px; font-size:7.5px; background-color:#cacbce"><div 
                              <?php
                                  $zara2 = $detail_truck->zara2;
                                  if($detail_truck->zara2 >0){                        
                                    $count_zara2++;
                                  }
                                  if($detail_truck->status == '2'){
                                  
                                ?> 
                                  truck_date="<?php echo $rdd_future; ?>"
                                  ondrop="drop(event)" 
                                  ondragover="allowDrop(event)"
                                  draggable="true" 
                                  ondragstart="drag(event)"
                                    
                                <?php
                                  }
                                ?>
                               id="drag<?php echo $id_drag++; ?>" ondblclick="getDetailedInformation('<?php echo $detail_truck->schedule_number;?>', <?php echo $detail_truck->status;  ?>)" value='<?php echo $detail_truck->id; ?>'>
                               <?php 
                              if($detail_truck->status > 0){
                                truckIcon($detail_truck->dock_id, $detail_truck->status, $detail_truck->vendor_alias, 0, 1, 0, $detail_truck->police_number, $detail_truck->gr_material_doc);
                              }
                              
                               ?>
                               </div>
                             </td>
                            <?php
                            $counter_truck++;
                           }
                          ?>
                          
                          <?php 
                            while($counter_truck < 5){
                              ?>
                                <td id="div<?php echo $id_row; ?>" truck_date="<?php echo $rdd_future; ?>" class="<?php echo $id_baris; ?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width: 60px;font-size:7.5px; background-color:#cacbce"></td>
                              <?php
                              $id_row++;
                              $counter_truck++;
                            }
                          ?>
                          
                        </tr>
                      <?php
                      $id_baris++;
                    } ?>
                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <?php
      $no++;
      }
      
    }
    // atas penutup else 1
}

  ?>

  
</div>  