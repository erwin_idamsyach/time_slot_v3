<?php

class Search extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		isLogin();
	}

	public function index(){
		redirect('login');
	}

	public function delivery_note(){
		$year = $_GET['year'];
		$week = $_GET['week'];
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$dataDN = $this->db->query("SELECT
									b.status,
									c.vendor_code,
									c.vendor_name,
									c.vendor_alias,
									a.id_truck,
									a.delivery_date,
									a.id_time_slot,
									e.do_number,
									a.receipt,
									a.id_schedule_group as sn,
									CONCAT(d.start_time,' - ',d.end_time) as TIME_SLOT
								FROM
									tb_delivery_detail a
								INNER JOIN tb_schedule_detail b ON a.id_schedule_group = b.schedule_number
								INNER JOIN skin_master.ms_supplier c ON b.vendor_code = c.vendor_code
								INNER JOIN ms_time_slot d ON a.id_time_slot = d.id
								LEFT JOIN tb_delivery_invoice as e ON a.id_schedule_group = e.id_schedule
								WHERE YEAR(a.delivery_date) = '$year' AND (WEEK(a.delivery_date)+1 )= '$week'
								AND b.vendor_code LIKE '%$vendor_code%'
								GROUP BY a.receipt
								");
		
		$data['dn_list'] = $dataDN;
		getHTML('search/delivery_note', $data);
	}

	public function material(){
		$supplierDN = $this->db->query("SELECT vendor_name, vendor_alias, vendor_code FROM skin_master.ms_supplier");
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$getMaterial = $this->db->query("SELECT a.id, b.material_name, a.material_code FROM tb_rds_detail AS a INNER JOIN skin_master.ms_material AS b ON a.material_code = b.material_sku WHERE a.vendor_code = '$vendor_code' GROUP BY a.material_code" );
		$get_category = $this->db->query("SELECT category FROM ms_category");
		$data['category'] = $get_category;
		$data['dn_supplier'] = $supplierDN;
		$data['material']	 = $getMaterial;
		getHTML('search/material', $data);
	}

	public function dn_action(){
		$keyword = $this->input->get('keyword');
		$dataDO = $this->db->query("SELECT * FROM tb_delivery_detail INNER JOIN tb_schedule_detail ON tb_schedule_detail.id = tb_delivery_detail.id_schedule_group INNER JOIN ms_time_slot ON tb_schedule_detail.id_time_slot = ms_time_slot.id WHERE do_number='$keyword'");
		if($dataDO->num_rows() > 0){
			$data['data'] = $dataDO;
			$this->load->view('search/dn_result', $data);
		}else{
		}
	}

	public function ajax_get_material(){
		$vendor_code = $this->input->get('supplier');
		$data = $this->db->query("SELECT b.material_name, a.material_code FROM tb_rds_detail AS a INNER JOIN skin_master.ms_material AS b ON a.material_code = b.material_sku WHERE a.vendor_code = '$vendor_code' GROUP BY a.material_code")->result();
			echo "<option value=''>-- SELECT ALL MATERIAL --</option>";
		foreach ($data as $get) {
			echo "<option value='".$get->material_code."'>".$get->material_code.' - '.$get->material_name."</option>";
		}

	}


	public function material_action(){
		if($this->session->userdata('sess_role_no') != '3'){
			$supplier = $this->input->get('supplier');	
		}else{
			$supplier = $this->session->userdata('sess_vendor_code');
		}
		
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$category = $this->input->get('category');
		$material = $this->input->get('keyword');
		if($supplier != "" AND $material == ""){
			
			$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,c.vendor_name,
										 SUM(b.receive_amount) as receive_amount,
										 (SELECT SUM(b.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND status='1' GROUP BY id_schedule) AS total, 
										 (SELECT status FROM tb_schedule_detail 
										 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS C_STATUS,
										 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name, 
										 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name 
										 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
										 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
										 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
										 WHERE a.week LIKE '%$week%' AND YEAR(a.requested_delivery_date) = '$year'
										 AND a.category LIKE '%$category%' 
										 AND a.vendor_code = '$supplier'
										 GROUP BY b.id
										 ORDER BY
										 	a.requested_delivery_date,
										 	a.shift ASC
				");
		}elseif($supplier != '' AND $material != ''){
			$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,c.vendor_name,
										 SUM(b.receive_amount) as receive_amount,
										 (SELECT SUM(b.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND status='1' GROUP BY id_schedule) AS total, 
										 (SELECT status FROM tb_schedule_detail 
										 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS C_STATUS,
										 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name, 
										 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name 
										 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
										 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
										 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
										 WHERE a.week LIKE '%$week%' AND YEAR(a.requested_delivery_date) = '$year'
										 AND a.category LIKE '%$category%' 
										 AND a.vendor_code = '$supplier' AND a.material_code LIKE '%$material%'
										 GROUP BY b.id
										 ORDER BY
										 	a.requested_delivery_date,
										 	a.shift ASC
				");
			
		}
		else if($supplier == "" AND $material == ""){
			$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,c.vendor_name,
										 SUM(b.receive_amount) as receive_amount,
										 (SELECT SUM(b.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND status='1' GROUP BY id_schedule) AS total, 
										 (SELECT status FROM tb_schedule_detail 
										 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS C_STATUS,
										 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name, 
										 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name 
										 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
										 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
										 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
										 WHERE a.week LIKE '%$week%' AND YEAR(a.requested_delivery_date) = '$year'
										 AND a.category LIKE '%$category%' 
										 -- AND material_code ='67750280' 
										 GROUP BY b.id
										 ORDER BY
										 	a.requested_delivery_date,
										 	a.shift ASC
				");
			
		}

		
		
		$data['search_result'] = $getData;
		$this->load->view('search/material_result', $data);
	}

}

?>