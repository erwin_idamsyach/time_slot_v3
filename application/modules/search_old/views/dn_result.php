<?php
foreach($data->result() as $del){}
?>
<hr>
<div class="row">
	<div class="col-md-4" style="border-right: 1px solid #000;">
		<div class="form-group">
			<label>RDD</label><br>
			<label><?php 
			$date = date_create($del->delivery_date);
			$df = date_format($date, "D, d M Y");
			echo $df; ?></label>
		</div>
		<div class="form-group">
			<label>Time</label><br>
			<label><p id="#remain"><?php echo $del->start_time." - ".$del->end_time; ?></p></label>
		</div>
		<hr style="border-color:grey;">
		<div class="form-group">
			<label>Truck No</label><br>
			<label class="unedit"><?php echo $del->id_truck; ?></label>
		</div>
		<div class="form-group">
			<label>Driver Name</label><br>
			<label class="unedit"><?php echo $del->driver_name; ?></label>
		</div>
		<div class="form-group">
			<label>Phone Number</label><br>
			<label class="unedit"><?php echo $del->phone_number; ?></label>
		</div>
		<div class="form-group">
			<label>Delivery Note</label><br>
			<label class="unedit"><?php echo $del->do_number; ?></label>
		</div>
	</div>
</div>