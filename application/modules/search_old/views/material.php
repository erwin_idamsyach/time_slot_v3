<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-pallet"></i>
                    </div>
                    <h4 class="card-title">Search Material</h4>
                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->
      <div class="box">
            
            <div class="box-body">
                <div class="row">
            <div class="col-md-2">
              <?php $year=$_GET['year']; $week=$_GET['week']; ?>
                     <div class="form-group">
                  <label style="margin-left:70px">Year</label>
                  <nav aria-label="Page navigation example">
                      <ul class="pagination pagination-primary">
                        
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>&category=<?php if(isset($_GET['category'])){ echo $_GET['category']; }?>"><?php echo $year-1; ?></a>
                        </li>
                        
                        <li class="page-item active">
                          <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php if(isset($_GET['category'])){echo $_GET['category']; }?>"><?php echo $year; ?></a>
                        </li>

                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>&category=<?php if(isset($_GET['category'])){ echo $_GET['category']; }?>"><?php echo $year+1; ?></a>
                        </li>

                      </ul>
                    </nav>
                  </div>
          </div>

          <div class="col-md-3 col-xl-3 col-sm-6">
          <div class="form-group">
            <label style="margin-left:85px">Week</label>
            <nav aria-label="Page navigation example">
                <ul class="pagination pagination-primary">
                  
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>&category=<?php if(isset($_GET['category'])){ echo $_GET['category']; }?>"><?php echo $week-2; ?></a>
                  </li>
                   <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>&category=<?php if(isset($_GET['category'])){ echo $_GET['category']; }?>"><?php echo $week-1; ?></a>
                  </li>
                  
                  <li class="page-item active">
                    <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php if(isset($_GET['category'])){ echo $_GET['category']; }?>"><?php echo $week; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>&category=<?php if(isset($_GET['category'])){ echo $_GET['category']; }?>"><?php echo $week+1; ?></a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>&category=<?php if(isset($_GET['category'])){ echo $_GET['category']; }?>"><?php echo $week+2; ?></a>
                  </li>
                  

                </ul>
              </nav>
            </div>
          </div>
      </div>

                <div class="row">
                    
                    <div class="col-3" style="margin-top:-10px;<?php if($this->session->userdata('sess_role_no') == '3'){ echo "display:none";} ?>" >
                      
                        <div class="form-group">
                            <label>Choose Supplier</label>
                            <select name="supplier" id="supplier" onchange="select_material(this.value)" class="form-control select2-init">
                                <option value="">-- SELECT ALL SUPPLIER --</option>
                                <?php
                                foreach($dn_supplier->result() as $get){
                                    ?>
                                <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_name." - ".$get->vendor_alias; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-3" style="margin-top:-10px">
                        <div class="form-group">
                            <label>Choose Material</label>
                            <select name="keyword" <?php if($this->session->userdata('sess_role_no') != '3'){echo "disabled='true'";} ?> id="materialz" class="form-control select2-init">
                              
                                <option value="">-- SELECT ALL MATERIAL --</option>      
                              <?php if($this->session->userdata('sess_role_no') == '3' ){  
                                 foreach ($material->result() as $data) {
                                    ?>
                                <option value="<?php echo $data->material_code; ?>"><?php echo $data->material_code.' - '.$data->material_code." - ".$data->material_name; ?> </option>
                                    <?php 
                                  }
                                } ?>                          
                            </select>
                        </div>
                    </div>

                   <!--  <div class="col-md-2" style="margin-top:20px">
                        <div class="form-group">
                            <label style="margin-top:-8px">Choose Calender</label>
                            <input id="calender" disabled="true" type="Date" value="" name="calender" class="form-control">
                        </div>
                    </div> -->
                    
                     <div class="col-2" style="margin-top:-10px">
                        <button type="button" onclick="window.location=('<?php echo base_url() ?>search/material?year=<?php if(isset($_GET['year'])){echo $_GET['year'];} ?>&week=<?php if(isset($_GET['week'])){echo $_GET['week'];} ?>&category=<?php if(isset($_GET['category'])){echo $_GET['category'];} ?>&supplier='+$('#supplier').val()+'&material='+$('#materialz').val());" class="btn btn-primary btn-sm" style="margin-top: 34px;">
                            <i class="fas fa-search"></i> Filter
                        </button>
                    </div>
                   
                </div>

                <div class="row">
                    <style>.category-4 .btn-group .btn.btn-info{padding : 15px;}</style>
                        <div class="col-4 category-4" style="padding: 0;position: absolute;margin-top:-10px;margin-left:15px">
                            <div class="btn-group" data-toggle="buttons">
                              <?php
                                foreach ($category->result() as $category) {
                                ?>
                                  <button type="button" class="btn  btn-info <?php if($_GET['category'] == $category->category){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>search/material?category=<?php echo $category->category; ?>&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>')"><?php echo $category->category; ?></button>
                                <?php
                                }
                               ?>
                            </div>
                        </div>
                </div>

                <div id="area-result">
                    
                </div>
            </div>
        </div>
    </section>
</div>

</div>
</div>
</div>
</div>
</div>
</div>