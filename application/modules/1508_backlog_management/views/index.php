
<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i style="font-size:30px" class="fas fa-stream"></i>
              </div>
              <h4 class="card-title">Backlog Management</h4>
            </div>
            <div class="card-body ">
    <!-- Content Header (Page header) -->
        <div class="box">
            <div class="row">
              <div class="col-md-2">
                <?php $year=$_GET['year']; $week=$_GET['week']; ?>
                       <div class="form-group">
                    <label style="margin-left:70px">Year</label>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination pagination-primary">
                          
                          <li class="page-item">
                            <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>"><?php echo $year-1; ?></a>
                          </li>
                          
                          <li class="page-item active">
                            <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $year; ?></a>
                          </li>

                          <li class="page-item">
                            <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>"><?php echo $year+1; ?></a>
                          </li>

                        </ul>
                      </nav>
                    </div>
              </div>

          <div class="col-md-3 col-xl-3 col-sm-6">
            <div class="form-group">
              <label style="margin-left:85px">Week</label>
              <nav aria-label="Page navigation example">
                  <ul class="pagination pagination-primary">
                    
                    <li class="page-item">
                      <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>"><?php echo $week-2; ?></a>
                    </li>
                     <li class="page-item">
                      <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>"><?php echo $week-1; ?></a>
                    </li>
                    
                    <li class="page-item active">
                      <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $week; ?></a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>"><?php echo $week+1; ?></a>
                    </li>
                    <li class="page-item">
                      <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>"><?php echo $week+2; ?></a>
                    </li>
                  </ul>
                </nav>
              </div>
          </div>
      </div>

<div class="row" style="text-align: right">
  <div class="col">
    <?php
      $zz = 0;
      ?>   
                  <?php
      while($zz < count($date_list)){
        $dc = date_create($date_list[$zz]);
        $df = date_format($dc, "D d M Y");
        $dl = date_format($dc, "Y-m-d");
        
        ?>
       <button type="button" onclick="window.location=('<?php base_url() ?>backlog_management?year=<?php echo date('Y', strtotime($dl)); ?>&week=<?php echo date('W', strtotime($dl)); ?>&date=<?php echo date('Y-m-d', strtotime($dl)); ?>')" style="width: 130px;font-size:12px" class="btn btn-primary btn-sm <?php  if($dl == $this->input->get('date')){echo "active";} ?>"><?php echo $df; ?></button>

        <?php
        $zz++;
      }
    ?>
  </div>
</div>

<div class="row">
  <div class="col">
    <div class="box-body">
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="table">
                    <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px;white-space: nowrap;">
                        <tr>
                            <th style="font-size:14px" class="text-center">No</th>
                            <th style="font-size:14px" class="text-center">Receipt</th>
                            <th style="font-size:14px" class="text-center">Supplier</th>
                            <th style="font-size:14px" class="text-center">Deliver To</th>
                            <th style="font-size:14px" class="text-center">No Truck</th>
                            <th style="font-size:14px" class="text-center">RDD</th>
                            <th style="font-size:14px" class="text-center">Time Slot</th>
                            <th style="font-size:14px" class="text-center">Arrived</th>
                            <th style="font-size:14px" class="text-center">Status</th>
                            <th style="font-size:14px" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach($dn_list->result() as $get){
                            ?>
                        <tr>
                            <td style="font-size:12px; text-align: center"><?php echo $no++; ?></td>
                            <td style="font-size:12px"><?php echo $get->receipt; ?></td>     
                            <td style="font-size:12px"><?php echo $get->vendor_name." ( ".$get->vendor_alias." )"; ?></td>
                            <td style="font-size:12px">9003</td>
                            <td style="font-size:12px"><?php echo $get->id_truck; ?></td>
                            <td style="font-size:12px"><?php
                            $rdd = date_create($get->delivery_date);
                            $df = date_format($rdd, "D, d M Y");
                            echo $df;
                            ?></td>
                            <td style="font-size:12px"><?php echo $get->TIME_SLOT; ?></td>
                            <td style="font-size:12px"><?php echo $get->arrive_date.' '.$get->arrive_time; ?></td>
                            
                            <td  style="font-size:10px"class="text-center" style="font-size:11px">
                                <?php LegendIcon($get->status); ?>
                            </td>
                            <td style="font-size:12px" class="text-center">
                                <button class="btn btn-info btn-sm" onclick="getDetailedInformation(<?php echo $get->sn;?>, <?php echo $get->status;  ?>)">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <button class="btn btn-warning btn-sm" onclick="changeSchedule(<?php echo $get->sn;?>, <?php echo $get->status;  ?>)">
                                    <i class="fa fa-business-time"></i>
                                </button>
                            </td>
                        </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
              </div>
            </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i style="font-size:30px" class="fas fa-history"></i>
        </div>
        <h4 class="card-title">Backlog History</h4>
      </div>
      <div class="card-body ">
        <div class="table-responsive">
          <table class="table table-bordered table-hover data-table">
            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px;white-space: nowrap;">
              <tr>
                <th style="font-size:14px" class="text-center">No</th>
                <th style="font-size:14px" class="text-center">Receipt</th>
                <th style="font-size:14px" class="text-center">Supplier</th>
                <th style="font-size:14px" class="text-center">Deliver To</th>
                <th style="font-size:14px" class="text-center">No Truck</th>
                <th style="font-size:14px" class="text-center">RDD</th>
                <th style="font-size:14px" class="text-center">Time Slot</th>
                <th style="font-size:14px" class="text-center">Update RDD</th>
                <th style="font-size:14px" class="text-center">Update Time Slot</th>
                <th style="font-size:14px" class="text-center">Arrived Time</th>
                <th style="font-size:14px" class="text-center">Status</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1; foreach($data_history_backlog as $data){ ?>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $data->receipt; ?></td>
                  <td><?php echo $data->vendor_alias; ?></td>
                  <td><?php echo 9003; ?></td>
                  <td><?php echo $data->police_number; ?></td>
                  <td><?php echo $data->rdd; ?></td>
                  <td><?php echo $data->time_slot; ?></td>
                  <td><?php echo $data->update_rdd; ?></td>
                  <td><?php echo $data->update_time_slot; ?></td>
                  <td><?php echo $data->arrive_time; ?></td>
                  <td><?php echo ($data->status == 1)?"Urgent":""; ?></td>

                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

        </div>
    </section>
</div>
</div>
</div>
</div>

<div id="tabel_truck">
</div>

</div>
</div>

<!-- //Modal -->
<!-- Button trigger modal -->
<button type="button" hidden id="btn_modal" class="btn btn-primary" data-toggle="modal" data-target="#modal_backlog">
  modal backlog
</button>

<!-- Modal -->
<div class="modal fade" id="modal_backlog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Change Schedule</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="row">
            <div class="col-6">
              <div class="row">
                  <ul class="nav nav-pills nav-pills-icons" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#Malam" role="tab" data-toggle="tab">
                            <!-- <i class="material-icons">DM</i> -->
                            DM
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#Pagi" role="tab" data-toggle="tab">
                            <!-- <i class="material-icons">schedule</i> -->
                            DP
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#Siang" role="tab" data-toggle="tab">
                            <!-- <i class="material-icons">list</i> -->
                            DS
                        </a>
                    </li>
                </ul>
              </div>
           
              <div class="row">
                <?php 
                    $header = array(
                      0 => "primary",
                      1 => "info", 
                      2 => "success"
                    );

                    $shift = array(
                      0 => "Malam",
                      1 => "Pagi",
                      2 => "Siang"
                    );

                    $shift2 = array(
                      "DM",
                      "DP",
                      "DS"
                    );
                    ?>
                  <div class="tab-content tab-space">
                  <?php 
                  $no = 0;
                  $id_row = 0;
                  $id_baris = 0;
                  $id_drag = 0;
                    foreach($data_schedule_dtnow as $dsd){
                      ?>
                      <div class="tab-pane <?php echo ($shift[$no]== "Malam")?"active":""; ?>" id="<?php echo $shift[$no]; ?>">
                      <div class="col">
                          <div class="card card-chart">
                            <div class="card-header card-header-<?php echo $header[$no]; ?> text-center">
                              <b>Dinas <?php echo $shift[$no]; ?> </b>
                            </div>
                          <div class="card-body ">
                            <div class="table-schedule table-responsive">      
                              <table class="table table-hover">
                                <thead>
                                  <tr align="center">
                                    <th width="120px">Time Slot</th>
                                    <th colspan="4" style="vertical-align: middle;">Schedule</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($dsd as $d){
                                    $counter_truck =0;
                                    ?>
                                      <tr>
                                        <td><b><?php echo substr(array_values($d)[0]["start_time"], 0,5)." - ".substr(array_values($d)[0]["end_time"], 0,5); ?></b></td>
                                        <?php $total_truck = count(array_values($d)[0]["detail"]) ?>
                                        <?php
                                        $b = 0;
                                         foreach(array_values($d)[0]["detail"] as $detail_truck){
                                          ?>
                                            <td id="div<?php echo $id_row; ?>" class="<?php echo $id_baris; ?>" style="width: 60px; font-size:7.5px;"><div 
                                            <?php
                                                if($detail_truck->status == '2'){
                                                 
                                              ?> 
                                                ondrop="drop(event)" 
                                                ondragover="allowDrop(event)"
                                                draggable="true" 
                                                ondragstart="drag(event)" 
                                              <?php
                                                }
                                              ?>
                                             id="drag<?php echo $id_drag++; ?>" value='<?php echo $detail_truck->id; ?>'><?php truckIcon($detail_truck->status,$detail_truck->vendor_alias); ?></div></td>
                                          <?php
                                          $counter_truck++;
                                         }
                                        ?>
                                        
                                        <?php 
                                          while($counter_truck < 5){
                                            ?>
                                              <td id="div<?php echo $id_row; ?>" class="<?php echo $id_baris; ?>" ondrop="drop(event)" ondragover="allowDrop(event)" style="width: 60px;font-size:7.5px"></td>
                                            <?php
                                            $id_row++;
                                            $counter_truck++;
                                          }
                                        ?>
                                      </tr>
                                    <?php
                                    $id_baris++;
                                  } ?>
                                  
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                      <?php
                      $no++;
                    }
                  ?>
              </div>
              </div>
            </div>
            <div class="col">
              <div class="row">

                <div class="card card-chart" style="margin-top:90px">
                    <div class="card-header card-header-success text-center">
                      <b>FORM Change Schedule BackLog</b>
                    </div>
                  <div class="card-body ">
                    <form method="GET" action="<?php echo base_url(); ?>backlog_management/changeSchedule">
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label>Schedule Time</label>
                          <input type="hidden" value="" id="sn" name="sn">
                          <input type="hidden" value="" id="schedule_id" name="schedule_now">
                          <input type="text" class="form-control" name="" style="background-color:white" id="schedule_now" readonly value="">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <label>Change Date</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                          
                        <select onchange="select_change(this.value)" required="" name="date" class="form-control">
                          <option value="">-- SELECT DATE --</option>
                          <option value="<?php echo date('Y-m-d', strtotime("0 day")); ?>"><?php echo date('Y-m-d', strtotime("0 day")); ?></option>
                          <option value="<?php echo date('Y-m-d', strtotime("+1 day")); ?>"><?php echo date('Y-m-d', strtotime("+1 day")); ?></option>
                        </select>
                          
                        </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <br>
                        <label>Change Time Slot</label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                            
                              <div id="select_time_slot">
                                <select class="form-control">
                                  <option>Select Time Slot</option>
                                </select>
                              </div>
                          
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-check" style="margin-top:25px">
                            <label class="form-check-label">
                                <input class="form-check-input" name="urgent" type="checkbox" value="true">
                                  Urgent
                                <span class="form-check-sign">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                    
                  </div>
                </div>
          
            </div>
          </div>
        </div>
        
        
      </div>
    </div>
  </div>
</div>
</div>
<script>
  select_change();
  function select_change(e){
   if(e === undefined){
      e = "<?php echo date('Y-m-d'); ?>";
   }
   $.ajax({
    url: "<?php echo base_url() ?>backlog_management/ajax_select_date",
    data : {
      "date" : e
    },
    dataType : "HTML",
    success: function(ev){
        $('#select_time_slot').html(ev);
        
    }, error: function(ev){
      alert("Something Error");
    }
   });
  } 
  </script>



