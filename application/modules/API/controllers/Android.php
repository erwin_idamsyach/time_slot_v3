<?php

class Android extends CI_Controller{

	protected $tb_location = "tb_location";

	function __Construct(){
		parent::__Construct();
		$this->load->model('schedule/Schedule_model');
		$this->load->model('api/OS_Model_Api');
		$this->load->model('outstanding/OS_Model');
	}

	public function get_arrived_data(){

		date_default_timezone_set("Asia/Jakarta");

		$receipt = $this->input->get('receipt');

		$data_receipt = $this->db->query("
			SELECT * FROM `tb_delivery_detail` WHERE receipt = ?", 
			[$receipt])->row();

		if (empty($data_receipt)) {

			echo json_encode([
				"status" => "error",
				"message" => "no data found"
			]);

			return;
		}

		if ($data_receipt->arrive_date == null) {

			echo json_encode([
				"status" => "error",
				"message" => "no arrived confirmation yet"
			]);

			return;
		}

		if ($data_receipt->receive_date != null) {

			echo json_encode([
				"status" => "error",
				"message" => "material has been received. please scan another receipt"
			]);

			return;
		}

		$data_truck = $this->db->query("
			SELECT * FROM `ms_truck` WHERE id = ?", 
			[$data_receipt->id_truck])->row();

		$data_driver = $this->db->query("
			SELECT * FROM `ms_driver` WHERE id = ?", 
			[$data_receipt->id_driver])->row();

		$data_scheduler = $this->db->query("
			SELECT * FROM `tb_scheduler` WHERE schedule_number = ?", 
			[$data_receipt->id_schedule_group])->row();

		$data_supplier = $this->db->query("
			SELECT * FROM `skin_master`.`ms_supplier` WHERE vendor_code = ?", 
			[$data_driver->vendor_code])->row();

		$data_material = $this->db->query("SELECT
							c.id,
							c.id_schedule,
							a.po_number,
							a.po_line_item,
							c.quantity,
							c.batch_number,
							c.prod_date,
							c.prod_date,
							c.exp_date,
							c.schedule_number,
							c.receive_amount,
							a.material_code,
							b.material_name,
							b.exp,
							a.id as idz,
							a.requested_delivery_date,
							a.shift,
							a.qty,
							a.uom,
							a.uom_plt,
							a.req_pallet,
							(c.quantity*a.uom_plt) as SEND_AMOUNT,
							a.is_non_full,
							(SELECT amount FROM tb_return_form WHERE id_scheduler = c.id) AS return_receipt
						FROM
							tb_scheduler c
						INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
						LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
						WHERE
							schedule_number = ?
						ORDER BY a.po_number ASC", [$data_receipt->id_schedule_group])->result();

		// update scanned_at
		$data = $this->db->where('receipt', $receipt)
			->update('tb_delivery_detail', [
				'scanned_at' => date('Y-m-d H:i:s')
			]);

		echo json_encode([
			"receipt" => $data_receipt,
			"truck" => $data_truck,
			"driver" => $data_driver,
			"supplier" => $data_supplier,
			"material" => $data_material
		]);

		return;
	}

	public function received(){

		date_default_timezone_set("Asia/Jakarta");

		$data = json_decode($this->input->post('json_data'));

		// id_schedule_group di delivery_detail
		$id = $data->id_schedule_group; //id_schedule_group
		$date = date("Y-m-d H:i:s");
		$received_date = date("Y-m-d");
		$received_hour = date("H");
		$received_minute = date("i");
		$time = $received_hour.":".$received_minute;
		$received_time = date('H:i',strtotime($time));
		
		try{
			$this->db->trans_start();

			$get_material = $this->db->query("SELECT a.id FROM tb_rds_detail AS a INNER JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE b.schedule_number = '$id' ")->result();

			$date_now = Date('Y-m-d H:i');
			$user_id = $data->id_user;

			foreach ($get_material as $get) {
				$this->db->query("UPDATE tb_rds_detail SET update_at = '$date_now', update_by = '$user_id' WHERE id = '$get->id' ");
			}

			$getData = $this->db->query("SELECT b.vendor_code, a.receipt FROM tb_delivery_detail AS a INNER JOIN tb_schedule_detail AS b ON a.id_schedule_group	 = b.schedule_number WHERE a.id_schedule_group = '$id' ")->row();

			$receipt = $getData->receipt;
			$vendor_code = $getData->vendor_code;

			$no = 0;

			foreach ($data->list_material as $get) {
				if($get->is_non_full == 0){
					$this->db->where('id',$get->id);
					$this->db->update('tb_scheduler', array( "receive_amount" => $get->qty ));
				}else{
					$this->db->where('id',$get->id);
					$this->db->update('tb_scheduler', array( "rec_qty" => $get->qty, "receive_amount" => 1 ));
				}

				$history = array(
					"date" 			=> Date('Y-m-d'),
					"time"			=> date('H:i:s'),
					"action"		=> "Received via Mobile Apps",
					"by_who"		=> $data->id_user,
					"table_join"	=> "tb_delivery_detail",
					"id_join"		=> $id,
					"value"			=> $receipt,
					"value2"		=> "AT ".date('d-m-Y',strtotime($received_date)).' '.date('H:i:s',strtotime($received_time)),
					"description"	=> "PO Number : ".$get->po_number.", Line : ".$get->po_line.", Material Code : ".$get->material_code,
					"select_join"	=> "arrive_date",
					"author"		=> 4,
					"vendor_code"	=> $vendor_code
				);
				$this->db->insert('tb_history', $history);

				$mm = array(
					"id_schedule" => $get->id_schedule,
					"quantity"	  => $get->qty,
					"action"	  => "Received",
					"created_by"  => $data->id_user
				);

				add_material_movement($mm);
			}

			$data = array(
				"receive_date"	=> $received_date,
				"receive_time"	=> $received_time,
				"received_by"	=> $data->id_user,
				"reason"		=> "from mobile"
			);

			$this->db->where("id_schedule_group",$id);
			$this->db->update("tb_delivery_detail", $data);

			$this->db->query("UPDATE tb_schedule_detail SET status = '3' WHERE schedule_number = '$id' ");

			$this->db->trans_complete();
			
			if ($this->db->trans_status() === FALSE)
			{
				echo json_encode([
					"status" => "error",
					"message" => "query error, Error : ".$this->db->_error_message()
				]);

				return;

			}else{

				echo json_encode([
					"status" => "sukses",
					"message" => "Data succesfully saved"
				]);

				return;

			}
		}catch(Exception $e){
			echo "SOMETHING WRONG! ERROR : ".$e->getMessage();
		}

	}

	public function arrived(){

		date_default_timezone_set("Asia/Jakarta");

		$id = $this->input->get('id');
		$arrive_date = $this->input->get('arrive_date');
		$user_id = $this->input->get('id_user');
		$time = $this->input->get('arrive_time');
		
		$get_material = $this->db->query("SELECT a.id FROM tB_rds_detail AS a INNER JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE b.schedule_number = '$id' ")
			->result();

		$date_now = Date('Y-m-d H:i');

		$this->db->trans_start();

		foreach ($get_material as $get) {
			$this->db->query("UPDATE tb_rds_detail 
				SET update_at = '$date_now', update_by = '$user_id' WHERE id = '$get->id' ");
		}

		$arrive_time = date('H:i:s',strtotime($time));
		$arrive_date = date('Y-m-d H:i:s', strtotime($arrive_date));

		$this->db->query("UPDATE tb_schedule_detail SET status = '2' WHERE schedule_number = '$id' ");
		$data = array(
			"arrive_date"	=> $arrive_date,
			"arrive_time"	=> $arrive_time,
			"arrived_by"	=> $user_id
		);

		$this->db->where("id_schedule_group",$id);
		$this->db->update("tb_delivery_detail", $data);

		$receipt = $this->db->query("SELECT receipt 
			FROM tb_delivery_detail WHERE id_schedule_group = '$id' ")->row()->receipt;

		$history = array(
  			"date" 			=> date('Y-m-d'),
  			"time"			=> date('H:i:s'),
  			"action"		=> "Arrive",
  			"by_who"		=> $user_id,
  			"table_join"	=> "tb_delivery_detail",
  			"id_join"		=> $id,
  			"value"			=> $receipt,
  			"value2"		=> date('d-m-Y',strtotime($arrive_date)).' '.date('H:i:s',strtotime($arrive_time)),
  			"description"	=> "AT",
  			"select_join"	=> "arrive_date",
  			"author"		=> 4
	  	);

	  	$this->db->insert('tb_history', $history);
	  	$this->db->trans_complete();

	  	if ($this->db->trans_status() === FALSE)
		{
			echo json_encode([
				"status" => "error"
			]);

		}else{

			echo json_encode([
				"status" => "sukses"
			]);

		}

	}

	public function receipt_info(){
		
		$time_slot = $this->db->query("SELECT start_time, end_time 
									   FROM ms_time_slot "
									);

		$sn = $this->input->get('id_schedule_group');
		$del = $this->db->query("SELECT a.*,c.vendor_code,e.vendor_name,e.vendor_alias,d.shift,d.start_time,d.end_time, a.receipt FROM tb_delivery_detail as a
				 LEFT JOIN tb_scheduler as b ON a.id_schedule_group = b.schedule_number
				 LEFT JOIN tb_schedule_detail as c ON c.schedule_number = a.id_schedule_group
				 LEFT JOIN skin_master.ms_supplier as e ON e.vendor_code = c.vendor_code
				 LEFT JOIN ms_time_slot as d ON c.id_time_slot = d.slot_number				 
			     WHERE id_schedule_group ='$sn' ")->row();

		$data_material = $this->db->query("SELECT
							c.id,
							c.id_schedule,
							a.po_number,
							a.po_line_item,
							c.quantity,
							c.batch_number,
							c.prod_date,
							c.prod_date,
							c.vendor_batch,
							c.exp_date,
							c.schedule_number,
							IF(a.is_non_full = 1, c.rec_qty, c.receive_amount) AS receive_amount,
							a.material_code,
							b.material_name,
							b.exp,
							a.id as idz,
							a.requested_delivery_date,
							a.shift,
							a.qty,
							a.uom,
							a.uom_plt,
							a.sap_line,
							a.req_pallet,
							(c.quantity*a.uom_plt) as SEND_AMOUNT,
							(SELECT amount FROM tb_return_form WHERE id_scheduler = c.id) AS return_receipt
						FROM
							tb_scheduler c
						INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
						LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
						WHERE
							schedule_number = ?
						ORDER BY a.po_number ASC", [$sn])->result();

		if(!is_null($del)){
			$data_driver = $this->db->query("
				SELECT * FROM `ms_driver` WHERE id = ?", 
				[$del->id_driver])->row();
		}else{
			$data_driver = null;
		}

		$db_dock = "docking_management_v3";
		$db_tsb = "db_time_slot_v3";
		// $db_tsb = "tsb_dev3";

		// $data_timed = $this->db->query("SELECT tdd.receipt, tdd.id_schedule_group, 
		// 	tdd.id_driver, tdd.gr_success_time,
		// 	CONCAT(tdd.receive_date, ' ', tdd.receive_time) as date_received,
		// 	TIMESTAMPDIFF(MINUTE,get_in.time, get_out.time) as dueling_time,
		// 	TIMESTAMPDIFF(MINUTE,dock_in.time, dock_out.time) as unloading_time,
		// 	TIMESTAMPDIFF(MINUTE,CONCAT(tdd.receive_date, ' ', tdd.receive_time), tdd.gr_success_time) as trx_time,
  //           get_in.date as get_in_date, 
		// 	get_in.time as get_in_time,
		// 	get_out.date as get_out_date,
		// 	get_out.time as get_out_time,
		// 	dock_in.date as dock_in_date, 
		// 	dock_in.time as dock_in_time,
		// 	dock_out.date as dock_out_date, 
		// 	dock_out.time as dock_out_time,
		// 	tdd.id_time_slot, tdd.id_truck, mst.police_number FROM 
		// 	${db_tsb}.tb_delivery_detail AS tdd
		// 	LEFT JOIN ${db_tsb}.ms_truck AS mst
		// 	ON tdd.id_truck = mst.id
  //           LEFT JOIN ${db_dock}.tb_get_in_history AS get_in
		// 	ON tdd.id_driver = get_in.id_driver 
		// 	LEFT JOIN ${db_dock}.tb_get_out_history AS get_out
		// 	ON tdd.id_driver = get_out.id_driver 
		// 	LEFT JOIN ${db_dock}.tb_dock_out_history AS dock_out
		// 	ON tdd.id_driver = dock_out.id_driver 
		// 	LEFT JOIN ${db_dock}.tb_dock_in_history AS dock_in
		// 	ON tdd.id_driver = dock_in.id_driver WHERE id_schedule_group = ? 
		// 	ORDER BY get_in.date DESC", [$sn])->row();

		$data_timed = $this->db->query("SELECT tdd.*, tt.*,
			CONCAT(tdd.receive_date, ' ', tdd.receive_time) as date_received,
			TIMESTAMPDIFF(MINUTE,tt.gate_in, tt.gate_out) as dueling_time,
			TIMESTAMPDIFF(MINUTE,tt.dock_in, tt.dock_out) as unloading_time,
			-- TIMESTAMPDIFF(MINUTE,CONCAT(tdd.receive_date, ' ', tdd.receive_time), tdd.gr_success_time) as trx_time FROM 
			TIMESTAMPDIFF(MINUTE, tdd.scanned_at, tdd.gr_success_time) as trx_time FROM 
			${db_tsb}.tb_delivery_detail AS tdd
            LEFT JOIN ${db_dock}.tb_time as tt
            ON tdd.receipt = tt.receipt 
			WHERE tdd.id_schedule_group = ?", [$sn])->row();



		echo json_encode([
			"data_receipt" => $del,
			"data_driver" => $data_driver,
			"data_material" => $data_material,
			"data_timed" => $data_timed,
		]);

	}

	public function get_detailed_information(){

		$key = $this->input->get('id_schedule');
		$count_do = $this->db->query("SELECT do_number FROM tb_delivery_invoice WHERE id_schedule = '$key' ")->num_rows();
		$sch_detail = $this->Schedule_model->getScheduleDetail($key);
		$del_detail = $this->Schedule_model->getDeliveryInfo($key);
		$dn_download = $this->Schedule_model->getdnDownload($key);
		$coa_download = $this->db->query("SELECT coa FROM tb_delivery_coa WHERE id_schedule = '$key' ");
		/*echo json_encode($del_detail->result()); die();*/

		$data['sch_detail'] = $sch_detail['data_list']->result();
		$data['del'] = $del_detail->row();
		$data['dn_download'] = $dn_download->result();
		$data['coa_download'] = $coa_download->result();

		echo json_encode($data);
		
	}

	public function get_date_and_time()
	{
		$receipt = $this->input->get('receipt');
		$data = $this->db->where('receipt', $receipt)
			->get('tb_delivery_detail')
			->row();
		
		date_default_timezone_set("Asia/Bangkok");
		echo json_encode([
			"date" => date('m/d/Y'),
			"time" => date("H:i"),
			"data" => $data
		]);
	}
	public function save_edit_user_profile($value=''){

		$id_user = $this->input->get('id_user');
		$data = [];

		$name = $this->input->get('name');
		$username = $this->input->get('username');
		$email = $this->input->get('email');
		$password = $this->input->get('password');
		$phone_number = $this->input->get('phone_number');

		if ($name != null && $name != '') {
			$data['nama'] = $name;
		}

		if ($username != null && $username != '') {
			$data['username'] = $username;
		}

		if ($email != null && $email != '') {
			$data['email'] = $email;
		}

		if ($password != null && $password != '') {
			$data['password'] = $password;
		}

		if ($phone_number != null && $phone_number != '') {
			$data['phone_number'] = $phone_number;
		}

		$this->db->where('id', $id_user);
        $status = $this->db->update('ms_user', $data);

        if ($status == 1) {
        	$this->get_user_profile($id_user);
        }else{
        	echo json_encode([
        		'status' => 'error'
        	]);
        }
	}

	public function get_user_profile($id_user='')
	{
		$id = $id_user;

		if ($id == '') {
			$id = $this->input->get('id_user');
		}
		$data = $this->db->query("SELECT * FROM `ms_user` WHERE id = ?",[$id]);
		echo json_encode($data->row());
	}

	public function test_email($value='')
	{
		// Create the Transport
		$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', '465', 'ssl')
		    ->setUsername('unj.email@gmail.com')
		    ->setPassword('r~x.Z;F^tGoI');

		// Create the Mailer using your created Transport
		$mailer = Swift_Mailer::newInstance($transport);

		$kode = "23245";

		// Create a message
		$message = (new Swift_Message('Verifikasi User'))
		  ->setFrom(['unj.email@gmail.com' => 'UNJ ADMIN'])
		  ->setTo([ "andriawan2014@gmail.com"=> "Andriawan"])
		  ->setBody('<p>Yang terhormat <b>Test</b> </p>
		  	<p>Berikut adalah kode verifikasi anda <b>' . $kode .
		  	'</b></p></p>Terima kasih</p>' , 'text/html');

		// Send the message
		$result = $mailer->send($message);
	}

	public function save_user_location()
	{
		date_default_timezone_set("Asia/Bangkok");
		$id_user = $this->input->get('id_user');
		$lat = $this->input->get('lat_user');
		$long = $this->input->get('long_user');
		$date = date('Y-m-d h:i:s');

		$data = [
			'id_user' => $id_user,
			'lat_user' => $lat,
			'long_user' => $long,
			'created_at' => $date,
			'updated_at' => $date
		];

		// dump($data);

		if ($this->cekExistUser($id_user)) {
			
			return $this->updateUserLocation($data);
		}else{
			
			return $this->saveUserLocation($data);
		}
	}

	public function cekExistUser($id)
	{
		$this->db->where('id_user', $id);
		$query = $this->db->get($this->tb_location);

		if($query->num_rows() >= 1)
		{
			return true;
		}

		return false;
	}

	public function saveUserLocation($data = [])
	{
		$status = $this->db->insert($this->tb_location, $data);

		if ($status == 1) {
			echo json_encode([
				"status" => "sukses",
				"action" => "save",
				"message" => "lokasi berhasil disimpan"
			]);

		}else{
			echo json_encode([
				"status" => "gagal",
				"action" => "save",
				"message" => "lokasi gagal disimpan"
			]);
		}
	}

	public function updateUserLocation($data = [])
	{
		unset($data['created_at']);
		$this->db->where('id_user', $data['id_user']);
		$status = $this->db->update($this->tb_location, $data);
		
		if ($status == 1) {
			echo json_encode([
				"status" => "sukses",
				"action" => "update",
				"message" => "lokasi berhasil diperbarui"
			]);

		}else{
			echo json_encode([
				"status" => "gagal",
				"action" => "update",
				"message" => "lokasi gagal diperbarui"
			]);
		}
	}

	public function login()
	{
		$username = $this->db->escape_str(
			$this->input->get('username')
		);
		$password = $this->db->escape_str(
			$this->input->get('password')
		);
		$pass_md  = $password;
		$arr 	  = array();

		$cek_login = $this->db->query("SELECT ms_user.*,ms_user.foto_type as foto, ms_role.role as ROLE_NAME, 
			c.vendor_name as vendor_names, c.vendor_alias, c.email as vendor_email, 
			c.phone_number as vendor_phone, c.vendor_category as vendor_category
			FROM ms_user INNER JOIN ms_role ON ms_user.role = ms_role.id 
			LEFT JOIN skin_master.ms_supplier as c ON ms_user.vendor_code  = c.vendor_code 
			WHERE username='$username' AND password='$pass_md'");

		if($cek_login->num_rows() == 1){
			$data = $cek_login->row();
			$nama = $data->nama;
			$username = $data->username;
			$role_no = $data->role;
			$role_name = $data->ROLE_NAME;
			$foto_type = $data->foto_type;

			$vendor_code = $data->vendor_code;
			$vendor_name = $data->nama;

			date_default_timezone_set("Asia/Bangkok");

          	$array = array(
				"id" => $data->id,
				"nama" => $nama,
				"user" => $username,
				"role_no" => $role_no,
				"role_name" => $role_name,
				"vendor_code" => $vendor_code,
				"vendor_name" => $vendor_name,
				"foto"			=> "${username}${foto_type}",
				"year" => date('Y'),
				"week" => date('W'),
				"date" => date('Y-m-d')
			);

			echo json_encode([
				"status" => "sukses",
				"message" => "welcome " . $array['nama'],
				"data" => $array
			]);
			
		}else{
			echo json_encode([
				"status" => "error",
				"message" => "Invalid user or password",
				"data" => ""
			]);
		}
	}

	public function getScheduleNew($date, $vendor_code, $user){
		if($user == '3'){
			$data = $this->db->query("SELECT
								a.*, b.vendor_name,
								b.vendor_alias,
								e.shift,
								(SELECT COUNT(*) FROM tb_scheduler WHERE schedule_number=a.schedule_number) as TOTAL_MATERIAL,
								z.id_truck, x.police_number as truck_no, z.gr_material_doc
							FROM
								tb_schedule_detail AS a
							LEFT JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
							INNER JOIN tb_scheduler d ON a.schedule_number = d.schedule_number
							INNER JOIN tb_rds_detail c ON d.id_schedule = c.id
							INNER JOIN ms_time_slot e ON a.id_time_slot = e.id
							LEFT JOIN tb_delivery_detail z ON z.id_schedule_group = a.schedule_number
							LEFT JOIN ms_truck x ON x.id = z.id_truck
							WHERE
								 a.rdd = '$date'
							GROUP BY
								a.schedule_number");

		}else{

			$data = $this->db->query("SELECT
								a.*, b.vendor_name,
								b.vendor_alias,
								e.shift,
								(SELECT COUNT(*) FROM tb_scheduler WHERE schedule_number=a.schedule_number) as TOTAL_MATERIAL,
								z.id_truck, x.police_number as truck_no, z.gr_material_doc
							FROM
								tb_schedule_detail AS a
							INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
							INNER JOIN tb_scheduler d ON a.schedule_number = d.schedule_number
							INNER JOIN tb_rds_detail c ON d.id_schedule = c.id
							LEFT JOIN tb_delivery_detail z ON z.id_schedule_group = a.schedule_number
							INNER JOIN ms_time_slot e ON a.id_time_slot = e.id
							LEFT JOIN ms_truck x ON x.id = z.id_truck
							WHERE
								a.rdd = '$date'
							GROUP BY
								a.schedule_number");
		}
		return $data;
	}

	public function schedule($value='')
	{
		$date = $this->input->get('date');
		$year_selected = date('Y', strtotime($date));
		$week_selected = date('W', strtotime($date));

		date_default_timezone_set("Asia/Bangkok");
		$arr = array();
		$arr_shift = array(
			array(
				'shift_name' => "Dinas Malam",
				"shift_alias" => "DM"
			),
			array(
				'shift_name' => "Dinas Pagi",
				"shift_alias" => "DP"
			),
			array(
				'shift_name' => "Dinas Sore",
				"shift_alias" => "DS"
			)
		);

		$dSchedule = array();
		$yearnow = date('Y');

		$ddate = date('Y-m-d');
		$date = new DateTime($ddate);
		$weeks = $date->format("W");

		$daten = date('Y-m-d');

		$week = (null === $this->input->get('date'))?$weeks:$week_selected;
		$year = (null === $this->input->get('date'))?$yearnow:$year_selected;
		$arrDate = $this->getDateList($week, $year);
		$date = (null === $this->input->get('date'))?$arrDate[0]:$this->input->get('date');
		$vendor_code = $this->input->get('vendor_code');
		$user = $this->input->get('role_no');
		$a = 0;

		while($a < count($arrDate)){
			$dSchedule[$arrDate[$a]] = array();

			if($user == 3){
				//$d = $this->setSchedule($arrDate[$a], $vendor_code, $user);
			}else{
				//$d = $this->setSchedule($arrDate[$a], $vendor_code, $user);
			}
			$dataSchedule = $this->getScheduleNew($arrDate[$a], $vendor_code, $user);
			/*foreach ($dataSchedule->result() as $get) {
				$detail_status_truck = $this->setTimeFreezeOff($get->schedule_number);
			}*/
			array_push($dSchedule[$arrDate[$a]], $dataSchedule->result());
			$a++;
		}


		$dataTimeSlot = $this->Schedule_model->getTimeSlot();
		$dataTimeSlot2 = $this->Schedule_model->getTimeSlot2();

		$arr = json_encode($dSchedule, JSON_PRETTY_PRINT);
		/*echo "<pre>".$arr."</pre>";die();*/
		$get = json_decode($arr, true);
		$c_time = date('H:i:s');
		$getShift = $this->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$shift = $getShift->shift;

		$vendor = $this->db->query("SELECT vendor_code, vendor_alias, vendor_name FROM skin_master.ms_supplier");
		/*echo "<pre>".json_encode($get, JSON_PRETTY_PRINT)."</pre>"; die();*/
		$role 						= $this->input->get('role_no');
		$data['week'] 				= $week;
		$data['year']				= $year;
		$data['vendor_code'] 		= $vendor_code;
		$data['vendor']				= $vendor->result();
		$data['data_time_slot'] 	= $dataTimeSlot->result();
		$data['data_time_slot2'] 	= $dataTimeSlot2->result();
		// $data['date_list']			= $arrDate;
		$data['data_shift']			= $arr_shift;
		$data['data']		 		= $get;
		$data['c_date']				= date('Y-m-d');
		$data['c_shift']			= $shift;
		$data['role']				= $role;

		header('Content-Type: application/json');
		echo json_encode($data);

	}

	public function getDateList($week, $year) {
	  $dto = new DateTime();
	  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
	  $s = 1;
	  while($s <= 6){
	  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
	  	$s++;
	  }
	  return $ret;
	}

	public function setSchedule($date, $vendor_code, $user){
		$arra = array();
		$arr_shift = array(
			array(
				'shift_name' => "Dinas Malam",
				"shift_alias" => "DM"
			),
			array(
				'shift_name' => "Dinas Pagi",
				"shift_alias" => "DP"
			),
			array(
				'shift_name' => "Dinas Sore",
				"shift_alias" => "DS"
			)
		);

		foreach($arr_shift as $as){

			$dataToday = $this->Schedule_model
				->getSchedule($date, $vendor_code, $user, $as['shift_alias']);
			// dump($dataToday->result());
			$counter = 0;
			$b = 0;
			$isSaved = false;
			if($dataToday->num_rows() > 0){
				$getLast = $this->Schedule_model->getScheduleNumber();
				foreach($dataToday->result() as $get){
					if($counter >= $get->row_number){
						$getLast = $getLast+1;
						$isSaved = false;
					}else{
						$getLast = $getLast;
					}
					$shift = $get->shift;
					$rdd = $get->requested_delivery_date;
					$sn = $getLast;
					$arr = array(
						'id_schedule' => $get->id,
						'vendor_code' => $get->vendor_code,
						'schedule_number' => $getLast,
						'rdd' => $get->requested_delivery_date,
						'quantity' => $get->req_pallet
					);
					$this->db->insert('tb_scheduler', $arr);
					$this->db->where('id', $get->id);
	                $this->db->update('tb_rds_detail', array(
	                    'is_schedule' => 1
	                ));

	                if(!$isSaved){
	                	$time_slot_shift = '';
						if($shift == 'DP'){
							$time_slot_shift = 6;
						}else if($shift == 'DM'){
							$time_slot_shift = 1;
						}else{
							$time_slot_shift = 12;
						}

						$getData = $this->db->query("SELECT COUNT(rdd) as count, id_time_slot from tb_schedule_detail WHERE rdd='$rdd' AND id_time_slot='$time_slot_shift' ORDER BY id_time_slot DESC");
						if(isset($getData->rdd)){
							if($getData->count == 3){
								$time_slot_shift = $getData->id_time_slot + 1;
							}else{
								$time_slot_shift = $getData->id_time_slot;
							}
						}

						$tsi = array(
							'vendor_code'		=> $get->vendor_code,
							'schedule_number' 	=> $getLast,
							'rdd' 				=> $get->requested_delivery_date,
							'id_time_slot' 		=> $time_slot_shift,

						);
						
						$this->db->insert('tb_schedule_detail', $tsi);
						$isSaved = true;
	                }

					$counter = $get->row_number;
					$b++;
				}
			}else{
				
			}
		}
	}

	public function outstanding_po($value='')
	{
		date_default_timezone_set('Asia/Jakarta');
		$shift = getShift();
		$arr_vendor = array();
		$arr_id = array();

		if (null != $this->input->get('date')) {
			$today = date("Y-m-d", strtotime(
				$this->input->get('date')));
			$year = date("Y", strtotime(
			$this->input->get('date')));
			$week = date("W", strtotime(
				$this->input->get('date')));
		}else{
			$today = date("Y-m-d");
			$year = date("Y");
			$week = date("W");
		}

		$ven_list = $this->OS_Model->getVendorList();
		$sess_no = $this->input->get('role_no');
		$vendor_code = $this->input->get('vendor_code');
		$pl = [];

		if($sess_no != 3){
			foreach ($ven_list->result() as $g) {
				array_push($arr_vendor, $g->vendor_code);
			}

			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialPool($imp, $year, $week);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model->getDateLess($year,$week, $imp);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLate($today, $shift, $imp, $year, $week);
		}else{
			$vc = $vendor_code;
			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model->getMaterialVendor($year, $week, $sess_no, $vc);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model->getDateLessVen($year,$week, $imp, $sess_no, $vc);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLateVen($today, $shift, $imp, $year, $week, $sess_no, $vc);
		}
		/*dump($dateLess->result());*/
		$pl = array_merge($dataPool->result(), $dateLess->result(), $shiftLate->result());
		/*if($dataPool->num_rows() > 0 && $dateLess->num_rows() > 0 && $shiftLate->num_rows() > 0){
			
		}else{
			if($dataPool->num_rows() > 0 && $dateLess->num_rows() == 0){
				$pl = $dataPool->result();	
			}else if($dataPool->num_rows() == 0 && $dateLess->num_rows() > 0){
				$pl = $dateLess->result();	
			}
		}*/
		$data['pool_list'] = $pl;
		echo json_encode($pl);
		
	}

	public function outstanding_po_filter()
	{
		date_default_timezone_set('Asia/Jakarta');
		$shift = getShift();
		$arr_vendor = array();
		$arr_id = array();

		if (null != $this->input->get('date')) {
			$today = date("Y-m-d", strtotime(
				$this->input->get('date')));
			$year = date("Y", strtotime(
			$this->input->get('date')));
			$week = date("W", strtotime(
				$this->input->get('date')));
		}else{
			$today = date("Y-m-d");
			$year = date("Y");
			$week = date("W");
		}

		$ven_list = $this->OS_Model_Api->getVendorList();
		$sess_no = $this->input->get('role_no');
		$vendor_code = $this->input->get('vendor_code');
		$material = $this->input->get('material');
		$pl = [];
		if($sess_no != 3){
			foreach ($ven_list->result() as $g) {
				array_push($arr_vendor, $g->vendor_code);
			}

			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model_Api->getMaterialPool($imp, $year, $week, $material);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model_Api->getDateLess($year,$week, $imp, $material);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model_Api->getShiftLate($today, $shift, $imp, $year, $week, $material);
		}else{
			$vc = $vendor_code;
			$imp = implode(",", $arr_vendor);
			$dataPool = $this->OS_Model_Api->getMaterialVendor($year, $week, $sess_no, $vc, $material);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}
			$dateLess = $this->OS_Model_Api->getDateLessVen($year,$week, $imp, $sess_no, $vc, $material);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model_Api->getShiftLateVen($today, $shift, $imp, $year, $week, $sess_no, $vc, $material);
		}
		/*dump($dateLess->result());*/
		$pl = array_merge($dataPool->result(), $dateLess->result(), $shiftLate->result());
		/*if($dataPool->num_rows() > 0 && $dateLess->num_rows() > 0 && $shiftLate->num_rows() > 0){
			
		}else{
			if($dataPool->num_rows() > 0 && $dateLess->num_rows() == 0){
				$pl = $dataPool->result();	
			}else if($dataPool->num_rows() == 0 && $dateLess->num_rows() > 0){
				$pl = $dateLess->result();	
			}
		}*/
		$data['pool_list'] = $pl;
		echo json_encode($pl);

	}

	public function delivery_note(){

		$dataDN = $this->db->query("SELECT
			b.status,
			c.vendor_code,
			c.vendor_name,
			a.delivery_date,
			a.id_time_slot,
			e.do_number,
			a.id_schedule_group as sn,
			CONCAT(d.start_time,' - ',d.end_time) as TIME_SLOT
		FROM
			tb_delivery_detail a
		INNER JOIN tb_schedule_detail b ON a.id_schedule_group = b.schedule_number
		INNER JOIN skin_master.ms_supplier c ON b.vendor_code = c.vendor_code
		INNER JOIN ms_time_slot d ON a.id_time_slot = d.id
		LEFT JOIN tb_delivery_invoice as e ON a.id_schedule_group = e.id_schedule");
		
		$data['dn_list'] = $dataDN->result();
		echo json_encode($data);
	}

	public function material(){
		$supplierDN = $this->db->query("SELECT vendor_name, vendor_alias FROM skin_master.ms_supplier");
		$getMaterial = $this->db->query("SELECT * FROM skin_master.ms_material");
		$data['material_list'] = $getMaterial->result();
		$data['dn_supplier'] = $supplierDN->result();
		echo json_encode($data);
	}

	public function dn_action(){
		$data = [];
		$keyword = $this->input->get('keyword');
		$dataDO = $this->db->query("SELECT * FROM tb_delivery_detail INNER JOIN tb_schedule_detail ON tb_schedule_detail.id = tb_delivery_detail.id_schedule_group INNER JOIN ms_time_slot ON tb_schedule_detail.id_time_slot = ms_time_slot.id WHERE do_number='$keyword'");
		if($dataDO->num_rows() > 0){
			$data['data'] = $dataDO->result();
		}else{
			$data['data'] = $dataDO->result();
		}

		echo json_encode($data);
	}

	public function material_action(){

		$keyword = $this->input->get('keyword');
		$supplier = $this->input->get('supplier');
		$date = $this->input->get('calender');
		$datenew = date('Y-m-d',strtotime($date));
		
		if($supplier == '' AND $datenew != '1970-01-01'){
			$getData = $this->db->query("SELECT
				a.*, b.vendor_alias,
				b.vendor_name,
				c.material_name,
					(
						(a.req_pallet - SUM(d.quantity))*a.uom_plt
					) AS sisa,
					f.start_time,f.end_time,
					e. STATUS AS C_STATUS,
					(SELECT SUM(d.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND `status`='1') AS total
				FROM
					tb_rds_detail a
				LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
				LEFT JOIN tb_schedule_detail AS e ON d.schedule_number = e.schedule_number
				LEFT JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
				LEFT JOIN skin_master.ms_material c ON a.material_code = c.material_sku
				LEFT JOIN ms_time_slot f ON e.id_time_slot = f.slot_number
				WHERE a.material_code = '$keyword' AND a.requested_delivery_date = '$date'
				GROUP BY
					a.id
				ORDER BY
					a.requested_delivery_date,
					a.shift ASC");
		}
		elseif($supplier == '' AND $datenew = '1970-01-01'){										
			$getData = $this->db->query("SELECT
				a.*, b.vendor_alias,
				b.vendor_name,
				c.material_name,
				(
					(a.req_pallet - SUM(d.quantity))*a.uom_plt
				) AS sisa,
				f.start_time,f.end_time,
				e.schedule_number,
				e. STATUS AS C_STATUS,
				(SELECT SUM(d.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND `status`='1') AS total
			FROM
				tb_rds_detail a
			LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
			LEFT JOIN tb_schedule_detail AS e ON d.schedule_number = e.schedule_number
			LEFT JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
			LEFT JOIN skin_master.ms_material c ON a.material_code = c.material_sku
			LEFT JOIN ms_time_slot f ON e.id_time_slot = f.slot_number
			WHERE a.material_code = '$keyword'
			GROUP BY
				a.id
			ORDER BY
				a.requested_delivery_date,
				a.shift ASC");		

		}elseif($supplier != '' AND $datenew != '1970-01-01'){
			$getData = $this->db->query("SELECT
				a.*, b.vendor_alias,
				b.vendor_name,
				(
					(a.req_pallet - SUM(d.quantity))*a.uom_plt
				) AS sisa,
				c.material_name,
				f.start_time,f.end_time,
				e. STATUS AS C_STATUS,
				(SELECT SUM(d.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND `status`='1') AS total
			FROM
				tb_rds_detail a
			LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
			LEFT JOIN tb_schedule_detail AS e ON d.schedule_number = e.schedule_number
			LEFT JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
			LEFT JOIN skin_master.ms_material c ON a.material_code = c.material_sku
			LEFT JOIN ms_time_slot f ON e.id_time_slot = f.slot_number
			WHERE a.material_code = '$keyword' AND a.requested_delivery_date = '$date' AND b.vendor_alias = '$supplier'
			GROUP BY
				a.id
			ORDER BY
				a.requested_delivery_date,
				a.shift ASC");
		}elseif($supplier != '' AND $datenew == '1970-01-01'){
			$getData = $this->db->query("SELECT
				a.*, b.vendor_alias,
				b.vendor_name,
				c.material_name,
				f.start_time,f.end_time,
				(
					(a.req_pallet - SUM(d.quantity))*a.uom_plt
				) AS sisa,
				e. STATUS AS C_STATUS,
				(SELECT SUM(d.quantity) FROM tb_scheduler WHERE id_schedule=a.id AND `status`='1') AS total
			FROM
				tb_rds_detail a
			LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
			LEFT JOIN tb_schedule_detail AS e ON d.schedule_number = e.schedule_number
			LEFT JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
			LEFT JOIN skin_master.ms_material c ON a.material_code = c.material_sku
			LEFT JOIN ms_time_slot f ON e.id_time_slot = f.slot_number
			WHERE a.material_code = '$keyword' AND b.vendor_alias = '$supplier'
			GROUP BY
				a.id
			ORDER BY
				a.requested_delivery_date,
				a.shift ASC");
		}
		
		$data['search_result'] = $getData;
		$this->load->view('search/material_result', $data);
	}

}