<?php
use pdeans\Builders\XmlBuilder;
class API extends CI_Controller{
	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();	
	}


	public function download_detail_truck(){
		$ch = curl_init("http://localhost/time_slotv2/API/detail_truck");
		$fp = fopen("assets/templates/test2.xml", "w");
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
	}

	public function test_json(){
		echo '{
			    "ID": 3145,
			    "Name": "Big Venue, Clapton",
			    "NameWithTown": "Big Venue, Clapton, London",
			    "NameWithDestination": "Big Venue, Clapton, London",
			    "ListingType": "A",
			    "Address": {
			        "Address1": "Clapton Raod",
			        "Address2": "",
			        "Town": "Clapton",
			        "County": "Greater London",
			        "Postcode": "PO1 1ST",
			        "Country": "United Kingdom",
			        "Region": "Europe"
			    },
			    "ResponseStatus": {
			        "ErrorCode": "200",
			        "Message": "OK"
			    }
			}';
	}

	public function download_foto(){
		$id_driver = $this->input->get('id_driver');
		$getFoto = $this->db->query("SELECT foto FROM ms_driver WHERE id = '$id_driver' ")->row()->foto;
		$file_url = base_url()."assets/upload/foto/driver/".$getFoto;
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
		readfile($file_url);
	}

	public function detail_truck(){
		ob_start();
		$this->load->library('xml_writer');
	    $xml = new xml_writer;
	    $xml->setRootName('trucks');
	    $xml->initiate();
	    $xml->startBranch('truck');
	    $getTruck = $this->db->query("SELECT * FROM tb_truck_detail");
	    foreach ($getTruck->result() as $get) {
	    	$xml->startBranch('truck_type', array('id' => $get->id)); // start branch 1-1
		    $xml->addNode('truck_type', $get->truck_type);
		    $xml->addNode('no_police', $get->truck_no);
		    $xml->addNode('driver_name', $get->driver_name);
		    $xml->endBranch();
	    }
	    $xml->endBranch();
	    $xml->getXml(true);
		file_put_contents('yourpage.xml', ob_get_contents());
	}

	public function get_detail_dock(){
		$date = date('Y-m-d');
		$data = $this->db->query("SELECT a.receipt, 
								  a.id_driver, 
								  c.driver_name, 
								  a.id_time_slot, 
								  b.no_kir, 
								  b.police_number as plat_number,
								  b.status_flag, 
								  CURDATE() As 'datez', 
								  CURTIME() AS 'time',
								  e.vendor_alias,
								  f.status_queue   
								  FROM tb_delivery_detail AS a 
								  LEFT JOIN ms_truck AS b ON a.id_truck = b.id 
								  LEFT JOIN ms_driver AS c ON a.id_driver = c.id
								  LEFT JOIN tb_schedule_detail AS d ON a.id_schedule_group = d.schedule_number  
								  LEFT JOIN skin_master.ms_supplier AS e ON e.vendor_code = d.vendor_code 
								  LEFT JOIN docking_management.tb_queue AS f ON a.receipt = f.receipt
								  WHERE date(a.delivery_date) = '$date'
								  AND d.status = 2
								  GROUP BY a.receipt
								  ORDER BY a.receipt")->result();
		echo json_encode($data);
	}

	public function delivery_detail(){
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$this->load->library('xml_writer');
	    $xml = new xml_writer;
	    $xml->setRootName('delivery_detail');
	    $xml->initiate();
	    $xml->startBranch('delivery');
	    $getTruck = $this->db->query("SELECT a.delivery_date,
	    	                                 a.id_truck,
	    	                                 a.driver_name,
	    	                                 a.phone_number,
	    	                                 a.receipt,
	    	                                 a.created_at,
	    	                                 a.reason,
	    	                                 a.arrive_date,
	    	                                 a.arrive_time,
	    	                                 a.arrived_by,
	    	                      			 a.receive_date,
	    	                      			 a.receive_time,
	    	                      			 a.received_by
	                                  FROM tb_delivery_detail AS a 
	    							  INNER JOIN tb_schedule_detail AS b ON a.id_schedule_group = b.schedule_number
	    							  INNER JOIN ms_time_slot As c ON c.id = b.id_time_slot");
	    
	    foreach ($getTruck->result() as $get) {
	    	$xml->startBranch('delivery_date', array('id' => $get->delivery_date)); // start branch 1-1
		    $xml->addNode('id_truck', $get->id_truck);
		    $xml->addNode('driver_name', $get->driver_name);
		    $xml->addNode('phone_number', $get->phone_number);
		    $xml->addNode('receipt', $get->receipt);
		    $xml->addNode('created_at', $get->created_at);
		    $xml->addNode('reason', $get->reason);
		    $xml->addNode('arrived_date', $get->arrive_date);
		    $xml->addNode('arrived_time', $get->arrive_time);
		    $xml->addNode('arrived_by', $get->arrived_by);
		    $xml->addNode('received_date', $get->receive_date);
		    $xml->addNode('received_time', $get->receive_time);
		    $xml->addNode('received_by', $get->received_by);
		    $xml->endBranch();
	    }

	    $xml->endBranch();
	    $xml->getXml(true);	
	}

	public function driver_master(){
		
	}

	public function xml_gr_automation(){
		$this->load->model('API_Model');
		$receipt_number = $this->uri->segment(3);
		$gln = $this->API_Model->glnCodeInfo();

		$getData = $this->API_Model->getRNDetail($receipt_number);
		$a = 0;
		foreach($getData->result() as $get){
			if($get->is_weighing == 1){
				$send_q = $get->receive_quantity;
			}else{
				$send_q = $get->q_rec;
			}
			$arr['receivingAdviceItemContainmentLineItem'][$a++] = array(
												'@a' => array(
													'number' => $get->sap_line
												),
												'containedItemIdentification' => array(
													'gtin' => '0000000', //TO BE CHANGED
													'additionalTradeItemIdentification' => array(
														array(
															'additionalTradeItemIdentificationValue' => $get->sap_sloc, //STORAGE LOCATION
															'additionalTradeItemIdentificationType' => 'FOR_INTERNAL_USE_2'
														),
														array(
															'additionalTradeItemIdentificationValue' => $get->material_code, //MATERIAL CODE
															'additionalTradeItemIdentificationType' => 'SUPPLIER_ASSIGNED'
														),
														array(
															'additionalTradeItemIdentificationValue' => $get->sap_stock_type, //CAN BE EDITED
															'additionalTradeItemIdentificationType' => 'FOR_INTERNAL_USE_8'
														),
														array(
															'additionalTradeItemIdentificationValue' => $get->vendor_batch, //CAN BE EDITED
															'additionalTradeItemIdentificationType' => 'FOR_INTERNAL_USE_4'
														),
														array(
															'additionalTradeItemIdentificationValue' => null,
															'additionalTradeItemIdentificationType' => 'StockType'
														)
													)
												),
												'purchaseOrder' => array(
													'documentReference' => array(
														'uniqueCreatorIdentification' => $get->po_number, //PO NUMBER
														'contentOwner' => array(
															'gln' => '00000'
														)
													)
												),
												'extendedAttributes' => array(
													'itemExpirationDate' => $get->exp_date, //TO BE CHANGED, TAKEN FROM DB
													'batchNumber' => $get->batch_number, //TO BE CHANGED, TAKEN FROM DB
													'productionDate' => $get->prod_date, //TO BE CHANGED, TAKEN FROM DB
												),
												'quantityAccepted' => array(
													'value' => 0 //TO BE CHANGED, TAKEN FROM DB
												),
												'quantityReceived' => array(
													'value' => $send_q, //TO BE CHANGED, TAKEN FROM DB
													'unitOfMeasure' => array(
														'measurementUnitCodeValue' => $get->sap_uom
													)
												)
											);
		}
		/*echo "<pre>".json_encode($arr, JSON_PRETTY_PRINT)."</pre>";
		die();*/

		$filename = 'XML_GR_'.$receipt_number."_".date('dmYHis').".xml";
		/*header('Content-Type: text/xml');*/
		//header('Content-Disposition: attachment; filename="'.$filename.'"');
		$xml = new XmlBuilder;

		$xml_object = $xml->create('ns1:StandardBusinessDocument', [
			'@a' => [
				'xmlns:ns1' => 'http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader'
			],
			'@t' => [
				'ns1:StandardBusinessDocumentHeader' => [
					'ns1:HeaderVersion' => 2.2,
					'ns1:Sender' => [
						'ns1:Identifier' => [
							'@a' => [
								'Authority' => 'EAN.UCC'
							],
							'@v' => $gln->gln_tsb
						]
					],
					'ns1:Receiver' => [
						'ns1:Identifier' => [
							'@a' => [
								'Authority' => 'EAN.UCC'
							],
							'@v' => $gln->gln_uli //GLN Unilever
						]
					],
					'ns1:DocumentIdentification' => [
						'ns1:Standard' => 'EAN.UCC',
						'ns1:TypeVersion' => '2.1.1',
						'ns1:InstanceIdentifier' => date('Y-m-d')."T".date('H:i:s.u'),
						'ns1:Type' => 'ReceivingAdvice',
						'ns1:CreationDateAndTime' => date('Y-m-d')."T".date('H:i:s.u')
					],
					'ns1:BusinessScope' => [
						'ns1:Scope' => [
							'ns1:Type' => 'DELIVERY',
							'ns1:InstanceIdentifier' => 'AAR_WCI',
							'ns1:Identifier' => 'GoodsReceipt_PO'
						]
					]
				],
				'ns2:message' => [
					'@a' => [
						'xmlns:ns2' => 'urn:ean.ucc:2',
						'xmlns:ns3' => 'urn:ean.ucc:deliver:2'
					],
					'entityIdentification' => [
						'uniqueCreatorIdentification' => date('Y-m-d')."T".date('H:i:s.u'),
						'contentOwner' => [
							'gln' => '12345678' //TO BE CHANGED
						]
					],
					'ns2:transaction' => [
						'entityIdentification' => [
							'uniqueCreatorIdentification' => date('Y-m-d')."T".date('H:i:s.u'),
							'contentOwner' => [
								'gln' => '12345678' //TO BE CHANGED
							]
						],
						'command' => [
							'ns2:documentCommand' => [
								'documentCommandHeader' => [
									'@a' => [
										'type' => "ADD"
									],
									'entityIdentification' => [
										'uniqueCreatorIdentification' => date('Y-m-d')."T".date('H:i:s.u'),
										'contentOwner' => [
											'gln' => '12345678' //TO BE CHANGED
										]
									]
								],
								'documentCommandOperand' => [
									'ns3:receivingAdvice' => [
										'@a' => [
											'creationDateTime' => date('Y-m-d')."T".date('H:i:s.u'),
											'documentStatus' => 'ORIGINAL'
										],
										'reportingCode' => 'CONFIRMATION',
										'receivingAdviceIdentification' => [
											'uniqueCreatorIdentification' => $receipt_number, //CHANGE TO TSB RECEIPT NUMBER
											'contentOwner' => [
												'gln' => '1234567', //TO BE CHANGED
												'additionalPartyIdentification' => [
													'additionalPartyIdentificationValue' => $receipt_number, //DELIVERY NOTE
													'additionalPartyIdentificationType' => 'FOR_INTERNAL_USE_6'
												]
											]
										],
										'shipTo' => [
											'gln' => '000000000' //TO BE CHANGED
										],
										'shipper' => [
											'gln' => '000000000' //TO BE CHANGED
										],
										'receiver' => [
											'gln' => '000000000', //TO BE CHANGED
											'additionalPartyIdentification' => [
												'additionalPartyIdentificationValue' => '9003', //TO BE CHANGED
												'additionalPartyIdentificationType' => 'SELLER_ASSIGNED_IDENTIFIER_FOR_A_PARTY'
											]
										],
										'receivingAdviceLogisticUnitLineItem' => [
											$arr
										],
										'receiptInformation' => [ // THIS PART CAN BE ITERATED
											'receivingDateTime' => date('Y-m-d')."T".date('H:i:s.u')
										],
									]
								]
							]
						]
					]

				]
			]
		]);
		file_put_contents("./assets/xml/".$filename, $xml_object);

		//CURL START
		$headers = array("Content-Type:multipart/form-data");
		//Initialise the cURL var
		$ch = curl_init();

		//Get the response from cURL
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		//Set the Url
		curl_setopt($ch, CURLOPT_URL, 'http://52.175.48.113:8080/sap_interface/upload_function.php');

		//Create a POST array with the file in it
		$postData = array(
			'file_contents' => curl_file_create(realpath('./assets/xml/'.$filename)),
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

		// Execute the request
		$response = curl_exec($ch);
		//echo $response;
		if(!curl_errno($ch)){
			/*echo $response;*/
			rename("./assets/xml/".$filename, "./assets/xml/uploaded/".$filename);
			$arr = array("status" => "SUCCESS", "file_name" => $filename);
		}else{
			echo curl_error($ch);
			$arr = array("status" => "FAILED");
		}
		//CURL END
		echo json_encode($arr);
	}

}
?>