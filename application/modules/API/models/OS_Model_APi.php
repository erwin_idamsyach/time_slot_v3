<?php

class OS_Model_Api extends CI_Model{

  public function getVendorList(){
    $getData = $this->db->query("SELECT vendor_code FROM tb_rds_detail GROUP BY vendor_code");
    return $getData;
  }

  public function getMaterialPool($ven_list, $year, $week, $material){

    if($ven_list != ''){
      $getData = $this->db->query("SELECT
                      *
                    FROM
                      (
                        SELECT
                          trd.*, mm.material_name,
                          IF(tb.receive_amount = 0,
                            (
                              trd.req_pallet - SUM(tb.quantity)
                            ), (
                              trd.req_pallet - SUM(tb.receive_amount)
                            )
                          ) AS sisa
                          
                        FROM
                          tb_rds_detail AS trd
                        INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.vendor_code IN ($ven_list)
                          AND YEAR(trd.requested_delivery_date) = '$year' AND trd.week = '$week'
                        AND trd.is_schedule = '1'
                        GROUP BY
                          trd.id
                      ) AS TB_1
                    WHERE sisa > 0 OR sisa IS NULL AND material_code LIKE '{$material}%'");
    }else{
      $getData = $this->db->query("SELECT
                      *
                    FROM
                      (
                        SELECT
                          trd.*, mm.material_name,
                          IF(tb.receive_amount = 0,(
                              trd.req_pallet - SUM(tb.quantity)
                            ), (
                              trd.req_pallet - SUM(tb.receive_amount)
                            )
                          ) AS sisa
                        FROM
                          tb_rds_detail AS trd
                        INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.is_schedule = '1'
                          AND YEAR(trd.requested_delivery_date) = '$year' AND trd.week = '$week'
                        GROUP BY
                          trd.id
                      ) AS TB_1
                    WHERE sisa > 0 OR sisa IS NULL AND material_code LIKE '{$material}%'");
    }
    
    return $getData;
  }

  public function getMaterialVendor($year = 0, $week = 0, $sess_no, $vencode, $sn = 0, $material){
    if($sn != 0){
      $fil_sn = "AND tb.schedule_number != ".$sn;
    }else{
      $fil_sn = "";
    }
if($year != 0){
      $filter = "AND YEAR(trd.requested_delivery_date) = '$year' AND trd.week = '$week'";
    }else{
      $filter = "";
    }
    $getData = $this->db->query("SELECT
                      *
                    FROM
                      (
                        SELECT
                          trd.*, mm.material_name,
                          IF(tb.receive_amount = 0,(
                              trd.req_pallet - SUM(tb.quantity)
                            ), (
                              trd.req_pallet - SUM(tb.receive_amount)
                            )
                          ) AS sisa
                        FROM
                          tb_rds_detail AS trd
                        INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.is_schedule = '1'
                          $filter
                          AND trd.vendor_code = '$vencode'
                        GROUP BY
                          trd.id
                      ) AS TB_1
                    WHERE sisa > 0 OR sisa IS NULL AND material_code LIKE '{$material}%'");
    return $getData;
  }

  public function getDateLess($year, $week, $imp, $material){
    if($imp != ""){
      $add_where = "AND trd.id NOT IN($imp)";
    }else{
      $add_where = "";
    }
    $date = date('Y-m-d');
    $getData = $this->db->query("SELECT
                    trd.*, mm.material_name, null as sisa
                  FROM
                    tb_rds_detail AS trd
                  INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                  LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                  WHERE YEAR(trd.requested_delivery_date) = '$year' AND trd.week = '$week' AND trd.status='0' AND requested_delivery_date < '$date' $add_where AND material_code LIKE '{$material}%'");
    
    return $getData;
  }

  public function getDateLessVen($year = 0, $week = 0, $imp, $sess_no, $vencode, $sn = 0, $material){
    if($sn != 0){
      $fil_sn = "AND tb.schedule_number != ".$sn;
    }else{
      $fil_sn = "";
    }

    if($year != 0){
  
        $filter = "AND YEAR(trd.requested_delivery_date) = '$year' AND trd.week = '$week'";
      
    }else{
      $filter = "";
    }
    if($imp != ""){
      $add_where = "AND trd.id NOT IN($imp)";
    }else{
      $add_where = "";
    }
    $date = date('Y-m-d');
    $getData = $this->db->query("SELECT
                    trd.*, mm.material_name,
                    NULL AS sisa
                  FROM
                    tb_rds_detail AS trd
                  INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                  LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                  WHERE
                  trd. STATUS = '0'
                  AND material_code LIKE '{$material}%'
                  AND requested_delivery_date < '$date'
                  AND trd.vendor_code='$vencode'
                  $filter
                  $fil_sn
                  $add_where");
    
    return $getData;
  }
public function os_check($vendor){
    if($vendor != null || $vendor != ''){
    $getData = $this->db->query("SELECT
                      *
                    FROM
                      (
                        SELECT
                          (
                            trd.req_pallet - SUM(tb.quantity)
                          ) AS sisa
                        FROM
                          tb_rds_detail AS trd
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.vendor_code = $vendor
                          
                        GROUP BY
                          trd.id
                      ) AS TB_1
                    WHERE sisa > 0 OR sisa IS NULL")->num_rows();
  }else{
    $getData = $this->db->query("SELECT
                      *
                    FROM
                      (
                        SELECT
                          (
                            trd.req_pallet - SUM(tb.quantity)
                          ) AS sisa
                        FROM
                          tb_rds_detail AS trd
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        GROUP BY
                          trd.id
                      ) AS TB_1
                    WHERE sisa > 0 OR sisa IS NULL")->num_rows();
      
  }
  return $getData;
  }

  public function getShiftLate($today, $shift, $imp, $year, $week, $material){
    if($imp != ""){
      $add_whereimp = "AND trd.id NOT IN($imp)";
    }else{
      $add_whereimp = "";
    }

    if($shift == "DM"){
      $add_where = "";
    }else if($shift == "DP"){
      $add_where = "AND trd.shift IN('DM')";
    }else{
      $add_where = "AND trd.shift IN('DM', 'DP')";
    }
    $getData = $this->db->query("SELECT
                      *
                    FROM
                      (
                        SELECT
                          trd.*, mm.material_name,
                          trd.req_pallet AS sisa
                        FROM
                          tb_rds_detail AS trd
                        INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.requested_delivery_date= '$today'
                        AND trd.is_schedule = '1'
                        AND YEAR(trd.requested_delivery_date) ='$year'
                        AND trd.week='$week'
                        AND material_code LIKE '{$material}%'
                        $add_whereimp
                        $add_where
                        GROUP BY
                          trd.id
                      ) AS TB_1");
    return $getData;
  }

  public function getShiftLateVen($today, $shift, $imp, $year = 0, $week = 0, $sess_no, $vencode, $sn = 0,$material){
    if($sn != 0){
      $fil_sn = "AND tb.schedule_number != ".$sn;
    }else{
      $fil_sn = "";
    }

    if($year != 0){
      $filter = "AND YEAR(trd.requested_delivery_date) = '$year' AND trd.week = '$week'";
    }else{
      $filter = "";
    }

    if($imp != ""){
      $add_whereimp = "AND trd.id NOT IN($imp)";
    }else{
      $add_whereimp = "";
    }
if($shift == "DM"){
      $add_where = "";
    }else if($shift == "DP"){
      $add_where = "AND trd.shift IN('DM')";
    }else{
      $add_where = "AND trd.shift IN('DM', 'DP')";
    }
    $getData = $this->db->query("SELECT
                      *
                    FROM
                      (
                        SELECT
                          trd.*, mm.material_name,
                          trd.req_pallet AS sisa
                        FROM
                          tb_rds_detail AS trd
                        INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.requested_delivery_date= '$today'
                        AND trd.is_schedule = '1'
                        $filter
                        AND trd.vendor_code='$vencode'
                        AND material_code LIKE '{$material}%'
                        $add_whereimp
                        $add_where
                        $fil_sn
                        GROUP BY
                          trd.id
                      ) AS TB_1");
    return $getData;
  }

  public function adt_os_check($shift, $role, $vendor_code, $material){

    $time = date('H:i');
    $today = date('Y-m-d');
    $getDataShift = [];
    if($shift == 'DS'){
      $getDataShift = $this->db->query("SELECT req_pallet FROM tb_rds_detail WHERE shift != 'DS' AND status = '0' AND material_code LIKE '{$material}%' ")->result();
    }else if($shift == 'DP'){
      $getDataShift = $this->db->query("SELECT req_pallet FROM tb_rds_detail WHERE shift = 'DM' AND status = '0' AND material_code LIKE '{$material}%' ")->result();
    }

    if($this->session->userdata('sess_vendor_code')== '3' ){

    $getDataPool = $this->db->query("SELECT
                      req_pallet
                    FROM
                      (
                        SELECT
                          trd.*, mm.material_name,
                          (
                            trd.req_pallet - SUM(tb.quantity)
                          ) AS sisa
                        FROM
                          tb_rds_detail AS trd
                        INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.vendor_code = '$vendor_code'
                        AND trd.is_schedule = '1'
                        GROUP BY
                          trd.id
                      ) AS TB_1
                    WHERE sisa > 0 OR sisa IS NULL AND material_code LIKE '{$material}%'")->result();
  }else{
    $getDataPool = $this->db->query("SELECT
                      req_pallet
                    FROM
                      (
                        SELECT
                          trd.*, mm.material_name,
                          (
                            trd.req_pallet - SUM(tb.quantity)
                          ) AS sisa
                        FROM
                          tb_rds_detail AS trd
                        INNER JOIN skin_master.ms_material AS mm ON mm.material_sku = trd.material_code
                        LEFT JOIN tb_scheduler AS tb ON trd.id = tb.id_schedule
                        WHERE
                          trd.is_schedule = '1'
                        GROUP BY
                          trd.id
                      ) AS TB_1
                    WHERE sisa > 0 OR sisa IS NULL AND material_code LIKE '{$material}%'")->result();
  }
    
    $getDataSisa = $this->db->query("SELECT (a.req_pallet - b.quantity) as req_pallet FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE material_code LIKE '{$material}%' ")->result();

    $getDataless = $this->db->query("SELECT req_pallet FROM tb_rds_detail WHERE requested_delivery_date < '$today' AND status = '0' AND material_code LIKE '{$material}%' ")->result();
return array_merge($getDataShift, $getDataSisa, $getDataless, $getDataPool);
  }
}

?>
