<?php 
class API_Model extends CI_Model{

	public function getRNDetail($rn){
		$getData = $this->db->query("SELECT a.sap_line, a.sap_sloc, a.material_code, a.po_number, a.sap_uom, a.is_non_full, b.*, c.is_weighing, d.receive_quantity, (b.receive_amount*a.uom_plt) as q_rec
									FROM
									tb_rds_detail a 
									INNER JOIN tb_scheduler b ON a.id = b.id_schedule
									INNER JOIN tb_schedule_detail c ON b.schedule_number = c.schedule_number
									INNER JOIN tb_delivery_detail d ON c.schedule_number = d.id_schedule_group
									INNER JOIN tb_delivery_invoice e ON e.id_schedule = d.id_schedule_group
									WHERE d.receipt = '$rn'
									GROUP BY a.id");
		return $getData;

	}

	public function glnCodeInfo(){
		$getData = $this->db->query("SELECT * FROM tb_xml_identifier");
		return $getData->row();
	}

}
?>