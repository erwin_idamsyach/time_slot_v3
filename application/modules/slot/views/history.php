<?php
$data = $rds_data->row();
?>
<div class="row" style="font-size: 14px">
	<div class="col-md-3 pull-right">
		<b>PO Number</b>
	</div>
	<div class="col-md-9"><?php echo $data->po_number; ?></div>
	<div class="col-md-3 pull-right">
		<b>Line</b>
	</div>
	<div class="col-md-9"><?php echo $data->po_line_item; ?></div>
	<div class="col-md-3 pull-right">
		<b>Material Code</b>
	</div>
	<div class="col-md-9"><?php echo $data->material_code; ?></div>
	<div class="col-md-3 pull-right">
		<b>Material Name</b>
	</div>
	<div class="col-md-9"><?php echo $data->material_name; ?></div>
	<div class="col-md-3 pull-right">
		<b>Vendor</b>
	</div>
	<div class="col-md-9"><?php echo $data->vendor_code." - ".$data->vendor_name; ?></div>
	<div class="col-md-3 pull-right">
		<b>RDD</b>
	</div>
	<div class="col-md-9"><?php echo date_format(date_create($data->requested_delivery_date), "D, d M Y"); ?></div>
</div>
<table class="table table-bordered table-striped">
	<thead style="font-size: 12px"	>
		<tr>
			<th>Receipt</th>
			<th>Delivery Date</th>
			<th>Time Slot</th>
			<th>Send Pallet</th>
			<th>Receive</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		foreach($mat_log->result() as $get){
			?>
		<tr style="font-size: 14px">
			<td><?php echo $get->receipt ?></td>
			<td>
				<?php
				echo date_format(date_create($get->rdd), "d-m-Y");
				?>
			</td>
			<td>
				<?php echo $get->start_time." - ".$get->end_time; ?>
			</td>
			<td>
				<?php echo $get->quantity ?>
			</td>
			<td>
				<?php echo $get->receive_amount	 ?>
			</td>
			<td class="text-center" style="font-size: 10px !important">
				<?php truckIcon($get->dock_id, $get->status,'','','','','',$get->gr_material_doc); ?>
			</td>
		</tr>
			<?php
		}
		?>
	</tbody>
</table>