<style>
	table.dataTable thead > tr > th {
    padding-right: 5px;
}
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">dashboard</i>
                    </div>
                    <h4 class="card-title">Schedule Dashboard</h4>
                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->

				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="pull-left" style="margin-bottom: 10px;">
								<form action="<?php echo base_url(); ?>slot/upload_excel" id="form-upload" method="post" enctype="multipart/form-data" style="display: none;">
									<input type="file" name="file" id="file" onchange="$('#form-upload').submit();">
								</form>
								<!-- <button class="btn btn-success" onclick="$('#file').trigger('click')"><i class="fa fa-upload"></i> UPLOAD EXCEL</button> -->
								<button class="btn btn-danger" <?php echo ($this->session->userdata('sess_role_no') == 4)?'style="display: none"':'' ?> onclick="check_delete()"><i class="fa fa-trash"></i> DELETE</button>
								<?php if(isset($_GET['delete'])) {?>
								<b style="color:red;font-size:14px"><?php echo $_GET['delete']; ?> Deleted</b>
								<?php } ?>
								<?php echo "<b style='color:red'>".$this->session->flashdata('ERR')."</b>"; ?>
								<?php echo "<b style='color:green'>".$this->session->flashdata('SCS')."</b>"; ?>
							</div>
						</div>
					</div>
					<div class="table-responsive">
					<table id="example" class="table table-bordered table-bordered table-striped data-table" style="margin-top: 10px;width: 100%;padding-right:10px!important">
						<thead>
							<tr>
								<th width="20" <?php echo ($this->session->userdata('sess_role_no') == 4)?'style="display: none"':'' ?> align="center"><input type="checkbox" id="checkAll" name="checkall"></th>
								<th width="50" class="text-center">Category</th>
								<th class="text-center">PO Number#</th>
								<th class="text-center">Line</th>
								<th class="text-center">Material Code</th>
								<th class="text-center">Material Name</th>
								<th class="text-center">Quantity</th>
								<th class="text-center">Outstanding Qty</th>
								<th class="text-center">Vendor Name</th>
								<th class="text-center">Request Del.Date</th>
								<th class="text-center">Shift</th>
								<th class="text-center">Status</th>
								<th class="text-center">Req. Qty (Pallet)</th>
								<th class="text-center">Send (Pallet)</th>
								<th class="text-center" width="10px">Outs. (Pallet)</th>
							</tr>
						</thead>
						<tbody> 
							<?php

						foreach($rds->result() as $get){
							$plt_kurang = $get->req_pallet - $get->TOTAL;
							?>
							<tr>
								<td <?php echo ($this->session->userdata('sess_role_no') == 4)?'style="display: none"':'' ?>>
									<?php if($get->statz == 0){ ?>
									<input type="checkbox" value="<?php echo $get->id; ?>"  id="checkItem" name="check_item">
									<?php } ?>
								</td>
								<td width="20" align="center"><?php echo $get->category; ?></td>
								<td><?php echo $get->po_number; ?></td>
								<td align="center"><?php echo $get->po_line_item; ?></td>
								<td><?php echo $get->material_code; ?></td>
								<td><?php echo $get->material_name; ?></td>
								<td><?php echo $get->qty." ".$get->uom; ?></td>
								<td><?php echo $plt_kurang*$get->uom_plt; echo " ".$get->uom?></td>
								<td align="center"><?php echo $get->vendor_alias; ?></td>
								
								<td><?php echo Date('d-m-Y',strtotime($get->requested_delivery_date)); ?></td>
								<td align="center"><?php echo $get->shift; ?></td>
								<td align="center"><?php LegendIcon($get->statz); ?></td>
								<td class="text-center"><?php echo $get->req_pallet ?></td>
								<td class="text-center"><?php echo ($get->TOTAL == null)?0:$get->TOTAL ?></td>
								<td class="text-center"><?php echo ($get->req_pallet - $get->TOTAL) ?></td>
							</tr>
								<?php
							}

							?>
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</section>
</div>

</div>
</div>
</div>
</div>
</div>
</div>