<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        Time Slotting
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
      <div class="box">
        <div class="box-header">
          <label>Time Slotting</label>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <div class="pull-left" style="margin-bottom: 10px;">
                <form action="<?php echo base_url(); ?>slot/upload_excel" id="form-upload" method="post" enctype="multipart/form-data" style="display: none;">
                  <input type="file" name="file" id="file" onchange="$('#form-upload').submit();">
                </form>

                <button class="btn btn-success" onclick="$('#file').trigger('click')"><i class="fa fa-upload"></i> UPLOAD EXCEL</button>
                 <?php if(isset($_GET['status'])){echo "<b style='color:green;font-size:13px'>Upload Success</b>";} ?>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-bordered table-striped data-table" style="margin-top: 10px;">
            <thead>
              <tr>
                <th class="text-center">Category</th>
                <th class="text-center">PO Number#</th>
                <th class="text-center">Material Code</th>
                <th class="text-center">Material Name</th>
                <th class="text-center">Vendor ID</th>
                <th class="text-center">Vendor Name</th>
                <!-- <th class="text-center">RDD</th> -->
                <th class="text-center">Status</th>
                 <th class="text-center">Request Del.Date</th>
                <th class="text-right">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php

            foreach($rds->result() as $get){
              ?>
              <tr>
                <td><?php echo $get->category; ?></td>
                <td><?php echo $get->po_number; ?></td>
                <td><?php echo $get->material_code; ?></td>
                <td><?php echo $get->material_name; ?></td>
                <td><?php echo $get->vendor_code; ?></td>
                <td><?php echo $get->vendor_name; ?></td>
                <td><?php 
                if($get->status == 0){
                  echo "NOT SCHEDULED";
                }else{
                  echo "SCHEDULED";
                }
                ?></td>
                <td><?php echo $get->requested_delivery_date; ?></td>
                <td class="text-right">
                  <?php
                  if($sess_role == 1){
                    ?>
                    <a href="<?php echo base_url()."slot/detail/".$get->po_number; ?>" class="btn btn-info btn-sm" title="View Detail"><i class="fa fa-eye"></i></a>
                    <button class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                    <?php
                  }else if($sess_role == 2){
                    ?>
                    <a href="<?php echo base_url()."slot/detail/".$get->po_number; ?>" class="btn btn-info btn-sm" title="View Detail"><i class="fa fa-eye"></i></a>
                    <?php
                  }else{
                    ?>
                    <a href="<?php echo base_url()."slot/detail/".$get->po_number; ?>" class="btn btn-info btn-sm" title="View Detail"><i class="fa fa-eye"></i></a>
                    <?php
                  }
                  ?>
                </td>
              </tr>
                <?php
              }

              ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
</div>