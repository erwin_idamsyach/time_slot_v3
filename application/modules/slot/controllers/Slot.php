<?php
require_once APPPATH.'third_party/spout-master/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
class Slot extends CI_Controller{
	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		isLogin();		
	}

	public function index(){
		date_default_timezone_set("Asia/Bangkok");
		$week = $this->input->get('week');
		$year = $this->input->get('year');
		$datenow = date('Y-m-d 00:00:01', strtotime('-1 days'));
		$category = $this->input->get('category');
		$get_day = date('D');

		$listDate = getDateListText($week, $year);

		$time_freeze = $this->db->query("SELECT start_time, end_time FROM tb_time_freeze WHERE day ='$get_day' ");
		if($this->session->userdata('sess_role_no') != 3){
			
			$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,
										 SUM(b.receive_amount) as receive_amount,
										 (
											SELECT
												SUM(quantity)
											FROM
												tb_scheduler
											WHERE
												id_schedule = a.id
											AND status != '0'
										) AS SEND_ACT,
										(
											SELECT
												SUM(receive_amount)
											FROM
												tb_scheduler
											WHERE
												id_schedule = a.id
										) AS REC_ACT,
										(
											SELECT
												SUM(quantity)
											FROM
												tb_scheduler
											WHERE
												id_schedule = a.id
											AND status != '0'
											AND receive_amount=0
										) AS OUT_REC_ACT, 
				 (SELECT status FROM tb_schedule_detail 
				 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS statz,
				 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name,
				 e.gr_material_doc, 
				 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name 
				 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
				 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
				 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
				 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = b.schedule_number
				 WHERE a.week LIKE '$week' AND a.requested_delivery_date IN($listDate)
				 AND a.category LIKE '%$category%' 
				 -- AND material_code ='67750280' 
				 GROUP BY a.id
				 ORDER BY
				 	a.requested_delivery_date,
				 	a.shift ASC
");
			
		}else{
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,
										 SUM(b.receive_amount) as receive_amount,
										 (
											SELECT
												SUM(quantity)
											FROM
												tb_scheduler
											WHERE
												id_schedule = a.id
											AND status != '0'
										) AS SEND_ACT,
										(
											SELECT
												SUM(receive_amount)
											FROM
												tb_scheduler
											WHERE
												id_schedule = a.id
										) AS REC_ACT,
										(
											SELECT
												SUM(quantity)
											FROM
												tb_scheduler
											WHERE
												id_schedule = a.id
											AND status != '0'
											AND receive_amount=0
										) AS OUT_REC_ACT, 
				 (SELECT status FROM tb_schedule_detail 
				 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS statz,
				 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name,
				 e.gr_material_doc, 
				 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name
										 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
										 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
										 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
										 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = b.schedule_number
										 WHERE a.week LIKE '$week' AND a.requested_delivery_date IN($listDate)
										 AND a.category LIKE '%$category%' 
										 AND a.vendor_code = '$vendor_code'
										 GROUP BY b.id_schedule
										 ORDER BY
										 	a.requested_delivery_date,
										 	a.shift ASC
				");
		}

		$get_category = $this->db->query("SELECT category FROM ms_category");
		$data['category'] = $get_category;
		$week = $this->db->query("SELECT week from tb_rds_detail GROUP BY week");
		$data['week'] = $week;
		$data['rds'] = $getData;
		$data['time_freeze'] = $time_freeze;
		$data['sess_role'] = $this->session->userdata('sess_role_no');
		getHTML('slot/index', $data);
	}

	public function upload_excel(){
		date_default_timezone_set("Asia/Bangkok");
		$file = $this->input->post('file', TRUE);
		$arr_vl = array();
		$date_c='';
		$config['upload_path'] = './assets/upload/excel'; 
  		$config['file_name'] = $file;
  		$config['allowed_types'] = 'xlsx|csv|xls';
  		$config['max_size'] = 0;
  		$this->load->library('upload', $config);
	  	$this->upload->initialize($config);
	  	if (!$this->upload->do_upload('file')) {
	  		echo $this->upload->display_errors();
	   		$this->session->set_flashdata('ERR', $this->upload->display_errors());
	   		redirect('slot?status=error_f');
	  	} else {
	  		$media = $this->upload->data();
	   		$inputFileName = 'C:\xampp\htdocs\time_slot\assets\upload/excel/'.$media['file_name'];
	   		$fn = $media['file_name'];
	   		$exp = explode(".", $fn);
	   		$ext = $exp[1];
	   		$sheetname = 'Processor';
	   		if($ext == "xlsx"){
	   			$reader = ReaderFactory::create(Type::XLSX); //set Type file xlsx
	   		}else if($ext == "xls"){
	   			$reader = ReaderFactory::create(Type::XLS); //set Type file xls
	   		}
	   		$no = 1;
	   		$reader->open($inputFileName);
	        foreach ($reader->getSheetIterator() as $sheet) {
	        	if($sheet->getName() == $sheetname){
		        	foreach ($sheet->getRowIterator() as $rn => $get) {
		        		
		        		if($rn > 4){
		        			$rdd = $get[37];
		        			if(is_object($rdd)){
			        			 	$datenew = $rdd->format('Y-m-d');
			        			 }else{
			        			 	continue;
			        			 }

		        			$ars = array("DP","DS","DM");
		        			if(!empty($get[29]) && $datenew >= date('Y-m-d 00:00:01', strtotime('-1 days'))){
		        				 
			        			 $material_code = $get[1];
			        			 $MR = $get[5];
			        			 $get_category = explode(' ', $get[6]);
			        			 $category = $get_category[0];
			        			 $get_vendor = $get[23];
			        			 $po_number = $get[29];
			        			 $line_item = $get[30];
			        			 $vendor = explode(' ',$get_vendor);
			        			 $vendor_code = $vendor[0];
			        			 $qty = $get[31];
			        			 $uom = $get[21];
			     				 $shift = $get[38];
			        			 $uom_plt = $get[26];
			        			 $plt_truck = '32';
			        			 $wk = $get[38];
			        			 /*echo $wk." ".!in_array($wk, $ars)."<br>";*/
			        			 $req_plt = round($qty / $uom_plt);
			        			 $get_po_number = '';
			        			 $get_rdt = '';
			        			 $get_week = '';
			        			 $get_material = '';
			        			 $get_qty = '';
			        			 $get_material_name = $get[25];

			        			 $material_checker = $this->db->query("SELECT material_sku FROM skin_master.ms_material WHERE material_sku = '$material_code' ")->num_rows();
			        			 if($material_checker == 0){
			        			 	$array = array(
							   			"material_sku" => $material_code,
							   			"material_name" => $get_material_name,
							   			"material_uom"	=> $uom
							   		);

							   		$this->db->insert('skin_master.ms_material', $array);
			        			 }

			        			 $data_checker = $this->db->query("SELECT id, po_number,material_code, requested_delivery_date,shift, qty FROM tb_rds_detail where po_number='$po_number' AND requested_delivery_date = '$datenew' AND material_code ='$material_code' AND shift='$shift'  ")->row();

			        			 if(!empty($data_checker)){
			        			 	$get_po_number = $data_checker->po_number;
			        				$get_rdt = $data_checker->requested_delivery_date;
			        				$get_material = $data_checker->material_code;
			        				$get_shift = $data_checker->shift;
			        				$get_qty = $data_checker->qty;
			        			 }		   
			        			$data = array(
			        			 		"category" 	=> $category,
				        				"po_number" => $po_number,
				        				"po_line_item" => $line_item,
				        				"material_code" => $material_code,
				        				"vendor_code" => $vendor_code,
				        				"qty" => $qty,
				        				"uom" => $uom,
				        				"requested_delivery_date" => $datenew,
				        				"uom_plt" => $uom_plt,
				        				"plt_truck" => $plt_truck,
				        				"req_pallet" => $req_plt,
				        				"shift" => $shift,
				        				"week" => substr($wk,-2),
				        				'is_schedule'=> '0',
		        				);
			        			 if($get_po_number == $po_number && $datenew == $get_rdt && $get_material == $material_code){
			        			 	
								  }
			         			 else if($get_po_number == $po_number && $datenew == $get_rdt && $get_material == $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew <> $get_rdt && $get_material == $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew <> $get_rdt && $get_material == $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew <> $get_rdt && $get_material <> $material_code){
		         					$this->db->where('po_number',$po_number);
		         					$this->db->update('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == $po_number && $datenew == $get_rdt && $get_material <> $material_code){
		         					$this->db->insert('tb_rds_detail', $data);
			        			 	
			         			 }else if($get_po_number == '' && $get_rdt == '' && $get_material == ''){
		         					$this->db->insert('tb_rds_detail', $data);
			        			 	
			         			}
		        			}else{
		        				continue;
		        			}
		        		}
		        	}
	        	}
	        }
	    }
	      $this->session->set_flashdata('SCS', 'Upload Success');
	      redirect('slot?status=success');
	}


	public function detail(){
		$po_number = $this->uri->segment(3);
		$material_code = $this->uri->segment(4);
		$id = $this->uri->segment(5);
		$getInfo = $this->db->query("SELECT 
			a.category, 
			a.po_number, 
			a.vendor_code, 
			a.requested_delivery_date,
			b.vendor_name,
			b.vendor_alias,
			a.week,
			a.shift
			FROM 
			tb_rds_detail a 
			INNER JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
			WHERE a.id='$id' ")->row();

		$getList = $this->db->query("SELECT 
			a.id, a.category, 
			a.po_number, 
			a.material_code, 
			a.vendor_code, 
			a.qty, 
			a.uom,
			a.uom_plt, 
			a.requested_delivery_date, 
			a.status, 
			b.material_name,
			c.vendor_name
			FROM 
			tb_rds_detail a 
			LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku 
			INNER JOIN skin_master.ms_supplier c ON a.vendor_code = c.vendor_code
			WHERE a.id='$id'");

		$data['po_number'] = $po_number;
		$data['info'] = $getInfo;
		$data['item_list'] = $getList;
		getHTML('slot/detail_po', $data);
	}

	public function delete(){
		$po_number = $this->uri->segment(3);
		$this->db->query("DELETE FROM tb_rds_detail WHERE po_number = '$po_number' ");
		redirect('slot?delete='.$po_number);
	}

	public function set_schedule(){
		$po_number = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$getTimeSlot = $this->db->query("SELECT * FROM ms_time_slot");
		$getData = $this->db->query("SELECT 
								a.*, 
								b.material_name,
								c.vendor_name
								FROM tb_rds_detail a 
								INNER JOIN skin_master.ms_material b ON a.material_code = b.material_sku 
								INNER JOIN skin_master.ms_supplier c ON a.vendor_code = c.vendor_code
								WHERE a.id='$id'");

		$data['po_number'] = $po_number;
		$data['data_po'] = $getData;
		$data['time_slot'] = $getTimeSlot;
		$data['id_schedule'] = $id;
		getHTML('slot/set_schedule', $data);
	}

	public function get_truck_detail(){
		$truck_no = $this->input->get('truck_no');
		$date = $this->input->get('date');
		
		$date = str_replace("/", "-", $date);
		$date_n = date_create($date);
		$date_new = date_format($date_n, 'Y-m-d');

		$cData = $this->db->query("SELECT SUM(pallet_count) as COUNT FROM tb_delivery_detail WHERE id_truck='$truck_no' AND delivery_date='$date_new'")->row();
		$count = $cData->COUNT;

		$available = 32-$count;
		echo $available;
	}

	public function save_schedule(){
		$id_schedule = $this->input->post('id_schedule');
		$delivery_date = $this->input->post('date');
		$time_slot = $this->input->post('time_slot');
		$truck_no = $this->input->post('truck_no');
		$f_invoice = $this->input->post('inv_file', TRUE);
		$po_number = $this->input->post('po_number');
		$slot_usage = $this->input->post('slot_usage');

		$date = str_replace("/", "-", $delivery_date);
		$date_n = date_create($date);
		$date_new = date_format($date_n, 'Y-m-d');

		$config['upload_path'] = './assets/upload/invoice'; 
  		$config['file_name'] = $po_number."_".$id_schedule."_".$f_invoice;
  		$config['allowed_types'] = 'docx|doc|pdf';
  		$config['max_size'] = 0;

  		$this->load->library('upload', $config);
	  	$this->upload->initialize($config);

	  	if (!$this->upload->do_upload('inv_file')) {
	   		$this->session->set_flashdata('ERR', $this->upload->display_errors()); 
	   		redirect('slot/set_schedule/'.$po_number."/".$id_schedule);
	  	} else {
	  		$media = $this->upload->data();
	   		$fileName = 'assets\upload/invoice/'.$media['file_name'];

	   		$array = array(
	   			"id_schedule" => $id_schedule,
	   			"pallet_count" => $slot_usage,
	   			"delivery_date" => $date_new,
	   			"id_time_slot" => $time_slot,
	   			"id_truck" => $truck_no,
	   			"invoice" => $fileName
	   		);

	   		$this->db->insert('tb_delivery_detail', $array);

	   		$this->db
	   			->where('id', $id_schedule)
	   			->update('tb_rds_detail', array(
	   				"status" => '1'
	   			));
	  	}
	  	redirect('slot/detail/'.$po_number);
	}

	public function check_delete(){
		$data = $this->input->get('check_item');
		$no = 0;
		$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H-i-s'),
	  			"action"		=> "Delete",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_rds_detail",
	  			"select_join"	=> "",
	  			"author"		=> 2
	  	);
		foreach($data as $get){
			$check_item = $data[$no];
			$data_item = $check_item['check_item'];
			$dataz = $this->db->query("SELECT po_number,material_code,week, po_line_item, sap_line, qty FROM tb_rds_detail WHERE id = '$data_item' ")->row();
			$purchase_order = $dataz->po_number;
			$line 			= $dataz->po_line_item;
			$sap_line 		= $dataz->sap_line;
			$qty 			= $dataz->qty;
			
			$value = "PO : ".$dataz->po_number." ";
			$value2 = "Line :".$dataz->po_line_item." week:".$dataz->week;

			$history['id_join'] = $data_item;
			$history['description'] = "Delete RDS ".$value.$value2;
			$history['value']	= $value;
			$history['value2']	= $value2;
			$this->db->insert('tb_history', $history);
			
			$this->db->query("DELETE FROM tb_rds_detail WHERE id = '$data_item' ");
			$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule = '$data_item' ");
			if($sap_line != "" || $sap_line != null){
				changeUsage($purchase_order, $sap_line, $qty, "MINUS");
			}
			$no++;
		}
		echo json_encode(array('status' => "success"));
	}

	public function get_delivery_detail(){
		$id = $this->input->get('id');

		$getData = $this->db->query("SELECT
						a.delivery_date,
						a.id_time_slot,
						a.id_truck,
						a.invoice,
						a.pallet_count,
						b.po_number,
						b.material_code,
						b.vendor_code,
						b.qty,
						b.requested_delivery_date,
						c.material_name,
						d.start_time,
						d.end_time,
						e.vendor_name
					FROM
						tb_delivery_detail a
					INNER JOIN tb_rds_detail b ON a.id_schedule = b.id
					INNER JOIN skin_master.ms_material c ON b.material_code = c.material_sku
					INNER JOIN ms_time_slot d ON a.id_time_slot = d.id
					INNER JOIN skin_master.ms_supplier e ON b.vendor_code = e.vendor_code
					WHERE
						b.id='$id'");
		$data['data_list'] = $getData;
		$this->load->view('slot/get_list_delivery_data', $data);
	}

	public function download_rds(){
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$category = $this->input->get('category');
		$filePath = "./assets/templates/template_raw_rds.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $sheetNames = $objPHPExcel->getSheetNames();

		$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,
										 SUM(b.receive_amount) as receive_amount,
										 (
												SELECT
													SUM(quantity)
												FROM
													tb_scheduler
												WHERE
													id_schedule = a.id
												AND receive_amount = 0
											) AS SEND_ACT,
											(
												SELECT
													SUM(receive_amount)
												FROM
													tb_scheduler
												WHERE
													id_schedule = a.id
											) AS REC_ACT, 
										 (SELECT status FROM tb_schedule_detail 
										 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS statz,
										 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name, 
										 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name 
										 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
										 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
										 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
										 WHERE a.week LIKE '%$week%' AND YEAR(a.requested_delivery_date) = '$year'
										 AND a.category LIKE '%$category%' 
										 -- AND material_code ='67750280' 
										 GROUP BY b.id_schedule
										 ORDER BY
										 	a.requested_delivery_date,
										 	a.shift ASC
				");
		$no = 1;
		foreach ($getData->result() as $get) {
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.($no+2), $no);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.($no+2), $get->category);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.($no+2), $get->po_number);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.($no+2), $get->po_line_item);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.($no+2), $get->material_code);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.($no+2), $get->material_name);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.($no+2), $get->qty." ".$get->uom);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.($no+2), ($get->req_pallet - $get->receive_amount) * $get->uom_plt);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.($no+2), $get->vendor_alias);
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.($no+2), $get->requested_delivery_date);
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.($no+2), $get->shift);
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.($no+2), $get->statz);
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.($no+2), $get->req_pallet);
			
			$send_pallet = $get->SEND_ACT+$get->REC_ACT;
			if($send_pallet == 0){ $send_pallet = 0;} 

			
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.($no+2), $send_pallet);

			if($get->statz == '3'){
				$receive_amount = $get->receive_amount;
			}else{ 
				$receive_amount = 0;
			}

			$objPHPExcel->getActiveSheet()->SetCellValue('O'.($no+2), $receive_amount);

			if($get->sisa != 0){
				$sisa = ($send_pallet - $get->receive_amount);
			}else{
				$sisa = ($get->req_pallet - $get->receive_amount);
			}
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.($no+2), $sisa);

			

			if($get->created_at == "0000-00-00 00:00:00" ){
									$created_at = '';
								}else{
									$created_at = Date('d/m H:i', strtotime($get->created_at) );
								} 
			$objPHPExcel->getActiveSheet()->SetCellValue('Q'.($no+2), $get->created_name."-".$created_at);

			if($get->update_at == "0000-00-00 00:00:00" ){
									$update_at = '';
								}else{
									$update_at = Date('d/m H:i', strtotime($get->update_at) );
								}

			
			$objPHPExcel->getActiveSheet()->SetCellValue('R'.($no+2), $get->edit_name."-".$update_at);
			$no++;
		}
		header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="raw_rds_'.date('d_m_Y').'.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');

	}

	public function view_history(){
		$db2 = $this->load->database('second', TRUE);

		$id = $this->input->get('id');

		$getRDSData = $this->db->query("SELECT po_number, po_line_item, b.material_name, requested_delivery_date, tb_rds_detail.vendor_code, material_code, c.vendor_name, req_pallet FROM 
			tb_rds_detail
			INNER JOIN ".$db2->database.".ms_material b ON tb_rds_detail.material_code = b.material_sku
			INNER JOIN ".$db2->database.".ms_supplier c ON tb_rds_detail.vendor_code = c.vendor_code
			WHERE tb_rds_detail.id='$id'");

		$getLog = $this->db->query("SELECT a.receipt, a.delivery_date, b.quantity, b.receive_amount, c.status, c.rdd, c.dock_id, d.start_time, d.end_time, gr_material_doc
			FROM 
			tb_delivery_detail a
			INNER JOIN tb_schedule_detail c ON a.id_schedule_group = c.schedule_number
			INNER JOIN tb_scheduler b ON a.id_schedule_group = b.schedule_number
			INNER JOIN ms_time_slot d ON c.id_time_slot = d.id
			WHERE b.id_schedule=$id");

		$data['rds_data'] = $getRDSData;
		$data['mat_log'] = $getLog;

		$this->load->view('slot/history', $data);
	}

	public function search_multiple(){
		$filter = $this->input->get('filter');
		$f_data = $this->input->get('f_data');
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$category = $this->input->get('category');

		if($this->session->userdata('sess_role_no') != 3){
			
			$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,
										 SUM(b.receive_amount) as receive_amount,
										 (
		SELECT
			SUM(quantity)
		FROM
			tb_scheduler
		WHERE
			id_schedule = a.id
		AND receive_amount = 0
	) AS SEND_ACT,
	(
		SELECT
			SUM(receive_amount)
		FROM
			tb_scheduler
		WHERE
			id_schedule = a.id
	) AS REC_ACT, 
				 (SELECT status FROM tb_schedule_detail 
				 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS statz,
				 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name,
				 e.gr_material_doc, 
				 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name 
				 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
				 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
				 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
				 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = b.schedule_number
				 WHERE a.week LIKE '%$week%' AND YEAR(a.requested_delivery_date) = '$year'
				 AND a.category LIKE '%$category%'
				 AND $filter IN ($f_data)
				 -- AND material_code ='67750280' 
				 GROUP BY a.id
				 ORDER BY
				 	a.requested_delivery_date,
				 	a.shift ASC
");
			
		}else{
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$getData = $this->db->query("SELECT a.*, (a.req_pallet - SUM(b.quantity)) AS sisa
										 , c.vendor_alias, d.material_name, d.uom_pallet,
										 SUM(b.receive_amount) as receive_amount,
										 (
		SELECT
			SUM(quantity)
		FROM
			tb_scheduler
		WHERE
			id_schedule = a.id
		AND receive_amount = 0
	) AS SEND_ACT,
	(
		SELECT
			SUM(receive_amount)
		FROM
			tb_scheduler
		WHERE
			id_schedule = a.id
	) AS REC_ACT, 
										 (SELECT status FROM tb_schedule_detail 
										 WHERE schedule_number = b.schedule_number GROUP BY b.id_schedule) AS statz,
										 (SELECT nama FROM ms_user WHERE id = a.created_by ) AS created_name, 
										 (SELECT nama FROM ms_user WHERE id = a.update_by ) AS edit_name,
										 e.gr_material_doc 
										 FROM `tb_rds_detail` AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule 
										 LEFT JOIN skin_master.ms_supplier AS c ON a.vendor_code = c.vendor_code 
										 LEFT JOIN skin_master.ms_material AS d ON a.material_code = d.material_sku 
										 LEFT JOIN tb_delivery_detail AS e ON e.id_schedule_group = b.schedule_number
										 WHERE a.week LIKE '%$week%' AND YEAR(a.requested_delivery_date) = '$year'
										 AND a.category LIKE '%$category%' 
										 AND a.vendor_code = '$vendor_code'
										 AND $filter IN ($f_data)
										 GROUP BY b.id_schedule
										 ORDER BY
										 	a.requested_delivery_date,
										 	a.shift ASC
				");
		}

		$data['data'] = $getData;

		$this->load->view('slot/table_search_m', $data);
	}

	public function getDateList($week, $year) {
	  $dto = new DateTime();
	  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
	  $s = 1;
	  while($s <= 6){
	  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
	  	$s++;
	  }
	  return $ret;
	}

}
?>