<?php
class Return_vendor extends CI_Controller
{

	function __Construct()
	{
		parent::__Construct();
		date_default_timezone_set("Asia/Bangkok");
		isLogin();
	}

	public function index()
	{
		date_default_timezone_set("Asia/Bangkok");
		$week = $this->input->get('week');
		$year = $this->input->get('year');
		$ld = getDateListText($week, $year);
		$datenow = date('Y-m-d 00:00:01', strtotime('-1 days'));
		$category = $this->input->get('category');
		$vendor_code = $this->input->get('supplier');
		$get_day = date('D');
		$receipt = (null === $this->input->get('receipt')) ? "" : $this->input->get('receipt');
		$time_freeze = $this->db->query("SELECT start_time, end_time FROM tb_time_freeze WHERE day ='$get_day' ");
		$getData = $this->db->query("SELECT
							a.*, b.vendor_alias, b.vendor_code,
							e.dock_id,
							c.material_name,
							f.gr_material_doc,
							(SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                            (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name,
							e. STATUS AS statz,
							f.reason,
							d.quantity AS TOTAL,
							f.receipt,
							d.receive_amount AS RA,
							d.id as IDS,
							e.schedule_number,
							e.dock_id
						FROM
							tb_rds_detail a
						LEFT JOIN tb_scheduler AS d ON a.id = d.id_schedule
						LEFT JOIN tb_schedule_detail AS e ON d.schedule_number = e.schedule_number
						INNER JOIN tb_delivery_detail AS f ON e.schedule_number = f.id_schedule_group
						LEFT JOIN skin_master.ms_supplier b ON a.vendor_code = b.vendor_code
						LEFT JOIN skin_master.ms_material c ON a.material_code = c.material_sku						
						WHERE requested_delivery_date IN ($ld)
						AND IF('$receipt' != '',f.receipt = '$receipt', a.vendor_code LIKE '%$vendor_code%')
						AND e.status = 3
						GROUP BY d.id
						ORDER BY
							a.requested_delivery_date,
							a.shift ASC
							");

		$receipt_data = $this->db->query("SELECT a.receipt FROM tb_delivery_detail AS a INNER JOIN tb_schedule_detail as b ON a.id_schedule_group = b.schedule_number INNER JOIN tb_scheduler AS c ON b.schedule_number = c.schedule_number INNER JOIN tb_rds_detail AS d ON c.id_schedule = d.id WHERE b.status = 3 AND d.category = '$category' AND d.vendor_code LIKE '%$vendor_code%' GROUP BY a.receipt");

		$get_category = $this->db->query("SELECT category FROM ms_category");
		$data['category'] = $get_category;

		$get_supplier = $this->db->query("SELECT vendor_code, vendor_alias FROM skin_master.ms_supplier")->result();

		$week = $this->db->query("SELECT week from tb_rds_detail GROUP BY week");
		$data['supplier']	= $get_supplier;
		$data['receipt'] = $receipt_data;
		$data['week'] = $week;
		$data['year'] = $year;
		$data['rds'] = $getData;
		$data['time_freeze'] = $time_freeze;
		$data['sess_role'] = $this->session->userdata('sess_role_no');
		getHTML('return_vendor/index', $data);
	}

	public function filter()
	{
		$menu = $this->input->get('menu');
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		//$get_category = $this->db->query("SELECT id FROM ms_category WHERE category = '$category' ")->row()->id;

		$data = [];
		if($menu == 1){

			$data = $this->db->query("SELECT a.receipt FROM tb_delivery_detail AS a INNER JOIN tb_schedule_detail AS b ON a.id_schedule_group = b.schedule_number INNER JOIN tb_scheduler AS c ON b.schedule_number = c.schedule_number LEFT JOIN tb_rds_detail AS d ON c.id_schedule = d.id WHERE YEAR(a.delivery_date) = '$year' AND d.week = '$week' GROUP BY a.receipt")->result();
		
				foreach ($data as $get) {
					echo "<option value='".$get->receipt."'> ".$get->receipt."</option>";
				}
		}else{
			$week = $week - 1 ;
			$data = $this->db->query("SELECT b.vendor_name, b.vendor_code, b.vendor_alias FROM tb_schedule_detail AS a INNER JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code WHERE YEAR(a.rdd) = '$year' AND '$week' = week(a.rdd) GROUP BY b.vendor_code")->result();

			foreach ($data as $get) {
				echo "<option value='" . $get->vendor_code . "'>" . $get->vendor_name . " " . $get->vendor_alias . "</option>";
			}
		}


		return true;
	}

	public function return_material()
	{
		$id = $this->input->post('id');
		$amount = $this->input->post('return');
		$reason = $this->input->post('reason');
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$sn = $this->input->get('schedule_number');
		$po_number = $this->input->post('po_number');

		$category = $this->input->get('category');
		$vendor_code = $this->input->post('vendor_code');
		$vendor_url = '';
		if ($this->input->get('vendor_code') != '') {
			$vendor_url = "&supplier=" . $this->input->post('vendor_code');
		}

		$receipt_url = '';
		$receipt = $this->input->post('receipt');

		if ($this->input->get('receipt') != '') {
			$receipt_url = "&receipt=" . $receipt;
		}

		$material_code = $this->input->post("material_code");
		if ($_FILES['reject_file']['size'] == 0) {
			$foto = '';
		} else {
			$config['allowed_types'] = 'jpg|jpeg|png|bmp';
			$config['max_size'] = 0;
			$config['upload_path'] = 'assets/upload/return_receipt';
			$config['file_name'] = $receipt;
			$foto = '';
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('reject_file')) {
				$data = $this->upload->data();
				$foto = $this->upload->data('file_name');
			} else {
				$error = $this->upload->display_errors();
				$this->session->set_flashdata('Error', $error);
				redirect('return_vendor?year=' . $year . '&week=' . $week . "&category=" . $category . $receipt_url . $vendor_url);
			}
		}


		$arr = array(
			"id_scheduler" => $id,
			"amount" => $amount,
			"reason" => $reason,
			"foto"	 => $foto,
			"created_by" => $this->session->userdata('sess_id'),
			"created_at" => date('Y-m-d H:i:s')

		);

		$check = $this->db->query("SELECT id FROM tb_return_form WHERE id_scheduler = '$id' ");
		$check_id = $check->row()->id;
		if ($check->num_rows() > 0) {
			$this->db->where('id', $check_id);
			$this->db->update('tb_return_form', $arr);
		} else {
			$this->db->insert("tb_return_form", $arr);
		}

		$getData = $this->db->query("SELECT receive_amount, id_schedule FROM tb_scheduler WHERE id = '$id' ")->row();
		$receive_amount = $getData->receive_amount;
		$id_schedule = $getData->id_schedule;

		$new_receive_amount = $receive_amount - $amount;
		$this->db->query("UPDATE tb_scheduler SET receive_amount = receive_amount-$amount WHERE id=$id");

		$mm = array(
			"id_schedule" => $id_schedule,
			"quantity"	  => $amount,
			"action"	  => "Return_Material",
			"created_by"  => $this->session->userdata('sess_id')
		);
		add_material_movement($mm);

		$history = array(
			"date" 			=> Date('Y-m-d'),
			"time"			=> date('H:i:s'),
			"action"		=> "Return Vendor",
			"by_who"		=> $this->session->userdata('sess_id'),
			"table_join"	=> "tb_rds_detail",
			"id_join"		=> $id_schedule,
			"value"			=> $receive_amount,
			"value2"		=> $amount,
			"description"	=> "No Receipt : " . $receipt . ", PO :" . $po_number . ", Material Code :" . $material_code . " Quantity:" . $amount . " Vendor Code :" . $vendor_code,
			"author"		=> 3,
			"vendor_code"   => $this->input->post('vendor_code')
		);

		$this->db->insert('tb_history', $history);

		redirect('return_vendor?year=' . $year . '&week=' . $week . "&category=" . $category . $receipt_url . $vendor_code);
	}
}
