<div class="row">
	<div class="col-md-8">
		<table border='1' class="table table-bordered">
			<tr>
				<th>PO Number#</th>
				<td><?php echo $info->po_number; ?></td>
			</tr>
			<tr>
				<th>Category</th>
				<td><?php echo $info->category; ?></td>
			</tr>
			<tr>
				<th>Vendor Code</th>
				<td><?php echo $info->vendor_code; ?></td>
			</tr>
			<tr>
				<th>Vendor Name</th>
				<td><?php echo "-"; ?></td>
			</tr>
			<tr>
				<th>Requested Delivery Date</th>
				<td><?php 
				$date = date_create($info->requested_delivery_date);
				$df = date_format($date, "D, d M Y");
				echo $df;
				 ?></td>
			</tr>
		</table>	
	</div>
</div>
<hr>
<table class="table table-bordered table-striped table-hover">
	<thead>
	<tr>
		<th class="text-center">No</th>
		<th class="text-center">Material Code</th>
		<th class="text-center">Material Description</th>
		<th class="text-center">Quantity</th>
		<th class="text-center">UOM</th>
		<th>DATE</th>
	</tr>
	</thead>
	<?php
	$a = 1;
	foreach($item_list->result() as $get){
		?>
	<tr>
		<td class="text-center"><?php echo $a++; ?></td>
		<td><?php echo $get->material_code ?></td>
		<td><?php echo $get->material_name; ?></td>
		<td class="text-right"><?php echo number_format($get->qty, 3, ',', ','); ?></td>
		<td><?php echo $get->uom; ?></td>
		<td><?php echo $get->requested_delivery_date; ?></td>
	</tr>
		<?php
	}
	?>
</table>