<style>
  table.dataTable thead>tr>th {
    padding-right: 5px;
  }

  @media screen and (min-width: 400px) {
    .category-4 {
      padding: 0px;
      text-align: center !important;
    }

  }

  @media screen and (min-width: 800px) {
    .category-4 {
      padding: 0px;
      margin-left: 20px;
      position: absolute;
    }
  }
</style>

<style>
  .fa-truck {
    font-size: 18px !important;
  }

  .fa-check {
    font-size: 15px !important;
  }

  .vertical-alignment-helper {
    display: table;
    height: 100%;
    width: 100%;
    pointer-events: none;
  }

  .vertical-align-center {
    /* To center vertically */
    display: table-cell;
    vertical-align: middle;
    pointer-events: none;
  }

  .modal-content {
    margin-left: -275px;
  }
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="fas fa-people-carry" style="font-size:30px"></i>
              </div>
              <h4 class="card-title">Received Management</h4>
            </div>
            <div class="card-body ">
              <?php
              date_default_timezone_set("Asia/Bangkok");
              $time = date('H:i:s');

              $start_time = $time_freeze->row()->start_time;
              $end_time = $time_freeze->row()->end_time;
              $status_time = '';
              if ($time >= $end_time || $time < $start_time) {
                $status_time = "disabled";
              } else {
              }
              ?>
              <div class="row">
                <div class="col">
                  <?php $year = $_GET['year'];
                  $week = $_GET['week']; ?>
                  <table>
                    <tr>
                      <td>
                        <div class="form-group">
                          <label style="margin-left:70px">Year</label>
                          <nav aria-label="Page navigation example">
                            <ul class="pagination pagination-primary">

                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year - 1; ?>&week=<?php echo $week; ?>"><?php echo $year - 1; ?></a>
                              </li>

                              <li class="page-item active">
                                <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $year; ?></a>
                              </li>

                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year + 1; ?>&week=<?php echo $week; ?>"><?php echo $year + 1; ?></a>
                              </li>

                            </ul>
                          </nav>
                        </div>
                      </td>

                      <td>
                        <div class="form-group">
                          <label style="margin-left:85px">Week</label>
                          <nav aria-label="Page navigation example">
                            <ul class="pagination pagination-primary">

                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week - 2; ?>"><?php echo $week - 2; ?></a>
                              </li>
                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week - 1; ?>"><?php echo $week - 1; ?></a>
                              </li>

                              <li class="page-item active">
                                <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>"><?php echo $week; ?></a>
                              </li>
                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week + 1; ?>"><?php echo $week + 1; ?></a>
                              </li>
                              <li class="page-item">
                                <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week + 2; ?>"><?php echo $week + 2; ?></a>
                              </li>


                            </ul>
                          </nav>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>

                <div class="col" style="margin-top:33px">
                  <select name='menu' onchange="select_menu(this.value)" id='menu' class="form-control select2-init">
                    <option value="">SELECT MENU</option>
                    <option value="1">SELECT RECEIPT</option>
                    <?php if ($this->session->userdata('sess_role_no') != '3') {  ?>
                      <option value="2">SELECT SUPPLIER</option>
                    <?php } ?>
                  </select>
                </div>


                <div class="col" style="margin-top:33px;">
                  <select disabled name='item_list' class="form-control select2-init" id="item_list" onchange="">
                    <option value=""></option>

                  </select>
                </div>

                <div class="col" style="margin-top:27px">
                  <button class="btn btn-primary btn-sm" onclick="search_receipt( $('#menu').val(), $('#item_list').val() )"><i class="fas fa-search"></i> Filter</button>
                </div>

              </div>
              <div class="row">
                <div class="col">
                  <?php echo "<b style='color:red'>" . $this->session->flashdata('Error') . "</b>"; ?>
                </div>
              </div>

              <div class="row">


                <div class="col-md-12">
                  <div class="pull-left" style="margin-bottom: 10px;">
                    <form action="<?php echo base_url(); ?>slot/upload_excel" id="form-upload" method="post" enctype="multipart/form-data" style="display: none;">
                      <input type="file" name="file" id="file" onchange="$('#form-upload').submit();">
                    </form>

                    <?php if (isset($_GET['delete'])) { ?>
                      <b style="color:red;font-size:14px"><?php echo $_GET['delete']; ?> Deleted</b>
                    <?php } ?>
                    <?php echo "<b style='color:red'>" . $this->session->flashdata('ERR') . "</b>"; ?>
                    <?php echo "<b style='color:green'>" . $this->session->flashdata('SCS') . "</b>"; ?>
                  </div>
                </div>

              </div>
              <div class="table-responsive">
                <table id="table" class="table table-striped table-bordered table-hover" style="margin-top: 10px;width: 100%;padding-right:10px!important;">
                  <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:14px;white-space: nowrap;">
                    <tr>

                      <th <?php if (!check_sub_menu(18)) {
                            echo "hidden";
                          } ?> style="font-size:14px" width="20" align="center">Return</th>

                      <th style="font-size:14px" width="50" class="text-center">Category</th>
                      <th style="font-size:14px" class="text-center">Receipt#</th>
                      <th style="font-size:14px" class="text-center">PO Number</th>
                      <th style="font-size:14px" class="text-center">Line</th>
                      <th style="font-size:14px" class="text-center">Material Code</th>
                      <th style="font-size:14px" class="text-center">Material Name</th>
                      <th style="font-size:14px" class="text-center">Vendor Name</th>
                      <th style="font-size:14px" class="text-center">Request Del.Date</th>
                      <th style="font-size:14px" class="text-center">Shift</th>
                      <th style="font-size:14px" class="text-center">Status</th>

                      <th style="font-size:14px" class="text-center">Send (Pallet)</th>
                      <th style="font-size:14px" class="text-center" width="10px">Return. (Pallet)</th>
                      <th style="font-size:14px" class="text-center">Reason</th>
                      <th style="font-size:14px" class="text-center" width="10px">Actual Receive. (Pallet)</th>
                      <th style="font-size:14px" class="text-center" width="10px">Created By</th>
                      <th style="font-size:14px" class="text-center" width="10px">Update By</th>
                    </tr>
                  </thead>
                  <tbody id="table_data" style="white-space: nowrap">
                    <?php

                    foreach ($rds->result() as $get) {
                      $plt_kurang = $get->req_pallet - $get->TOTAL;
                    ?>
                      <tr>

                        <td <?php if (!check_sub_menu(18)) {
                              echo "hidden";
                            } ?> class="text-center">
                          <a href='#' onclick="openReturnModal(<?php echo $get->IDS ?>, <?php echo $get->RA ?>, '<?php echo $get->vendor_code; ?>', '<?php echo $get->receipt; ?>', '<?php echo $get->material_code; ?>', '<?php echo $get->schedule_number; ?>','<?php echo $get->po_number; ?>')"><span style="font-size:18px"><i class="fa fa-undo" style="color:green"></i></span></a>
                        </td>
                        <td style="font-size:12px" width="20" align="center"><?php echo $get->category; ?></td>
                        <td style="font-size:12px"><?php echo $get->receipt; ?></td>
                        <td style="font-size:12px"><?php echo $get->po_number; ?></td>
                        <td style="font-size:12px" align="center"><?php echo $get->po_line_item; ?></td>
                        <td style="font-size:12px"><?php echo $get->material_code; ?></td>
                        <td style="font-size:12px"><?php echo $get->material_name; ?></td>
                        <td style="font-size:12px" align="center"><?php echo $get->vendor_alias; ?></td>
                        <td style="font-size:12px"><?php echo Date('d-m-Y', strtotime($get->requested_delivery_date)); ?></td>
                        <td style="font-size:12px" align="center"><?php echo $get->shift; ?></td>
                        <td style="font-size:8px; text-align:left" ondblclick="getDetailedInformation('<?php echo $get->schedule_number ?>','<?php echo $get->statz; ?>')" class="text-center" style="font-size:11px">
                          <div style="margin-left:-20px;">
                            <?php truckIcon($get->dock_id, $get->statz, '', 0, 0, 0, 0, $get->gr_material_doc); ?>
                          </div>
                        </td>
                        <td style="font-size:12px" class="text-center"><?php echo $get->TOTAL ?></td>
                        <td style="font-size:12px" class="text-center"><?php echo ($get->TOTAL - $get->RA) ?></td>
                        <td style="font-size:12px" class="text-center"><?php if ($get->TOTAL - $get->RA != 0) {
                                                                          echo $get->reason;
                                                                        } else {
                                                                          echo " - ";
                                                                        } ?></td>
                        <td style="font-size:12px" class="text-center"><?php echo $get->RA ?></td>
                        <?php if ($get->created_at == "0000-00-00 00:00:00") {
                          $created_at = '';
                        } else {
                          $created_at = Date('d/m H:i', strtotime($get->created_at));
                        } ?>
                        <td style="font-size:12px" class="text-center dataz" width="60"><?php echo $get->created_name; ?><br><?php echo $created_at; ?></td>
                        <?php if ($get->update_at == "0000-00-00 00:00:00") {
                          $update_at = '';
                        } else {
                          $update_at = Date('d/m H:i', strtotime($get->update_at));
                        } ?>
                        <td style="font-size:12px" class="text-center dataz" width="60"><?php echo $get->edit_name ?><br><?php echo $update_at; ?></td>
                      </tr>
                    <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          </section>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog modal-sm">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <h4 class="modal-title">Return Form</h4>

              </div>
              <div class="modal-body">
                <form action="<?php echo base_url(); ?>return_vendor/return_material?year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week']; ?>&supplier=<?php if (!empty($_GET['supplier'])) {
                                                                                                                                                                    echo $_GET['supplier'];
                                                                                                                                                                  }  ?>&receipt=<?php if (!empty($_GET['receipt'])) {
                                                                                                                                                                                                                            echo $_GET['receipt'];
                                                                                                                                                                                                                          } ?>" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="id" class="id_material" value="">
                  <input type="hidden" name="vendor_code" id="vendor_code" value="">
                  <input type="hidden" value="" name="receipt" id="receipt">
                  <input type="hidden" value="" name="material_code" id="material_code">
                  <input type="hidden" value="" name="po_number" id="po_number">
                  <input type="hidden" name="schedule_number" value="" id="schedule_number">
                  <div class="form-group">
                    <label>Returned Amount</label>
                    <input type="text" name="return" onkeyup="max_return(this.value)" class="total_material form-control" value="">
                  </div>
                  <div class="form-group">
                    <textarea id="" class="form-control" name="reason" placeholder="Reason"></textarea>
                  </div>
                  <label>Foto</label>
                  <input type="file" class="form-control btn btn-primary" name="reject_file">

                  <button class="btn btn-primary btn-sm" id="return_button" type="submit">SUBMIT</button>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>