<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        <a href="<?php echo base_url()."slot/detail/".$po_number ?>" class="btn btn-info">
          <i class="fa fa-arrow-left"></i> BACK
        </a><br>
        Set Schedule
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>
    <?php foreach($data_po->result() as $get){} ?>
    <section class="content" style="padding:19px">
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <label>Set Schedule</label>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6" style="border-right: 1px solid #A1A1A1;">
                  <table class="table">
                    <tr>
                      <th>PO Number#</th>
                      <td><?php echo $get->po_number; ?></td>
                    </tr>
                    <tr>
                      <th>Vendor Code</th>
                      <td><?php echo $get->vendor_code; ?></td>
                    </tr>
                    <tr>
                      <th>Vendor Name</th>
                      <td><?php echo $get->vendor_name; ?></td>
                    </tr>
                    <tr>
                      <th>Material Code</th>
                      <td><?php echo $get->material_code; ?></td>
                    </tr>
                    <tr>
                      <th>Material Name</th>
                      <td><?php echo $get->material_name; ?></td>
                    </tr>
                  </table>
                </div>
                <div class="col-md-6">
                  <?php
                  $qty = $get->qty;
                  $uom = $get->uom_plt;
                  $req_plt = $qty/$uom;
                  ?>
                  <table class="table">
                    <tr>
                      <th>Requested Qty.</th>
                      <td><?php echo number_format($get->qty, 3, ",", ",")." ".$get->uom; ?></td>
                    </tr>
                    <tr>
                      <th>Qty / Plt</th>
                      <td><?php echo number_format($get->uom_plt, 3, ",", ",") ?></td>
                    </tr>
                    <tr>
                      <th>Required Pallet</th>
                      <td><?php echo $req_plt; ?></td>
                    </tr>
                    <tr>
                      <th>RDD</th>
                      <td><?php
                       $date = date_create($get->requested_delivery_date);
                       $df = date_format($date, 'D, d M Y');
                       echo $df;
                      ?></td>
                    </tr>
                  </table>
                </div>
              </div><hr>
              <div class="row">
                <form action="<?php echo base_url().'slot/save_schedule'; ?>" method="post" enctype="multipart/form-data">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Delivery Date</label>
                      <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="date" class="form-control pull-right dapick" id="datepicker" required="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Delivery Time</label>
                      <select name="time_slot" id="time-slot" class="form-control" required="">
                        <option value="" default selected="">--SELECT--</option>
                        <?php
                        foreach($time_slot->result() as $time){
                          ?>
                        <option value="<?php echo $time->id; ?>"><?php echo $time->start_time; ?> - <?php echo $time->end_time; ?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label id="title-truck">Truck No</label>
                      <input type="text" class="form-control" name="truck_no" id="truck-no" onchange="getTruckDetail(this.value, $('.dapick').val())" required="">
                      <p class="help-block">Write truck number to check slot availability.</p>
                    </div>
                    <div class="area-slot_ava"></div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Invoice File</label>
                      <input type="file" name="inv_file" class="form-control" required="">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <input type="hidden" name="id_schedule" value="<?php echo $id_schedule; ?>">
                    <input type="hidden" name="slot_usage" value="<?php echo $req_plt; ?>">
                    <input type="hidden" name="po_number" value="<?php echo $get->po_number; ?>">
                    <button class="btn btn-primary">SUBMIT</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>