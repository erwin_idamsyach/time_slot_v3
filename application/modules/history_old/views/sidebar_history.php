<?php if($this->session->userdata('sess_role_no') != '' ){ ?> 
              <li class="nav-item dropdown">
                <a class="nav-link" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?php $data_history = history($this->session->userdata('sess_role_no'), $this->session->userdata('sess_vendor_code'),10, $this->session->userdata('sess_id')); 
                    
                    $data_count = history_count($this->session->userdata('sess_id'));
                  ?>
                  <i class="fas fa-bell" style="font-size:18px"></i>
                  <span class="notification"><?php echo $data_count; ?></span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  
                  <?php 
                  if(date('Y-m-d') > "2019-08-15"){
                    foreach ($data_history as $get) {
                        $update = '';
                        $history = '' ;
                        $table_join = $get->table_join;
                        $id_join = $get->id_join;
                        $select_join = $get->select_join;
                        
                          if($get->action == "Arrived" || $get->action == "Received"){
                            $update = $get->value." ".$get->value2;
                          }
                          else if(!empty($get->value) && !empty($get->value2) ){
                            $update = $get->value." => ".$get->value2;
                          }
                          $history = "# &nbsp<b style='color:rgb(0, 44, 114)'>".Date('d-m-Y',strtotime($get->date))." : ".$get->time."</b>, &nbsp"."<a href='#' style='color:green;font-weight:600'>".$get->username."</a>"."&nbsp;<b style='color:rgb(234, 131, 4)'>".$get->action."</b>&nbsp;<b>".$get->description."<b style='color:rgb(234, 131, 4)'>&nbsp;".$update."</b>"."</b>";
                        ?>
                      
                      <b class="dropdown-item" href="#"><?php echo $history ?></b>
                      <?php } ?>
                  <?php 
                }else{
                    foreach ($data_history as $get) {
                    $history = '' ;
                    $table_join = $get->table_join;
                    $id_join = $get->id_join;
                    $select_join = $get->select_join;
                   
                    if($get->action == 'Arrive' || $get->action == 'Received'){
                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->username."</a>"."</b>"."&nbsp;"."&nbsp; ".$get->value.'&nbsp;<b style="color:orange">'.$get->action."</b>&nbsp;".$get->description.'&nbsp'.$get->value2."</b>";
                    }
                    elseif($get->action == 'Update' || $get->action == 'Take Out' || $get->action == 'Take' ){
                            $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->username."</a>"."</b>"."&nbsp;".$get->action."&nbsp;".$get->description."&nbsp; <b style='color:blue'>".$get->value.'&nbsp;'.$get->value2."</b>";        
                    }
                    else{
                      
                      $history = "# ".Date('d-m-Y',strtotime($get->date))." : ".$get->time." , <b style='color:blue'>"."<a href='#' style='color:blue'>".$get->username."</a>"."</b>"."&nbsp;".$get->action." ".$get->description;
                    }

                    ?>
                  <b class="dropdown-item" href="#"><?php echo $history ?></b>
                  <?php 
                }
              }
              ?>
                 
                  <hr style="padding:0px;margin:0px">
                  <a class="dropdown-item" href="<?php echo base_url() ?>history?year=<?php echo date('Y');?>&week=<?php echo date('W'); ?>" style="color:blue">Click Here to See All History</a>
                </div>
              </li>
            <?php } ?>