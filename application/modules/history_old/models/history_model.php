<?php

class history_model extends CI_Model{

	public function get_all_history($role, $id,$week, $year){
	
	$dto = new DateTime();
  	$dto->setISODate($year, $week);
  	$week_start = $dto->format('Y-m-d');
  	$dto->modify('+6 days');
  	$week_end = $dto->format('Y-m-d');
  	$vendor_code = $this->session->userdata('sess_vendor_code');
	$data = '';
		if($role == 1 || $role == 2){
			$data = $this->db->query("SELECT a.*, b.username as user FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE  a.date BETWEEN '$week_start' AND '$week_end'  ORDER BY a.id DESC ");	
		}else if($role == 3){
			$data = $this->db->query("SELECT a.*, b.username as user FROM tb_history as a LEFT JOIN ms_user as b ON a.by_who = b.id where a.vendor_code = '$vendor_code' AND a.date BETWEEN '$week_start' AND '$week_end' ORDER BY a.id DESC");
		}else if($role == 4){
			$data = $this->db->query("SELECT a.*, b.username as user FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE table_join != 'ms_user' ORDER BY a.id DESC");
		}
		
	return $data;	
	}
}

?>