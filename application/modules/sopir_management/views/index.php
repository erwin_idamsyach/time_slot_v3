<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i style="font-size:30px" class="fa fa-id-card"></i>
              </div>
            <h4 class="card-title">Sopir Management</h4>
            </div>
            <div class="card-body ">
              <div class="row">
                          <div class="col">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalSupir">
                              Add DRIVER
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="ModalSupir" tabindex="-1" role="dialog" aria-labelledby="data_supir" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="data_supir">ADD DRIVER</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form action="sopir_management/add_supir" method="POST" enctype="multipart/form-data" id="form_supir">
                                      <div class="row">
                                        <div class="col" hidden>
                                          <div class="form-group">
                                            <label>Driver ID</label>
                                            <input disabled required type="text" class="form-control" name="driver_id" >
                                          </div>
                                        </div>
                                        <div class="col-4">
                                          <div class="form-group">
                                            <label>Date</label>
                                            <input type="date" style="background-color:white" readonly="on" class="form-control" name="date" value="<?php echo date('Y-m-d') ?>" >
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col">
                                          <div class="form-group">
                                            <label>SIM</label>
                                            <input required="true" type="text" class="form-control" name="sim" >
                                          </div>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label>SIM EXP</label>
                                            <input required="true" type="date" class="form-control" name="sim_exp" >
                                          </div>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col">
                                          <div class="form-group">
                                            <label>Driver Name</label>
                                            <input required type="text" class="form-control" name="driver_name" >
                                          </div>
                                        </div>

                                        
                                      </div>

                                      <div class="row">
                                        <div class="col">
                                          <?php if(check_sub_menu(42)){ ?>
                                            <label for="vendor_code">Vendor Code</label>
                                            <select required class="form-control" name="vendor_code" >
                                              <option value="">Select Vendor</option>
                                              <?php foreach ($data_vendor as $get) {
                                                ?>
                                                <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code." - ".$get->vendor_name; ?></option>
                                                <?php
                                              } ?>
                                            </select>
                                          <?php }else{ ?>
                                            <div class="form-group">
                                            <label>Vendor Code</label>
                                            <input required type="text" value="<?php echo $this->session->userdata('sess_vendor_code'); ?>" readonly style="background-color:white" class="form-control" name="vendor_code" >
                                          </div>
                                          <?php } ?>
                                        </div>
                                      </div>
                                      <br>
                                    <div class="row">
                                        <div class="col" id="modal_upload_foto">
                                          <label id="status0" style="color:red; display:none;">* Muka tidak terdeteksi, silahkan coba lagi</label>
                                          <label>Upload Foto</label><br>
                                            <input required type="file" id="upload_foto_supir" class="btn" name="foto_supir" >
                                        </div>

                                        <div class="col" style="display: none" id="loading">
                                          <label>Checking Foto</label><br>
                                          <img src="<?php echo base_url() ?>assets/images/Loading.gif" width="25" height="25">
                                        </div>
                                        
                                        <div class="col" id="modal_picture_foto" style="display: none">
                                          <label id="status1" style="color:green; display:none">* Muka terdeteksi</label>
                                          <br>
                                           <img src="" id="picture" width="250" height="250">
                                        </div>

                                        <div class="col" style="display: none" id="modal_upload_cancel">
                                          <label>Cancel</label><br>
                                          <button type="button" onclick="cancel_upload()" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                        </div>
                                      </div>
                                    
                                    <button type="submit" id="save_change" class="btn btn-primary">Save changes</button>

                                    </form>
                                    
                                  </div>
                                  
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <div class="table-responsive">
                              <table class="table table-striped table-bordered table-hover tables" id="tables2" >
                            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
                                  <tr>
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Vendor Code</th>
                                    <!-- <th style="text-align:center">Driver ID</th> -->
                                    <th style="text-align:center">SIM</th>
                                    <th style="text-align:center">Exp SIM</th>
                                    <th style="text-align:center">Driver Name</th>
                                    <th style="text-align:center">Foto</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                  $no = 1;
                                  foreach ($data_driver as $get) {
                                    
                                  ?>
                                  <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $get->vendor_code; ?></td>
                                    
                                    <td><?php echo $get->sim; ?></td>
                                    <td><?php echo $get->exp_sim; ?></td>
                                    <td><?php echo $get->driver_name; ?></td>
                                    <?php 
                                    $foto = base_url()."assets/upload/foto/driver/".$get->foto;
                                    if(@getimagesize($foto)){
                                       
                                    }else{
                                      $foto = base_url()."assets/images/dummy-profile.png";
                                    }
                                    ?>
                                    <td style="text-align:center"><img width="50" height="50" src="<?php echo $foto; ?>" alt="Driver Foto"></td>
                                    <td style="text-align:center">
                                      <button style="font-size:15px" class="btn btn-just-icon btn-round btn-info btn-sm" onclick="openModalDriver(<?php echo $get->id; ?>, 'view')"><i class="fa fa-eye"></i></button>
                                      <button style="font-size:15px" onclick="openModalDriver(<?php echo $get->id; ?>, 'Edit')" class="btn btn-just-icon btn-round btn-warning btn-sm"><i class="fa fa-edit"></i></button>
                                      <button style="font-size:15px" onclick="deleteDriver(<?php echo $get->id; ?>)" class="btn btn-just-icon btn-round btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </td>
                                  </tr>
                                <?php } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Button trigger modal -->
<button type="button" hidden class="btn btn-primary" data-toggle="modal" id="modalDriverBtn" data-target="#modalDriver">
</button>

<!-- Modal -->
<div class="modal fade" id="modalDriver" tabindex="-1" role="dialog" aria-labelledby="ModalDriverTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalDriverTitle">View Driver</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="sopir_management/edit_supir" method="POST" enctype="multipart/form-data" id="form_supir">
        <div class="row">
          <div class="col" hidden>
            <div class="form-group">
              <label>Driver ID</label>
              <input type="hidden" id="id" name="id">
              <input required disabled type="text" class="form-control" name="driver_id" id="driver_id">
            </div>
          </div>
          <div class="col-6">
            <div class="form-group">
              <label id="labelDate">Date</label>
              <input type="text" style="background-color:white" readonly class="form-control" name="date" value="<?php echo date('Y-m-d') ?>" id="date_view">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label>SIM</label>
              <input required="true" type="text" class="form-control" name="sim" id="sim">
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <label>SIM EXP</label>
              <input required="true" type="date" class="form-control" name="sim_exp" id="sim_exp">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col">
            <div class="form-group">
              <label>Driver Name</label>
              <input required type="text" class="form-control" name="driver_name" id="driver_name">
            </div>
          </div>

          
        </div>

        <div class="row">
          <div class="col">
              <div class="form-group">
              <label>Vendor Code</label>
              <input required type="text" value="" readonly style="background-color:white" class="form-control" name="vendor_code" id="vendor_code">
            </div>
           
          </div>
        </div>
        <div class="row">
          <div class="col" id="edit_modal_upload_foto">
            <label id="edit_status0" style="color:red; display:none;">* Muka tidak terdeteksi, silahkan coba lagi</label>
            <label>Upload Foto</label><br>
              <input required type="file" id="edit_upload_foto_supir" class="btn" name="foto_supir" >
          </div>

          <div class="col" style="display: none" id="edit_loading">
            <label>Checking Foto</label><br>
            <img src="<?php echo base_url() ?>assets/images/Loading.gif" width="25" height="25">
          </div>
          
          <div class="col" id="edit_modal_picture_foto" style="display: none">
            <label id="edit_status1" style="color:green; display:none">* Muka terdeteksi</label>
            <br>
             <img src="" id="edit_picture" width="250" height="250">
          </div>
          
          <div class="col" style="display: none" id="edit_modal_upload_cancel">
            <label>Cancel</label><br>
            <button type="button" onclick="edit_cancel_upload()" class="btn btn-danger"><i class="fa fa-times"></i></button>
          </div>
        </div>
      <button type="submit" id="edit_save_change" class="btn btn-primary">Save changes</button>

      </form>
      
        

      </div>
      
    </div>
  </div>
</div>