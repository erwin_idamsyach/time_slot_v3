<script type="text/javascript">
	function openModalDriver(id, action){
		if(action == "view"){
			$('#modalDriverBtn').click();
			$.ajax({
				url: "<?php echo base_url(); ?>sopir_management/view_driver",
				data: {
					"id" : id
				},
				type: "GET",
				dataType:"JSON",
				success:function(e){
					$('#driver_id').val(e.driver_id);
					var getdate = new Date(e.created_at);
					if(getdate.getMonth() < 10){
						$month = "0"+getdate.getMonth();
					}else{
						$month = getdate.getMonth();
					}
					var date = getdate.getFullYear()+"-"+$month+"-"+getdate.getDate();
					$('#edit_btn').hide();
					$('#edit_save_change').hide();
					$('#upload_foto').hide();
					$('#foto_supir').show();
					$('#labelDate').hide();
					$('#date_view').hide();
					$('#sim').val(e.sim);
					$('#ModalDriverTitle').html('View Driver');
					$('#sim_exp').val(e.exp_sim);
					$('#driver_name').val(e.driver_name);
					$('#vendor_code').val(e.vendor_code);
					$('#foto_supir').attr("src","<?php echo base_url(); ?>assets/upload/foto/driver/"+e.foto);
				},error:function(e){
					alert("Something Error");
				}

			});
		}else{
			$('#modalDriverBtn').click();
			$.ajax({
				url: "<?php echo base_url(); ?>sopir_management/view_driver",
				data: {
					"id" : id
				},
				type: "GET",
				dataType:"JSON",
				success:function(e){
					$('#driver_id').val(e.driver_id);
					var getdate = new Date(e.created_at);
					if(getdate.getMonth() < 10){
						$month = "0"+getdate.getMonth();
					}else{
						$month = getdate.getMonth();
					}
					var date = getdate.getFullYear()+"-"+$month+"-"+getdate.getDate();
					$('#edit_btn').show();
					$('#edit_save_change').show();
					$('#id').val(e.id);
					$('#upload_foto').show();
					$('#labelDate').show();
					$('#date_view').show();
					$('#foto_supir').hide();
					$('#label_foto').html("Upload Foto");
					$('#sim').val(e.sim);
					$('#sim_exp').val(e.exp_sim);
					$('#ModalDriverTitle').html('Edit Driver');
					$('#driver_name').val(e.driver_name);
					$('#vendor_code').val(e.vendor_code);
					$('#foto_supir').attr("src","<?php echo base_url(); ?>assets/upload/foto/driver/"+e.foto);
				},error:function(e){
					alert("Something Error");
				}

			});
		}
	}

	$("#upload_foto_supir").change(function(){
        face_recognition(this);
	});

	$("#edit_upload_foto_supir").change(function(){
        edit_face_recognition(this);
	});

	var ajax = "";

	function cancel_upload(){
		ajax.abort();
    	$('#modal_upload_cancel').hide();
    	$('#status_foto').hide();
    	$('#modal_upload_foto').show();
    	$('#loading').hide();
    	$('#modal_picture_foto').hide();
    	$('#save_change').hide();
	}

	function edit_cancel_upload(){
		ajax.abort();
    	$('#edit_modal_upload_cancel').hide();
    	$('#edit_status_foto').hide();
    	$('#edit_modal_upload_foto').show();
    	$('#edit_loading').hide();
    	$('#edit_modal_picture_foto').hide();
    	$('#edit_save_change').hide();
	}

	function face_recognition(input){
	    $('#loading').show();
	    $('#status_foto').hide();
	    $('#modal_upload_foto').hide();
	    $('#modal_upload_cancel').show();
	    if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function (e) {
		       var fd = new FormData();
		       var files = $('#upload_foto_supir')[0].files[0];
		       fd.append('file', files);
		       ajax = $.ajax({
	           url: "sopir_management/face_recognition",
			   type: "POST",
			   data:  fd,
			   contentType: false,
			   cache: false,
			   processData:false,
			   success: function(data)
			   {
				console.log(data);
			   	if(data == ""){
					console.log("ITS HERE");
			   		$('#loading').hide();
			   		$('#modal_picture_foto').show();
		        	$('#picture').attr('src', e.target.result);
		        	$('#save_change').show();
					//$('#status0').css("display","block");
					$('#status1').show();
		        	$('#status1').text("FR API is down!");
			   	}else{
			   		data = $.parseJSON(data);
				   	if(data["face_detected"] == 1){
				   		$('#loading').hide();
				   		$('#modal_picture_foto').show();
			        	$('#picture').attr('src', e.target.result);
			        	$('#save_change').show();
			        	$('#status1').show();
				   	}else{
				   		$('#loading').hide();
				   		$('#modal_upload_foto').show();
				   		$('#status0').css("display","block");
				   		$('#status0').show();
				   		$('#status0').text("Muka tidak terdeteksi, mohon untuk mengganti foto!");
				   	}
			   	}
			   },error: function(data)
			   {
			   	console.log("Something Error "+data);
			   }
			});
		    	
		    }
		    reader.readAsDataURL(input.files[0]);
		}
    
	}

	function edit_face_recognition(input){
	    $('#edit_loading').show();
	    $('#edit_status_foto').hide();
	    $('#edit_modal_upload_foto').hide();
	    $('#edit_modal_upload_cancel').show();
	    if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function (e) {
		       var fd = new FormData();
		       var files = $('#edit_upload_foto_supir')[0].files[0];
		       fd.append('file', files);
		       ajax = $.ajax({
	           url: "sopir_management/face_recognition",
			   type: "POST",
			   data:  fd,
			   contentType: false,
			   cache: false,
			   processData:false,
			   success: function(data)
			   {
			   	console.log(data);
			   	if(data == ""){
					console.log("ITS HERE");
			   		$('#edit_loading').hide();
			   		$('#edit_modal_picture_foto').show();
		        	$('#edit_picture').attr('src', e.target.result);
		        	$('#edit_save_change').show();
		        	$('#edit_status1').show();
		        	$('#edit_status1').text("FR API is down!");
			   	}else{
			   		data = $.parseJSON(data);
				   	if(data["face_detected"] == 1){
				   		$('#edit_loading').hide();
				   		$('#edit_modal_picture_foto').show();
			        	$('#edit_picture').attr('src', e.target.result);
			        	$('#edit_save_change').show();
			        	$('#edit_status1').show();
				   	}else{
				   		$('#edit_loading').hide();
				   		$('#edit_modal_upload_foto').show();
				   		$('#edit_status0').css("display","block");
				   		$('#edit_status0').show();
				   		$('#edit_status0').text("Muka tidak terdeteksi, mohon untuk mengganti foto!");
				   	}
			   	}
			   },error: function(data)
			   {
			   	console.log("Something Error "+data);
			   }
			});
		    	
		    }
		    reader.readAsDataURL(input.files[0]);
		}
    
	}

	function deleteDriver(id){
		const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger'
			  },
			  buttonsStyling: false,
			})

			swalWithBootstrapButtons.fire({
			  title: 'Apakah Anda Yakin?',
			  text: "Kamu tidak dapat mengembalikan data tersebut!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, Hapus!',
			  cancelButtonText: 'No, cancel!',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {

			    swalWithBootstrapButtons.fire(
			      'Deleted!',
			      'Your file has been deleted.',
			      'success'
			    )
			    $.ajax({
	            method : "GET",
	            url: "<?php echo base_url(); ?>sopir_management/delete_driver",
				data: {
					"id" : id
				},
	            dataType : "JSON",
	            success: function(result){ 
	            }
            }); 
			window.location=("<?php echo base_url() ?>sopir_management");
			  } else if (
			    // Read more about handling dismissals
			    result.dismiss === Swal.DismissReason.cancel
			  ) {
			    swalWithBootstrapButtons.fire(
			      'Cancelled',
			      'Data tersebut tidak dihapus',
			      'error'
			    )
			  }
			})

	}

	$(".tables").DataTable({
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
		
	});
</script>