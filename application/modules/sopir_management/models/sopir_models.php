<?php

class sopir_models extends CI_Model{

	public function getTruckDetail($vendor_code=''){
		$data = $this->db->query("SELECT * FROM ms_truck AS a INNER JOIN ms_truck_type AS b On a.truck_type = b.id WHERE a.vendor_code LIKE '%$vendor_code%' ")->result();
		return $data;
	}

	public function getSupirDetail($vendor_code =''){
		$data = $this->db->query("SELECT * FROM ms_driver WHERE ms_driver.vendor_code LIKE '%$vendor_code%' ORDER BY id DESC")->result();
		return $data;
	}

	public function getVendorCode(){
		$data = $this->db->query("SELECT * FROM skin_master.ms_supplier")->result();
		return $data;
	}

	public function getDataDriver($id){
		$data = $this->db->query("SELECT * FROM ms_driver WHERE id ='$id' ")->row();
		return $data;
	}

	public function getDataTruck($id){
		$data = $this->db->query("SELECT * FROM ms_truck WHERE id ='$id' ")->row();
		return $data;
	}

	public function getTruckType(){
		$data = $this->db->query("SELECT * FROM ms_truck_type ")->result();
		return $data;
	}

	public function getDataTruckType($id){
		$data = $this->db->query("SELECT * FROM ms_truck_type WHERE id = '$id' ")->row();
		return $data;
	}
}

?>