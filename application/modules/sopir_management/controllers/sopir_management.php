<?php

class sopir_management extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		isLogin();
		$this->load->model('sopir_models');
		if(!check_sub_menu(27)){
			isRole();
		}
	}

	public function index(){
		if(check_sub_menu(42)){
			$data["data_truck"] = $this->sopir_models->getTruckDetail();
			$data["data_driver"] = $this->sopir_models->getSupirDetail();
			$data["data_vendor"] = $this->sopir_models->getVendorCode();
			$data["data_truck_type"] = $this->sopir_models->getTruckType();
		}else{
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$data["data_truck"] = $this->sopir_models->getTruckDetail($vendor_code);
			$data["data_truck_type"] = $this->sopir_models->getTruckType();
			$data["data_driver"] = $this->sopir_models->getSupirDetail($vendor_code);
		}
		getHTML('sopir_management/index', $data);
	}

	public function add_supir(){
		$config['allowed_types'] = 'jpg|jpeg|png|bmp|gif|JPG|PNG';
	  	$config['max_size'] = 0;
		$config['upload_path'] = 'assets/upload/foto/driver';
		// $receipt = $this->db->query("SELECT receipt FROM tb_delivery_detail WHERE id_schedule_group = '$id' ")->row()->receipt;

  		$config['file_name'] = "test";
  		$foto = '';
  		$this->load->library('upload', $config);
	  	if ($this->upload->do_upload('foto_supir')) {
	  		$data = $this->upload->data();
		  	$foto = $this->upload->data('file_name');

		}else{
		  	print_r($this->upload->display_errors());
		}

		$data = array(
			// "driver_id"		=> $this->input->post("driver_id"),
			"sim"			=> $this->input->post("sim"),
			"exp_sim"		=> $this->input->post("sim_exp"),
			"driver_name"	=> $this->input->post("driver_name"),
			"created_at"	=> date('Y-m-d H:i:s'),
			"created_by"	=> $this->session->userdata('sess_id'),
			"foto"			=> $foto,
			"vendor_code"	=> $this->input->post('vendor_code'),
		);
		$this->db->insert("ms_driver", $data);

		$get_last_id = $this->db->query("SELECT id FROM ms_driver ORDER BY id DESC")->row()->id;
		$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Add",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "ms_driver",
	  			"id_join"		=> $get_last_id,
	  			"value"			=> "",
	  			"value2"		=> "",
	  			"description"	=> "New Driver Name : ".$this->input->post("driver_name").", sim : ".$this->input->post("sim"),
	  			"select_join"	=> "id",
	  			"author"		=> 1,
	  			"vendor_code"	=> $this->input->post('vendor_code')
		 );
		$this->db->insert('tb_history', $history);
		
		redirect('sopir_management');
	}

	public function face_recognition(){
		$files = glob('assets/upload/foto/face_recognition/*');
		foreach($files as $file){
		  if(is_file($file))
		    unlink($file);
		}

		$get_last_id = $this->db->query("SELECT id FROM ms_driver ORDER BY id DESC")->row()->id;
		$foto = '';
	  	$config['allowed_types'] = 'jpg|jpeg|png|bmp';
	  	$config['max_size'] = 0;
		$config['upload_path'] = 'assets/upload/foto/face_recognition'; 
  		$config['file_name'] = $get_last_id + 1;
  		$this->load->library('upload', $config);
	  	if ($this->upload->do_upload('file')) {
	  		$data = $this->upload->data();
		  	$foto = $this->upload->data('file_name');
			//echo $foto;
			$this->send_API_face_recognition($foto);
		}else{
		  	print_r($this->upload->display_errors());
		}

	}

	public function send_API_face_recognition($foto){
		$data = array(
		    'file' =>  curl_file_create(realpath("./assets/upload/foto/face_recognition/".$foto))
		);
		$headers = array("Content-Type:multipart/form-data");
		$url = 'http://35.201.254.197:8999/api/Alfabeta-check-face';
		$ch = curl_init($url);
		$postString = http_build_query($data);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		curl_close($ch);
		echo $response;
	}

	public function edit_supir(){
		$id = $this->input->post("id");
		$config['allowed_types'] = 'jpg|jpeg|png|bmp';
	  	$config['max_size'] = 0;
		$config['upload_path'] = 'assets/upload/foto/driver';
		// $receipt = $this->db->query("SELECT receipt FROM tb_delivery_detail WHERE id_schedule_group = '$id' ")->row()->receipt;

  		$config['file_name'] = "test";
  		$foto = '';
  		$this->load->library('upload', $config);
	  	if ($this->upload->do_upload('foto_supir')) {
	  		$data = $this->upload->data();
		  	$foto = $this->upload->data('file_name');

		}else{
		  	print_r($this->upload->display_errors());
		}

		$data = array(
			// "driver_id"		=> $this->input->post("driver_id"),
			"sim"			=> $this->input->post("sim"),
			"exp_sim"		=> $this->input->post("sim_exp"),
			"driver_name"	=> $this->input->post("driver_name"),
			"update_at"	=> date('Y-m-d H:i:s'),
			"update_by"	=> $this->session->userdata('sess_id'),
			"vendor_code"	=> $this->input->post('vendor_code'),
		);
		
		if($foto != ""){
			$data['foto'] =  $foto;
		}
		$this->db->where("id",$id);
		$this->db->update("ms_driver", $data);

		$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Update",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "ms_driver",
	  			"id_join"		=> $id,
	  			"value"			=> "",
	  			"value2"		=> "",
	  			"description"	=> "Driver Name : ".$this->input->post("driver_name").", sim/exp : ".$this->input->post("sim")." / ".$this->input->post("sim_exp"),
	  			"select_join"	=> "id",
	  			"author"		=> 1,
	  			"vendor_code"	=> $this->input->post('vendor_code')
		);
		$this->db->insert('tb_history', $history);
		redirect('sopir_management');
	}

	public function view_driver(){
		$id = $this->input->get('id');
		$data_driver = $this->sopir_models->getDataDriver($id);
		echo json_encode($data_driver);
	}

	public function view_truck(){
		$id = $this->input->get('id');
		$data_truck = $this->sopir_models->getDataTruck($id);
		echo json_encode($data_truck);
	}

	public function get_truck_type(){
		$id = $this->input->get('id');
		$data_truck_type = $this->sopir_models->getDataTruckType($id);
		echo json_encode($data_truck_type);
	}

	public function delete_driver(){
		$id = $this->input->get("id");
		$delete = $this->db->query("DELETE FROM ms_driver WHERE id = '$id' ");
		if($delete){
			return true;
		}
	}
}