<style type="text/css">
.nav-pills .nav-item .nav-link {
    line-height: 24px;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 500;
    min-width: 100px;
    text-align: center;
    color: #555;
    transition: all .3s;
    border-radius: 30px;
    padding: 0px 5px;
}

.table thead tr th {
    /* font-size: 1.063rem; */
    font-size: 14px;
}


@media screen and (min-width: 400px){
  
}

@media screen and (min-width: 800px){
  .action-button{
    position: absolute;
    display: inline-block;
    z-index: 9999;
  }

  .nav-pills{
    margin-left:-14px;
  }
}

td{
  white-space: nowrap;
}
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i style="font-size:30px" class="fas fa-truck"></i>
              </div>
            <h4 class="card-title">Truck Management</h4>
            </div>
            <div class="card-body ">
              <div class="row">
                <div class="col-12">
                        <div class="row">
                          <div class="col">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#ModalTruck">
                              Add Truck
                            </button>
                            
                            <!-- Button trigger modal -->
                            <button type="button" <?php if(!check_sub_menu(34)){echo "hidden";} ?> class="btn btn-warning btn-sm" data-toggle="modal" data-target="#ModalTruckType">
                              Add Truck Type
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="ModalTruckType" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Add Truck Type</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form class="form_truck_type" action="truck_management/add_truck_type" method="POST" enctype="multipart/form-data" id="form_supir">
                                    <div class="row">
                                      <div class="col">
                                        <div class="form-group">
                                          <label>Truck Type</label>
                                          <input required id="truck_type" type="text" class="form-control" name="truck_type">
                                        </div>
                                      </div>
                                      <!-- <div class="col">
                                          <select id="capacity" required class="form-control" name="capacity">
                                            <option value="">Select Capacity Truck</option>
                                            <option value="4">4</option>
                                            <option value="8">8</option>
                                            <option value="16">16</option>
                                            <option value="32">32</option>
                                          </select>
                                        </div> -->
                                    </div>
                                    <button id="btn_truck_type" type="button" onclick="validate_truck_type()" class="btn btn-primary">Save changes</button>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            

                            <!-- Modal -->
                            <div class="modal fade" id="ModalTruck" tabindex="-1" role="dialog" aria-labelledby="data_supir" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="data_supir">ADD TRUCK</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <form class="form_add_truck" action="truck_management/add_truck" method="POST" enctype="multipart/form-data" id="form_supir">
                                    <div class="row">
                                      <div class="col" hidden>
                                        <div class="form-group">
                                          <label>Truck ID</label>
                                          <input id="truck_id" type="text" disabled class="form-control" name="truck_id">
                                        </div>
                                      </div>
                                      <div class="col-6">
                                        <div class="form-group">
                                          <label>Date</label>
                            
                                          <input type="date" style="background-color:white" readonly class="form-control" name="date" value="<?php echo date('Y-m-d') ?>">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-6">
                                        
                                            <select id="select_truck" required class="form-control" name="truck_type" >
                                              <option value="">Select Truck Type</option>
                                              <?php foreach ($data_truck_type as $get) {
                                                ?>
                                                <option value="<?php echo $get->id; ?>"><?php echo $get->truck_type; ?></option>
                                                <?php
                                              } ?>
                                            </select>
                                      </div>
                                      <div class="col">
                                        <div class="form-group">
                                          <label>Police Number</label>
                                          <input id="police_number" type="text" class="form-control" name="license_plat">
                                        </div>
                                      </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                          <?php if($this->session->userdata('sess_vendor_code') == null){ ?>
                                           
                                            <select id="select_vendor" required class="form-control" name="vendor_code" >
                                              <option value="">Select Vendor</option>
                                              <?php foreach ($data_vendor as $get) {
                                                ?>
                                                <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code." - ".$get->vendor_name; ?></option>
                                                <?php
                                              } ?>
                                            </select>
                                          <?php }else{ ?>
                                            <div class="form-group">
                                            <label>Vendor Code</label>
                                            <input required type="text" value="<?php echo $this->session->userdata('sess_vendor_code'); ?>" readonly style="background-color:white" class="form-control" name="vendor_code">
                                          </div>
                                          <?php } ?>
                                        </div>
                                        <div class="col">
                                          <div class="form-group">
                                            <label>No Kir</label>
                                            <input id="add_no_kir" type="text" class="form-control" name="no_kir">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col">
                                          <div class="form-group">
                                            <select required="true" name="status_flag" class="form-control">
                                              <option value="">-- Select Status Flag --</option>
                                              <option value="1">Hijau</option>
                                              <option value="2">Kuning</option>
                                              <option value="3">Merah</option>
                                            </select>
                                          </div>
                                        </div>
                                      </div>
                                      <br>
                                    <button id="btn_add_truck" type="submit" class="btn btn-primary">Save changes</button>
                                  </form>
                                  </div>
                                 
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col">
                            <h4 style="position: absolute;margin-top:10px"><u>Data Truck</u></h4>
                            <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover tables" id="tables2" >
                            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
                                  <tr>
                                    <th style="text-align:center">No</th>
                                    <th style="text-align:center">Vendor Code</th>
                                    <!-- <th style="text-align:center">Truck ID</th> -->
                                    <th style="text-align:center">Truck Type</th>
                                    <th style="text-align:center">Police Number</th>
                                    <th style="text-align:center">Safety Color</th>
                                    <th style="text-align:center">No Kir</th>
                                    <th style="text-align:center">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php 
                                  $no = 1;
                                  foreach ($data_truck as $get) {
                                   ?>
                                  <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $get->vendor_code; ?></td>

                                    <td><?php echo $get->truck_type; ?></td>
                                    <td><?php echo $get->police_number; ?></td>
                                    <td><?php echo $get->status_flag; ?></td>
                                    <td><?php echo $get->no_kir; ?></td>
                                    <td style="text-align:center">
                                       <button style="font-size:15px" class="btn btn-just-icon btn-round btn-info btn-sm" onclick="openModalTruck(<?php echo $get->id; ?>, 'view')"><i class="fa fa-eye"></i></button>
                                        <button style="font-size:15px" onclick="openModalTruck(<?php echo $get->id; ?>, 'edit')" class="btn btn-just-icon btn-round btn-warning btn-sm"><i class="fa fa-edit"></i></button>
                                        <button style="font-size:15px" onclick="deleteTruck(<?php echo $get->id; ?>)" class="btn btn-just-icon btn-round btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </td>
                                  </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        
                        <div class="row" <?php if(!check_sub_menu(34)){echo "hidden";} ?>>
                          <div class="col">
                          <h4 style="position: absolute;margin-top:10px"><u>Data Truck Type</u></h4>
                          <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover tables" id="tables2" >
                            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
                              <tr>
                                <th>No</th>
                                <th>Truck Type</th>
                                <!-- <th>Capacity</th> -->
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $no = 1;
                              foreach ($data_truck_type as $get) {
                                
                               ?>
                              <tr>
                                <td style="text-align:center"><?php echo $no++; ?></td>
                                <td><?php echo $get->truck_type; ?></td>
                                <!-- <td><?php echo $get->capacity; ?></td> -->
                                <td style="text-align:center">

                                  <button style="font-size:15px" onclick="openModalTruckType(<?php echo $get->id; ?>)" class="btn btn-warning btn-just-icon btn-round btn-sm"><i class="fa fa-edit"></i></button>
                                  <button style="font-size:15px" onclick="deleteTruckType(<?php echo $get->id; ?>)" class="btn btn-danger btn-just-icon btn-round btn-sm"><i class="fa fa-trash"></i></button>
                                </td>
                              </tr>
                            <?php } ?>
                            </tbody>
                          </table>
                          </div>
                              
                          </div>
                        </div>

                      </div>
               
            </div>
          </div>
        </div>
       </div>
      </div>
    </div>
  </div>
</div>



  <!-- Button trigger modal -->
  <button type="button" style="display:none" id="modalTruckBtn" class="btn btn-primary" data-toggle="modal" data-target="#OpenModalTruck">
    truck
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="OpenModalTruck" tabindex="-1" role="dialog" aria-labelledby="modalTruckTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalTruckTitle">View Truck</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="truck_management/edit_truck" method="POST" enctype="multipart/form-data" id="form_supir">
            <div class="row">
              <div class="col" hidden>
                <div class="form-group">
                  <label>Truck ID</label>
                  
                  <input type="text" disabled class="form-control" id="" name="truck_id" >
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label>Date</label>
                  <input type="hidden" value="" id="edit_truck_id" name="id">
                  <input type="date" style="background-color:white" id="DateTruck" readonly class="form-control" name="date" value="">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-6">
                <select id="truck_type_view" required class="form-control" name="truck_type" >
                  <option value="">Select Truck Type</option>
                  <?php foreach ($data_truck_type as $get) {
                    ?>
                    <option value="<?php echo $get->id; ?>"><?php echo $get->truck_type; ?></option>
                    <?php
                  } ?>
                </select>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>Police Number</label>
                  <input type="text" class="form-control" name="license_plat" id="license_plat">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <?php if(check_sub_menu(26)){ ?>
                 
                  <select id="view_select_vendor" required class="form-control" name="vendor_code" >
                    <option value="">Select Vendor</option>
                    <?php foreach ($data_vendor as $get) {
                      ?>
                      <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code." - ".$get->vendor_name; ?></option>
                      <?php
                    } ?>
                  </select>
                <?php }else{ ?>
                  <div class="form-group">
                  <label>Vendor Code</label>
                  <input required type="text" value="<?php echo $this->session->userdata('sess_vendor_code'); ?>" readonly style="background-color:white" class="form-control" name="vendor_code">
                </div>
                <?php } ?>
              </div>
              <div class="col">
                <div class="form-group">
                  <label>No Kir</label>
                  <input id="view_no_kir" type="text" class="form-control" name="no_kir">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col">
                <div class="form-group">
                  <select required="true" id="view_status_flag" name="status_flag" class="form-control">
                      <option value="">-- Select Status Flag --</option>
                      <option value="1">Hijau</option>
                      <option value="2">Kuning</option>
                      <option value="3">Merah</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <button type="submit" id="btnEditTruck" style="display: none" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>

  <!-- Button trigger modal -->
  <button type="button" hidden class="btn btn-primary" id="ModalTruckTypeBtn" data-toggle="modal" data-target="#ModalTruckTypeEdit">
    Modal Truck Type
  </button>
  
  <!-- Modal -->
  <div class="modal fade" id="ModalTruckTypeEdit" tabindex="-1" role="dialog" aria-labelledby="ModalTruckTypeTitleEdit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="ModalTruckTypeTitleEdit">Edit Truck Type</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="truck_management/edit_truck_type" method="POST" enctype="multipart/form-data" id="form_supir">
          <div class="row">
            <div class="col">
              <div class="form-group">
                <label>Truck Type</label>
                <input type="hidden" id="id_truck_type" name="id_truck_type">
                <input type="text" id="truck_type_edit" class="form-control" name="truck_type">
              </div>
            </div>
            <!-- <div class="col">
              <select id="truck_capacity" class="form-control" name="capacity">
                <option value="">Select Capacity Truck</option>
                <option value="4">4</option>
                <option value="8">8</option>
                <option value="16">16</option>
                <option value="32">32</option>
              </select>
            </div> -->
          </div>
          <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
        </div>
      </div>
    </div>
  </div>