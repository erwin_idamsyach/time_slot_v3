<script type="text/javascript">
	function add_new(){
		$('.action').toggle();
	}


	function openModalTruck(id, action){
		if(action == "view"){
			$('#modalTruckBtn').click();
			$.ajax({
				url: "<?php echo base_url(); ?>truck_management/view_truck",
				data: {
					"id" : id
				},
				type: "GET",
				dataType:"JSON",
				success:function(e){
				
					var getdate = new Date(e.created_at);
					if(getdate.getMonth() < 10){
						$month = "0"+getdate.getMonth();
					}else{
						$month = getdate.getMonth();
					}
					var date = getdate.getFullYear()+"-"+$month+"-"+getdate.getDate();
					
					$('#truck_id').val(e.id_truck);
					$('#DateTruck').val(date);
					$('#truck_type_view').val(e.truck_type);
					$('#license_plat').val(e.police_number);
					$('#view_select_vendor').val(e.vendor_code);
					$('#btnEditTruck').hide();
					$('#view_no_kir').val(e.no_kir);
					$('#view_status_flag').val(e.status_flag);
					$('#modalTruckTitle').html('View Truck');
					console.log(e);
					
				},error:function(e){
					alert("Something Error");
				}

			});
		}else{
			$('#modalTruckBtn').click();
			$.ajax({
				url: "<?php echo base_url(); ?>truck_management/view_truck",
				data: {
					"id" : id
				},
				type: "GET",
				dataType:"JSON",
				success:function(e){
					var getdate = new Date(e.created_at);
					if(getdate.getMonth() < 10){
						$month = "0"+getdate.getMonth();
					}else{
						$month = getdate.getMonth();
					}
					var date = getdate.getFullYear()+"-"+$month+"-"+getdate.getDate();
					
					$('#edit_truck_id').val(e.id);
					$('#DateTruck').val(date);
					$('#truck_type').val(e.truck_type);
					$('#license_plat').val(e.police_number);
					$('#view_select_vendor').val(e.vendor_code);
					$('#view_no_kir').val(e.no_kir);
					$('#view_status_flag').val(e.status_flag);
					$('#btnEditTruck').show();
					$('#modalTruckTitle').html('Edit Truck');
					console.log(e);
					
				},error:function(e){
					alert("Something Error");
				}

			});
		}
	}

	function openModalTruckType(id){
			$('#ModalTruckTypeBtn').click();
			$.ajax({
				url: "<?php echo base_url() ?>truck_management/get_truck_type",
				data:{
					"id" : id
				},
				method:"GET",
				dataType:"JSON",
				success:function(e){
					$('#truck_type_edit').val(e.truck_type);
					$('#id_truck_type').val(e.id);
					$('#truck_capacity').val(e.capacity);
					console.log(e);
				},error:function(e){
					alert("Something Error");
				}
			});
	}

	function deleteTruck(id){
		const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger'
			  },
			  buttonsStyling: false,
			})

			swalWithBootstrapButtons.fire({
			  title: 'Apakah Anda Yakin?',
			  text: "Kamu tidak dapat mengembalikan data tersebut!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, Hapus!',
			  cancelButtonText: 'No, cancel!',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {

			    swalWithBootstrapButtons.fire(
			      'Deleted!',
			      'Your file has been deleted.',
			      'success'
			    )
			    $.ajax({
	            method : "GET",
	            url: "<?php echo base_url(); ?>truck_management/delete_truck",
				data: {
					"id" : id
				},
	            dataType : "JSON",
	            success: function(result){ 
	            }
            }); 
			window.location=("<?php echo base_url() ?>truck_management");
			  } else if (
			    // Read more about handling dismissals
			    result.dismiss === Swal.DismissReason.cancel
			  ) {
			    swalWithBootstrapButtons.fire(
			      'Cancelled',
			      'Data tersebut tidak dihapus',
			      'error'
			    )
			  }
			});
	}

	function validate_truck_type(){
		var select_truck_type =  $('#select_truck').val();
		var police_number =  $('#police_number').val();
		var select_vendor = $('#select_vendor').val();
		var isValid = true;
		if(truck_type == '' && capacity == ''){
			isValid = false;
		}else{
			$('#btn_add_truck').attr('disabled', true);
			loading();
			$('.form_add_truck').submit();
		}
	}

	function validate_truck_type(){
		var truck_type =  $('#truck_type').val();
		var capacity =  $('#capacity').val();
		var isValid = true;
		if(truck_type == '' && capacity == ''){
			isValid = false;
		}else{
			$('#btn_truck_type').attr('disabled', true);
			loading();
			$('.form_truck_type').submit();
		}
	}

	function deleteTruckType(id){
		
		const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger'
			  },
			  buttonsStyling: false,
			})

			swalWithBootstrapButtons.fire({
			  title: 'Apakah Anda Yakin?',
			  text: "Kamu tidak dapat mengembalikan data tersebut!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, Hapus!',
			  cancelButtonText: 'No, cancel!',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {

			    swalWithBootstrapButtons.fire(
			      'Deleted!',
			      'Your file has been deleted.',
			      'success'
			    )
			    $.ajax({
	            method : "GET",
	            url: "<?php echo base_url(); ?>truck_management/delete_truck_type",
				data: {
					"id" : id
				},
	            dataType : "JSON",
	            success: function(result){ 
	            }
            }); 
			window.location=("<?php echo base_url() ?>truck_management");
			  } else if (
			    // Read more about handling dismissals
			    result.dismiss === Swal.DismissReason.cancel
			  ) {
			    swalWithBootstrapButtons.fire(
			      'Cancelled',
			      'Data tersebut tidak dihapus',
			      'error'
			    )
			  }
			});
	}

	$(".tables").DataTable({
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
		
	});
</script>