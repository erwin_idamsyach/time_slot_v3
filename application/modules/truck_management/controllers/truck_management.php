<?php

class truck_management extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		isLogin();
		$this->load->model('truck_models');
		if(!check_sub_menu(31)){
			isRole();
		}
	}

	public function index(){
		if($this->session->userdata('sess_role_no') == 3){
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$data["data_truck"] = $this->truck_models->getTruckDetail($vendor_code);
			$data["data_truck_type"] = $this->truck_models->getTruckType();
		}
		else if(check_sub_menu(31)){
			$data["data_truck"] = $this->truck_models->getTruckDetail();
			$data["data_vendor"] = $this->truck_models->getVendorCode();
			$data["data_truck_type"] = $this->truck_models->getTruckType();
		}
		
		getHTML('truck_management/index', $data);
	}

	public function add_truck(){
		
		$data = array(
			// "id_truck"		=> $this->input->post('truck_id'), 
			"truck_type"	=> $this->input->post('truck_type'),
			"police_number"	=> $this->input->post('license_plat'),
			"vendor_code"	=> $this->input->post('vendor_code'),
			"status_flag"	=> $this->input->post('status_flag'),
			"no_kir"		=> $this->input->post('no_kir'),
			"vendor_code"	=> $this->input->post('vendor_code'),
			"created_at"	=> $this->input->post('date'),
			"created_by"	=> $this->session->userdata('sess_id')
		);
		$this->db->insert('ms_truck', $data);
		redirect('truck_management');
	}

	public function edit_truck(){
		$id = $this->input->post('id');
		$data = array(
			// "id_truck"		=> $this->input->post('truck_id'), 
			"truck_type"	=> $this->input->post('truck_type'),
			"police_number"	=> $this->input->post('license_plat'),
			"vendor_code"	=> $this->input->post('vendor_code'),
			"status_flag"	=> $this->input->post('status_flag'),
			"no_kir"		=> $this->input->post('no_kir'),
			"vendor_code"	=> $this->input->post('vendor_code'),
			"created_at"	=> $this->input->post('date'),
			"created_by"	=> $this->session->userdata('sess_id')
		);
		$this->db->where('id', $id);
		$this->db->update('ms_truck', $data);
		redirect('truck_management');
	}

	public function edit_truck_type(){
		$id = $this->input->post('id_truck_type');
		$capacity = $this->input->post('capacity');
		$data = array(
			// "id_truck"		=> $this->input->post('truck_id'), 
			"truck_type"	=> $this->input->post('truck_type'),
			"capacity"		=> $this->input->post('capacity')
		);
		$this->db->where('id', $id);
		$this->db->update('ms_truck_type', $data);
		redirect('truck_management');
	}

	public function view_truck(){
		$id = $this->input->get('id');
		$data_truck = $this->truck_models->getDataTruck($id);
		echo json_encode($data_truck);
	}

	public function get_truck_type(){
		$id = $this->input->get('id');
		$data_truck_type = $this->truck_models->getDataTruckType($id);
		echo json_encode($data_truck_type);
	}

	public function delete_truck(){
		$id = $this->input->get("id");
		$delete = $this->db->query("DELETE FROM ms_truck WHERE id = '$id' ");
		if($delete){
			return true;
		}
	}

	public function delete_truck_type(){
		$id = $this->input->get("id");
		$delete = $this->db->query("DELETE FROM ms_truck_type WHERE id = '$id' ");
		if($delete){
			return true;
		}
	}

	public function add_truck_type(){
		$truck_type = $this->input->post('truck_type');
		$capacity = $this->input->post('capacity');
		$this->db->insert("ms_truck_type", array('truck_type' => $truck_type, 'capacity' => $capacity));
		redirect('truck_management');
	}

}