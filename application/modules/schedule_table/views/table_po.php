<?php
$i = 0;
$row = 1;
foreach($data_po->result() as $get){
	?>
<tr class="row-data" id="ronum-<?php echo $row; ?>">
	<td width="40" class="text-center">
		<input type="checkbox" class="cb_row" value="<?php echo $row; ?>" >
	</td>

	<td width="65" bgcolor="#FDFDB0">
		<select class="form-control inputz <?php echo $row; ?>" name="category[]" id="2_<?php echo $row; ?>" require="">
			<option value="IBD">IBD</option>
		</select>
	</td>

	<td width="80">
		<?php echo $get->vendor_code ?>
	</td>

	<td width="100">
		<?php echo $get->material_number ?>
	</td>

	<td width="300">
		<?php echo $get->material_name ?>
	</td>

	<td width="60">
		<?php echo "9003" ?>
	</td>

	<td width="100">
		<?php echo $get->po_number ?>
		<input type="hidden" name="po[]" value="<?php echo $get->po_number ?>">
	</td>

	<td width="45">
		<?php echo $get->item_number ?>
		<input type="hidden" name="po_line[]" value="<?php echo $get->item_number ?>">
	</td>

	<td width="60" class="text-right">
		<?php echo $get->order_qty ?>
	</td>

	<td width="20" class="text-center">
		<?php echo $get->order_uom; ?>
	</td>

	<td width="45" bgcolor="#FDFDB0">
		<input type="text" class="form-control" name="f_bol[]">
	</td>

	<td width="110" bgcolor="#FDFDB0">
		<?php 
			$dc = date_create($get->delivery_date);
			$df = date_format($dc, "Y-m-d"); 
		?>
		<input type="date" class="form-control date-rds" name="date_rds[]" value="<?php echo $df; ?>">
	</td>

	<td width="40" bgcolor="#FDFDB0">
		<select class="form-control inputz <?php echo $row; ?>" name="shift[]" id="13_<?php echo $row; ?>" require="">
			<option value=""></option>
			<option value="DM">DM</option>
			<option value="DP">DP</option>
			<option value="DS">DS</option>
		</select>
	</td>
</tr>
	<?php
	$i++;
	$row++;
}
?>