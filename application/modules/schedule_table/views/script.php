<script>
	function getDataPO(){
		var df = $("[name='date_from']").val();
		var dt = $("[name='date_to']").val();
		var sup = $("[name='supplier']").val();

		$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>schedule_table/find_data",
			data : {
				'date_from' : df,
				'date_to' : dt,
				'supplier' : sup
			},
			success:function(resp){
				$("#modal-get-po").modal('hide');
				$("#area-content").html(resp);
			},
			error:function(e){
				alert("Soemthing wrong!");
				console.log(e.getMessage());
			}
		})
	}

	function sumbitForm(){
		$("#idSchTbl").submit();
	}

	function validateData(){
		var cek = 0;
		var conf = confirm("Apakah anda yakin?");
		if(conf){
			if($(".row-data").length == 0){
				alert("Tidak ada data yang disimpan");
			}else{
				$('[name="category[]"]').each(function(){
					if(this.value == ""){
						alert("Terdapat kolom 'category' yang kosong!");
						cek++;
						return false;
					}
				})

				$('[name="f_bol[]"]').each(function(){
					if(this.value == ""){
						alert("Terdapat kolom 'Bill of Lading' yang kosong!");
						cek++;
						return false;
					}
				})

				$('[name="date_rds[]"]').each(function(){
					if(this.value == ""){
						alert("Terdapat kolom 'Date' yang kosong!");
						cek++;
						return false;
					}
				})

				$('[name="shift[]"]').each(function(){
					if(this.value == ""){
						alert("Terdapat kolom 'shift' yang kosong!");
						cek++;
						return false;
					}
				});

				if(cek == 0){
					sumbitForm();
				}
			}
		}
	}

	function removeRow(){
		var cek = confirm("Apakah anda yakin?");
		if(cek){
			$.each($("input[class='cb_row']:checked"), function(){
                var c = this.value;
                $("#ronum-"+c).remove();
            });
		}
	}
</script>