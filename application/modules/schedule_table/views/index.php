<style type="text/css">
.select2-results__option {
    padding: 6px;
    user-select: none;
    -webkit-user-select: none;
}

	table th{
		background-color: #00aec5 !important;
		text-align: center;
		font-size:12px;
		justify-content: center;
  		flex-direction: column;
	}

table td 
{
	white-space: nowrap;
	margin: 0px;
	padding: 0px!important;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: middle;
    border-top: 1px solid #ddd;
}

.table-responsive{
	margin-top:-20px;
}

	input {
	font-size:12px;
    width: 100%;
    margin:0px;
    padding:0px;
    display: inline-table;
    
		}
}

@media screen and (min-width: 400px){
	.button-action{
		text-align: center;
		
	}

	.schedule-periode{
		width: 100%
	}

	#date-filter{
		padding:0px;
		margin-left:35px;
	}

}

@media screen and (min-width: 800px){
	.button-action{
		text-align: right;
		margin-top:30px;
	}

	.schedule-periode{
		margin-top:20px;
		width: 100%
	}

	.date-filter{
		padding:0px;
		margin-left:35px;
	}

	.category-4{
		
	}
}

</style>
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col">
                <div class="card" style="margin-bottom:0px; margin-top:50px">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-file-invoice"></i>
                    </div>
                    <h4 class="card-title">
                    	<table>
                    		<tr>
                    			<td width="130">Schedule Table</td>
                    		</tr>
                    	</table>			
					</div></h4>
                <div class="card-body " style="margin-bottom:-15px">
                	<?php
			if($this->session->flashdata('DELETED') != ""){
				?>
			<div class="alert alert-danger alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				<b><?php echo $this->session->flashdata('DELETED'); ?></b>
			</div>
				<?php
			}
			?>

			<?php
			if($this->session->flashdata('SAVED') != ""){
				?>
			<div class="alert alert-info alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				 <b><?php echo $this->session->flashdata('SAVED'); ?></b>
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_ERROR') != ""){
				?>
			<div class="alert alert-danger alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				Error! material : <b><?php echo $this->session->flashdata('LIST_ERROR') ?></b>, master data standar pallet belum ditentukan.
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_NOACT') != ""){
				?>
			<div class="alert alert-warning alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				<a href="#" target="_">Jadwal pengiriman dengan nomor IBD <b><?php echo $this->session->flashdata('LIST_NOACT') ?></b> tidak diubah, material sedang dikirim / sudah diterima.</a>
			</div>
				<?php
			}
			?>
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-md-4"></div>
						<div class="col-md-4"></div>
						<div class="col-md-4 pull-right float-left">
							<button class="btn btn-success btn-sm" onclick="validateData()">
								<i class="fas fa-save"></i>&nbsp;&nbsp;SAVE
							</button>
							<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-get-po">
								<i class="fas fa-sync"></i>&nbsp;&nbsp;GET PO DATA
							</button>
							<button class="btn btn-danger btn-sm" onclick="removeRow()">
								<i class="fas fa-save"></i>&nbsp;&nbsp;REMOVE
							</button>
						</div>
					</div>
					<form id="idSchTbl" action="<?php echo base_url(); ?>schedule_table/save_schedule" method="post">
							<div class="table-schedule table-responsive" style=" overflow-x:hidden;margin-top:5px">
								<table border="0" id="schedule_table" class="table table-hover">
									<thead style="position: relative; z-index: 2;">
										<tr>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="40"><b disabled value="No" style="height: 50px;" />No</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="45"><b disabled value="Category" style="height: 50px" />Category</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="80"><b disabled value="Vendor/Supplier" style="height: 50px" />Vendor</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="100"><b disabled value="Item Code" style="height: 50px;" />Item Code</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="240"><b disabled value="Item Description" style="height: 50px;"/>Item Description</th>
													
													<th style="font-size:12px; font-weight:800" rowspan="2" width="60"><b disabled value="Plant" style="height: 50px"/>Storage Location</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="100"><b disabled value="Purchase Order Doc." style="height: 50px"/>IBD Number</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="45"><b disabled value="Line" style="height: 50px"/>Line</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="60"><b disabled value="Order Qty" style="height: 50px"/>Order Qty</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="20"><b disabled value="UOM" style="height: 50px"/>UOM</th>
													<th style="font-size:12px; font-weight:800" rowspan="2" width="90"><b disabled value="Line" style="height: 50px"/>Bill of Lading</th>
													<th style="font-size:12px; font-weight:800" colspan="2"><b disabled value="Revision Del.Plan" style="width: 260px;height: 25px"/>Revision Del.Plan
													</th>

												</tr>
												<tr>
													<th style="font-size:12px; font-weight:800" width="110"><b disabled/>Date</th>
													<th style="font-size:12px; font-weight:800" width="40"><b disabled/>Shift</th>

												</tr>
									</thead>
									<tbody id="area-content">

									</tbody>
								</table>
							</div>
						</form>
				</div>
							
				<!-- <div class="col-md-4" style="margin-top:-2px;">
							<div class="form-inline" style="margin-top:-7px">
				              	<button type="button" class="btn btn-info" style="" onclick="setYear_sch($('#year_sch').val(), 'DEC')">
				                  <i class="fa fa-chevron-left"></i>
				                </button>
				                <div class="form-group">
				                <input type="number" class="form-control" onchange="(setYear2_sch())"  id="year_sch" style="width: 70px; text-align: center; font-size: 15px" value="<?php echo (null === $_GET['year'])?date('Y'):$_GET['year']; ?>">
				            	</div>

				                <button type="button" class="btn btn-info" style="" onclick="setYear_sch($('#year_sch').val(), 'INC')">
				                  <i class="fa fa-chevron-right" ></i>
				                </button>

				                <button type="button" class="btn btn-info" style="" onclick="setWeek_sch($('.week_sch').val(), 'DEC')">
				                  <i class="fa fa-chevron-left"></i>
				                </button>
				                
				                <input onchange="setWeek_sch(($('.week_sch').val())-1, 'INC')" type="number" class="form-control week_sch" style="width: 70px; text-align: center; font-size: 15px" value="<?php echo (null === $_GET['week'])?date('W'):$_GET['week']; ?>">
				                <button type="button" class="btn btn-info" style=""onclick="setWeek_sch($('.week_sch').val(), 'INC')">
				                  <i class="fa fa-chevron-right" ></i>
				                </button>
		            		</div>
							
				</div> -->

			</div>
		</div>
	</div>
</div>	
<div class="modal fade" id="modal-get-po">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="card card-signup card-plain">
				<div class="modal-header">
					<div class="card-header card-header-primary text-center">
	                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
	                      <i class="material-icons">clear</i>
	                    </button>
			            <h4 class="card-title">Search PO Data</h4>
				    </div>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>From</label>
								<input type="date" class="form-control date_from" name="date_from">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>To</label>
								<input type="date" class="form-control date_to" name="date_to">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label>From Supplier :</label>
								<select name="supplier" class="form-control">
									<option value="ALL">ALL SUPPLIER</option>
									<?php
									foreach($supplier->result() as $sup){
										?>
									<option value="<?php echo $sup->vendor_code ?>"><?php echo $sup->vendor_code." - ".$sup->vendor_name ?></option>
										<?php
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<button class="btn btn-primary btn-sm btn-search" onclick="getDataPO()">SEARCH</button>
				</div>
			</div>
		</div>
	</div>
</div>
	