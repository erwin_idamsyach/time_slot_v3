<?php

class Schedule_table extends CI_Controller{

	public function index(){
		$db_sap = $this->load->database('sap', TRUE);
		$db2 = $this->load->database('second', TRUE);

		$role = $this->session->userdata('sess_role_no');
		if($role == 1 || $role == 2){

		}else{
			isRole();
		}

		$get_day = date('D');
		$time_freeze = $this->db->query("SELECT day, start_time, end_time FROM tb_time_freeze WHERE day ='$get_day' ");
		$table_select = $this->input->get('table');
		$category = $this->input->get('category');
		$year = (null === $this->input->get('year'))?date('Y'):$this->input->get('year');
		$week = (null === $this->input->get('week'))?date('Y'):$this->input->get('week');

		$get_item_code = $this->db->query("SELECT material_code FROM tB_rds_detail where category = '$category' ");
		$get_supplier = $this->db->query("SELECT vendor_code, vendor_name, vendor_alias FROM ".$db2->database.".ms_supplier");
		$get_category = $this->db->query("SELECT * FROM ms_category");
		$data['category'] = $get_category;
		$data['supplier'] = $get_supplier;

		//dump($get_data->result());
		getHTML('schedule_table/index',$data);
	}

	public function find_data(){
		$db_sap = $this->load->database('sap', TRUE);
		$db2 = $this->load->database('second', TRUE);
		$dn = 0;
		$du = 0;
		$date_from = $this->input->get('date_from');
		$date_to = $this->input->get('date_to');
		$supplier = $this->input->get('supplier');

		if($supplier != "ALL"){
			$where_vencode = "AND vendor_code = ".$supplier;
		}else{
			$where_vencode = "";
		}

		$getData = $this->db->query("SELECT 
						".$db_sap->database.".tb_po_subpo.*, 
						".$db2->database.".ms_material.material_name 
							FROM ".$db_sap->database.".tb_po_subpo 
							INNER JOIN ".$db2->database.".ms_material ON ".$db_sap->database.".tb_po_subpo.material_number = ".$db2->database.".ms_material.material_sku
							WHERE delivery_date >= '$date_from' 
							AND delivery_date <= '$date_to' 
							AND status = 0
							AND po_number LIKE '2%'
							AND order_qty != 0
							$where_vencode
							ORDER BY po_number ASC");
		/*$update = $this->db->query("UPDATE sap_temp_db.tb_po_subpo SET status=1
							WHERE delivery_date >= '$date_from' 
							AND delivery_date <= '$date_to' 
							$where_vencode");*/

		$data['data_po'] = $getData;
		$this->load->view('table_po', $data);
	}

	public function save_schedule(){
		$db_sap = $this->load->database('sap', TRUE);
		$db2 = $this->load->database('second', TRUE);

		$arr_err = array();
		$arr_used = array();

		$po = $this->input->post('po');
		$line = $this->input->post('po_line');
		$category = $this->input->post('category');
		$shift = $this->input->post('shift');
		$rds = $this->input->post('date_rds');
		$bol = $this->input->post('f_bol');

		$a = 0;
		$c = count($po);

		while($a < $c){
			$f_po = $po[$a];
			$f_line = $line[$a];
			$bol_no = $bol[$a];
			/*$this->db->where('po_number', $f_po);
			$this->db->where('item_number', $f_line);
			$this->db->update('tb_po_subpo', array(
				"category" => $category[$a],
				"shift" => $shift[$a]
			));*/
			$this->db->query("UPDATE ".$db_sap->database.".tb_po_subpo SET category='$category[$a]', shift='$shift[$a]', delivery_date='$rds[$a]' WHERE po_number='$f_po' AND item_number='$f_line'");

			$get = $this->db->query("SELECT
								po_number,
								vendor_code,
								item_number,
								material_number,
								order_qty,
								order_uom,
								category,
								shift,
								delivery_date,
								WEEK (delivery_date) AS WEEK,
								reference_po,
								storage_location,
								stock_type
							FROM
								".$db_sap->database.".tb_po_subpo
							WHERE
								po_number = '$f_po'
							AND item_number = '$f_line'")->row();
			if($get->week < 10 && strpos($get->week, "0") == 0){
				$week_n = str_replace("0", "", $get->week);
				$week = $week_n;
			}else if($get->week == 53){
				$week = 1;
			}else{
				$week = $get->week;
			}

			$std_plt = $this->db->query("SELECT uom_pallet FROM ".$db2->database.".ms_material WHERE material_sku='$get->material_number'")->row();
			if($std_plt->uom_pallet != 0){
				$req_pallet = $get->order_qty/$std_plt->uom_pallet;
				$data = array(
					"category" => $get->category,
					"po_number" => $get->po_number,
					"po_line_item" => $get->item_number,
					"material_code" => $get->material_number,
					"vendor_code" => $get->vendor_code,
					"qty" => $get->order_qty,
					"uom" => $get->order_uom,
					"requested_delivery_date" => $get->delivery_date,
					"shift" => $get->shift,
					"plt_truck" => "9003",
					"uom_plt" => $std_plt->uom_pallet,
					"req_pallet" => $req_pallet,
					"week" => $week,
					"reference_po" => $get->reference_po,
					"sap_line" => $get->item_number,
					"sap_stock_type" => $get->stock_type,
					"sap_sloc" => $get->storage_location,
					"sap_uom" => $get->order_uom,
					"qty_ori" => $get->order_qty,
					"bill_of_lading" => $bol_no
				);

				$cekNR = $this->db->query("SELECT status FROM tb_rds_detail WHERE po_number='$f_po' AND po_line_item='$f_line'");
				if($cekNR->num_rows() == 0){
					$this->db->insert("tb_rds_detail", $data);

					$update = $this->db->query("UPDATE sap_temp_db.tb_po_subpo SET status=1, qty_acc=order_qty
								WHERE po_number='$f_po'
								AND item_number='$f_line'");
					$dn++;
				}else{
					$nr = $cekNR->row();
					if($nr->status != 1){
						$data['is_schedule'] = '0';
						$this->db->where("po_number", $f_po);
						$this->db->where("po_line_item", $f_line);
						$this->db->update("tb_rds_detail", $data);

						$update = $this->db->query("UPDATE sap_temp_db.tb_po_subpo SET status=1, qty_acc=order_qty
								WHERE po_number='$f_po'
								AND item_number='$f_line'");
						$du++;
					}else{
						$update = $this->db->query("UPDATE sap_temp_db.tb_po_subpo SET status=1, qty_acc=order_qty
								WHERE po_number='$f_po'
								AND item_number='$f_line'");

						array_push($arr_used, $get->po_number);
					}
				}
			}else{
				array_push($arr_err, $get->material_number);
			}
			//echo $po[$a]." ".$line[$a]." ".$category[$a]." ".$shift[$a]."<br>";
			$a++;
		}

		if(count($arr_err) > 0){
			$list_err = implode(", ", $arr_err);
			$this->session->set_flashdata("LIST_ERROR", $list_err);
		}

		if(count($arr_used) > 0){
			$list_used = implode(", ", $arr_used);
			$this->session->set_flashdata("LIST_NOACT", $list_used);
		}

		$this->session->set_flashdata("SAVED", "Data berhasil disimpan, Data baru : ".$dn." Data Update : ".$du);
		redirect('schedule_table');
	}

	public function schedule2(){
		$db_sap = $this->load->database('sap', TRUE);
		$db2 = $this->load->database('second', TRUE);

		$role = $this->session->userdata('sess_role_no');
		if($role == 1 || $role == 2){

		}else{
			isRole();
		}

		$get_day = date('D');
		$time_freeze = $this->db->query("SELECT day, start_time, end_time FROM tb_time_freeze WHERE day ='$get_day' ");
		$table_select = $this->input->get('table');
		$category = $this->input->get('category');
		$year = (null === $this->input->get('year'))?date('Y'):$this->input->get('year');
		$week = (null === $this->input->get('week'))?date('Y'):$this->input->get('week');
		$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  a.created_at,
									  a.update_at,
									  b.vendor_alias ,
									  c.material_name,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tb_rds_detail as a
									  LEFT JOIN ".$db2->database.".ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN ".$db2->database.".ms_material as c ON a.material_code = c.material_sku
									  WHERE a.category LIKE '%$category%' AND a.week = '$week' AND YEAR(requested_delivery_date) = '$year'
									  ORDER BY a.id ASC
									  ");

		$get_item_code = $this->db->query("SELECT material_code FROM tB_rds_detail where category = '$category' ");
		$get_supplier = $this->db->query("SELECT vendor_code, vendor_name, vendor_alias FROM ".$db2->database.".ms_supplier");
		$get_category = $this->db->query("SELECT * FROM ms_category");
		$data['category'] = $get_category;
		$data['data'] = $get_data;
		$data['supplier'] = $get_supplier;
		$data['item_code'] = $get_item_code;
		$data['time_freeze'] = $time_freeze;

		//dump($get_data->result());
		if(isset($table_select)){
			getHTML('schedule_table/schedules2',$data);
		}else{
			getHTML('schedule_table/schedule2',$data);
		}

	}

	public function save_schedule2(){
		$db_sap = $this->load->database('sap', TRUE);
		$db2 = $this->load->database('second', TRUE);

		$arr_data = array();
		$c_date = date('Y-m-d');

		$yyy  = (null === $this->input->post('yyy'))?date('Y'):$this->input->post('yyy');
		$www  = (null === $this->input->post('www'))?date('W'):$this->input->post('www');

		if(null !== $this->input->get('category')){
			$category_uri = $this->input->get('category');
		}else{
			$category_uri = "FACE";
		}
		$i 						= 0;
		$arr_err 				= array();
		$arr_noact				= array();
		$arr_data_lack			= array();
		$arr_data_order			= array();
		$arr_sap_er				= array();
		$arr_sap_po				= array();
		$arr_not_roh			= array();
		$get_no 				= $this->input->post('no');
		$get_category 			= $this->input->post('category');
		$get_vendor 			= $this->input->post('vendor');
		$get_item_code 			= $this->input->post('item_code');
		$get_material_name 		= $this->input->post('material_name');
		$get_qty_per_palet 		= $this->input->post('qty_per_palet');
		$get_plan 				= $this->input->post('plan');
		$get_line 				= $this->input->post('line');
		$get_purchase_order 	= $this->input->post('purchase_order');
		$get_order_qty 			= $this->input->post('order_qty');
		$get_uom 				= $this->input->post('uom');
		$get_date 				= $this->input->post('date');
		$get_shift 				= $this->input->post('shift');
		$history['by_who']		= $this->session->userdata('sess_id');
		$history['date']		= Date('Y-m-d');
		$history['time']		= Date('H:i:s');
		$history['table_join']	= "tb_rds_detail";
		$history['select_join'] = "id";
		$history['author']		= 2;
		/*echo json_encode($get_order_qty);
		echo json_encode($get_qty_per_palet); die();*/
		$save = 0;
		$update = 0;
			foreach($get_category as $bru){
				if($bru == ""){continue;}
				$category 		= $bru;
				$vendor_split 	= explode(' ',$get_vendor[$i]);
				$vendor 		= $vendor_split[0];
				$item_code 		= $get_item_code[$i];
				$material_name 	= $get_material_name[$i];
				$qty_per_palet 	= (double)$get_qty_per_palet[$i];
				$plan 			= $get_plan[$i];
				$line 			= $get_line[$i];
				$purchase_order = $get_purchase_order[$i];
				$order_qty      = str_replace(",",".",$get_order_qty[$i]);
				$uom 			= $get_uom[$i];
				if($get_date != ''){
					$date 			= date('Y-m-d' ,strtotime($get_date[$i]));
				}else{
					$date       = '';
				}

				$shift 			= $get_shift[$i];
				$date_now 		= date('D' ,strtotime($get_date[$i]));
				$week 			= date('W' ,strtotime($get_date[$i]));
				if($week < 10 && strpos($week, "0") == 0){
					$week_n = str_replace("0", "", $week);
					$week = $week_n;
				}else if($week == 53){
					$week = 1;
				}else{
					$week = $week;
				}

				$cekUomPlt = '';
				$getUom = $this->db->query("SELECT material_type FROM ".$db2->database.".ms_material WHERE material_sku='$item_code'")->row();
				
				if($getUom != ''){
					$cekUomPlt = $getUom->material_type;
				}

				$arr_rec = array(
									"category" => $category,
									"vendor" => $get_vendor[$i],
									"item_code" => $get_item_code[$i],
									"material_name" => $get_material_name[$i],
									"uom_plt" => $cekUomPlt,
									"plant" => $get_plan[$i],
									"line" => $get_line[$i],
									"po_number" => $get_purchase_order[$i],
									"order_qty" => $get_order_qty[$i],
									"uom" => $get_uom[$i],
									"date" => $get_date[$i],
									"shift" => $get_shift[$i]
								);
				
				if($cekUomPlt != "ROH"){
					array_push($arr_not_roh, $item_code);
					$data_state = "3";

				}else{

					$data = array(
						"po_number" 			  => $purchase_order,
						"po_line_item" 			  => $line,
						"uom_plt" 			  	  => $cekUomPlt,
						"category" 				  => $category,
						"vendor_code"			  => $vendor,
						"material_code"			  => $item_code,
						"qty"					  => $order_qty,
						"qty_ori"				  => $order_qty,
						"uom"					  => $uom,
						"requested_delivery_date" => $date,
						"shift" 				  => $shift,
						"plt_truck" 			  => $plan,
						"flag"					  => 0,
						"week"					  => $week,
						"weighing_material"		  => 1

						);
					$history['vendor_code']	= $vendor;
					if($cekUomPlt != "ROH"){

					}else if( ($line != null || $line != "") && ($purchase_order != null || $purchase_order != '') && ($category != null || $category != '') && ($vendor != null || $vendor != '') && ($item_code != null || $item_code != '') && ($order_qty != null || $order_qty != '') && ($uom != null || $uom != '') && ($date != null || $date != '') && ($date != null || $date != '') && ($shift != null || $shift != '') && ($plan != null || $plan != '')){

						$check_ava = $this->db->query("SELECT status, id, is_schedule, qty, req_pallet, requested_delivery_date, week, po_line_item, shift, material_code, uom_plt, sap_line, sap_stock_type, sap_sloc FROM tb_rds_detail WHERE po_number='$purchase_order' AND po_line_item='$line' AND material_code = '$item_code' ORDER BY id DESC");

						if($check_ava->num_rows() >= 1){
							$cek_row = $check_ava->row();
							$ids = $cek_row->id;
							$c_status = $cek_row->status;
							$c_sched = $cek_row->is_schedule;

							$this->db->where('id', $ids);
							$this->db->update('tb_rds_detail', array("flag" => 0));

							if($c_status != '1'){
								$data['update_by'] = $this->session->userdata('sess_id');
								$data['update_at'] = date('Y-m-d H:i:s');
								$data['is_schedule'] = 0;
								$get_check = $check_ava->row();
								if($get_check->qty == $order_qty && $get_check->week == $week && $get_check->shift == $shift && $get_check->requested_delivery_date == $date && $get_check->po_line_item == $line && $get_check->material_code == $item_code){
									$data_state = "4";

								}else{
									if($get_check->sap_line == ""){
										$cekpo = $this->cek_po_sap($purchase_order, $vendor, $item_code, $date, $order_qty);
										if($cekpo['status'] == true){
											$row = $cekpo['data'];
											$id_sap = $row->id;
											$qty_acc = $row->qty_acc;
											$data['sap_line'] = $row->item_number;
											$data['sap_stock_type'] = $row->stock_type;
											$data['sap_sloc'] = $plan;
											$data['sap_uom'] = $row->order_uom;

											$this->db->where('id', $ids);
											$this->db->update('tb_rds_detail', $data);
											$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule='$ids'");
											$mm = array(
												"id_schedule" => $get_check->id,
												"quantity"	  => 0,
												"action"	  => "Update",
												"created_by"  => $this->session->userdata('sess_id')
											);
											//add_material_movement($mm);
											$history['action']		= "Update";
											$history['value']		= '';
											$history['value2']		= '';
											$history['description'] = "RDS, PO :".$purchase_order.", line :".$line.", Material Code :".$item_code.", Qty :".$order_qty.", ".$req_plt.", RDD : ".date_format(date_create($date), "D, d M Y")." week :".$week;
											$this->db->insert('tb_history',$history);

											$acc = $qty_acc+$order_qty;
											$this->update_sap_qty($id_sap, $acc);
											$data_state = "1";
											$update++;
										}else{
											if($cekpo['msg'] == "PO_NOT_FOUND"){
												array_push($arr_sap_po, $purchase_order);
												$data_state = 5;
											}else{
												array_push($arr_sap_er, $purchase_order);
												$data_state = 6;
											}
										}
									}else{
										$this->db->where('id', $ids);
										$this->db->update('tb_rds_detail', $data);
										$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule='$ids'");
										$mm = array(
											"id_schedule" => $get_check->id,
											"quantity"	  => 0,
											"action"	  => "Update",
											"created_by"  => $this->session->userdata('sess_id')
										);
										//add_material_movement($mm);
										$history['action']		= "Update";
										$history['value']		= '';
										$history['value2']		= '';
										$history['description'] = "RDS, PO :".$purchase_order.", line :".$line.", Material Code :".$item_code.", Qty :".$order_qty.", ".$req_plt.", RDD : ".date_format(date_create($date), "D, d M Y")." week :".$week;
										$this->db->insert('tb_history',$history);
										$data_state = "1";
										$this->update_sap_qty($purchase_order, $vendor, $item_code, $date, $order_qty);
										$update++;
									}
								}
							}else{
									array_push($arr_noact, $purchase_order);
									$data_state = "2";
							}
						}else{
							$cekpo = $this->cek_po_sap($purchase_order, $vendor, $item_code, $date, $order_qty);
							if($cekpo['status'] == true){
								$row = $cekpo['data'];
								$id_sap = $row->id;
								$qty_acc = $row->qty_acc;
								$data['sap_line'] = $row->item_number;
								$data['sap_stock_type'] = $row->stock_type;
								$data['sap_sloc'] = $plan;
								$data['sap_uom'] = $row->order_uom;
								$data['created_by'] = $this->session->userdata('sess_id');
								$data['created_at'] = date('Y-m-d H:i:s');
								$data['is_schedule'] = 0;
								$this->db->insert('tb_rds_detail', $data);
								$last_id = $this->db->query("SELECT id FROM tb_rds_detail ORDER by id DESC")->row()->id;
								$mm = array(
												"id_schedule" => $last_id,
												"quantity"	  => 0,
												"action"	  => "Add",
												"created_by"  => $this->session->userdata('sess_id')
								);
								//add_material_movement($mm);
								$history['action']		= "Add";
								$history['id_join']		= $last_id;
								$history['description'] = "RDS, PO :".$purchase_order.", line :".$line.", Material Code :".$item_code.", Qty :".$order_qty.", ".$req_plt.", RDD : ".date_format(date_create($date), "D, d M Y")." week :".$week;
								$this->db->insert('tb_history',$history);
								$this->update_sap_qty($purchase_order, $vendor, $item_code, $date, $order_qty);

								$acc = $qty_acc+$order_qty;
								$this->update_sap_qty($id_sap, $acc);
								$data_state = "1";
								$save++;
							}else{
								if($cekpo['msg'] == "PO_NOT_FOUND"){
									array_push($arr_sap_po, $purchase_order);
									$data_state = 5;
								}else{
									array_push($arr_sap_er, $purchase_order);
									$data_state = 6;
								}
							}
						}
					}else{
						array_push($arr_err, $purchase_order);
						$data_state = "3";
					}
				}

				$arr_rec['data_state'] = $data_state;

				array_push($arr_data, $arr_rec);

				if($save > 0 || $update > 0){
					// echo "YES, WE HERE";
					$this->db->query("UPDATE tb_rds_detail SET is_schedule=0 WHERE requested_delivery_date='$date' AND status='0'");
					$this->db->query("DELETE FROM tb_schedule_detail WHERE rdd='$date' AND status=0");
					$this->db->query("DELETE FROM tb_scheduler WHERE rdd='$date' AND status='0'");
				}

				$i++;
			}

			if(count($arr_err) >= 1){
				$po_list = implode(", ", $arr_err);
				$this->session->set_flashdata('LIST_ERROR', $po_list);
			}

			if(count($arr_data_lack >= 1)){
				$data_lack = implode(", ", $arr_data_lack);
				$this->session->set_flashdata('LIST_DATA_LACK', $data_lack);
			}

			if(count($arr_data_order >= 1)){
				$data_order = implode(", ", $arr_data_order);
				$this->session->set_flashdata('LIST_DATA_ORDER', $data_order);
			}

			if(count($arr_noact) >= 1){
				$noact_list = implode(", ", $arr_noact);
				$this->session->set_flashdata('LIST_NOACT', $noact_list);
			}

			if(count($arr_sap_er) > 0){
				$sap_er = implode(", ", $arr_sap_er);
				$this->session->set_flashdata('SAP_ER', $sap_er);
			}

			if(count($arr_sap_po) > 0){
				$sap_po = implode(", ", $arr_sap_po);
				$this->session->set_flashdata('SAP_PO', $sap_po);
			}

			if(count($arr_not_roh) > 0){
				$not_roh = implode(", ", $arr_not_roh);
				$this->session->set_flashdata('NOT_ROH', $not_roh);
			}

			$this->db->query("UPDATE tb_data_checker AS a INNER JOIN ms_user AS b ON a.id_user = b.id SET a.data_checker =1 WHERE b.vendor_code != '' ");
			$this->session->set_flashdata('SAVED', "<b style='color:black'>Data berhasil tersimpan,</b> <b style='color:green'> Data Baru : ".$save." Data Update : ".$update."</b>");
		//echo json_encode($arr_data); die();
		$data['arr_data'] = $arr_data;
		$data['year'] = $yyy;
		$data['week'] = $www;
		getHTML('schedule_table/schedule_tbl_after_save', $data);
		//redirect('schedule/schedule2?category='.$category_uri."&year=".$yyy."&week=".$www);
	}

	public function cek_po_sap($po_number, $vendor_code, $material_number, $delivery_date, $quantity){
		$db_sap = $this->load->database('sap', TRUE);
		$db2 = $this->load->database('second', TRUE);

		$arr = array();
		$cek_po = $this->db->query("SELECT * FROM ".$db_sap->database.".tb_po_subpo WHERE po_number='$po_number' AND vendor_code='$vendor_code' AND material_number='$material_number'");

		if($cek_po->num_rows() >= 1){
			foreach($cek_po->result() as $data){
				$po_qty = $data->order_qty;
				$po_qty_acc = $data->qty_acc;
				$temp_qty = $po_qty_acc+$quantity;
				if($temp_qty > $po_qty){
					if($temp_qty > $po_qty){
						$arr = array("status" => false, "msg" => "QTY_EXCEEDED");
					}else{
						$arr = array("status" => true, "msg" => "PO_FOUND", "data" => $data);
						break;
					}
				}else{
					$arr = array("status" => true, "msg" => "PO_FOUND", "data" => $data);
					break;
				}
			}
		}else{
			$arr = array("status" => false, "msg" => "PO_NOT_FOUND");
		}
		return $arr;
	}

	public function update_sap_qty($id_sap, $qty){
		$db_sap = $this->load->database('sap', TRUE);
		$db2 = $this->load->database('second', TRUE);

		$cek_po = $this->db->query("UPDATE ".$db_sap->database.".tb_po_subpo SET qty_acc=$qty WHERE id=$id_sap");		
	}
}
?>