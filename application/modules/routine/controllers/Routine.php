<?php

class Routine extends CI_Controller{

	public function check_data_changed(){
		$user_id = $this->session->userdata('sess_id');
		$get = $this->db->query("SELECT data_checker FROM tb_data_checker WHERE id_user = '$user_id' ")->row();
		echo $get->data_checker;
		
	}

	public function check_schedule_changed(){
		$user_id = $this->session->userdata('sess_id');
		$get = $this->db->query("SELECT schedule_checker FROM tb_data_checker WHERE id_user = '$user_id' ")->row();
		echo $get->schedule_checker;
		
	}

	
	public function check_periode_changed(){
		$user_id = $this->session->userdata('sess_id');
		$get = $this->db->query("SELECT periode_checker FROM tb_data_checker WHERE id_user = '$user_id' ")->row();
		echo $get->periode_checker;
		
	}

	public function check_event_data_changed(){
		$user_id = $this->session->userdata('sess_id');
		$get = $this->db->query("SELECT freezing_event_checker FROM tb_data_checker WHERE id_user = '$user_id' ")->row();
		echo $get->freezing_event_checker;
	}

	public function set_to_zero(){
		$user_id = $this->session->userdata('sess_id');
		$this->db->query("UPDATE tb_data_checker SET data_checker= 0 WHERE id_user='$user_id' ");
		$this->session->set_flashdata("ALERT", "Anda diarahkan ke halaman ini karena Progressor melakukan perubahan pada data, mohon cek kembali data anda pada halaman ini");
	}

	public function set_event_to_zero(){
		$user_id = $this->session->userdata('sess_id');
		$this->db->query("UPDATE tb_data_checker SET freezing_event_checker= 0 WHERE id_user='$user_id' ");
		//$this->session->set_flashdata("ALERT", "Ada perubahan schedule halaman ini telah di reload");
	}

	public function set_to_zero_schedule(){
		$user_id = $this->session->userdata('sess_id');
		$this->db->query("UPDATE tb_data_checker SET schedule_checker= 0 WHERE id_user='$user_id' ");
		$this->session->set_flashdata("ALERT", "Anda diarahkan ke halaman ini karena adanya perubahan schedule");
	}

	public function set_to_zero_periode(){
		$user_id = $this->session->userdata('sess_id');
		$this->db->query("UPDATE tb_data_checker SET periode_checker= 0 WHERE id_user='$user_id' ");
		$this->session->set_flashdata("ALERT", "Halaman di reload otomatis dikarenakan adanya perubahan jam kerja scheduling");
	}

}

?>