<?php
$plan = "style='background: #FF2525; color: #FFF; height: 25px;'";
$not_plan = "style='background: #25FF3F; color: #000; height: 25px;'";
?>
<table class="table table-bordered" style="margin-top:12px">
	<thead>
		<tr bgcolor="#2184FF" style="color: #fff">
			<th width="260px">Time Slot (24 Hours format)</th>
			<th>Status</th>
			
		</tr>
	</thead>
	<tbody>
		<?php
		foreach($time_slot->result() as $get){
			?>
		<tr>
			<td>
				<?php 
				$st = date_create($get->start_time);
				$et = date_create($get->end_time);
				$st = date_format($st, "H:i");
				$et = date_format($et, "H:i");
				echo $st." - ".$et; ?>
			</td>
			<td>
				<table class="table table-bordered" style="margin: 0">
					<tr>
				<?php
				$getInfo = $this->db->query("SELECT id FROM tb_delivery_detail WHERE id_time_slot='$get->id' AND delivery_date='$date_find' GROUP BY id_truck");
				if($getInfo->num_rows() == 0){
					?>
					<td <?php echo $not_plan ?>></td>
					<td <?php echo $not_plan ?>></td>
					<td <?php echo $not_plan ?>></td>
					
					<?php
				}else if($getInfo->num_rows() == 1){
					?>
					<td <?php echo $plan ?>></td>
					<td <?php echo $not_plan ?>></td>
					<td <?php echo $not_plan ?>></td>
					
					<?php
				}else if($getInfo->num_rows() == 2){
					?>
					<td <?php echo $plan ?>></td>
					<td <?php echo $plan ?>></td>
					<td <?php echo $not_plan ?>></td>
					
					<?php
				}else if($getInfo->num_rows() == 3){
					?>
					<td <?php echo $plan ?>></td>
					<td <?php echo $plan ?>></td>
					<td <?php echo $plan ?>></td>
					
					<?php
				}else{
					?>
					<td <?php echo $plan ?>></td>
					<td <?php echo $plan ?>></td>
					<td <?php echo $plan ?>></td>
					
					<?php
				}
				?>
				</tr>
				</table>
			</td>
			
		</tr>
			<?php
		}
		?>
	</tbody>
</table>