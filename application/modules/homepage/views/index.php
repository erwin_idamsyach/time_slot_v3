<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Time Slot Management</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/images/unilever.png" />
	  <!-- Tell the browser to be responsive to screen width -->
	  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	  <!-- Bootstrap 3.3.6 -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
	  <!-- Font Awesome -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
	  <!-- Ionicons -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/ionicons.min.css">
	  <!-- daterange picker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/daterangepicker/daterangepicker.css">
	  <!-- bootstrap datepicker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
	  <!-- iCheck for checkboxes and radio inputs -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/all.css">
	  <!-- Bootstrap Color Picker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
	  <!-- Bootstrap time Picker -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
	  <!-- Select2 -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">
	  <!-- Theme style -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
	  <!-- AdminLTE Skins. Choose a skin from the css/skins
	       folder instead of downloading all of them to reduce the load. -->
	  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/skin-blue.css">

	  <!-- My Css -->
	  <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/style_cms.css"> -->

	  <!-- DataTable -->
	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">

	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/bootstrap-select.min.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/dropify.min.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/flexselect.css">
	  <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/bs-toggle.css">
<style type="text/css">
	body { 
  background: url(assets/images/homepage.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>
</head>
<body class="hold-transition">
	<nav class="navbar navbar-default" style="border-color:none!important;background-color:darkblue;border-color:rgba(0,0,0,.075);">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">
	      	<img src="<?php base_url() ?>assets/dist/img/admin.PNG" style="margin-top: -5px;" width="33px" height="33px">
	      </a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li style="color:white;background-color: white;font-size:17px"><a href="<?php echo base_url(); ?>login">Login</a></li>
	        <!-- <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
	          <ul class="dropdown-menu">
	            <li><a href="#">Action</a></li>
	            <li><a href="#">Another action</a></li>
	            <li><a href="#">Something else here</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#">Separated link</a></li>
	          </ul>
	        </li> -->
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<?php
	$plan = "style='background: #FF2525; color: #FFF; height: 25px;'";
	$not_plan = "style='background: #25FF3F; color: #000; height: 25px;'";
	?>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label style="color:white">Delivery Date :</label>
					<div class="input-group date">
	                	<div class="input-group-addon">
	                      <i class="fa fa-calendar"></i>
	                    </div>
	                    <input type="text" name="date" class="form-control pull-right dapick" id="datepicker" required="" autocomplete="false" value="<?php echo $curr_date ?>">
	                </div>
				</div>
			</div>
			<div class="col-md-1">
				<button class="btn btn-primary btn-sm btn-block" style="margin-top: 26px;" onclick="getScheduleInfo($('.dapick').val())">
					<i class="fa fa-search"></i>
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9" id="area-table" style="border-right: 1px solid #CACACA; background-color:black;opacity:0.9;color:white">
				<table class="table table-bordered" style="margin-top:12px">
					<thead>
						<tr bgcolor="#2184FF" style="color: #fff" >
							<th style="text-align: center" width="260px">Time Slot (24 Hours format)</th>
							<th style="text-align: center">STATUS</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($time_slot->result() as $get){
							?>
						<tr>
							<td>
								<?php 
								$st = date_create($get->start_time);
								$et = date_create($get->end_time);
								$st = date_format($st, "H:i");
								$et = date_format($et, "H:i");
								echo $st." - ".$et; ?>
							</td>
							<td>
								<table class="table table-bordered" style="margin: 0">
									<tr>
								<?php
								$getInfo = $this->db->query("SELECT id FROM tb_delivery_detail WHERE id_time_slot='$get->id' AND delivery_date='$date_find' GROUP BY id_truck");
								if($getInfo->num_rows() == 0){
									?>
									<td width="50" <?php echo $not_plan?>></td>
									<td width="50" <?php echo $not_plan ?>></td>
									<td width="50" <?php echo $not_plan ?>></td>
									
									<?php
								}else if($getInfo->num_rows() == 1){
									?>
									<td width="50" <?php echo $plan ?>></td>
									<td width="50" <?php echo $not_plan ?>></td>
									<td width="50" <?php echo $not_plan ?>></td>
									
									<?php
								}else if($getInfo->num_rows() == 2){
									?>
									<td width="50" <?php echo $plan ?>></td>
									<td width="50" <?php echo $plan ?>></td>
									<td width="50" <?php echo $not_plan ?>></td>
									
									<?php
								}else if($getInfo->num_rows() == 3){
									?>
									<td width="50" <?php echo $plan ?>></td>
									<td width="50" <?php echo $plan ?>></td>
									<td width="50" <?php echo $plan ?>></td>
									
									<?php
								}else{
									?>
									<td width="50" <?php echo $plan ?>></td>
									<td width="50" <?php echo $plan ?>></td>
									<td width="50" <?php echo $plan ?>></td>
									
									<?php
								}
								?>
								</tr>
								</table>
							</td>
						</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
			<div class="col-md-3" style="background-color:grey; opacity: 0.95">
				<h4 style="color:black;font-weight: 600">Legend :</h4>
				<div class="row" style="color:black;font-weight: 600;">
					<div class="col-md-6">
						<div class="panel" <?php echo $plan ?>></div>
					</div>
					<div class="col-md-6">
						BOOKED
					</div>
				</div>
				<div class="row" style="color:black;font-weight: 600">
					<div class="col-md-6">
						<div class="panel" <?php echo $not_plan ?>></div>	
					</div>
					<div class="col-md-6">
						NO SCHEDULE
					</div>
				</div>
				<!-- <hr> --><!-- 
				Auto Reload : <input type="checkbox" class="cb-reload" checked data-toggle="toggle"  data-onstyle="success" data-offstyle="danger"> -->
			</div>
		</div>
	</div>
	
</body>
<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url() ?>assets/dist/js/moment.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url() ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url() ?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url() ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url() ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url() ?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>assets/dist/js/demo.js"></script>

<script src="<?php echo base_url() ?>assets/dist/js/bootstrap-select.min.js"></script>

<script src="<?php echo base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- My Style -->
<script src="<?php echo base_url() ?>assets/dist/js/dropify.min.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/function-style_cms.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/liquidmetal.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/jquery.flexselect.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/style_cms.js"></script>
<script src="<?php echo base_url() ?>assets/dist/js/bs-toggle.min.js"></script>
<script>
	$("#datepicker").datepicker({
		autoclose : true
	});

	$("[data-toggle='toggle']").bootstrapToggle({
      on: 'ON',
      off: 'OFF'
    });

    function getScheduleInfo(date){
    	/*alert(date);*/
    	$("#area-table").html('<center><i class="fa fa-spinner fa-3x fa-spin"></i></center>');
    	$.ajax({
    		type : "GET",
    		url : "<?php echo base_url() ?>homepage/search_schedule",
    		data : {
    			'date' : date
    		},
    		success:function(resp){
    			$("#area-table").html(resp);
    		},
    		error:function(e){
    			alert("Something wrong!");
    		}
    	})
    }

    function getTimeSlotDetail(id){
    	alert(id);
    	$("#area-table").html('<center><i class="fa fa-spinner fa-3x fa-spin"></i></center>');
    }
</script>
</html>