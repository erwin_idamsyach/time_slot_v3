<style>
  .nav-pills .nav-item .nav-link {
    line-height: 14px;
    text-transform: uppercase;
    font-size: 10px;
    font-weight: 500;
    min-width: 0px;
    text-align: center;
    color: #555;
    transition: all .3s;
    border-radius: 30px; 
    padding: 0px 2px;
}

@media and screen(min-width: 800px){
  .authorization{
    margin-top:-40px;
  }
}

.authorization{
    margin-top:-40px;
}

.nav-pills .nav-item i {
    display: block;
    font-size: 20px;
    padding: 15px 0;
}
</style>
<div class="content">
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9">
            <div class="card ">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                <i class="fas fa-user-plus" style="font-size:30px"></i>
                </div>
                <h4 class="card-title">Edit User</h4>
                </div>
                <div class="card-body ">
                <form action="<?php echo base_url(); ?>user_management/edit_action" method="post" enctype='multipart/form-data'>
                      <div class="row">
                          <div class="col-md-3">
                            <input type="hidden" name="id" value="<?php echo $data_user->id; ?> ">
                            <div class="form-group">
                                  <label>Name</label>
                                  <input type="text" class="form-control" value="<?php echo $data_user->nama; ?>" name="name" required="">
                                </div>
                          </div>
                          
                      </div>

                      <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Username</label>
                                  <input type="text" class="form-control" value="<?php echo $data_user->username; ?>" name="username" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Password</label>
                                  <input type="password" value="<?php echo $data_user->password; ?>" class="form-control" name="password" required="">
                                </div>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                  <label>Email</label>
                                  <input type="email" value="<?php echo $data_user->email; ?>" class="form-control" name="email" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Phone Number</label>
                                  <input type="Number" value="<?php echo $data_user->phone_number; ?>" class="form-control" name="phone" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <!-- <label>Role</label> -->
                                    <select required class="form-control" name="role" onchange="if(this.value == 3){$('#vendor_form').attr('hidden',false);$('#category').attr('hidden',true);}else if(this.value == 2){$('#category').attr('hidden',false);$('#vendor_form').attr('hidden',true);}else{$('#vendor_form').attr('hidden',true);$('#category').attr('hidden',true);}">
                                      <?php if(!isset($_GET['sup'])){ ?>
                                        <option value="">Select Role</option>
                                      <?php
                                    foreach($data_role->result() as $data){
                                      if($data->id != 3){
                                      ?>

                                        <option <?php echo ($data_user->role == $data->id)?"selected":""; ?> value="<?php echo $data->id; ?>"><?php echo $data->role ?></option>
                                          <?php
                                          }
                                        }
                                  }else{
                                    ?>
                                      <option value="3">Supplier</option>
                                    <?php
                                  }
                                    ?>
                                    </select>
                                </div>
                          </div>

                          
                      </div>

                      <div class="row" >
                        <div class="col-md-3" style="height: 70px" hidden="true" id="category">
                            <div class="form-group">
                                   <!--  <label>Category</label> -->
                                    <select class="form-control" name="category">
                                      <option value="">Select Category</option>
                                      <?php
                                    foreach($data_cat->result() as $data){
                                      ?>
                                    <option value="<?php echo $data->id; ?>"><?php echo $data->category ?></option>
                                      <?php
                                    }
                                    ?>
                                    </select>
                            </div>
                          </div>

                      </div>

                       <div class="row" <?php if(isset($_GET['sup'])){}else{echo 'hidden="true"'; } ?> id="vendor_form">

                          <div class="col-md-3">
                            <div class="form-group">
                                  <!-- <label>Vendor Code</label> -->
                                  <select class="form-control" name="vendor_code" onchange="vendor_complete(this.value)" style="color:black">
                                    <option value="" style="color:black">-- SELECT SUPPLIER --</option>
                                    <?php 
                                    foreach ($data_vendor->result() as $get) {
                                      ?>
                                      <option value="<?php echo $get->vendor_code;?>" style="color:black"><?php echo $get->vendor_code;?></option>
                                      <?php 
                                    }
                                    ?>
                                  </select>
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Name</label>
                                  <input type="text" id="vendor_name" disabled class="form-control" name="vendor_name">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Alias</label>
                                  <input type="text" id="vendor_alias" disabled class="form-control" name="vendor_alias">
                                </div>
                          </div>
                      </div>  
                      <button onclick="loading()" class="btn btn-primary">SUBMIT</button>                
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card ">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                <i class="fas fa-camera" style="font-size:30px"></i>
                </div>
                <h4 class="card-title">Add Foto</h4>
                </div>
                <div class="card-body ">
                  <div class="row">
                    <div class="col-12 text-center">
                      <?php 
                        $foto = base_url()."assets/upload/foto/".$data_user->username.$data_user->foto_type;
                        if(@getimagesize($foto)){
                           
                        }else{
                          $foto = base_url()."assets/images/dummy-profile.png";
                        }
                        ?>
                      <img id="foto" src="<?php echo $foto; ?>" onclick="$('#imagez').click()" onmouseout="$(this).css('opacit22,1),$('#add_plus').css('opacity',1)" onmouseover="$(this).css('opacity',0.5),$('#add_plus').css('opacity',0.5)" src="#" width="105" height="105" >
                      <input type="file" class="btn btn-primary" style="width: 220px" id="imagez" name="foto">
                    </div>
                  </div>                  
                </div>
            </div>
          </div>
        </div> 
          </div>
        </div>
      </div>
    </div>
</div>
 </form>