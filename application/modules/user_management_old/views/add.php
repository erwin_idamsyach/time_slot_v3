
<div class="content">
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9">
            <div class="card ">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                <i class="fas fa-user-plus" style="font-size:30px"></i>
                </div>
                <h4 class="card-title">Add User</h4>
                </div>
                <div class="card-body ">
                <form class="form-field" action="<?php echo base_url(); ?>user_management/add_action" method="post" enctype='multipart/form-data'>
                      <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Name</label>
                                  <input type="text" id="name" class="form-control" name="name" required="">
                                </div>
                          </div>
                          
                      </div>

                      <div class="row">
                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Username</label>
                                  <input type="text" id="username" class="form-control" name="username" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Password</label>
                                  <input type="password" id="password" class="form-control" name="password" required="">
                                </div>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                  <label>Email</label>
                                  <input type="email" id="email" class="form-control" name="email" required="">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Phone Number</label>
                                  <input type="text" id="phone" name="phone" required="" class="form-control" maxlength="13" oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="phone"/>
                                  
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <!-- <label>Role</label> -->
                                    <select required class="form-control" id="role" name="role" onchange="if(this.value == 3){$('#vendor_form').attr('hidden',false);$('#category').attr('hidden',true);}else if(this.value == 2){$('#category').attr('hidden',false);$('#vendor_form').attr('hidden',true);}else{$('#vendor_form').attr('hidden',true);$('#category').attr('hidden',true);}">
                                      <?php if(!isset($_GET['sup'])){ ?>
                                        <option value="">Select Role</option>
                                      <?php
                                    foreach($data_role->result() as $data){
                                      if($data->id != 3){
                                      ?>

                                        <option <?php if(isset($_GET['sup']) AND $data->id == '3'){echo "selected";} ?> value="<?php echo $data->id; ?>"><?php echo $data->role ?></option>
                                          <?php
                                          }
                                        }
                                  }else{
                                    ?>
                                      <option value="3">Supplier</option>
                                    <?php
                                  }
                                    ?>
                                    </select>
                                </div>
                          </div>

                          
                      </div>

                      <div class="row" >
                        <div class="col-md-3" style="height: 70px" hidden="true" id="category">
                            <div class="form-group">
                                   <!--  <label>Category</label> -->
                                    <select class="form-control" name="category">
                                      <option value="">Select Category</option>
                                      <?php
                                    foreach($data_cat->result() as $data){
                                      ?>
                                    <option value="<?php echo $data->id; ?>"><?php echo $data->category ?></option>
                                      <?php
                                    }
                                    ?>
                                    </select>
                            </div>
                          </div>

                      </div>

                       <div class="row" <?php if(isset($_GET['sup'])){}else{echo 'hidden="true"'; } ?> id="vendor_form">

                          <div class="col-md-3">
                            <div class="form-group">
                                  <!-- <label>Vendor Code</label> -->
                                  <select class="form-control" name="vendor_code" onchange="vendor_complete(this.value)" style="color:black">
                                    <option value="" style="color:black">-- SELECT SUPPLIER --</option>
                                    <?php 
                                    foreach ($data_vendor->result() as $get) {
                                      ?>
                                      <option value="<?php echo $get->vendor_code;?>" style="color:black"><?php echo $get->vendor_code;?></option>
                                      <?php 
                                    }
                                    ?>
                                  </select>
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Name</label>
                                  <input type="text" id="vendor_name" disabled class="form-control" name="vendor_name">
                                </div>
                          </div>

                          <div class="col-md-3">
                            <div class="form-group">
                                  <label>Vendor Alias</label>
                                  <input type="text" id="vendor_alias" disabled class="form-control" name="vendor_alias">
                                </div>
                          </div>

                          
                      </div>
                      <div class="row">
                    <div class="col-md-3">
                      <button type="submit" id="btn_submit" hidden>submit</button>
                      <button onclick="validateForm_add()" type="button" id="submit_form" class="btn btn-primary">SUBMIT</button>
                    </div>
                </div>
                                    
                </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card ">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                <i class="fas fa-camera" style="font-size:30px"></i>
                </div>
                <h4 class="card-title">Add Foto</h4>
                </div>
                <div class="card-body ">
                  <div class="row">
                    <div class="col-12 text-center">
                      <img id="foto" src="<?php echo base_url() ?>assets/images/dummy-profile.png" onclick="$('#imagez').click()" onmouseout="$(this).css('opacit22,1),$('#add_plus').css('opacity',1)" onmouseover="$(this).css('opacity',0.5),$('#add_plus').css('opacity',0.5)" src="#" width="105" height="105" >
                      <input type="file" class="btn btn-primary" style="width: 220px" id="imagez" name="foto">
                    </div>
                  </div>
                                    
                </div>
            </div>
          </div>
        </div>
        </form> 

        

                
              </div>
        </div>
      </div>
    </div>
</div>
 