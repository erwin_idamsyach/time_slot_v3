<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i class="fas fa-user-cog" style="font-size:30px"></i>
                    </div>
                    <h4 class="card-title">Edit Vendor</h4>
                    </div>
                    <div class="card-body ">

          <form action="<?php echo base_url(); ?>user_management/vendor_update" method="post">
          <div class="row">
            <div class="col-md-2">
              <div class="form-group">
                <label>Vendor Code</label>
                <input type="text" hidden name="id" value="<?php echo $data->id; ?>">
                <input required="true" value="<?php echo $data->vendor_code; ?>" type="text" class="form-control" name="vendor_code">
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label>Vendor Name</label>
                <input required="true" type="text" value="<?php echo $data->vendor_name; ?>" class="form-control" name="vendor_name">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>Vendor Alias</label>
                <input required="true" type="text" value="<?php echo $data->vendor_alias; ?>" class="form-control" name="vendor_alias">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label>Phone Number</label>
                <input required="true" type="text" value="<?php echo $data->phone_number; ?>" class="form-control" name="phone">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <select name="full_capacity" class="form-control">
                  <option>-- Full Capacity --</option>
                  <option <?php if($data->full_capacity == 4){echo "selected";} ?> value="4">4</option>
                  <option <?php if($data->full_capacity == 8){echo "selected";} ?> value="8">8</option>
                  <option <?php if($data->full_capacity == 16){echo "selected";} ?> value="16">16</option>
                  <option <?php if($data->full_capacity == 32){echo "selected";} ?> value="32">32</option>
                </select>
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                
                <select required="true" name="vendor_type" class="form-control">
                  <option>-- VENDOR TYPE --</option>
                      <option <?php if($data->vendor_type == "RM"){echo "selected";} ?> value="RM"> RM </option>
                      <option <?php if($data->vendor_type == "PM"){echo "selected";} ?> value="PM"> PM </option>
                    </select>
              </div>
            </div>
  
          </div>
          <br>
          <div class="row">
            <div class="col-md-12">
              <button required="true" type="text" class="btn btn-primary"> Submit </button>
            </div>
          </div>

          </form>
        </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

