<?php

class User_Management extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		isLogin();
		if($this->session->userdata('sess_role_no') != 1){
			isRole();
		}
	}

	public function index(){
		$getUser_uni = $this->db->query("SELECT a.id,a.nama,a.email,a.phone_number, a.username, a.vendor_code, b.role, c.category FROM ms_user a INNER JOIN ms_role b ON a.role = b.id
			LEFT JOIN ms_category c ON a.category = c.id WHERE a.role != 3 ");
		$getUser_sup = $this->db->query("SELECT a.id,a.nama,a.email,a.phone_number, a.username, a.vendor_code, b.role, c.vendor_name, c.vendor_alias, c.full_capacity FROM ms_user a INNER JOIN ms_role b ON a.role = b.id
			 LEFT JOIN skin_master.ms_supplier c ON a.vendor_Code = c.vendor_code WHERE a.role = 3 ");
		$data['data_user_uni'] = $getUser_uni;
		$data['data_user_sup'] = $getUser_sup;
		getHTML('user_management/index', $data);
	}

	public function authorization(){
		getHTML('user_management/authorization');
	}

	public function form_authorization(){
		$role = $this->input->post("role");
		$getMenu = $this->db->query("SELECT * FROM ms_menu")->result();
		$getUserMenu = $this->db->query("SELECT menu FROM ms_role WHERE id = '$role' ")->row()->menu;
		$get_menu_role = explode(",",$getUserMenu);
		$data['data_menu'] = $getMenu;
		$data['data_role_menu'] = $get_menu_role; 
		$data['role'] = $role;

		$this->load->view('user_management/form_authorization', $data);
	}

	public function add(){
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$getVendor = $this->db->query("SELECT vendor_code FROM skin_master.ms_supplier");
		$data['data_role'] = $getRole;
		$data['data_vendor'] = $getVendor;
		$data['data_cat']  = $getCate;
		getHTML('user_management/add', $data);
	}

	public function add_supplier(){
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$data['data_role'] = $getRole;
		$data['data_cat']  = $getCate;
		getHTML('user_management/add_vendor', $data);
	}

	public function select_vendor(){
		$vendor_code = $this->input->get('vendor_code');
		$data = $this->db->query("SELECT vendor_name FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->row()->vendor_name;

		echo json_encode($data);
	}

	public function edit(){
		$getMenu = $this->db->query("SELECT * FROM ms_menu");
	 	$id = $_GET['id'];
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$getData = $this->db->query("SELECT a.*, b.vendor_alias, c.role as rolez, b.vendor_name FROM ms_user AS a LEFT JOIN skin_master.ms_supplier AS b ON b.vendor_code = a.vendor_code INNER JOIN ms_role AS c ON a.role = c.id where a.id='$id' ")->row();
		$getVendor = $this->db->query("SELECT vendor_code FROM skin_master.ms_supplier");
		$getUserMenu = $this->db->query("SELECT menu FROM ms_role AS a INNER JOIN ms_user AS b ON a.id = b.role WHERE b.id = '$id' ")->row()->menu;
		$get_menu = explode(",",$getUserMenu);
		$data['data_role'] = $getRole;
		$data['data_vendor'] = $getVendor;
		$data['data_cat']  = $getCate;
		$data['data_user'] = $getData;
		$data['data_menu'] = $getMenu;
		$data['data_user_menu'] = $get_menu;
		getHTML('user_management/edit', $data);
	}

	public function add_action(){
		$name = $this->input->post('name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email		= $this->input->post('email');
		$phone_number = $this->input->post('phone');
		$role = $this->input->post('role');

		$vendor = '';
		if($role == 2){$category = $this->input->post('category');}else{$category = "";}
		if($role == 3){$vendor_code = $this->input->post('vendor_code');}else{$vendor_code = "";}
		$vendor = $vendor_code;

		$config['allowed_types'] = 'jpg|jpeg|png|bmp|gif|JPG|PNG';
	  	$config['max_size'] = 0;
		$config['upload_path'] = 'assets/upload/foto';
		// $receipt = $this->db->query("SELECT receipt FROM tb_delivery_detail WHERE id_schedule_group = '$id' ")->row()->receipt;
  		$config['file_name'] = $username;
  		$foto = '';
  		$data_foto = '';
  		$this->load->library('upload', $config);
	  	if ($this->upload->do_upload('foto')) {
	  		$data_foto = $this->upload->data('file_ext');
		  	$foto = $this->upload->data('file_name');
		  	
		}else{
		  	print_r($this->upload->display_errors());
		}

		$data = array(
			'nama' 			=> $name,
			"username" 		=> $username,
			"password" 		=> $password,
			"role" 			=> $role,
			"category" 		=> $category,
			"vendor_code" 	=> $vendor_code,
			"email"			=> $email,
			"foto_type"		=> $data_foto,
			"phone_number"  => $phone_number,
			"created_at"	=> date('Y-m-d H:i:s')
			
		);
		$this->db->insert("ms_user", $data);
		$getLastId = $this->db->query("SELECT id FROM ms_user ORDER BY id DESC")->row()->id;
		
		$this->db->insert("tb_data_checker", array("id_user" => $getLastId) );

		$last_id = $this->db->query("SELECT id FROM ms_user ORDER BY id DESC")->row()->id;
		$role_user = $this->db->query("SELECT b.role FROM ms_user AS a INNER JOIN ms_role AS b ON a.role = b.id WHERE a.id = '$last_id' ")->row()->role;
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Add",
			"table_join"	=> 'ms_user',
			"id_join"		=> 	$last_id,
			"description" 	=> "New User".$role_user.",Name :".$name." Username : ".$username.$vendor." Email :".$email." Phone Number :".$phone_number,
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);

		$this->session->set_flashdata("SUCCESS", "User :".$username." berhasil ditambahkan!");
		redirect('user_management');
	}

	public function add_role(){
		$role_name = $this->input->post("role_name");
		$this->db->insert("ms_role", array("role" => $role_name));
		return true;
	}

	public function delete_role(){
		$role_id = $this->input->post('role');
		$this->db->query("DELETE FROM ms_role WHERE id = '$role_id' ");
		return true;
	}

	public function update_authorization_user(){
		$getMenu = $this->input->post('menu');
		$role = $this->input->post('role_id');
		if($getMenu == null){
			$update = $this->db->query("UPDATE ms_role SET menu = '' WHERE id = '$role' ");
		}else{
			$menu = implode(",",$getMenu);
			$update = $this->db->query("UPDATE ms_role SET menu = '$menu' WHERE id = '$role' ");
		}
		
		if($update){
			getHTML('user_management/authorization');
		}else{
			echo "failed";
		}
		
	}

	public function delete_authorization_user(){

	}

	public function ajax_role(){
		$getRole = $this->db->query("SELECt * FROM ms_role")->result();
		echo json_encode($getRole);
	}

	public function add_action_vendor(){
		
		$vendor_code = $this->input->post('vendor_code');
		$capacity = $this->input->post('capacity');
		$vendor_name = $this->input->post('vendor_name');
		$vendor_alias = $this->input->post('vendor_alias');
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$vendor_type = $this->input->post('vendor_type');

		$data = array(
			"vendor_code" 		=> $vendor_code,
			"vendor_name"		=> $vendor_name,
			"vendor_alias"		=> $vendor_alias,
			"email"				=> $email,
			"phone_number"		=> $phone,
			"full_capacity"		=> $capacity,
			"vendor_type"		=> $vendor_type
		);
		
		$this->db->insert('skin_master.ms_supplier', $data);

		$last_id = $this->db->query("SELECT id FROM skin_master.ms_supplier ORDER BY id DESC")->row()->id;
		$history = array(
				"date"			=> date('Y-m-d'),
				"time"			=> date('H:i:s'),
				"action"		=> "Add",
				"by_who"		=> $this->session->userdata('sess_id'),
				"table_join" 	=> "skin_master.ms_supplier",
				"id_join"		=> $last_id,
				"description"	=> "Add new Vendor, Vendor Code :".$vendor_code." Vendor Name :".$vendor_name."(".$vendor_alias.")",
				"value"			=> '',
				"value2"		=> '',
				"author"		=> 1
			);
		$this->db->insert('tb_history',$history);

		redirect('user_management');
	}

	public function edit_action(){
			
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email		= $this->input->post('email');
		$phone_number = $this->input->post('phone');
		$role = $this->input->post('role');
		if($role == 2){$category = $this->input->post('category');}else{$category = "";}
		if($role == 3){$vendor_code = $this->input->post('vendor_code');}else{$vendor_code = "";}
		$vendor = $vendor_code;
		$data = array(	
			'nama' 			=> $name,
			"username" 		=> $username,
			"password" 		=> $password,
			"role" 			=> $role,
			"category" 		=> $category,
			"vendor_code" 	=> $vendor_code,
			"email"			=> $email,
			"phone_number"	=> $phone_number,
			"update_at"	=> date('Y-m-d H:i:s')

		);
		$getData = $this->db->query("SELECT id, foto_type FROM ms_user WHERE id='$id' ")->row();
		$file = "assets/upload/foto/".$username.$getData->foto_type;
		if($_FILES['foto']['size'] != 0){

			if (!unlink($file))
			  {
			  echo ("Error deleting $file");
			  }
			else
			  {
			  echo ("Deleted $file");
			  }
		}
		$config['allowed_types'] = 'jpg|jpeg|png|bmp|gif|JPG|PNG';
	  	$config['max_size'] = 0;
		$config['upload_path'] = 'assets/upload/foto';
		// $receipt = $this->db->query("SELECT receipt FROM tb_delivery_detail WHERE id_schedule_group = '$id' ")->row()->receipt;
  		$config['file_name'] = $username;
  		$foto = '';
  		$data_foto = '';
  		$this->load->library('upload', $config);
	  	if ($this->upload->do_upload('foto')) {
	  		$data_foto = $this->upload->data('file_ext');
		  	$foto = $this->upload->data('file_name');
		  	
		}else{
		  	print_r($this->upload->display_errors());
		}

		$data = array(
			'nama' 			=> $name,
			"username" 		=> $username,
			"password" 		=> $password,
			"role" 			=> $role,
			"category" 		=> $category,
			"vendor_code" 	=> $vendor_code,
			"email"			=> $email,
			"phone_number"  => $phone_number,
			"created_at"	=> date('Y-m-d H:i:s')
			
		);
		if($foto != ''){
			$data['foto_type'] = $data_foto;	
		}
		$this->db->where('id', $id);
		$this->db->update("ms_user", $data);

		$last_id = $getData->id;
		$role_user = $this->db->query("SELECT b.role FROM ms_user AS a INNER JOIN ms_role AS b ON a.role = b.id WHERE a.id = '$last_id' ")->row()->role;
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Update",
			"table_join"	=> 'ms_user',
			"id_join"		=> 	$last_id,
			"description" 	=> "User ".$role_user.",Name :".$name." Username : ".$username.$vendor." Email :".$email." Phone Number :".$phone_number,
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);

		$this->session->set_flashdata("UPDATED", "User ".$username." berhasil diedit!");
		redirect('user_management');
	}

	public function delete(){
		$id = $this->input->get('id');
		$user = $this->db->query("SELECT a.*, b.role as role_name FROM ms_user as a INNER JOIN ms_role as b WHERE a.id ='$id' ")->row();
		$role_user = $user->role_name;
		$name = $user->nama;
		$username = $user->username;
		$email = $user->email;
		$phone_number = $user->phone_number;
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Delete",
			"table_join"	=> 'ms_user',
			"id_join"		=> 	$id,
			"description" 	=> ", Nama :".$name.", Username : ".$username.", Email :".$email.", Phone Number:".$phone_number,
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);
		
		$this->db->where('id', $id);
		$this->db->delete("ms_user");

		$this->db->where('id_user', $id);
		$this->db->delete('tb_data_checker');
		$this->session->set_flashdata("DELETED", "User :".$username." berhasil dihapus!");
		redirect('user_management');
	}

	public function ajax_vendor(){
		$vendor_code = $this->input->get('vendor_code');
		$data = $this->db->query("SELECT vendor_name, vendor_alias FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->result();
		echo json_encode($data);
	}

	public function delete_vendor(){
		$id = $this->input->post('id');
		$data = $this->db->query("SELECT vendor_code, vendor_name, vendor_alias FROM skin_master.ms_supplier WHERE id = '$id' ")->row();
		
		$vendor_code = $data->vendor_code;
		$vendor_name = $data->vendor_name;
		$vendor_alias = $data->vendor_alias;
		$this->db->query("DELETE FROM skin_master.ms_supplier WHERE id='$id'  ");
		$history = Array(
			"by_who"		=> $this->session->userdata('sess_id'),
			"date"			=> Date('Y-m-d'),
			"time"			=> Date('H:i:s'),
			"action"		=> "Delete",
			"table_join"	=> 'ms_supplier',
			"id_join"		=> 	$id,
			"description"	=> "Delete Vendor, Vendor Code :".$vendor_code." Vendor Name :".$vendor_name."(".$vendor_alias.")",
			"select_join"	=> "username",
			"author"		=> 1
		);
		$this->db->insert('tb_history',$history);

	}

	public function vendor_edit(){
		$id = $this->input->get('id');
		$getData = $this->db->query("SELECT * FROM skin_master.ms_supplier WHERE id = '$id' ")->row();
		$getRole = $this->db->query("SELECT * FROM ms_role");
		$getCate = $this->db->query("SELECT * FROM ms_category");
		$data['data_role'] = $getRole;
		$data['data_cat']  = $getCate;
		$data['data'] = $getData;
		getHTML('user_management/edit_vendor', $data);
	}

	public function vendor_update(){
		$id= $this->input->post('id');
		$vendor_code = $this->input->post('vendor_code');
		$vendor_name = $this->input->post('vendor_name');
		$vendor_alias = $this->input->post('vendor_alias');
		$phone = $this->input->post('phone');
		$capacity = $this->input->post('capacity');
		$vendor_type = $this->input->post('vendor_type');

		$data = array(
			"vendor_code" 		=> $vendor_code,
			"vendor_name"		=> $vendor_name,
			"vendor_alias"		=> $vendor_alias,
			"phone_number"		=> $phone,
			"full_capacity"			=> $capacity,
			"vendor_type"		=> $vendor_type
		);

		$this->db->where('id', $id);
		$this->db->update('skin_master.ms_supplier', $data);
		$last_id = $this->db->query("SELECT id FROM skin_master.ms_supplier WHERE id='$id' ")->row()->id;

		$history = array(
				"date"			=> date('Y-m-d'),
				"time"			=> date('H:i:s'),
				"action"		=> "Update",
				"by_who"		=> $this->session->userdata('sess_id'),
				"table_join" 	=> "skin_master.ms_supplier",
				"id_join"		=> $last_id,
				"description"	=> "Update new Vendor, V.Code :".$vendor_code." V.Name :".$vendor_name."(".$vendor_alias.")",
				"value"			=> '',
				"value2"		=> '',
				"author"		=> 1
			);
		$this->db->insert('tb_history',$history);
		redirect('user_management');
	}

	public function vendor_list(){
		$this->load->view('user_management/vendor_list');
		return true;
	}

}

?>