<?php

class Rds_Model extends CI_Model{

	public function Rds_Data(){
		if($this->session->userdata('sess_role_no') != 3){
			$getData = $this->db->query("SELECT * FROM tb_schedule_detail as tsd 
				INNER JOIN tb_scheduler as ts ON tsd.schedule_number = ts.schedule_number 
				INNER JOIN tb_rds_detail as trd ON ts.id_schedule = trd.id
				INNER JOIN skin_master.ms_supplier as ms ON tsd.vendor_code = ms.vendor_code
				INNER JOIN ms_time_slot as mts ON tsd.id_time_slot = mts.id");
		}else{
			$vendor_code = $this->session->userdata('sess_vendor_code');
			$getData = $this->db->query("SELECT * FROM tb_schedule_detail as tsd 
				INNER JOIN tb_scheduler as ts ON tsd.schedule_number = ts.schedule_number 
				INNER JOIN tb_rds_detail as trd ON ts.id_schedule = trd.id
				INNER JOIN skin_master.ms_supplier as ms ON tsd.vendor_code = ms.vendor_code WHERE vendor_code = '$vendor_code' ");
		}
		
		return $getData;
	}
}

?>