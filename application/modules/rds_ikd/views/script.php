<script type="text/javascript">
	$(document).ready(function(){
		var link = "<?php echo $this->uri->segment(2); ?>";
		if(link == "view_schedule"){
			callOffPallet();
		}

		$('.datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }
      });
		 var table = $('datatables').DataTable();
	});
</script>