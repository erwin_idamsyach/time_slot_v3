<?php

class security extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
	}

	public function index(){
		$username = $this->db->escape_str("security");
		$password = $this->db->escape_str("password");
		$pass_md = $password;

		$data = $this->db->query("SELECT ms_user.*, ms_role.role as ROLE_NAME, c.vendor_name as vendor_names, c.vendor_alias, c.email, c.vendor_type as vendor_type, c.phone_number as vendor_phone, c.vendor_category as vendor_category FROM ms_user LEFT JOIN ms_role ON ms_user.role = ms_role.id LEFT JOIN skin_master.ms_supplier as c ON ms_user.vendor_code  = c.vendor_code WHERE username='$username' AND password='$pass_md'")->row();

		$nama = $data->nama;
		$username = $data->username;
		$role_no = $data->role;
		$role_name = $data->ROLE_NAME;
		$vendor_code = $data->vendor_code;
		$vendor_name = $data->nama;

		$array = array(
				"sess_id" => $data->id,
				"sess_nama" => $nama,
				"sess_user" => $username,
				"sess_role_no" => $role_no,
				"sess_role_name" => $role_name,
				"sess_vendor_code" => $vendor_code,
				"sess_vendor_name" => $vendor_name,
				"sess_vendor_alias" => $data->vendor_alias,
				"sess_foto"			=> $data->foto_type,
				"sess_vendor_type"  => $data->vendor_type
		);	

		$this->session->set_userdata($array);
		redirect("sopir_management");
	}
}