<?php

class Login extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		date_default_timezone_set("Asia/Bangkok");
		
	}

	public function index(){
		$date_now = date('Y-m-d');
		$data = array();
		$data['DM'] = $this->db->query("SELECT start_time, end_time, id FROM ms_time_slot WHERE shift ='DM' ORDER BY ordering ");
		$data['DP'] = $this->db->query("SELECT start_time, end_time, id FROM ms_time_slot WHERE shift ='DP' ORDER BY ordering ");
		$data['DS'] = $this->db->query("SELECT start_time, end_time, id FROM ms_time_slot WHERE shift ='DS' ORDER BY ordering ");
		
		$this->load->view('index',$data);
	}

	public function function_login(){
		$username = $this->db->escape_str($this->input->post('username'));
		$password = $this->db->escape_str($this->input->post('password'));
		$pass_md  = $password;
		/*echo $pass_md; die();*/
		$arr 	  = array();

		$cek_login = $this->db->query("SELECT ms_user.*, ms_role.role as ROLE_NAME, c.vendor_name as vendor_names, c.vendor_alias, c.email, c.vendor_type as vendor_type, c.phone_number as vendor_phone, c.vendor_category as vendor_category FROM ms_user LEFT JOIN ms_role ON ms_user.role = ms_role.id LEFT JOIN skin_master.ms_supplier as c ON ms_user.vendor_code  = c.vendor_code WHERE username='$username' AND password='$pass_md'");
		
		if($cek_login->num_rows() == 1){
			
			$cek_role = $cek_login->row()->role;
			if($cek_role == '' || $cek_role == 0){
				$this->session->set_flashdata('STATUS', "ROLE");
				redirect('login');
			}

			$data = $cek_login->row();
			$nama = $data->nama;
			$username = $data->username;
			$role_no = $data->role;
			$role_name = $data->ROLE_NAME;
			$vendor_code = $data->vendor_code;
			$vendor_name = $data->nama;

			$array = array(
				"sess_id" => $data->id,
				"sess_nama" => $nama,
				"sess_user" => $username,
				"sess_role_no" => $role_no,
				"sess_role_name" => $role_name,
				"sess_vendor_code" => $vendor_code,
				"sess_vendor_name" => $vendor_name,
				"sess_vendor_alias" => $data->vendor_alias,
				"sess_foto"			=> $data->foto_type,
				"sess_vendor_type"  => $data->vendor_type
				
			);
			$this->session->set_userdata($array);
			date_default_timezone_set("Asia/Bangkok");
			$date = date('Y-m-d');
			if($date == "2019-12-30" || $date == "2019-12-31"){
				$year = date('Y')+1;
				$week = date('W');
			}else{
				$year = date('Y');
				$week = date('W');
			}
			
          	$dt = date('Y-m-d');

          	$getUserMenu = $this->db->query("SELECT a.menu FROM ms_role As a INNER JOIN ms_user AS b ON a.id = b.role WHERE b.id = '$data->id' ")->row()->menu;
            $get_menus = explode(",",$getUserMenu);
            $menuz = $get_menus[0];
            $get_menuz = $this->db->query("SELECT a.link from ms_menu as a inner join ms_sub_menu as b on a.id = b.id_menu where b.id = '$menuz'")->row()->link;
            $menu = $get_menuz.'?year='.$year.'&week='.$week."&date=".$dt;
            
			redirect($menu);
			
		}else{
			$this->session->set_flashdata('STATUS', "ERROR");
			redirect('login');
		}

	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}

?>