<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Time Slot Booking
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fa/fonts/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo base_url() ?>assets/plugins/material_dashboard/material-dashboard.css" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?php echo base_url() ?>assets/plugins/demo/demo.css" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fa/css/fa.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/spin.css">

<style type="text/css">
  .card .card-header-rose .card-icon, .card .card-header-rose .card-text, .card .card-header-rose:not(.card-header-icon):not(.card-header-text), .card.bg-rose, .card.card-rotate.bg-rose .front, .card.card-rotate.bg-rose .back {
    background: linear-gradient(60deg, #1e5dad, #060621);
}

  body{
      background: url(assets/images/bglogin.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  }

.fa-stack{
  margin:0px!important;
  padding:0px!important;
  font-size:7.5px!important;
}

table {
    border-collapse: separate;
    opacity: 0.75;
}

td{
  height:25px!important;
  padding:0px;
  margin:0px;
}

.wrapper.wrapper-full-page {
    height: 0;
    min-height: 100vh;
}

table {
    border-collapse: separate;
    
}

.login-page .container {
    padding-top: 0px !important;
}

</style>
</head>
<?php 
date_default_timezone_set("Asia/Bangkok");
$curr_date = date('Y-m-d'); ?>
<body class="">

  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page" filter-color="black" style="background-image: url('../../assets/img/login.jpg'); background-size: cover; background-position: top center;">
      <!--   you can change the color of the filter page using: data-color="blue | purple | white | orange | red | rose " -->
      <div class="container" >
        <div class="row">
          <div class="col-md-5">
            <table style="color:black;font-weight: 800">
              <tr>
                <td align="center" width="150"><?php echo date('D'); ?></td>
                <td rowspan="2" style="justify-content: center;font-size:40px;"><p id='jam'></p></td>
              </tr>
              <tr>
                <td align="center"><?php echo date('d m Y'); ?></td>

              </tr>
            </table>
          </div>
        </div>

        <div class="row">
          <div class="col-md-5" style="margin-top:0px">
                  <div class="container-fluid" style="margin-left:-15px">
                    
                    <div class="row" style="margin-bottom: 10px">
                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table style="width: 100%;font-size:12px;background-color:#333333" border="1"  style="background-color:#333333;">
                            <tr>
                              <td colspan="4" style="padding:0px;margin:0px;height: 30px;background-color:#333333;" align="center">DINAS MALAM</td>
                            </tr>
                            <?php 
                              foreach ($DM->result() as $get) {
                              $total_truck = 0;
                              $truck = $this->db->query("SELECT
                                                          a.id,
                                                          a.dock_id,
                                                          a.status,
                                                          d.gr_material_doc
                                                        FROM
                                                          tb_schedule_detail AS a
                                                        INNER JOIN tb_scheduler AS b ON a.schedule_number = b.schedule_number
                                                        INNER JOIN ms_time_slot AS c ON a.id_time_slot = c.id
                                                        INNER JOIN tb_delivery_detail AS d ON a.schedule_number = d.id_schedule_group
                                                        WHERE
                                                          a.rdd = '$curr_date'
                                                        AND a.id_time_slot = '$get->id'
                                                        GROUP BY
                                                          b.schedule_number");
                              ?>

                            <tr>
                              <td width="150" align="center"><b><?php echo $get->start_time." - ".$get->end_time; ?></b></td>
                              <?php 
                                  foreach($truck->result() as $truck){
                                    if($truck->status > 0){
                                    ?>
                                  <td bgcolor="white" align="center" width="85"><?php echo truckIcon($truck->dock_id, $truck->status,'','','','','',$truck->gr_material_doc); ?></td>
                                    <?php
                                    $total_truck++; 
                                    }
                                  }
                              ?>

                              <?php while($total_truck < 5){
                                ?>
                                 <td bgcolor="white" align="center" width="85"></td>
                              <?php
                              $total_truck++; 
                                  } 
                              ?>

                            </tr>
                            <?php 
                            } ?>
                          </table>
                           </div>
                      </div>
                    </div>

                    <div class="row" style="margin-bottom: 10px">
                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table style="width: 100%;font-size:12px;background-color:#333333" border="1"  style="background-color:#333333;">
                            <tr>
                              <td colspan="4" style="padding:0px;margin:0px;height: 30px;background-color:#333333;" align="center">DINAS PAGI</td>
                            </tr>
                            <?php 
                              foreach ($DP->result() as $get) {
                                $total_truck = 0;
                              $truck = $this->db->query("SELECT
                                                      a.id,
                                                      a.dock_id,
                                                      a.status,
                                                      d.gr_material_doc
                                                    FROM
                                                      tb_schedule_detail AS a
                                                    INNER JOIN tb_scheduler AS b ON a.schedule_number = b.schedule_number
                                                    INNER JOIN ms_time_slot AS c ON a.id_time_slot = c.id
                                                    INNER JOIN tb_delivery_detail AS d ON a.schedule_number = d.id_schedule_group
                                                    WHERE
                                                      a.rdd = '$curr_date'
                                                    AND a.id_time_slot = '$get->id'
                                                    GROUP BY
                                                      b.schedule_number");
                              ?>

                            <tr>
                              <td width="150" align="center"><b><?php echo $get->start_time." - ".$get->end_time; ?></b></td>
                              <?php 
                                  foreach($truck->result() as $truck){
                                    if($truck->status > 0){
                                    ?>
                                  <td bgcolor="white" align="center" width="85"><?php echo truckIcon($truck->dock_id, $truck->status,'','','','','',$truck->gr_material_doc); ?></td>
                                    <?php
                                    $total_truck++; 
                                    }
                                  }
                              ?>

                              <?php while($total_truck < 5){
                                ?>
                                 <td bgcolor="white" align="center" width="85"></td>
                              <?php
                              $total_truck++; 
                                  } 
                              ?>

                            </tr>
                            <?php 
                            } ?>
                          </table>
                           </div>
                      </div>
                    </div>

                    <div class="row" style="margin-bottom: 10px">
                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table style="width: 100%;font-size:12px;background-color:#333333" border="1"  style="background-color:#333333;">
                            <tr>
                              <td colspan="4" style="padding:0px;margin:0px;height: 30px;background-color:#333333;" align="center">DINAS SIANG</td>
                            </tr>
                            <?php 
                              foreach ($DS->result() as $get) {
                                $total_truck = 0;
                              $truck = $this->db->query("SELECT
                                                          a.id,
                                                          a.dock_id,
                                                          a.status,
                                                          d.gr_material_doc
                                                        FROM
                                                          tb_schedule_detail AS a
                                                        INNER JOIN tb_scheduler AS b ON a.schedule_number = b.schedule_number
                                                        INNER JOIN ms_time_slot AS c ON a.id_time_slot = c.id
                                                        INNER JOIN tb_delivery_detail AS d ON a.schedule_number = d.id_schedule_group
                                                        WHERE
                                                          a.rdd = '$curr_date'
                                                        AND a.id_time_slot = '$get->id'
                                                        GROUP BY
                                                          b.schedule_number");
                              ?>

                            <tr>
                              <td width="150" align="center"><b><?php echo $get->start_time." - ".$get->end_time; ?></b></td>
                              <?php 
                                  foreach($truck->result() as $truck){
                                  if($truck->status > 0){
                                    ?>
                                  <td bgcolor="white" align="center" width="85"><?php echo truckIcon($truck->dock_id, $truck->status,'','','','','',$truck->gr_material_doc); ?></td>
                                    <?php
                                    $total_truck++; 
                                    }
                                  }
                              ?>

                              <?php while($total_truck < 5){
                                ?>
                                 <td bgcolor="white" align="center" width="85"></td>
                              <?php
                              $total_truck++; 
                                  } 
                              ?>

                            </tr>
                            <?php 
                            } ?>
                          </table>
                           </div>
                      </div>
                    </div>
                  </div>

          </div>
          <div class="col">
          </div>

          <div class="col-md-4" style="margin-top:200px">

           <form action="<?php echo base_url(); ?>login/function_login" method="post">
              <div class="card card-login card-hidden" style="background-color: black;opacity: 0.8">
                <div class="card-header card-header-rose text-center">
                 
                  <h4 class="card-title">Login</h4>
                </div>
                <div class="card-body ">
                  <p style="font-size:15px;color:white;font-weight: 800" class="card-description text-center">Welcome To Time Slot Booking</p>
                  <?php
            if($this->session->flashdata('STATUS') == "ERROR"){
              ?>
            <div class="alert alert-error" align="center">
              <b style="color:red;margin:0px;padding:0px">Username atau Password salah!</b>
            </div>
              <?php
            }
            ?>

            <?php
            if($this->session->flashdata('STATUS') == "ROLE"){
              ?>
            <div class="alert alert-error" align="center">
              <b style="color:red;margin:0px;padding:0px">Role Username tidak terdaftar silahkan contact Unilever SKIN!</b>
            </div>
              <?php
            }
            ?>


                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-user-circle" style="font-size:25px"></i>
                        </span>
                      </div>
                      <input type="text" style="color:white" name="username" class="form-control" placeholder="First Name...">
                    </div>
                  </span>
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="fas fa-lock" style="font-size:25px"></i>
                        </span>
                      </div>
                      <input type="password" name="password" class="form-control" placeholder="Password..." style="color:white">
                    </div>
                  </span>
                </div>
                <div class="card-footer justify-content-center">
                  <button type="submit" onclick="loading()" class="btn btn-rose btn-link btn-lg">Login</button>
                  
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!--   Core JS Files   -->
 <script src="<?php echo base_url() ?>assets/plugins/jQuery/jquery.min.js"></script>
  <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/popper.min.js"></script>
  <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap-material-design.min.js"></script>
  <script src="<?php echo base_url() ?>assets/plugins/moment/moment.min.js"></script>
  <script src="<?php echo base_url() ?>assets/plugins/scroll/perfect_scroll.js"></script>
  <script src="<?php echo base_url() ?>assets/dist/js/spin.umd.js"></script>
  <!--  Google Maps Plugin    -->
  <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/sweet_alert/sweetalert2.min.js"></script>
  <!-- Chartist JS -->
  <!--  Notifications Plugin    -->
  <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo base_url() ?>assets/plugins/material_dashboard/dashboard.js" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo base_url() ?>assets/plugins/demo/demo.js"></script>
  <script>
    
    $(document).ready(function() {

      $().ready(function() {

        $sidebar = $('.sidebar');

        $sidebar_img_container = $sidebar.find('.sidebar-background');

        $full_page = $('.full-page');

        $sidebar_responsive = $('body > .navbar-collapse');

        window_width = $(window).width();

        fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

        if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
          if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
            $('.fixed-plugin .dropdown').addClass('open');
          }

        }

        $('.fixed-plugin a').click(function(event) {
          // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
          if ($(this).hasClass('switch-trigger')) {
            if (event.stopPropagation) {
              event.stopPropagation();
            } else if (window.event) {
              window.event.cancelBubble = true;
            }
          }
        });

        $('.fixed-plugin .active-color span').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-color', new_color);
          }

          if ($full_page.length != 0) {
            $full_page.attr('filter-color', new_color);
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.attr('data-color', new_color);
          }
        });

        $('.fixed-plugin .background-color .badge').click(function() {
          $(this).siblings().removeClass('active');
          $(this).addClass('active');

          var new_color = $(this).data('background-color');

          if ($sidebar.length != 0) {
            $sidebar.attr('data-background-color', new_color);
          }
        });

        $('.fixed-plugin .img-holder').click(function() {
          $full_page_background = $('.full-page-background');

          $(this).parent('li').siblings().removeClass('active');
          $(this).parent('li').addClass('active');


          var new_image = $(this).find("img").attr('src');

          if ($sidebar_img_container.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            $sidebar_img_container.fadeOut('fast', function() {
              $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
              $sidebar_img_container.fadeIn('fast');
            });
          }

          if ($full_page_background.length != 0 && $('.switch-sidebar-image input:checked').length != 0) {
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $full_page_background.fadeOut('fast', function() {
              $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
              $full_page_background.fadeIn('fast');
            });
          }

          if ($('.switch-sidebar-image input:checked').length == 0) {
            var new_image = $('.fixed-plugin li.active .img-holder').find("img").attr('src');
            var new_image_full_page = $('.fixed-plugin li.active .img-holder').find('img').data('src');

            $sidebar_img_container.css('background-image', 'url("' + new_image + '")');
            $full_page_background.css('background-image', 'url("' + new_image_full_page + '")');
          }

          if ($sidebar_responsive.length != 0) {
            $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
          }
        });

        $('.switch-sidebar-image input').change(function() {
          $full_page_background = $('.full-page-background');

          $input = $(this);

          if ($input.is(':checked')) {
            if ($sidebar_img_container.length != 0) {
              $sidebar_img_container.fadeIn('fast');
              $sidebar.attr('data-image', '#');
            }

            if ($full_page_background.length != 0) {
              $full_page_background.fadeIn('fast');
              $full_page.attr('data-image', '#');
            }

            background_image = true;
          } else {
            if ($sidebar_img_container.length != 0) {
              $sidebar.removeAttr('data-image');
              $sidebar_img_container.fadeOut('fast');
            }

            if ($full_page_background.length != 0) {
              $full_page.removeAttr('data-image', '#');
              $full_page_background.fadeOut('fast');
            }

            background_image = false;
          }
        });

        $('.switch-sidebar-mini input').change(function() {
          $body = $('body');

          $input = $(this);

          if (md.misc.sidebar_mini_active == true) {
            $('body').removeClass('sidebar-mini');
            md.misc.sidebar_mini_active = false;

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

          } else {

            $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

            setTimeout(function() {
              $('body').addClass('sidebar-mini');

              md.misc.sidebar_mini_active = true;
            }, 300);
          }

          // we simulate the window Resize so the charts will get updated in realtime.
          var simulateWindowResize = setInterval(function() {
            window.dispatchEvent(new Event('resize'));
          }, 180);

          // we stop the simulation of Window Resize after the animations are completed
          setTimeout(function() {
            clearInterval(simulateWindowResize);
          }, 1000);

        });
      });
    });
  </script>


  <script>
    var x = new Spin.Spinner().spin();
    function loading(){
      
        $('body').append(x.el);
    }
    function loading_stop(){
      x.stop();
    }

    loading();
    $(document).ready(function() {
      loading_stop();
      md.checkFullPageBackgroundImage();
      setTimeout(function() {
        // after 1000 ms we add the class animated to the login/register card
        $('.card').removeClass('card-hidden');
      }, 700);

    var jam = setInterval(jams, 300);
    
    });

// function loading(){
//   let timerInterval
//   Swal.fire({
//     html: 'Checking data',
//     timer: 2000,
//     onBeforeOpen: () => {
//       Swal.showLoading()
//       timerInterval = setInterval(() => {
//         Swal.getContent().querySelector('strong')
//           .textContent = Swal.getTimerLeft()
//       }, 100)
//     },
//     onClose: () => {
//       clearInterval(timerInterval)
//     }
//   }).then((result) => {
//     if (
//       // Read more about handling dismissals
//       result.dismiss === Swal.DismissReason.timer
//     ) {
//       console.log('I was closed by the timer')
//     }
//   })
// }


function jams(){
  $('#jam').html(moment().format('HH:mm:ss'));
}

  </script>
</body>

</html>