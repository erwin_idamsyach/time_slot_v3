<?php

class Schedule_model extends CI_Model{

	public function getSchedule($date, $vendor_code){

		$data = $this->db->query("SELECT
							tb_rds_detail.id,
							material_code,
							tb_rds_detail.vendor_code,
							qty,
							requested_delivery_date,
							req_pallet,
							IF(@rownum + req_pallet <= 32, @rownum := @rownum + req_pallet, @rownum := 0 + req_pallet) as row_number
						FROM
							tb_rds_detail
						LEFT JOIN tb_scheduler ON tb_rds_detail.id = tb_scheduler.id_schedule
						LEFT JOIN tb_schedule_pool ON tb_rds_detail.id = tb_schedule_pool.id_schedule
						CROSS JOIN (SELECT @rownum := 0) r
						WHERE
							tb_rds_detail.vendor_code = '$vendor_code'
						AND 
						tb_scheduler.id_schedule IS NULL
						AND tb_schedule_pool.id_schedule IS NULL
						AND requested_delivery_date = '$date'
						AND tb_rds_detail.status = '0'");
		return $data;
	}

	public function getTimeSlot(){
		$data = $this->db->query("SELECT * FROM ms_time_slot");
		return $data;
	}

	public function getScheduleNumber(){
		$data = $this->db->query("SELECT schedule_number FROM tb_scheduler ORDER BY schedule_number DESC LIMIT 0,1")->row();
		$sn = ($data->schedule_number == null)?0:$data->schedule_number;
		return $sn+1;
	}

	public function getScheduleNew($date, $vendor_code){
		$data = $this->db->query("SELECT * FROM tb_scheduler WHERE rdd='$date' AND vendor_code='$vendor_code' GROUP BY schedule_number");
		return $data;
	}

	public function checkSlotAva($id_slot, $date){
		$cek = $this->db->query("SELECT COUNT(*) as COUNT FROM tb_delivery_detail WHERE id_time_slot='$id_slot' AND delivery_date='$date'")->row();
		return $cek->COUNT;
	}

	public function getScheduleDetail($id){
		$data = $this->db->query("SELECT
							c.id,
							c.id_schedule,
							a.po_number,
							c.schedule_number,
							c.vendor_code,
							d.vendor_name,
							a.material_code,
							b.material_name,
							a.qty,
							a.uom,
							a.uom_plt,
							a.req_pallet
						FROM
							tb_scheduler c
						INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
						LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
						INNER JOIN skin_master.ms_supplier d ON c.vendor_code = d.vendor_code
						WHERE
							schedule_number='$id'
						ORDER BY a.po_number ASC");
		$get_arr = $this->db->query("SELECT id_schedule FROM tb_scheduler WHERE schedule_number='$id'");
		return array(
			"data_list" => $data,
			"array_id" => $get_arr
		);
	}

	public function getDeliveryInfo($id){
		$data = $this->db->query("SELECT
									e.id_truck,
									e.delivery_date,
									e.driver_name,
									e.invoice,
									e.phone_number,
									concat(
										f.start_time,
										' - ',
										f.end_time
									) AS time_slot
								FROM
									tb_scheduler c
								INNER JOIN tb_delivery_detail e ON c.schedule_number = e.id_schedule_group
								INNER JOIN ms_time_slot f ON e.id_time_slot = f.id
								WHERE
									schedule_number = '$id'
								LIMIT 0, 1");
		return $data;
	}

	public function moveSchedule($id){
		$this->db->query("
			INSERT INTO tb_schedule_pool
			SELECT * FROM tb_scheduler WHERE id='$id'
			");
		$this->db->query("DELETE FROM tb_scheduler WHERE id='$id'");
	}

	public function getTimeSlotById($id){
		$data = $this->db->query("SELECT * FROM ms_time_slot WHERE slot_number=$id")->row();
		$st = date_create($data->start_time);
		$st = date_format($st, "H:i");
		$et = date_create($data->end_time);
		$et = date_format($et, "H:i");
		echo $st." - ".$et;
	}

	public function getSuggestion($find, $sn, $rdd){
		$data = $this->db->query("SELECT
									c.id,
									c.id_schedule,
									a.po_number,
									c.schedule_number,
									c.vendor_code,
									d.vendor_name,
									a.material_code,
									b.material_name,
									a.req_pallet
								FROM
									tb_scheduler c
								INNER JOIN tb_rds_detail a ON c.id_schedule = a.id
								LEFT JOIN skin_master.ms_material b ON a.material_code = b.material_sku
								INNER JOIN skin_master.ms_supplier d ON c.vendor_code = d.vendor_code
								WHERE
									schedule_number !='$sn'
								AND
									c.rdd = '$rdd'
								AND
									a.status = '0'
								AND
									a.req_pallet <= $find
								ORDER BY a.po_number ASC");
		return $data;
	}
}

?>