<?php
class Schedule extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('Schedule_model');
		isLogin();
	}

	public function index(){
		$arr = array();
		$tt = (null === $this->input->get('date_start'))?'26/10/2018':$this->input->get('date_start');
		$tt = str_replace("/", "-", $tt);
		$today = date_create($tt);
		$today = date_format($today, "Y-m-d");
		$vendor_code = $this->session->userdata('sess_vendor_code');

		$dataSchedule = $this->setSchedule($today, $vendor_code);

		$dataTimeSlot = $this->Schedule_model->getTimeSlot();
		$dataSchedule = $this->Schedule_model->getScheduleNew($today, $vendor_code);
		$arr = json_encode($dataSchedule->result());
		$get = json_decode($arr, true);

		$datec = date_create($today);
		$formatted_date = date_format($datec, "d/m/Y");

		$data['formatted_date'] 	= $formatted_date;
		$data['vendor_code'] 		= $vendor_code;
		$data['date_today'] 		= $today;
		$data['data_time_slot'] 	= $dataTimeSlot;
		$data['data_schedule'] 		= $get;
		getHTML('schedule/index', $data);
	}

	public function setSchedule($date, $vendor_code){
		$getLast = $this->Schedule_model->getScheduleNumber();
		$dataToday = $this->Schedule_model->getSchedule($date, $vendor_code);
		$counter = 0;
		/*echo $dataToday->num_rows(); die();*/
		if($dataToday->num_rows() > 0){
			foreach($dataToday->result() as $get){
				if($counter >= $get->row_number){
					$getLast = $getLast+1;
				}else{
					$getLast = $getLast;
				}
				$arr = array(
					'id_schedule' => $get->id,
					'vendor_code' => $get->vendor_code,
					'schedule_number' => $getLast,
					'rdd' => $get->requested_delivery_date
				);
				$this->db->insert('tb_scheduler', $arr);
				$counter = $get->row_number;
			}
		}else{
			
		}
	}

	public function view_schedule(){
		$sn = $this->uri->segment(3);
		$vendor_code = $this->session->userdata("sess_vendor_code");
		$req_data = $this->Schedule_model->getScheduleDetail($sn);

		$data['vendor_code'] = $vendor_code;
		$data['sn'] = $sn;
		$data['data_list'] = $req_data['data_list'];
		$data['array_id'] = $req_data['array_id'];

		getHTML('schedule/view_detail', $data);
	}

	public function move_schedule(){
		$id = $this->input->get('id');
		$this->Schedule_model->moveSchedule($id);
		return true;
	}

	public function get_schedule_list(){
		$sn = $this->input->get('sn');
		$vendor_code = $this->session->userdata("sess_vendor_code");
		$req_data = $this->Schedule_model->getScheduleDetail($sn);

		$data['vendor_code'] = $vendor_code;
		$data['sn'] = $sn;
		$data['data_list'] = $req_data['data_list'];
		$data['array_id'] = $req_data['array_id'];

		$this->load->view('schedule/v_schedule_new', $data);
	}

	public function get_pool_schedule_list(){
		$vendor_code = $this->input->get('vendor_code');
	}

	public function save_schedule(){
		$truck_no 		= $this->input->post('truck_no');
		$driver_name 	= $this->input->post('driver_name');
		$phone_no 		= $this->input->post('phone_number');
		$id_schedule 	= $this->input->post('id_schedule');
		$rdd 			= $this->input->post('rdd');
		$time_slot 		= $this->input->post('id_time_slot');
		$do_number 		= $this->input->post('do_number');

		$inv_file = $this->input->post('file', TRUE);

		$date = date('d-m-Y');

		$config['upload_path'] = './assets/upload/invoice'; 
  		$config['file_name'] = $date."_".$id_schedule."_".$inv_file;
  		$config['allowed_types'] = 'docx|doc|pdf';
  		$config['max_size'] = 0;

  		$this->load->library('upload', $config);
	  	$this->upload->initialize($config);

	  	if (!$this->upload->do_upload('file')) {
	  		echo $this->upload->display_errors(); die();
	   		$this->session->set_flashdata('ERR', $this->upload->display_errors()); 
	   		redirect('schedule');
	  	} else {
	  		$media = $this->upload->data();
	  		$f_name = 'assets/upload/invoice/'.$media['file_name'];

	  		$array = array(
	  			'id_schedule_group' => $id_schedule,
	  			'delivery_date' 	=> $rdd,
	  			'id_time_slot'		=> $time_slot,
	  			'id_truck'			=> $truck_no,
	  			'invoice'			=> $f_name,
	  			'driver_name'		=> $driver_name,
	  			'phone_number'		=> $phone_no,
	  			'do_number'			=> $do_number
	  		);

	  		$this->db->insert('tb_delivery_detail', $array);

	  		$this->setStatus($id_schedule, '1');

	  		redirect('schedule');
	  	}
	}

	public function setStatus($schedule_number, $status){
		$array = array();

		$this->db->where('schedule_number', $schedule_number);
		$this->db->update('tb_scheduler', array(
			'status' => $status
		));

		$data = $this->db->query("SELECT id_schedule FROM tb_scheduler WHERE schedule_number='$schedule_number'");
		foreach($data->result() as $get){
			array_push($array, $get->id_schedule);
		}
		$js = json_encode($array);
		$js = substr($js, 1);
		$js = substr($js, 0, -1);
		$this->db->query("UPDATE tb_rds_detail SET status='1' WHERE id IN ($js)");
	}

	public function test(){
		$array = array();
		$data = $this->db->query("SELECT id_schedule FROM tb_scheduler WHERE schedule_number='2'");
		foreach($data->result() as $get){
			array_push($array, $get->id_schedule);
		}
		$js = json_encode($array);
		$js = substr($js, 1);
		$js = substr($js, 0, -1);
		echo $js;
	}

	public function get_detailed_information(){
		$key = $this->input->get('key');

		$sch_detail = $this->Schedule_model->getScheduleDetail($key);
		$del_detail = $this->Schedule_model->getDeliveryInfo($key);
		/*echo json_encode($del_detail->result()); die();*/

		$data['sch_detail'] = $sch_detail['data_list'];
		$data['del'] = $del_detail->row();

		$this->load->view('schedule/modal_info', $data);
	}

	public function get_suggestion(){
		$p_min = $this->input->get("pallet_minus");
		$sn = $this->input->get('sn');
		$rdd = $this->input->get('rdd');

		$data_suggestion = $this->Schedule_model->getSuggestion($p_min, $sn, $rdd);

		$data['data_list'] = $data_suggestion;
		$data['p_min'] = $p_min;
		$data['sn'] = $sn;

		$this->load->view('schedule/data_suggestion', $data);
	}

	public function add_material(){
		$id = $this->input->get('id');
		$sn = $this->input->get('sn');

		$this->db->where('id_schedule', $id);
		$this->db->update('tb_scheduler', array(
			"schedule_number" => $sn
		));

		return true;
	}
}
?>