<?php
$a = 0;
$c = count($data_schedule);
?>
<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        Schedule
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
      <div class="row">
        <div class="col-md-3">
          <div class="form-group">
            <label>Date</label>
            <div class="input-group date">
              <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control pull-right dapick" id="datepicker" required="" value="<?php echo $formatted_date ?>">
            </div>
          </div>
        </div>
        <div class="col-md-1">
          <button class="btn btn-primary btn-block" style="margin-top: 25px;" onclick="changeURL()">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <label for="">Schedule</label>
            </div>
            <div class="box-body" style="padding: 0;">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th class="text-center" colspan="2">
                      <?php 
                      $date = date_create($date_today); 
                      $df = date_format($date, "D, d M Y");
                      echo $df;
                      ?>
                    </th>
                  </tr>
                  <tr bgcolor="#2184FF" style="color: #fff">
                    <th width="200px">Time Slot (24 Hours format)</th>
                    <th class="text-center">Status</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($data_time_slot->result() as $get){
                  ?>
                  <tr class="height-65">
                    <td>
                      <?php 
                      $st = date_create($get->start_time);
                      $et = date_create($get->end_time);
                      $st = date_format($st, "H:i");
                      $et = date_format($et, "H:i");
                      echo $st." - ".$et; ?>
                    </td>
                    <td class="text-center">
                      <?php
                      $cek = $this->Schedule_model->checkSlotAva($get->id, $date_today);
                      if($cek < 3){
                        if($a < $c){
                            $status = $data_schedule[$a]['status'];
                            if($status == '0'){
                              ?>
                              <a href="<?php echo base_url().'schedule/view_schedule/'.$data_schedule[$a]['schedule_number'].'/'.$data_schedule[$a]['rdd'].'/'.$get->id; ?>" class="truck">
                                <?php truckIcon($data_schedule[$a]['status']) ?>
                              </a>
                              <?php
                            }else{
                              ?>
                              <a class="truck" onclick="getDetailedInformation(<?php echo $data_schedule[$a]['schedule_number']; ?>)">
                                <?php truckIcon($data_schedule[$a]['status']); ?>
                              </a>
                              <?php
                            }
                        }else{

                        }
                        $a++;
                      }
                      ?>
                    </td>
                  </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <label for="">LEGEND</label>
            </div>
            <div class="box-body">
              <table class="table">
                <tr>
                  <td width="70px"><?php truckIcon(); ?></td>
                  <td>
                    <h4>NOT PLANNED</h4>
                  </td>
                </tr>
                <tr>
                  <td width="70px"><?php truckIcon(1); ?></td>
                  <td>
                    <h4>PLANNED</h4>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>