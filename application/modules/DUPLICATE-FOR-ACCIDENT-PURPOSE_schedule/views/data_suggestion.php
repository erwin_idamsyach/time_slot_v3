<table class="table table-striped table-hover">
  <thead>
    <tr>
      <th class="text-center">PO Number</th>
      <th class="text-center">Material Code</th>
      <th class="text-center">Material Name</th>
      <th class="text-center">Req. Pallet</th>
      <th class="text-center">*</th>
    </tr>
  </thead>
  <tbody id="area-list">
    <?php
    foreach($data_list->result() as $get){
      ?>
    <tr>
      <td class="text-center"><?php echo $get->po_number ?></td>
      <td><?php echo $get->material_code ?></td>
      <td><?php echo $get->material_name ?></td>
      <td width="25px">
        <?php echo $get->req_pallet ?>
      </td>
      <td class="text-center">
        <button class="btn btn-link clr-green" onclick="addMaterial(<?php echo $get->id_schedule; ?>, <?php echo $sn; ?>)">
          <i class="fa fa-arrow-up"></i>
        </button>
      </td>
    </tr>
      <?php
    }
    ?>
  </tbody>
</table>