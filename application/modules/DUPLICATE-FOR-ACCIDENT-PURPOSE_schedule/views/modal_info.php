<div class="row">
	<div class="col-md-4" style="border-right: 1px solid #BDBDBD">
		<div class="form-group">
			<label>RDD</label><br>
			<label><?php 
			$date = date_create($del->delivery_date);
			$df = date_format($date, "D, d M Y");
			echo $df; ?></label>
		</div>
		<div class="form-group">
			<label>Time</label><br>
			<label><?php echo $del->time_slot; ?></label>
		</div>
		<hr>
		<div class="form-group">
			<label>Truck No</label><br>
			<label><?php echo $del->id_truck; ?></label>
		</div>
		<div class="form-group">
			<label>Driver Name</label><br>
			<label><?php echo $del->driver_name; ?></label>
		</div>
		<div class="form-group">
			<label>Phone Number</label><br>
			<label><?php echo $del->phone_number; ?></label>
		</div>
		<hr>
		<div class="form-group">
			<label>Invoice File</label><br>
			<a href="<?php echo base_url().$del->invoice ?>" class="btn btn-default" target="__Blank">
				<i class="fa fa-file-o"></i> Download
			</a>
		</div>
	</div>
	<div class="col-md-8">
		<table class="table table-striped table-hover">
            <thead>
              <tr>
                <th class="text-center">PO Number</th>
                <th class="text-center">Material Code</th>
                <th class="text-center">Material Name</th>
                <th class="text-center">Req. Pallet</th>
              </tr>
            </thead>
            <tbody id="area-list">
              <?php
              foreach($sch_detail->result() as $get){
                ?>
              <tr>
                <td class="text-center"><?php echo $get->po_number ?></td>
                <td><?php echo $get->material_code ?></td>
                <td><?php echo $get->material_name ?></td>
                <td width="25px">
                  <?php echo $get->req_pallet ?>
                </td>
              </tr>
                <?php
              }
              ?>
            </tbody>
          </table>
	</div>
</div>