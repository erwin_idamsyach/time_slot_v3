<?php
foreach($data_list->result() as $get){}
?>
<table class="table">
	<!-- <tr>
		<th>PO Number</th>
		<td>
			<?php echo $get->po_number ?>
		</td>
	</tr>
	<tr>
		<th>Vendor Code</th>
		<td>
			<?php echo $get->vendor_code; ?>
		</td>
	</tr>
	<tr>
		<th>Vendor Name</th>
		<td>
			<?php echo $get->vendor_name ?>
		</td>
	</tr>
	<tr>
		<th>RDD</th>
		<td>
			<?php
			$datec = date_create($get->requested_delivery_date);
			$daf = date_format($datec, "D, d M Y");
			echo $daf;
			?>
		</td>
	</tr>
	<tr>
		<td colspan="3"><hr></td>
	</tr> -->
	<tr>
		<th>Material Code</th>
		<td>
			<?php echo $get->material_code ?>
		</td>
	</tr>
	<tr>
		<th>Material Name</th>
		<td>
			<?php echo $get->material_name ?>
		</td>
	</tr>
	<tr>
		<th>Qty</th>
		<td>
			<?php echo number_format($get->qty, 3, ',', ','); ?>
		</td>
	</tr>
	<tr>
		<th>Pallet Used</th>
		<td>
			<?php echo $get->pallet_count; ?>
		</td>
	</tr>
	<tr>
		<th>Delivery Date</th>
		<td><b>
			<?php
			$datec = date_create($get->delivery_date);
			$daf = date_format($datec, "D, d M Y");
			echo $daf;
			?>
			</b>
		</td>
	</tr>
	<tr>
		<th>Time</th>
		<td><b>
			<?php
			echo $get->start_time." - ".$get->end_time;
			?>
			</b>
		</td>
	</tr>
	<tr>
		<th>Truck No</th>
		<td>
			<b><?php echo $get->id_truck; ?></b>
		</td>
	</tr>
	<tr>
		<th>Invoice File</th>
		<td>
			<a href="<?php echo base_url().$get->invoice ?>" target="__Blank" class="btn btn-default">
				<i class="fa fa-file-pdf-o"></i> Download
			</a>
		</td>
	</tr>
</table>