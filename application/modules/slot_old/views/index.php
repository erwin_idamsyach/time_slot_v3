<style>
	
.dataz{
    padding: 0px;
    white-space: nowrap;
    margin:0px;
}
.category-4 .btn-group .btn.btn-info{padding : 15px;}

@media screen and  (min-width: 400px){
	.filter-action{
			
	}
}

@media screen and  (min-width: 825px){
	.filter-action{
		position: absolute;
	}

	.delete-button{
		margin-top:20px;
	}
}

</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-boxes"></i>
                    </div>
                    <h4 class="card-title">RAW RDS Data</h4>
                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->

				<div class="box-body">
					<div class="container-fluid">
					<div class="row">
					          <?php $year = $_GET['year']; $week = $_GET['week']; $get_category= $_GET['category']; ?>

					          <div class="col">
					          <table>
					          	<tr>
					          		<td>
					          			<div class="form-group">
								            <label style="margin-left:70px">Year</label>
								            <nav aria-label="Page navigation example">
								                <ul class="pagination pagination-primary">
								                  
								                  <li class="page-item">
								                    <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $year-1; ?></a>
								                  </li>
								                  
								                  <li class="page-item active">
								                    <a class="page-link " id='year' href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $year; ?></a>
								                  </li>

								                  <li class="page-item">
								                    <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $year+1; ?></a>
								                  </li>

								                </ul>
								              </nav>
								            </div>
					          		</td>
					          		<td>
					          			<div class="form-group">
								            <label style="margin-left:85px">Week</label>
								            <nav aria-label="Page navigation example">
								                <ul class="pagination pagination-primary">
								                  
								                  <li class="page-item">
								                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>&category=<?php echo $get_category; ?>"><?php echo $week-2; ?></a>
								                  </li>
								                   <li class="page-item">
								                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>&category=<?php echo $get_category; ?>"><?php echo $week-1; ?></a>
								                  </li>
								                  
								                  <li class="page-item active">
								                    <a class="page-link " id='week' href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $week; ?></a>
								                  </li>
								                  <li class="page-item">
								                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>&category=<?php echo $get_category; ?>"><?php echo $week+1; ?></a>
								                  </li>
								                  <li class="page-item">
								                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>&category=<?php echo $get_category; ?>"><?php echo $week+2; ?></a>
								                  </li>
								                </ul>

								              </nav>

								            </div>
					          		</td>
					          	</tr>
					          </table>
						    </div>

						    <div class="col" style="text-align: right;">
									
										<!-- <form action="<?php echo base_url(); ?>slot/upload_excel" id="form-upload" method="post" enctype="multipart/form-data" style="display: none;">
											<input type="file" name="file" id="file" onchange="$('#form-upload').submit();">
										</form> -->
										<!-- <button class="btn btn-success" onclick="$('#file').trigger('click')"><i class="fa fa-upload"></i> UPLOAD EXCEL</button> -->
										<?php
					date_default_timezone_set("Asia/Bangkok"); 
					$time = date('H:i:s');

					$start_time = $time_freeze->row()->start_time;
					$end_time = $time_freeze->row()->end_time;
					$status_time = '';
					if($time >= $end_time || $time < $start_time){
						 $status_time = "disabled";
					}else{
						
					}
					 ?>

					<?php if(isset($_GET['delete'])) {?>
					<b style="color:red;font-size:14px"><?php echo $_GET['delete']; ?> Deleted</b>
					<?php } ?>
					<?php echo "<b style='color:red'>".$this->session->flashdata('ERR')."</b>"; ?>
					<?php echo "<b style='color:green'>".$this->session->flashdata('SCS')."</b>"; ?>
					</div>
				</div>
					<div class="row filter-action">
						<div class="col category-4 ">
							<div class="btn-group" data-toggle="buttons">
							  <button type="button" class="btn  btn-info <?php if($_GET['category'] == ''){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>slot?category=&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>')">ALL</button>
		                      <?php
							foreach ($category->result() as $category) {
							?>
								<button type="button" class="btn  btn-info <?php if($_GET['category'] == $category->category){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>slot?category=<?php echo $category->category; ?>&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>')"><?php echo $category->category; ?></button>
							<?php
							}
						 ?>
		                    </div>
					</div>
					<div class="col delete-button" <?php if(!check_sub_menu(9)){echo "hidden";} ?>>
						<div >
						<button <?php echo $status_time; ?> class="btn btn-danger btn-sm" <?php echo ($this->session->userdata('sess_role_no') == 1 || $this->session->userdata('sess_role_no') == 2)?'':'style="display: none"' ?> onclick="check_delete()"><i class="fa fa-trash"></i> DELETE</button>
						</div>
					</div>
					<div class="col-6">
					</div>
				</div>

				<div class="row">
					<div class="table-responsive" style="margin-top:15px">
					<table id="table" class="table table-striped table-bordered table-hover" width="100%">
						<thead class="" style="color:white; background-color:#31559F;font-weight: 650">
							<tr>
								<th <?php echo ($this->session->userdata('sess_role_no') == 1 || $this->session->userdata('sess_role_no') == 2)?'style="font-size:14px"':'style="display: none"' ?> width="30" align="center"><input type="checkbox" id="checkAll" name="checkall"></th>
								<th style="font-size:14px;white-space: nowrap;" width="50" class="text-center">Category</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">PO Number#</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Line</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Material Code</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Material Name</th>
								<th style="font-size:14px;white-space: nowrap;" style="font-size:14px" class="text-center">Quantity</th>
								
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Outs. Qty</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Vendor Name</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Request Del.Date</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Shift</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Status</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Req. Qty (Pallet)</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center">Send (Pallet)</th>
								<th style="font-size:14px;white-space: nowrap;" style="font-size:14px" class="text-center">Received Amount</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center" width="10px">Outs. (Pallet)</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center" width="50px">Created By</th>
								<th style="font-size:14px;white-space: nowrap;" class="text-center" width="50px">Update By</th>
							</tr>
						</thead>
						<tbody id="table_data"> 
							<?php

						foreach($rds->result() as $get){
							
							?>
							<tr>
								<td <?php echo ($this->session->userdata('sess_role_no') == 1 || $this->session->userdata('sess_role_no') == 2)?'':'style="display: none"' ?>>
									<?php if($get->statz == 0){ ?>
									<input type="checkbox" value="<?php echo $get->id; ?>"  id="checkItem" name="check_item">
									<?php } ?>
								</td>
								<td style="font-size:12px" class="dataz" width="15" align="center"><?php echo $get->category; ?></td>
								<td style="font-size:12px" class="dataz"><?php echo $get->po_number; ?></td>
								<td style="font-size:12px" class="dataz" align="center"><?php echo $get->po_line_item; ?></td>
								<td style="font-size:12px" class="dataz"><?php echo $get->material_code; ?></td>
								<td style="font-size:12px" class="dataz" ><?php echo $get->material_name; ?></td>
								<td style="font-size:12px" class="dataz" align="center"><?php echo $get->qty." ".$get->uom; ?></td>
								<td style="font-size:12px" class="dataz" align="center">
									<!-- <?php echo $plt_kurang * $get->uom_plt; echo " ".$get->uom?> -->
										<?php echo ($get->req_pallet - $get->receive_amount) * $get->uom_plt; ?>
									</td>
								
								<td style="font-size:12px" class="dataz" align="center"><?php echo $get->vendor_alias; ?></td>
								
								<td style="font-size:12px" class="dataz" align="center" width="60"><?php echo Date('d-m-Y',strtotime($get->requested_delivery_date)); ?></td>
								<td style="font-size:12px" class="dataz" align="center"><?php echo $get->shift; ?></td>
								<td style="font-size:10px" class="dataz" align="center" style="font-size:10px"><?php LegendIcon($get->statz); ?></td>
								<td style="font-size:12px" class="text-center dataz" align="center"> <?php echo $get->req_pallet; ?></td>
								<td style="font-size:12px" class="text-center dataz" align="center"><?php 
								$send_pallet = $get->SEND_ACT+$get->REC_ACT;
								if($send_pallet != 0){echo $send_pallet;}else{echo 0;} 
								?></td>
								<td style="font-size:12px" class="dataz" align="center"><?php if($get->statz == '3'){echo $get->receive_amount;}else{echo 0;} ?></td>
								<td style="font-size:12px" class="text-center dataz" align="center">
									<!-- <?php echo $plt_kurang; echo " ".$get->uom?> -->
										<?php echo ($get->req_pallet - $get->receive_amount); ?></td>
								<?php if($get->created_at == "0000-00-00 00:00:00" ){
									$created_at = '';
								}else{
									$created_at = Date('d/m H:i', strtotime($get->created_at) );
								} ?>
								<td style="font-size:12px" class="text-center dataz" width="60"><?php echo $get->created_name; ?><br><?php echo $created_at; ?></td>
								<?php if($get->update_at == "0000-00-00 00:00:00" ){
									$update_at = '';
								}else{
									$update_at = Date('d/m H:i', strtotime($get->update_at) );
								} ?>
								<td style="font-size:12px" class="text-center dataz" width="60"><?php echo $get->edit_name ?><br><?php echo $update_at; ?></td>
							</tr>
								<?php
							}

							?>
						</tbody>
					</table>
					</div>
				</div>

				</div>
				</div>
			</div>
	</div>

</div>
</div>
</div>
</div>
</div>
</div>