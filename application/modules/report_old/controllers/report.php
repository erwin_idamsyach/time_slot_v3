<?php
require_once APPPATH.'third_party/spout-master/src/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Box\Spout\Common\Exception\IOException;
use Box\Spout\Reader\Exception\ReaderNotOpenedException;
class report extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		isLogin();
		$this->load->model('report_models');
	}

	public function test(){
		echo date('W', mktime(0,0,0,12,28,2019));
	}

//OTIF
	public function otif(){
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		if(!check_sub_menu(35)){ 
			$vendor_code = $this->session->userdata('sess_vendor_code');

		}else{
			$vendor_code = $this->input->get('vendor_code');
		}
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		$get_category 					= $this->db->query("SELECT category FROM ms_category");
		$data['category'] 				= $get_category;
		// $data['date_list'] 				= $this->getDateList($week, $year);
		$data['data_vendor_list'] 		= $this->report_models->getDataVendor();
		$data['data_vendor']			= $this->report_models->getVendor($vendor_code);
		$data['data_select_otif']		= $this->report_models->getDataMsOtif();
		getHTML("report/otif", $data);
	}

	public function chart(){
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$vendor_code = $this->input->get('vendor_code');
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		$data['data_vendor']			= $this->report_models->getVendor($vendor_code);
		$data['data_select_otif']		= $this->report_models->getDataMsOtif();
		$date_list 						= $this->getDateList($week, $year);
		$data['date_list']				= $date_list;
		$data["data_report"] 			= $this->report_models->getReport($date_list, $week, $year, $vendor_code);
		$data["data_otif"] 				= $this->report_models->getDataOtif($date_list, $data["data_report"], $vendor_code);
		$data['data_otif_loss_tree'] 	= $this->report_models->otifLossTree($data["data_report"] , $data["data_otif"]);
		
		$this->load->view("report/chart", $data);
		
	}

	public function loss_tree(){
		$year = $this->input->post('year');
		$week = $this->input->post('week');
		if(!check_sub_menu(35)){
			$vendor_code = $this->session->userdata('sess_vendor_code');
		}else{
			$vendor_code  = $this->input->post('vendor_code');
		}
		
		
		$category = $this->input->post('category');
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		$dto 								= new DateTime();
		if(empty($week)){
			$year = date('Y');
			$week = date('W');
			$dto->setISODate($year, $week);
			$start_date 					= $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}else{
			if(is_array($week)){
				sort($week);
				$getWeek = $week[0];
				$getEndWeek = $week[count($week)-1];
			}else{
				$getWeek = $week;
				$getEndWeek = $week;
			}	
			$dto->setISODate($year, $getWeek);
			$start_date 					= $dto->format('Y-m-d');
			$dto->setISODate($year, $getEndWeek);
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}

		$data['data_vendor']			= $this->report_models->getVendor($vendor_code);
		$data['data_select_otif']		= $this->report_models->getDataMsOtif();
		$date_list 						= $this->getDateList($week, $year);
		$data['date_list']				= $date_list;
		$data["data_report"] 			= "";
		$data["data_otif"]				= "";
		$weekz= $week;
		if(is_array($weekz)){
			$weekz = $week[0];
		}
		$check_avaible = $this->db->query("SELECT id FROM tb_otif_loss_tree WHERE week = '$weekz' AND YEAR(date) ='$year' AND vendor_code = '$vendor_code' ")->num_rows();
		$data["data_report"] 			= $this->report_models->getReport($date_list, $week, $year, $vendor_code, $category, $start_date, $end_date);
		$data['data_otif_loss_tree'] 	= $this->report_models->otifLossTree($data["data_report"], $week, $year);
		if($check_avaible > 0){
			$data["data_otif"] 				= $this->report_models->getSavedOtif($date_list, $vendor_code, $week, $category, $year);
		}else{
			$data["data_otif"] 				= $this->report_models->getDataOtif($date_list, $data["data_report"], $vendor_code, $week, $category, $year, $data['data_otif_loss_tree']);
			$data["data_otif_weekly"]		= $this->report_models->getDataOtifWeekly($data["data_otif"], $week, $year);
		}
		$data["year"]					= $year;
		$data['week']					= $week;
		$data['total_date']				= $this->report_models->total_date($data["data_otif"], $date_list);
		
		$this->load->view("report/otif_loss_delivery_date", $data);
	}

	public function data_report(){
		$year = $this->input->post('year');
		$week = $this->input->post('week');
		$vendor_code = $this->input->post('vendor_code');
		$category = $this->input->post('category');
		if($this->session->userdata('sess_vendor_code') != '' ){
			$vendor_code = $this->session->userdata('vendor_code');
		}else{
			if($this->input->post('vendor_code') == ''){
				$vendor_code = '21632';
			}else{
				$vendor_code = $this->input->post('vendor_code');
			}		
		}
		$dto = new DateTime();
		if(empty($week)){
			$year = date('Y');
			$week = date('W');
			$dto->setISODate($year, $week);
			$start_date 					= $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}else{
			if(is_array($week)){
				sort($week);
				$getWeek = $week[0];
				$getEndWeek = $week[count($week)-1];
			}else{
				$getWeek = $week;
				$getEndWeek = $week;
			}	
			$dto->setISODate($year, $getWeek);
			$start_date 					= $dto->format('Y-m-d');
			$dto->setISODate($year, $getEndWeek);
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}
		
		$date_list 						= $this->getDateList($week, $year);
		$data['date_list'] 				= $date_list;
		$data_report = $this->report_models->getReport($date_list, $week, $year, $vendor_code, $category, $start_date, $end_date);
		if(count($week) > 1){
			$data["data_report"]		= $this->report_models->getWeeklyDataReport($data_report, $week, $year);
		}else{
			$data["data_report"] 		= $data_report;
		}
		
		$data['data_select_otif']		= $this->report_models->getDataMsOtif();
		$data['year']					= $year;
		$data["vendor_code"]			= $vendor_code;
		$data["category"]				= $category;
		$data['week']					= $week;
		$data['year']					= $year;
		$data["start_date"]				= $start_date;
		$data["end_date"]				= $end_date;
		$this->load->view('report/report_otif',$data);
	}
//END OTIF

// ON TIME REPORT

	public function on_time(){
		if(!check_sub_menu(35)){ 
			$vendor_code = $this->session->userdata('sess_vendor_code');

		}else{
			$vendor_code = $this->input->get('vendor_code');
		}
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		$get_category 					= $this->db->query("SELECT category FROM ms_category");
		$data['category'] 				= $get_category;
		$data['data_vendor_list'] 		= $this->report_models->getDataVendor();
		$data['data_vendor']			= $this->report_models->getVendor($vendor_code);
		getHTML("report/ontime_slot", $data);
	}

	public function unloading_time(){
		if(!check_sub_menu(35)){ 
			$vendor_code = $this->session->userdata('sess_vendor_code');

		}else{
			$vendor_code = $this->input->get('vendor_code');
		}
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		$get_category 					= $this->db->query("SELECT category FROM ms_category");
		$data['category'] 				= $get_category;
		
		$data['data_vendor_list'] 		= $this->report_models->getDataVendor();
		$data['data_vendor']			= $this->report_models->getVendor($vendor_code);
		getHTML("report/unloading_time", $data);
	}

	public function on_time_data_report(){
		$link							= $this->input->post('link');
		$year 							= $this->input->post('year');
		$week 							= $this->input->post('week');
		$dto 							= new DateTime();
		$early 							= 0;
		$delay 							= 0;
		if(empty($week)){
			$year = date('Y');
			$week = date('W');
			$dto->setISODate($year, $week);
			$start_date 					= $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}else{
			if(is_array($week)){
				sort($week);
				$getWeek = $week[0];
				$getEndWeek = $week[count($week)-1];
			}else{
				$getWeek = $week;
				$getEndWeek = $week;
			}	
			$dto->setISODate($year, $getWeek);
			$start_date 					= $dto->format('Y-m-d');
			$dto->setISODate($year, $getEndWeek);
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}
		
		if(!check_sub_menu(35)){ 
			$vendor_code = $this->session->userdata('sess_vendor_code');

		}else{
			$vendor_code  = $this->input->post('vendor_code');
		}
		
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		
		$data_list 						= $this->report_models->DataOntime($start_date, $end_date, $vendor_code, $link);
		$date_list 						= $this->getDateList2($week, $year);
		$get_category 					= $this->db->query("SELECT category FROM ms_category");
		$data['week_list']				= $week;
		$data['category'] 				= $get_category;
		
		if(count($week) > 1){
			$data['data_list']			= $data_list;
			$data['data_list_week']		= $this->report_models->getWeeklyDataOntime($data_list, $week);
		}else{
			$data['data_list']			= $data_list;
		}
		$data["total_data_list"]		= count($data_list)-1;
		$data["week"]					= $week;
		$data["year"]					= $year;
		$data["vendor_code"]			= $vendor_code;
		$data['date_list'] 				= $date_list;
		$data['data_vendor_list'] 		= $this->report_models->getDataVendor();
		$data['data_vendor']			= $this->report_models->getVendor($vendor_code);
		$data['data_select_otif']		= $this->report_models->getDataMsOtif();
		if($link == "on_time"){
			if(count($data_list) > 0){
				$early = $data_list[count($data_list)-1]->early;
				$delay = $data_list[count($data_list)-1]->delay;
			}
			$data_average = $this->report_models->getAverrageOntime($date_list, $data_list);
			$data["total_early"]			= $early;
			$data["total_delay"]			= $delay;
			$data["data_average"]			= $data_average;
			$this->load->view('report/ontime_data_report', $data);
		}else{
			$shift_list = array(
				"DM","DP","DS"
			);
			$data_unloading					= $this->report_models->getDataUnloading($date_list, $data_list, 									  $shift_list);
			$data_average					= $this->report_models->getAverageUnloading($date_list, $data_unloading, 							   $shift_list);
			$data_weekly_chart1				= $this->report_models->getDataWeeklyOnTime($data_average, $week, $year);
			$data["data_weekly_chart1"]		= $data_weekly_chart1;
			$data_chart_avg_week			= $this->report_models->getDataChart($data_average, $week, $year);
			$data['data_unloading']			= $data_unloading;
			$data['data_chart']				= $data_chart_avg_week;
			$data['data_average']			= $data_average;
			$data['shift_list']				= $shift_list;
			$this->load->view('report/unloading_data_report', $data);
		}
	}

	public function get_week(){
		$year 							= $this->input->post('year');
		$week 							= $this->input->post('week');
		$month							= $this->input->post('month');
		sort($month);
		$frist_week = date('W', mktime( 0, 0, 0, $month[0], 01, $year));
		$end_week = date('W', mktime( 0, 0, 0, $month[count($month)-1], 28, $year));
		$data["frist_week"] = $frist_week;
		$data["end_week"] = $end_week; 
		$this->load->view('report/modal_week', $data);
	}

	public function on_time_react(){
		$year 							= $this->input->get('year');
		$week 							= $this->input->get('week');
		$dto 							= new DateTime();
		$shift_list = array(
			"DM","DP","DS"
		);
		$dto->setISODate($year, $week);
		$start_date 					= $dto->format('Y-m-d');
		$dto->modify('+6 days');
		$end_date 						= $dto->format('Y-m-d');
		$vendor_code 					= $this->input->get('vendor_code');
		
	}

//END ON TIME //

	public function chart_confirm_data(){
		$year 							= $this->input->get('year');
		$week 							= $this->input->get('week');
		$dto 							= new DateTime();
		if(empty($week)){
			$year = date('Y');
			$week = date('W');
			$dto->setISODate($year, $week);
			$start_date 					= $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}else{
			if(is_array($week)){
				sort($week);
				$getWeek = $week[0];
				$getEndWeek = $week[count($week)-1];
			}else{
				$getWeek = $week;
				$getEndWeek = $week;
			}	
			$dto->setISODate($year, $getWeek);
			$start_date 					= $dto->format('Y-m-d');
			$dto->setISODate($year, $getEndWeek);
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
			
		}
		$date_list 						= $this->getDateList($week, $year);
		$vendor_code					= $this->input->get('vendor_code');
		$data							= $this->report_models->getReport($date_list, $week, $year, $vendor_code);
		$data_otif 						= $this->report_models->getDataOtif($date_list, $data, $vendor_code);
		$total_date = array();
		$shift_list = array(
			"DM", "DP", "DS"
		);
		$no=0;	
		
		foreach ($date_list as $date) {
			$average = 0;
			$average2 = 0;
			foreach ($shift_list as $shift) {
				$data_list =str_replace('%','',($data_otif[$date]["otif"][$shift]));
				$average = $average + $data_list;
				// $data_list2 =str_replace('%','',($data_otif[$date]["actual"][$shift]));
				// $average2 = $average2 + $data_list2;
			}
			$total_date[$no]["otif"] = round($average/3,2);
			//$total_date[$no]["actual"] = round($average2);
			$no++;
		}
		
		echo json_encode($total_date);
	}


	public function getDateList($week, $year){
		$getDateTime = new DateTime();
		if(!is_array($week)){
			$waktu[0] = $getDateTime->setISODate($year, $week)->format('Y-m-d');
			$i = 1;
			while($i < 7){
				$waktu[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
			  	$i++;
			}
			return $waktu;
		}else{
			$total_week = count($week);
			$frist_week = $week[0];
			$end_week = $week[$total_week-1];
			$waktu[0] = $getDateTime->setISODate($year, $week[0])->format('Y-m-d');
			$i = 1;
			while($i <= ($total_week*7)-1)	{
				$waktu[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
			  	$i++;
			}
			return $waktu;
		}
		
	}

	public function getDateList2($week, $year){
		$getDateTime = new DateTime();
		if(!is_array($week)){
			$waktu[0] = $getDateTime->setISODate($year, $week)->format('Y-m-d');
			$i = 1;
			while($i < 7){
				$waktu[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
			  	$i++;
			}
			return $waktu;
		}else{
			$total_week = count($week);
			$frist_week = $week[0];
			$end_week = $week[$total_week-1];
			$waktu[0] = $getDateTime->setISODate($year, $week[0])->format('Y-m-d');
			$i = 1;
			while($i <= ($total_week*7)-1)	{
				$waktu[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
			  	$i++;
			}
			return $waktu;
		}
		
	}

	public function save_otif_code(){
		$code = $this->input->get('code');
		$level = $this->input->get('level');
		$loss_tree = $this->input->get('loss_tree');
		$example = $this->input->get('example');
		$data = array(
			"code" => $code,
			"level" => $level,
			"loss_tree" => $loss_tree,
			"example" => $example
		);
		$save = $this->db->insert('ms_otif_loss_code', $data);
		if($save){
			return true;
		}else{
			return false;
		}
	}

	public function table_otif(){
		$data['data_otif'] = $this->db->query("SELECT * FROM ms_otif_loss_code")->result();
		$this->load->view('report/table_otif',$data);
	}

	public function delete_otif(){
		$id = $this->input->get('id');
		$delete = $this->db->query("DELETE FROM ms_otif_loss_code WHERE id = '$id' ");
		if($delete){
			return true;
		}else{
			return false;
		}
	}

	public function update_otif_LossTree(){
		$id_schedule = $this->input->get("id_schedule");
		$id_otif = $this->input->get("id_otif");
		$data = array(
			"otif"	=> $id_otif
		);
		$this->db->where('id', $id_schedule);
		$update = $this->db->update("tb_rds_detail", $data);
		if($update){

			return true;
		}else{
			return false;
		}
	}

	public function download_otif(){
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$vendor_code = $this->input->get('vendor_code');
		$category = $this->input->get('category');
		if($this->session->userdata('sess_vendor_code') != '' ){
			$vendor_code = $this->session->userdata('vendor_code');
		}else{
			if($this->input->post('vendor_code') == ''){
				$vendor_code = '21632';
			}else{
				$vendor_code = $this->input->post('vendor_code');
			}		
		}
		$dto 							= new DateTime();
		if(empty($week)){
			$year = date('Y');
			$week = date('W');
			$dto->setISODate($year, $week);
			$start_date 					= $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}else{
			if(is_array($week)){
				sort($week);
				$getWeek = $week[0];
				$getEndWeek = $week[count($week)-1];
			}else{
				$getWeek = $week;
				$getEndWeek = $week;
			}	
			$dto->setISODate($year, $getWeek);
			$start_date = $dto->format('Y-m-d');
			$dto->setISODate($year, $getWeek);
			$dto->modify('+6 days');
			$end_date = $dto->format('Y-m-d');
		}
		$data_select_otif 				= $this->report_models->getDataMsOtif();
		$date_list 						= $this->getDateList($week, $year);
		$data = $this->report_models->getReport($date_list, $week, $year, $vendor_code, $category, $start_date, $end_date);

		$this->report_models->download_otif($data, $year, $week, $date_list, $vendor_code, $data_select_otif);
	}

	public function download_on_time(){
		$year 							= $this->input->get('year');
		$week 							= $this->input->get('week');
		$dto 							= new DateTime();
		if(empty($week)){
			$year = date('Y');
			$week = date('W');
			$dto->setISODate($year, $week);
			$start_date 					= $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}else{
			if(is_array($week)){
				sort($week);
				$getWeek = $week[0];
				$getEndWeek = $week[count($week)-1];
			}else{
				$getWeek = $week;
				$getEndWeek = $week;
			}	
			$dto->setISODate($year, $getWeek);
			$start_date 					= $dto->format('Y-m-d');
			$dto->setISODate($year, $getEndWeek);
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}
		
		$shift_list = array(
			"DM","DP","DS"
		);
		
		$vendor_code 					= $this->input->get('vendor_code');
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		
		$data_list 						= $this->report_models->DataOntime($start_date, $end_date, $vendor_code);
		
		$date_list 						= $this->getDateList2($week, $year);
		$data_unloading					= $this->report_models->getDataUnloading($date_list, $data_list, $shift_list);
		$data_average					= $this->report_models->getAverageUnloading($date_list, $data_unloading, $shift_list);
		$this->report_models->download_on_time($data_list, $date_list, $data_unloading, $data_average, $shift_list, $year, $week, $date_list, $vendor_code);

	}

	public function download_unloading(){
		$year 							= $this->input->get('year');
		$week 							= $this->input->get('week');
		$dto 							= new DateTime();
		if(empty($week)){
			$year = date('Y');
			$week = date('W');
			$dto->setISODate($year, $week);
			$start_date 					= $dto->format('Y-m-d');
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}else{
			if(is_array($week)){
				sort($week);
				$getWeek = $week[0];
				$getEndWeek = $week[count($week)-1];
			}else{
				$getWeek = $week;
				$getEndWeek = $week;
			}	
			$dto->setISODate($year, $getWeek);
			$start_date 					= $dto->format('Y-m-d');
			$dto->setISODate($year, $getEndWeek);
			$dto->modify('+6 days');
			$end_date 						= $dto->format('Y-m-d');
		}
		
		$shift_list = array(
			"DM","DP","DS"
		);
		
		$vendor_code 					= $this->input->get('vendor_code');
		if($vendor_code == ''){
			$vendor_code = '21632';
		}
		
		$data_list 						= $this->report_models->DataOntime($start_date, $end_date, $vendor_code);
		
		$date_list 						= $this->getDateList2($week, $year);
		$data_unloading					= $this->report_models->getDataUnloading($date_list, $data_list, $shift_list);
		
		$data_average					= $this->report_models->getAverageUnloading($date_list, $data_unloading, $shift_list);
		$this->report_models->download_unloading($data_list, $date_list, $data_unloading, $data_average, $shift_list, $year, $week, $date_list, $vendor_code);

	}
}

?>