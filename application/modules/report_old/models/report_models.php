<?php

class report_models extends CI_Model{

	public function getReport($date_list, $week, $year, $vendor_code, $category="", $start_date = '', $end_date = ''){
		if($category=="ALL"){
			$category = "";
		}
		
		$no = 0;
		$noo = 1;
		$data = [];
		$shift_list = array(
			"DM", "DP", "DS"
		);
		$array_report = array();
		$getData = $this->db->query("SELECT a.id,
											a.po_number, 
											a.po_line_item, 
											a.material_code,
											b.material_name,
											a.requested_delivery_date AS rdd, 
											a.shift,
											d.rdd as delivery_date,
											a.qty,
											a.uom_plt,
											a.otif,
											a.qty_ori,
											e.arrive_date,
											f.shift AS shift_delivery
											FROM tb_rds_detail AS a 
											LEFT JOIN skin_master.ms_material AS b ON a.material_code = b. material_sku
											LEFT JOIN tb_scheduler AS c ON a.id = c.id_schedule 
											LEFT JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number
											INNER JOIN ms_time_slot AS f ON d.id_time_slot = f.id
											LEFT JOIN tb_delivery_detail AS e ON c.schedule_number = e.id_schedule_group
											WHERE
											d.rdd BETWEEN '$start_date' AND '$end_date'
											AND a.vendor_code = '$vendor_code'
											AND a.category LIKE '%$category%'
											GROUP BY a.id
											ORDER BY a.requested_delivery_date
											")->result();

		$array_report = array();
		$data_loss_tree = array();
		foreach ($getData as $get) {
			$check_partial = 0;
			$check_date = 0;
			$check_load_optimized = 0;
			$check_delay_1 = 0;
			$check_delay_2 = 0;
			$check_delay_3 = 0;
			$check_outstanding = 0;
			$check_cancel = 0;
			$check_array_cancel = 0;
			$check_reschedule = 0;
			$check_shift = 0;
			$first_read= 0;
			$data_rdd = 0;
			$qty_ori = $get->qty_ori;
			$qty_rev = $get->qty;
			$array_report[$no] = array(
				"id"				=> $get->id,
				"po_number" 		=> $get->po_number,
				"po_line_item" 		=> $get->po_line_item,
				"material_code"		=> $get->material_code,
				"material_name"		=> $get->material_name,
				"rdd"				=> $get->rdd,
				"delivery_date"		=> $get->delivery_date,
				"delivery_shift"	=> $get->shift_delivery,
				"shift"				=> $get->shift,
				"qty_ori"			=> $qty_ori,
				"qty_rev"			=> $qty_rev
			);
			$otif_shift = 0;
		 	foreach ($date_list as $date) {
		 		foreach ($shift_list as $shift) {
		 			$getDataShift = $this->db->query("SELECT 
		 				IF(SUM(a.receive_amount) > 0 , 
		 				SUM(a.receive_amount), 0) AS ra, 
		 				b.rdd,
		 				d.arrived_id_time_slot 
					  FROM tb_scheduler AS a
					  LEFT JOIN tb_schedule_detail AS b ON a.schedule_number = b.schedule_number
					  INNER JOIN ms_time_slot AS c ON b.id_time_slot = c.id
					  LEFT JOIN tb_delivery_detail AS d ON a.schedule_number = d.id_schedule_group
					  WHERE a.id_schedule = '$get->id' 
					  AND c.shift = '$shift' 
					  AND b.rdd = '$date' 
					  AND b.vendor_code = '$vendor_code' 
					  GROUP BY a.id_schedule
						  ")->row();
		 			
		 			if(isset($getDataShift->ra) && $getDataShift->ra > 0){
		 				$data_rdd = $getDataShift->ra;
		 				$dataShift = round($getDataShift->ra*$get->uom_plt,3);
		 				$get->shift = str_replace(" ", "", $get->shift);
		 				if($get->rdd == $date && $get->shift == $shift){
		 					$otif_shift = $otif_shift + $dataShift;
		 					if($qty_rev > $dataShift && $dataShift > 0){
		 						$check_partial = 1;
		 					}else{
		 						$check_partial = 0;
		 					}
		 				}else if($get->rdd == $date){
		 					
		 					if($get->shift == "DP" && $shift == "DM"){
		 						$check_load_optimized = 1;
		 					}else if($get->shift == "DS" && $shift != "DS"){
		 						$check_load_optimized = 1;
		 					}else if($get->shift =="DM" && $shift == "DP"){
		 						$check_delay_1 = 1;
		 					}else if($get->shift =="DM" && $shift == "DS"){
		 						$check_delay_2 = 1;
		 						
		 					}else if($get->shift =="DP" && $shift == "DS"){
		 						$check_delay_1 = 1;
		 					}
		 				}else if($get->rdd < $date){
		 					$dto = new DateTime();
		 					$ddatez 	= $get->delivery_date;
							$datez 	= new DateTime($ddatez);
							$weeks 	= $datez->format("W");
							$year 	= date('Y');
							$weekz 	= date('W');
							$dto->setISODate($year, $weekz);
							$start_date = $dto->format('Y-m-d');

		 					$earlier = new DateTime($get->rdd);
							$later = new DateTime($date);
							$diff = $later->diff($earlier)->format("%a");
							
		 					if($diff <= 1){

		 						if($get->shift == "DS" && $shift == "DM"){
		 							$check_delay_1 = 1;
			 					}else if($get->shift == "DS" && $shift == "DP"){
			 						$check_delay_2 = 1;
			 					}else if($get->shift == "DP" && $shift =="DP"){
			 						$check_delay_2 = 1;
			 					}else{
			 						$check_delay_3 = 1;
			 					}
		 					}else{
		 						if($get->rdd < $start_date){
		 							$check_outstanding = 1;
		 						}else{
		 							$check_delay_3 = 1;
		 						} 
		 						
		 					}
		 				}else if($get->rdd > $date){
		 					$check_load_optimized = 1;
		 				}else if(date('W', strtotime($get->rdd)) < date('W', strtotime($date)) ){
		 					$check_outstanding = 1;
		 				}
		 			}else{
		 				$dataShift = 0;

		 			}
		 			
		 			$array_report[$no][$date][$shift] = $dataShift;
		 		}
		 	}

		 	if($dataShift > 0 ){
		 		$data_loss_tree[$get->po_number.$get->po_line_item.$date.$shift] = 1;
		 	}
		 	
		 	if($qty_ori == $qty_rev){
		 		$qty_status = "original";
		 	}else{
		 		$qty_status = "Revised Qty";
		 	}

		 	$loss_tree_status = 0;
		 	if($check_partial == 0 && $check_load_optimized == 0 && $check_delay_1 == 0 && $check_delay_2 == 0 && $check_delay_3 == 0 && $check_outstanding == 0 && $get->arrive_date != '' && $data_rdd > 0){
		 		$loss_tree_status = '';
		 	}
		 	else if($check_partial > 0){
			 	$loss_tree_status = 1;
			}else if($check_load_optimized > 0){
				$loss_tree_status = 2;
			}else if($check_delay_1 > 0){
				$loss_tree_status = 4;
			}else if($check_delay_2 > 0){
				$loss_tree_status = 5;
			}else if($check_delay_3 > 0){
				$loss_tree_status = 3;
			}else if($check_outstanding > 0){
				$loss_tree_status = 6;
			}else if($get->arrive_date == ''){
				$loss_tree_status = 7;
			}else{
				$loss_tree_status = 8;
			}
			
		 	$noo++;
		 	$array_report[$no]["otif_shift"] = $otif_shift;
		 	$array_report[$no]["otif_quantity"] = $qty_rev - $otif_shift;
		 	$array_report[$no]["otif"] = ABS(round((($get->qty - ($get->qty - $otif_shift)) / $get->qty) * 100, 2) ) ;
		 	$array_report[$no]["otif_loss_tree"] = $loss_tree_status;
		 	$array_report[$no]["qty_status"] = $qty_status;
		 	$no++;
		 }

		 return $array_report;
	}

	public function getDataOtif($date_list, $array_report, $vendor_code, $week = 0, $category = '', $year, $data_otif_loss_tree){
		if($category=="ALL"){
			$category = "";
		}

		$shift_list = array(
			"DM", "DP", "DS"
		);
		$data = [];
		$total = 0;
		$total_actual = 0;
		$check_data = false;
		$data_otif = array();
		$check_week_now = date('YW');
		$total_outstanding = 0;
		$total_os_week = array();
		if(!is_array($week)){
			$week = array(
				0 => $week
			);
		}

		foreach($week as $weekz){
			$data_otif["outstanding_week"][$weekz] = 0;
			$data_otif["total_week"][$weekz] = 0;
			$data_otif["total_actual_week"][$weekz] = 0;
		}
		// $check_data_week = $year.$week;
		// if($check_week_now <= $check_data_week){
		// 	$check_data = false;
		// }else{
		// 	$check_data = true;
		// }
		
		// $check_avaible = $this->db->query("SELECT id FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) ='$year' AND vendor_code = '$vendor_code' ")->num_rows();
		// if($check_avaible > 0){

		// }else{
		
			foreach ($date_list as $date) {
				foreach ($shift_list as $shift) {
					$data_otif[$date]["plan"][$shift] = 0;
				}
			}

			foreach($array_report as $data){
				//dump($data);
				$ddate = $data["delivery_date"];
				$date = new DateTime($ddate);
				$weeks = $date->format("W");
				
				$dto = new DateTime();
				$dto->setISODate($year, $weeks);
				$start_date = $dto->format('Y-m-d');
				if($data["rdd"] < $start_date){					
					$total_outstanding = $total_outstanding + $data["qty_rev"];
					$data_otif["outstanding_week"][$weeks] = $total_outstanding;
				}else{
					$data_shift = str_replace(" ", "", $data["delivery_shift"]);
					$data_otif[$data["delivery_date"]]["plan"][$data_shift] = $data_otif[$data["delivery_date"]]["plan"][$data_shift] + $data["qty_rev"];
					$total = $total + $data["qty_rev"];
					$data_otif["total_week"][$weeks] = $data_otif["total_week"][$weeks] + $data["qty_rev"];

				}
				$total_actual = $total_actual + ( $data["qty_rev"] - ABS($data["qty_rev"] - $data["otif_shift"]) );
			}

			foreach($week as $weekz){
				$data_otif["total_week"][$weekz] = $data_otif["total_week"][$weekz] + $data_otif["outstanding_week"][$weekz]; 
			}

			$total = $total +$total_outstanding;
			foreach ($date_list as $date) {
				$DM = 0;
				$DP = 0;
				$DS = 0;
				$data_otif[$date]["actual"]["DM"] = $DM;
				$data_otif[$date]["actual"]["DP"] = $DP;
				$data_otif[$date]["actual"]["DS"] = $DS; 
				foreach ($array_report as $get) {
					$DM = $DM + (($get[$date]["DM"]*$get["otif"])/100);
					$DP = $DP + (($get[$date]["DP"]*$get["otif"])/100);
					$DS = $DS + (($get[$date]["DS"]*$get["otif"])/100);
					$data_otif[$date]["actual"]["DM"] = round($DM,3);
					$data_otif[$date]["actual"]["DP"] = round($DP,3);
					$data_otif[$date]["actual"]["DS"] = round($DS,3);
					 
				}
				$otif_DM = $data_otif[$date]["plan"]["DM"];
				$otif_DP = $data_otif[$date]["plan"]["DP"];
				$otif_DS = $data_otif[$date]["plan"]["DS"];

				if($otif_DM > 0 ){
					$oDM = ABS(ROUND(($otif_DM - ABS($DM - $otif_DM))/$otif_DM*100,1))."%";
					
				}else{
					$oDM = "0%";
				}

				if($otif_DP > 0 ){
					$oDP = ABS(ROUND(($otif_DP - ABS($DP - $otif_DP))/$otif_DP*100,1))."%";
					
				}else{
					$oDP = "0%";
				}

				if($otif_DS > 0 ){
					$oDS = ABS(ROUND(($otif_DS - ABS($DS - $otif_DS))/$otif_DS*100,1))."%";
				}else{
					$oDS = "0%";
				}
				$data_otif[$date]["otif"]["DM"] = $oDM;
				$data_otif[$date]["otif"]["DP"] = $oDP;
				$data_otif[$date]["otif"]["DS"] = $oDS;
			}
			
			$data_otif["total_actual"] 	= ROUND($total_actual,3);
			if($total > 0){
				$total_otif = ROUND(($total_actual / $total) * 100)."%";
			}else{
				$total_otif = "0%";
			}

			$data_otif["outstanding"]	= $total_outstanding;
			$data_otif["total"] 		= $total;
			$data_otif["total_otif"]	= $total_otif;
		// }
		// $category = array(
		// 	"plan", "actual", "otif"
		// );
		// $data_save= array();
		
		// if($check_avaible == 0 && $check_data == true){
		// 	foreach($date_list as $date){
		// 		$data_save = array(
		// 						"week" 					=> $week,
		// 						"date"					=> $date,
		// 						"vendor_code"			=> $vendor_code
		// 		);
		// 			foreach($category as $ctr){
		// 				foreach ($shift_list as $shift) {
		// 					$data_save[$ctr."_".$shift] = $data_otif[$date][$ctr][$shift];
		// 				}
		// 			}
				//$this->db->insert("tb_otif_loss_tree", $data_save);
			// }
			//dump("saved");
		// }
		return $data_otif;
	}

	public function getSavedOtif($date_list, $vendor_code, $week, $category, $year){
		$data = array();
		$category = array(
			"plan", "actual", "otif"
		);
		$shift_list = array(
			"DM", "DP", "DS"
		);
		if(is_array($week)){
			$week = $week[0];
		}
		$getData = $this->db->query("SELECT * FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) = '$year' AND vendor_code = '$vendor_code' ")->result();
		foreach($date_list as $date){
			foreach ($category as $ctr) {
				foreach ($shift_list as $shift) {
					$data[$date][$ctr][$shift] = 0;
				}
			}
		}
		foreach($getData as $get){
			$data[$get->date]["plan"]["DM"] = $get->plan_dm;
			$data[$get->date]["plan"]["DP"] = $get->plan_dp;
			$data[$get->date]["plan"]["DS"] = $get->plan_ds;

			$data[$get->date]["actual"]["DM"] = $get->actual_dm;
			$data[$get->date]["actual"]["DP"] = $get->actual_dp;
			$data[$get->date]["actual"]["DS"] = $get->actual_ds;

			$data[$get->date]["otif"]["DM"] = $get->otif_dm."%";
			$data[$get->date]["otif"]["DP"] = $get->otif_dp."%";
			$data[$get->date]["otif"]["DS"] = $get->otif_ds."%";

		}
		$getTotalDataPlan = $this->db->query("SELECT ( SUM(plan_dm) + SUM(plan_dp) + SUM(plan_ds)) AS plan FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) = '$year' AND vendor_code = '$vendor_code' GROUP BY week")->row()->plan;
		$getTotalDataActual = $this->db->query("SELECT ( SUM(actual_dm) + SUM(actual_dp) + SUM(actual_ds)) AS actual FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) = '$year' AND vendor_code = '$vendor_code' GROUP BY week")->row()->actual;
		if($getTotalDataPlan > 0){
			$getTotalDataOtif = ABS(round(($getTotalDataPlan - ABS($getTotalDataActual - $getTotalDataPlan))/$getTotalDataPlan*100))."%";
		}else{
			$getTotalDataOtif = "0%";
		}
		$data["total"] = $getTotalDataPlan;
		$data["total_actual"] = $getTotalDataActual;
		$data["total_otif"] = $getTotalDataOtif;
		
		return $data;
	}

	public function otifLossTree($array_report, $week, $year){
		$get_otif = $this->db->query("SELECT id, loss_tree FROM ms_otif_loss_code");
		$count_ms_otif = $get_otif->num_rows();
		$loss_tree = $get_otif->result();
		$data = array();
		$otifTotal = array();
		$total_otif_loss = 0;
		if(!is_array($week)){
			$week = array(
				0 => $week
			);
		}

		$data_report = array();
		$weeks = '';

		foreach($week as $weekz){
			$date_listz = $this->getDateList($weekz, $year);
			foreach($date_listz as $date){
				for ($i=0; $i < $count_ms_otif ; $i++) {
					$otifTotal[$weekz][$loss_tree[$i]->loss_tree] = 0;
					$otifTotal[$weekz][$date][$loss_tree[$i]->loss_tree] = 0;
				}
			}
		}
		foreach($array_report as $data){
			$ddate = $data["delivery_date"];
			$date = new DateTime($ddate);
			$weeks = $date->format("W");
			if($data["otif_loss_tree"] != ''){
				$otif_loss = $loss_tree[$data["otif_loss_tree"]-1]->loss_tree;
				$otifTotal[$weeks][$otif_loss] = $otifTotal[$weeks][$otif_loss] + $data["otif_quantity"];
				$otifTotal[$weeks][$ddate][$otif_loss] = $otifTotal[$weeks][$ddate][$otif_loss] + $data["otif_quantity"]; 
			}
			
			if($weeks != ''){
				$otifTotal[$weeks][$date_listz[0]]["Outstanding Week"] = $otifTotal[$weeks]["Outstanding Week"];
			}else{
				$otifTotal[$weekz][$date_listz[0]]["Outstanding Week"] = 0;
			}
		}

		return $otifTotal;
	}

	public function getDataOtifWeekly($data_report, $week, $year){
		$array_report = array();
		if(!is_array($week)){
			$week = array(
				0 => $week
			);
		}
		$no = 0;
		
		foreach($week as $weekz){
			$DM = 0;
			$DP = 0;
			$DS = 0;
			$date_list = $this->getDateList($weekz, $year);
			foreach($date_list as $date){
				$array_report[$weekz][$date] = $data_report[$date];
				$DM = $data_report[$date]["plan"]["DM"] + $DM;
				$DP = $data_report[$date]["plan"]["DP"] + $DP;
				$DS = $data_report[$date]["plan"]["DS"] + $DS;
			}
			$array_report[$weekz]["total"] = $DM + $DP + $DS;
		$no++;
		}
		return $array_report;
	}

	public function getWeeklyDataReport($data_report, $week, $year){
		if(!is_array($week)){
			$week = array(
				0 => $week
			);
		}
		$no = 0;
		$array_report = array();
		foreach($week as $weekz){
			$array_report[$weekz] = [];
			$date_list[$weekz] = $this->getDateList($weekz, $year);
		}
		foreach($data_report as $data){
			$ddate = $data["delivery_date"];
			$date = new DateTime($ddate);
			$weeks = $date->format("W");
			$array_report[$weeks][$no++] = $data; 
		}
		return $array_report;
	}

	public function getDataVendor(){
		$data = $this->db->query("SELECT a.id, a.vendor_code, a.vendor_name, a.vendor_alias FROM skin_master.ms_supplier AS a")->result();
		return $data;
	}

	public function getVendor($vendor_code){
		$data = $this->db->query("SELECT a.id, a.vendor_code, a.vendor_name, a.vendor_alias FROM skin_master.ms_supplier AS a WHERE vendor_code = '$vendor_code' ")->row();
		return $data;
	}

	public function getDataMsOtif(){
		$data = $this->db->query("SELECT a.id, a.loss_tree FROM ms_otif_loss_code AS a")->result();
		return $data;
	}

	public function DataOntime($start_date, $end_date, $vendor_code = '', $link =''){
		if($link == "on_time"){
			$getRdd = "c.set_rdd";
			$joinIdTime = "c.set_id_time_slot";
		}else{
			$getRdd = "c.rdd";
			$joinIdTime = "c.id_time_slot";
		}
		$data = $this->db->query("SELECT a.receipt, 
								a.arrive_date, 
								a.arrive_time, 
								a.receive_date, 
								a.receive_time,
								b.police_number, 
								d.start_time, 
								d.end_time,
								CASE
									WHEN a.arrive_date < c.rdd
										THEN  'Early'
									WHEN a.arrive_date = c.rdd AND a.arrive_time < d.start_time
										THEN 'Early'
									WHEN a.arrive_date > c.rdd
										THEN 'Delay'
									WHEN a.arrive_date = c.rdd AND a.arrive_time > d.end_time
										THEN 'Delay'
									ELSE 'Good'
								END AS status,
								CASE
									WHEN a.arrive_date < c.rdd
										THEN  @early := @early + 1
									WHEN a.arrive_date = c.rdd AND a.arrive_time < d.start_time
										THEN @early := @early +1
									WHEN a.arrive_date > c.rdd
										THEN @delay := @dealy +1
									WHEN a.arrive_date = c.rdd AND a.arrive_time > d.end_time
										THEN @delay := @delay + 1
									ELSE @good := @good + 1
								END AS counter,
								@early AS early,
								@delay AS delay,
								@good AS good,
								$getRdd AS rdd, 
								d.shift 
								FROM tb_delivery_detail AS a 
								LEFT JOIN ms_truck AS b ON a.id_truck = b.id 
								INNER JOIN tb_schedule_detail AS c ON a.id_schedule_group = c.schedule_number
								INNER JOIN ms_time_slot AS d ON $joinIdTime = d.id
								CROSS JOIN (SELECT @early := 0) AS e
								CROSS JOIN (SELECT @delay := 0) AS f
								CROSS JOIN (SELECT @good := 0) AS g
								WHERE c.rdd BETWEEN '$start_date' AND '$end_date' AND a.arrive_date != '' AND a.receive_date != '' AND c.vendor_code LIKE '%$vendor_code%'
								 ")->result();
		
		return $data;
	}

	public function getDataUnloading($date_list, $data_list, $shift_list){
		$data = array();
		$no = 0;
		foreach($data_list as $getData){
			$arrived = strtotime($getData->arrive_date.' '.$getData->arrive_time);
			$received = strtotime($getData->receive_date.' '.$getData->receive_time);
			$total = ABS(($received - $arrived) / 60);
			
			foreach ($date_list as $date) {
				foreach ($shift_list as $shift) {
					$data[$no][$date][$shift] = '';
				}
			}

			$data[$no][$getData->rdd][$getData->shift] = $total;
			$no++;
			 
		}
		return $data;
	}

	public function getAverageUnloading($date_list, $data_unloading, $shift_list){
		$data = array();
		$total = 0;
		$Unloading_DP = 0;
		$Unloading_DM = 0;
		$Unloading_DS = 0;
		$data["DM"] = 0;
		$data["DP"] = 0;
		$data["DS"] = 0;
		$data["average"] = 0;

		foreach ($date_list as $date) {
			foreach ($shift_list as $shift) {

				if(count($data_unloading) > 0){
					for($i = 0; $i< count($data_unloading); $i++ ){
						$data[$date][$shift] = 0;
					}	
				}else{
					for($i = 0; $i< 1; $i++ ){
						$data[$date][$shift] = 0;
					}
				}
				
			}
		}

		$count_DM = 0;
		$count_DP = 0;
		$count_DS = 0;
		if(!empty($data_unloading) ){
			foreach ($date_list as $date) {
				foreach ($shift_list as $shift) {
					for($i = 0; $i< count($data_unloading); $i++ ){
						$total = $total + $data_unloading[$i][$date][$shift];
						if($shift == 'DM'){
							$Unloading_DM = $Unloading_DM + $data_unloading[$i][$date][$shift];
							if($data_unloading[$i][$date][$shift] > 0){
								$count_DM++;
							}
						}else if($shift == 'DP'){
							$Unloading_DP = $Unloading_DP + $data_unloading[$i][$date][$shift];
							if($data_unloading[$i][$date][$shift] > 0){
								$count_DP++;
							}
						}else{
							$Unloading_DS = $Unloading_DS + $data_unloading[$i][$date][$shift];
							if($data_unloading[$i][$date][$shift] > 0){
								$count_DS++;
							}
						}
						$data[$date][$shift] = $data[$date][$shift] + $data_unloading[$i][$date][$shift];
					}
					$data[$date][$shift] = $data[$date][$shift];
				}
			}
		if($count_DM == 0){
			$count_DM = 1;
		}
		if($count_DP == 0){
			$count_DP = 1;
		}
		if($count_DS == 0){
			$count_DS = 1;
		}
		$data["DM"] = ($Unloading_DM / $count_DM);
		$data["DP"] = ($Unloading_DP / $count_DP);
		$data["DS"] = ($Unloading_DS / $count_DS);
		$data["average"] = ($total / count($data_unloading));
		}
		return $data;
	}

	public function getDataChart($data_average, $week, $year){
		$getDateTime = new DateTime();
		$total_week = count($week);
		$data = array();
		$date = array();
		if($total_week == 1){
			if(is_array($week)){
				$week = $week[0];
			}
			$total_DM = 0;
			$total_DP = 0;
			$total_DS = 0;

			$getDate = $getDateTime->setISODate($year, $week)->format('Y-m-d');
			$date[0][$week]["DM"] = $total_DM + $data_average[$getDate]["DM"];
			$date[0][$week]["DP"] = $total_DP + $data_average[$getDate]["DP"];
			$date[0][$week]["DS"] = $total_DS + $data_average[$getDate]["DS"];
			$w = 1;
			
			while($w < 6)	{
				$getDate = $getDateTime->modify('+1 days')->format('Y-m-d');
				$total_DM = $total_DM + $data_average[$getDate]["DM"];
				$total_DP = $total_DP + $data_average[$getDate]["DP"];
				$total_DS = $total_DS + $data_average[$getDate]["DS"];
				$date[0][$week]["DM"] = $total_DM;
				$date[0][$week]["DP"] = $total_DP;
				$date[0][$week]["DS"] = $total_DS;
			  	$w++;
			}

		}else{
			for($i=0; $i < $total_week; $i++){
				$getDate = $getDateTime->setISODate($year, $week[$i])->format('Y-m-d');
				$w = 1;
				$total_DM = 0;
				$total_DP = 0;
				$total_DS = 0;
				while($w < 7)	{
					$getDate = $getDateTime->modify('+1 days')->format('Y-m-d');
					$total_DM = $total_DM + $data_average[$getDate]["DM"];
					$total_DP = $total_DP + $data_average[$getDate]["DP"];
					$total_DS = $total_DS + $data_average[$getDate]["DS"];
					$date[$i][$week[$i]]["DM"] = $total_DM;
					$date[$i][$week[$i]]["DP"] = $total_DP;
					$date[$i][$week[$i]]["DS"] = $total_DS;
				  	$w++;
				}
			}
		}
		return $date;
	}

	public function total_date($data_otif, $date_list){
		$shift_list = array(
			"DM", "DP", "DS"
		);
		$no=0;
		
		foreach ($date_list as $date) {
			$average_count = 0;
			$average = 0;
			$average2 = 0;
			$average3 = 0;
			foreach ($shift_list as $shift) {

				$data_list =str_replace('%','',($data_otif[$date]["otif"][$shift]));
				if($data_list > 1){
					$average = $average + $data_list;
					$average_count++;	
				}
				
				$data_list2 =str_replace('%','',($data_otif[$date]["actual"][$shift]));
				$average2 = $average2 + $data_list2;
				$data_list3 =str_replace('%','',($data_otif[$date]["plan"][$shift]));
				$average3 = $average3 + $data_list3;
					
			}
			if($average_count == 0){
				$average_count = 1;
			}
			$total_date[$no]["otif"] = round($average,3);
			$total_date[$no]["actual"] = round($average2,3);
			$total_date[$no]["plan"] = round($average3,3);
			if($average2 > 0){
				$total_date[$date]["total_plan"] = $average3;
				$total_date[$date]["total_actual"] = $average2;
				$total_date[$date]["total_otif"] = ROUND(($average2/$average3)*100);	
			}else{
				$total_date[$date]["total_plan"] =0;
				$total_date[$date]["total_otif"] =0;
			}
			 
			$no++;
		}
		//dump($total_date);
		return $total_date;
	}

	public function getDateList($week, $year){
		$getDateTime = new DateTime();
		if(!is_array($week)){
			$waktu[0] = $getDateTime->setISODate($year, $week)->format('Y-m-d');
			$i = 1;
			while($i < 7){
				$waktu[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
			  	$i++;
			}
			return $waktu;
		}else{
			$total_week = count($week);
			$frist_week = $week[0];
			$end_week = $week[$total_week-1];
			$waktu[0] = $getDateTime->setISODate($year, $week[0])->format('Y-m-d');
			$i = 1;
			while($i <= ($total_week*7)-1)	{
				$waktu[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
			  	$i++;
			}
			return $waktu;
		}
		
	}

	public function getWeeklyDataOntime($data_list, $week){
		if(!is_array($week)){
			$week = array(
				0 => $week
			);
		}
		$data = array();
		$no = 0;
		foreach($week as $weekz){
			$data[$weekz] = [];
		}
		foreach($data_list as $dataz){
			$ddate = $dataz->rdd;
			$date  = new DateTime($ddate);
			$weeks = $date->format("W");
			$data[$weeks][$no++] = $dataz;
		}
		return $data;
	}

	public function download_otif($data_report, $year, $week, $date_list, $vendor_code, $data_select_otif){
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		$filePath = "./assets/templates/template_otif2.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $otif_loss_tree = $this->db->query("SELECT * FROM ms_otif_loss_code")->result();
        $shift = array(
        	"DM", "DP", "DS"
        );
        $vendor_alias = $this->db->query("SELECT vendor_alias FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code'")->row()->vendor_alias;
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', date('Y-m-d H:i:s'));
        $objPHPExcel->getActiveSheet()->SetCellValue('C2', $year);
        $objPHPExcel->getActiveSheet()->SetCellValue('C3', $week);
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', "TIME SLOT REPORT - ".$vendor_alias." - WEEK".$week);
        
        $objPHPExcel->getActiveSheet()->SetCellValue('K10', $date_list[0]);
        $objPHPExcel->getActiveSheet()->SetCellValue('N10', $date_list[1]);
        $objPHPExcel->getActiveSheet()->SetCellValue('Q10', $date_list[2]);
        $objPHPExcel->getActiveSheet()->SetCellValue('T10', $date_list[3]);
        $objPHPExcel->getActiveSheet()->SetCellValue('W10', $date_list[4]);
        $objPHPExcel->getActiveSheet()->SetCellValue('Z10', $date_list[5]);
        $objPHPExcel->getActiveSheet()->SetCellValue('AC10', $date_list[6]);

        //unique date shift
        $objPHPExcel->getActiveSheet()->SetCellValue('K8', date('Ymd',strtotime($date_list[0])).'1' );
        $objPHPExcel->getActiveSheet()->SetCellValue('L8', date('Ymd',strtotime($date_list[0])).'2' );
        $objPHPExcel->getActiveSheet()->SetCellValue('M8', date('Ymd',strtotime($date_list[0])).'3' );
        $objPHPExcel->getActiveSheet()->SetCellValue('N8', date('Ymd',strtotime($date_list[1])).'1' );
        $objPHPExcel->getActiveSheet()->SetCellValue('O8', date('Ymd',strtotime($date_list[1])).'2' );
        $objPHPExcel->getActiveSheet()->SetCellValue('P8', date('Ymd',strtotime($date_list[1])).'3' );
        $objPHPExcel->getActiveSheet()->SetCellValue('Q8', date('Ymd',strtotime($date_list[2])).'1' );
        $objPHPExcel->getActiveSheet()->SetCellValue('R8', date('Ymd',strtotime($date_list[2])).'2' );
        $objPHPExcel->getActiveSheet()->SetCellValue('S8', date('Ymd',strtotime($date_list[2])).'3' );
        $objPHPExcel->getActiveSheet()->SetCellValue('T8', date('Ymd',strtotime($date_list[3])).'1' );
        $objPHPExcel->getActiveSheet()->SetCellValue('U8', date('Ymd',strtotime($date_list[3])).'2' );
        $objPHPExcel->getActiveSheet()->SetCellValue('V8', date('Ymd',strtotime($date_list[3])).'3' );
        $objPHPExcel->getActiveSheet()->SetCellValue('W8', date('Ymd',strtotime($date_list[4])).'1' );
        $objPHPExcel->getActiveSheet()->SetCellValue('X8', date('Ymd',strtotime($date_list[4])).'2' );
        $objPHPExcel->getActiveSheet()->SetCellValue('Y8', date('Ymd',strtotime($date_list[4])).'3' );
        $objPHPExcel->getActiveSheet()->SetCellValue('Z8', date('Ymd',strtotime($date_list[5])).'1' );
        $objPHPExcel->getActiveSheet()->SetCellValue('AA8', date('Ymd',strtotime($date_list[5])).'2' );
        $objPHPExcel->getActiveSheet()->SetCellValue('AB8', date('Ymd',strtotime($date_list[5])).'3' );
        $objPHPExcel->getActiveSheet()->SetCellValue('AC8', date('Ymd',strtotime($date_list[6])).'1' );
        $objPHPExcel->getActiveSheet()->SetCellValue('AD8', date('Ymd',strtotime($date_list[6])).'2' );
        $objPHPExcel->getActiveSheet()->SetCellValue('AE8', date('Ymd',strtotime($date_list[6])).'3' );
        //
        $no = 1;
        $row= 18;
        $assc_otif = array();
        foreach($data_select_otif as $data){
        	$assc_otif[$data->loss_tree] = 0;
        }

        foreach($data_report as $data){
        	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $no);
        	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $data["po_number"]);
        	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $data["po_line_item"]);
        	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $data["material_code"]);
        	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $data["material_name"]);
        	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $data["rdd"]);
        	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $data["shift"]);
        	$shift_code = 1;
        	if($data["shift"] == "DP"){
        		$shift_code = 2;
        	}else if($data["shift"] == "DS"){
        		$shift_code = 3;
        	}
        	$unique = date('Ymd', strtotime($data['rdd'])).$shift_code;
        	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $unique);
        	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $data["qty_rev"]);
        	$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $data["qty_status"]);
        	$alfa = 'K';
        	foreach($date_list as $date){
        		foreach($shift as $sh){
        			$objPHPExcel->getActiveSheet()->SetCellValue($alfa.$row, $data[$date][$sh]);
        			if($data[$date][$sh] == $data["qty_rev"]){
	                	$color = array('rgb' => '56f563');
		            }else if($data[$date][$sh] == 0){
		                $color = array('rgb' => 'ffffff');
		            }else{
		            	$color = array('rgb' => 'f7b634');
		            }
		            $objPHPExcel->getActiveSheet()->getStyle($alfa++.$row)->applyFromArray(
					    array(
					        'fill' => array(
					            'type' => PHPExcel_Style_Fill::FILL_SOLID,
					            'color' => $color
					        )
					    )
					);
        		}
        	}

        	$objPHPExcel->getActiveSheet()->SetCellValue('AF'.$row, $data["otif_shift"]);
        	$objPHPExcel->getActiveSheet()->SetCellValue('AG'.$row, $data["otif"]/100);
        	$otif_loss = "";
 			//dump($no." : ".$data['otif_loss_tree']);
        	
        	if($data['otif_loss_tree'] > 0){
        		$otif_loss = $otif_loss_tree[$data['otif_loss_tree']-1]->loss_tree;
        		$assc_otif[$otif_loss] = $assc_otif[$otif_loss] + ($data["qty_rev"] - $data["otif_shift"]);
        		
        	}
        	$objPHPExcel->getActiveSheet()->SetCellValue('AH'.$row, $otif_loss );
        	$row++;
        	$no++;
        }
        arsort($assc_otif);
        $row_otif = 6;
        foreach($assc_otif as $otif => $key){
        	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row_otif++, $otif);
        }
        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="report_otif_'."Y".$year."_W".$week.'.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');
	}

	public function download_on_time($data_list, $date_list, $data_unloading, $data_average, $shift_list, $year, $week, $date_list, $vendor_code){
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		$filePath = "./assets/templates/template_on_time.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $vendor_name = $this->db->query("SELECT vendor_alias FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->row()->vendor_alias;
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', date('Y-m-d'));
        $objPHPExcel->getActiveSheet()->SetCellValue('C2', $year);
        $objPHPExcel->getActiveSheet()->SetCellValue('C3', $week);
        $objPHPExcel->getActiveSheet()->SetCellValue('B5', "TIME SLOT REPORT -".$vendor_name."- SLOT EFFICIENCY");

        if(count($data_list) > 0){
        	$early_data = (count($data_list) > 0)?$data_list[count($data_list)-1]->early:0;
        	$good_data = (count($data_list) > 0)?$data_list[count($data_list)-1]->good:0; 
        	$delay_data = (count($data_list) > 0)?$data_list[count($data_list)-1]->delay:0;

          	$early = round(($data_list[count($data_list)-1]->early/count($data_list))*100);
          	$good = round(($data_list[count($data_list)-1]->good/count($data_list))*100);
          	$delay = round(($data_list[count($data_list)-1]->delay/count($data_list))*100);
      	}else{
        	$early = 0;
        	$good = 0;
        	$delay = 0;
      	}
      	$objPHPExcel->getActiveSheet()->SetCellValue('J3', $early_data);
      	$objPHPExcel->getActiveSheet()->SetCellValue('J4', $good_data);
      	$objPHPExcel->getActiveSheet()->SetCellValue('J5', $delay_data);

      	$objPHPExcel->getActiveSheet()->SetCellValue('K3', $early);
      	$objPHPExcel->getActiveSheet()->SetCellValue('K4', $good);
      	$objPHPExcel->getActiveSheet()->SetCellValue('K5', $delay);

        $no = 1;
        $row = 9;
        foreach($data_list as $data){
        	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $no);
        	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $data->receipt);
        	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $data->police_number);
        	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $data->rdd);
        	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $data->start_time." - ".$data->end_time);
        	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $data->arrive_date.' - '.$data->arrive_time);
        	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $data->receive_date.' - '.$data->receive_time);
        		if($data->status == "Early"){
	              $color = array('rgb' => 'ff9900');
	            }else if($data->status =="Delay"){
	              $color = array('rgb' => 'cc0000');
	            }else{
	              $color = array('rgb' => '32a852');
	            }
          	$objPHPExcel->getActiveSheet()->getStyle('H'.$row)->applyFromArray(
			    array(
			        'fill' => array(
			            'type' => PHPExcel_Style_Fill::FILL_SOLID,
			            'color' => $color
			        )
			    )
			);
        	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $data->status);
        	$start_date = new DateTime($data->rdd.' '.$data->end_time);
            $since_start = $start_date->diff(new DateTime($data->arrive_date.' '.$data->arrive_time));
            if($since_start->days == 0){
              $days = '';
            }else{
              $days= $since_start->days.'d';
            }

            if($since_start->h < 10){
              $h = '0'.$since_start->h;
            }else{
              $h = $since_start->h;
            }

            if($since_start->i < 10){
              $i = '0'.$since_start->i;
            }else{
              $i = $since_start->i;
            }
        	$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $days.' '.$h.':'.$i);
        	$no++;
        	$row++;
        }

        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="report_unloading_time_'."Y".$year."_W".$week.'.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');
        }

        public function download_unloading($data_list, $date_list, $data_unloading, $data_average, $shift_list, $year, $week, $date_list, $vendor_code){
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		$filePath = "./assets/templates/template_unloading.xlsx";                        
        $objPHPExcel = IOFactory::load($filePath);
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $vendor_name = $this->db->query("SELECT vendor_alias FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->row()->vendor_alias;
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', date('Y-m-d'));
        $objPHPExcel->getActiveSheet()->SetCellValue('C2', $year);
        $objPHPExcel->getActiveSheet()->SetCellValue('C3', $week);
        $objPHPExcel->getActiveSheet()->SetCellValue('B5', "TIME SLOT REPORT -".$vendor_name."- SLOT EFFICIENCY");

        $objPHPExcel->getActiveSheet()->SetCellValue('I7', $date_list[0]);
        $objPHPExcel->getActiveSheet()->SetCellValue('l7', $date_list[1]);
        $objPHPExcel->getActiveSheet()->SetCellValue('O7', $date_list[2]);
        $objPHPExcel->getActiveSheet()->SetCellValue('R7', $date_list[3]);
        $objPHPExcel->getActiveSheet()->SetCellValue('U7', $date_list[4]);
        $objPHPExcel->getActiveSheet()->SetCellValue('X7', $date_list[5]);
        $objPHPExcel->getActiveSheet()->SetCellValue('AA7', $date_list[6]);
        
        $H = floor(($data_average["DM"]+$data_average["DP"]+$data_average["DS"])/3/60);
        $I = (($data_average["DM"]+$data_average["DP"]+$data_average["DS"])/3)%60;
        if($H < 10){
          $H = '0'.$H;
        }
        if($I < 10){
          $I = '0'.$I;
        }
        $average =  $H.':'.$I; 
        $objPHPExcel->getActiveSheet()->SetCellValue('K4', $average);

        if(count($data_list) > 0){
          	$early = round(($data_list[count($data_list)-1]->early/count($data_list))*100);
      	}else{
        	$early = 0;
      	}
      	$early_data = (count($data_list) > 0)?$data_list[count($data_list)-1]->early:0;
      	$objPHPExcel->getActiveSheet()->SetCellValue('R4', $early_data);

      	if(count($data_list) > 0){
	          $delay = round(($data_list[count($data_list)-1]->delay/count($data_list))*100);
	      }else{
	        $delay = 0;
	      }
	     $delay = (count($data_list) > 0)?$data_list[count($data_list)-1]->delay:0;

	     $objPHPExcel->getActiveSheet()->SetCellValue('R3', $delay);
	     if($delay > 0){
	     	$data_delay = $delay / ($early_data + $delay);
	     	$data_early = $early_data / ($early_data + $delay);
	     }else{
	     	$data_delay = 0;
	     	$data_early = 0;
	     }
	     $objPHPExcel->getActiveSheet()->SetCellValue('S3',  $data_delay);
	     $objPHPExcel->getActiveSheet()->SetCellValue('S4',  $data_early);
        $no = 1;
        $row = 9;
        foreach($data_list as $data){
        	$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $no);
        	$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $data->receipt);
        	$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $data->police_number);
        	$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $data->rdd);
        	$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $data->start_time." - ".$data->end_time);
        	$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $data->arrive_date.' - '.$data->arrive_time);
        	$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $data->receive_date.' - '.$data->receive_time);
        	$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $data->status);
        	$alfabeth = "I";
        	foreach($date_list as $date){
	          foreach($shift_list as $shift){
	          	if($data->status == "Early"){
	              $color = array('rgb' => 'ff9900');
	            }else if($data->status =="Delay"){
	              $color = array('rgb' => 'cc0000');
	            }else{
	              $color = array('rgb' => 'green');
	            }

	            $H = floor($data_unloading[$no-1][$date][$shift] / 60);
	            $I = ($data_unloading[$no-1][$date][$shift] % 60);
	            $D = '';

	            if($H < 10){
                $H = '0'.$H;
              }

              if($H > 24){
                $getD = round($H / 24);
                if($getD < 10){
                  $getD = '0'.$getD;
                }
                $D = $getD.' ';

                $H = $H % 24;

              }

              if($I < 10){
                $I = '0'.$I;
              }
              $data_time = $D.$H.':'.$I;
              if($data_unloading[$no-1][$date][$shift] > 0){
              	 $objPHPExcel->getActiveSheet()->getStyle($alfabeth.$row)->applyFromArray(
				    array(
				        'fill' => array(
				            'type' => PHPExcel_Style_Fill::FILL_SOLID,
				            'color' => $color
				        )
				    )
				);
              }
          
              $objPHPExcel->getActiveSheet()->SetCellValue($alfabeth.$row, $data_time);
              $alfabeth++;
              
	        }
	    }

        	$no++;
        	$row++;
        }
         


        header('Content-type: application/vnd.ms-excel');
        // It will be called file.xls
        header('Content-Disposition: attachment; filename="report_unloading_time_'."Y".$year."_W".$week.'.xlsx"');
        // Write file to the browser
        $objWriter->save('php://output');
        }

        public function getDataWeeklyOnTime($data_average, $week, $year){
        	$no = 0;
        	$number = 0;
        	$data = array();
        	$shift = array("DM","DP","DS");
        	if(!is_array($week)){
        		$week = array(
        			0 => $week
        		);
        	}
        	foreach($week as $weekz){
				$data_DM = 0;
				$data_DP = 0;
				$data_DS = 0;
				$date_list = $this->getDateList($weekz, $year);
				foreach($date_list as $date){
					$data_DM = $data_DM + $data_average[$date]["DM"];
					$data_DP = $data_DP + $data_average[$date]["DP"];
					$data_DS = $data_DS + $data_average[$date]["DS"];
				}
				$data[$weekz]["DM"] = $data_DM;
				$data[$weekz]["DP"] = $data_DP;
				$data[$weekz]["DS"] = $data_DS;
			}
			
			return $data;
        }

        public function getAverrageOntime($date_list, $data_list){
			
			$data = array();
			foreach($date_list as $date){
				$data[$date] = 0;
			}
			foreach($data_list as $get){
				$rdd = strtotime($get->rdd.' '.$get->end_time);
				$arrived = strtotime($get->arrive_date.' '.$get->arrive_time);
				$total = ABS(($arrived - $rdd)/60);
				$data[$get->rdd] = $data[$get->rdd] + $total;
			}
			return $data;

		}
	
}



?>