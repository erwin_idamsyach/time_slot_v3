<style>
  th{
    font-size:10px;
  }
  td{
    font-size:10px;
  }

  .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 0px 0px;
    vertical-align: middle;
    text-align: center;
    border-color: #ddd;
}

.selected {
    background: lightBlue
}
}
</style>
<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <div class="card card-chart">
            <?php 
              $year = $this->input->get('year');
              $week = $this->input->get('week');
              $vendor_code = $this->input->get('vendor_code');
              if(check_sub_menu(35)){
                $vendor_link = "&vendor_code=".$vendor_code;
              }else if(check_sub_menu(34)){
                $vendor_link = $this->session->userdata('sess_vendor_code');
              }
            ?>
            <div class="card-header card-header-success card-header-icon">
              <div class="row">
                <div class="col-4">
                  <div class="card-icon">
                  <i class="fas fa-chart-bar" style="font-size:30px"></i>
                  </div>
                  <h4 class="card-title">ON TIME SLOT REPORT - <?php echo $data_vendor->vendor_alias; ?></h4>
                </div>
                <div class="col">
                </div>
                <div class="col-2" <?php if(!check_sub_menu(33)){echo "hidden";} ?>>
                  <?php if(check_sub_menu(35)){
                      ?>
                    <select style="margin-top:10px" onchange="change_vendor(this.value)" name="select_vendor" class="form-control select2-init">
                      
                      
                        <?php foreach ($data_vendor_list as $get) {
                          ?>
                          <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_name." - ".$get->vendor_code; ?></option>
                          <?php
                        } ?>
                      <?php
                    } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-2 col-sm-12">
                  <div class="form-group" style="margin-top:15px">
                    <label style="margin-left:70px">Year</label>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination pagination-primary">
                          
                          <li class="page-item">
                            <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?><?php echo $vendor_link; ?>"><?php echo $year-1; ?></a>
                          </li>
                          
                          <li class="page-item active">
                            <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?><?php echo $vendor_link; ?>"><?php echo $year; ?></a>
                          </li>

                          <li class="page-item">
                            <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?><?php echo $vendor_link; ?>"><?php echo $year+1; ?></a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                </div>
                
                  <div class="col-8">
                  <div class="table-responsive" >
                    <table class="table" style="margin-top:15px">
                      <thead>
                        <tr>
                          <th>Month</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="2">
                            <table id="month" class="table" style="height: 30px">
                              <tr>
                                <td>
                                  <button value="1" style="width:55px" id="month1" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">JAN</button>
                                  <button value="2" style="width:55px" id="month2" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">FEB</button>
                                  <button value="3" style="width:55px" id="month3" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">MAR</button>
                                  <button value="4" style="width:55px" id="month4" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">APR</button>
                                  <button value="5" style="width:55px" id="month5" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">MEI</button>
                                  <button value="6" style="width:55px" id="month6" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">JUN</button>
                                  <button value="7" style="width:55px" id="month7" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">JUL</button>
                                  <button value="8" style="width:55px" id="month8" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">AGU</button>
                                  <button value="9" style="width:55px" id="month9" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">SEP</button>
                                  <button value="10" style="width:55px" id="month10" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">OKT</button>
                                  <button value="11" style="width:55px" id="month11" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">NOV</button>
                                  <button value="12" style="width:55px" id="month12" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">DES</button>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-2">
                  <div class="table-responsive" >
                    <table class="table table-hover" style="margin:0px; margin-top:12px">
                      <thead>
                        <tr>
                          <th>Pareto By Week</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <!-- Button trigger modal -->
                            <button type="button" style="width: 95%" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalLong">
                              WEEK
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Pareto By Week</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div id="modal_week">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    <div id="ontime_data_report">
    </div>
    
    <div id="loading_ontime">
      <div class="row">
        <div class="col">
          <div class="card card-chart">
            <div class="card-header card-header-info card-header-icon">
              <div class="row">
                <div class="col-4">
                  <div class="card-icon">
                    <i class="fas fa-chart-line" style="font-size:35px"></i>
                  </div>
                  <h4 class="card-title">Unloading Performance, AVERAGE :</h4>
                </div>
                <div class="col">
                </div>
              </div>
            </div>
            <div class="card-body" style="text-align: center">
              <img style="text-align: center" width="70" height="70" src="<?php echo base_url(); ?>assets/images/Loading.gif" >
            </div>
        </div> 
      </div>
    </div>
    </div>

</div>
</div>
</div>

<script>
  var year = <?php echo $this->input->get('year'); ?>;
  var category = '<?php echo $this->input->get('category'); ?>';
  var frist_month = 0;
  var last_month = 0;
  var month_diff = 0;
  var month = [];
  var week = [];
  var vendor_code = "21632";

  data_report();
  

  function CellClick(cell){
    if(frist_month == 0){
       month.push( parseInt($('#'+cell.id).attr("value")) );
      frist_month = $('#'+cell.id).attr('value');
       $('#'+cell.id).css('background-color','#00acc1');
       $('#'+cell.id).addClass('selectedMonth');
    }else{
      last_month = parseInt($('#'+cell.id).attr('value'));
      if (window.event.button === 0) {
        if (!window.event.ctrlKey && !window.event.shiftKey) {
            month = [];
            clear_month();
            month.push(parseInt($('#'+cell.id).attr("value")) );
            $('#'+cell.id).css('background-color','#00acc1');
            $('#'+cell.id).addClass('selectedMonth');

        }else if (window.event.shiftKey) {
          month = [];
          
            if(frist_month < last_month){
              for (var i = frist_month; i <= last_month; i++) {
                month.push(i);
                $('#month'+i).css('background-color','#00acc1');
                $('#month'+i).addClass('selectedMonth');
              }
            }else if(frist_month > last_month){
              for (var i = frist_month; i >= last_month; i--) {
                month.push(i);
                $('#month'+i).css('background-color','#00acc1');
                $('#month'+i).addClass('selectedMonth');
              }
            }
            }
        
        frist_month = last_month;
       }
    }
      get_week();
      console.log(month);
    
  }

  function clear_month(){
    $('.month').css({ 'background-color' : ''});
    $('.month').removeClass('selectedMonth');
  }


  function change_vendor(e){
    vendor_code = e;
    data_report(week)
  }

  function get_week(){
    $.ajax({
      url : "<?php base_url();  ?>report/get_week",
      method : "post",
      data : {
        "year" : year,
        "category" : category,
        "month": month,
        "week" : week,
        "vendor_code" : vendor_code
      },
      dataType : "HTML",
      success: function(e){
        $('#modal_week').html(e);
      },error: function(e){

      }
    });
  }

  function data_report(weekz){
    
    $('#loading_ontime').show();
    $('#ontime_data_report').empty();
    $.ajax({
      url : "<?php echo base_url(); ?>report/on_time_data_report",
      method : "POST",
      data : {
        "link" : "unloading_time",
        "year" : year,
        "category" : category,
        "week" : week,
        "vendor_code" : vendor_code
      }, dataType : "HTML",
      success:function(e){
        $('#loading_ontime').hide();
        $('#ontime_data_report').html(e);
      }, error:function(e){
        alert("Something Error");
      }
    });
  }

  var frist_week = '';
  var last_week = '';
  var week_diff = 0;
  function WeekClick(cell){
    if(frist_week == ''){
      clear_week();
      frist_week = $('#'+cell).attr('value');
      week.push(parseInt($('#'+cell).attr("value")) );
       $('#'+cell).css("background-color",'rgb(11, 117, 201)');
       $('#'+cell).addClass('selectedWeek');
    }else{
      last_week = parseInt($('#'+cell).attr('value'));
      if (window.event.button === 0) {
        if (!window.event.ctrlKey && !window.event.shiftKey) {
          week = [];
            clear_week();
            week.push(parseInt($('#'+cell).attr("value")) );
            $('#'+cell).addClass('selectedWeek');
            $('#'+cell).css("background-color",'rgb(11, 117, 201)');
        }else if (window.event.shiftKey) {
          week = [];
          if(frist_week < last_week){
            for (var i = frist_week; i <= last_week; i++) {
              week.push(parseInt(i));
              $('#Week'+i).addClass('selectedWeek');
              $('#Week'+i).css("background-color",'rgb(11, 117, 201)');
            }
          }else if(frist_week > last_week){
            for (var i = frist_week; i >= last_week; i--) {
              week.push(parseInt(i));
              $('#Week'+i).addClass('selectedWeek');
              $('#Week'+i).css("background-color",'rgb(11, 117, 201)');
            }
          }

        }else{
          week.push( parseInt($('#'+cell).attr("value")) );
          $('#'+cell).css("background-color",'rgb(11, 117, 201)');
          $('#'+cell).addClass('selectedWeek');
        }
        frist_week = last_week;
       }
    }
    
    console.log(week);
  }

  function clear_week(){
    $('.week.active').removeClass('selectedWeek');
    $('.Week').css('background-color', '#9c27b0');
  }
loading();
  function loading(){
  y = new Spin.Spinner().spin();
  $('.loading').append(y.el);
}

function loading_stop(){
  y.stop();
}
</script>
