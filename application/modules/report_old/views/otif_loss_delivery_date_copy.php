<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-header-icon card-header-info">
        <div class="card-icon">
          <i class="fas fa-calendar-check" style="font-size:35px"></i>
        </div>
        <h4 class="card-title">CONFIRM DELIVERY DATE
          <small> - STATISTIC</small>
        </h4>
      </div>
      <div class="card-body" style="height: 400px">
        <div id="container2" style="height: 100%"></div>
      </div>
      <div class="card-body" style="height: 400px">
        <div id="container3" style="height: 100%"></div>
      </div>
    </div>
  </div>
  <div class="col-12">
    <div class="card card-chart">
      <div class="card-header card-header-icon card-header-danger">
        <div class="card-icon">
          <i class="fas fa-calendar-times" style="font-size:35px"></i>
        </div>
        <h4 class="card-title">OTIF LOSS TREE</h4>
      </div>
      <div class="card-body" style="height: 400px">
        <div id="container" style="height: 100%"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
<div class="card card-chart">
  <div class="card-header card-header-success card-header-icon">
    <div class="card-icon">
      <i class="fas fa-chart-bar" style="font-size:30px"></i>
    </div>
    <h4 class="card-title">LOSS TREE</h4>
  </div>
  <div class="card-body">
    <p class="card-category"> 
    <h4 class="card-title">OTIF LOSS TREE</h4>
      <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover" >
        <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
          <tr>
            <th>LOSS TREE</th>
            <?php foreach ($data_select_otif as $get) {
              ?>
              <th><?php echo $get->loss_tree; ?></th>

            <?php
            } ?>
            
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>QTY(T)</td>
            <?php foreach ($data_otif_loss_tree as $get => $value) {
              ?>
              <td><?php echo $value; ?></td>
              <?php 
            } ?>
            
          </tr>
          <tr>
            <td>% Cont</td>
            <?php 
            $outstanding_week = 0;
            foreach ($data_otif_loss_tree as $get => $value) {
                if($get == "Outstanding Week"){
                    $outstanding_week = $value;
                }
              ?>
              <td><?php echo ($data_otif["total"] > 0)?round(($value/$data_otif["total"])*100,2)."%":"0%"; ?></td>
              <?php 
            } ?>
            
          </tr>
        </tbody>
      </table>
</div>

CONFIRM DELIVERY DATE

<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover" >
    <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
      <tr>
        <th rowspan="2"> Data OTIF </th>
        <th colspan="3"><?php echo $date_list[0]; ?></th>
        <th colspan="3"><?php echo $date_list[1]; ?></th>
        <th colspan="3"><?php echo $date_list[2]; ?></th>
        <th colspan="3"><?php echo $date_list[3]; ?></th>
        <th colspan="3"><?php echo $date_list[4]; ?></th>
        <th colspan="3"><?php echo $date_list[5]; ?></th>
        
        <th rowspan="2">OTIF QTY (Shiftly)</th>
      </tr>
      <tr>
        <th>DM</th>
        <th>DP</th>
        <th>DS</th>
        <th>DM</th>
        <th>DP</th>
        <th>DS</th>
        <th>DM</th>
        <th>DP</th>
        <th>DS</th>
        <th>DM</th>
        <th>DP</th>
        <th>DS</th>
        <th>DM</th>
        <th>DP</th>
        <th>DS</th>
        <th>DM</th>
        <th>DP</th>
        <th>DS</th>

      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Plan Qty</td>
        <td><?php echo $outstanding_week; ?></td>
        <td><?php echo $data_otif[$date_list[0]]["plan"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[0]]["plan"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[0]]["plan"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["plan"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["plan"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["plan"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["plan"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["plan"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["plan"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["plan"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["plan"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["plan"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["plan"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["plan"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["plan"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["plan"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["plan"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["plan"]["DS"]; ?></td>
        <td><?php echo $data_otif["total"]; ?></td>
      </tr>
    
    <tr>
        <td>Actual Qty</td>
        <td><?php echo $data_otif[$date_list[0]]["actual"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[0]]["actual"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[0]]["actual"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["actual"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["actual"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["actual"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["actual"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["actual"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["actual"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["actual"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["actual"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["actual"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["actual"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["actual"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["actual"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["actual"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["actual"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["actual"]["DS"]; ?></td>
        
        <td><?php echo $data_otif["total_actual"]; ?></td>
      </tr>
      
      <tr>
        <td>%OTIF Shiftly</td>
        <td><?php echo $data_otif[$date_list[0]]["otif"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[0]]["otif"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[0]]["otif"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["otif"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["otif"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[1]]["otif"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["otif"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["otif"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[2]]["otif"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["otif"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["otif"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[3]]["otif"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["otif"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["otif"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[4]]["otif"]["DS"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["otif"]["DM"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["otif"]["DP"]; ?></td>
        <td><?php echo $data_otif[$date_list[5]]["otif"]["DS"]; ?></td>
        
        <td><?php echo $data_otif["total_otif"]; ?></td>
      </tr>
    </tbody>
  </table>
</div>
</p>
</div>
</div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
var vendor = "<?php echo $data_vendor->vendor_alias; ?>";
var vendor_code = "<?php echo $this->input->get('vendor_code'); ?>";
var week = "<?php echo $this->input->get('week'); ?>";
var year = "<?php echo $this->input->get('year'); ?>";


var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
option = null;
// var weatherIcons = {
//     'Sunny': ROOT_PATH + 'data/asset/img/weather/sunny_128.png',
//     'Cloudy': ROOT_PATH + 'data/asset/img/weather/cloudy_128.png',
//     'Showers': ROOT_PATH + 'data/asset/img/weather/showers_128.png'
// };

option = {
    title: {
        text: '天气情况统计',
        subtext: '虚构数据',
        left: 'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        // orient: 'vertical',
        // top: 'middle',
        bottom: 10,
        left: 'center',
        data: ['otif',
            <?php foreach ($data_select_otif as $get) {
                echo "'".$get->loss_tree."',";
            }
            ?>
        ]
    },
    series : [
        {
            type: 'pie',
            radius : '65%',
            center: ['50%', '50%'],
            selectedMode: 'single',
            data:[
                {
                    value:1000,
                    name: 'otif',
                    label: {
                        normal: {
                            formatter: [
                                '{title|{b}}{abg|}',
                                '  {weatherHead|天气}{valueHead|天数}{rateHead|占比}',
                                '{hr|}',
                                '  {Sunny|}{value|999}{rate|55.3%}',
                                '  {Cloudy|}{value|142}{rate|38.9%}',
                                '  {Showers|}{value|21}{rate|5.8%}'
                            ].join('\n'),
                            backgroundColor: '#eee',
                            borderColor: '#777',
                            borderWidth: 1,
                            borderRadius: 4,
                            rich: {
                                title: {
                                    color: '#eee',
                                    align: 'center'
                                },
                                abg: {
                                    backgroundColor: '#333',
                                    width: '100%',
                                    align: 'right',
                                    height: 25,
                                    borderRadius: [4, 4, 0, 0]
                                },
                                Sunny: {
                                    height: 30,
                                    align: 'left',
                                    // backgroundColor: {
                                    //     image: weatherIcons.Sunny
                                    // }
                                },
                                Cloudy: {
                                    height: 30,
                                    align: 'left',
                                    // backgroundColor: {
                                    //     image: weatherIcons.Cloudy
                                    // }
                                },
                                Showers: {
                                    height: 30,
                                    align: 'left',
                                    // backgroundColor: {
                                    //     image: weatherIcons.Showers
                                    // }
                                },
                                weatherHead: {
                                    color: '#333',
                                    height: 24,
                                    align: 'left'
                                },
                                hr: {
                                    borderColor: '#777',
                                    width: '100%',
                                    borderWidth: 0.5,
                                    height: 0
                                },
                                value: {
                                    width: 0,
                                    padding: [20, 20, 0, 30],
                                    align: 'left'
                                },
                                valueHead: {
                                    color: '#333',
                                    width: 20,
                                    padding: [0, 20, 0, 30],
                                    align: 'center'
                                },
                                rate: {
                                    width: 40,
                                    align: 'right',
                                    padding: [0, 10, 0, 0]
                                },
                                rateHead: {
                                    color: '#333',
                                    width: 40,
                                    align: 'center',
                                    padding: [0, 10, 0, 0]
                                }
                            }
                        }
                    }
                },
                <?php $total_data_otif = count($data_select_otif); 
                    $no = 0;
                    while ($no < $total_data_otif) {
                        ?>
                            {value:1000, name: '<?php echo $data_select_otif[$no]->loss_tree; ?>'},
                        <?php
                        $no++;
                    }
                ?>
            ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}

var dom = document.getElementById("container2");
var myChart = echarts.init(dom);
var app = {};
var total_plan = [];
var total_actual = [];
var markline = [];

option = null;
option = {
    title: {
        text: 'Daily Otif, Total OTIF : <?php echo $data_otif['total_otif']; ?>',
        subtext: vendor+" WEEK "+week
    },
    tooltip: {
        trigger: 'axis',
        formatter: "{a} <br/>{b} : {c}%"
    },
    legend: {
        data:['OTIF','OTIF']
    },
    toolbox: {  
        show: true,
        feature: {
            magicType: {
                type: ['line', 'bar', 'pie', 'funnel'],
                title : {
                    line : 'Statistic',
                    bar : 'Bar',
                    stack : 'test',
                    tiled : 'test',
                    force: 'test',
                    chord: 'test',
                    pie: 'test',
                    funnel: 'test'
                },
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage: {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    xAxis:  {
        type: 'category',
        boundaryGap: false,
        data: [
          <?php foreach($date_list as $date){
              echo "'".$date."', ";
          } ?>
        ]
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: '{value}'
        }
    },
    series: [
        {
            name:'Otif Shiftly',
            type:'line',
            data:[
              <?php foreach($total_date as $total){
                echo "'".$total["otif"]."', ";
              } ?>
            ],
            markPoint: {
                data: markline
            },
            markLine: {
                data: [
                    {type: 'average', name: 'Average'}
                ]
            }
         }
        // ,{
        //     name:'Actual Qty',
        //     type:'line',
        //     data:total_actual,
        //     markPoint: {
        //         data: [
        //             {name: 'Actual', value: -2, xAxis: 1, yAxis: -1.5}
        //         ]
        //     },
        //     // markLine: {
        //     //     data: [
        //     //         {type: 'average', name: 'OTIFZz'}
                    
        //     //     ]
        //     // }
        // }
    ]
};

if (option && typeof option === "object") {
    myChart.setOption(option, true);
}

});

//CHART
var dom = document.getElementById("container3");
var myChart = echarts.init(dom);
var app = {};
option = null;
var posList = [
    'left', 'right', 'top', 'bottom',
    'inside',
    'insideTop', 'insideLeft', 'insideRight', 'insideBottom',
    'insideTopLeft', 'insideTopRight', 'insideBottomLeft', 'insideBottomRight'
];

app.configParameters = {
    rotate: {
        min: -90,
        max: 90
    },
    align: {
        options: {
            left: 'left',
            center: 'center',
            right: 'right'
        }
    },
    verticalAlign: {
        options: {
            top: 'top',
            middle: 'middle',
            bottom: 'bottom'
        }
    },
    position: {
        options: echarts.util.reduce(posList, function (map, pos) {
            map[pos] = pos;
            return map;
        }, {})
    },
    distance: {
        min: 0,
        max: 100
    }
};

app.config = {
    rotate: 90,
    align: 'left',
    verticalAlign: 'middle',
    position: 'insideBottom',
    distance: 15,
    onChange: function () {
        var labelOption = {
            normal: {
                rotate: app.config.rotate,
                align: app.config.align,
                verticalAlign: app.config.verticalAlign,
                position: app.config.position,
                distance: app.config.distance
            }
        };
        myChart.setOption({
            series: [{
                label: labelOption
            }, {
                label: labelOption
            }, {
                label: labelOption
            }]
        });
    }
};


var labelOption = {
    normal: {
        show: true,
        position: app.config.position,
        distance: app.config.distance,
        align: app.config.align,
        verticalAlign: app.config.verticalAlign,
        rotate: app.config.rotate,
        formatter: '{c}%  {name|{a}}',
        fontSize: 16,
        rich: {
            name: {
                textBorderColor: '#fff'
            }
        }
    }
};

option = {
    color: ['#003366', '#4cabce', '#e5323e'],
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    title: {
        text: 'Shiftly Actual Otif',
        subtext: vendor+" WEEK "+week
    },
    legend: {
        data: ['DM', 'DP', 'DS']
    },
    toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
        feature: {
            
            magicType: {
                show: true, type: ['line', 'bar'],
                title : {
                    line : 'Statistic',
                    bar : 'Bar',
                   
                },
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage : {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    calculable: true,
    xAxis: [
        {
            type: 'category',
            axisTick: {show: false},
            data: [
              <?php foreach($date_list as $date){
                echo "'".$date."', ";
            } ?>
            ]
        }
    ],
    yAxis: [
        {
            type: 'value'
        }
    ],
    series: [
        {
            name: 'DM',
            type: 'bar',
            barGap: 0,
            label: labelOption,
            data: [
            <?php foreach($date_list as $date){
                echo "'".str_replace("%","",$data_otif[$date]['otif']['DM'])."', ";
            } ?>
                
            ]
        },
        {
            name: 'DP',
            type: 'bar',
            label: labelOption,
            data: [
               <?php foreach($date_list as $date){
                echo "'".str_replace("%","",$data_otif[$date]['otif']['DP'])."', ";
            } ?>
            ]
        },
        {
            name: 'DS',
            type: 'bar',
            label: labelOption,
            data: [
                <?php foreach($date_list as $date){
                echo "'".str_replace("%","",$data_otif[$date]['otif']['DS'])."', ";
            } ?>
            ]
        }
    ]
};;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
</script>