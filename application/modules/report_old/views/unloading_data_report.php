<?php
  $total_data_chart = count($data_unloading);
 ?>

 <div class="loading">
 </div>
 
<div class="row">
        <div class="col-2">
          <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="fas fa-truck"></i>
              </div>
              <p class="card-category" style="font-size:14px;font-weight:800;color:black">TOTAL</p>
              <h4 class="card-title"><b><?php echo 

              count($data_list); ?>
                  
                </b></h4>
            </div>
          </div>
        </div>

        <div class="col-lg-2">
          <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
            <div class="card-header card-header-rose card-header-icon">
              <div class="card-icon">
                <i class="fas fa-truck"></i>
              </div>
              <p class="card-category" style="font-size:14px;font-weight:800;color:black">Early :</p>
              <?php 
              
              if(count($data_list) > 0){
                  $early = round(($data_list[count($data_list)-1]->early/count($data_list))*100);
              }else{
                $early = 0;
              } ?>
              <h4 class="card-title"><b><?php echo (count($data_list) > 0)?$data_list[count($data_list)-1]->early:0; ?></b></h4>
            </div>
            <div class="card-footer">
              <b><?php echo $early."%"; ?></b>
            </div>
          </div>
        </div>

        <div class="col-lg-2">
          <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i class="fas fa-truck"></i>
              </div>
              <p class="card-category" style="font-size:14px;font-weight:800;color:black">Delay :</p>
              <?php 
              
              if(count($data_list) > 0){
                  $delay = round(($data_list[count($data_list)-1]->delay/count($data_list))*100);
              }else{
                $delay = 0;
              } ?>
              <h4 class="card-title"><b><?php echo (count($data_list) > 0)?$data_list[count($data_list)-1]->delay:0; ?></b></h4>
            </div>
            <div class="card-footer">
               <b> <?php echo $delay."%"; ?></b>
            </div>
          </div>
        </div>

        <div class="col-2">
          <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="fas fa-truck"></i>
              </div>
              <p class="card-category" style="font-size:13px;font-weight:800;color:black">AVG TIME DM</p>
              <p style="font-size:12px" class="card-title">
                <b>
                  <?php 
                
                $H = floor($data_average["DM"] / 60);

                $I = ($data_average["DM"]) % 60;
                if($H < 10){
                  $H = '0'.$H;
                }

                if($I < 10){
                  $I = '0'.$I;
                }
                echo $H.':'.$I; 
                ?>
                  
                </b></p>
            </div>
          </div>
        </div>

        <div class="col-2">
          <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="fas fa-truck"></i>
              </div>
              <p class="card-category" style="font-size:13px;font-weight:800;color:black">AVG TIME DP</p>
              <p style="font-size:12px" class="card-title"><b>
                  <?php 
                
                $H = floor($data_average["DP"] / 60);

                $I = ($data_average["DP"]) % 60;
                if($H < 10){
                  $H = '0'.$H;
                }

                if($I < 10){
                  $I = '0'.$I;
                }
                echo $H.':'.$I; 
                ?>
                  
                </b></p>
            </div>
          </div>
        </div>

        <div class="col-2">
          <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="fas fa-truck"></i>
              </div>
              <p class="card-category" style="font-size:13px;font-weight:800;color:black">AVG TIME DS</p>
              <p style="font-size:12px" class="card-title"><b>
                  <?php 
                $H = floor($data_average["DS"] / 60);
                $I = ($data_average["DS"] % 60);
                if($H < 10){
                  $H = '0'.$H;
                }

                if($I < 10){
                  $I = '0'.$I;
                }
                echo $H.':'.$I; 
                ?>
                  
                </b></p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header card-header-icon card-header-info">
              <div class="card-icon">
                <i class="fas fa-chart-line" style="font-size:35px"></i>
              </div>
              <h4 class="card-title">Unloading Performance, AVERAGE : 
                <?php 
                $H = floor(($data_average["DM"]+$data_average["DP"]+$data_average["DS"])/3/60);
                $I = (($data_average["DM"]+$data_average["DP"]+$data_average["DS"])/3)%60;
                if($H < 10){
                  $H = '0'.$H;
                }

                if($I < 10){
                  $I = '0'.$I;
                }
                echo $H.':'.$I; 
                ?>
                
              </h4>
            </div>
            <div class="card-body">
              <div id="on_time_chart" style="height: 500px"></div>
            </div>
             
            </div>
          </div>
        </div>

<div class="row">
        <div class="col-12">
          <div class="card" >
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i class="fas fa-chart-bar" style="font-size:30px"></i>
                </div>
                <h4 class="card-title">REPORT SLOT ON TIME</h4>
              </div>
            <div class="card-body">

              <ul class="nav nav-pills nav-pills-icons" role="tablist">
                <?php $no = 0; 
                if(count($week) > 1 ){ 
                  foreach($week as $weekz){
                  ?>
                <li class="nav-item">
                  <a class="nav-link <?php if($no ==0 ){echo "active";} ?>" href="#week-<?php echo $weekz; ?>" role="tab" data-toggle="tab">
                      W<?php echo $weekz; $no++?>
                  </a>
                </li>
                <?php 
                }
              }else{ 
                if(!is_array($week)){
                  $week = array(
                    0 => $week
                  );
                }
                ?>
                <li class="nav-item">
                  <a class="nav-link active" href="#week-<?php echo $week[0]; ?>" role="tab" data-toggle="tab">
                      W<?php echo $week[0]; ?>
                  </a>
                </li>
                <?php } ?>
              </ul>

              <div class="tab-content tab-space">
                <?php $number = 0;
                  $no_data = 0;
                  $no_week = 0;
                if(count($week) > 1 ){
                  foreach($week as $weekz){
                    $getDateTime = new DateTime();
                    $date_listz[0] = $getDateTime->setISODate($year, $weekz)->format('Y-m-d');
                    $i = 1;
                      while($i < 7){
                      $date_listz[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
                      $i++;
                    }
                  ?>
                  <div class="tab-pane <?php if($number == 0){echo "active";} ?>" id="week-<?php echo $weekz; ?>">
                    <a href="<?php echo base_url() ?>report/download_unloading?year=<?php echo $year; ?>&week=<?php echo $weekz; ?>&vendor_code=<?php echo $vendor_code; ?>"><button class="btn btn-primary"><i class="fas fa-download"></i> Download W<?php echo $weekz; ?></button></a>
                    
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-hover data-table tables" width="100%">
                        <thead style="color:white; background-color:#31559F;font-weight: 650;">
                          <tr>
                            <th rowspan="2" style="font-size:10px">No</th>
                            <th rowspan="2" style="font-size:10px">Receipt</th>
                            <th rowspan="2" style="font-size:10px">No Truck</th>
                            <th rowspan="2" style="font-size:10px">Rdd</th>
                            <th rowspan="2" style="font-size:10px">Time Slot</th>
                            <th rowspan="2" style="font-size:10px">Arrived</th>
                            <th rowspan="2" style="font-size:10px">Received</th>
                            <th rowspan="2" style="font-size:10px">SLOT HIT</th>
                            <?php $no=0; $total_date_list = count($date_listz); ?>
                            <?php while($no < $total_date_list){ ?>
                            <th style="text-align:center; font-size:10px" colspan="3"><?php echo $date_listz[$no]; ?></th>

                            <?php $no++; } ?>
                          </tr>
                          <tr>
                            <?php $no =0; while($no < $total_date_list){ ?>
                            <th style="font-size:10px">DM</th>
                            <th style="font-size:10px">DP</th>
                            <th style="font-size:10px">DS</th>
                             <?php $no++; } ?>
                            
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=0; foreach($data_list_week[$weekz] as $get){ ?>
                          <tr>
                            <td ><?php echo $no+1; ?></td>
                            <td ><?php echo $get->receipt; ?></td>
                            <td ><?php echo $get->police_number; ?></td>
                            <td ><?php echo $get->rdd; ?></td>
                            <td ><?php echo $get->start_time.' - '.$get->end_time; ?></td>
                            <td ><?php echo $get->arrive_date.' - '.$get->arrive_time; ?></td>
                            <td ><?php echo $get->receive_date.' - '.$get->receive_time; ?></td>
                            <td ><?php echo $get->status; ?></td>
                            <?php 
                              foreach($date_listz as $date){
                                foreach($shift_list as $shift){
                                  $H = floor($data_unloading[$no_data][$date][$shift] / 60);
                                  $I = ($data_unloading[$no_data][$date][$shift] % 60);
                                  $D = '';
                                  ?>
                                      <td  style="<?php if($H != 0 || $I != 0){
                                        if($get->status == "Early"){
                                          echo 'background-color:#ff9900;color:white';
                                        }else if($get->status =="Delay"){
                                          echo 'background-color:#cc0000;color:white';
                                        }else{
                                          echo 'background-color:green;color:white';
                                        }
                                      } ?>">
                                        <?php 
                                        if($H < 10){
                                          $H = '0'.$H;
                                        }

                                        if($H > 24){
                                          $getD = round($H / 24);
                                          if($getD < 10){
                                            $getD = '0'.$getD;
                                          }
                                          $D = $getD.' ';

                                          $H = $H % 24;

                                        }



                                        if($I < 10){
                                          $I = '0'.$I;
                                        }
                                        echo $D.$H.':'.$I; 
                                        ?>
                                      </td>
                                  <?php
                                }
                              }
                            ?>
                          </tr>
                        <?php 
                        $no_data++;
                        $no++;} ?>
                        
                          
                        </tbody>
                      </table>
                    </div>

                  </div>
                <?php
                  $number++; 
                  }
                }else{ 
                  if(!is_array($week)){
                    $week = array(
                      0 => $week
                    );
                  }
                  ?>
                  <div class="tab-pane <?php if($no ==0){echo "active";} ?>" id="week-<?php echo $week[0]; ?>">
                    <a href="<?php echo base_url() ?>report/download_unloading?year=<?php echo $year; ?>&week=<?php echo $week[0]; ?>&vendor_code=<?php echo $vendor_code; ?>"><button class="btn btn-primary"><i class="fas fa-download"></i> Download W<?php echo $week[0]; $no++; ?></button></a>
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover data-table tables" width="100%">
                      <thead style="color:white; background-color:#31559F;font-weight: 650;">
                        <tr>
                          <th rowspan="2" style="font-size:10px">No</th>
                          <th rowspan="2" style="font-size:10px">Receipt</th>
                          <th rowspan="2" style="font-size:10px">No Truck</th>
                          <th rowspan="2" style="font-size:10px">Rdd</th>
                          <th rowspan="2" style="font-size:10px">Time Slot</th>
                          <th rowspan="2" style="font-size:10px">Arrived</th>
                          <th rowspan="2" style="font-size:10px">Received</th>
                          <th rowspan="2" style="font-size:10px">SLOT HIT/MISS</th>
                          <?php $no=0; $total_date_list = count($date_list); ?>
                          <?php while($no < $total_date_list){ ?>
                          <th style="text-align:center; font-size:10px" colspan="3"><?php echo $date_list[$no]; ?></th>

                          <?php $no++; } ?>
                        </tr>
                        <tr>
                          <?php $no =0; while($no < $total_date_list){ ?>
                          <th style="font-size:10px">DM</th>
                          <th style="font-size:10px">DP</th>
                          <th style="font-size:10px">DS</th>
                           <?php $no++; } ?>
                          
                        </tr>
                      </thead>
                      <tbody>
                        <?php $no=0; foreach($data_list as $get){ ?>
                        <tr>
                          <td ><?php echo $no+1; ?></td>
                          <td ><?php echo $get->receipt; ?></td>
                          <td ><?php echo $get->police_number; ?></td>
                          <td ><?php echo $get->rdd; ?></td>
                          <td ><?php echo $get->start_time.' - '.$get->end_time; ?></td>
                          <td ><?php echo $get->arrive_date.' - '.$get->arrive_time; ?></td>
                          <td ><?php echo $get->receive_date.' - '.$get->receive_time; ?></td>
                          <td ><?php echo $get->status; ?></td>
                          <?php 
                            foreach($date_list as $date){
                              foreach($shift_list as $shift){
                                $H = floor($data_unloading[$no][$date][$shift] / 60);
                                $I = ($data_unloading[$no][$date][$shift] % 60);
                                $D = '';
                                ?>
                                    <td  style="<?php if($H != 0 || $I != 0){
                                      if($get->status == "Early"){
                                        echo 'background-color:#ff9900;color:white';
                                      }else if($get->status =="Delay"){
                                        echo 'background-color:#cc0000;color:white';
                                      }else{
                                        echo 'background-color:green;color:white';
                                      }
                                    } ?>">
                                      <?php 
                                      if($H < 10){
                                        $H = '0'.$H;
                                      }

                                      if($H > 24){
                                        $getD = round($H / 24);
                                        if($getD < 10){
                                          $getD = '0'.$getD;
                                        }
                                        $D = $getD.' ';

                                        $H = $H % 24;

                                      }



                                      if($I < 10){
                                        $I = '0'.$I;
                                      }
                                      echo $D.$H.':'.$I; 
                                      ?>
                                    </td>
                                <?php
                              }
                            }
                          ?>
                        </tr>
                      <?php $no++;} ?>
                      
                        
                      </tbody>
                    </table>
                  </div>
                  </div>
                <?php } ?>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="row" style="height: 500px">
        <div class="col">
          
        </div>
      </div>
<style>
  .chart {
    width: 100%; 
    min-height: 100%;
  }

</style>
<?php
  $no_week = 0;
  $chart_value = "['Week', 'DM', 'DP', 'DS'],";
  $total_week = count($week_list);
  
  $legend_week = "";
  $series_week = "";
  $data_DM = array();
  $data_DP = array();
  $data_DS = array();
  $dm_val = 0;
  $dp_val = 0;
  $ds_val = 0;
  $no = 0;
  $total_dp = 0;
  foreach($data_unloading as $data){
    foreach($date_list as $date){
      $data_DM[$date] = 0;
      $data_DP[$date] = 0;
      $data_DS[$date] = 0;
    }
  }

  foreach($data_unloading as $data){
    foreach($date_list as $date){
        $data_DM[$date] = $data_DM[$date] + floor($data_unloading[$no][$date]["DM"]);
        $data_DP[$date] = $data_DP[$date] + floor($data_unloading[$no][$date]["DP"]);
        $data_DS[$date] = $data_DS[$date] + floor($data_unloading[$no][$date]["DS"]);
    }  
    $no++;
  }
  
  $chart_DM = "";
  $chart_DP = "";
  $chart_DS = "";
  $mark_line_DM = "";
  $mark_line_DP = "";
  $mark_line_DS = "";
  $no = 1;
  foreach($date_list as $date){
    $DM_data = 0;
    $DP_data = 0;
    $DS_data = 0;
    if(isset($data_DM[$date])){
      $DM_data = $data_DM[$date];
    }
    if(isset($data_DP[$date])){
      $DP_data = $data_DP[$date];
    }
    if(isset($data_DS[$date])){
      $DS_data = $data_DS[$date];
    }
    $chart_DM = $chart_DM."".$DM_data.", ";
    $chart_DP = $chart_DP."".$DP_data.", ";
    $chart_DS = $chart_DS."".$DS_data.", ";
    
    $H_DM = floor($DM_data / 60);
    $I_DM = ($DM_data % 60);
    $D_DM = '';
    if($H_DM < 10){
      $H_DM = '0'. $H_DM;
    }

    if($H_DM > 24){
      // $getD_DM = round($H_DM / 24);
      // if($D_DM < 10){
      //    $D_DM = '0'.$getD_DM;
      // }
      $D_DM = $D_DM .' ';
      $H_DM = $H_DM % 24;

    }

    if($I_DM < 10){
      $I_DM = '0'. $I_DM;
    }
    $new_value =  $H_DM.':'.$I_DM;

    $mark_line_DM = $mark_line_DM."{name: 'Otif', value: '".$new_value."', xAxis: ".$no.", yAxis: ".$DM_data."},";
    $H_DP = floor($DP_data / 60);
    $I_DP = ($DP_data % 60);
    $D_DP = '';
    if($H_DP < 10){
      $H_DP = '0'.$H_DP;
    }

    if($H_DP > 24){
      // $getD_DP = round($H_DP / 24);
      // if($D_DP < 10){
      //    $D_DP = '0'.$getD_DP;
      // }
      $D_DP = $D_DP .' ';
      $H_DP = $H_DP % 24;

    }

    if($I_DP < 10){
      $I_DP = '0'. $I_DP;
    }
    $new_value =  $H_DP.':'.$I_DP;

    $mark_line_DP = $mark_line_DP."{name: 'Otif', value: '".$new_value."', xAxis: ".$no.", yAxis: ".$DP_data."},"; 
    
    $H_DS = floor($DS_data / 60);
    $I_DS = ($DS_data % 60);
    $D_DS = '';
    if($H_DS < 10){
      $H_DS = '0'.$H_DS;
    }

    if($H_DS > 24){
      // $getD_DS = round($H_DS / 24);
      // if($D_DS < 10){
      //    $D_DS = '0'.$getD_DS;
      // }
      $D_DS = $D_DS .' ';
      $H_DS = $H_DS % 24;

    }

    if($I_DS < 10){
      $I_DS = '0'. $I_DS;
    }
    $new_value =  $H_DS.':'.$I_DS;
    $mark_line_DS = $mark_line_DS."{name: 'Otif', value: '".$new_value."', xAxis: ".$no.", yAxis: ".$DS_data."},"; 
    $no++;
  }
  // if($total_week > 1){
  //   foreach($week_list as $get){
  //     $legend_week = $legend_week."'W".$get."',";
  //     $data_DM = $data_DM.$data_chart[$no_week][$get]["DM"].",";
  //     $data_DP = $data_DP.$data_chart[$no_week][$get]["DP"].",";
  //     $data_DS = $data_DS.$data_chart[$no_week][$get]["DS"].",";  
  //     $no_week++;
  //   }
  // }else{
  //   if(is_array($week_list)){
  //     $week_list = $week_list[0];
  //   }
  //   $legend_week = $legend_week."'W".$week_list."',";
  //   $data_DM = $data_DM.$data_chart[$no_week][$week_list]["DM"].",";
  //   $data_DP = $data_DP.$data_chart[$no_week][$week_list]["DP"].",";
  //   $data_DS = $data_DS.$data_chart[$no_week][$week_list]["DS"].",";
  // }
if(!is_array($week)){
  $week = array(
    0 => $week
  );
}
?>

<script type="text/javascript">
var dom = document.getElementById("on_time_chart");
var myChart = echarts.init(dom);
var app = {};
option = null;
option = {
    title: {
        text: 'Week <?php if(count($week) >1){echo $week[0]."-".$week[count($week)-1]; }else{echo $week[0];} ?>',
        subtext: '<?php echo $data_vendor->vendor_name; ?>'
    },
    tooltip: {
        trigger: 'axis',
        formatter: "{a} <br/>{b} : {c}"
    },
    legend: {
        data:["DM", "DP", "DS"]
    },
    toolbox: {
        show: true,
        feature: {
            magicType: {
                type: ['line', 'bar', 'pie', 'funnel'],
                title : {
                    line : 'Statistic',
                    bar : 'Bar',
                    stack : 'stack',
                    tiled : 'tiled',
                    force: 'force',
                    chord: 'chord',
                    pie: 'pie',
                    funnel: 'funnel'
                },
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage: {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    xAxis:  {
        type: 'category',
        boundaryGap: false,
        data: [0,<?php 
            if(count($week) > 1){
              foreach($week as $weekz){
                echo "'".$weekz."',";
              }
            }else{
              foreach($date_list as $date){
                echo "'".date('d-m-Y', strtotime($date) )."', ";
              }
            }
            
         ?>]
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: (function(value){
                    var new_value = Math.round(value);
                    var H = 0;
                    var I = 0;
                    var D = '';
                    if(new_value > 0){
                      H = Math.floor(new_value / 60);
                      I = (new_value % 60);
                      // D = '';
                      if(H < 10){
                        H = '0'+H;
                      }

                      if(H > 24){
                       H = H % 24;

                      }

                      if(I < 10){
                        I = '0'+I;
                      }
                      new_value =  H+':'+I; 
                      
                    }
                    return new_value;
                })
        }
    },
    series: [
        
        {
            name:'DM',
            type:'bar',
            barWidth : 35,
            data:[0,
              <?php 
              if(count($week) > 1){
                foreach($week as $weekz){
                  echo "'".$data_weekly_chart1[$weekz]["DM"]."',";   
                }
                
              }else{
                echo $chart_DM;   
              }
              
              ?>
            ],
            markPoint: {
                data: [
                    <?php 
                      if(count($week) > 1){
                        $no = 1;
                        foreach($week as $weekz){
                          $H = floor($data_weekly_chart1[$weekz]["DM"] / 60);
                          $I = ($data_weekly_chart1[$weekz]["DM"] % 60);
                          $D = '';
                          if($H < 10){
                            $H = '0'.$H;
                          }

                          if($H > 24){
                            $D = $D .' ';
                            $H = $H % 24;

                          }

                          if($I < 10){
                            $I = '0'. $I;
                          }
                          $new_value =  $H.':'.$I;
                          echo "{name: 'Otif', value: '".$new_value."', xAxis: ".$no.", yAxis: ".$data_weekly_chart1[$weekz]["DM"]."},";
                          $no++;
                        }
                      }else{
                        echo $mark_line_DM;
                      }
                       
                    ?>
                ]
            }
        },
        {
            name:'DP',
            type:'bar',
            barWidth : 35,
            data:[0,
              <?php 
              if(count($week) > 1){
                foreach($week as $weekz){
                  echo "'".$data_weekly_chart1[$weekz]["DP"]."',";  
                }
                
              }else{
                echo $chart_DP;   
              } ?>
            ],
            markPoint: {
                data: [
                    <?php 
                      if(count($week) > 1){
                        $no = 1;
                        foreach($week as $weekz){
                          $H = floor($data_weekly_chart1[$weekz]["DP"] / 60);
                          $I = ($data_weekly_chart1[$weekz]["DP"] % 60);
                          $D = '';
                          if($H < 10){
                            $H = '0'.$H;
                          }

                          if($H > 24){
                            $D = $D .' ';
                            $H = $H % 24;

                          }

                          if($I < 10){
                            $I = '0'. $I;
                          }
                          $new_value =  $H.':'.$I;
                          echo "{name: 'Otif', value: '".$new_value."', xAxis: ".$no.", yAxis: ".$data_weekly_chart1[$weekz]["DP"]."},";
                          $no++;
                        }
                      }else{
                        echo $mark_line_DP;
                      }
                       
                    ?>
                ]
            },
            
        },
         {   
            name:'DS',
            type:'bar',
            barWidth : 35,
            data:[0,
            <?php 
              if(count($week) > 1){
                foreach($week as $weekz){
                  echo "'".$data_weekly_chart1[$weekz]["DS"]."',";  
                }
                
              }else{
                echo $chart_DS;   
              } 
              ?>],
            markPoint: {
                data: [
                    <?php 
                      if(count($week) > 1){
                        $no = 1;
                        foreach($week as $weekz){
                          $H = floor($data_weekly_chart1[$weekz]["DS"] / 60);
                          $I = ($data_weekly_chart1[$weekz]["DS"] % 60);
                          $D = '';
                          if($H < 10){
                            $H = '0'.$H;
                          }

                          if($H > 24){
                            $D = $D .' ';
                            $H = $H % 24;
                          }

                          if($I < 10){
                            $I = '0'. $I;
                          }
                          $new_value =  $H.':'.$I;
                          echo "{name: 'Otif', value: '".$new_value."', xAxis: ".$no.", yAxis: ".$data_weekly_chart1[$weekz]["DS"]."},";
                          $no++;
                        }
                      }else{
                        echo $mark_line_DS;
                      }
                       
                    ?>
                ]
            },
            
        }
    ]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
</script>

<!-- Truck On time -->

<?php

$early = array();
$ontime = array();
$delay = array();
  foreach($date_list as $date){
    $early[$date] = 0;
    $delay[$date] = 0;
    $ontime[$date] = 0;
  }

foreach ($data_list as $data) {
  $shift = $data->shift;
  
    if($data->status == "Early"){
      $early[$data->rdd] = $early[$data->rdd]+1;
    }else if($data->status == "Delay"){
      $delay[$data->rdd] = $delay[$data->rdd]+1;
    }else{
      $ontime[$data->rdd] = $ontime[$data->rdd] + 1;
   }
}



?>

<script type="text/javascript">

$(document).ready(function(){
  setTimeout(function(){
      $('#loading_ontime').hide();
    }, 1000);
});

$(".tables").DataTable({
    "dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
  });
</script>