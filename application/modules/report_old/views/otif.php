<style>
  td{
    font-size:12px;
  }
  th{
    font-size:12px;
  }
</style>
<style type="text/css">

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 0px 10px;
    vertical-align: middle;
    border-color: #ddd;
}

.nav-pills .nav-item .nav-link {
    line-height: 24px;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 500;
    min-width: 100px;
    text-align: center;
    color: #555;
    transition: all .3s;
    border-radius: 30px;
    padding: 0px 5px;
}

.table thead tr th {
    /* font-size: 1.063rem; */
    font-size: 12px;
    text-align:center!important;

}

@media screen and (min-width: 400px){
  
}

@media screen and (min-width: 800px){
  .action-button{
    position: absolute;
    display: inline-block;
    z-index: 9999;
  }

  .nav-pills{
    margin-left:-14px;
  }
}

td{
  white-space: nowrap;
}
</style>
<div id="loading">
</div>
<div class="content">
  <div class="content">
    <div class="container-fluid">

      <div class="row">
        <div class="col">
          <div class="card card-chart">
            <?php 
              $year = $this->input->get('year');
              $week = $this->input->get('week');
              $vendor_code = $this->input->get('vendor_code');
              if(check_sub_menu(35)){
                $vendor_link = "&vendor_code=".$vendor_code;
              }else if(check_sub_menu(34)){
                $vendor_link = $this->session->userdata('sess_vendor_code');
              }
            ?>
            <div class="card-header card-header-success card-header-icon">
              <div class="row">
                <div class="col-4">
                  <div class="card-icon">
                  <i class="fas fa-chart-bar" style="font-size:30px"></i>
                  </div>
                  <h4 class="card-title">OTIF REPORT - <?php echo $data_vendor->vendor_alias; ?></h4>
                </div>
                <div class="col">
                </div>
                <div class="col-2" <?php if(!check_sub_menu(33)){echo "hidden";} ?>>
                  <?php if(check_sub_menu(35)){
                      ?>
                    <select style="margin-top:10px" onchange="change_vendor(this.value)" name="select_vendor" class="form-control select2-init">
                      
                      
                        <?php foreach ($data_vendor_list as $get) {
                          ?>
                          <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_name." - ".$get->vendor_code; ?></option>
                          <?php
                        } ?>
                      <?php
                    } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-2 col-sm-12">
                  <div class="form-group" style="margin-top:15px">
                    <label style="margin-left:70px">Year</label>
                    <nav aria-label="Page navigation example">
                        <ul class="pagination pagination-primary">
                          
                          <li class="page-item">
                            <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?><?php echo $vendor_link; ?>"><?php echo $year-1; ?></a>
                          </li>
                          
                          <li class="page-item active">
                            <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?><?php echo $vendor_link; ?>"><?php echo $year; ?></a>
                          </li>

                          <li class="page-item">
                            <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?><?php echo $vendor_link; ?>"><?php echo $year+1; ?></a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                </div>
                
                  <div class="col-8">
                  <div class="table-responsive" >
                    <table class="table" style="margin-top:15px">
                      <thead>
                        <tr>
                          <th>Month</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="2">
                            <table id="month" class="table" style="height: 30px">
                              <tr>
                                <td>
                                  <button value="1" style="width:55px" id="month1" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">JAN</button>
                                  <button value="2" style="width:55px" id="month2" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">FEB</button>
                                  <button value="3" style="width:55px" id="month3" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">MAR</button>
                                  <button value="4" style="width:55px" id="month4" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">APR</button>
                                  <button value="5" style="width:55px" id="month5" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">MEI</button>
                                  <button value="6" style="width:55px" id="month6" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">JUN</button>
                                  <button value="7" style="width:55px" id="month7" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">JUL</button>
                                  <button value="8" style="width:55px" id="month8" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">AGU</button>
                                  <button value="9" style="width:55px" id="month9" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">SEP</button>
                                  <button value="10" style="width:55px" id="month10" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">OKT</button>
                                  <button value="11" style="width:55px" id="month11" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">NOV</button>
                                  <button value="12" style="width:55px" id="month12" onclick="CellClick(this,false);" class="btn btn-primary month btn-sm">DES</button>
                                </td>
                                
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-2">
                  <div class="table-responsive" >
                    <table class="table table-hover" style="margin:0px; margin-top:12px">
                      <thead>
                        <tr>
                          <th>Pareto By Week</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                            <!-- Button trigger modal -->
                            <button type="button" style="width: 95%" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalLong">
                              WEEK
                            </button>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Pareto By Week</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    <div id="modal_week">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                
                </div>
              <div class="row" >
                 <div class="col">
                  <div style="" class="table-responsive" >
                    <table class="table table-hover">
                      <tbody>
                        <tr>
                        <td>
                          <div class="btn-group" data-toggle="buttons">
                            <button type="button" id="category" class="btn category  btn-info <?php if($this->input->get('category') == "ALL" || $this->input->get('category') == ""){echo "active";} ?>" onclick="change_category('ALL')">ALL</button>
                                      <?php
                          foreach ($category->result() as $category) {
                          ?>
                            <button type="button" id="<?php echo $category->category; ?>" class="btn category  btn-info" onclick="change_category('<?php echo $category->category; ?>')"><?php echo $category->category; ?></button>
                          <?php
                          }
                         ?>
                        </div>
                        </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div> 
                <div class="col text-right">
                    <!-- Button trigger modal -->
                  <button type="button" class="btn btn-info" onclick="table_otif()" data-toggle="modal" data-target="#otif_code">
                    Daily Loss Tree Code
                  </button>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_otif_code">
                    Add Tree Code
                  </button>
                  
                  <!-- Modal -->
                  <div class="modal fade" id="otif_code" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Daily Loss Tree Code</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div id="table_otif">
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!-- Modal -->
                  <div class="modal fade" id="add_otif_code" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLongTitle">Add Loss Tree Code</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="row">
                            <div class="col">
                              <div class="form-group">
                                <label>Code</label>
                                <input type="text" id="code" class="form-control" name="Code">
                              </div>
                            </div>
                          </div>

                         <!--  <div class="row">
                            <div class="col">
                              <select required="" id="level" name="level" class="form-control">
                                <option value="">Select Level</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                              </select>
                            </div>
                          </div> -->

                          <div class="row">
                            <div class="col">
                              <div class="form-group">
                                <label>Loss Tree</label>
                                <input type="text" id="loss_tree_input" class="form-control" name="loss_tree">
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col">
                              <div class="form-group">
                                <label>Example</label>
                                <textarea class="form-control" id="example" name="example"></textarea> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="button" onclick="save_otif_code()" class="btn btn-primary">Save changes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>


      
      <div id="loss_tree">
        <div class="col loading_loss_tree" style="text-align:center">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
              <i class="fas fa-chart-bar" style="font-size:30px"></i>
              </div>
              <h4 class="card-title">LOSS TREE</h4>
            </div>
            <div class="card-body ">
              <div class="col loading_report_otif" style="text-align:center">
                <div class="card">
                  <div class="card-body">
                    <img src="<?php echo base_url() ?>assets/images/Loading.gif" width="50" height="50">
                  </div>
                </div>
              </div>
            </div>
            </div>
        </div>
      </div>
            
          
  <!-- report Otif -->
      
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
              <i class="fas fa-chart-bar" style="font-size:30px"></i>
              </div>
              <h4 class="card-title">REPORT OTIF</h4>
            </div>
            <div class="card-body ">
              <div id="report_otif">
              </div> 
              <div class="col loading_report_otif" style="text-align:center">
                <div class="card">
                  <div class="card-body">
                    <img src="<?php echo base_url() ?>assets/images/Loading.gif" width="50" height="50">
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
      </div>
         
  <!-- End report Otif -->
    </div>
  </div>
</div>
<script src="<?php echo base_url() ?>assets/dist/js/spin.umd.js"></script>
<script>
  var year = <?php echo $this->input->get('year'); ?>;
  var category = 'ALL';
  var frist_month = 0;
  var last_month = 0;
  var month_diff = 0;
  var month = [];
  var week = [];
  var vendor_code = "21632";

  data_report();

  function CellClick(cell){
    if(frist_month == 0){
       month.push( parseInt($('#'+cell.id).attr("value")) );
      frist_month = $('#'+cell.id).attr('value');
       $('#'+cell.id).css('background-color','#00acc1');
       $('#'+cell.id).addClass('selectedMonth');
    }else{
      last_month = parseInt($('#'+cell.id).attr('value'));
      if (window.event.button === 0) {
        if (!window.event.ctrlKey && !window.event.shiftKey) {
            month = [];
            clear_month();
            month.push(parseInt($('#'+cell.id).attr("value")) );
            $('#'+cell.id).css('background-color','#00acc1');
            $('#'+cell.id).addClass('selectedMonth');

        }else if (window.event.shiftKey) {
          month = [];
          
            if(frist_month < last_month){
              for (var i = frist_month; i <= last_month; i++) {
                month.push(i);
                $('#month'+i).css('background-color','#00acc1');
                $('#month'+i).addClass('selectedMonth');
              }
            }else if(frist_month > last_month){
              for (var i = frist_month; i >= last_month; i--) {
                month.push(i);
                $('#month'+i).css('background-color','#00acc1');
                $('#month'+i).addClass('selectedMonth');
              }
            }
            }
        
        frist_month = last_month;
       }
    }
      get_week();
      console.log(month);
    
  }

  function clear_month(){
    $('.month').css({ 'background-color' : ''});
    $('.month').removeClass('selectedMonth');
  }


  function change_vendor(e){
    vendor_code = e;
    data_report(week);
  }

  function change_category(e){
    category = e;
    $('.category').removeClass('active');
    data_report(week);
  }

  function get_week(){
    $.ajax({
      url : "<?php base_url();  ?>report/get_week",
      method : "post",
      data : {
        "year" : year,
        "category" : category,
        "month": month,
        "week" : week,
        "vendor_code" : vendor_code
      },
      dataType : "HTML",
      success: function(e){
        $('#modal_week').html(e);
      },error: function(e){

      }
    });
  }

  var frist_week = '';
  var last_week = '';
  var week_diff = 0;
  function WeekClick(cell){
    if(frist_week == ''){
      clear_week();
      frist_week = $('#'+cell).attr('value');
      week.push(parseInt($('#'+cell).attr("value")) );
       $('#'+cell).css("background-color",'rgb(11, 117, 201)');
       $('#'+cell).addClass('selectedWeek');
    }else{
      last_week = parseInt($('#'+cell).attr('value'));
      if (window.event.button === 0) {
        if (!window.event.ctrlKey && !window.event.shiftKey) {
          week = [];
            clear_week();
            week.push(parseInt($('#'+cell).attr("value")) );
            $('#'+cell).addClass('selectedWeek');
            $('#'+cell).css("background-color",'rgb(11, 117, 201)');
        }else if (window.event.shiftKey) {
          week = [];
          if(frist_week < last_week){
            for (var i = frist_week; i <= last_week; i++) {
              week.push(parseInt(i));
              $('#Week'+i).addClass('selectedWeek');
              $('#Week'+i).css("background-color",'rgb(11, 117, 201)');
            }
          }else if(frist_week > last_week){
            for (var i = frist_week; i >= last_week; i--) {
              week.push(parseInt(i));
              $('#Week'+i).addClass('selectedWeek');
              $('#Week'+i).css("background-color",'rgb(11, 117, 201)');
            }
          }

        }else{
          week.push( parseInt($('#'+cell).attr("value")) );
          $('#'+cell).css("background-color",'rgb(11, 117, 201)');
          $('#'+cell).addClass('selectedWeek');
        }
        frist_week = last_week;
       }
    }
    console.log(week);
  }

  function clear_week(){
    $('.week.active').removeClass('selectedWeek');
    $('.Week').css('background-color', '#9c27b0');
  }

//REPORT
  function save_select_otif(a, b){
    $.ajax({
      url : "<?php echo base_url(); ?>report/update_otif_LossTree",
      data : {
        "id_schedule" : a,
        "id_otif"     : b
      },
      method: "GET",
      success:function(e){
        alert("Loss Tree berhasil di update");
        loss_tree();
      },error:function(e){
        alert("Something Error");
      }
    });
  }

var opts = {
  lines: 13, // The number of lines to draw
  length: 10, // The length of each line
  width: 5, // The line thickness
  radius: 10, // The radius of the inner circle
  scale: 1, // Scales overall size of the spinner
  corners: 1, // Corner roundness (0..1
  speed: 1, // Rounds per second
  rotate: 0, // The rotation offset
  animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
  direction: 1, // 1: clockwise, -1: counterclockwise
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  className: 'spinner', // The CSS class to assign to the spinner
  top: '50%', // Top position relative to parent
  left: '50%', // Left position relative to parent
  shadow: '0 0 1px transparent', // Box-shadow for the lines
  position: 'fixed' // Element positioning
};
  
function data_report(){
  var spinner = new Spin.Spinner(opts).spin();
  
    console.log(week);
    $.ajax({
      url : "<?php echo base_url(); ?>report/data_report",
      data : {
        "year" : year,
        "week" : week,
        "vendor_code" : vendor_code,
        "category"  : category
      },
      method: "POST",
      dataType: "HTML",
      beforeSend: function () {
          $('#loading').append(spinner.el);
        },
      success:function(e){
        $('#report_otif').html(e);
        loss_tree();
        spinner.stop();
        $('.loading_report_otif').hide();
      },error:function(e){
        alert("Data Report, Something Error");
      }
    });
  }

  function loss_tree(){
    var spinner = new Spin.Spinner(opts).spin();
    $.ajax({
      url : "<?php echo base_url(); ?>report/loss_tree",
      data : {
        "year" : year,
        "week" : week,
        "vendor_code" : vendor_code,
        "category" : category
      },
      method: "POST",
      dataType: "HTML",
      beforeSend: function () {
          $('#loading').append(spinner.el);
        },
      success:function(e){
        $('#loss_tree').html(e);
        spinner.stop();
        if(frist_week == ''){
          frist_week = "<?php echo date('W'); ?>" 
        }
        getWeek = "Week "+frist_week;

        if(last_week != ''){
          total_week = week.length;
          if(total_week > 1){
            end_week = week[total_week-1];
            frist_week = week[0];
            getWeek = "Week "+frist_week+" - Week "+end_week;
          }
          
        }
        $('.getWeek').html(getWeek);

      },error:function(e){
        alert("Loss Tree, Something Error");
      }
    });
  }

  function save_otif_code(){
    var code = $('#code').val();
    var level = $('#level').val();
    var loss_tree = $('#loss_tree_input').val();
    var example = $('#example').val();
    $.ajax({
      url: "<?php echo base_url() ?>report/save_otif_code",
      data: {
        "code"      : code,
        "level"     : level,
        "loss_tree" : loss_tree,
        "example"   : example
      },success:function(e){
        alert("Data otif code "+code+" berhasil disimpan");
      },error:function(e){
        Alert("Something Error");
      }
    });
  }

  function delete_otif(a){
       $.ajax({
      url: "<?php echo base_url() ?>report/delete_otif",
      data: {
        "id" : a
      },
      success:function(e){
        table_otif();
      },error:function(e){
        alert("Something Error");
      }
    });
  }

  function table_otif(){
    $('#table_otif').empty();
    $.ajax({
      url: "<?php echo base_url() ?>report/table_otif",
      dataType:"HTML",
      data:{
        "vendor_code" : "<?php echo $this->input->get('vendor_code'); ?>"
      },
      success:function(e){
        $('#table_otif').html(e);
      },error:function(e){
        alert("Something Error");
      }
    });
  }

</script>
      


