<style type="text/css">
  @media screen and (max-width: 600px) {
    .filter {
      text-align: center;
    }
  }

  @media screen and (min-width: 800px) {
    .filter {
      margin-top: 20px;
      position: absolute;
      display: inline-block;
      z-index: 9999;
    }
  }
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i style="font-size:30px" class="fas fa-file-archive"></i>
              </div>
              <h4 class="card-title">Master Data Material</h4>
            </div>
            <div class="card-body ">

              <div class="container-fluid">

                <div class="row">
                  <div class="col-6">
                    <!--  <button type="button" id="saveModal" onclick="$('#form').attr('action','<?php echo base_url(); ?>master_data/add_material'), openModal()" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Add Master</button> -->
                    <button type="button" <?php if (!check_sub_menu(14)) {
                                            echo "hidden";
                                          } ?> onclick="$('#upload').click()" class="btn btn-success btn-sm">
                      <i class="fa fa-upload"> </i> Upload
                    </button>
                    <button type="button" <?php if (!check_sub_menu(15)) {
                                            echo "hidden";
                                          } ?> onclick="window.location.href = `<?php echo base_url('master_data/download_excel') ?>`;" class="btn btn-info btn-sm">
                      <i class="fa fa-download"> </i> Download
                    </button>

                    <button type="button" <?php if (!check_sub_menu(16)) {
                                            echo "hidden";
                                          } ?> onclick="window.location.href = `<?php echo base_url('master_data/download_template') ?>`;" class="btn btn-primary btn-sm">
                      <i class="fa fa-download"> </i> Download Template
                    </button>

                    <img id="loading" style="display:none" width="40" height="40" src="<?php base_url() ?>assets/images/Loading.gif">
                    <b style="color:green"><?php if ($this->session->flashdata('SCS') != '') {
                                              echo $this->session->flashdata('SCS');
                                            } ?></b>
                    <form method="POST" action="<?php echo base_url() ?>master_data/upload_master" enctype="multipart/form-data">
                      <input type="file" onchange='$("#upload_submit").click();$("#loading").show()' style="display:none" name="upload_master" id="upload">
                      <button type="submit" style="display:none" id="upload_submit"></button>
                    </form>

                  </div>

                  <div class="col-md-12" style="margin-top:20px">
                    <?php $no = 1; ?>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-hover" id="datatables">
                        <thead class="" style="color:white; background-color:#31559F;font-weight: 650">
                          <tr>
                            <td>No</td>
                            <td>Material Code</td>
                            <td>Material Name</td>
                            <td>Type</td>
                            <td>UOM</td>
                            <td>Exp</td>
                            <td width="60px">Shelf Loc.</td>
                            <td width="50">Uom Per Pallet</td>
                            <td class="text-center" width="50">Action</td>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>

                <button type="button" id="openModal" onclick="$('#form').attr('action','<?php echo base_url(); ?>master_data/update_material')" style="display:none" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>
                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close " data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"></h4>
                      </div>
                      <form action="" id="form" method="POST">
                        <div class="modal-body">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Material Code</label>
                                  <input type="text" class="form-control" id="material_code" name="material_code">
                                  <input type="text" hidden name="idz" id="idz" value="">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Material Name</label>
                                  <input type="text" required class="form-control" id="material_name" name="material_name">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Type</label>
                                  <input type="text" required class="form-control" id="type" name="type">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit of Measurements</label>
                                  <input type="text" required class="form-control" id="uom" name="uom">
                                </div>
                              </div>
                              <div class="col-md-4">
                                <div class="form-group">
                                  <label>Unit / Pallet</label>
                                  <input type="number" required class="form-control" id="uom_pallet" name="uom_pallet">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <button id="button" class="btn btn-warning">Update</button>
                                </div>
                              </div>
                            </div>
                      </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                  </div>

                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>