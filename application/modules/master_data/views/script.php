<script>
	$(document).ready(function() {
		var table;

		//datatables
		table = $('#datatables').DataTable({

			"processing": true,
			"serverSide": true,
			responsive: true,
			"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>',
			"order": [],

			"ajax": {
				"url": "<?php echo base_url('master_data/get_data_master') ?>",
				"type": "POST"
			},
			"columns": [{
					"id": "no"
				},
				{
					"data": "material_sku"
				},
				{
					"data": "material_name"
				},
				{
					"data": "material_type"
				},
				{
					"data": "material_uom"
				},
				{
					"data": "exp"
				},
				{
					"data": "shelf_loc"
				},
				{
					"data": "uom_pallet"
				},
				{
					"render": function(data, type, row, meta) {
						return `								  
								  <button style="font-size:15px;margin-left:10px;" data-toggle="modal" data-target="#modalEdit` + row.material_sku + `" class="btn btn-just-icon btn-round btn-warning btn-sm"><i class="fa fa-edit"></i></button>
								  
								  <!-- Modal -->
									<div class="modal fade" id="modalEdit` + row.material_sku + `" role="dialog">
									<div class="modal-dialog">

										<!-- Modal content-->
										<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close " data-dismiss="modal">&times;</button>
											<h4 class="modal-title"></h4>
										</div>
										<form action="<?= base_url('master_data/editMaterial'); ?>" id="form" method="POST">
											<div class="modal-body">
											<div class="container-fluid">
												<div class="row">
												<div class="col-md-6">
													<label>Material Code</label>
													<input type="text" class="form-control" disabled value="` + row.material_sku + `">
													<input type="text" hidden name="idz" id="idz" value="` + row.id + `">
													<input type="text" hidden id="material_code" name="material_code" value="` + row.material_sku + `">
												</div>
												</div>

												<div class="row">
												<div class="col-md-12">
													<div class="form-group">
													<label>Material Name</label>
													<input type="text" required class="form-control" id="material_name" name="material_name" value="` + row.material_name + `">
													</div>
												</div>
												</div>

												<div class="row">
												<div class="col-md-6">
													<div class="form-group">
													<label>Type</label>
													<input type="text" required class="form-control" id="type" name="type" value="` + row.material_type + `">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
													<label>Expire (Days)</label>
													<input type="number" required class="form-control" id="exp" name="exp" value="` + row.exp + `">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
													<label>Unit of Measurements</label>
													<input type="text" required class="form-control" id="uom" name="uom" value="` + row.material_uom + `">
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group">
													<label>Unit / Pallet</label>
													<input type="number" required class="form-control" id="uom_pallet" name="uom_pallet" step="0.005" value="` + row.uom_pallet + `">
													</div>
												</div>
												<div class="col-md-6">
														<label>Shelf Location</label>
														<select class="form-control" id="shelf-loc" name="shelf_loc" required>
														<option value="">--SELECT--</option>
														<option value="1" `+((row.shelf_loc == "Zara 1")?`selected`:``)+`>Zara 1</option>
														<option value="2" `+((row.shelf_loc == "Zara 2")?`selected`:``)+`>Zara 2</option>
														</select>
												</div>
												</div>

										</div>
										<div class="modal-footer">
										<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
										&nbsp;&nbsp;
										<button id="button" class="btn btn-success">Update</button>
										</div>
										</form>
									</div>

									</div>	  							  
								  
								  `;
					}
				}

			]

		});

	});

	$("#table").DataTable({


	});


	function deletez(id) {

		var confirmz = confirm("Apakah anda ingin menghapus data ini ?");

		if (confirmz) {
			$.ajax({
				url: "<?php echo base_url('master_data/delete_data_material')  ?>",
				type: "POST",
				data: {
					'id': id
				},
				success: function(a) {
					location.reload();
				},
				error: function(a) {
					alert("Something Wrong");
				}

			});

		}
	}


	function openModal(id, status = 0) {

		var material_code = '';
		var material_name = '';
		var material_type = '';
		var material_uom = '';
		var uom_pallet = '';
		var session = "<?php $this->session->userdata('sess_role_no'); ?>";
		if (status == 1) {
			$.ajax({
				url: "<?php echo base_url('master_data/get_data_material')  ?>",
				type: "POST",
				data: {
					'id': id
				},
				dataType: "JSON",
				success: function(a) {
					material_code = a[0].material_sku;
					material_name = a[0].material_name;
					material_type = a[0].material_type;
					material_uom = a[0].material_uom;
					uom_pallet = a[0].uom_pallet;
				},
				error: function(a) {
					alert("Something Wrong");
				},
				async: false

			});

			$('.modal-title').html(material_code + ' - ' + material_name);
			$('#material_code').val(material_code);
			$('#material_name').val(material_name);
			$('#type').val(material_type);
			$('#uom').val(material_uom);
			$('#uom_pallet').val(uom_pallet);
			$('#button').attr('class', "btn btn-warning").html("Update");
			$('#idz').val(id);
			$('#openModal').click();
		} else {
			$('.modal-title').html("Add New Master");
			$('#material_code').val("");
			$('#material_name').val("");
			$('#type').val("");
			$('#uom').val("");
			$('#uom_pallet').val("");
			$('#button').attr('class', "btn btn-primary").html("Save");
		}
	}
</script>