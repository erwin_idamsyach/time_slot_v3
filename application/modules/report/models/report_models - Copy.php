<?php

class report_models extends CI_Model{

	public function getReport($date_list, $week, $year, $vendor_code, $category="", $start_date = '', $end_date = ''){
		if($category=="ALL"){
			$category = "";
		}
		
		$no = 0;
		$noo = 1;
		$data = [];
		$shift_list = array(
			"DM", "DP", "DS"
		);
		$array_report = array();

		$getData = $this->db->query("SELECT a.id,
											a.po_number, 
											a.po_line_item, 
											a.material_code,
											b.material_name,
											a.requested_delivery_date AS rdd, a.shift,
											a.qty,
											a.uom_plt,
											a.otif,
											a.qty_ori
											FROM tb_rds_detail AS a 
											LEFT JOIN skin_master.ms_material AS b ON a.material_code = b. material_sku
											INNER JOIN tb_scheduler AS c ON a.id = c.id_schedule 
											INNER JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number
											WHERE
											c.rdd BETWEEN '$start_date' AND '$end_date'
											AND a.vendor_code = '$vendor_code'
											AND a.category LIKE '%$category%'
											GROUP BY a.id
											ORDER BY a.requested_delivery_date
											")->result();

		$array_report = array();
		$data_loss_tree = array();
		foreach ($getData as $get) {
			$check_date = 0;
			$check_load_optimized = 0;
			$check_delay_1 = 0;
			$check_delay_2 = 0;
			$check_delay_3 = 0;
			$check_outstanding = 0;
			$check_cancel = 0;
			$check_shift = 0;
			$first_read= 0;
			$qty_ori = $get->qty_ori;
			$qty_rev = $get->qty;
			$array_report[$no] = array(
				"id"				=> $get->id,
				"po_number" 		=> $get->po_number,
				"po_line_item" 		=> $get->po_line_item,
				"material_code"		=> $get->material_code,
				"material_name"		=> $get->material_name,
				"rdd"				=> $get->rdd,
				"shift"				=> $get->shift,
				"qty_ori"			=> $qty_ori,
				"qty_rev"			=> $qty_rev
			);
			$otif_shift = 0;
		 	foreach ($date_list as $date) {
		 		foreach ($shift_list as $shift) {
		 			$getDataShift = $this->db->query("SELECT IF(SUM(a.receive_amount) > 0 , SUM(a.receive_amount), 0) AS ra, b.rdd 
					  FROM tb_scheduler AS a
					  INNER JOIN tb_schedule_detail AS b ON a.schedule_number = b.schedule_number
					  INNER JOIN ms_time_slot AS c ON b.id_time_slot = c.id
					  WHERE a.id_schedule = '$get->id' AND c.shift = '$shift' AND b.rdd = '$date' AND b.vendor_code = '$vendor_code' 
					  GROUP BY a.id_schedule
						  ")->row();
		 			if(isset($getDataShift->ra) ){
		 				$dataShift = $getDataShift->ra*$get->uom_plt;

		 				if($get->rdd == $date && $get->shift == $shift){
		 					$otif_shift = $otif_shift + $dataShift;
		 					if($qty_rev > $otif_shift){
		 						if($first_read != 0){

		 						}else{
		 							dump("wkwkwk ".$get->po_number." ".$get->po_line_item);
		 							$check_partial = 1;
		 						}
		 					}
		 				}else if($get->rdd == $date){
		 					$check_partial = 0;
		 					
		 					if($get->shift == "DP" && $shift == "DM"){
		 						$check_load_optimized = 1;
		 					}else if($get->shift == "DS" && $shift != "DS"){
		 						$check_load_optimized = 1;
		 					}
		 				}else if($date < $get->rdd){
		 					$check_partial = 0;
		 					$check_load_optimized = 1;
		 				}
		 				
		 			}else{
		 				$dataShift = 0;
		 			}
		 			
		 			$array_report[$no][$date][$shift] = $dataShift;
		 		}
		 	}
		 	if($dataShift > 0 ){
		 		$data_loss_tree[$get->po_number.$get->po_line_item.$date.$shift] = 1;
		 	}
		 	
		 	$otif_shift = $otif_shift;
		 	if($qty_ori == $qty_rev){
		 		$qty_status = "original";
		 	}else{
		 		$qty_status = "Revised Qty";
		 	}

		 	$loss_tree_status = 0;

		 	if($check_partial > 0){
			 	$loss_tree_status = 1;
			}else if($check_load_optimized > 0){
				$loss_tree_status = 2;
			}
		 	$noo++;
		 	$array_report[$no]["otif_shift"] = $otif_shift;
		 	$array_report[$no]["otif_quantity"] = $qty_rev - $otif_shift;
		 	$array_report[$no]["otif"] = ABS(round((($get->qty - ($get->qty - $otif_shift)) / $get->qty) * 100, 2) ) ;
		 	$array_report[$no]["otif_loss_tree"] = $loss_tree_status;
		 	$array_report[$no]["qty_status"] = $qty_status;
		 	$no++;
		 }
		 return $array_report;
	}

	public function getDataOtif($date_list, $array_report, $vendor_code, $week = 0, $category = '', $year){
		if($category=="ALL"){
			$category = "";
		}

		$shift_list = array(
			"DM", "DP", "DS"
		);

		$data = [];
		$total = 0;
		$total_actual = 0;
		$check_data = false;
		$data_otif = array();
		$check_week_now = date('YW');
		if(is_array($week) ){
			$week = $week[0];
		}
		$check_data_week = $year.$week;
		if($check_week_now <= $check_data_week){
			$check_data = false;
		}else{
			$check_data = true;
		}
		
		$check_avaible = $this->db->query("SELECT id FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) ='$year' ")->num_rows();
		if($check_avaible > 0){

		}else{
			foreach ($date_list as $date) {
				foreach ($shift_list as $shift) {
					$data = $this->db->query("SELECT SUM(a.qty) as total FROM tb_rds_detail AS a 
											  WHERE a.requested_delivery_date = '$date' AND shift = '$shift' AND a.vendor_code = '$vendor_code' AND a.category LIKE '%$category%' ")->row()->total;
					if($data == null){
						$data = 0;
					}
					$data_otif[$date]["plan"][$shift] = $data;
					$total = $total+$data;
				}
			}

			foreach ($date_list as $date) {
				$DM = 0;
				$DP = 0;
				$DS = 0;
				$data_otif[$date]["actual"]["DM"] = $DM;
				$data_otif[$date]["actual"]["DP"] = $DP;
				$data_otif[$date]["actual"]["DS"] = $DS; 
				foreach ($array_report as $get) {
					$DM = $DM + (($get[$date]["DM"]*$get["otif"])/100);
					$DP = $DP + (($get[$date]["DP"]*$get["otif"])/100);
					$DS = $DS + (($get[$date]["DS"]*$get["otif"])/100);
					$data_otif[$date]["actual"]["DM"] = round($DM,2);
					$data_otif[$date]["actual"]["DP"] = round($DP,2);
					$data_otif[$date]["actual"]["DS"] = round($DS,2); 
				}
				$otif_DM = $data_otif[$date]["plan"]["DM"];
				$otif_DP = $data_otif[$date]["plan"]["DP"];
				$otif_DS = $data_otif[$date]["plan"]["DS"];
				$total_actual = round($total_actual + $DM + $DP + $DS, 2);
				if($otif_DM > 0 ){
					$oDM = ABS(round(($otif_DM - ABS($DM - $otif_DM))/$otif_DM*100,2))."%";
					
				}else{
					$oDM = "0%";
				}

				if($otif_DP > 0 ){
					$oDP = ABS(round(($otif_DP - ABS($DP - $otif_DP))/$otif_DP*100,2))."%";
					
				}else{
					$oDP = "0%";
				}

				if($otif_DS > 0 ){
					$oDS = ABS(round(($otif_DS - ABS($DS - $otif_DS))/$otif_DS*100,2))."%";
				}else{
					$oDS = "0%";
				}
				$data_otif[$date]["otif"]["DM"] = $oDM;
				$data_otif[$date]["otif"]["DP"] = $oDP;
				$data_otif[$date]["otif"]["DS"] = $oDS;
				
			}

			$data_otif["total"] 		= $total;
			$data_otif["total_actual"] 	= $total_actual;
			if($total > 0){
				$total_otif = ABS(round(($total - ABS($total_actual - $total))/$total*100,2))."%";
			}else{
				$total_otif = "0%";
			}
			$data_otif["total_otif"]	= $total_otif;
		}
		$category = array(
			"plan", "actual", "otif"
		);
		$data_save= array();
		
		if($check_avaible == 0 && $check_data == true){
			foreach($date_list as $date){
				$data_save = array(
								"week" 					=> $week,
								"date"					=> $date
				);
					foreach($category as $ctr){
						foreach ($shift_list as $shift) {
							$data_save[$ctr."_".$shift] = $data_otif[$date][$ctr][$shift];
						}
					}
				$this->db->insert("tb_otif_loss_tree", $data_save);
			}
			dump("saved");
		}
		return $data_otif;
	}

	public function getSavedOtif($date_list, $vendor_code, $week, $category, $year){
		$data = array();
		$category = array(
			"plan", "actual", "otif"
		);
		$shift_list = array(
			"DM", "DP", "DS"
		);
		if(is_array($week)){
			$week = $week[0];
		}
		$getData = $this->db->query("SELECT * FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) = '$year' ")->result();
		foreach($date_list as $date){
			foreach ($category as $ctr) {
				foreach ($shift_list as $shift) {
					$data[$date][$ctr][$shift] = 0;
				}
			}
		}
		foreach($getData as $get){
			$data[$get->date]["plan"]["DM"] = $get->plan_dm;
			$data[$get->date]["plan"]["DP"] = $get->plan_dp;
			$data[$get->date]["plan"]["DS"] = $get->plan_ds;

			$data[$get->date]["actual"]["DM"] = $get->actual_dm;
			$data[$get->date]["actual"]["DP"] = $get->actual_dp;
			$data[$get->date]["actual"]["DS"] = $get->actual_ds;

			$data[$get->date]["otif"]["DM"] = round($get->otif_dm)."%";
			$data[$get->date]["otif"]["DP"] = round($get->otif_dp)."%";
			$data[$get->date]["otif"]["DS"] = round($get->otif_ds)."%";

		}
		$getTotalDataPlan = $this->db->query("SELECT ( SUM(plan_dm) + SUM(plan_dp) + SUM(plan_ds)) AS plan FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) = '$year' GROUP BY week")->row()->plan;
		$getTotalDataActual = $this->db->query("SELECT ( SUM(actual_dm) + SUM(actual_dp) + SUM(actual_ds)) AS actual FROM tb_otif_loss_tree WHERE week = '$week' AND YEAR(date) = '$year' GROUP BY week")->row()->actual;
		if($getTotalDataPlan > 0){
			$getTotalDataOtif = ABS(round(($getTotalDataPlan - ABS($getTotalDataActual - $getTotalDataPlan))/$getTotalDataPlan*100))."%";
		}else{
			$getTotalDataOtif = "0%";
		}
		$data["total"] = $getTotalDataPlan;
		$data["total_actual"] = $getTotalDataActual;
		$data["total_otif"] = $getTotalDataOtif;
		return $data;
	}

	public function otifLossTree($array_report){
		$get_otif = $this->db->query("SELECT id, loss_tree FROM ms_otif_loss_code");
		$count_ms_otif = $get_otif->num_rows();
		$loss_tree = $get_otif->result();
		$data = array();
		$otifTotal = array();
		for ($i=0; $i < $count_ms_otif ; $i++) {
			$otifTotal[$loss_tree[$i]->loss_tree] = 0;
			$data[$loss_tree[$i]->loss_tree] = array_filter($array_report, function($var) use($i){ 
				return ($var['otif_loss_tree'] == ($i+1));
				} 
			);
			$total = 0;
			foreach ($data[$loss_tree[$i]->loss_tree] as $get => $value) {
				$total = $value["otif_quantity"] + $total;
				$otifTotal[$loss_tree[$i]->loss_tree] = $total;
			}
		}
		return $otifTotal;
	}

	public function getDataVendor(){
		$data = $this->db->query("SELECT a.id, a.vendor_code, a.vendor_name, a.vendor_alias FROM skin_master.ms_supplier AS a")->result();
		return $data;
	}

	public function getVendor($vendor_code){
		$data = $this->db->query("SELECT a.id, a.vendor_code, a.vendor_name, a.vendor_alias FROM skin_master.ms_supplier AS a WHERE vendor_code = '$vendor_code' ")->row();
		return $data;
	}

	public function getDataMsOtif(){
		$data = $this->db->query("SELECT a.id, a.loss_tree FROM ms_otif_loss_code AS a")->result();
		return $data;
	}

	public function DataOntime($start_date, $end_date, $vendor_code = ''){
		$data = $this->db->query("SELECT a.receipt, 
								a.arrive_date, 
								a.arrive_time, 
								a.receive_date, 
								a.receive_time,
								b.police_number, 
								d.start_time, 
								d.end_time,
								CASE
									WHEN a.arrive_date < c.rdd OR a.arrive_time < d.start_time
										THEN 'Early'
									WHEN a.arrive_date > c.rdd OR a.arrive_time > d.start_time
										THEN 'Delay'
									ELSE 'Good'
								END AS status,
								CASE
									WHEN a.arrive_date < c.rdd OR a.arrive_time < d.start_time
										THEN  @early := @early + 1
									WHEN a.arrive_date > c.rdd OR a.arrive_time > d.start_time
										THEN @delay := @delay + 1
									ELSE @good := @good + 1
								END AS counter,
								@early AS early,
								@delay AS delay,
								@good AS good,
								c.rdd, 
								d.shift 
								FROM tb_delivery_detail AS a 
								LEFT JOIN ms_truck AS b ON a.id_truck = b.id 
								INNER JOIN tb_schedule_detail AS c ON a.id_schedule_group = c.schedule_number
								INNER JOIN ms_time_slot AS d ON c.id_time_slot = d.id
								CROSS JOIN (SELECT @early := 0) AS e
								CROSS JOIN (SELECT @delay := 0) AS f
								CROSS JOIN (SELECT @good := 0) AS g
								WHERE c.rdd BETWEEN '$start_date' AND '$end_date' AND a.arrive_date != '' AND a.receive_date != '' AND c.vendor_code LIKE '%$vendor_code%'
								 ")->result();
		return $data;
	}

	public function getDataUnloading($date_list, $data_list, $shift_list){
		$data = array();
		$no = 0;
		foreach($data_list as $getData){
			$arrived = strtotime($getData->arrive_date.' '.$getData->arrive_time);
			$received = strtotime($getData->receive_date.' '.$getData->receive_time);
			$total = ($received - $arrived) / 60;
			
			foreach ($date_list as $date) {
				foreach ($shift_list as $shift) {
					$data[$no][$date][$shift] = '';
				}
			}

			$data[$no][$getData->rdd][$getData->shift] = $total;
			$no++;
			 
		}
		return $data;
	}

	public function getAverageUnloading($date_list, $data_unloading, $shift_list){
		$data = array();
		$total = 0;
		$Unloading_DP = 0;
		$Unloading_DM = 0;
		$Unloading_DS = 0;
		$data["DM"] = 0;
		$data["DP"] = 0;
		$data["DS"] = 0;
		$data["average"] = 0;

		foreach ($date_list as $date) {
			foreach ($shift_list as $shift) {

				if(count($data_unloading) > 0){
					for($i = 0; $i< count($data_unloading); $i++ ){
						$data[$date][$shift] = 0;
					}	
				}else{
					for($i = 0; $i< 1; $i++ ){
						$data[$date][$shift] = 0;
					}
				}
				
			}
		}

		$count_DM = 0;
		$count_DP = 0;
		$count_DS = 0;

		if(!empty($data_unloading) ){
			foreach ($date_list as $date) {
				foreach ($shift_list as $shift) {
					for($i = 0; $i< count($data_unloading); $i++ ){
						$total = $total + $data_unloading[$i][$date][$shift];
						if($shift == 'DM'){
							$Unloading_DM = $Unloading_DM + $data_unloading[$i][$date][$shift];
							if($data_unloading[$i][$date][$shift] > 0){
								$count_DM++;
							}
						}else if($shift == 'DP'){
							$Unloading_DP = $Unloading_DP + $data_unloading[$i][$date][$shift];
							if($data_unloading[$i][$date][$shift] > 0){
								$count_DP++;
							}
						}else{
							$Unloading_DS = $Unloading_DS + $data_unloading[$i][$date][$shift];
							if($data_unloading[$i][$date][$shift] > 0){
								$count_DS++;
							}
						}
						$data[$date][$shift] = $data[$date][$shift] + $data_unloading[$i][$date][$shift];
					}
					$data[$date][$shift] = $data[$date][$shift];
				}
			}
		if($count_DM == 0){
			$count_DM = 1;
		}
		if($count_DP == 0){
			$count_DP = 1;
		}
		if($count_DS == 0){
			$count_DS = 1;
		}
		$data["DM"] = ($Unloading_DM / $count_DM);
		$data["DP"] = ($Unloading_DP / $count_DP);
		$data["DS"] = ($Unloading_DS / $count_DS);
		$data["average"] = round($total / count($data_unloading), 2);
		}
		dump($data);
		return $data;
	}

	public function getDataChart($data_average, $week, $year){
		$getDateTime = new DateTime();
		$total_week = count($week);
		$data = array();
		$date = array();
		if($total_week == 1){
			if(is_array($week)){
				$week = $week[0];
			}
			$total_DM = 0;
			$total_DP = 0;
			$total_DS = 0;

			$getDate = $getDateTime->setISODate($year, $week)->format('Y-m-d');
			$date[0][$week]["DM"] = $total_DM + $data_average[$getDate]["DM"];
			$date[0][$week]["DP"] = $total_DP + $data_average[$getDate]["DP"];
			$date[0][$week]["DS"] = $total_DS + $data_average[$getDate]["DS"];
			$w = 1;
			
			while($w < 6)	{
				$getDate = $getDateTime->modify('+1 days')->format('Y-m-d');
				$total_DM = $total_DM + $data_average[$getDate]["DM"];
				$total_DP = $total_DP + $data_average[$getDate]["DP"];
				$total_DS = $total_DS + $data_average[$getDate]["DS"];
				$date[0][$week]["DM"] = $total_DM;
				$date[0][$week]["DP"] = $total_DP;
				$date[0][$week]["DS"] = $total_DS;
			  	$w++;
			}

		}else{
			for($i=0; $i < $total_week; $i++){
				$getDate = $getDateTime->setISODate($year, $week[$i])->format('Y-m-d');
				$w = 1;
				$total_DM = 0;
				$total_DP = 0;
				$total_DS = 0;
				while($w < 7)	{
					$getDate = $getDateTime->modify('+1 days')->format('Y-m-d');
					$total_DM = $total_DM + $data_average[$getDate]["DM"];
					$total_DP = $total_DP + $data_average[$getDate]["DP"];
					$total_DS = $total_DS + $data_average[$getDate]["DS"];
					$date[$i][$week[$i]]["DM"] = $total_DM;
					$date[$i][$week[$i]]["DP"] = $total_DP;
					$date[$i][$week[$i]]["DS"] = $total_DS;
				  	$w++;
				}
			}
		}
		return $date;
	}

	public function total_date($data_otif, $date_list){
		$shift_list = array(
			"DM", "DP", "DS"
		);
		$no=0;
		foreach ($date_list as $date) {
			$average = 0;
			$average2 = 0;
			foreach ($shift_list as $shift) {
				$data_list =str_replace('%','',($data_otif[$date]["otif"][$shift]));
				$average = $average + $data_list;
				// $data_list2 =str_replace('%','',($data_otif[$date]["actual"][$shift]));
				// $average2 = $average2 + $data_list2;
			}
			$total_date[$no]["otif"] = round($average/3,2);
			//$total_date[$no]["actual"] = round($average2);
			$no++;
		}
		
		return $total_date;
	}

	
}

?>