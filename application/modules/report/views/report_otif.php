<style>
  td{
    font-size:10px;
  }
  th{
    font-size:10px;
  }
</style>
              
<?php 
  if(count($week) == 1 && is_array($week)){
    $week = $week[0];
  }
?>

<ul class="nav nav-pills nav-pills-icons" role="tablist">
      <?php $no = 0; 
      if(count($week) > 1 ){ 
        foreach($week as $weekz){
        ?>
      <li class="nav-item">
        <a class="nav-link <?php if($no ==0 ){echo "active";} ?>" href="#week-<?php echo $weekz; ?>" role="tab" data-toggle="tab">
            W<?php echo $weekz; $no++?>
        </a>
      </li>
      <?php 
      }
    }else{ ?>
      <li class="nav-item">
        <a class="nav-link active" href="#week-<?php echo $week; ?>" role="tab" data-toggle="tab">
            W<?php echo $week; ?>
        </a>
      </li>
      <?php } ?>
</ul>

<div class="tab-content tab-space">
  <?php $no = 0;
        $no_week = 0;
      if(count($week) > 1 ){
        foreach($week as $weekz){
          $getDateTime = new DateTime();
          $date_list[0] = $getDateTime->setISODate($year, $weekz)->format('Y-m-d');
          $i = 1;
            while($i < 7){
            $date_list[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
            $i++;
          }
        ?>
      <div class="tab-pane <?php if($no ==0){echo "active";} ?>" id="week-<?php echo $weekz; ?>">
          <a href="<?php echo base_url() ?>report/download_otif?year=<?php echo $year; ?>&week=<?php echo $weekz; ?>&vendor_code=<?php echo $vendor_code; ?>&category=<?php echo $category; ?>"><button class="btn btn-primary"><i class="fas fa-download"></i> Download W<?php echo $weekz; $no++; ?></button></a>
          <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover tables" width="100%">
            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
              <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Po Number</th>
                <th rowspan="2">Line</th>
                <th rowspan="2">Material Code</th>
                <th rowspan="2">Material Description</th>
                <th rowspan="2">Req. Del. Date</th>
                <th rowspan="2">Shift</th>
                <th rowspan="2">Req.QTY</th>
                <th rowspan="2">Req.Qty Status</th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[0]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[1]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[2]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[3]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[4]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[5]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[6]; ?></th>
                <th rowspan="2">OTIF Shiftly</th>
                <th rowspan="2">
                  <?php if($this->uri->segment(2) == 'otif'){echo 'OTIF';}else{echo 'ON TIME';} ?> Weekly
                </th>
                <th rowspan="2" <?php if($weekz >= date('W')){
                    echo 'hidden';
                  } ?>>LOSS TREE
                </th>
              </tr>
              <tr>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($data_report[$weekz] as $get => $value) {
                
               ?>
              <tr>
                <td scope="row"><?php echo $no++; ?></td>
                <td><?php echo $value["po_number"]; ?></td>
                <td><?php echo $value["po_line_item"]; ?></td>
                <td><?php echo $value["material_code"]; ?></td>
                <td><?php echo $value["material_name"]; ?></td>
                <td><?php echo $value["rdd"]; ?></td>
                <td><?php echo $value["shift"]; ?></td>
                <td><?php echo $value["qty_rev"]; ?></td>
                <td><?php echo $value["qty_status"]; ?></td>
                <td <?php if($value[$date_list[0]]["DM"] > 0){
                  echo "style='background-color:green;color:white'";
                } 
                ?>
                ><?php echo $value[$date_list[0]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[0]]["DP"] > 0){
                  echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[0]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[0]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[0]]["DS"];  ?></td>
                <td
                  <?php if($value[$date_list[1]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[1]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[1]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                
                ><?php echo $value[$date_list[1]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[1]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[1]]["DS"];  ?></td>
                <td
                  <?php if($value[$date_list[2]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[2]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[2]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[2]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[2]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[2]]["DS"];  ?></td>
                <td
                  <?php if($value[$date_list[3]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[3]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[3]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[3]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[3]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[3]]["DS"];  ?></td>

                <td
                  <?php if($value[$date_list[4]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[4]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[4]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[4]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[4]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[4]]["DS"];  ?></td>

                <td
                  <?php if($value[$date_list[5]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[5]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[5]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[5]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[5]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[5]]["DS"];  ?></td>
                
                <td
                  <?php if($value[$date_list[6]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[6]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[6]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[6]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[6]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[6]]["DS"];  ?></td>
                
                <td><?php echo $value["otif_shift"]; ?></td>
                <td>
                  <?php echo $value["otif"].'%'; 
                  ?>

                </td>
                <td <?php if($weekz >= date('W')){
                    echo 'hidden';
                  } 

                  ?> >
                  <?php
                  if($value['otif_loss_tree'] != ""){
                    echo $data_select_otif[$value['otif_loss_tree']-1]->loss_tree; 
                  }
                  ?>
                </td>
                
              </tr>
            <?php } ?>
              
            </tbody>
          </table>
        </div>
      </div>
      <?php 
      }
    }else{ ?>
      <div class="tab-pane active" id="week-<?php echo $week;?>">
        <a href="<?php echo base_url() ?>report/download_otif?year=<?php echo $year; ?>&week=<?php echo $week; ?>&vendor_code=<?php echo $vendor_code; ?>&category=<?php echo $category; ?>"><button class="btn btn-primary"><i class="fas fa-download"></i> Download W<?php echo $week; $no++; ?></button></a>

        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover tables" width="100%" >
            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
              <tr>
                <th rowspan="2">No</th>
                <th rowspan="2">Po Number</th>
                <th rowspan="2">Line</th>
                <th rowspan="2">Material Code</th>
                <th rowspan="2">Material Description</th>
                <th rowspan="2">Req. Del. Date</th>
                <th rowspan="2">Shift</th>
                
                <th rowspan="2">Req.QTY</th>
                <th rowspan="2">Req.Qty Status</th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[0]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[1]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[2]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[3]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[4]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[5]; ?></th>
                <th style="text-align:center" colspan="3"><?php echo $date_list[6]; ?></th>
                <th rowspan="2">OTIF Shiftly</th>
                <th rowspan="2"><?php if($this->uri->segment(2) == 'otif'){echo 'OTIF';}else{echo 'ON TIME';} ?> Weekly</th>
                
                <th rowspan="2" <?php if($week >= date('W')){
                    echo 'hidden';
                  } ?>>LOSS TREE</th>
                
              </tr>
              <tr>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no = 1;
              foreach ($data_report as $get => $value) {
                
               ?>
              <tr>
                <td scope="row"><?php echo $no++; ?></td>
                <td><?php echo $value["po_number"]; ?></td>
                <td><?php echo $value["po_line_item"]; ?></td>
                <td><?php echo $value["material_code"]; ?></td>
                <td><?php echo $value["material_name"]; ?></td>
                <td><?php echo $value["rdd"]; ?></td>
                <td><?php echo $value["shift"]; ?></td>
                <td><?php echo $value["qty_rev"]; ?></td>
                <td><?php echo $value["qty_status"]; ?></td>
                <td <?php if($value[$date_list[0]]["DM"] > 0){
                  echo "style='background-color:green;color:white'";
                } 
                ?>
                ><?php echo $value[$date_list[0]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[0]]["DP"] > 0){
                  echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[0]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[0]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[0]]["DS"];  ?></td>
                <td
                  <?php if($value[$date_list[1]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[1]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[1]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                
                ><?php echo $value[$date_list[1]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[1]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[1]]["DS"];  ?></td>
                <td
                  <?php if($value[$date_list[2]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[2]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[2]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[2]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[2]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[2]]["DS"];  ?></td>
                <td
                  <?php if($value[$date_list[3]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[3]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[3]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[3]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[3]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[3]]["DS"];  ?></td>

                <td
                  <?php if($value[$date_list[4]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[4]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[4]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[4]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[4]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[4]]["DS"];  ?></td>

                <td
                  <?php if($value[$date_list[5]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[5]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[5]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[5]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[5]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[5]]["DS"];  ?></td>
                
                <td
                  <?php if($value[$date_list[6]]["DM"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[6]]["DM"];  ?></td>
                <td
                  <?php if($value[$date_list[6]]["DP"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[6]]["DP"];  ?></td>
                <td
                  <?php if($value[$date_list[6]]["DS"] > 0){
                    echo "style='background-color:green;color:white'";
                  } 
                  ?>
                ><?php echo $value[$date_list[6]]["DS"];  ?></td>
                
                <td><?php echo $value["otif_shift"]; ?></td>
               <td><?php echo ROUND($value["otif"],2).'%'; 
                  
                ?></td>
                <td <?php if($week >= date('W')){
                    echo 'hidden';
                  } ?> >
                  
                  <?php
                  if($value['otif_loss_tree'] != ""){
                    echo $data_select_otif[$value['otif_loss_tree']-1]->loss_tree; 
                  }
                  ?>
                </td>
                
              </tr>
            <?php } ?>
              
            </tbody>
          </table>
        </div>
      </div>
      <?php } ?>
</div>

<script type="text/javascript">
  $(".tables").DataTable({
    "dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
  });
</script>