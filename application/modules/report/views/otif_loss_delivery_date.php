﻿<?php $outstanding_week = 0; ?>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-header-icon card-header-info">
        <div class="card-icon">
          <i class="fas fa-calendar-check" style="font-size:35px"></i>
        </div>
        <h4 class="card-title">CONFIRM DELIVERY DATE
          <small class="getWeek"></small>
        </h4>
      </div>
      <div class="card-body" style="height: 400px">
        <div id="container2" style="height: 100%"></div>
      </div>
      <div class="card-body" style="height: 400px">
        <div id="container3" style="height: 100%"></div>
      </div>
    </div>
  </div>
  <div class="col-12">
    <div class="card card-chart">
      <div class="card-header card-header-icon card-header-danger">
        <div class="card-icon">
          <i class="fas fa-calendar-times" style="font-size:35px"></i>
        </div>
        <h4 class="card-title">OTIF LOSS TREE</h4>
      </div>
      <div class="card-body" style="height: 400px">
        <div id="container" style="height: 100%"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
<div class="card card-chart">
  <div class="card-header card-header-success card-header-icon">
    <div class="card-icon">
      <i class="fas fa-chart-bar" style="font-size:30px"></i>
    </div>
    <h4 class="card-title">LOSS TREE</h4>
  </div>
  <div class="card-body">
    <p class="card-category"> 
    <?php 
      if(count($week) == 1 && is_array($week)){
        $week = $week[0];
      }
    ?>

    <ul class="nav nav-pills nav-pills-icons" role="tablist">
      <?php $no = 0; 
      if(count($week) > 1 ){ 
        foreach($week as $weekz){
        ?>
      <li class="nav-item">
        <a class="nav-link <?php if($no ==0 ){echo "active";} ?>" href="#otif_week_<?php echo $weekz; ?>" role="tab" data-toggle="tab">
            W<?php echo $weekz; $no++?>
        </a>
      </li>
      <?php 
      }
    }else{ ?>
      <li class="nav-item">
        <a class="nav-link active" href="#otif_week-<?php echo $week; ?>" role="tab" data-toggle="tab">
            W<?php echo $week; ?>
        </a>
      </li>
      <?php 
  } ?>
    </ul>
    
    <div class="tab-content tab-space">
  <?php $no = 0;
        $no_week = 0;
      if(count($week) > 1 ){
        foreach($week as $weekz){
          $getDateTime = new DateTime();
          $date_listz[0] = $getDateTime->setISODate($year, $weekz)->format('Y-m-d');
          $i = 1;
            while($i < 7){
            $date_listz[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
            $i++;
          }
        ?>
    <div class="tab-pane <?php if($no ==0){echo "active";} ?>" id="otif_week_<?php echo $weekz; ?>">
        <!-- Tabel Otif Weekly -->
        <h4 class="card-title">OTIF LOSS TREE</h4>
          <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" >
            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
              <tr>
                <th>LOSS TREE</th>
                <?php foreach ($data_select_otif as $get) {
                  ?>
                  <th><?php echo $get->loss_tree; ?></th>

                <?php
                } ?>
                <th>Total Loss Tree</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>QTY(T)</td> 
                <?php 
                $total_otif = 0;
                $outstanding_week = 0; 
                foreach($data_select_otif as $select){
                      if($select->loss_tree == "Outstanding Week"){
                        $outstanding_week = $data_otif_loss_tree[$weekz][$select->loss_tree];
                      }
                    ?>
                        <td><?php echo $data_otif_loss_tree[$weekz][$select->loss_tree]; 
                        $total_otif = $total_otif+$data_otif_loss_tree[$weekz][$select->loss_tree]; ?></td>

                    <?php
                }
                  ?>
                  <td><?php echo $total_otif; ?></td>

              </tr>
              <tr>
                <td>% Cont</td>
                <?php 
                $total_otif = 0; 
                foreach ($data_select_otif as $select) {
                  ?>
                  <td>
                    <?php 
                      if( $data_otif_loss_tree[$weekz][$select->loss_tree] > 0){
                        $total_otif = $total_otif + ROUND(($data_otif_loss_tree[$weekz][$select->loss_tree]/$data_otif["total_week"][$weekz])*100);
                        echo ROUND(($data_otif_loss_tree[$weekz][$select->loss_tree]/$data_otif["total_week"][$weekz])*100)."%";
                      }else{
                        echo 0;
                      }
                    ?>
                  </td>
                  <?php 
                } ?>
                  <td><?php echo $total_otif."%";  ?></td>
              </tr>
            </tbody>
          </table>
    </div>

    CONFIRM DELIVERY DATE

    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover" >
        <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
          <tr>
            <th rowspan="2"> Data OTIF </th>
            <th colspan="3"><?php echo $date_listz[0]; ?></th>
            <th colspan="3"><?php echo $date_listz[1]; ?></th>
            <th colspan="3"><?php echo $date_listz[2]; ?></th>
            <th colspan="3"><?php echo $date_listz[3]; ?></th>
            <th colspan="3"><?php echo $date_listz[4]; ?></th>
            <th colspan="3"><?php echo $date_listz[5]; ?></th>
            <th colspan="3"><?php echo $date_listz[6]; ?></th>
            <th rowspan="2">Outstanding</th>
            <th rowspan="2">OTIF QTY (Shiftly)</th>
          </tr>
          <tr>
            <th>DM</th>
            <th>DP</th>
            <th>DS</th>
            <th>DM</th>
            <th>DP</th>
            <th>DS</th>
            <th>DM</th>
            <th>DP</th>
            <th>DS</th>
            <th>DM</th>
            <th>DP</th>
            <th>DS</th>
            <th>DM</th>
            <th>DP</th>
            <th>DS</th>
            <th>DM</th>
            <th>DP</th>
            <th>DS</th>
            <th>DM</th>
            <th>DP</th>
            <th>DS</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Plan Qty</td>
            <?php 
                $shift = array(
                    0 => "DM",
                    1 => "DP",
                    2 => "DS"
                );
                $total_dataz_plan = 0;
                foreach($date_listz as $date){ 
                    foreach($shift as $sft){
                    $total_dataz_plan = $total_dataz_plan + $data_otif[$date]["plan"][$sft];
                ?>
                <td><?php echo $data_otif[$date]["plan"][$sft]; ?></td>
            <?php 
                }
            } ?>
            <td><?php echo $data_otif["outstanding_week"][$weekz]; ?></td>
            <td><?php echo $data_otif["total_week"][$weekz]; ?></td>
          </tr>
        
        <tr>
            <td>Actual Qty</td>
            <?php 
                
                $total_dataz_actual = 0;
                foreach($date_listz as $date){ 
                    foreach($shift as $sft){
                    $total_dataz_actual = $total_dataz_actual + $data_otif[$date]["actual"][$sft];
                ?>
                <td><?php 
                
                echo $data_otif[$date]["actual"][$sft]; ?></td>
            <?php 
                }
            } ?>
            <td></td>
            <td><?php 
            $total_week_actual[$weekz] = $total_dataz_actual;
            echo $total_dataz_actual; ?></td>
          </tr>
          
          <tr>
            <td>%OTIF Shiftly</td>
            <?php 
                foreach($date_listz as $date){ 
                    foreach($shift as $sft){
                ?>
                <td><?php echo $data_otif[$date]["otif"][$sft]; ?></td>
            <?php 
                }
            } ?>
            <td></td>
            <td><?php 
            echo ROUND(($total_dataz_actual / $data_otif["total_week"][$weekz] )*100)."%";
            ?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <!-- end tabel Otif -->        
    </div>
    
            <?php
            $no++;
            }
        ?>
    <?php 
        }else{ 
            if(!is_array($week)){
                $weekz = $week; 
            }else{
                $weekz = $week[0];
            }
        ?>
        <div class="tab-pane <?php if($no ==0){echo "active";} ?>" id="otif_week-<?php echo $weekz; ?>">
            <!-- Tabel Otif -->
            <h4 class="card-title">OTIF LOSS TREE</h4>
              <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" >
                <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
                  <tr>
                    <th>LOSS TREE</th>
                    <?php foreach ($data_select_otif as $get) {
                      ?>
                      <th><?php echo $get->loss_tree; ?></th>

                    <?php
                    } ?>
                    <th>Total Loss Tree</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>QTY(T)</td>
                    <?php 
                        if(!is_array($week)){
                            $weekz = array(
                                0 => $week
                            );
                        }else{
                            $weekz = $week;
                        }
                    ?> 
                    <?php $total_otif = 0; 
                    $outstanding_week  = $data_otif_loss_tree[$weekz[0]]["Outstanding Week"];
                    foreach($data_select_otif as $select){
                        ?>
                            <td>
                             
                            <?php echo $data_otif_loss_tree[$weekz[0]][$select->loss_tree]; 
                            $total_otif = $total_otif+$data_otif_loss_tree[$weekz[0]][$select->loss_tree]; ?></td>
                        <?php
                    }
                      ?>
                      <td>
                        
                        <?php echo $total_otif; ?> </td>
                  </tr>
                  <tr>
                    <td>% Cont</td>
                    <?php 
                    $total_otif = 0; 
                    foreach ($data_select_otif as $select) {
                      ?>
                      <td><?php 
                      echo ($data_otif["total"] > 0)?round(($data_otif_loss_tree[$weekz[0]][$select->loss_tree]/$data_otif["total"])*100)."%":"0%";
                        ($data_otif["total"] > 0)?$total_otif = $total_otif + round(($data_otif_loss_tree[$weekz[0]][$select->loss_tree]/$data_otif["total"])*100):0;
                       ?></td>
                      <?php 
                    } ?>
                      <td><?php echo $total_otif."%";  ?></td>
                  </tr>
                </tbody>
              </table>
        </div>

        CONFIRM DELIVERY DATE
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" >
            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
              <tr>
                <th rowspan="2"> Data OTIF </th>
                <th colspan="3"><?php echo $date_list[0]; ?></th>
                <th colspan="3"><?php echo $date_list[1]; ?></th>
                <th colspan="3"><?php echo $date_list[2]; ?></th>
                <th colspan="3"><?php echo $date_list[3]; ?></th>
                <th colspan="3"><?php echo $date_list[4]; ?></th>
                <th colspan="3"><?php echo $date_list[5]; ?></th>
                <th colspan="3"><?php echo $date_list[6]; ?></th>
                <th rowspan="2">Outstanding</th>
                <th rowspan="2">OTIF QTY (Shiftly)</th>
              </tr>
              <tr>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
                <th>DM</th>
                <th>DP</th>
                <th>DS</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Plan Qty</td>
                <td><?php echo $data_otif[$date_list[0]]["plan"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[0]]["plan"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[0]]["plan"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["plan"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["plan"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["plan"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["plan"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["plan"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["plan"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["plan"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["plan"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["plan"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["plan"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["plan"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["plan"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["plan"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["plan"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["plan"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["plan"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["plan"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["plan"]["DS"]; ?></td>
                <td><?php echo $data_otif["outstanding"]; ?></td>
                <td><?php echo $data_otif["total"]; ?></td>
              </tr>
            
            <tr>
                <td>Actual Qty</td>
                <td><?php echo $data_otif[$date_list[0]]["actual"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[0]]["actual"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[0]]["actual"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["actual"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["actual"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["actual"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["actual"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["actual"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["actual"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["actual"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["actual"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["actual"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["actual"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["actual"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["actual"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["actual"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["actual"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["actual"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["actual"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["actual"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["actual"]["DS"]; ?></td>
                <td></td>
                <td><?php echo $data_otif["total_actual"]; ?></td>
              </tr>
              
              <tr>
                <td>%OTIF Shiftly</td>
                <td><?php echo $data_otif[$date_list[0]]["otif"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[0]]["otif"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[0]]["otif"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["otif"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["otif"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[1]]["otif"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["otif"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["otif"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[2]]["otif"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["otif"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["otif"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[3]]["otif"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["otif"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["otif"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[4]]["otif"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["otif"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["otif"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[5]]["otif"]["DS"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["otif"]["DM"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["otif"]["DP"]; ?></td>
                <td><?php echo $data_otif[$date_list[6]]["otif"]["DS"]; ?></td>
                <td></td>
                <td><?php echo $data_otif["total_otif"]; ?></td>
              </tr>
            </tbody>
          </table>
        </div>

<!-- END otif -->
        </div>
    <?php } ?>
    </div>

    <?php 
      if(count($week)>1){
        $total_average_otif = 0;
        $total_average_actual = 0;
        foreach($week as $weekz){
          $total_average_otif = $total_average_otif + $data_otif['total_week'][$weekz];
          $total_average_actual =  $total_average_actual + $total_week_actual[$weekz];
        }

        $total_average = ROUND(($total_average_actual / $total_average_otif)*100)."%";
      }else{
        $total_average = $data_otif['total_otif'];
      }
    ?>
</p>
</div>
</div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
var vendor = "<?php echo $data_vendor->vendor_alias; ?>";
var vendor_code = "<?php echo $this->input->get('vendor_code'); ?>";
var week = "<?php echo $this->input->get('week'); ?>";
var year = "<?php echo $this->input->get('year'); ?>";

var dom = document.getElementById("container2");
var myChart = echarts.init(dom);
var app = {};
var total_plan = [];
var total_actual = [];
var markline = [];

option = null;
option = {
    title: {

        text: '<?php echo (count($week) > 1)?"Weekly":"Daily"; ?> OTIF, Total : <?php echo $total_average; ?>',
        subtext: vendor
    },
    tooltip: {
        axisPointer : {           
            type : 'shadow'        
        },
        trigger: 'axis'
    },
    legend: {
        x : 'center',
        y : 'bottom',
        data: ['OTIF',<?php
            foreach($data_select_otif as $otif){
                echo "'".$otif->loss_tree."',";
            }
         ?>]
    },
    grid: {show: false},
    toolbox: {  
        show: true,
        feature: {
            magicType: {
                type: ['line', 'bar', 'pie', 'funnel'],
                title : {
                    line : 'Statistic',
                    bar : 'Bar',
                    stack : 'test',
                    tiled : 'test',
                    force: 'test',
                    chord: 'test',
                    pie: 'test',
                    funnel: 'test'
                },
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage: {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    xAxis:  {
        type: 'category',
        showGrid: false,
        boundaryGap: false,
        splitLine: {
         show: false
        },
        data: [0,
          <?php 
          if(count($week) > 1){
            foreach($week as $weekz){
                echo "'".$weekz."',";
            }
          }else{
            foreach($date_list as $date){
              echo "'".date('d-m-Y', strtotime($date) )."', ";
            }  
          }
           
          ?>
        ]
    },
    yAxis: {
        type: 'value',
        splitLine: {
         show: false
        },
        axisPointer: {
            snap: true
        },
        axisLabel: {
            formatter: '{value} %'
        }
    },
    series: [
        {
            name:'OTIF',
            type:'line',
            data:[
            0,
              <?php 
              $no = 1;
              $markline = "";
              if(count($week) > 1){
                
                $no_week = 0;
                
                foreach($week as $weekz){
                  $data_total = $total_date[$weekz];
                    $plan_week = 0;
                    $actual_week = 0;
                    $data_week = 0;
                    $otif_week = 0;
                    foreach($data_total as $dataz){
                        $plan_week = $plan_week + $dataz["plan"];
                        $actual_week = $actual_week + $dataz["actual"];
                        $otif_week = $otif_week + $dataz["otif"];
                    }
                    if($actual_week > 0 && $otif_week > 0){
                        $data_week = ABS(ROUND(($plan_week - ABS($actual_week - $plan_week))/$plan_week*100,1)) ;
                    }else{
                        $data_week = 0;
                    }
                    
                    echo "'".$data_week."',";
                    $markline = $markline."{name: 'OTIF', value: '".$data_week."', xAxis: ".$no.", yAxis: ".$data_week."},";
                    $no_week++;
                    $no++;
                }
              }else{
                foreach($date_list as $date){
                    echo "'".$total_date[$date]["total_otif"]."', ";
                    $markline = $markline."{name: 'OTIF', value: '".$total_date[$date]["total_otif"]."', xAxis: ".$no.", yAxis: ".$total_date[$date]["total_otif"]."},";
                    $no++;
                  } 
              }            
              ?>
            ],
            markPoint: {
                data: [<?php echo $markline; ?>]
            },
         },
         <?php foreach($data_select_otif as $select){ ?>
             {
                name: '<?php echo $select->loss_tree; ?>',
                type: 'bar',
                barGap: 0,
                barMaxWidth:50,

                stack: 'loss_tree',
                data: [
                    0,
                    <?php
                        $no = 1;
                        $markline = "{name: 'OTIF', value: 0, xAxis: 0, yAxis: 0},"; 
                        if(!is_array($week)){
                            $week = array(
                                0 => $week
                            );
                        }
                        if(count($week) > 1){
                          foreach($week as $weekz){
                            if($data_otif["total_week"][$weekz] > 0){
                              $data_otif_loss = ROUND(($data_otif_loss_tree[$weekz][$select->loss_tree]/ $data_otif["total_week"][$weekz])*100);
                              echo "'".$data_otif_loss."',";
                            }else{
                              echo "'0',";
                            }
                          }
                        }else{
                          foreach($date_list as $date){
                            if($data_otif_loss_tree[$week[0]][$date][$select->loss_tree] > 0 && $total_date[$date]["total_plan"] > 0){
                              $data_otif_total = round((($data_otif_loss_tree[$week[0]][$date][$select->loss_tree]) / $total_date[$date]["total_plan"])*100);
                              echo "'".$data_otif_total."',";
                            }else{
                              echo "'0',";
                            }
                             
                              
                          } 
                        }
                        
                    ?>                
                ],
                
            },
        <?php } ?>
      
    ]
};

if (option && typeof option === "object") {
    myChart.setOption(option, true);
}

});

//CHART
var dom = document.getElementById("container3");
var myChart = echarts.init(dom);
var app = {};
option = null;
var posList = [
    'left', 'right', 'top', 'bottom',
    'inside',
    'insideTop', 'insideLeft', 'insideRight', 'insideBottom',
    'insideTopLeft', 'insideTopRight', 'insideBottomLeft', 'insideBottomRight'
];

app.configParameters = {
    rotate: {
        min: -90,
        max: 90
    },
    align: {
        options: {
            left: 'left',
            center: 'center',
            right: 'right'
        }
    },
    verticalAlign: {
        options: {
            top: 'top',
            middle: 'middle',
            bottom: 'bottom'
        }
    },
    position: {
        options: echarts.util.reduce(posList, function (map, pos) {
            map[pos] = pos;
            return map;
        }, {})
    },
    distance: {
        min: 0,
        max: 100
    }
};

app.config = {
    rotate: 90,
    align: 'left',
    verticalAlign: 'middle',
    position: 'insideBottom',
    distance: 15,
    onChange: function () {
        var labelOption = {
            normal: {
                rotate: app.config.rotate,
                align: app.config.align,
                verticalAlign: app.config.verticalAlign,
                position: app.config.position,
                distance: app.config.distance
            }
        };
        myChart.setOption({
            series: [{
                label: labelOption
            }, {
                label: labelOption
            }, {
                label: labelOption
            }]
        });
    }
};


var labelOption = {
    normal: {
        show: true,
        position: app.config.position,
        distance: app.config.distance,
        align: app.config.align,
        verticalAlign: app.config.verticalAlign,
        rotate: app.config.rotate,
        formatter: '{c}%  {name|{a}}',
        fontSize: 16,
        rich: {
            name: {
                textBorderColor: '#fff'
            }
        }
    }
};

option = {
    color: ['#003366', '#4cabce', '#e5323e'],
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    title: {
        text: 'Shift <?php echo (count($week) > 1 )?"Weekly":"Daily"; ?> OTIF',
        subtext: vendor
    },
    legend: {
        data: ['DM', 'DP', 'DS']
    },
    grid: {show: false},
    toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
        feature: {
            
            magicType: {
                show: true, type: ['line', 'bar'],
                title : {
                    line : 'Statistic',
                    bar : 'Bar',
                   
                },
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage : {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    calculable: true,
    xAxis: [
        {
            showGrid: false,
            type: 'category',
            axisTick: {show: false},
            data: [
              <?php 
                if(count($week) > 1){
                    foreach($week as $weekz){
                        echo "'".$weekz."',";
                    }
                }else{
                    foreach($date_list as $date){
                        echo "'".date('d-m-Y', strtotime($date) )."', ";
                    }
                }
               ?>
            ]
        }
    ],
    yAxis: [
        {
            splitLine: {
                 show: false
            },
            type: 'value'
        }
    ],
    series: [
        {
            name: 'DM',
            type: 'bar',
            barGap: 0,
            label: labelOption,
            data: [
                <?php
                if(count($week) > 1){
                    foreach($week as $weekz){
                        $total_otif_DM = 0;
                        $total_otif_DP = 0;
                        $total_otif_DS = 0;
                        $array_otif_DM[$weekz] = 0;
                        $array_otif_DP[$weekz] = 0;
                        $array_otif_DS[$weekz] = 0;
                        $counter_dm = 0;
                        $counter_dp = 0;
                        $counter_ds = 0;
                        foreach($data_otif_weekly[$weekz] as $get => $dow){
                            if($get == "total"){

                            }else{
                              $total_otif_DM          = $total_otif_DM + str_replace('%','',$dow["otif"]["DM"]);
                              $total_otif_DP          = $total_otif_DP + str_replace('%','',$dow["otif"]["DP"]);
                              $total_otif_DS          = $total_otif_DS + str_replace('%','',$dow["otif"]["DS"]);
                              if(str_replace('%','',$dow["otif"]["DM"]) > 0){
                                $counter_dm++;
                              }
                              if(str_replace('%','',$dow["otif"]["DP"]) > 0){
                                $counter_dp++;
                              }
                              if(str_replace('%','',$dow["otif"]["DS"]) > 0){
                                $counter_ds++;
                              }
                            }
                            
                        }
                        if($counter_dm > 0){
                          $total_otif_DM = ROUND($total_otif_DM / $counter_dm,1);
                          $array_otif_DM[$weekz] = ROUND($total_otif_DM,1);
                        }
                        if($counter_dp > 0){
                          $total_otif_DP = $total_otif_DP / $counter_dp;
                          $array_otif_DP[$weekz] = ROUND($total_otif_DP,1);
                        }
                        if($counter_ds > 0){
                          $total_otif_DS = $total_otif_DS / $counter_ds;
                          $array_otif_DS[$weekz] = ROUND($total_otif_DS,1);
                        }
                        
                        echo "'".$total_otif_DM."',";    
                    }
                }else{
                    foreach($date_list as $date){
                        echo "'".str_replace("%","",$data_otif[$date]['otif']['DM'])."', ";
                    }
                } 
                 
                ?>                
            ]
        },
        {
            name: 'DP',
            type: 'bar',
            label: labelOption,
            data: [
               <?php 
               if(count($week) > 1){
                    foreach($week as $weekz){
                          echo "'".$array_otif_DP[$weekz]."',";
                    }
                    
               }else{
                    foreach($date_list as $date){
                        echo "'".str_replace("%","",$data_otif[$date]['otif']['DP'])."', ";
                    } 
               }
               ?>
            ]
        },
        {
            name: 'DS',
            type: 'bar',
            label: labelOption,
            data: [
                <?php 
               if(count($week) > 1){
                    foreach($week as $weekz){
                        echo "'".$array_otif_DS[$weekz]."',";
                    }
                    
               }else{
                    foreach($date_list as $date){
                        echo "'".str_replace("%","",$data_otif[$date]['otif']['DS'])."', ";
                    } 
               }
               ?>
            ]
        }
    ]
};;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}

//OTIF LOSS TREE
var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
option = null;
option = {
    title: {
        text: 'OTIF Loss Tree %',
        subtext: vendor,
    },
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        },
        formatter: function (params) {
            var tar;
            if (params[1].value != '-') {
                tar = params[1];
            }
            else {
                tar = params[0];
            }
            return tar.name + '<br/>' + tar.seriesName + ' : ' + tar.value;
        }
    },
    legend: {
        data:['OTIF',<?php
            foreach($data_select_otif as $otif){
                echo "'".$otif->loss_tree."',";
            }
         ?>]
    },
    grid: {
        show:false,
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type : 'category',
        splitLine: {show:false},
        axisLabel : {
            interval:0,
            rotate:10
            },
        data : [
            <?php 
                $total_actual = 0;
                $total_plan = 0;
                $total_otif = 0;
                $otif_actual = "0,";
                $partial = 0;
                $full_load  = 0;
                $delay_1 = 0;
                $delay_2 = 0;
                $delay_3 = 0;
                $outstanding = 0;
                $cancel = 0;
                $reschedule = 0;
                
                $total_plan = $data_otif["total"];
                $total_actual = $data_otif["total_actual"];
                if($total_plan > 0){
                    if(count($week) > 1){
                      $total_otif = ROUND(($total_actual / $total_plan)*100,1);
                      foreach($week as $weekz){
                        $partial = $partial + $data_otif_loss_tree[$weekz]["Partial Delivery"];
                        $full_load = $full_load + $data_otif_loss_tree[$weekz]["Full Truck Load Optimization"];
                        $delay_1 = $delay_1 + $data_otif_loss_tree[$weekz]["Delay Delivery 1 Shift"];
                        $delay_2 = $delay_2 + $data_otif_loss_tree[$weekz]["Delay Delivery 2 Shift"];
                        $delay_3 = $delay_3 + $data_otif_loss_tree[$weekz]["Delay Delivery > 3 Shift"];
                        $outstanding = $outstanding + $data_otif_loss_tree[$weekz]["Outstanding Week"];
                        $cancel = $data_otif_loss_tree[$weekz]["Cancel Plan Mutual Agrement"];
                        $reschedule = $data_otif_loss_tree[$weekz]["Reschedule / Revise Plan"];
                        
                      }
                      $partial = ROUND(($partial / $total_plan) * 100);
                      $full_load = ROUND(($full_load / $total_plan) * 100);
                      $delay_1 = ROUND(($delay_1 / $total_plan) * 100);
                      $delay_2 = ROUND(($delay_2 / $total_plan) * 100);
                      $delay_3 = ROUND(($delay_3 / $total_plan) * 100);
                      $outstanding = ROUND(($outstanding / $total_plan) * 100);
                      $cancel = ROUND(($cancel / $total_plan) * 100);
                      $reschedule = ROUND(($reschedule / $total_plan) * 100);
                    }else{
                      if(is_array($week)){
                          $weekz = $week[0];
                      }
                      $total_otif = ROUND(($total_actual / $total_plan)*100,1);
                      $partial = ROUND(($data_otif_loss_tree[$weekz]["Partial Delivery"]/$total_plan)*100,1);
                      $full_load = ROUND(($data_otif_loss_tree[$weekz]["Full Truck Load Optimization"]/$total_plan)*100,1);
                      $delay_1 = ROUND(($data_otif_loss_tree[$weekz]["Delay Delivery 1 Shift"]/$total_plan)*100,1);
                      $delay_2 = ROUND(($data_otif_loss_tree[$weekz]["Delay Delivery 2 Shift"]/$total_plan)*100,1);
                      $delay_3 = ROUND(($data_otif_loss_tree[$weekz]["Delay Delivery > 3 Shift"]/$total_plan)*100,1);
                      $outstanding = ROUND(($data_otif_loss_tree[$weekz]["Outstanding Week"]/$total_plan)*100,1);
                      $cancel = ROUND(($data_otif_loss_tree[$weekz]["Cancel Plan Mutual Agrement"]/$total_plan)*100,1);
                      $reschedule = ROUND(($data_otif_loss_tree[$weekz]["Reschedule / Revise Plan"]/$total_plan)*100,1);
                      
                    }
                    
                    $array_select_otif = array(
                        "Partial Delivery"              => $partial,
                        "Full Truck Load Optimization"  => $full_load,
                        "Delay Delivery 1 Shift"        => $delay_1,
                        "Delay Delivey 2 Shift"         => $delay_2,
                        "Delay Delivery > 3 Shift"      => $delay_3,
                        "Outstanding Week"              => $outstanding,
                        "Cancel Plan Mutual Agrement"   => $cancel,
                        "Reschedule / Revise Plan"      => $reschedule,
                    );
                    arsort($array_select_otif);
                    
                }
                
            ?>
            'OTIF',<?php
            if($total_plan > 0){
                foreach($array_select_otif as $array => $value){
                    echo "'".$array."',";
                }
            }else{
                foreach($data_select_otif as $otif){
                    echo "'".$otif->loss_tree."',";
                }
            }
         ?>
        ]
    },
    yAxis: {
        splitLine: {
         show: false
      },
      axisLabel: {
            formatter: '{value} %'
        },
        type : 'value'
    },
    series: [
        {
            name: 'Otif',
            type: 'bar',
            stack: 'otif',
            itemStyle: {
                normal: {
                    barBorderColor: 'rgba(0,0,0,0)',
                    color: 'rgba(0,0,0,0)'
                },
                emphasis: {
                    barBorderColor: 'rgba(0,0,0,0)',
                    color: 'rgba(0,0,0,0)'
                }
            },
            
            data: [0, <?php echo $total_otif; ?>,
                        <?php
                            $total_data = $total_otif;
                            if($total_plan > 0){
                                foreach($array_select_otif as $array => $value){
                                    $total_data = $total_data + $value;
                                    echo "'".$total_data."',";
                                }
                            }else{
                                foreach($data_select_otif as $otif){
                                    echo "0,";
                                }
                            }
                        ?> 
            ]
        },
        {
            name: 'OTIF',
            type: 'bar',
            stack: 'otif',
            label: {
                normal: {
                    show: true,
                    position: 'top'
                }
            },
            data: [<?php echo $total_otif; ?>, 
                <?php
                if($total_plan > 0){
                    foreach($array_select_otif as $array => $value){
                        echo "'".$value."',";
                    }
                }else{
                    foreach($data_select_otif as $otif){
                        echo "0,";
                    }
                }
            ?>
            ]
        },
        
    ]
};

if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
</script>
