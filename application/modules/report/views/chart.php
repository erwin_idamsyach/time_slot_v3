
<div class="col-12">
  <div class="card">
    <div class="card-header card-header-icon card-header-info">
      <div class="card-icon">
        <i class="material-icons">timeline</i>
      </div>
      <h4 class="card-title">CONFIRM DELIVERY DATE
        <small> - STATISTIC</small>
      </h4>
    </div>
    <div class="card-body" style="height: 400px">
      <div id="container2" style="height: 100%"></div>
    </div>
    <div class="card-body" style="height: 400px">
      <div id="container3" style="height: 100%"></div>
    </div>
  </div>
</div>
<div class="col-12">
  <div class="card card-chart">
    <div class="card-header card-header-icon card-header-danger">
      <div class="card-icon">
        <i class="material-icons">pie_chart</i>
      </div>
      <h4 class="card-title">OTIF LOSS TREE</h4>
    </div>
    <div class="card-body" style="height: 400px">
      <div id="container" style="height: 100%"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
$( document ).ready(function() {
var vendor = "<?php echo $data_vendor->vendor_alias; ?>";
var vendor_code = "<?php echo $this->input->get('vendor_code'); ?>";
var week = "<?php echo $this->input->get('week'); ?>";
var year = "<?php echo $this->input->get('year'); ?>";
var dom = document.getElementById("container");
var myChart = echarts.init(dom);
var app = {};
option = null;
option = {
    title : {
        text: vendor+' WEEK '+week,
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        x : 'center',
        y : 'bottom',
        data:[
          <?php foreach ($data_select_otif as $get) {
              echo "'".$get->loss_tree."',";
          } ?>
        ]
    },
    toolbox: {
        show : true,
        feature : {
            magicType : {
                show: true,
                type: ['pie', 'funnel'],
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage : {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    calculable : true,
    series : [
        {
            name:'Chart '+vendor+' week '+week,
            type:'pie',
            radius : [20, 100],
            center : ['50%', '50%'],
            roseType : 'area',
            data:[
                <?php foreach ($data_select_otif as $get) {
                    ?>
                    {value:<?php echo $data_otif_loss_tree[$get->loss_tree]; ?>, name:'<?php echo $get->loss_tree; ?>'},
                <?php
                } ?>
            ]
        }
    ]
};
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}

var dom = document.getElementById("container2");
var myChart = echarts.init(dom);
var app = {};
var total_plan = [];
var total_actual = [];
var markline = [];
$.ajax({
  url:"<?php base_url(); ?>report/chart_confirm_data",
  method: "GET",
  data: {
    "vendor_code" : "<?php echo $data_vendor->vendor_code; ?>",
    "week"        : week,
    "year"        : year,
    "link"        : "otif"
  },
  dataType: "JSON",
  success:function(e){
   
    for (var i =  0; i < 7; i++) {
        markline[i] = {name: 'Otif', value: e[i].otif+"%", xAxis: i, yAxis: e[i].otif 
                      };
        total_plan[i] = e[i].otif;
        //total_actual[i] = e[i].actual; 
    }

    // for (var i =  0; i < 7; i++) {
    //     total_actual[i] = e[i].Actual; 
    // }
      
    console.log(total_plan);
  },error:function(e){
    alert("Something Error");
  },async:false
});

option = null;
option = {
    title: {
        text: 'Daily Otif, Total OTIF : <?php echo $data_otif['total_otif']; ?>',
        subtext: vendor+" WEEK "+week
    },
    tooltip: {
        trigger: 'axis',
        formatter: "{a} <br/>{b} : {c}%"
    },
    legend: {
        data:['OTIF','OTIF']
    },
    toolbox: {  
        show: true,
        feature: {
            magicType: {
                type: ['line', 'bar', 'pie', 'funnel'],
                title : {
                    line : 'Statistic',
                    bar : 'Bar',
                    stack : 'test',
                    tiled : 'test',
                    force: 'test',
                    chord: 'test',
                    pie: 'test',
                    funnel: 'test'
                },
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage: {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    xAxis:  {
        type: 'category',
        boundaryGap: false,
        data: [<?php 
          for($i = 0; $i < 7 ; $i ++){
          ?>
              '<?php echo $date_list[$i]; ?>',
          <?php
        } ?>]
    },
    yAxis: {
        type: 'value',
        axisLabel: {
            formatter: '{value}'
        }
    },
    series: [
        {
            name:'Otif Shiftly',
            type:'line',
            data:total_plan,
            markPoint: {
                data: markline
            },
            markLine: {
                data: [
                    {type: 'average', name: 'Average'}
                ]
            }
         }
        // ,{
        //     name:'Actual Qty',
        //     type:'line',
        //     data:total_actual,
        //     markPoint: {
        //         data: [
        //             {name: 'Actual', value: -2, xAxis: 1, yAxis: -1.5}
        //         ]
        //     },
        //     // markLine: {
        //     //     data: [
        //     //         {type: 'average', name: 'OTIFZz'}
                    
        //     //     ]
        //     // }
        // }
    ]
};

if (option && typeof option === "object") {
    myChart.setOption(option, true);
}

});


</script>

<script type="text/javascript">
var dom = document.getElementById("container3");
var myChart = echarts.init(dom);
var app = {};
option = null;
var posList = [
    'left', 'right', 'top', 'bottom',
    'inside',
    'insideTop', 'insideLeft', 'insideRight', 'insideBottom',
    'insideTopLeft', 'insideTopRight', 'insideBottomLeft', 'insideBottomRight'
];

app.configParameters = {
    rotate: {
        min: -90,
        max: 90
    },
    align: {
        options: {
            left: 'left',
            center: 'center',
            right: 'right'
        }
    },
    verticalAlign: {
        options: {
            top: 'top',
            middle: 'middle',
            bottom: 'bottom'
        }
    },
    position: {
        options: echarts.util.reduce(posList, function (map, pos) {
            map[pos] = pos;
            return map;
        }, {})
    },
    distance: {
        min: 0,
        max: 100
    }
};

app.config = {
    rotate: 90,
    align: 'left',
    verticalAlign: 'middle',
    position: 'insideBottom',
    distance: 15,
    onChange: function () {
        var labelOption = {
            normal: {
                rotate: app.config.rotate,
                align: app.config.align,
                verticalAlign: app.config.verticalAlign,
                position: app.config.position,
                distance: app.config.distance
            }
        };
        myChart.setOption({
            series: [{
                label: labelOption
            }, {
                label: labelOption
            }, {
                label: labelOption
            }]
        });
    }
};


var labelOption = {
    normal: {
        show: true,
        position: app.config.position,
        distance: app.config.distance,
        align: app.config.align,
        verticalAlign: app.config.verticalAlign,
        rotate: app.config.rotate,
        formatter: '{c}%  {name|{a}}',
        fontSize: 16,
        rich: {
            name: {
                textBorderColor: '#fff'
            }
        }
    }
};

option = {
    color: ['#003366', '#4cabce', '#e5323e'],
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    title: {
        text: 'Shiftly Actual Otif',
        subtext: vendor+" WEEK "+week
    },
    legend: {
        data: ['DM', 'DP', 'DS']
    },
    toolbox: {
        show: true,
        orient: 'vertical',
        left: 'right',
        top: 'center',
        feature: {
            
            magicType: {
                show: true, type: ['line', 'bar'],
                title : {
                    line : 'Statistic',
                    bar : 'Bar',
                   
                },
                option : {}
            },
            restore: {
                show : true,
                title : 'Refresh'
            },
            saveAsImage : {
                show: true,
                title: 'Save Image',
                option : {}
            }
        }
    },
    calculable: true,
    xAxis: [
        {
            type: 'category',
            axisTick: {show: false},
            data: [<?php 
                  for($i = 0; $i < 7 ; $i ++){
                  ?>
                      '<?php echo $date_list[$i]; ?>',
                  <?php
                } ?>
            ]
        }
    ],
    yAxis: [
        {
            type: 'value'
        }
    ],
    series: [
        {
            name: 'DM',
            type: 'bar',
            barGap: 0,
            label: labelOption,
            data: [
                <?php 
                  for($i = 0; $i < 7 ; $i ++){
                  ?>
                      '<?php echo str_replace("%","",$data_otif[$date_list[$i]]['otif']['DM']); ?>',
                  <?php
                } ?>
            ]
        },
        {
            name: 'DP',
            type: 'bar',
            label: labelOption,
            data: [
                <?php 
                  for($i = 0; $i < 7 ; $i ++){
                  ?>
                      '<?php echo str_replace("%","",$data_otif[$date_list[$i]]['otif']['DP']); ?>',
                  <?php
                } ?>
            ]
        },
        {
            name: 'DS',
            type: 'bar',
            label: labelOption,
            data: [
                <?php 
                  for($i = 0; $i < 7 ; $i ++){
                  ?>
                      '<?php echo str_replace("%","",$data_otif[$date_list[$i]]['otif']['DS']); ?>',
                  <?php
                } ?>
            ]
        }
    ]
};;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
       </script>