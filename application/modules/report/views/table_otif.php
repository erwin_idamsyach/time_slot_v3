<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
	  <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
	    <tr>
	      <th scope="col">No</th>
	      <th scope="col">Code</th>
	      <th scope="col">Level</th>
	      <th scope="col">Loss Tree</th>
	      <th scope="col">Example</th>
	      <!-- <th scope="col">Delete</th> -->
	    </tr>
	  </thead>
	  <tbody>
	    <?php 
	    $no = 1;
	    foreach ($data_otif as $get) {
	    	?>
			<tr>
		      <td scope="row" style="text-align:center"><?php echo $no++; ?></td>
		      <td><?php echo $get->code; ?></td>
		      <td><?php echo $get->level; ?></td>
		      <td><?php echo $get->loss_tree; ?></td>
		      <td><?php echo $get->example; ?></td>
		      <!-- <td style="text-align:center">
		      	<button class="btn btn-danger btn-sm" onclick="delete_otif('<?php echo $get->id; ?>')">
		      		<i class="fas fa-trash"> </i> Delete
		      	</button>
		      </td> -->
		    </tr>
	    	<?php
	    } ?>
	  </tbody>
	</table>
</div>