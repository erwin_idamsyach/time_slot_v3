<div class="loading">
</div>
<?php
$total_average = 0;
$count_average = 0;
foreach ($date_list as $date) {
  $total_average = $total_average + $data_average[$date];
  if ($data_average[$date] > 0) {
    $count_average++;
  }
}
if ($total_average > 0) {
  $total_average = $total_average / $count_average;
  $m = ROUND($total_average % 60);
  $h = floor($total_average / 60);
  $d = "";
  if ($m < 10) {
    $m = "0" . $m;
  }
  if ($h > 24) {
    $d = floor($h / 24) . " d";
    $h = floor($h % 24);
  }
  if ($h < 10) {
    $h = "0" . $h;
  }
  $time_average = $d . " " . $h . ":" . $m;
} else {
  $time_average = "00:00";
}


?>
<div class="row">
  <div class="col-3">
    <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
      <div class="card-header card-header-warning card-header-icon">
        <div class="card-icon">
          <i class="fas fa-truck"></i>
        </div>
        <p class="card-category" style="font-size:14px;font-weight:800;color:black">TOTAL</p>
        <h4 class="card-title"><b><?php echo

                                    count($data_list); ?>

          </b></h4>
      </div>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
      <div class="card-header card-header-rose card-header-icon">
        <div class="card-icon">
          <i class="fas fa-truck"></i>
        </div>
        <p class="card-category" style="font-size:14px;font-weight:800;color:black">Early :</p>
        <?php

        if (count($data_list) > 0) {
          $early = round(($data_list[count($data_list) - 1]->early / count($data_list)) * 100);
        } else {
          $early = 0;
        } ?>
        <h4 class="card-title"><b><?php echo (count($data_list) > 0) ? $data_list[count($data_list) - 1]->early : 0; ?></b></h4>
      </div>
      <div class="card-footer">
        <b><?php echo $early . "%"; ?></b>
      </div>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="fas fa-truck"></i>
        </div>
        <p class="card-category" style="font-size:14px;font-weight:800;color:black">On Time :</p>
        <?php

        if (count($data_list) > 0) {
          $on_time = round(($data_list[count($data_list) - 1]->good / count($data_list)) * 100);
        } else {
          $on_time = 0;
        } ?>
        <h4 class="card-title"><b><?php echo (count($data_list) > 0) ? $data_list[count($data_list) - 1]->good : 0; ?></b></h4>
      </div>
      <div class="card-footer">
        <b><?php echo $on_time . "%"; ?></b>
      </div>
    </div>
  </div>

  <div class="col-lg-3">
    <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
      <div class="card-header card-header-primary card-header-icon">
        <div class="card-icon">
          <i class="fas fa-truck"></i>
        </div>
        <p class="card-category" style="font-size:14px;font-weight:800;color:black">Delay :</p>
        <?php

        if (count($data_list) > 0) {
          $delay = round(($data_list[count($data_list) - 1]->delay / count($data_list)) * 100);
        } else {
          $delay = 0;
        } ?>
        <h4 class="card-title"><b><?php echo (count($data_list) > 0) ? $data_list[count($data_list) - 1]->delay : 0; ?></b></h4>
      </div>
      <div class="card-footer">
        <b> <?php echo $delay . "%"; ?></b>
      </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-header-icon card-header-info">
        <div class="card-icon">
          <i class="fas fa-chart-line" style="font-size:35px"></i>
        </div>
        <h4 class="card-title">ONTIME Performance, AVERAGE : <?php $time_average; ?>


        </h4>
      </div>
      <div class="card-body">
        <div id="on_time_chart" style="height: 500px"></div>
      </div>

    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-header-icon card-header-success">
        <div class="card-icon">
          <i class="fas fa-chart-line" style="font-size:35px"></i>
        </div>
        <h4 class="card-title">Truck On Time Status

        </h4>
      </div>
      <div class="card-body">
        <div id="truck_on_time_chart" style="height: 500px"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-header card-header-success card-header-icon">
        <div class="card-icon">
          <i class="fas fa-chart-bar" style="font-size:30px"></i>
        </div>
        <h4 class="card-title">REPORT SLOT ON TIME</h4>
      </div>
      <div class="card-body">

        <ul class="nav nav-pills nav-pills-icons" role="tablist">
          <?php $no = 0;
          if (count($week) > 1) {
            foreach ($week as $weekz) {
          ?>
              <li class="nav-item">
                <a class="nav-link <?php if ($no == 0) {
                                      echo "active";
                                    } ?>" href="#week-<?php echo $weekz; ?>" role="tab" data-toggle="tab">
                  W<?php echo $weekz;
                    $no++ ?>
                </a>
              </li>
            <?php
            }
          } else {
            if (!is_array($week)) {
              $week = array(
                0 => $week
              );
            }
            ?>
            <li class="nav-item">
              <a class="nav-link active" href="#week-<?php echo $week[0]; ?>" role="tab" data-toggle="tab">
                W<?php echo $week[0]; ?>
              </a>
            </li>
          <?php } ?>
        </ul>

        <div class="tab-content tab-space">
          <?php $number = 0;
          $no_data = 0;
          $no_week = 0;
          if (count($week) > 1) {
            foreach ($week as $weekz) {
              $getDateTime = new DateTime();
              $date_listz[0] = $getDateTime->setISODate($year, $weekz)->format('Y-m-d');
              $i = 1;
              while ($i < 7) {
                $date_listz[$i] = $getDateTime->modify('+1 days')->format('Y-m-d');
                $i++;
              }
          ?>
              <div class="tab-pane <?php if ($number == 0) {
                                      echo "active";
                                    } ?>" id="week-<?php echo $weekz; ?>">
                <a href="<?php echo base_url() ?>report/download_on_time?year=<?php echo $year; ?>&week=<?php echo $weekz; ?>&vendor_code=<?php echo $vendor_code; ?>"><button class="btn btn-primary"><i class="fas fa-download"></i> Download W<?php echo $weekz; ?></button></a>

                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover data-table tables" width="100%">
                    <thead style="color:white; background-color:#31559F;font-weight: 650;">
                      <tr>
                        <th style="font-size:10px">No</th>
                        <th style="font-size:10px">Receipt</th>
                        <th style="font-size:10px">No Truck</th>
                        <th style="font-size:10px">Rdd</th>
                        <th style="font-size:10px">Time Slot</th>
                        <th style="font-size:10px">Arrived</th>
                        <th style="font-size:10px">Received</th>
                        <th style="font-size:10px">SLOT HIT</th>
                        <th style="font-size:10px">Time Comparison</th>

                      </tr>

                    </thead>
                    <tbody>
                      <?php $no = 0;
                      foreach ($data_list_week[$weekz] as $get) { ?>
                        <tr>
                          <td><?php echo $no + 1; ?></td>
                          <td><?php echo $get->receipt; ?></td>
                          <td><?php echo $get->police_number; ?></td>
                          <td><?php echo $get->rdd; ?></td>
                          <td><?php echo $get->start_time . ' - ' . $get->end_time; ?></td>
                          <td><?php echo $get->arrive_date . ' - ' . $get->arrive_time; ?></td>
                          <td><?php echo $get->receive_date . ' - ' . $get->receive_time; ?></td>
                          <td><?php echo $get->status; ?></td>
                          <?php
                          $start_date = new DateTime($get->rdd . ' ' . $get->end_time);
                          $since_start = $start_date->diff(new DateTime($get->arrive_date . ' ' . $get->arrive_time));
                          if ($since_start->days == 0) {
                            $days = '';
                          } else {
                            $days = $since_start->days . 'd';
                          }

                          if ($since_start->h < 10) {
                            $h = '0' . $since_start->h;
                          } else {
                            $h = $since_start->h;
                          }

                          if ($since_start->i < 10) {
                            $i = '0' . $since_start->i;
                          } else {
                            $i = $since_start->i;
                          }
                          ?>
                          <td <?php
                              if ($get->status == 'Early') {
                                echo "style='background-color:orange'";
                              } else if ($get->status == 'Delay') {
                                echo "style='background-color:red'";
                              } else {
                                echo "style='background-color:green'";
                              }
                              ?>><?php

                                  echo $days . ' ' . $h . ':' . $i;
                                  ?></td>
                        </tr>
                      <?php
                        $no_data++;
                        $no++;
                      } ?>


                    </tbody>
                  </table>
                </div>

              </div>
            <?php
              $number++;
            }
          } else {
            if (!is_array($week)) {
              $week = array(
                0 => $week
              );
            }
            ?>
            <div class="tab-pane <?php if ($no == 0) {
                                    echo "active";
                                  } ?>" id="week-<?php echo $week[0]; ?>">
              <a href="<?php echo base_url() ?>report/download_on_time?year=<?php echo $year; ?>&week=<?php echo $week[0]; ?>&vendor_code=<?php echo $vendor_code; ?>"><button class="btn btn-primary"><i class="fas fa-download"></i> Download W<?php echo $week[0];
                                                                                                                                                                                                                                                  $no++; ?></button></a>
              <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover data-table tables" id='tabel' width="100%">
                  <thead style="color:white; background-color:#31559F;font-weight: 650;">
                    <tr>
                      <th style="font-size:10px">No</th>
                      <th style="font-size:10px">Receipt</th>
                      <th style="font-size:10px">No Truck</th>
                      <th style="font-size:10px">Rdd</th>
                      <th style="font-size:10px">Time Slot</th>
                      <th style="font-size:10px">Arrived</th>
                      <th style="font-size:10px">Received</th>
                      <th style="font-size:10px">SLOT HIT/MISS</th>
                      <th style="font-size:10px">Time Comparison</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 0;
                    foreach ($data_list as $get) { ?>
                      <tr>
                        <td><?php echo $no + 1; ?></td>
                        <td><?php echo $get->receipt; ?></td>
                        <td><?php echo $get->police_number; ?></td>
                        <td><?php echo $get->rdd; ?></td>
                        <td><?php echo $get->start_time . ' - ' . $get->end_time; ?></td>
                        <td><?php echo $get->arrive_date . ' - ' . $get->arrive_time; ?></td>
                        <td><?php echo $get->receive_date . ' - ' . $get->receive_time; ?></td>
                        <td <?php
                            if ($get->status == 'Early') {
                              echo "style='background-color:orange'";
                            } else if ($get->status == 'Delay') {
                              echo "style='background-color:red'";
                            } else {
                              echo "style='background-color:green'";
                            }
                            ?>><?php echo $get->status; ?></td>
                        <?php
                        $start_date = new DateTime($get->rdd . ' ' . $get->end_time);
                        $since_start = $start_date->diff(new DateTime($get->arrive_date . ' ' . $get->arrive_time));
                        if ($since_start->days == 0) {
                          $days = '';
                        } else {
                          $days = $since_start->days . 'd';
                        }

                        if ($since_start->h < 10) {
                          $h = '0' . $since_start->h;
                        } else {
                          $h = $since_start->h;
                        }

                        if ($since_start->i < 10) {
                          $i = '0' . $since_start->i;
                        } else {
                          $i = $since_start->i;
                        }
                        ?>
                        <td><?php

                            echo $days . ' ' . $h . ':' . $i;
                            ?></td>

                      </tr>
                    <?php $no++;
                    } ?>

                  </tbody>
                </table>
              </div>
            </div>
          <?php } ?>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="row" style="height: 500px">
  <div class="col">

  </div>
</div>
<style>
  .chart {
    width: 100%;
    min-height: 100%;
  }
</style>

<?php



if (!is_array($week)) {
  $week = array(
    0 => $week
  );
}
?>

<script type="text/javascript">
  $("#table").DataTable({
    "scrollX": true,
    ordering: false,
    responsive: true,
    "dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
  });
  var dom = document.getElementById("on_time_chart");
  var myChart = echarts.init(dom);
  var app = {};
  option = null;
  option = {
    title: {
      text: 'Week <?php if (count($week) > 1) {
                    echo $week[0] . "-" . $week[count($week) - 1];
                  } else {
                    echo $week[0];
                  } ?>',
      subtext: '<?php echo $data_vendor->vendor_name; ?>'
    },
    tooltip: {
      trigger: 'axis',
      formatter: "{a} <br/>{b} : {c} Minutes"
    },
    toolbox: {
      show: true,
      feature: {
        magicType: {
          type: ['line', 'bar', 'pie', 'funnel'],
          title: {
            line: 'Statistic',
            bar: 'Bar',
            stack: 'stack',
            tiled: 'tiled',
            force: 'force',
            chord: 'chord',
            pie: 'pie',
            funnel: 'funnel'
          },
          option: {}
        },
        restore: {
          show: true,
          title: 'Refresh'
        },
        saveAsImage: {
          show: true,
          title: 'Save Image',
          option: {}
        }
      }
    },
    grid: {
      show: false
    },
    xAxis: {
      type: 'category',
      boundaryGap: false,
      showGrid: false,
      splitLine: {
        show: false
      },
      data: [0, <?php
                if (count($week) > 1) {
                  foreach ($week as $weekz) {
                    echo "'Week " . $weekz . "',";
                  }
                } else {
                  foreach ($date_list as $date) {
                    echo "'" . date('d-m-Y', strtotime($date)) . "', ";
                  }
                }

                ?>]
    },
    yAxis: {
      type: 'value',
      splitLine: {
        show: false
      },
      axisLabel: {
        formatter: (function(value) {

          var new_value = Math.round(value);
          var H = 0;
          var I = 0;
          var D = '';
          if (new_value > 0) {
            H = Math.floor(new_value / 60);
            I = Math.round((new_value % 60));
            // D = '';
            if (H < 10) {
              H = '0' + H;
            }

            if (H > 24) {
              D = Math.floor(H / 24) + 'd';
              H = Math.floor(H % 24);

            }

            if (I < 10) {
              I = '0' + I;
            }
            new_value = D + ' ' + H + ':' + I;

          } else {
            new_value = "00:00";
          }
          return new_value;
        })
      }
    },
    series: [

      {
        name: 'average',
        type: 'bar',
        barWidth: 35,
        data: [0,
          <?php
          if (count($week) > 1) {
            $date_listz = array_chunk($date_list, 7);
            $no_dataz = 0;
            foreach ($week as $weekz) {
              $dataz_total = 0;
              $count_dataz = 0;
              foreach ($date_listz[$no_dataz] as $date) {
                $dataz_total = $dataz_total + $data_average[$date];
                if ($data_average[$date] > 0) {
                  $count_dataz++;
                }
              }
              if ($dataz_total > 0) {
                $dataz_total = ROUND($dataz_total / $count_dataz);
              } else {
                $dataz_total = 0;
              }

              echo "'" . $dataz_total . "',";
              $no_dataz++;
            }
          } else {
            foreach ($date_list as $date) {
              echo "'" . ROUND($data_average[$date]) . "',";
            }
          }

          ?>

        ],
        markPoint: {
          data: [
            0,
            <?php
            $no = 1;
            if (count($week) > 1) {
              $date_listz = array_chunk($date_list, 7);
              $no_dataz = 0;
              foreach ($week as $weekz) {
                $dataz_total = 0;
                $count_dataz = 0;
                foreach ($date_listz[$no_dataz] as $date) {
                  $dataz_total = $dataz_total + $data_average[$date];
                  if ($data_average[$date] > 0) {
                    $count_dataz++;
                  }
                }
                if ($dataz_total > 0) {
                  $dataz_total = ROUND($dataz_total / $count_dataz);
                } else {
                  $dataz_total = 0;
                }
                if ($dataz_total > 0) {
                  $m = ROUND($dataz_total % 60);
                  $h = floor($dataz_total / 60);
                  $d = "";
                  if ($m < 10) {
                    $m = "0" . $m;
                  }
                  if ($h > 24) {
                    $d = floor($h / 24);
                    $h = floor($h % 24);
                  }
                  if ($h < 10) {
                    $h = "0" . $h;
                  }
                  $getTime = $d . " " . $h . ":" . $m;
                } else {
                  $getTime = "00:00";
                }

                echo "{name: 'Otif', value: '" . $getTime . "', xAxis: " . $no . ", yAxis: " . $dataz_total . "},";
                $no++;
                $no_dataz++;
              }
            } else {
              foreach ($date_list as $date) {
                if ($data_average[$date] > 0) {
                  $m = ROUND($data_average[$date] % 60);
                  $h = floor($data_average[$date] / 60);
                  $d = "";
                  if ($m < 10) {
                    $m = "0" . $m;
                  }
                  if ($h > 24) {
                    $d = floor($h / 24);
                    $h = floor($h % 24);
                  }
                  if ($h < 10) {
                    $h = "0" . $h;
                  }
                  $getTime = $d . " " . $h . ":" . $m;
                } else {
                  $getTime = "00:00";
                }
                echo "{name: 'Otif', value: '" . $getTime . "', xAxis: " . $no . ", yAxis: " . ROUND($data_average[$date]) . "},";
                $no++;
              }
            }

            ?>
          ]
        }

      }
    ]
  };;
  if (option && typeof option === "object") {
    myChart.setOption(option, true);
  }
</script>

<!-- Truck On time -->

<?php

$early = array();
$ontime = array();
$delay = array();
foreach ($date_list as $date) {
  $early[$date] = 0;
  $delay[$date] = 0;
  $ontime[$date] = 0;
}

foreach ($data_list as $data) {
  $shift = $data->shift;

  if ($data->status == "Early") {
    $early[$data->rdd] = $early[$data->rdd] + 1;
  } else if ($data->status == "Delay") {
    $delay[$data->rdd] = $delay[$data->rdd] + 1;
  } else {
    $ontime[$data->rdd] = $ontime[$data->rdd] + 1;
  }
}



?>

<script type="text/javascript">
  //
  var dom = document.getElementById("truck_on_time_chart");
  var myChart = echarts.init(dom);
  var app = {};
  option = null;
  app.title = 'On Timenes Truck';

  option = {
    color: ['orange', 'green', '#e5323e'],
    tooltip: {
      trigger: 'axis',
      axisPointer: { // 坐标轴指示器，坐标轴触发有效
        type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
      }
    },
    legend: {
      data: [
        'Early', 'On Time', 'Delay'
      ]
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [{
        type: 'category',
        splitLine: {
          show: false
        },
        data: [<?php
                if (count($week) > 1) {
                  foreach ($week as $weekz) {
                    echo "'" . $weekz . "',";
                  }
                } else {
                  foreach ($date_list as $date) {
                    echo "'" . date('d-m-Y', strtotime($date)) . "', ";
                  }
                }
                ?>]
      },

    ],
    yAxis: [{
      splitLine: {
        show: false
      },
      type: 'value'
    }],
    series: [{
        name: 'Early',
        type: 'bar',
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        },
        data: [
          <?php
          if (count($week) > 1) {
            foreach ($week as $weekz) {
              $last = count($data_list_week[$weekz]) - 1;
              if ($last < 0) {
                $data_early = 0;
              } else {
                if ($data_list_week[$weekz][$last]->early > 0) {
                  $data_early = $data_list_week[$weekz][$last]->early;
                } else {
                  $data_early = 0;
                }
              }
              echo "'" . $data_early . "',";
            }
          } else {
            foreach ($date_list as $date) {
              echo $early[$date] . ',';
            }
          }
          ?>
        ]
      },
      {
        name: 'On Time',
        type: 'bar',
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        },
        data: [
          <?php
          if (count($week) > 1) {
            foreach ($week as $weekz) {
              $last = count($data_list_week[$weekz]) - 1;
              if ($last < 0) {
                $go = 0;
              } else {
                if ($data_list_week[$weekz][$last]->good > 0) {
                  $go = $data_list_week[$weekz][$last]->good;
                } else {
                  $go = 0;
                }
              }
              echo "'" . $go . "',";
            }
          } else {
            foreach ($date_list as $date) {
              echo $ontime[$date] . ',';
            }
          }
          ?>
        ]
      },
      {
        name: 'Delay',
        type: 'bar',
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        },
        data: [
          <?php
          if (count($week) > 1) {
            foreach ($week as $weekz) {
              $last = count($data_list_week[$weekz]) - 1;
              if ($last < 0) {
                $del = 0;
              } else {
                if ($data_list_week[$weekz][$last]->delay > 0) {
                  $del = $data_list_week[$weekz][$last]->delay;
                } else {
                  $del = 0;
                }
              }
              echo "'" . $del . "',";
            }
          } else {
            foreach ($date_list as $date) {
              echo $delay[$date] . ',';
            }
          }
          ?>
        ]
      },


    ]
  };;
  if (option && typeof option === "object") {
    myChart.setOption(option, true);
  }

  //

  $(document).ready(function() {
    setTimeout(function() {
      $('#loading_ontime').hide();
    }, 1000);
  });

  $(".tables").DataTable({
    "dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
  });
</script>