<style type="text/css">
.select2-results__option {
    padding: 6px;
    user-select: none;
    -webkit-user-select: none;
}

	table th{
		background-color: #00aec5 !important;
		text-align: center;
		font-size:12px;
		justify-content: center;
  		flex-direction: column;
	}

table td 
{
	white-space: nowrap;
	margin: 0px;
	padding: 0px!important;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: middle;
    border-top: 1px solid #ddd;
}

.table-responsive{
	margin-top:-20px;
}

	input {
	font-size:12px;
    width: 100%;
    margin:0px;
    padding:0px;
    display: inline-table;
    
		}
}

@media screen and (min-width: 400px){
	.button-action{
		text-align: center;
		
	}

	.schedule-periode{
		width: 100%
	}

	#date-filter{
		padding:0px;
		margin-left:35px;
	}

}

@media screen and (min-width: 800px){
	.button-action{
		text-align: right;
		margin-top:30px;
	}

	.schedule-periode{
		margin-top:20px;
		width: 100%
	}

	.date-filter{
		padding:0px;
		margin-left:35px;
	}

	.category-4{
		
	}
}

</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col">
                <div class="card" style="margin-bottom:0px; margin-top:20px">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-file-invoice"></i>
                    </div>
                    <h4 class="card-title">
                    	<table>
                    		<tr>
                    			<td width="130">Schedule Table</td>
                    			<td><div class="togglebutton">
				                        <label>
				                          <input onchange="window.location = '?category=<?php echo $_GET['category'] ?>&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>&table=ON'" type="checkbox" data-toggle="switch">
				                          <span class="toggle"></span>
				                          Select is off
				                        </label>
				                      </div></td>
                    		</tr>
                    	</table>
                    	
									
									
					</div></h4>
                  <div class="card-body " style="margin-bottom:-15px">
    <!-- Content Header (Page header) -->

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2 col-sm-12">
					<?php 
				$time = date('H:i:s');

				$start_time = $time_freeze->row()->start_time;
				$end_time = $time_freeze->row()->end_time;
				$status_time = '';
				if($time >= $end_time || $time < $start_time){
					 $status_time = "disabled";
				}else{
					
				}
				 ?> 

				 <?php $year = $_GET['year']; $week = $_GET['week']; $get_category= $_GET['category']; ?>
				
				
							<div class="form-group date-filter">
						            <label style="margin-left:70px" >Year</label>
						            <nav aria-label="Page navigation example">
						                <ul class="pagination pagination-primary">
						                  
						                  <li class="page-item">
						                    <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $year-1; ?></a>
						                  </li>
						                  
						                  <li class="page-item active">
						                    <a class="page-link " id='year' href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $year; ?></a>
						                  </li>

						                  <li class="page-item">
						                    <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $year+1; ?></a>
						                  </li>
						                </ul>
						              </nav>
						           </div>
						           </div>

						<div class="col-md-3 col-sm-12">
							<div class="form-group date-filter">
						            <label style="margin-left:85px">Week</label>
						            <nav aria-label="Page navigation example">
						                <ul class="pagination pagination-primary">
						                  
						                  <li class="page-item">
						                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>&category=<?php echo $get_category; ?>"><?php echo $week-2; ?></a>
						                  </li>
						                   <li class="page-item">
						                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>&category=<?php echo $get_category; ?>"><?php echo $week-1; ?></a>
						                  </li>
						                  
						                  <li class="page-item active">
						                    <a class="page-link " id='week' href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&category=<?php echo $get_category; ?>"><?php echo $week; ?></a>
						                  </li>
						                  <li class="page-item">
						                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>&category=<?php echo $get_category; ?>"><?php echo $week+1; ?></a>
						                  </li>
						                  <li class="page-item">
						                    <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>&category=<?php echo $get_category; ?>"><?php echo $week+2; ?></a>
						                  </li>
						                </ul>
						              </nav>
						            </div>         	         
				</div>
				

				<div class="col-md-3 col-sm-12 schedule-periode" <?php if(!check_sub_menu(10)){echo "hidden";} ?> style="text-align:center">
					<?php if($this->session->userdata('sess_role_no') == 1){ ?>
					<button class="btn btn-sm" data-toggle="modal" data-target="#myModal"><img width="25" height="25" src="<?php echo base_url() ?>assets/images/ice.png">Scheduling Periode</button>
					<?php } ?>
				</div>			

					<div class="col button-action" style="padding:0px" <?php if(!check_sub_menu(9)){echo "hidden";} ?>>
						<button style="width: 75px;" <?php echo $status_time; ?> class="btn btn-primary btn-sm" onclick="sumbitForm();"><i class="fa fa-book"></i> SAVE</button>
						<button style="width: 102px" <?php echo $status_time; ?> class="btn btn-warning btn-sm" onclick="clear_all();"><i class="fa fa-paint-brush" ></i> CLEAR ALL</button>
						<!-- <button <?php if($this->session->userdata('sess_role_no') != 1){ echo "style='display:none'";} ?> class="btn btn-danger" onclick="delete_schedule2()"><i class="fa fa-trash"></i> DELETE ALL</button> -->
						 <button class="btn btn-danger btn-sm" <?php echo $status_time; ?> <?php echo ($this->session->userdata('sess_role_no') == 3)?'style="display: none"':'' ?> onclick="check_delete()"><i class="fa fa-trash"></i> DELETE</button>
						<b hidden="true" id="saving" style="color:green">Saving...</b><b hidden="true" id="deleting" style="color:red">Deleting..</b>
						<img hidden="true" id="loading" src="<?php echo base_url()?>assets/images/loading.gif" style="width: 30px;height: 30px">
					</div>
			</div>

			<style>.category-4 .btn-group .btn.btn-info{padding : 15px;}</style>
			<div class="row">
				<div class="col-md-4 col-sm-12 category-4" style="padding: 0">
					<div class="btn-group" data-toggle="buttons">
						<?php
							foreach ($category->result() as $category) {
							?>
								<button type="button" class="btn  btn-info <?php if($_GET['category'] == $category->category){echo "active";} ?>" onclick="window.location=('<?php echo base_url() ?>schedule/schedule2?category=<?php echo $category->category; ?>&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>')"><?php echo $category->category; ?></button>
							<?php
							}
						 ?>
                      
                    </div>
				</div>

				<div class="col">
				<form action="<?php echo base_url(); ?>schedule/schedule2_search" method='GET'>
					<div class="container-fluid">
						<div class="row">
							<div class="col">
							</div>
							<div class="col-md-3 col-sm-12" style="margin-top:22px">					
								<select name="select_category" class="form-control text-center categoryx" onchange="search_category(this.value)">
									<option <?php if(isset($_GET['select_category']) && $_GET['select_category'] == '' ){ echo "selected"; } ?> value="">-- SELECT ROWS --</option>

									<option <?php if(isset($_GET['select_category']) && $_GET['select_category'] == 'supplier' ){ echo "selected"; } ?> value="supplier">-- SUPPLIER --</option>

									<option <?php if(isset($_GET['select_category']) && $_GET['select_category'] == 'item' ){ echo "selected"; } ?> value="item">-- ITEM --</option>
								</select>
							</div>

							<div class="col-md-3 col-sm-12" style="margin-top:22px; <?php if(!isset($_GET['select_category']) || $_GET['select_category'] != 'item'){echo 'display:none'; } ?>" id="select_item">
							
							<input type="text" hidden value="<?php echo $_GET['category']; ?>" name="category">
							<input type="text" hidden value="<?php echo $_GET['week']; ?>" name="week">
							<input type="text" hidden value="<?php echo $_GET['year']; ?>" name="year">
							<select name="menu" <?php if(!isset($_GET['select_category'])){echo 'disabled="true"'; } ?> id="#menu" class="form-control text-center menux" onchange="select_menu(this.value)">
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == '' ){ echo "selected"; } ?> value="">-- SELECT ROWS --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'material_code' ){ echo "selected"; } ?> value="material_code">-- ITEM CODE --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'material_name' ){ echo "selected"; } ?> value="material_name">-- ITEM DESC --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'plt_truck' ){ echo "selected"; } ?> value="plt_truck">-- PLANT --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'po_number' ){ echo "selected"; } ?> value="po_number">-- PO DOC --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'po_line_item' ){ echo "selected"; } ?> value="po_line_item">-- LINE --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'qty' ){ echo "selected"; } ?> value="qty">-- ORDER QTY --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'uom' ){ echo "selected"; } ?> value="uom">-- UOM --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'requested_delivery_date' ){ echo "selected"; } ?> value="requested_delivery_date">-- DATE --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'shift' ){ echo "selected"; } ?> value="shift">-- SHIFT --</option>
								<option <?php if(isset($_GET['menu']) && $_GET['menu'] == 'uom_plt' ){ echo "selected"; } ?> value="uom_plt">-- QTY/PLT --</option>
							</select>
						</div>

						<div class="col-md-3 col-sm-12" style="margin-top:22px; <?php if(!isset($_GET['select_category']) || $_GET['select_category'] != 'supplier'){echo 'display:none'; } ?>" id="select_supplier">
							<select name="supplier" id="supplier" <?php if(!isset($_GET['supplier'])){ ?> disabled="true" <?php } ?> class="form-control text-center">
		                        <option value=''>-- SELECT SUPPLIER --</option>
		                        <?php
		                        foreach($supplier->result() as $get){
		                            ?>
		                        <option <?php if(isset($_GET['select_category']) && $_GET['select_category'] == 'supplier' && isset($_GET['supplier']) && $_GET['supplier'] == $get->vendor_code){ echo "selected"; } ?>  value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code." - ".$get->vendor_name; ?></option>
		                            
		                            <?php
		                        	}
		                        ?>
		                     </select>
						</div>
						<div class="col-md-3 col-sm-12" style="margin-top:22px;<?php if(!isset($_GET['select_category']) || $_GET['select_category'] != 'item'){echo 'display:none'; } ?>" id="filter_text">
							<input type="text" placeholder="Search Item" value="<?php if(isset($_GET['filter_text'])){ echo $_GET['filter_text'];} ?>" name="filter_text" class="form-control">
						</div>
						
						<div class="col-2" id="filter" style="margin-top:22px;">
							    <button id="filter" type="submit" onclick="$('#select_form').submit()" class="btn btn-primary btn-sm">Filter</button> 
							</div>
						</div>
					</form>
				</div>
			</div>
				
				</div>
				
			</div>

			<?php
			if($this->session->flashdata('DELETED') != ""){
				?>
			<div class="alert alert-danger alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				<b><?php echo $this->session->flashdata('DELETED'); ?></b>
			</div>
				<?php
			}
			?>

			<?php
			if($this->session->flashdata('SAVED') != ""){
				?>
			<div class="alert alert-info alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				 <b><?php echo $this->session->flashdata('SAVED'); ?></b>
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_ERROR') != ""){
				?>
			<div class="alert alert-error alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				Error! Perhatikan lagi data yang diupload dengan nomor PO <b><?php echo $this->session->flashdata('LIST_ERROR') ?></b>
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_NOACT') != ""){
				?>
			<div class="alert alert-warning alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				<a href="#" target="_">Jadwal pengiriman dengan nomor PO <b><?php echo $this->session->flashdata('LIST_NOACT') ?></b> tidak berubah, material sedang dikirim / sudah diterima.</a>
			</div>
				<?php
			}
			?>
							
				<!-- <div class="col-md-4" style="margin-top:-2px;">
							<div class="form-inline" style="margin-top:-7px">
				              	<button type="button" class="btn btn-info" style="" onclick="setYear_sch($('#year_sch').val(), 'DEC')">
				                  <i class="fa fa-chevron-left"></i>
				                </button>
				                <div class="form-group">
				                <input type="number" class="form-control" onchange="(setYear2_sch())"  id="year_sch" style="width: 70px; text-align: center; font-size: 15px" value="<?php echo (null === $_GET['year'])?date('Y'):$_GET['year']; ?>">
				            	</div>

				                <button type="button" class="btn btn-info" style="" onclick="setYear_sch($('#year_sch').val(), 'INC')">
				                  <i class="fa fa-chevron-right" ></i>
				                </button>

				                <button type="button" class="btn btn-info" style="" onclick="setWeek_sch($('.week_sch').val(), 'DEC')">
				                  <i class="fa fa-chevron-left"></i>
				                </button>
				                
				                <input onchange="setWeek_sch(($('.week_sch').val())-1, 'INC')" type="number" class="form-control week_sch" style="width: 70px; text-align: center; font-size: 15px" value="<?php echo (null === $_GET['week'])?date('W'):$_GET['week']; ?>">
				                <button type="button" class="btn btn-info" style=""onclick="setWeek_sch($('.week_sch').val(), 'INC')">
				                  <i class="fa fa-chevron-right" ></i>
				                </button>
		            		</div>
							
				</div> -->

			</div>
		</div>
	</div>
</div>
</div>			

<div class="content">
  <div class="content">
    <!-- <div class="container-fluid"> -->
            <div class="row">
              <div class="col">
                <div class="card " style="margin-bottom:0px; margin-top:5px">
                  <!-- <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-file-invoice"></i>
                    </div>
                    <h4 class="card-title">Schedule Table </h4>
                  </div> -->
                  <div class="card-body ">

                  	<!-- Start -->
                  <div class="row">
                  	<div class="col">
						<form id="idSchTbl" action="<?php echo base_url(); ?>schedule/save_schedule2?category=<?php echo $_GET['category'] ?>" method="post">
						<input type="hidden" name="yyy" value="<?php echo (null === $_GET['year'])?date('Y'):$_GET['year']; ?>">
						<input type="hidden" name="www" value="<?php echo (null === $_GET['week'])?date('W'):$_GET['week']; ?>">
					<div class="table-schedule table-responsive" style=" overflow-x: scroll;margin-top:5px">
				<table border="0" id="schedule_table" class="table table-hover">
					<thead style="position: relative; z-index: 2;">
						<tr>
									<th rowspan="2" width="30" style="vertical-align : middle;text-align:center;"><input <?php if($this->session->userdata('sess_role_no') == 3){ echo "style='display:none'";} ?> id="check_all" style="width: 15px;height: 15px" type="checkbox" name="check_all"></th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="40"><b disabled value="No" style="height: 50px;" />No</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="45"><b disabled value="Category" style="height: 50px" />Category</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="80"><b disabled value="Vendor/Supplier" style="height: 50px" />Vendor</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="100"><b disabled value="Item Code" style="height: 50px;" />Item Code</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="270"><b disabled value="Item Description" style="height: 50px;"/>Item Description</th>
									
									<th style="font-size:12px; font-weight:800" rowspan="2" width="60"><b disabled value="Plant" style="height: 50px"/>Plant</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="100"><b disabled value="Purchase Order Doc." style="height: 50px"/>Purchase Order Doc.</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="45"><b disabled value="Line" style="height: 50px"/>Line</th>
									<?php
										if($this->input->get('category') == "IBD"){
											?>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="45"><b disabled value="Bill Of Lading" style="height: 50px"/>Bill of Lading</th>
											<?php
										}
									?>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="60"><b disabled value="Order Qty" style="height: 50px"/>Order Qty</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="20"><b disabled value="UOM" style="height: 50px"/>UOM</th>
									<th style="font-size:12px; font-weight:800" colspan="2"><b disabled value="Revision Del.Plan" style="width: 260px;height: 25px"/>Revision Del.Plan
									</th>
									<th style="font-size:12px; font-weight:800" rowspan="2" width="40"><b disabled value="Qty per Pallet" style="height: 50px"/>Qty Per Pallet</th>

									<?php if($data->result() != null){
										?>
										<th style="font-size:12px; font-weight:800" colspan="2" width="100"><b disabled value="Progressor" style="height: 50px;" />Progressor</th>
										<?php
									} ?>
								</tr>
								<tr>
									<th style="font-size:12px; font-weight:800" width="110"><b disabled/>Date</th>
									<th style="font-size:12px; font-weight:800" width="40"><b disabled/>Shift</th>

									<?php if($data->result() != null){
										?>
									<th style="font-size:12px; font-weight:800">Created By</th>
									<th style="font-size:12px; font-weight:800">Edit By</th>
								<?php } ?>
								</tr>
					</thead>
					<tbody>
					
						<?php $row=0;
						$i=0;
						
						foreach ($data->result() as $get) {
						 $col = 0;
						?>
					<tr>
						<td style="text-align: center" width="30"><input <?php if($this->session->userdata('sess_role_no') == 3){ echo "style='display:none'";}else if($get->status != 0){echo "style='display:none'";} ?> value="<?php echo $get->id;?>" type="checkbox" value="<?php echo $get->id; ?>"  id="checkItem" name="check_item"><!-- <span  class="inputz <?php echo $row; ?>" onclick="delete_row_schedule2(<?php echo $get->id;?>, <?php echo $i+1; ?>, '<?php echo $get->category ?>')"><i class="fa fa-close"  style="color:red"></i></span> --></td>
						<td width="40"><input disabled value="<?php echo $i+1; ?>"  name="<?php echo $get->id;?>" id="no<?php echo $row;?>"/></td>

						<td width="65"><input style="text-align:center" value="<?php echo $get->category; ?>" class="inputz <?php echo $row; ?> " name="category[]" id="2_<?php echo $row; ?>" /></td>

						<td width="80"><input value="<?php echo $get->vendor_code." ".$get->vendor_alias; ?>" class="inputz <?php echo $row; ?> "  name="vendor[]" id="3_<?php echo $row; ?>" /></td>

						<td width="100"><input value="<?php echo $get->material_code; ?>" class="inputz <?php echo $row; ?> " name="item_code[]" id="4_<?php echo $row; ?>" /></td>

						<td width="300"><input value="<?php echo $get->material_name; ?>" class="inputz <?php echo $row; ?> " name="material_name[]" id="5_row<?php echo $row; ?>col<?php echo $col++; ?>" /></td>

						<td width="60"><input value="<?php echo $get->plt_truck; ?>" class="inputz <?php echo $row; ?> " name="plan[]"  id="7_<?php echo $row; ?>" /></td>

						<td width="100"><input value="<?php echo $get->po_number; ?>" class="inputz <?php echo $row; ?> " name="purchase_order[]" id="8_<?php echo $row; ?>" /></td>

						<td width="45"><input style="text-align: center!important;" value="<?php echo $get->po_line_item; ?>" class="inputz <?php echo $row; ?> " name="line[]" id="9_<?php echo $row; ?>" /></td>
						<?php
							if($this->input->get('category') == "IBD"){
								?>
						<td width="45"><input style="text-align: center!important;" value="<?php echo $get->bill_of_lading; ?>" class="inputz <?php echo $row; ?> " name="bill_of_lading[]" id="20_<?php echo $row; ?>" /></td>
								<?php
							}
						?>

						<td width="60"><input value="<?php echo round($get->qty,3); ?>" class="inputz <?php echo $row; ?> " name="order_qty[]" id="10_<?php echo $row; ?>" /></td>

						<td width="35" align="center"><input style="text-align: center!important;" value="<?php echo $get->uom; ?>" class="inputz <?php echo $row; ?> "name="uom[]" id="11_<?php echo $row; ?>" /></td>

						<td width="40"><input style="text-align:center" value="<?php echo Date('d-m-Y',strtotime($get->requested_delivery_date)); ?>"  class="inputz <?php echo $row; ?> " name="date[]" id="12_<?php echo $row; ?>"/></td>

						<td  width="40"><input style="text-align: center!important;" value="<?php echo $get->shift; ?>" class="inputz <?php echo $row; ?> " name="shift[]" id="13_<?php echo $row; ?>"/></td>
						
						<td width="60" align="center"><input disabled value="<?php echo $get->uom_plt; ?>" class="inputz <?php echo $row; ?> " name="qty_per_palet[]" id="6_<?php echo $row; ?>" /></td>

						<?php if($data->result() != null){
										?>
						<td  width="40"  style="border: 1px solid"><input type="text" class="inputz" disabled value="<?php echo $get->created_name; ?>" name=""> </td>
						<td  width="40" style="border: 1px solid"><input type="text" class="inputz" disabled value="<?php echo $get->edit_name; ?>" name=""></td>
						<?php } ?>
						
					</tr>
					
					<?php $i++; $row++; } ?> 
					 <?php
					 $a = $i;
					 while($i < $a+100) {
						 $col = 0;
						?>
					<tr>
						<td width="30" style="text-align: right"></td>
						<td width="40">
							<input disabled value="<?php echo $i+1; ?>" id="no<?php echo $row;?>"/>
						</td>

						<td width="65">
							<input class="inputz <?php echo $row; ?>" name="category[]" id="2_<?php echo $row; ?>" />
						</td>

						<td width="80">
							<input class="inputz <?php echo $row; ?>"  name="vendor[]" id="3_<?php echo $row; ?>" />
						</td>

						<td width="80">
							<input class="inputz <?php echo $row; ?>" name="item_code[]" id="4_<?php echo $row; ?>" />
						</td>

						<td width="270">
							<input class="inputz <?php echo $row; ?>" name="material_name[]" id="5_row<?php echo $row; ?>col<?php echo $col++; ?>" />
						</td>

						

						<td width="60">
							<input class="inputz <?php echo $row; ?>" name="plan[]"  id="7_<?php echo $row; ?>" />
						</td>

						<td width="100">
							<input class="inputz <?php echo $row; ?>" name="purchase_order[]" id="8_<?php echo $row; ?>" /></td>

						<td width="45">
							<input class="inputz <?php echo $row; ?>" name="line[]" id="9_<?php echo $row; ?>" />
						</td>
						<?php
							if($this->input->get('category') == "IBD"){
								?>
						<td width="90"><input style="text-align: center!important;" class="inputz <?php echo $row; ?> " name="bill_of_lading[]" id="20_<?php echo $row; ?>" /></td>
								<?php
							}
						?>

						<td width="60">
							<input class="inputz <?php echo $row; ?>" name="order_qty[]" id="10_<?php echo $row; ?>" />
						</td>

						<td width="20">
							<input style="text-align:center!important" class="inputz <?php echo $row; ?>"name="uom[]" id="11_<?php echo $row; ?>" />
						</td>

						<td width="40">
							<input  class="inputz <?php echo $row; ?>" name="date[]" id="12_<?php echo $row; ?>"/>
						</td>

						<td width="40">
							<input class="inputz <?php echo $row; ?>" name="shift[]" id="13_<?php echo $row; ?>"/>
						</td>

						<td width="60">
							<input disabled class="inputz <?php echo $row; ?>" name="qty_per_palet[]" id="6_<?php echo $row; ?>" />
						</td>

						<?php if($data->result() != null){ ?>
						
						<td style="border: 1px solid">
							<input class="inputz" type="text" disabled name="">
						</td>
						<td style="border: 1px solid">
							<input class="inputz" type="text" disabled name="">
						</td>
						<?php } ?>
					</tr> 
					<?php $i++; } ?>

					</tbody>
				</table>
			</div>
				</div>	
				</form>
				</div>
						

						<!-- End -->
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
</div>

			


		</div>
	</div>

		<table id="header-fixed"></table>
		<img/><div id="json"></div>          
		</div>
	

<div class="modal fade" id="myModal" tabindex="-1" role="">
    <div class="modal-dialog modal-login" role="document">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                  <div class="card-header card-header-primary text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                      <i class="material-icons">clear</i>
                    </button>

            <h4 class="card-title">Scheduling Periode</h4>
            <div class="social-line">
              
        </div>
      </div>
    </div>

      <div class="modal-body">

        <table class="table table-striped table-bordered table-hover">
             <thead style="font-weight: 650">
        		<tr>
        			<td align="center">No</td>
        			<td align="center">Day</td>
        			<td align="center">Start Activity</td>
        			<td align="center">End Activity</td>
        			<!-- <td>Action</td> -->
        		</tr>
        	</thead>
        	<tbody>
        		<?php 
        		$no = 1;
        		$freeze = $this->db->query("SELECT * FROM tb_time_freeze");
        		foreach($freeze->result() as $get){
        			$get_start = explode(':',$get->start_time);
        			$start_hour = $get_start[0];
        			$start_minute = $get_start[1];

        			$get_end = explode(':',$get->end_time);
        			$end_hour = $get_end[0];
        			$end_minute = $get_end[1];

        			if($get->day == 'Mon'){
        				$dayz = "Senin";
        			}else if($get->day == 'Tue'){
        				$dayz = "Selasa";
        			}else if($get->day == 'Wed'){
        				$dayz = "Rabu";
        			}else if($get->day == 'Thu'){
        				$dayz = "Kamis";
        			}else if($get->day == 'Fri'){
        				$dayz = "Jumat";
        			}else if($get->day == 'Sat'){
        				$dayz = "Sabtu";
        			}else if($get->day == 'Sun'){
        				$dayz = "Minggu";
        			}
        			?>

        		<tr>
        			<td width="40" align="center"><?php echo $no; ?></td>
        			<td style="width: 150px" ><?php echo $dayz; ?></td>
        			<td align="center"><input type="number" class="activity_start_hour" data-id="<?php echo $get->id ?>" name="start_hour" id="start_hour<?php echo $no ?>" style="width:65px;text-align:center;font-size:14px" max="23" value="<?php echo $start_hour; ?>">
        				<input style="width:65px;text-align:center;font-size:14px" class="activity_start_minute" data-id="<?php echo $get->id ?>" type="number" max="59" value="<?php echo $start_minute; ?>" id="start_minute<?php echo $no ?>" name="start_minute"></td>

        			<td><input type="number" data-id="<?php echo $get->id ?>" class="activity_end_hour" name="end_hour" id="end_hour<?php echo $no ?>" style="width:65px;text-align:center;font-size:14px" max="23" value="<?php echo $end_hour; ?>"><input class="activity_end_minute"style="width:65px;font-size:14px;text-align:center" type="number" max="59" data-id="<?php echo $get->id ?>" id="end_minute<?php echo $no ?>" value="<?php echo $end_minute; ?>" name="end_minute"></td>
        			<!-- <td><button type="button" onclick="freeze_time('<?php echo $get->id; ?>', <?php echo $no; ?>)" class="btn btn-primary"><i class="fa fa-save"></i> Save Time</button> <i hidden="true" id="status_time" style="color:green">Data has been Saved</i></td> -->
        		</tr>
        	<?php $no++; } ?>
        	</tbody>
        </table>

      </div>
      <button type="button" style="margin-left: 10px;margin-bottom: 10px;" class="btn save-freeze btn-primary"><i class="fa fa-save"></i> Save Time</button> <i hidden="true" id="status_time" style="color:green">Data has been Saved</i>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
      </div>
    </div>

  </div>
</div>

</div>
	