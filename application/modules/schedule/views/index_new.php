
<?php
$a = 0;
?>
<style type="text/css">
  div.scrollmenu {
  background-color: #333;
  overflow: auto;
  white-space: nowrap;
}

div.scrollmenu a {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px;
  text-decoration: none;
}

.card-stats .card-header+.card-footer {
    border-top: 1px solid #eee;
    margin-top: 0px;
}

body{
  font-size: 0.7rem;
}

.table thead tr th {
    font-size: 0.95rem;
}

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 15px 0px;
    vertical-align: middle;
    border-color: #ddd;
}

form .form-group select.form-control {
    position: relative; 
    top: 0px;
    text-align: center; 
}

input {
    border: none;
    color: #555;
    text-align: center;
    width: 60px;
  }

div.scrollmenu a:hover {
  background-color: #777;
}

.card-category{
  font-size:5px;
}


@media screen and (min-width: 400px) {
  .header-dashboard{
    text-align:center;
  }

  .year{
    text-align: center;
  }

  .week{
    text-align:center;
  }

  .button-fillter{
    margin-top:0px;
    text-align:center;
  }
  
  .legends{
    padding:15px;
    font-size:14px;
  }

  .icon1{
    margin-top:20px;
  }

  .button-days{
    text-align: center;
  }
}


@media screen and (min-width: 600px) {
  .day-filter{
    text-align:left;

  }
  
  .legends{
    padding:15px;
    font-size:12px;
  }

  .icon1{
    margin-top:0px;
  }

  .button-days{
    margin-top:-20px;
    text-align: right;
  }

  .button-fillter{
    margin-top:20px;
    text-align: right;
  }
}

.swal2-popup .swal2-title {
  font-size:20px;
  color:white;
}

.swal2-popup #swal2-content {
    font-size: 15px;
    font-weight: 600;
}

.swal2-popup .swal2-header {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding: 3px;
    background-color: #23355b;
    color: white;
}
</style>
<?php date_default_timezone_set("Asia/Bangkok"); ?>
<div class="content">
<div class="content">
  <div class="container-fluid">
        <!-- <div class="row">
              <div class="col-md-4">
                <div class="card card-chart">
                  <div class="card-header card-header-rose">
                    <div id="roundedLineChart" class="ct-chart"></div>
                  </div>
                  <div class="card-body">
                    <h4 class="card-title">Rounded Line Chart</h4>
                    <p class="card-category">Line Chart</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-chart">
                  <div class="card-header card-header-warning">
                    <div id="straightLinesChart" class="ct-chart"></div>
                  </div>
                  <div class="card-body">
                    <h4 class="card-title">Straight Lines Chart</h4>
                    <p class="card-category">Line Chart with Points</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="card card-chart">
                  <div class="card-header card-header-info">
                    <div id="simpleBarChart" class="ct-chart"></div>
                  </div>
                  <div class="card-body">
                    <h4 class="card-title ">Simple Bar Chart</h4>
                    <p class="card-category">Bar Chart</p>
                  </div>
                </div>
              </div>
            </div>   -->
<br>

          <div class="row">
            <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="fas fa-th-large" style="font-size:30px"></i>
                  </div>
                  <h4 class="card-title">Schedule Dashboard</h4>
                </div>
                
    <!-- Content Header (Page header) -->
    <div class="card-body">
      <div class="row justify-content-start">
        <div class="col-md-2 col-sm-12">
                <div class="form-group">
                  <?php 
                    $dto = new DateTime();
                    $dto->setISODate($year-1, $week);
                    $year1 = $dto->format('Y-m-d');

                    $dto->setISODate($year, $week);
                    $year2 = $dto->format('Y-m-d'); 

                    $dto->setISODate($year+1, $week);
                    $year3 = $dto->format('Y-m-d');

                    $dto->setISODate($year, $week-2);
                    $week1 = $dto->format('Y-m-d');

                    $dto->setISODate($year, $week-1);
                    $week2 = $dto->format('Y-m-d');

                    $dto->setISODate($year, $week);
                    $week3 = $dto->format('Y-m-d');

                    $dto->setISODate($year, $week+1);
                    $week4 = $dto->format('Y-m-d');

                    $dto->setISODate($year, $week+2);
                    $week5 = $dto->format('Y-m-d'); 
                   ?>
                  <label style="margin-left:70px">Year</label>
                  <nav aria-label="Page navigation example">
                      <ul class="pagination pagination-primary">
                        
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year-1; ?>&week=<?php echo $week; ?>&date=<?php echo $year1; ?>"><?php echo $year-1; ?></a>
                        </li>
                        
                        <li class="page-item active">
                          <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo $year2; ?>"><?php echo $year; ?></a>
                        </li>

                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year+1; ?>&week=<?php echo $week; ?>&date=<?php echo $year3; ?>"><?php echo $year+1; ?></a>
                        </li>

                      </ul>
                    </nav>
                  </div>
      </div>
      <div class="col-md-2 col-sm-12">
                <div class="form-group" >
                  <label style="margin-left:85px">Week</label>
                  <nav aria-label="Page navigation example">
                      <ul class="pagination pagination-primary" >
                        
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-2; ?>&date=<?php echo $week1; ?>"><?php echo $week-2; ?></a>
                        </li>
                         <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week-1; ?>&date=<?php echo $week2; ?>"><?php echo $week-1; ?></a>
                        </li>
                        
                        <li class="page-item active">
                          <a class="page-link " href="?year=<?php echo $year; ?>&week=<?php echo $week; ?>&date=<?php echo $week3; ?>"><?php echo $week; ?></a>
                        </li>
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+1; ?>&date=<?php echo $week4; ?>"><?php echo $week+1; ?></a>
                        </li>
                        <li class="page-item">
                          <a class="page-link" href="?year=<?php echo $year; ?>&week=<?php echo $week+2; ?>&date=<?php echo $week5; ?>"><?php echo $week+2; ?></a>
                        </li>
                      </ul>
                    </nav>
                  </div>
                </div>
          <?php
            $date = date_create($date_list[$a]);
            $df = date_format($date, 'Y/m/d');
                  $check_date = '';
                  if(!isset($_GET['date'])){
                    $check_date = $df;
                  }else{
                    $check_date = $_GET['date'];
                  }

                  $style = '';

                  if($check_date == date('Y-m-d')){

                    $style = "style='margin-top:10px'";

                  }elseif(date('H:i:s') > "22:30:01" && 
                    $check_date == date('Y-m-d',strtotime('+1 day'))){

                    $style = "style='margin-top:10px'";

                  }
                  
                 ?>

          <div class="col button-fillter">
            <div style="color:white;">
              <a <?php if(!check_sub_menu(3)){echo "hidden";} ?> <?php echo $style; ?> onclick="showModalForm(0,0)" class="btn btn-pinterest btn-sm">
                <i class="fa fa-plus"></i> <i class="fa fa-truck"></i> Additional
              </a>
             
              <a <?php if(!check_sub_menu(2)){echo "hidden";} ?> href="<?php echo base_url()?>schedule/material_movement?week=<?php echo $week; ?>&year=<?php echo $year; ?>&DateStart=<?php 
                    $date = date_create($date_list[$a]); 
                    $df = date_format($date, 'Y/m/d');
                    echo $df;
                    ?>" class="btn btn-facebook btn-sm">
                <i class="fa fa-download"></i> Download Schedule
              </a>
            </div>
        </div>
      </div>

      <div class="row button-days" >
        
        <div class="col">
        <?php if($this->session->flashdata('ALERT') != ""){
          ?>
        <div class="alert alert-warning alert-dismissable">
          <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
          <b><?php echo $this->session->flashdata('ALERT'); ?></b>
        </div>
          <?php
        } ?>
        
          <?php
          $zz = 0;
          ?>   
                      <?php
          while($zz < count($date_list)){
            $dc = date_create($date_list[$zz]);
            $df = date_format($dc, "D d M Y");
            $dl = date_format($dc, "Y-m-d");
            
            ?>
           <button type="button" onclick="window.location=('<?php base_url() ?>schedule?year=<?php echo date('Y', strtotime($dl)); ?>&week=<?php echo date('W', strtotime($dl)); ?>&date=<?php echo date('Y-m-d', strtotime($dl)); ?>')" style="width: 130px;font-size:12px" class="btn btn-primary btn-sm <?php  if($dl == $c_date){echo "active";} ?>"><?php echo $df; ?></button>

            <?php
            $zz++;
          }
          ?>
        </div>
     </div>
  <!--  <div class="row" id="legends">
      <div class="col-md-1" style="font-size:10px">
        <?php LegendIcon(0) ?>
      </div>
      <div class="col-md-1" style="font-size:10px">
        <?php LegendIcon(1) ?>
      </div>
      <div class="col-md-1" style="font-size:10px">
        <?php LegendIcon(2) ?>
      </div>
      <div class="col-md-1" style="font-size:10px">
        <?php LegendIcon(3) ?>
      </div>
      <div class="col-md-1" style="font-size:10px">
        <?php LegendIcon(4) ?>
      </div>
      <div class="col-md-2" style="font-size:10px">
        <?php LegendIcon(5) ?>
      </div>
   </div> -->
   </div>
 </div>
      <div class="row">
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
              <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-truck"></i>
                </div>
                <p class="card-category" style="font-size:13px">Total Truck Bookings</p>
                <h3 class="card-title"><?php echo $total_booking; ?></h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  This day bookings : <?php echo $booking_day; ?>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
              <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-shipping-fast"></i>
                </div>
                <p class="card-category" style="font-size:13px">Total Truck Arrived</p>
                <h3 class="card-title"><?php echo $total_arrived; ?> </h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  This day truck arrived : <?php echo $arrived_day; ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
              <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-pallet"></i>
                </div>
                <p class="card-category" style="font-size:13px">Total Material (Pallet)</p>
                <h3 class="card-title"><?php  if(!empty($material_day) ){echo $total_material;}else{echo 0;} ?></h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  This day materials : <?php if(!empty($material_day) ){echo $material_day;}else{echo 0;} ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats" style="margin-bottom: 10px;margin-top:10px;height: 125px">
              <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-truck-loading"></i>
                </div>
                <p class="card-category" style="font-size:13px">Total Received (Pallet)</p>
                <h3 class="card-title"><?php  if(!empty($material_day) ){echo $total_received;}else{echo 0;} ?></h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  This day received : <?php  if(!empty($material_day) ){echo $received_day;}else{echo 0;} ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      <div class="row">
        <?php
        $a = 0;
        $card_header = ['primary','info','warning'];
        $bg_color = ['#ab47bc','#26c6da','#ffa726'];
        while($a < count($data_shift)){
          ?>
                    <div class="col-md-4">
                      <div class="card card-chart">
                        <div class="card-header card-header-<?php echo $card_header[$a]; ?> text-center" 
                        
                        ><b>
                           <?php echo $data_shift[$a]['shift_name'] ?></b>
                        </div>
                        <div class="card-body">
                <div class="table-schedule table-responsive">      
                  <table class="table table-hover">
                  <tr align="center" style="padding:0px">
                    <th rowspan="1" style="padding:0px" width="120px">Time Slot</th>
                    <th colspan="7" style="vertical-align: middle;padding:5px" >Schedule</th>
                  </tr>
                  <?php
                  $c_pop_time_slot = ''; 
                   $va = $this->session->userdata('sess_vendor_alias'); 
                  foreach($data_time_slot->result() as $get){
                    if($get->shift == $data_shift[$a]['shift_alias']){
                      $st = date_create($get->start_time);
                      $et = date_create($get->end_time);

                      $st = date_format($st, "H:i");
                      $et = date_format($et, "H:i");
                      ?>
                    <tr>
                     <?php
                      $check_popevent = $this->db->query("SELECT a.id,a.id_time_slot, a.title_event, a.event FROM tb_schedule_event AS a WHERE a.id_time_slot ='$get->id' AND a.date = '$c_date' ");

                      $style_bgevent = "style='background-color: ".$bg_color[$a].";vertical-align : middle;text-align:center;font-weight: 800';";
                      if($check_popevent->num_rows() == 0){
                          if(!check_sub_menu(6)){ 
                            
                            $pop_event = '';

                          }else{

                            $pop_event = "data-toggle='popover' data-placement='top' title='' data-content=\"<form action='".base_url()."schedule/pop_event' method='post'>
                            <div class='form-group'>
                            <input type='text'  hidden value='?year=".$_GET['year']."&week=".$_GET['week']."&date=".$c_date."' name='url'>
                             <input type='text' style='display:none' name='id_time_slot' value='".$get->id."'>
                             <input type='text' style='display:none' name='date_event' value='".$c_date."'>
                              <label>Title</label>
                              <input style='width:135px' required name='title' class='form-control'></input>
                            </div>
                            <div class='form-group'>
                              <label>Content</label>
                              <textarea style='width:135px' required name='content' class='form-control'></textarea>
                            </div>
                            <button class='btn btn-info form-control'>Add Event</button></form>\" data-trigger='click'";
                          }
                      }else{
                        $style_bgevent = "style='background-color:red;vertical-align : middle;text-align:center;font-weight: 800';";
                        if(!check_sub_menu(6)){ 
                            
                            $pop_event = 'data-toggle="popover" data-placement="top" title="'.$check_popevent->row()->title_event.'" data-content="'.$check_popevent->row()->event.'" data-trigger="hover" ';

                          }else{
                            $pop_event = "data-toggle='popover' data-placement='top' title='' data-content=\"<a href='".base_url()."schedule/cancel_event?id=".$check_popevent->row()->id."&year=".$_GET['year']."&week=".$_GET['week']."&date=".$c_date."'><button class='form-control btn btn-danger'>Cancel Event</button></a><form action='".base_url()."schedule/pop_event' method='post'>
                            <div class='form-group'>
                            <input type='text' hidden value='?year=".$_GET['year']."&week=".$_GET['week']."&date=".$c_date."' name='url'>
                             <input type='text' style='display:none' name='id_time_slot' value='".$get->id."'>
                             <input type='text' style='display:none' name='date_event' value='".$c_date."'>
                              <label>Title</label>
                              <input style='width:135px' required value='".$check_popevent->row()->title_event."' name='title' class='form-control'></input>
                            </div>
                            <div class='form-group'>
                              <label>Content</label>
                              <textarea style='width:135px' required name='content' class='form-control'>".$check_popevent->row()->event."</textarea>
                            </div>
                            <button class='btn btn-warning form-control'>Update Event</button></form>\" data-trigger='click'";
                          }                      
                      }

                       ?>

                      <td  onmouseover="" <?php echo $pop_event; ?> <?php echo $style_bgevent; ?> ><button style="display:none"></button><?php if($st == "00:01"){ $st = "24:01";} echo $st." - ".$et; ?></td>
                      <td width="5"></td>
                      <?php $count_dock = 0; ?>
                      <?php
                      if(count($data[$c_date][0]) > 0){
                        $index = array_keys(array_column($data[$c_date][0], "id_time_slot"), $get->id);
                        foreach($index as $g){
                      ?>
                      <td class="text-center pull-left" style="width: 75px;padding:0px;font-size:6.5px;background-color:#cacbce">
                        <?php
                          $id = $data[$c_date][0][$g]["id"];
                          $ts = $data[$c_date][0][$g]["id_time_slot"];
                          $sn = $data[$c_date][0][$g]["schedule_number"];
                          $vn = $data[$c_date][0][$g]["vendor_alias"]; 
                          $sh = $data[$c_date][0][$g]["shift"];
                          $time_now = date('H:i:s');
                          $tmat = $data[$c_date][0][$g]["TOTAL_MATERIAL"];
                          $status = $data[$c_date][0][$g]["status"];
                          $additional = $data[$c_date][0][$g]["is_additional"];
                          $vcc = $this->session->userdata('sess_vendor_alias');
                          $datenow = date('Y-m-d');
                          if($status == '0'){
                            if(!check_sub_menu(7)){
                              if($vn == $vcc){
                              ?>
                              <a class="truck"
                                data-toggle="popover"
                                data-content="<?php if($pop_event != ''){ ?> <button disabled class='btn btn-success btn-sm btcek btn-block'>SET PLAN</button> <?php }else{ ?> 
                                  <a href='<?php echo base_url().'schedule/view_schedule/'.$sn.'/'.$c_date.'/'.$ts.'/'.$week; if($additional == '1'){echo '/adt';} ?>' class='btn btn-success btn-sm btcek btn-block' >SET PLAN</a>
                                 <?php } ?>
                                <hr style='margin: 5px'>
                                <form action='<?php echo base_url().'schedule/change_schedule/'.$id.'/'.$c_date.'/'.$ts.'/'.$sh; ?>' method='post'>
                                  <div class='form-group'>
                                    <label>CHANGE SCHEDULE</label>
                                    <input type='hidden' value='<?php echo $year; ?>' name='yearz'>
                                    <input type='hidden' value='<?php echo $week; ?>' name='weekz'>
                                    <input type='hidden' value='<?php echo $c_date; ?>' name='date'>
                                    <select required name='ts' class='form-control'>
                                      <option value=''>--SELECT--</option>
                                      <?php
                                      foreach($data_time_slot2->result() as $d){
                                        $check_popevent = $this->db->query("SELECT a.id,a.id_time_slot, a.title_event, a.event FROM tb_schedule_event AS a WHERE a.id_time_slot ='$d->id' AND a.date = '$c_date' ")->num_rows();

                                        if($d->shift != $sh || $check_popevent > 0 || $time_now > $d->end_time && $datenow == $c_date){ continue; }
                                        $st = date_create($d->start_time);
                                        $et = date_create($d->end_time);
                                        $st = date_format($st, "H:i");
                                        $et = date_format($et, "H:i");
                                        ?>
                                      <option value='<?php echo $d->id ?>'><?php echo $st.' - '.$et; ?></option>
                                        <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                  <button class='btn btn-info btn-block' type='submit'>SAVE</button>
                                </form>

                                <form action='' method='post'>

                                </form>
                                "
                                data-placement="top"
                                onclick="checkDate('<?php echo $c_date; ?>', '<?php echo date('Y-m-d') ?>', '<?php echo $sh; ?>', '<?php echo $c_shift ?>','<?php echo $sn; ?>')"
                                >

                                <?php  truckIcon($status, $vn, $additional, $tmat, 0); ?>
                              </a>
                            <?php
                              }else{
                                truckIcon($status,'', '', $tmat, 1);
                              }
                            }else if(check_sub_menu(4))
                            { ?>
                               <a class="truck"
                                data-toggle="popover"
                                data-content="
                                <button class='btn btn-primary' onclick='getDI(<?php echo $sn; ?>)'>Schedule Information</button>
                                
                                <?php 
                                $check_popevent = $this->db->query("SELECT a.id,a.id_time_slot, a.title_event, a.event FROM tb_schedule_event AS a WHERE a.id_time_slot ='$get->id' AND a.date = '$c_date' ");

                                if($check_popevent->num_rows() > 0){?> <button style='margin-top:10px' disabled class='btn btn-success btn-sm btcek btn-block'>SET PLAN</button> <?php }else{?> 
                                  <a style='margin-top:10px' href='<?php echo base_url().'schedule/view_schedule/'.$sn.'/'.$c_date.'/'.$ts.'/'.$week; if($additional == '1'){echo '/adt';} ?>' class='btn btn-success btn-sm btcek btn-block'  >SET PLAN</a>
                                <?php } ?>

                                <hr style='margin: 5px'>
                                <form action='<?php echo base_url().'schedule/change_schedule/'.$id.'/'.$c_date.'/'.$ts.'/'.$vn.'/'.$sh; ?>' method='post'>
                                  <div class='form-group'>
                                    <label>CHANGE SCHEDULE</label>
                                    <input type='hidden' value='<?php echo $year; ?>' name='yearz'>
                                    <input type='hidden' value='<?php echo $week; ?>' name='weekz'>
                                    <input type='hidden' value='<?php echo $c_date; ?>' name='date'>
                                    <select name='ts' class='form-control'>
                                      <option value=''>--SELECT--</option>
                                      <?php
                                      foreach($data_time_slot2->result() as $d){
                                         $check_popevent = $this->db->query("SELECT a.id,a.id_time_slot, a.title_event, a.event FROM tb_schedule_event AS a WHERE a.id_time_slot ='$d->id' AND a.date = '$c_date' ")->num_rows();
                                        $st = date_create($d->start_time);
                                        $et = date_create($d->end_time);
                                        $st = date_format($st, "H:i");
                                        $et = date_format($et, "H:i");
                                        if($d->shift != $sh || $check_popevent > 0 || $time_now > $et && $datenow == $c_date){ continue; }
                                        
                                        ?>
                                      <option value='<?php echo $d->id ?>'><?php echo $st.' - '.$et; ?></option>
                                        <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                  <button class='btn btn-info btn-block' type='submit'>SAVE</button>
                                </form>

                                <form action='' method='post'>

                                </form>
                                "
                                data-placement="top"
                                  >
                                <?php  
                                truckIcon($status, $vn, $additional, $tmat); ?>
                              </a>
                            <?php }
                            else if(!check_sub_menu(4)){
                              ?>
                            
                                <a class="truck" onmouseout="" onmouseover="getDI(<?php echo $sn; ?>)">
                                  <?php truckIcon($status, $vn, $additional, $tmat); ?>
                                </a>
                             
                              <?php
                            }
                          }else if($status == '1'){
                            if(!check_sub_menu(7)){
                              if($vn == $vcc){
                
                              ?>

                                <a class="truck" onclick="getDetailedInformation(<?php echo $sn; ?>)">
                                  <?php truckIcon($status,$vn, $additional); ?>
                                </a>
                              <?php }else{ ?>
                                
                                  <?php truckIcon($status,'', $additional,$tmat, 1); ?>
                                

                              <?php
                               }
                            }
                            else if(check_sub_menu(5)){
                              ?>
                              <a class="truck" onclick="showModalForm(<?php echo $status ?>, <?php echo $sn ?>)">
                                <?php truckIcon($status, $vn, $additional); ?>
                              </a>
                            <?php
                            }else if(!check_sub_menu(5)){
                              ?>
                              <a class="truck" onclick="getDetailedInformation(<?php echo $sn; ?>)">
                                  <?php truckIcon($status,$vn, $additional); ?>
                                </a>
                              <?php
                            }
                            
                          }else if($status == '2'){
                            if(!check_sub_menu(7)){
                              ?>
                                  <?php if($vn == $vcc){
                
                               ?>
                                  <a class="truck" onclick="getDetailedInformation(<?php echo $sn; ?>)">
                                    <?php truckIcon($status, $vn, $additional); ?>
                                  </a>

                                <?php }else{ ?>
                                  
                                   
                                    <?php truckIcon($status,'', $additional, $tmat, 1); ?>
                                  
                              <?php
                              }
                            }
                            else if(check_sub_menu(5)){
                              ?>
                             
                                  <a class="truck" onclick="showModalForm(<?php echo $status ?>, <?php echo $sn ?>)">
                                    <?php truckIcon($status, $vn, $additional); ?>
                                  </a>
                               
                              <?php
                            }else if(!check_sub_menu(5)){
                              ?>
                              
                                  <a class="truck" onclick="getDetailedInformation(<?php echo $sn; ?>)">
                                    <?php truckIcon($status, $vn, $additional); ?>
                                  </a>
                              
                              <?php
                            }
                          }else if($status == 3){
                            if(!check_sub_menu(7)){
                              ?>
                                  <?php if($vn == $vcc){
                
                               ?>
                                  <a class="truck" onclick="getDetailedInformation(<?php echo $sn; ?>, <?php echo $status; ?>)">
                                    <?php truckIcon($status, $vn, $additional, $tmat); ?>
                                  </a>

                                <?php }else{ ?>

                                   
                                    <?php truckIcon($status,'', $additional, $tmat, 1); ?>
                                  
                               
                              <?php
                              }
                            }
                            else if(check_sub_menu(1)){
                              ?>
                              
                                  <a class="truck" onclick="getDetailedInformation(<?php echo $sn; ?>, <?php echo $status; ?>)">
                                    <?php truckIcon($status, $vn, $additional); ?>
                                  </a>                             
                              <?php
                            }
                          }
                          $count_dock++; 
                        }
                        
                        ?>
                      </td>
                      <?php
                      
                      }

                      while($count_dock < 3){
                        ?>
                      <td class="text-center pull-left" style="width:75px;padding:0px;font-size:6.5px;background-color:#cacbce">
                      </td>
                      <?php
                      $count_dock++;
                      }
                      ?>
                      <td class="text-center pull-left" style="width:75px;padding:0px;font-size:6.5px;background-color:#cacbce">
                      </td>

                    </tr>
                      <?php
                    }
                  }
                  ?>
                
              </table>
            </div>
          </div>
        </div>
      </div>

      <?php $a++;} ?>
              
            </div>

      <div class="row" style="margin-top:-40px">
        <div class="col-md-12">
              <div class="card ">
                <div class="card-header card-header-rose card-header-text">
                  <div class="card-text">
                    <h4 class="card-title">Legends</h4>
                  </div>
                </div>
                <div class="card-body ">
                  <table border="0">
                          <tr>
                            <td class="legends"><div class="icon1" ><?php LegendIcon(0) ?></div></td>
                            <td class="legends"><?php LegendIcon(1) ?></td>
                            <td class="legends"><?php LegendIcon(2) ?></td>
                            <td class="legends"><?php LegendIcon(3) ?></td>
                            <td class="legends"><?php LegendIcon(4) ?></td>
                            <td class="legends"><div class="icon1"><?php LegendIcon(5) ?></div></td>

                          </tr>
                        </table> 
                </div>
              </div>
            </div>
      </div>
      <!-- Modal -->
    <div class="modal" id="modal-schedule-detail">
      <div class="modal-dialog modal-sm" id='modal_size'>
        <div class="modal-content">
          <div class="modal-header" style="background-color:#31559F;color:white!important">
            <button class="close" data-dismiss="modal">&times;</button>
            <label class="modal-title" style="color:white">Option</label>
          </div>
          <?php 
            $hour_time = date('H');
            $minute_time = date('i');
          ?>
          <div class="modal-body">
            <button id="schedule_info" class="btn btn-primary panel-click form-control">Schedule Information</button>
            <div class="row" style="margin-top: 10px;">
              <div class="col-md-12">
                <div class="panel panel-primary panel-hide-all panel-arrived">
                  <div class="panel-body">
                    <h4 class="text-center">Set Arrival Time</h4>
                    <form action='schedule/arrived' style="text-align:center" method='POST'>
                        <label>Date Arrive</label>
                        <div class='form-group' align="center">                          
                          <div>
                          <p class="help-block">*Waktu penerimaan surat jalan dari driver</p>
                          <input type='text' id='arrived' hidden value='<?php echo $sn; ?>' name='id'>
                          <input type="text" id="arrived_id_time_slot" hidden value="<?php echo $time_slot_now; ?>" name="arrived_id_time_slot">
                          <input type='text' value='<?php echo '?year='.$_GET['year'].'&week='.$_GET['week'].'&date='.$_GET['date']; ?>' hidden name='url'>
                          <input class='form-control' <?php echo ($this->session->userdata('sess_role_no') == 1)?"":"readonly" ?> type='date' style="padding-left:40px;font-size:18px;height: 60px" value="<?php echo Date('Y-m-d'); ?>" name='arrive_date'> 
                          </div>
                        </div>
                        <div class='form-group' align="center">
                          <label>Time Arrive</label>
                          <div>
                            <input type="number" onclick="$(this).select()" name="arrived_hour" value="<?php echo $hour_time; ?>" class="hourz" style="font-size:20px" min="0" max="23" placeholder="00" <?php echo ($this->session->userdata('sess_role_no') == 1)?"":"readonly" ?>>:
                            <input type="number" name="arrived_minute" onclick="$(this).select()" value="<?php echo $minute_time; ?>" class="minutez" style="font-size:20px" min="0" max="59" placeholder="00" <?php echo ($this->session->userdata('sess_role_no') == 1)?"":"readonly" ?>>
                          </div>
                        </div>
                        <div class='form-group'>
                          <button class='btn btn-primary text-center'>Save</button>
                        </div>
                    </form>
                  </div>
                </div> 
                <div class="panel panel-primary panel-hide-all panel-received">
                  <div class="panel-body" align="center">

                      <form action='schedule/received' method='POST' enctype="multipart/form-data">
                    <div class="container-fluid">

                      <div class="row">
                        <div class="col-md-9">
                          <div class="table-responsive return_form">
                          </div>
                         </div>
                         <div class="col-md-3">
                    <h4 class="text-center">Set Received Time</h4>
                     <label>Date Received</label>
                    <p class="help-block">*Waktu penyerahan receiver ticket ke driver</p>
                    
                      <input type='text' id='received' value='<?php echo $sn; ?>' hidden name='id'>
                      <input type='text' value='<?php echo '?year='.$_GET['year'].'&week='.$_GET['week'].'&date='.$_GET['date']; ?>' hidden name='url'>
                        <div class='form-group' align='center'>
                         
                          <input class='form-control' <?php echo ($this->session->userdata('sess_role_no') == 1)?"":"readonly" ?> type='date' value="<?php echo Date('Y-m-d'); ?>" style="padding-left:40px;font-size:13px;height: 60px" name='received_date'>
                        </div>
                        <div class='form-group' align='center'>
                          <label>Time Received</label>
                         
                          <div>
                                <input type="number" name="received_hour" value="<?php echo $hour_time; ?>" class="hourz" style="font-size:14px" min="0" max="23" placeholder="00" <?php echo ($this->session->userdata('sess_role_no') == 1)?"":"readonly" ?>>:
                                <input type="number" name="received_minute" value="<?php echo $minute_time; ?>" class="minutez" style="font-size:14px" min="0" max="59" placeholder="00" <?php echo ($this->session->userdata('sess_role_no') == 1)?"":"readonly" ?>>
                              </div>
                          
                        </div>
                        <p class="help-block">Pastikan format file adalah JPG|PNG|BMP|GIF  </p>
                              <input  type="file" name="reject_file" class="form-control btn btn-primary" >
                        <div class="form-group" align="center">
                              
                              <textarea required="true" placeholder="Reason" name="reason" class="form-control"></textarea>
                            </div>
                          
                        <div class='form-group' align='center'>
                          <button class='btn btn-success text-center btn-sm form-control'> Save</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
                <!-- -->

                <div class="panel card-primary panel-hide-all panel-adt_truck">
                  <div class="panel-body">
                    <form action='schedule/adt_truck' method='POST'>
                      <div class="form-grup">
                        <label>Tanggal</label>
                        <input type="date" name="date" readonly value="<?php echo $c_date; ?>" class="form-control bg-primary" name="">
                      </div><br>
                      <input type='text' value='<?php echo '?year='.$_GET['year'].'&week='.$_GET['week'].'&date='.$_GET['date']; ?>' hidden name='url'>
                      <input type="text" name="week" hidden="" value="<?php echo $_GET['week'] ?>">
                       <input type="text" name="date" hidden="" value="<?php echo $_GET['date'] ?>">
                      <div class="form-group">
                        <label>Time Slot</label>
                      </div>
                        <select required="true" name="time_slot" class="form-control">
                          <option value=''>--SELECT--</option>
                            <?php
                            $sisa = 3;
                            foreach($ts_sisa as $d){

                              $st = date_create($d->start_time);
                              $et = date_create($d->end_time);
                              $st = date_format($st, "H:i");
                              $et = date_format($et, "H:i");

                              ?>
                            <option <?php if($d->sisa == 0){echo "disabled";} ?> value='<?php echo $d->id ?>'><?php echo $st.' - '.$et ." ({$d->sisa} slot tersisa)"; ?></option>
                              <?php
                            }
                            ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Vendor Code</label>
                        <?php if($this->session->userdata('sess_role_no') != 3){ ?>
                        <select name="vendor" class="form-control" style="font-size:12px">
                          <option>--SELECT--</option>
                          <?php foreach ($vendor->result() as $get) {
                            ?>
                            <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code.' '.$get->vendor_alias; ?></option>
                            <?php 
                          } ?>
                        </select>
                      <?php }else{ ?>
                          <option class="form-control" value="<?php echo $this->session->userdata('sess_vendor_code') ?>"><?php echo $this->session->userdata('sess_vendor_code') ?></option>
                      <?php } ?>
                      </div>
                      
                      <div class="form-group">
                        <label>Reason</label>
                        <textarea name="reason" class="form-control" placeholder="Alasan untuk menambah truck"></textarea>
                      </div>

                      <br>
                        <div class='form-group' align='center'>
                          <button class='btn btn-success text-center form-control'> Save</button>
                        </div>
                    </form>
                  </div>
                </div>
                <!--  -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
      <!-- End Modal -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
       

