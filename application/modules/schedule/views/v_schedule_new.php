<?php
      $no = 0;
      foreach($data_list->result() as $get){
        ?>
      <tr>
        <td class="text-center">
          <?php 
          echo $get->po_number;
          if($get->reference_po != ""){
            echo " / ".$get->reference_po;
          }
          ?>
          <input type="text" hidden value="<?php echo $get->idz; ?>" id="idz<?php echo $no; ?>" ></td>
        <?php
        if($category == "IBD"){
          ?>
        <td>
          <?php echo $get->bill_of_lading; ?>
        </td>
          <?php
        }
        ?>
        <td><?php echo $get->material_code ?><input type="text" hidden value="<?php echo $get->material_code; ?>" id="material_code" ></td>
        <td><?php echo $get->material_name ?></td>
        <td>
          <?php
          $r_date = date_create($get->requested_delivery_date);
          $df = date_format($r_date, "d-m-Y");
          echo $df." / ".$get->shift
          ?>
        </td>
        <td width="35px">
          <input type="text" id="quantity<?php echo $no; ?>" hidden value="<?php echo $get->quantity; ?>" name="">
          <input type="text" id="id<?php echo $no; ?>" hidden value="<?php echo $get->id; ?>" name="">
          <input type="text" class="form-control inp-pallet" value="<?php echo $get->quantity ?>" onchange="cutPallet($('#id<?php echo $no; ?>').val(), this.value, <?php echo $get->idz ?>, $('#quantity<?php echo $no;?>').val(),'quantity<?php echo $no; ?>' ) ">
          <input type="text" hidden value="<?php echo $get->quantity; ?>" id="qty_material<?php echo $no++; ?>" >
        </td>
        <td><?php echo ($get->uom_plt * $get->quantity)." ".$get->uom; ?></td>
        
         <td>
          <input type="date" data-date-format="YYYY MM DD" value="<?php echo $get->prod_date ?>" style="width: 130px" value="" onchange="exp_date(this.value, <?php echo $no; ?>, <?php echo $get->id; ?>)" name="prod_date" class="form-control prod_date">
        </td>
        <td>
          <input type="text"  hidden value="<?php echo $get->exp; ?>" name="exp" id="exp<?php echo $no; ?>">
          <input readonly value="<?php echo $get->exp_date; ?>" id="exp_date<?php echo $no; ?>" style="background-color:white; width: 130px" type="date" name="exp_date" class="form-control">
        </td>
        <td>
          <input type="text" maxlength="10" id="<?php echo $get->id; ?>" value="<?php echo $get->vendor_batch; ?>" name="vendor_batch[]" onchange="update_vendor_batch(this.value, $(this).attr('id') )" class="form-control vendor_batch">
        </td>
        <td>
          <input type="text" class="form-control i_do_number" data-id="do_<?php echo $get->id ?>" value="<?php echo $get->do_number ?>">
        </td>
        <td class="div-dn area-dn-<?php echo $get->id ?> <?php echo ($get->invoice != '')?'bg__green':'' ?>">
          <button class="btn btn-primary btn-sm" type="button" onclick="$('#file_<?php echo $get->id ?>').click();">
            <i class="fa fa-upload"></i>
          </button>
          <input type="file" name="upload_dn" id="file_<?php echo $get->id ?>" style="display: none;" onchange="submit_file('DN', <?php echo $get->id ?>, '<?php echo $get->schedule_number ?>')">
        </td>
        <td class="div-coa area-coa-<?php echo $get->id ?> <?php echo ($get->coa != '')?'bg__green':'' ?>">
          <button class="btn btn-warning btn-sm" type="button" onclick="$('#file_coa_<?php echo $get->id ?>').click();">
            <i class="fa fa-upload"></i>
          </button>
          <input type="file" name="upload_dn" id="file_coa_<?php echo $get->id ?>" style="display: none;" onchange="submit_file('COA', <?php echo $get->id ?>, '<?php echo $get->schedule_number ?>')">
        </td>
        <td class="text-center" style="width: 10px">
          <button class="btn btn-link clr-red btn-sm" onclick="moveMaterial(<?php echo $get->id; ?>, '<?php echo $sn; ?>')">
            <i class="fa fa-times"></i>
          </button>
        </td>
        
      </tr>
<?php } ?>
