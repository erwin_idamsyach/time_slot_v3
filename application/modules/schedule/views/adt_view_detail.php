<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        Additional View Schedule Detail<br>
        <a href="<?php echo base_url()."schedule/?week=".$this->uri->segment(6)."&date=".$this->uri->segment(4); ?>" class="btn btn-info">
          <i class="fa fa-arrow-left"></i> BACK
        </a>
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
      <div class="row">
        <div class="col-md-8">
          <div class="box">
            <div class="box-header">
              <label>Material List</label>
              <h4 id="area-cnt-pallet">Slot Occupancy : .../32</h4>
            </div>
            <div class="box-body">
              <table class="table table-striped table-hover">
                <thead>
                  <tr>
                    <th class="text-center">PO Number</th>
                    <th class="text-center">Material Code</th>
                    <th class="text-center">Material Name</th>
                    <th class="text-center">Date / Shift</th>
                    <th class="text-center">Req. Pallet</th>
                    <th class="text-center">Send Qty</th>
                    <th class="text-center">*</th>
                  </tr>
                </thead>
                <tbody id="area-list">
                  <?php
                  $no = 0;
                  foreach($data_list->result() as $get){
                    ?>
                  <tr>
                    <td class="text-center"><?php echo $get->po_number ?><input type="text" hidden value="<?php echo $get->idz; ?>" id="idz<?php echo $no; ?>" ></td>
                    <td><?php echo $get->material_code ?><input type="text" hidden value="<?php echo $get->material_code; ?>" id="material_code" ></td>
                    <td><?php echo $get->material_name ?></td>
                    <td>
                      <?php
                      $r_date = date_create($get->requested_delivery_date);
                      $df = date_format($r_date, "d-m-Y");
                      echo $df." / ".$get->shift
                      ?>
                    </td>
                    <td width="35px">
                      <input type="text" class="form-control inp-pallet" value="<?php echo $get->quantity ?>" onchange="cutPallet(<?php echo $get->id ?>, this.value, <?php echo $get->idz ?>)">
                      <input type="text" hidden value="<?php echo $get->quantity; ?>" id="qty_material<?php echo $no++; ?>" >
                    </td>
                    <td><?php echo ($get->uom_plt * $get->quantity)." ".$get->uom; ?></td>
                    <td class="text-center">
                      <button class="btn btn-link clr-red" onclick="moveMaterial(<?php echo $get->id; ?>, <?php echo $sn; ?>)">
                        <i class="fa fa-times"></i>
                      </button>
                    </td>
                    
                  </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <label>Material Pool</label>
                </div>
                <div class="box-body" id="area-pool">
                  
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="box">
                <div class="box-header">
                  <label>Find From Same RDD</label>
                </div>
                <div class="box-body" id="area-same-rdd"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box">
            <div class="box-header">
              <label>Delivery Form</label>
            </div>
            <div class="box-body">
              <form action="<?php echo base_url(); ?>schedule/save_schedule" method="post" enctype="multipart/form-data">
                <input type="hidden" name="slot_number" value="<?php echo $this->uri->segment(3); ?>">
                <div class="form-group">
                  <label>RDD</label><br>
                  <label>
                    <?php
                  $date = date_create($this->uri->segment(4));
                  $dfns = date_format($date, "D, d M Y");
                  echo $dfns;
                    ?>
                  </label>
                </div>
                <div class="form-group">
                  <label>Time Slot</label><br>
                    <label>
                      <?php
                        $this->Schedule_model->getTimeSlotById($this->uri->segment(5));
                      ?>
                    </label>
                </div><hr>
                <div class="form-group">
                  <label>Truck No.</label>
                  <input type="text" name="truck_no" class="form-control" required="">
                </div>

                <div class="form-group">
                  <label>Driver Name</label>
                  <input type="text" name="driver_name" class="form-control" required="">
                </div>

                <div class="form-group">
                  <label>Phone Number</label>
                  <input type="text" name="phone_number" class="form-control" required="">
                </div><hr>

                <div id="delivery_notes">
                  <label>Delivery Note</label>
                  <?php echo "<b style='color:red'>".$this->session->flashdata('ERR')."</b>"; ?>
                      <div class="input-group">                      
                          <input type="text" name="do_number1" class="form-control" required="">
                          <input type="file" name="file1" class="input-group-addon form-control" required="">
                          <p class="help-block">Pastikan format file adalah .DOCX atau .PDF</p>
                      </div>
                  
                </div>
               <button type="button" style="margin:5px;margin-bottom:5px" class="btn btn-primary" onclick="addDN()"><span class="fa fa-plus"></span></button>
                <input hidden type="text" id="dn_count" value="1" name="dn_count">
                <hr>
                <div id="CoA">
                  <div class="form-group">
                    <label>Upload CoA</label>
                    <?php echo "<b style='color:red'>".$this->session->flashdata('ERR2')."</b>"; ?>
                    <input type="file" name="coa1" class="form-control" required="">
                    <p class="help-block">Pastikan format file adalah .DOCX atau .PDF</p>
                  </div>
                </div>
                <button type="button" style="margin:5px;margin-bottom:5px" class="btn btn-warning" onclick="addCOA()"><span class="fa fa-plus"></span></button>
                <input hidden type="text" id="coa_count" value="1" name="coa_count">
                <br><br>
                <div class="form-group">
                  <input type="hidden" name="id_schedule" value="<?php echo $this->uri->segment(3) ?>">
                  <input type="hidden" name="rdd" value="<?php echo $this->uri->segment(4) ?>">
                  <input type="hidden" name="id_time_slot" value="<?php echo $this->uri->segment(5) ?>">
                  <button class="btn btn-primary">SUBMIT</button>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
</div>