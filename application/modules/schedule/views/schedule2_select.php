<style type="text/css">
	table th{
		background-color: #96b5ff!important;
		text-align: center;
		font-size:12px!important; 
		font-weight:800!important;
		justify-content: center;
  		flex-direction: column;
	}

table td 
{
	white-space: nowrap;
	margin: 0px;
	padding: 0px!important;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: middle;
    border-top: 1px solid #ddd;
}

.table-responsive{
	margin-top:-20px;
}

	input {
	font-size:12px;
    width: 100%;
    
		}
}
</style>


<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">dashboard</i>
                    </div>
                    <h4 class="card-title">Schedule Dashboard</h4>
                  </div>
                  <div class="card-body ">
    <!-- Content Header (Page header) -->

	<section class="content" style="padding:10px;">
		<div class="box">
		<div class="box-body" style="overflow: unset;">

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-5">               
					<button class="btn btn-primary" onclick="sumbitForm();"><i class="fa fa-book"></i> SAVE</button>
					<button class="btn btn-warning" onclick="clear_all();"><i class="fa fa-paint-brush" ></i> CLEAR ALL</button>
					<!-- <button <?php if($this->session->userdata('sess_role_no') != 1){ echo "style='display:none'";} ?> class="btn btn-danger" onclick="delete_schedule2()"><i class="fa fa-trash"></i> DELETE ALL</button> -->
					
					 <button class="btn btn-danger" <?php echo ($this->session->userdata('sess_role_no') == 3)?'style="display: none"':'' ?> onclick="check_delete()"><i class="fa fa-trash"></i> DELETE</button>
					<b hidden="true" id="saving" style="color:green">Saving...</b><b hidden="true" id="deleting" style="color:red">Deleting..</b>
					<img hidden="true" id="loading" src="<?php echo base_url()?>assets/images/loading.gif" style="width: 30px;height: 30px">
				</div>
			</div>

			<div class="row">
				<div>
					<form method="get" action="schedule2_search?category<?php echo $_GET['category']; ?>&year=<?php echo $_GET['year']; ?>&week=<?php echo $_GET['week']; ?>">
					<input type="text" hidden value="<?php echo $_GET['category']; ?>" name="category">
					<input type="text" hidden value="<?php echo $_GET['week']; ?>" name="week">
					<input type="text" hidden value="<?php echo $_GET['year']; ?>" name="year">
				</div>

				<div class="col-md-3">
					<select name="menu" id="#menu" class="form-control text-center" onchange="select_menu(this.value)">
						<option value="">-- SELECT ROWS --</option>
						<option value="material_code">-- ITEM CODE --</option>
						<option value="material_name">-- ITEM DESC --</option>
						<option value="uom_plt">-- QTY/PLT --</option>
						<option value="plt_truck">-- PLANT --</option>
						<option value="po_number">-- PO DOC --</option>
						<option value="po_line_item">-- LINE --</option>
						<option value="qty">-- ORDER QTY --</option>
						<option value="uom">-- UOM --</option>
						<option value="requested_delivery_date">-- DATE --</option>
						<option value="shift">-- SHIFT --</option>
					</select>
				</div>

				<div class="col-md-3" id="item_data" hidden="true">
					<select disabled="true" name="item_list" class="form-control"  style="margin-left:50px">
						<option value="">-- SELECT ITEM --</option>
					</select>
				</div>

				<div class="col-md-2">
					<select name="supplier" id="supplier" class="form-control">
                                <option value=''>-- SELECT SUPPLIER --</option>
                                <?php
                                foreach($supplier->result() as $get){
                                    ?>
                                <option value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code." - ".$get->vendor_name; ?></option>
                                    <?php
                                }
                                ?>
                     </select>
                     
				</div>
				<div class="col-md-1">
					<button class="btn btn-primary" style="margin-left:-20px"><span><i class="fa fa-search"></i></span></s></button>
				</div>
				</form>	        
			</div>

			<br>
			<?php
			if($this->session->flashdata('DELETED') != ""){
				?>
			<div class="alert alert-error alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				<b><?php echo $this->session->flashdata('DELETED'); ?></b>
			</div>
				<?php
			}
			?>

			<?php
			if($this->session->flashdata('SAVED') != ""){
				?>
			<div class="alert alert-info alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				 <b><?php echo $this->session->flashdata('SAVED'); ?></b>
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_ERROR') != ""){
				?>
			<div class="alert alert-error alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				Error! Perhatikan lagi data yang diupload dengan nomor PO <b><?php echo $this->session->flashdata('LIST_ERROR') ?></b>
			</div>
				<?php
			}

			if($this->session->flashdata('LIST_NOACT') != ""){
				?>
			<div class="alert alert-warning alert-dismissable">
				<button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
				Jadwal pengiriman dengan nomor PO <b><?php echo $this->session->flashdata('LIST_NOACT') ?></b> tidak berubah, material sedang dikirim / sudah diterima.
			</div>
				<?php
			}
			?>
			<br>
			<div class="row">
				<div class="col-md-4">
					<ul class="nav nav-pills" style="margin-left:-15px;margin-top:0px;">
						<li class="btn btn-info" <?php if(null !== $_GET['category'] && $_GET['category'] == 'FACE'){echo "class='active'";} ?>><a href="<?php echo base_url() ?>schedule/schedule2?category=FACE&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>">FACE</a></li>
						<li class="btn btn-info" <?php if(null !== $_GET['category'] && $_GET['category'] == 'BODY'){echo "class='active'";} ?>><a href="<?php echo base_url() ?>schedule/schedule2?category=BODY&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>">BODY</a></li>
						<li class="btn btn-info" <?php if(null !== $_GET['category'] && $_GET['category'] == 'PWL'){echo "class='active'";} ?>><a href="<?php echo base_url() ?>schedule/schedule2?category=PWL&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>">PWL</a></li>
						<li class="btn btn-info" <?php if(null !== $_GET['category'] && $_GET['category'] == 'DEO'){echo "class='active'";} ?>><a href="<?php echo base_url() ?>schedule/schedule2?category=DEO&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>">DEO</a></li>
					</ul>
				</div>
				<div class="col-md-3">
					<a href="<?php echo base_url() ?>schedule/schedule2?category=<?php echo $_GET['category'] ?>&year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>"><button class="btn btn-success" style="margin-left:-30px;"><span class="fa fa-table"> ON</span></button></a>
				</div>
				<div class="col-md-5">
							<div class="form-inline" style="margin-top:-5px">
				              	<button type="button" class="btn btn-info" style="" onclick="setYear_sch($('#year_sch').val(), 'DEC')">
				                  <i class="fa fa-chevron-left"></i>
				                </button>
				                <div class="form-group">
				                <input type="number" class="form-control" onchange="(setYear2_sch())"  id="year_sch" style="width: 70px; text-align: center; font-size: 20px" value="<?php echo (null === $_GET['year'])?date('Y'):$_GET['year']; ?>">
				            	</div>

				                <button type="button" class="btn btn-info" style="" onclick="setYear_sch($('#year_sch').val(), 'INC')">
				                  <i class="fa fa-chevron-right" ></i>
				                </button>

				                <button type="button" class="btn btn-info" style="" onclick="setWeek_sch($('.week_sch').val(), 'DEC')">
				                  <i class="fa fa-chevron-left"></i>
				                </button>
				                
				                <input onchange="setWeek_sch(($('.week_sch').val())-1, 'INC')" type="number" class="form-control week_sch" style="width: 60px; text-align: center; font-size: 20px" value="<?php echo (null === $_GET['week'])?date('W'):$_GET['week']; ?>">
				                <button type="button" class="btn btn-info" style=""onclick="setWeek_sch($('.week_sch').val(), 'INC')">
				                  <i class="fa fa-chevron-right" ></i>
				                </button>
		            		</div>
							
				</div>
			</div>
		</div>
	</div>
		
		<div>
			<div class="container-fluid">
			<div class="row">
				
			</div>
			<div class="row">
				<form id="idSchTbl" action="<?php echo base_url(); ?>schedule/save_schedule2?category=<?php echo $_GET['category'] ?>" method="post">
				<input type="hidden" name="yyy" value="<?php echo (null === $_GET['year'])?date('Y'):$_GET['year']; ?>">
				<input type="hidden" name="www" value="<?php echo (null === $_GET['week'])?date('W'):$_GET['week']; ?>">
			<div class="table-schedule table-responsive" style="height: 500px; width: 100%; overflow-y: scroll; overflow-x: scroll;">
				<br>
			<table border="1" id="schedule_table" class="table table-hover" style="">
				<th style="font-size:12px; font-weight:800"ead>
					<tr>
								
								<th style="font-size:12px; font-weight:800" rowspan="2" width="45"><b disabled value="Category" style="height: 50px" />Category</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="80"><b disabled value="Vendor/Supplier" style="height: 50px" />Vendor</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="100"><b disabled value="Item Code" style="height: 50px;" />Item Code</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="300"><b disabled value="Item Description" style="height: 50px;"/>Item Description</th>
								
								<th style="font-size:12px; font-weight:800" rowspan="2" width="60"><b disabled value="Plant" style="height: 50px"/>Plant</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="100"><b disabled value="Purchase Order Doc." style="height: 50px"/>Purchase Order Doc.</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="45"><b disabled value="Line" style="height: 50px"/>Line</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="60"><b disabled value="Order Qty" style="height: 50px"/>Order Qty</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="20"><b disabled value="UOM" style="height: 50px"/>UOM</th>
								<th style="font-size:12px; font-weight:800" colspan="2"><b disabled value="Revision Del.Plan" style="width: 260px;height: 25px"/>Revision Del.Plan</th>
								<th style="font-size:12px; font-weight:800" rowspan="2" width="60"><b disabled value="Qty per Pallet" style="height: 50px"/>Qty Per Pallet</th>
							</tr>
							<tr>
								<th style="font-size:12px; font-weight:800" width="110"><b disabled/>Date</th>
								<th style="font-size:12px; font-weight:800" width="40"><b disabled/>Shift</th>
							</tr>
				</thead>
				<tbody>
				
					<?php $row=0;
					$i=0;
					foreach ($data->result() as $get) {
					 $col = 0;
					?>
				<tr>
					
					<td width="85" align="center"><?php echo $get->category; ?></td>

					<td width="80" align="center"><?php echo $get->vendor_code; ?></td>

					<td width="100"><?php echo $get->material_code; ?></td>

					<td width="300"><?php echo $get->material_name; ?></td>

					

					<td width="60"><?php echo $get->plt_truck; ?></td>

					<td width="100"><?php echo $get->po_number; ?></td>

					<td width="45" align="center"><?php echo $get->po_line_item; ?></td>

					<td width="60"><?php echo $get->qty; ?></td>

					<td width="35" align="center"><?php echo $get->uom; ?></td>

					<td width="110"><?php echo Date('d-m-Y',strtotime($get->requested_delivery_date)); ?></td>

					<td  width="40" align="center"><?php echo $get->shift; ?></td>

					<td width="60"><?php echo $get->uom_plt; ?></td>
					</form>
				</tr>
				
				<?php $i++; $row++; } ?> 
				

				
				</tbody>
				
				  
			</table>
			</div>
		</div>
		</div>
	</div>

		<table id="header-fixed"></table>
		<img/><div id="json"></div>          
		</div>
	</section>


</div>