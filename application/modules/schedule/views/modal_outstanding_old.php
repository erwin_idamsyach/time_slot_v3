<div class="row">
	<div class="col">
	</div>
	<div class="col-2">
		<select class="form-control select-2-init" name="vendor_code" onchange="check_outstanding(this.value)" class="form-control">
			<?php $no = 0; foreach ($vendor_list as $get) {
				?>
				<option <?php echo ($vendor == $get->vendor_code)?"selected":""; ?>  value="<?php echo $get->vendor_code; ?>"><?php echo $get->vendor_code.'-'.$get->vendor_alias; ?></option>
				<?php
			} ?>
		</select>
		<br>
	</div>
</div>
<?php $data= array_chunk($data_pool,10);
?>
<div class="row" style="text-align: center">
	<div class="col text-center">
		<?php $total = ceil(count($data_pool) / 10); 
		?>
		<ul class="nav nav-pills nav-pills-icons" style="text-align:center" role="tablist">
			<?php for($i = 1 ; $i <= $total ; $i++){ ?>
		    <li class="nav-item">
		        <a class="nav-link <?php if($i == 1){echo "active";} ?>" href="#oustanding-<?php echo $i; ?>" role="tab" data-toggle="tab">
		            <?php echo $i; ?>
		        </a>
		    </li>
		    <?php } ?>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col">
		

		<div class="tab-content tab-space">
		    <?php for($i = 0 ; $i < $total ; $i++){ ?>
		    <div class="tab-pane <?php if($i == 0){echo "active";} ?>" id="oustanding-<?php echo $i+1; ?>">
		    	<div class="container-fluid">
			    	<div class="row">
					<?php foreach ($data[$i] as $get) {
						?>
						
							<div class="col-4" style="margin:0px;padding:0px">
								<div class="card" style="max-width: 18rem;">
								  <div class="card-header" style="background-color:#2a3f6c;color:white">
								    PO : <?php echo $get->po_number; ?> 
								    Line : <?php echo $get->po_line_item; ?></div>
								  <div class="card-body">
								    <p class="card-text">
								    Material Code : <?php echo $get->material_code; ?><br>
								    RDD : <?php echo $get->requested_delivery_date; ?> Shift : <?php echo $get->shift; ?> <br>
								    Req.Qty : <?php echo ($get->sisa > 0)?$get->sisa*$get->uom_plt:$get->qty; ?><br>
								    
									</p>
								  </div>
								</div>
							</div>	
						<?php 
					} ?>
					</div>
				</div>
		    </div>
		    <?php } ?>
		</div>
	</div>
</div>