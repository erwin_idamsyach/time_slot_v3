<style>
	th{
		text-align:center;
	}
	td{
		text-align:center!important;
	}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col">
			<div class="table-responsive">
			<table class="table table-bordered data-table" id="table" width="100%">	  
			  <thead class="" style="color:white; background-color:#31559F;font-weight: 650">
			    <tr>
			      <th >No</th>
			      <th >Category</th>
			      <th >Material Code</th>
			      <th >Vendor</th>
			      <th >RDD</th>
			      <th >Shift</th>
			      <th >Qty</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php $no = 1; foreach($data_pool as $data){ ?>
			    <tr>
			      <td><?php echo $no++; ?></td>
			      <td><?php echo $data->category; ?></td>
			      <td><?php echo $data->material_code; ?></td>
			      <?php 
			      $vendor_alias = "";
			      $dataz = $this->db->query("SELECT vendor_alias FROM skin_master.ms_supplier WHERE vendor_code = '$data->vendor_code' ")->row();
			      if(isset($dataz->vendor_alias)){
			      	$vendor_alias = $dataz->vendor_alias;
			      }
			      ?>
			      <td><?php echo $data->vendor_code."(".$vendor_alias.")"; ?></td>
			      <td><?php echo $data->requested_delivery_date; ?></td>
			      <td><?php echo $data->shift; ?></td>
			      <td><?php echo ($data->sisa > 0)?$data->sisa*$data->uom_plt:$data->qty; ?></td>
			      
			    </tr>
				<?php } ?>
			  </tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<script>
	$("#table").DataTable({
			"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'		
		});
</script>