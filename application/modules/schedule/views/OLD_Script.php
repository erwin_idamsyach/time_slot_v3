<script>
	$(document).ready(function(){
		var link = "<?php echo $this->uri->segment(2); ?>";
		if(link == "view_schedule"){
			callOffPallet();
		}
	});
	var c_minus = 0;
	var vendor_code = "<?php if(isset($vendor_code)){echo $vendor_code;}else{} ?>";
	var sn = "<?php echo (isset($sn))?$sn:'-'; ?>";
	var rdd = "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):'' ?>";

	function callOffPallet(){
		var a = 0;
		$(".inp-pallet").each(function(){
			var as = parseInt(this.value);
			a = a+as;
		});
		c_minus = 32-a;
		$("#area-cnt-pallet").html("Slot Occupancy : <b>"+a+" / 32</b>");

		getSuggestion(c_minus, sn);
		getMaterialPool(sn);
	}
	

	function editModal(){
		$('.unedit').hide();
		$('.edit').show();
		$('#EditBtn').hide();
		$('#Cancle').show();
		$('#editp').html('Cancel');
	}

	function Cancel(){
		$('.unedit').show();
		$('.edit').hide();
		$('#EditBtn').show();
		$('#Cancle').hide();
		$('#editp').html('Edit');
	}


	function moveMaterial(id, sn){

		$.ajax({
			type : "GET",
			url  : "<?php echo base_url() ?>schedule/move_schedule",
			data : {
				'id' : id
			},
			success:function(resp){
				getNewScheduleList(sn);
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}

	function getNewScheduleList(sn){
		$.ajax({
			type : "GET",
			url  : "<?php echo base_url() ?>schedule/get_schedule_list",
			data : {
				'sn' : sn
			},
			success:function(resp){
				$("#area-list").html(resp);
				callOffPallet();
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}


	function getDetailedInformation(key){
		$(".modal").modal('hide');
		setTimeout(function(){
			$.ajax({
				type : "GET",
				url : "<?php echo base_url() ?>schedule/get_detailed_information",
				data : {
					'key' : key
				},
				success:function(resp){
					openModal("Information", resp, "lg");
					editTime($("#created_at").val());
				},
				error:function(e){
					alert("Something wrong!");
					console.log(e);
				}
			})
			preventDefault();
		}, 200);	
	}

	function getDI(key){
		$(".modal").modal('hide');
		setTimeout(function(){
			$.ajax({
				type : "GET",
				url : "<?php echo base_url() ?>schedule/get_DI",
				data : {
					'key' : key
				},
				success:function(resp){
					openModal("Information", resp, "lg");
					editTime($("#created_at").val());
				},
				error:function(e){
					alert("Something wrong!");
					console.log(e);
				}
			})
			preventDefault();
		}, 200);	
	}

	function getSuggestion(c_min, sn){
		$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>schedule/get_suggestion",
			data : {
				'pallet_minus' : c_min,
				'sn' : sn,
				'rdd' : "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):"123" ?>"
			},
			success:function(resp){
				$("#area-same-rdd").html(resp);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})
	}

	function getMaterialPool(sn){
		
		$.ajax({
			type : "GET",
			url  : "<?php echo base_url() ?>schedule/get_pool_schedule_list",
			data : {
				'sn' : sn,
				'rdd' : "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):"123" ?>",
			},
			success:function(resp){
				$("#area-pool").html(resp);
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}

	function addMaterial(id, sn){

		$.ajax({
			type : 'GET',
			url : "<?php echo base_url() ?>schedule/add_material",
			data : {
				'id' : id,
				'sn' : sn
			},
			success:function(resp){
				getNewScheduleList(sn);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		});
	}

	function addMaterialPool(id, sn){
		$.ajax({
			type : 'GET',
			url : "<?php echo base_url() ?>schedule/add_material_pool",
			data : {
				'id' : id,
				'sn' : sn,
				'slot' : "<?php echo $this->uri->segment(5); ?>"
			},
			success:function(resp){
				getNewScheduleList(sn);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		});
	}

	function changeURL(){
		var dapick = $(".dapick").val();
		window.location=("<?php echo base_url() ?>schedule?date_start="+dapick);
	}

	$(".btcek").on('click', function(e){
		alert("UWUWEWE");
		e.preventDefault();
		return false;
	});

	$('.bt-res').on('click', function(){
		alert("YESSS");
	});

	function setWeek(week, state){
		/*alert(week+" "+state);*/
		var year = $('#year').val();
		if(state == "INC"){
			week = parseInt(week)+1;
			$('.week').val(week);
		}else{
			week = parseInt(week)-1;
			$('.week').val(week);
		}
		window.location=('<?php base_url() ?>schedule?year='+year+'&week='+week);
	}


	function setYear2(year, state){
			var year = $('#year').val();
			var week = $('.week').val();
			window.location=('<?php base_url() ?>schedule?year='+year+'&week='+week);
	}


	function setYear(year, state){
		/*alert(week+" "+state);*/
		var year = $('#year').val();
		var week = $('.week').val();
		if(state == "INC"){
			year = parseInt(year)+1;
			$('#year').val(year);
		}else{
			year = parseInt(year)-1;
			$('#year').val(year);
		}
		window.location=('<?php base_url() ?>schedule?year='+year+'&week='+week);
	}

	function checkz(){
		var data =$('input[name=chekCOA]:checked').val();
		if(data == 'Yes'){
			$('#COA').show();
		}else{
			$('#COA').hide();
		}
	}

	function cutPallet(id, value, idz){
		/*alert(id+" "+value);*/
		$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>schedule/cut_pallet",
			data : {
				'id' : id,
				'pallet' : value,
				'id_material' : idz
				
			},
			success:function(resp){
				getNewScheduleList(sn);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})
	}
	
	function addMaterialToList(sno, id, qty, id_list){
			$('#'+id_list).hide();		
			$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>schedule/add_material_to_list",
			data : {
				'sno' : sn,
				'id' : id,
				'qty' : qty
			},
			success:function(resp){
				getNewScheduleList(sn);

			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})

		
		

	}

	function editTime(time){	
		var countDownDate = new Date(time).getTime();

		var x = setInterval(function() {

		  var now = new Date().getTime();
			
		  var distance = countDownDate - now;
			
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
		  $('#remain').html(days + "d " + hours + "h " + minutes + "m " + seconds + "s "); 
		  $('#btnbtn').prop('disabled',false);
		 
		  $('#btnbtn').prop('disabled',false);
		  if (distance < 0) {
			clearInterval(x);
			document.getElementById("remain").innerHTML = "EXPIRED";
			$('#btnbtn').prop('disabled',true);
			$('#EditBtn').hide();
			$('#Cancle').show();
			$('#btnbtncancel').prop('disabled',true);
		  }
		}, 1000);
	}

	

	function showModalForm(status, sn){
		$("#received").val(sn);
		$("#arrived").val(sn);
		$("#modal-schedule-detail").modal('show');
		$(".panel-click").attr('onclick', 'getDetailedInformation('+sn+')')
		$(".panel-hide-all").hide();
		if(status == 1){
			$(".panel-arrived").show();
			$("#modal-schedule-detail .modal-title").text("Set Arrival Time")
		}else if(status == 2){
			$(".panel-received").show();
			$("#modal-schedule-detail .modal-title").text("Set Received Time")
		}else{

		}
	}

	var total_DN = 1;
	var Counter_DN = 1;
	function addDN(){
		total_DN = total_DN + 1;
		Counter_DN = Counter_DN +1;
		$('#delivery_notes').append("<div id='new_dn"+total_DN+"'><br><div class='input-group'><input type='text' name='do_number"+total_DN+"' class='form-control' required=''><input type='file' name='file"+total_DN+"' class='form-control'><span class='input-group-addon'><i class='fa fa-envelope'></i></span></div><button type='button' class='btn btn-danger pull-right' style='margin-top:5px' onclick='removeDN("+total_DN+")'><span class='fa fa-minus'></span></button><br></div>");
		$('#dn_count').val(Counter_DN);
	}
	function removeDN(no_dn){
		Counter_DN = Counter_DN - 1;
		$('#new_dn'+no_dn).remove();
		$('#dn_count').val(Counter_DN);
	}

	var total_COA = 1;
	var Counter_COA = 1;

	function addCOA(){
		total_COA = total_COA + 1;
		Counter_COA = Counter_COA +1;
		$('#CoA').append("<div id='CoA"+total_COA+"'><div class='form-group'><input type='file' name='coa"+total_COA+"' class='form-control' required=''></div><button type='button' class='btn btn-danger pull-right' style='margin-top:5px' onclick='removeCoA("+total_COA+")'><span class='fa fa-minus'></span></button><br></div>");
		$('#coa_count').val(Counter_COA);
	}

	function removeCoA(no_CoA){
		Counter_COA = Counter_COA - 1;
		$('#CoA'+no_CoA).remove();
		$('#coa_count').val(Counter_COA);
	}
		

	//Schedule 2
$('input').on('paste', function(e){
	var $this = $(this);
	$.each(e.originalEvent.clipboardData.items, function(i, v){
		if (v.type === 'text/plain'){
			v.getAsString(function(text){
				var x = $this.closest('td').index(),
					y = $this.closest('tr').index(),
					obj = {};
				text = text.trim('\r\n');
				$.each(text.split('\r\n'), function(i2, v2){
					$.each(v2.split('\t'), function(i3, v3){
						var row = y+i2, col = x+i3;
						obj['cell-'+row+'-'+col] = v3;
						$this.closest('table').find('tr:eq('+row+') td:eq('+col+') input').val(v3);
					});
				});
				//$('#json').text(JSON.stringify(obj));
			});
		}
	});
	return false;
   
});

//header fixed
// var tableOffset = $("#schedule_table").offset().top;
// var $header = $("#header_fix").html();
// var $fixedHeader = $("#header-fixed").append($header);

// $(window).bind("scroll", function() {
//     var offset = $(this).scrollTop();
	
//     if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
//         $fixedHeader.show();
//     }
//     else if (offset < tableOffset) {
//         $fixedHeader.hide();
//     }
// });
//end header fixed

//Update onchange Input
/*$("input").change(function(){
  var get_value = $(this).val();
  var get_menu = $(this).attr('id').split("_")[0];
  var get_row = $(this).attr('id').split("_")[1];
  var get_id = $('#no'+get_row).attr('name');
  var menu = '';
  if(get_menu == 2){
	menu = "category";
  }else if(get_menu == 3){
	menu = "vendor_code";
  }else if(get_menu == 4){
	menu = "material_code";
  }else if(get_menu == 5){
	menu = "material_name";
  }else if(get_menu == 6){
	menu = "uom_plt";
  }else if(get_menu == 7){
	menu = "plt_truck";
  }else if(get_menu == 8){
	menu ="po_number";
  }else if(get_menu == 9){
	menu ="po_line_item";
  }if(get_menu == 10){
	menu = "qty";
  }if(get_menu == 11){
	menu = "uom";
  }if(get_menu == 12){
	menu = "requested_delivery_date";
  }if(get_menu == 13){
	menu = "shift";
  }

 $.ajax({
	type : "POST",
	data : {
		"value" : get_value,
		"id"	: get_id,
		"menu"	: menu,
	},
	url : "<?php echo base_url(); ?>schedule/save_schedule_change",
	success:function(resp){
		
	}
 });

});*/
//end update onchange input

	function save_schedule2(){
		$('#loading').show();
		$('#saving').show();
		var confirmz = confirm('Are you sure you want to Save this all?');
		if(confirmz == true){
			var data_item = [];
		var no =[];
		var category =[];
		var vendor =[];
		var item_code =[];
		var material_name =[];
		var qty_per_palet =[];
		var qty_per_palet_date =[];
		var plan =[];
		var purchase_order =[];
		var line =[];
		var order_qty =[];
		var UOM =[];
		var date =[];
		var shift = [];
		var col = 0;
		var row = 0;
		var data = [];
		$('input').each(function() {
			if($(this).val() !== ""){
				var check = $(this).attr('id').split("_")[0];
				if(check == 1){
					no.push({"No" : $(this).val() });
				}else if(check == 2){
					category.push({"category" : $(this).val() });
				}else if(check == 3){
					vendor.push({"vendor" : $(this).val() });
				}else if(check == 4){
					item_code.push({"item_code" : $(this).val() });
				}else if(check == 5){
					material_name.push({"material_name" : $(this).val() });
				}else if(check == 6){
					qty_per_palet.push({"qty_per_palet" : $(this).val() });
				}else if(check == 7){
					plan.push({"plan" : $(this).val() });
				}else if(check == 8){
					purchase_order.push({"purchase_order" : $(this).val() });
				}else if(check == 9){
					line.push({"Line" : $(this).val() });
				}else if(check == 10){
					order_qty.push({"Order_qty" : $(this).val() });
				}else if(check == 11){
					UOM.push({"UOM" : $(this).val() });
				}else if(check == 12){
					date.push({"Date" : $(this).val() });
				}else if(check == 13){
					shift.push({"Shift" : $(this).val() });
				}
				// data_item.push({ "data" : $(this).val() });        
			}
		});

		/*console.log(purchase_order);
		console.log(line);*/
		$.ajax({
			type : 'POST',
			url  : "<?php echo base_url() ?>schedule/save_schedule2",
			data : {
				'no' : no,
				'category' : category,
				'vendor' :vendor,
				'item_code' : item_code,
				'material_name' : material_name,
				'qty_per_palet' : qty_per_palet,
				'qty_per_palet_date' : qty_per_palet_date,
				'plan' : plan,
				'purchase_order' : purchase_order,
				'line' : line,
				'order_qty' : order_qty,
				'uom' : UOM,
				'date' : date,
				'shift' : shift
			},
			
			success:function(resp){
			   alert("Data has been successfully saved");
			   $('#loading').hide();
			   $('#saving').hide();
			   location.reload();
			},
			error:function(e){
				alert("Something Wrong!");
				console.log(e);
			}

		});
		}
		
	}

	function delete_row_schedule2(a,b,d){
		var confirmz = confirm('Are you sure you want to delete row : '+b+' ?');
		var c = b-1;
		if(confirmz){
				$.ajax({
				type : "get",
				data : { id : a, category:d},
				url  : "<?php echo base_url() ?>schedule/delete_row_schedule2",
				success:function(resp){
					//$('.inputz.'b).val('');
					$(".inputz."+c).val('');
					self.location="schedule2?category="+d;

				},
				error:function(e){
					alert("Something Wrong!");
				}
			});
		}
		
	}

	function delete_schedule2(){
		var confirmz = confirm('Are you sure you want to delete this all?');
			if(confirmz){
				$('#deleting').show();
				$('#loading').show();
				$.ajax({
				type : "GET",
				url  : "<?php echo base_url() ?>schedule/delete_schedule2",
				success:function(resp){
					$('#deleting').hide();
					$('#loading').hide();
					alert("All Data has been deleted");
					location.reload();
				},
				error:function(e){
					alert("Something Wrong!");
				}
			});
		}
		
}


function clear_all(){
	$('.inputz').val('');
var confirmz = confirm("Are you sure want to Clear this all?");
	if(confirmz){
		var week = $('.week_sch').val();
		var year = $('#year_sch').val();
		var category = '<?php echo $_GET['category']; ?>'
		$('.inputz').val('');
		$.ajax({
			type : "GET",
			data : {
				"year" : year,
				"week" : week,
				"category" : category
			},
			url : "<?php echo base_url(); ?>schedule/clear_flag",
			success:function(resp){
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		})
	} 
	
}

$("#check_all").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 });

function check_delete(){
		if(($('input:checkbox:checked').length) == 0){
			alert('Nothing is checked');
		}
 		else if(($('input:checkbox:checked').length) == 1){
 			var confirmz = confirm('Are you sure you want to delete this?');
 			$('#loading').show();
 			$('#delete').show();
 		}else{
 			var confirmz = confirm('Are you sure you want to delete this all?');
 			$('#loading').show();
 			$('#delete').show();
 		}
 		
 		if(confirmz){
 			var check_item = [];
 			var a = 0;
            $.each($("input[name='check_item']:checked"), function(){            
                check_item.push({"check_item" : $(this).val()});
            	// check_item[a] = $(this).val();
            	// a += 1;
            });
            // alert("Item yang di check adalah: " + check_item[0].check_item);
            // console.log(check_item);

 		$.ajax({
 			type : "GET",
 			url  : "<?php echo base_url(); ?>schedule/check_delete",
 			data : {
 				"check_item" : check_item,
 			},
 			success:function(resp){
 				location.reload();
 			},
 			error: function(e){
 				alert('Something Wrong');
 				console.log(e);
 			}
 		});
 		}
 		
 		
 	}

 	function select_menu(a){
		var year = $('#year_sch').val();
		var week = $('.year_sch').val();
		var category = '<?php echo $_GET['category'] ?>';
		if(a == ''){
			$('#item_list').attr('disabled',true);
			$('#item_list').empty();	
		}else{
			$('#item_list').attr('disabled',false);
			$('#item_list').empty();
		$.ajax({
			type : "POST",
			url  : "<?php echo base_url() ?>schedule/filter_menu",
			data : {
				'menu' : a,
				'year' : year,
				'wekk' : week,
				'category' : category

			},
			success:function(resp){
				$('#item_list').append(resp);
			},
			error:function(e){
				alert("Something Wrong!");
			}
		})
	}
		
		
	}

	 <?php if($this->uri->segment(2) == 'schedule2'){ ?>
	function setWeek_sch(week, state){
		/*alert(week+" "+state);*/
		var category = '<?php echo $_GET['category']; ?>';
		var year = $('#year_sch').val();
		if(state == "INC"){
			week = parseInt(week)+1;
			$('.week_sch').val(week);
		}else{
			week = parseInt(week)-1;
			$('.week_sch').val(week);
		}
		window.location=('<?php base_url() ?>schedule2?category='+category+'&year='+year+'&week='+week);
	}

	function setYear_sch(year, state){
			var year = $('#year_sch').val();
			var week = $('.week_sch').val();
			var category = '<?php echo $_GET['category']; ?>';

		if(state == "INC"){
			year = parseInt(year)+1;
			$('#year_sch').val(year);
		}else{
			year = parseInt(year)-1;
			$('#year_sch').val(year);
		}
		window.location=('<?php base_url() ?>schedule2?category='+category+'&year='+year+'&week='+week);	
	}

	function setYear2_sch(year, state){
			var year = $('#year_sch').val();
			var week = $('.week_sch').val();
			var category = '<?php echo $_GET['category']; ?>';
			window.location=('<?php base_url() ?>schedule2?category='+category+'&year='+year+'&week='+week);	
	}

<?php } ?>
//End Schedule 2
function checkDate(date, c_date, shift, c_shift){
	var ll = new Date();
	var mo = (ll.getMonth() < 10)?"0"+(ll.getMonth()+1):(ll.getMonth()+1);
	var cid = ll.getFullYear()+"-"+mo+"-"+ll.getDate();
	var ncid = ll.getFullYear()+"-"+mo+"-"+(ll.getDate()+1);
	var nncid = ll.getFullYear()+"-"+mo+"-"+(ll.getDate()+2);
	var nnncid = ll.getFullYear()+"-"+mo+"-"+(ll.getDate()+3);
	if(date < c_date){
		alert("Tanggal telah terlampaui, silahkan kontak tim Unilever");
		setTimeout(function(){
			$("[data-toggle='popover']").popover('hide');
		}, 50);
	}else{
		if(date == cid){
			if(c_shift == "DM"){
				setTimeout(function(){
					this.popover('show');
				}, 50);
			}else if(c_shift == "DP"){
				if(shift == "DM"){
					alert("Shift telah terlampaui, silahkan kontak tim Unilever");
					setTimeout(function(){
						$("[data-toggle='popover']").popover('hide');
					}, 50);
				}else{
					setTimeout(function(){
						this.popover('show');
					}, 50);
				}
			}else{
				if(shift == "DM" || shift == "DP"){
					alert("Shift telah terlampaui, silahkan kontak tim Unilever");
					setTimeout(function(){
						$("[data-toggle='popover']").popover('hide');
					}, 50);
				}else{
					setTimeout(function(){
						this.popover('show');
					}, 50);
				}
			}
		}else if(date == ncid){
			if(c_shift == "DM"){
				alert("Anda belum bisa melakukan penjadwalan untuk jadwal ini.");
				setTimeout(function(){
					$("[data-toggle='popover']").popover('hide');
				}, 50);
			}else if(c_shift == "DP"){
				if(shift == "DM" || shift == "DS"){
					setTimeout(function(){
						this.popover('show');
					}, 50);
				}else{
					alert("Anda belum bisa melakukan penjadwalan untuk jadwal ini.");
					setTimeout(function(){
						$("[data-toggle='popover']").popover('hide');
					}, 50);
				}
			}else{
				if(shift == "DM" || shift == "DP"){
					setTimeout(function(){
						this.popover('show');
					}, 50);
				}else{
					alert("Anda belum bisa melakukan penjadwalan untuk jadwal ini.");
					setTimeout(function(){
						$("[data-toggle='popover']").popover('hide');
					}, 50);
				}
			}
		}else{
			alert("Anda belum bisa melakukan penjadwalan untuk jadwal ini.");
			setTimeout(function(){
				$("[data-toggle='popover']").popover('hide');
			}, 50);
		}
	}
}



$('#menu').select2();
$('#item_list').select2();

function sumbitForm(){
	$('#saving').show();
	$('#loading').show();
	var conf = confirm('Apakah anda yakin?\nPastikan mengecek semua data sebelum menyimpan.');
	if(conf){
		$("#idSchTbl").submit();
	}else{
		$('#loading').hide();
		$('#saving').hide();	
	}

	
}

</script>