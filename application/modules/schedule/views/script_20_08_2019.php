<script>
  $(".table-responsive").perfectScrollbar()
  $('#popoverData').popover();
  $(document).ready(function(){
    var link = "<?php echo $this->uri->segment(2); ?>";
    var link1 = "<?php echo $this->uri->segment(1); ?>";
    if(link == "view_schedule"){      
      callOffPallet();
      $('body').perfectScrollbar();
      $('.modal').perfectScrollbar();
      var material_count = 0;
      var jam = setInterval(jams, 100);
    }

    if(link1 == "schedule" && link == ""){
      check_outstanding();
      setInterval(function(){
        getHM();
      }, 1000);

      $('body').perfectScrollbar();
      $.ajax({
      url : "<?php echo base_url(); ?>schedule/check_adt",
      dataType : "JSON",
      success:function(a){
        if(a.length > 0){
        var data = [];
        for (var i = a.length - 1; i >= 0; i--) {
          data += "Vendor Code :" + a[i].vendor_code+" \nRdd :"+a[i].rdd+" \nTime Slot :"+a[i].start_time+' - '+a[i].end_time +" \nShift :"+a[i].shift+" Reason :"+ a[i].reason+"\n\n"; 
        }
          alert("Terdapat Schedule Additional Truck dengan jadwal : \n"+data);
        }
      },error:function(a){
        alert("Something Error");
      }
    });
  } 


    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search RDS",
        }
      });
  });

$("#schedule_information").on("mouseup", function (e) {
    var l = $(e.target);
    if (l[0].className.indexOf("popover") == -1) {
        $(".popover").each(function () {
            $(this).popover("hide");
        });
    }
});

function check_outstanding(vendor_code = '21632'){
  var week = "<?php echo $this->input->get('week'); ?>";
  var vendor_code = "<?php echo $this->session->userdata('sess_vendor_code'); ?>";
  var year = "<?php echo $this->input->get('year'); ?>";
  
  setTimeout(function(){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url() ?>schedule/check_outstanding",
      data : {
        'week' : week,
        'year' : year,
        'vendor_code' : vendor_code
      },
      success:function(resp){
          
          if(resp == "false"){
            
          }else{
            openModal("Terdapat Oustanding ", resp, "lg");  
          }
          
        
        
      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      }
    })
    }, 200);

}

function jams(){
  $('#jam').html(moment().format('dddd, DD-MM-YYYY h:mm:ss'));
}

$('.hourz').on('change',function(){
  if($(this).val() >= 24){
    $(this).val(23);
  }
});
  
$('.minutez').on('change',function(){
  if($(this).val() >= 60){
    $(this).val(59);
  }
});

$('.hourz').on('click', function(){
  $(this).select();
});

$('.minutez').on('click', function(){
  $(this).select();
});

  $('#arrive_time').on('change', function(){
    var arrive_time = $('#arrive_time').val();
    $('#arrive_time_24').html('*Time : '+arrive_time);
  });

  $('#received_time').on('change', function(){
    var received_time = $('#received_time').val();
    $('#received_time_24').html('*Time : '+received_time);
  });

  var c_minus = 0;
  var vendor_code = "<?php if(isset($vendor_code)){echo $vendor_code;}else{} ?>";
  var sn = "<?php echo (isset($sn))?$sn:'-'; ?>";
  var rdd = "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):'' ?>";
  var capacity = 0;
  function callOffPallet(b){
    capacity = "<?php if(isset($full_pallet) ){echo $full_pallet; }else{echo 32;} ?>";
    var a = 0;
    $(".inp-pallet").each(function(){
      var as = parseInt(this.value);
      a = a+as;
    });
    c_minus = capacity-a;
    material_count = a;
    $("#area-cnt-pallet").html("Slot Occupancy : <b>"+a+" / "+capacity+"</b>");
    $('#count-pallet').val(a);
    getSuggestion(c_minus, sn);
    getMaterialPool(c_minus,sn);
  }

  function pop_event(id_time_slot, c_date){
    alert(c_date);
  }
  

  function editModal(){
    $('.unedit').hide();
    $('.edit').show();
    $('#EditBtn').hide();
    $('#Cancel').show();
    $('#editp').html('Cancel');
  }

  function Cancel(){
    $('.unedit').show();
    $('.edit').hide();
    $('#EditBtn').show();
    $('#Cancel').hide();
    $('#editp').html('Edit');
  }


  function moveMaterial(id, sn){

    $.ajax({
      type : "GET",
      url  : "<?php echo base_url() ?>schedule/move_schedule",
      data : {
        'id' : id
      },
      success:function(resp){
        getNewScheduleList(sn);
      },
      error:function(e){
        alert("Something Wrong!");
      }
    })
  }

  function getNewScheduleList(sn){
    var batch_number = $('#batch_number').val();
    $.ajax({
      type : "GET",
      url  : "<?php echo base_url() ?>schedule/get_schedule_list",
      data : {
        'sn' : sn,
        'batch_number' : batch_number
      },
      success:function(resp){
        $("#area-list").html(resp);
        callOffPallet();
      },
      error:function(e){
        alert("Something Wrong!");
      }
    })
  }


  function getDetailedInformation(key, status){
    $(".modal").modal('hide');
    setTimeout(function(){
      $.ajax({
        type : "GET",
        url : "<?php echo base_url() ?>schedule/get_detailed_information",
        data : {
          'key' : key,
          'status' : status
        },
        success:function(resp){
          openModal("Information", resp, "xl");
          editTime($("#created_at").val());
          //$('#modal-dynamic').perfectScrollbar();
        },
        error:function(e){
          alert("Something wrong!");
          console.log(e);
        }
      })
      
    }, 200);  
  }

  $('.table-schedule').on('scroll', function() {
    $(this).find('thead th')
      .css('transform', 'translateY('+ this.scrollTop +'px)')
      .css({
        position: "relative",
        "z-index" : 3
      });;
    $(this).find('tr td:nth-child(1)')
      .css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
      .css({
        "z-index" : 2
      });
  });

  function getDI(key){
    $(".modal").modal('hide');
    setTimeout(function(){
      $.ajax({
        type : "GET",
        url : "<?php echo base_url() ?>schedule/get_DI",
        data : {
          'key' : key
        },
        success:function(resp){
          openModal("Information", resp, "lg");
          editTime($("#created_at").val());
        },
        error:function(e){
          alert("Something wrong!");
          console.log(e);
        }
      })
      preventDefault();
    }, 200);  
  }

  function getSuggestion(c_min, sn){
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>schedule/get_suggestion",
      data : {
        'pallet_minus' : c_min,
        'sn' : sn,
        'rdd' : "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):"123" ?>"
      },
      success:function(resp){
        $("#area-same-rdd").html(resp);
      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      }
    })
  }

  function getMaterialPool(cminus, sn){
    $.ajax({
      type : "GET",
      url  : "<?php echo base_url() ?>schedule/get_pool_schedule_list",
      data : {
        'cminus'  : cminus,
        'adt' : "<?php echo $this->uri->segment(7); ?>",
        'sn' : sn,
        'rdd' : "<?php echo (null !== $this->uri->segment(4))?$this->uri->segment(4):"123" ?>",
      },
      success:function(resp){
        $("#area-pool").html(resp);
      },
      error:function(e){
        alert("Something Wrong!");
      }
    })
  }


  function getHM(){
    
    var today = new Date();
    var date_now = moment().format("YYYY-mm-DD");
    $("[name='arrived_hour'], [name='received_hour']").val(today.getHours());
    $("[name='arrived_minute'], [name='received_minute']").val(today.getMinutes());
    // $("[name='arrive_date'], [name='received_date']").val(date_now);
  }

  function addMaterial(id, sn){

    $.ajax({
      type : 'GET',
      url : "<?php echo base_url() ?>schedule/add_material",
      data : {
        'id' : id,
        'sn' : sn
      },
      success:function(resp){
        getNewScheduleList(sn);
      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      }
    });
  }

  function addMaterialPool(id, sn){
    $.ajax({
      type : 'GET',
      url : "<?php echo base_url() ?>schedule/add_material_pool",
      data : {
        'id' : id,
        'sn' : sn,
        'slot' : "<?php echo $this->uri->segment(5); ?>"
      },
      success:function(resp){
        getNewScheduleList(sn);
      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      }
    });
  }

  function changeURL(){
    var dapick = $(".dapick").val();
    window.location=("<?php echo base_url() ?>schedule?date_start="+dapick);
  }

  $(".btcek").on('click', function(e){
    alert("UWUWEWE");
    e.preventDefault();
    return false;
  });

  $('.bt-res').on('click', function(){
    alert("YESSS");
  });

  function setWeek(week, state){
    /*alert(week+" "+state);*/
    var year = $('#year').val();
    if(state == "INC"){
      week = parseInt(week)+1;
      $('.week').val(week);
    }else{
      week = parseInt(week)-1;
      $('.week').val(week);
    }
    window.location=('<?php base_url() ?>schedule?year='+year+'&week='+week);
  }


  function setYear2(year, state){
      var year = $('#year').val();
      var week = $('.week').val();
      window.location=('<?php base_url() ?>schedule?year='+year+'&week='+week);
  }


  function setYear(year, state){
    /*alert(week+" "+state);*/
    var year = $('#year').val();
    var week = $('.week').val();
    if(state == "INC"){
      year = parseInt(year)+1;
      $('#year').val(year);
    }else{
      year = parseInt(year)-1;
      $('#year').val(year);
    }
    window.location=('<?php base_url() ?>schedule?year='+year+'&week='+week);
  }

  function checkz(){
    var data =$('input[name=chekCOA]:checked').val();
    if(data == 'Yes'){
      $('#COA').show();
    }else{
      $('#COA').hide();
    }
  }

  function check_detail(dn, coa){
    var dn_check = 0;
    var coa = 0;
    var coa_check = 0;
    var total_pallet = $('#count-pallet').val();

    if(parseInt(total_pallet) > parseInt(capacity)){
      Swal.fire({
        type: 'error',
        text: 'Tidak dapat mensubmit Slot Occupancy melebihi '+capacity+'!',
      });
    }

    $('.dn').each(function(){
      if( this.value == '' ){
        dn_check += 1;
      }
    });

    $('.dn_input').each(function(){
      if(this.files.length === 0){
        dn_check += 1;
      }
    });

    $('.coa_input').each(function(){
      if(this.files.length == 0 ){
        coa_check += 1;
      }

    });

    if(dn_check > 0){
      Swal.fire({
        type: 'error',
        text: 'Data DN tidak terisi dengan benar, Mohon untuk di cek kembali!',
        
      });
    }else if(coa_check > 0){
      Swal.fire({
        type: 'error',
        text: 'Terdapat COA yang kosong!',
      });
    }   

    

    if(dn_check == 0 && coa_check == 0 && parseInt(total_pallet) <= parseInt(capacity)){
      $('#submitz').click();
      $('#submitz').attr('disabled', true);
    }
  }

  function cutPallet(id, value, idz, quantity, no){
    /*alert(id+" "+value);*/
    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>schedule/cut_pallet",
      data : {
        'id' : id,
        'pallet' : value,
        'id_material' : idz,
        'quantity'  : quantity
        
      },
      success:function(resp){
        getNewScheduleList(sn);
      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      }
    });

  }
  
  function addMaterialToList(sno, id, qty, id_list){
      $('#'+id_list).hide();    
      $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>schedule/add_material_to_list",
      data : {
        'sno' : sn,
        'id' : id,
        'qty' : qty
      },
      success:function(resp){
        getNewScheduleList(sn);

      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      }
    })

    
    

  }

  function editTime(time){      
    var countDownDate = new Date(time).getTime();
    clearInterval();
    var now = new Date().getTime(); 
    var distance = countDownDate - now;

    if(distance > 0){
      $('#timer_edit').show();
    }
    
    var x = setInterval(function() {
      
      var now = new Date().getTime();
      var distance = countDownDate - now;           
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
      
      $('#remain').html(days + "d " + hours + "h " + minutes + "m " + seconds + "s "); 
      $('#btnbtn').prop('disabled',false);
      $('#btnbtn').prop('disabled',false);
      if (distance <= 0) {
      clearInterval(x);
      document.getElementById("remain").innerHTML = "EXPIRED";
      $('#btnbtn').prop('disabled',true);
      $('#timer_edit').hide();
      $('#Cancle').show();
      $('#btnbtncancel').prop('disabled',true);
      }
    }, 1000);
  }

  

  function showModalForm(status, sn){
    $("#modal_size").removeClass("modal-sm");
    $("#modal_size").removeClass("modal-md");
    $("#modal_size").removeClass("modal-lg");
    $("#received").val(sn);
    $("#arrived").val(sn);
    $('#schedule_info').show();
    
    $(".panel-click").attr('onclick', 'getDetailedInformation('+sn+')')
    $(".panel-hide-all").hide();
    if(status == 1){
      
      $("#modal_size").addClass("modal-sm");
      $("#modal-schedule-detail").modal('show');
      $(".panel-arrived").show();
      $("#modal-schedule-detail .modal-title").text("Set Arrival Time");

    }else if(status == 2){
      
      $("#modal_size").addClass("modal-lg");
      $("#modal-schedule-detail").modal('show');
      $(".panel-received").show();
      $("#modal-schedule-detail .modal-title").text("Set Received Time");

      $.ajax({
        type : "GET",
        url : "<?php echo base_url() ?>schedule/return_form",
        data : {
          'key' : sn
        },
        success:function(resp){
          $('.return_form').empty();
          $('.return_form').html(resp);
          
        },
        error:function(e){
          alert("Something wrong!");
          console.log(e);
        }
      });
      
    }else{
      $("#modal_size").removeClass("modal-md");
      $("#modal_size").addClass("modal-sm");

      $('#schedule_info').hide();
      var check_os = 0;
      var check_truck = 0;
      var total_os = 0;
      var os_qty =[];
      var min_qty = 0;
      var week = "<?php echo $this->input->get('week'); ?>";
      var year = "<?php echo $this->input->get('year'); ?>";
      $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>outstanding/check",
      data : {
        'vendor' : vendor_code,
        'week'   : week,
        'year'   : year
      },
      dataType: "JSON",
      success:function(resp){
        var hasil = resp.length;

        for (var i = resp.length - 1; i >= 0; i--) {
          os_qty[i] = resp[i].req_pallet;
        }

         min_qty = Math.min.apply(null, os_qty);
         var max = Math.max.apply(null, os_qty);
         
        if(max > 0){
          check_os=1;
        }else{
          check_os=0;
        }
        
      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      },async: false
    });

      $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>schedule/check_truck",
      dataType: "JSON",
      data:{
        "min_qty" : min_qty
      },
      success:function(resp){ 

        if(resp== '' || resp == 'null' || resp == null || resp == 0){
          check_truck = 1;
        }else{
          check_truck = 0;
        }
        
      },
      error:function(e){
        alert("Something wrong!");
        console.log(e);
      },async: false
    });

      if( (check_os == 1) && (check_truck) == 1 ){
          $("#modal-schedule-detail").modal('show');
          $(".panel-adt_truck").show();
          $("#modal-schedule-detail .modal-title").text("Set Additional Truck");
        }else if(check_os == 0 ){
          $("#modal-schedule-detail").modal('hide');
          alert("Tidak ada outstanding material yang tersisa");
        }else if(check_truck == 0){
          $("#modal-schedule-detail").modal('hide');
          alert("Terdapat schedule yang masih tersedia");
        }

    }
    
  }

  var total_DN = 1;
  var Counter_DN = 1;

  function addDN(){
    total_DN = total_DN + 1;
    Counter_DN = Counter_DN +1;
    var no_dn = 1;
    

$('#body_dn').append("<tr id='dn_number"+Counter_DN+"'><td><span class='btn btn-danger btn-sm' onclick='removeDN("+Counter_DN+")' id='delete_dn"+Counter_DN+"'><i class='fas fa-minus'></i></span></td> <td class='no_dn text-center'></td> <td><input type='text' class='dn' style='width: 100' required name='do_number"+Counter_DN+"'></td><td><input required type='file' class='dn_input' class='btn btn-primary btn-sm' style='' name='file"+Counter_DN+"'></td></tr>");
    // $('#delivery_notes').append("<div id='new_dn"+total_DN+"'><br><div class='input-group'><input type='text' name='do_number"+total_DN+"' class='form-control' required=''><input type='file' name='file"+total_DN+"' class='form-control'><span class='input-group-addon'><i class='fa fa-envelope'></i></span></div><button type='button' class='btn btn-danger pull-right' style='margin-top:5px' onclick='removeDN("+total_DN+")'><span class='fa fa-minus'></span></button><br></div>");
    $('.no_dn').each(function(){
      $(this).html(no_dn);
      no_dn += 1; 
    });

    $('#dn_count').val(Counter_DN);
  }
  function removeDN(no_dn){
    
    Counter_DN = Counter_DN - 1;
    $('#dn_number'+no_dn).remove();
    $('#dn_count').val(Counter_DN);
    var no_dnz = 1;
    $('.no_dn').each(function(){
      $(this).html(no_dnz);
      no_dnz += 1; 
    });
  }

  var total_COA = 1;
  var Counter_COA = 1;

  function addCOA(){
    total_COA = total_COA + 1;
    Counter_COA = Counter_COA +1;
    var no_coa = 1;

    $('#body_coa').append("<tr id='no_coa"+Counter_COA+"'><td id='delete_coa"+Counter_COA+"'><span class='btn btn-danger btn-sm' onclick='removeCOA("+Counter_COA+")' id='delete_coa"+Counter_COA+"'><i class='fas fa-minus'></i></span></td><td class='no_coa text-center'></td><td><input required class='coa_input' type='file' class='btn btn-warning btn-sm' style='' name='coa"+Counter_COA+"'></td></tr>");
    
    $('.no_coa').each(function(){
      $(this).html(no_coa);
      no_coa += 1; 
    });

    $('#coa_count').val(Counter_COA);
  }

  function removeCOA(no_COA){
    
    Counter_COA = Counter_COA - 1;
    $('#no_coa'+no_COA).remove();
    $('#coa_count').val(Counter_COA);
    var no_coaz = 1;
    $('.no_coa').each(function(){
      $(this).html(no_coaz);
      no_coaz += 1; 
    });
  }
    

  //Schedule 2
$('input').on('paste', function(e){
  var $this = $(this);
  $.each(e.originalEvent.clipboardData.items, function(i, v){
    if (v.type === 'text/plain'){
      v.getAsString(function(text){
        var x = $this.closest('td').index(),
          y = $this.closest('tr').index(),
          obj = {};
        text = text.trim('\r\n');
        $.each(text.split('\r\n'), function(i2, v2){
          $.each(v2.split('\t'), function(i3, v3){
            var row = y+i2, col = x+i3;
            
            obj['cell-'+row+'-'+col] = v3;
            
            $this.closest('tbody').find('tr:eq('+row+') td:eq('+col+') input').val(v3);
          });
        });
        //$('#json').text(JSON.stringify(obj));
      });
    }
  });
  return false;
   
});

//header fixed
// var tableOffset = $("#schedule_table").offset().top;
// var $header = $("#header_fix").html();
// var $fixedHeader = $("#header-fixed").append($header);

// $(window).bind("scroll", function() {
//     var offset = $(this).scrollTop();
  
//     if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
//         $fixedHeader.show();
//     }
//     else if (offset < tableOffset) {
//         $fixedHeader.hide();
//     }
// });
//end header fixed

//Update onchange Input
/*$("input").change(function(){
  var get_value = $(this).val();
  var get_menu = $(this).attr('id').split("_")[0];
  var get_row = $(this).attr('id').split("_")[1];
  var get_id = $('#no'+get_row).attr('name');
  var menu = '';
  if(get_menu == 2){
  menu = "category";
  }else if(get_menu == 3){
  menu = "vendor_code";
  }else if(get_menu == 4){
  menu = "material_code";
  }else if(get_menu == 5){
  menu = "material_name";
  }else if(get_menu == 6){
  menu = "uom_plt";
  }else if(get_menu == 7){
  menu = "plt_truck";
  }else if(get_menu == 8){
  menu ="po_number";
  }else if(get_menu == 9){
  menu ="po_line_item";
  }if(get_menu == 10){
  menu = "qty";
  }if(get_menu == 11){
  menu = "uom";
  }if(get_menu == 12){
  menu = "requested_delivery_date";
  }if(get_menu == 13){
  menu = "shift";
  }

 $.ajax({
  type : "POST",
  data : {
    "value" : get_value,
    "id"  : get_id,
    "menu"  : menu,
  },
  url : "<?php echo base_url(); ?>schedule/save_schedule_change",
  success:function(resp){
    
  }
 });

});*/
//end update onchange input

  function save_schedule2(){
    $('#loading').show();
    $('#saving').show();
    var confirmz = confirm('Are you sure you want to Save this all?');
    if(confirmz == true){
      var data_item = [];
    var no =[];
    var category =[];
    var vendor =[];
    var item_code =[];
    var material_name =[];
    var qty_per_palet =[];
    var qty_per_palet_date =[];
    var plan =[];
    var purchase_order =[];
    var line =[];
    var order_qty =[];
    var UOM =[];
    var date =[];
    var shift = [];
    var col = 0;
    var row = 0;
    var data = [];
    $('input').each(function() {
      if($(this).val() !== ""){
        var check = $(this).attr('id').split("_")[0];
        if(check == 1){
          no.push({"No" : $(this).val() });
        }else if(check == 2){
          category.push({"category" : $(this).val() });
        }else if(check == 3){
          vendor.push({"vendor" : $(this).val() });
        }else if(check == 4){
          item_code.push({"item_code" : $(this).val() });
        }else if(check == 5){
          material_name.push({"material_name" : $(this).val() });
        }else if(check == 6){
          qty_per_palet.push({"qty_per_palet" : $(this).val() });
        }else if(check == 7){
          plan.push({"plan" : $(this).val() });
        }else if(check == 8){
          purchase_order.push({"purchase_order" : $(this).val() });
        }else if(check == 9){
          line.push({"Line" : $(this).val() });
        }else if(check == 10){
          order_qty.push({"Order_qty" : $(this).val() });
        }else if(check == 11){
          UOM.push({"UOM" : $(this).val() });
        }else if(check == 12){
          date.push({"Date" : $(this).val() });
        }else if(check == 13){
          shift.push({"Shift" : $(this).val() });
        }
        // data_item.push({ "data" : $(this).val() });        
      }
    });

    /*console.log(purchase_order);
    console.log(line);*/
    $.ajax({
      type : 'POST',
      url  : "<?php echo base_url() ?>schedule/save_schedule2",
      data : {
        'no' : no,
        'category' : category,
        'vendor' :vendor,
        'item_code' : item_code,
        'material_name' : material_name,
        'qty_per_palet' : qty_per_palet,
        'qty_per_palet_date' : qty_per_palet_date,
        'plan' : plan,
        'purchase_order' : purchase_order,
        'line' : line,
        'order_qty' : order_qty,
        'uom' : UOM,
        'date' : date,
        'shift' : shift
      },
      
      success:function(resp){
         alert("Data has been successfully saved");
         $('#loading').hide();
         $('#saving').hide();
         location.reload();
      },
      error:function(e){
        alert("Something Wrong!");
        console.log(e);
      }

    });
    }
    
  }

  function delete_schedule2(){
    var confirmz = confirm('Are you sure you want to delete this all?');
      if(confirmz){
        $('#deleting').show();
        $('#loading').show();
        $.ajax({
        type : "GET",
        url  : "<?php echo base_url() ?>schedule/delete_schedule2",
        success:function(resp){
          $('#deleting').hide();
          $('#loading').hide();
          alert("All Data has been deleted");
          location.reload();
        },
        error:function(e){
          alert("Something Wrong!");
        }
      });
    }
    
}

<?php
if($this->uri->segment(2) == "schedule2" ||  $this->uri->segment(2) == "schedule2_search"){
  ?>
  function clear_all(){
  
  var confirmz = confirm("Are you sure want to clear this all?");
    if(confirmz){
      var week = "<?php echo $_GET['week']; ?>";
      var year = "<?php echo $_GET['year']; ?>";
      var category = '<?php echo $_GET['category']; ?>'
      $('.inputz').val('');
      $.ajax({
        type : "GET",
        data : {
          "year" : year,
          "week" : week,
          "category" : category
        },
        url : "<?php echo base_url(); ?>schedule/clear_flag",
        success:function(resp){
          location.reload();
        },
        error:function(e){
          alert("Something wrong!");
          console.log(e);
        }
      })
    } 
    
  }

  var menu = $('.menux').val();
  function select_menu(a){
    var year = "<?php echo $_GET['year']; ?>";
    var week = "<?php echo $_GET['week']; ?>";
    var category = '<?php echo $_GET['category'] ?>';
    menu = a;
    
  }

  function search_category(a){
    $('#select_supplier').hide();
    $('.menux').show().attr('disabled', true);

    if(a == 'supplier'){
      $('#item_listz').hide();
      $('#filter_text').hide();
      $('#select_supplier').show();
      $('#supplier').attr('disabled', false);
      $('#select_item').hide();
    }else if(a == 'item'){
      $('#supplier').val('');
      $('#filter_text').show();
      $('.menux').show().attr('disabled', false);
      $('#supplier').attr('disabled', false);
      $('#select_item').show();
      $('#item_listz').show();
      $('#filter').show();
    }
    
  }

  function select_item(b){
    var year = "<?php echo $_GET['year']; ?>";
    var week = "<?php echo $_GET['week']; ?>";
    var category = '<?php echo $_GET['category'] ?>';
    var supplier = $('#supplier').val();
    var categoryx = $('.categoryx').val();
    $('#item_list').empty();
    if(categoryx == 'supplier'){
      loading();
      window.location=('<?php base_url() ?>schedule2_search?category='+category+'&year='+year+'&week='+week+'&supplier='+supplier+"&menu=supplier");  
    }else{
      $('#item_list').attr('disabled',false);
      $.ajax({
        type : "POST",
        url  : "<?php echo base_url() ?>schedule/filter_menu",
        data : {
          'menu'    : menu,
          'supplier'  : b,
          'year'    : year,
          'wekk'    : week,
          'category'  : category
        },

        success:function(resp){       
          $('#item_list').append(resp);
        },

        error:function(e){
          alert("Something Wrong!");
        }
      }); 
    }

    
  }


  function loading(){
    var x = new Spin.Spinner().spin();
    $('body').append(x.el);
  }

  function loading_stop(){
    x.stop();
  }

  function filter_schedule(a){
    var year = "<?php echo $_GET['year']; ?>";
    var week = "<?php echo $_GET['week']; ?>";
    var category = '<?php echo $_GET['category'] ?>';
    var supplier = $('#supplier').val();
    var menu_list = '';
    
    if(menu != 'supplier'){
      menu_list = "&menu="+menu+"&item_list="+a;
    }else{
      menu_list = '';
    }
    loading();
    window.location=('<?php base_url() ?>schedule2_search?category='+category+'&year='+year+'&week='+week+'&supplier='+supplier+menu_list);
  }
  <?php
}
?>

$("#check_all").click(function () {

     $('input:checkbox').not(this).prop('checked', this.checked);
      $('#select').prop('checked',false);
 });

function check_delete(){
    if(($('input:checkbox:checked').length) == 0){
      alert('Nothing is checked');
    }
    else if(($('input:checkbox:checked').length) == 1){
      var confirmz = confirm('Are you sure you want to delete this?');
      $('#loading').show();
      $('#delete').show();
    }else{
      var confirmz = confirm('Are you sure you want to delete this all?');
      $('#loading').show();
      $('#delete').show();
    }
    
    if(confirmz){
      var check_item = [];
      var a = 0;
            $.each($("input[name='check_item']:checked"), function(){            
                check_item.push({"check_item" : $(this).val()});
              // check_item[a] = $(this).val();
              // a += 1;
            });
            // alert("Item yang di check adalah: " + check_item[0].check_item);
            // console.log(check_item);

    $.ajax({
      type : "GET",
      url  : "<?php echo base_url(); ?>schedule/check_delete",
      data : {
        "check_item" : check_item,
      },
      success:function(resp){
        location.reload();
      },
      error: function(e){
        alert('Something Wrong');
        console.log(e);
      }
    });
    }
    
    
  }
 <?php if($this->uri->segment(2) == 'schedule2' || $this->uri->segment(2) == 'schedule2_search'){ ?>
  setInterval(function(){
    time_periode_checker();
  }, 1000);
  $end_time = "<?php echo $time_freeze->row()->end_time; ?>";
  function time_periode_checker(){
     $time_now =  moment().format('HH:mm:ss');
     if($end_time == $time_now){
      location.reload();
     }
  }
  
  function setWeek_sch(week, state){
    /*alert(week+" "+state);*/
    var category = '<?php echo $_GET['category']; ?>';
    var year = $('#year_sch').val();
    if(state == "INC"){
      week = parseInt(week)+1;
      $('.week_sch').val(week);
    }else{
      week = parseInt(week)-1;
      $('.week_sch').val(week);
    }
    window.location=('<?php base_url() ?>schedule2?category='+category+'&year='+year+'&week='+week);
  }

  function setYear_sch(year, state){
      var year = $('#year_sch').val();
      var week = $('.week_sch').val();
      var category = '<?php echo $_GET['category']; ?>';

    if(state == "INC"){
      year = parseInt(year)+1;
      $('#year_sch').val(year);
    }else{
      year = parseInt(year)-1;
      $('#year_sch').val(year);
    }
    window.location=('<?php base_url() ?>schedule2?category='+category+'&year='+year+'&week='+week);  
  }

  function setYear2_sch(year, state){
      var year = $('#year_sch').val();
      var week = $('.week_sch').val();
      var category = '<?php echo $_GET['category']; ?>';
      window.location=('<?php base_url() ?>schedule2?category='+category+'&year='+year+'&week='+week);  
  }

<?php } ?>
//End Schedule 2
// function checkDate(date, c_date, shift, c_shift){
//  var ll = new Date();
//  var mo = (ll.getMonth() < 10)?"0"+(ll.getMonth()+1):(ll.getMonth()+1);
//  var cid = ll.getFullYear()+"-"+mo+"-"+ll.getDate();
//  var ncid = ll.getFullYear()+"-"+mo+"-"+(ll.getDate()+1);
//  var nncid = ll.getFullYear()+"-"+mo+"-"+(ll.getDate()+2);
//  var nnncid = ll.getFullYear()+"-"+mo+"-"+(ll.getDate()+3);

//  if(date < c_date){
//    alert("Tanggal telah terlampaui, silahkan kontak tim Unilever");
//    setTimeout(function(){
//      $("[data-toggle='popover']").popover('hide');
//    }, 50);
//  }else{
//    if(date == cid){
//      if(c_shift == "DM"){
//        setTimeout(function(){
//          this.popover('show');
//        }, 50);
//      }else if(c_shift == "DP"){
//        if(shift == "DM"){
//          alert("Shift telah terlampaui, silahkan kontak tim Unilever");
//          setTimeout(function(){
//            $("[data-toggle='popover']").popover('hide');
//          }, 50);
//        }else{
//          setTimeout(function(){
//            this.popover('show');
//          }, 50);
//        }
//      }else{
//        if(shift == "DM" || shift == "DP"){
//          alert("Shift telah terlampaui, silahkan kontak tim Unilever");
//          setTimeout(function(){
//            $("[data-toggle='popover']").popover('hide');
//          }, 50);
//        }else{
//          setTimeout(function(){
//            this.popover('show');
//          }, 50);
//        }
//      }
//    }else if(date == ncid){
//      setTimeout(function(){
//            this.popover('show');
//          }, 50);
//    }else if(date == nncid){
//      setTimeout(function(){
//            this.popover('show');
//          }, 50);
//    }else{
//      alert("Anda belum bisa melakukan penjadwalan untuk jadwal ini.");
//      setTimeout(function(){
//        $("[data-toggle='popover']").popover('hide');
//      }, 50);
//    }
//  }
// }

function checkDate(date, c_date, shift, c_shift, sn){

  var confirm = 0;
  $.ajax({
      type : "GET",
      url  : "<?php echo base_url(); ?>schedule/check_status_schedule",
      data : {
          "sn" : sn,
          "shift" : shift,
          "date"  : date
      },
      dataType : 'json',
      success:function(resp){
        var data = '';
        if(resp.length > 0){
          for(i = 0 ; i< resp.length ; i++){
          data += 'Tanggal : '+resp[i].rdd + ' Time Slot ' + resp[i].start_time +' - '+ resp[i].end_time + ' Shift ' + resp[i].shift+'\n';
          
        }
        
        alert("Tidak dapat melakukan penjadwalan dikarenakan schedule berikut belum terjadwal :\n"+data+"\n Silahkan kontak tim Unilever");
        }
        confirm = resp.length;
        
      },
      error: function(e){
        alert('Something Wrong');
        console.log(e);
      },async:false
  });

  if(confirm == 0){

    var ll = new Date(c_date);
    var mo = (ll.getMonth() < 10)?"0"+(ll.getMonth()+1):(ll.getMonth()+1);
    var dy = (ll.getDate() < 10)?"0"+(ll.getDate()):(ll.getDate());
    var cid = ll.getFullYear()+"-"+mo+"-"+dy;
    var ncid = new Date(ll.setDate(ll.getDate()+1));
    ndate = (ncid.getDate() < 10)?"0"+ncid.getDate():ncid.getDate();
    ncid = ncid.getFullYear()+"-"+('0' + (ncid.getMonth()+1)).slice(-2)+"-"+ndate;
    
    var nncid = new Date(ll.setDate(ll.getDate()+1));
    nndate = (nncid.getDate() < 10)?"0"+nncid.getDate():nncid.getDate();
    nncid = nncid.getFullYear()+"-"+('0' + (nncid.getMonth()+1)).slice(-2)+"-"+nndate;
    var nnncid = new Date(ll.setDate(ll.getDate()+1));

    nnndate = (nnncid.getDate() < 10)?"0"+nnncid.getDate():nnncid.getDate();
    nnncid = nnncid.getFullYear()+"-"+('0' + (nnncid.getMonth()+1)).slice(-2)+"-"+nnndate;

    if(date < c_date){
      alert("Tanggal telah terlampaui, silahkan kontak tim Unilever");
      setTimeout(function(){
        $("[data-toggle='popover']").popover('hide');
      }, 50);
    }else{
      if(date == cid){
        
        if(c_shift == "DM"){
          setTimeout(function(){
            this.popover('show');
          }, 50);
        }else if(c_shift == "DP"){
          if(shift == "DM"){
            alert("Shift telah terlampaui, silahkan kontak tim Unilever");
            setTimeout(function(){
              $("[data-toggle='popover']").popover('hide');
            }, 50);
          }else{
            setTimeout(function(){
              this.popover('show');
            }, 50);
          }
        }else{
          if(shift == "DM" || shift == "DP"){
            alert("Shift telah terlampaui, silahkan kontak tim Unilever");
            setTimeout(function(){
              $("[data-toggle='popover']").popover('hide');
            }, 50);
          }else{
            setTimeout(function(){
              this.popover('show');
            }, 50);
          }
        }
      }else if(date == ncid){
        setTimeout(function(){
              this.popover('show');
            }, 50);
      }else if(date == nncid){
        setTimeout(function(){
              this.popover('show');
            }, 50);
      }else if(date == nnncid){
        var last_shift = '';
        $.ajax({
          type : "GET",
          url  : "<?php echo base_url(); ?>schedule/check_9shift",
          dataType : 'json',
          success:function(resp){
            var last_shift = resp.shift;

          },error:function(resp){

          },async:false
        });
        
        if(last_shift == 'DP' && shift == 'DS' || shift == 'DM' || shift == 'DP'){
          setTimeout(function(){
              this.popover('show');
            }, 50);
        }else if(last_shift == 'DM' && shift == 'DP' || shift == 'DM'){
          setTimeout(function(){
              this.popover('show');
            }, 50);
        }else if(last_shift == 'DS' && shift == 'DM' || shift == 'DP' || shift == 'DS'){
          setTimeout(function(){
              this.popover('show');
            }, 50);
        }else{
          alert("Anda belum bisa melakukan penjadwalan untuk jadwal ini.");
          setTimeout(function(){
            $("[data-toggle='popover']").popover('hide');
          }, 50);
        }
        
      }else{
        alert("Anda belum bisa melakukan penjadwalan untuk jadwal ini.");
        setTimeout(function(){
          $("[data-toggle='popover']").popover('hide');
        }, 50);
      }
    }
}else{
  setTimeout(function(){
          $("[data-toggle='popover']").popover('hide');
        }, 0);
        
}

}

$('#item_list').select2();

function sumbitForm(){
  $('#saving').show();
  $('#loading').show();
  var conf = confirm('Apakah anda yakin?\nPastikan mengecek semua data sebelum menyimpan.');
  if(conf){
    $("#idSchTbl").submit();
  }else{
    $('#loading').hide();
    $('#saving').hide();  
  }

  
}

function filterInput(state){
  $(".table-input").hide();
  $("."+state).show();
}

function showAllInput(){
  $(".table-input").show();
}

var data_freeze = [];
var reload = false;

$(".activity_start_hour").change(function(e) {

  var el = $(e.target);
  var id = el.data('id');
  var val = el.val();
  var val2 = el.siblings(".activity_start_minute").val();

  var update = false;

  $.each(data_freeze, function(index, value) {
    if (value.id === parseInt(id)) {
      value.start_hour = val;
      value.start_minute = val2;
      update = true;
    }
  });


  if(!update){

    var data = {};
    data.id = id
    data.start_hour = val;
    data.start_minute = val2;

    data_freeze.push(data);

  }
});

$(".activity_start_minute").change(function(e) {
  
  var el = $(e.target);
  var id = el.data('id');
  var val = el.val();
  var val2 = el.siblings(".activity_start_hour").val()

  var update = false;

  $.each(data_freeze, function(index, value) {
    if (value.id === parseInt(id)) {
      value.start_minute = val;
      value.start_hour = val2;
      update = true;
    }
  });


  if(!update){

    var data = {};
    data.id = id
    data.start_minute = val;
    data.start_hour = val2;

    data_freeze.push(data);

  }

});

$(".activity_end_hour").focusout(function(e) {

  var el = $(e.target);
  var id = el.data('id');
  var val = el.val();
  var val2 = el.siblings(".activity_end_minute").val()

  var update = false;

  $.each(data_freeze, function(index, value) {
    if (value.id === parseInt(id)) {
      value.end_hour = val;
      value.end_minute = val2;
      update = true;
    }
  });


  if(!update){

    var data = {};
    data.id = id
    data.end_hour = val;
    data.end_minute = val2;

    data_freeze.push(data);

  }
  
});

$(".activity_end_minute").focusout(function(e) {

  var el = $(e.target);
  var id = el.data('id');
  var val = el.val();
  var val2 = el.siblings(".activity_end_hour").val()

  var update = false;

  $.each(data_freeze, function(index, value) {
    if (value.id === parseInt(id)) {
      value.end_minute = val;
      value.end_hour = val2;
      update = true;
    }
  });


  if(!update){
    var data = {};
    data.id = idz
    data.end_minute = val;
    data.end_hour = val2;

    data_freeze.push(data);

  }

});

$(".save-freeze").click(function(event) {
  freeze_time2();
});

$('#myModal').on('hidden.bs.modal', function () {
  if (reload) {
    location.reload();
  }
})

function freeze_time2(){

  if (data_freeze.length == 0) {
    alert("Silahkan Ubah data terlebih dahulu");
    return;
  }
  
  $.ajax({
    url   : "<?php echo base_url() ?>schedule/freeze_time_update_new",
    method  : "POST",
    data  : {
      data : JSON.stringify(data_freeze)
    },success:function(data){

      if (data == 1) {
        $.each(data_freeze, function(index, val) {
           var el = $(`[data-id='${val.id}']`);
           el.parents('tr').css('background','#d3ffd3');
           el.parents('tr').css('color','black');
           el.parents('tr').fadeIn(1000).fadeOut(1000).fadeIn(1000).fadeOut(1000).fadeIn(1000);

           setTimeout(function() {
            el.parents('tr').css('background','white');
           },5000);
        });

        data_freeze = [];
        reload = true;
        
      }

    },error:function(a){

    }

  });
}

function freeze_time(id, no){
  var start_time = $('#start_hour'+no).val()+':'+$('#start_minute'+no).val();
  var end_time = $('#end_hour'+no).val()+':'+$('#end_minute'+no).val();
  
  $.ajax({
    url   : "<?php echo base_url() ?>schedule/freeze_time_update",
    method  : "POST",
    data  : {
      'id' : id,
      "start_time" : start_time,
      "end_time"  : end_time
    },success:function(a){
      alert("Data time Freeze baru tersimpan");
    },error:function(a){

    }

  });
}

function return_func(return_value, quantity, no){

  if(return_value > quantity){
    $('#received_value').val(quantity);
  }else if(return_value <= quantity && return_value >= 0){
    var hasil = quantity - return_value;
    $('#return_value'+no).val(hasil); 
  }else if(quantity < 0){
    $('#received_value'+no).val('0');
  }
  

}

if("<?php echo $this->uri->segment(2); ?>" == "schedule2" || "<?php echo $this->uri->segment(2); ?>" == "schedule2_search"){

check_periode_changing();
function check_periode_changing(){

    $.ajax({
      type : "GET",
      url : "<?php echo base_url(); ?>routine/check_periode_changed",
      success:function(resp){
        if(resp == 1){
          set_periode_to_zero();
        }else{
          setTimeout(function(){
          check_periode_changing();
        }, 1000);
        }
      },
      error:function(e){
        setTimeout(function(){
          check_periode_changing();
        }, 1000);
        
      }
    });
  }


function set_periode_to_zero(){

  var week = "<?php if(isset($_GET['week'])){ echo $_GET['week']; } ?>";
  var year = "<?php if(isset($_GET['year'])){ echo $_GET['year']; } ?>";
  var category = '<?php if(isset($_GET['category'])){ echo $_GET['category']; } ?>';
  $.ajax({
    type : "GET",
    url : "<?php echo base_url(); ?>routine/set_to_zero_periode",
    success:function(resp){
      alert("Admin telah mengubah jam kerja scheduling, halaman otomatis di reload");
      window.location=("?category="+category+"&year="+year+"&week="+week+"");
    },
    error:function(reso){
      alert("Something Error");
    }
  });
}

$('.table-fix').on('scroll', function() {
    $(this).find('thead th')
      .css('transform', 'translateY('+ this.scrollTop +'px)')
      .css({
        position: "relative",
        "z-index" : 3
      });;
    $(this).find('tr td:nth-child(1)')
      .css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
      .css({
        "z-index" : 2
      });
});

}

function select_truck(a){
  $.ajax({
    url: "<?php echo base_url() ?>truck_management/view_truck",
    data:{
      "id" : a
    },
    method:"GET",
    dataType:"JSON",
    success:function(e){
      //$("#area-cnt-pallet").html("Slot Occupancy : <b> "+material_count+" / "+32+"</b>");
      //capacity = 20;
      console.log(e);
    },error:function(e){
      alert("Something Error");
    },async:false
  });
}

function update_vendor_batch(val, id){
  $.ajax({
    url: "<?php echo base_url() ?>schedule/update_vendor_batch",
    data:{
      "id"  : id,
      "value" : val
    },
    method:"GET",
    success:function(e){
      
    },error:function(e){
      alert("Something Error");
    },async:false
  });
}

function exp_date(a, no, id, batch){
  var exp = $('#exp'+no).val();
  var time_now =  moment(a).format('YYYY-MM-DD');
  var exp_date = moment(a, 'YYYY-MM-DD').add(exp,'d');
  var new_date = new Date(exp_date);
  var month = new_date.getMonth()+1;
  var year = new_date.getFullYear();
  var date = new_date.getDate();
  if(month < 10){
    month = '0'+month;
  }

  if(date < 10){
    date = '0'+date;
  }
  var date_now = "<?php echo date('Y-m-d'); ?>";
  console.log(time_now);
  console.log(date_now);
  if(time_now <= date_now){
    var new_exp_date = year + '-' + month + '-' + date;
    $('#exp_date'+no).val(new_exp_date);
    $.ajax({
      url: "<?php echo base_url() ?>schedule/update_batch",
      data:{
        "id" : id,
        "prod_date" : a,
        "exp_date" : new_exp_date
      },success:function(e){

      },error:function(e){
        alert("Something Error");
      }
    });
    console.log(new_exp_date);
  }else{
    alert("Prod date tidak dapat dipilih coba pilih lain hari");
    $('#exp_date'+no).val("00-00-00");
    $('#prod_date'+no).val("00-00-00");
  }


  
}

</script>