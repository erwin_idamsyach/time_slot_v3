<style type="text/css">
  .box-header {
    color: #ffffff !important;
    background-color: #31559F !important;
    text-transform: uppercase;
}
.box-header {
    
    display: block;
    padding: 10px;
    position: relative;
}

/*.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 3px 8px;
    vertical-align: middle;
    border-color: #ddd;
}

.table>thead>tr>th {
    border: 1px solid #6A6A6A !important;
    background-color: rgb(255,204,102);
    color: black;
}*/

.box .box-body {
    background: #FFF;
}

.box-body {
    max-width: 100%;
    overflow: auto;
    padding: 15px;
}
.box-body {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    padding: 10px;
}

.form-check, label {
    font-size: 14px;
    line-height: 0;
    color: #ffffff;
    font-weight: 400;
}
.fa-times{
  color:red;
}

.fa-arrow-up{
  color:green;
}

b{
  font-size:12px;
}

.bg__green{
  background: #87FB87;
}
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="far fa-address-card"></i>
                    </div>
                    <h4 class="card-title">Delivery Form</h4>
                  </div>
                  <div class="card-body ">

                    <form action="<?php echo base_url(); ?>schedule/save_schedule" method="post" enctype="multipart/form-data">
                <input type="hidden" name="slot_number" value="<?php echo $this->uri->segment(3); ?>">
                
                <div class="row">
                  <div class="col-md-2">
                    <div class="form-group">
                      <b>RDD</b><br>
                      <b>
                        <?php
                      $date = date_create($this->uri->segment(4));
                      $dfns = date_format($date, "D, d M Y");
                      echo $dfns;
                        ?>
                      </b>
                    </div>
                  </div>
                  
                  <div class="col-md-2">
                    <div class="form-group">
                      <b>Time Slot</b><br>
                        <b>
                          <?php
                            $this->Schedule_model->getTimeSlotById($this->uri->segment(5));
                          ?>
                        </b>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="form-group">
                      <b>Date Time</b><br>
                        <p id='jam'></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-4" style="margin-top:10px">
                    <select required class="form-control select2-init" id="truck_no" style="margin-top:20px" name="truck_no" data-style="select-with-transition" title="Select Truck" data-size="7">
                      <option value=''> Select Truck</option>
                      <?php foreach ($truck as $get) {
                      ?>
                      <option value="<?php echo $get->id; ?>"><?php echo $get->police_number.' '.$get->truck_name; ?></option>
                      <?php
                      } ?>
                    </select>
                  </div>

                  <div class="col-md-4" style="margin-top:10px">
                    <select required class="select2-init form-control" id="driver_name" name="driver_name" data-style="select-with-transition" title="Select Driver" data-size="7">
                      <option value=''> Select Driver</option>
                      <?php foreach ($driver as $get) {
                      ?>
                      <option value="<?php echo $get->id; ?>"><?php echo $get->driver_name; ?></option>
                      <?php
                      } ?>
                    </select>
                    
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Phone Number</label>
                      <input type="number" name="phone_number" value="" id="phone_number" class="form-control" required="">
                      <input type="hidden" name="curr_date" value="<?php echo $this->uri->segment(4) ?>">
                    </div>
                  </div>
                </div>
                <div class="form-inline">
                  <input type="hidden" name="id_schedule" value="<?php echo $this->uri->segment(3) ?>">
                  <input type="hidden" name="rdd" value="<?php echo $this->uri->segment(4) ?>">
                  <input type="hidden" name="id_time_slot" value="<?php echo $this->uri->segment(5) ?>">
                  
                  <label id="check-error"></label>
                  <input type="hidden" value="" id="count-pallet">
                  <button type="button" class="btn btn-primary btn-save" id="submitPlan" onclick="cekPlanned()">SUBMIT</button>
                  <div class="area-preloader"></div>
                  <button hidden disabled type="submit" id="submitz"></button>
                </div>
              
                  </div>
                </div>
              </div>
          </div>
    </div>
  </form>

  <div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-info card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-pallet"></i>
                    </div>
                    <h4 class="card-title">Material List</h4>
                    <h4 id="area-cnt-pallet" style="color:black">Slot Occupancy : .../<?php echo $full_pallet; ?></h4>
                  </div>
                  <div class="card-body ">
                  <div style="width: 100%;">
                    <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-matlist" id="tableId" style="width: 1600px;overflow-x: scroll">
                <thead style="color:white; background-color:#31559F;font-weight: 650">
                  <tr>
                    <th class="text-center">
                      <?php if($category == "IBD"){ ?>
                        IBD /
                      <?php } ?>
                      PO Number
                    </th>
                    <?php
                    if($category == "IBD"){
                      ?>
                    <th class="text-center">Bill of Ladding</th>
                      <?php
                    }
                    ?>
                    <th class="text-center">Material Code</th>
                    <th class="text-center">Material Name</th>
                    <th class="text-center">Date / Shift</th>
                    <th class="text-center">Req. Pallet</th>
                    <th class="text-center">Send Qty</th>
                    <th class="text-center">Prod Date</th>
                    <th class="text-center">Exp Date</th>
                    <th class="text-center">Vendor Batch</th>
                    <th class="text-center">DN No</th>
                    <th class="text-center">File DN</th>
                    <th class="text-center">File COA</th>
                    <th class="text-center">*</th>
                  </tr>
                </thead>
                <tbody id="area-list">
                  <?php
                  $no = 0;
                  foreach($data_list->result() as $get){
                    ?>
                  <tr>
                    <td class="text-center">
                      <?php 
                      echo $get->po_number;
                      if($get->reference_po != ""){
                        echo " / ".$get->reference_po;
                      }
                      ?>
                      <input type="text" hidden value="<?php echo $get->idz; ?>" id="idz<?php echo $no; ?>" ></td>
                    <?php
                    if($category == "IBD"){
                      ?>
                    <td>
                      <?php echo $get->bill_of_lading; ?>
                    </td>
                      <?php
                    }
                    ?>
                    <td><?php echo $get->material_code ?><input type="text" hidden value="<?php echo $get->material_code; ?>" id="material_code" ></td>
                    <td><?php echo $get->material_name ?></td>
                    <td>
                      <?php
                      $r_date = date_create($get->requested_delivery_date);
                      $df = date_format($r_date, "d-m-Y");
                      echo $df." / ".$get->shift
                      ?>
                    </td>
                    <td width="35px">
                      <input type="text" id="quantity<?php echo $no; ?>" hidden value="<?php echo $get->quantity; ?>" name="">
                      <input type="text" id="id<?php echo $no; ?>" hidden value="<?php echo $get->id; ?>" name="">
                      <input type="text" class="form-control inp-pallet" value="<?php echo $get->quantity ?>" onchange="cutPallet($('#id<?php echo $no; ?>').val(), this.value, <?php echo $get->idz ?>, $('#quantity<?php echo $no;?>').val(),'quantity<?php echo $no; ?>' ) ">
                      <input type="text" hidden value="<?php echo $get->quantity; ?>" id="qty_material<?php echo $no++; ?>" >
                    </td>
                    <td><?php echo ($get->uom_plt * $get->quantity)." ".$get->uom; ?></td>
                    <td>
                      <input type="date" data-date-format="YYYY MM DD" id="prod_date<?php echo $no; ?>" value="<?php echo $get->prod_date ?>" style="width: 130px" value="" onchange="exp_date(this.value, <?php echo $no; ?>, <?php echo $get->id; ?>)" name="prod_date" id="prod_date<?php echo $no; ?>" class="form-control prod_date">
                    </td>
                    <td>
                      <input type="text"  hidden value="<?php echo $get->exp; ?>" name="exp" id="exp<?php echo $no; ?>">
                      <input readonly value="<?php echo $get->exp_date; ?>" id="exp_date<?php echo $no; ?>" style="background-color:white; width: 130px" type="date" name="exp_date" class="form-control">
                    </td>
                    <td width="140px">
                      <input type="text" maxlength="25" id="<?php echo $get->id; ?>" value="<?php echo $get->vendor_batch; ?>" name="vendor_batch[]" onchange="update_vendor_batch(this.value, $(this).attr('id') )" class="form-control vendor_batch">
                    </td>
                    <td width="140px">
                      <input type="text" class="form-control i_do_number" data-id="do_<?php echo $get->id ?>" value="<?php echo $get->do_number ?>">
                    </td>
                    <td class="div-dn area-dn-<?php echo $get->id ?> <?php echo ($get->invoice != '')?'bg__green':'' ?>">
                      <button class="btn btn-primary btn-sm" type="button" onclick="$('#file_<?php echo $get->id ?>').click();">
                        <i class="fa fa-upload"></i>
                      </button>
                      <input type="file" name="upload_dn" id="file_<?php echo $get->id ?>" style="display: none;" onchange="submit_file('DN', <?php echo $get->id ?>, '<?php echo $get->schedule_number ?>')">
                    </td>
                    <td class="div-coa area-coa-<?php echo $get->id ?> <?php echo ($get->coa != '')?'bg__green':'' ?>">
                      <button class="btn btn-warning btn-sm" type="button" onclick="$('#file_coa_<?php echo $get->id ?>').click();">
                        <i class="fa fa-upload"></i>
                      </button>
                      <input type="file" name="upload_dn" id="file_coa_<?php echo $get->id ?>" style="display: none;" onchange="submit_file('COA', <?php echo $get->id ?>, '<?php echo $get->schedule_number ?>')">
                    </td>
                    <td class="text-center" style="width: 10px">
                      <button class="btn btn-link clr-red btn-sm" onclick="moveMaterial(<?php echo $get->id; ?>, '<?php echo $sn; ?>')">
                        <i class="fa fa-times"></i>
                      </button>
                    </td>
                  </tr>
                    <?php
                  }
                  ?>
                  </tbody>
                </table>
              </div>
                  </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="content">
  <div class="content">
    <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-danger card-header-icon">
                    <div class="card-icon">
                      <i style="font-size:30px" class="fas fa-dolly-flatbed"></i>
                    </div>
                    <h4 class="card-title">Material Pool</h4>
                  </div>
                  <div class="card-body ">

                    <div class="box-body" id="area-pool">
                  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon">
                <i style="font-size:30px" class="fas fa-boxes"></i>
              </div>
              <h4 class="card-title">Find Material From Same RDD & Advance Delivery</h4>
            </div>
            <div class="card-body ">
                <div <?php echo ($this->uri->segment(7) == 'adt')?"hidden":'' ?> class="col-md-12">
                  <div class="box"> 
                    <div class="box-body" id="area-same-rdd"></div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>