<option value=''>--SELECT--</option>
<?php
$sisa = 3;
foreach($ts_sisa as $d){

  $st = date_create($d->start_time);
  $et = date_create($d->end_time);
  $st = date_format($st, "H:i");
  $et = date_format($et, "H:i");
  $sissa = $d->sisa;
  if($sissa < 0){
    $sissa = 0;
  }
  ?>
<option <?php if($sissa <= 0){echo "disabled";} ?> value='<?php echo $d->id ?>'><?php echo $st.' - '.$et ." ({$sissa} slot tersisa)"; ?></option>
  <?php
}
?>