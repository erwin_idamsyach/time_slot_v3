<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
  <thead style="color:white; background-color:#31559F;font-weight: 650">
    <tr>
      <th class="text-center">
        <?php if($category == "IBD"){ echo "IBD / "; }?>
        PO Number
      </th>
      <?php if($category == "IBD"){
        ?>
      <th class="text-center">Bill of Ladding</th>
        <?php
      }
      ?>
      <th class="text-center">Material Code</th>
      <th class="text-center">Material Name</th>
      <th class="text-center">Date / Shift</th>
      <th class="text-center">Req. Pallet</th>
      <th width="70" class="text-center">Qty</th>
      <th class="text-center">*</th>
    </tr>
  </thead>
  <tbody id="area-list">
    <?php
    foreach($data_list as $get){
      ?>
    <tr>
      <td class="text-center">
        <?php echo $get->po_number; echo ($get->reference_po != "")?" / ".$get->reference_po:""; ?>
      </td>
      <?php if($category == "IBD"){ ?>
      <td><?php echo $get->bill_of_lading; ?></td>
        <?php } ?>
      <td><?php echo $get->material_code ?></td>
      <td><?php echo $get->material_name ?></td>
      <td>
        <?php
        $r_date = date_create($get->requested_delivery_date);
        $df = date_format($r_date, "d-m-Y");
        echo $df." / ".$get->shift
        ?>
      </td>
      <td width="25px">
        <?php echo $get->req_pallet ?>
      </td>
      <td><?php echo $get->qty ?></td>
      <td class="text-center">
        <button class="btn btn-link clr-green" onclick="addMaterial(<?php echo $get->id_schedule; ?>, '<?php echo $sn; ?>')">
          <i class="fa fa-arrow-up"></i>
        </button>
      </td>
    </tr>
      <?php
    }
    ?>
  </tbody>
</table>
</div>