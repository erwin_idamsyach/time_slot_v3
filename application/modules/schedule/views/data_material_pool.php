<style type="text/css">
    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 0px 8px;
    vertical-align: middle;
    border-color: #ddd;
}

</style>

<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
  <thead style="color:white; background-color:#31559F;font-weight: 650">
    <tr>
      <th class="text-center">
        <?php if($category == "IBD"){ echo "IBD / "; }?>
        PO Number
      </th>
      <?php if($category == "IBD"){
        ?>
      <th class="text-center">Bill of Ladding</th>
        <?php
      }
      ?>
      <th class="text-center">Material Code</th>
      <th class="text-center">Material Name</th>
      <th class="text-center">Date / Shift</th>
      <th class="text-center">Req. Pallet</th>
      <th class="text-center" width="70">Qty</th>
      <th class="text-center">*</th>
    </tr>
  </thead>
  <tbody id="area-list">
    <?php
    foreach($data_list as $get){
      /*if($get->schedule_number == $sn){
        continue;
      }*/
      ?>
    <tr>
      <td style="padding:0px 8px" class="text-center"><?php echo $get->po_number; echo ($get->reference_po != "")?" / ".$get->reference_po:""; ?></td>
      <?php if($category == "IBD"){ ?>
      <td><?php echo $get->bill_of_lading; ?></td>
        <?php } ?>
      <td><?php echo $get->material_code ?></td>
      <td><?php echo $get->material_name ?></td>
      <td>
        <?php
        $r_date = date_create($get->requested_delivery_date);
        $df = date_format($r_date, "d-m-Y");
        echo $df." / ".$get->shift
        ?>
      </td>
      <td width="25px" class="text-right">
        <?php echo ($get->sisa == null)?$get->req_pallet:$get->sisa ?>
      </td>
      <td width="55px" class="text-right">
        <?php echo ($get->sisa == null)?$get->req_pallet*$get->uom_plt:$get->sisa*$get->uom_plt; echo " ".$get->uom; ?>
      </td>
      <td class="text-center">
        <button id="material_list_<?php echo $get->id;?>" class="btn btn-link clr-green" onclick="addMaterialToList('<?php echo $sn; ?>', <?php echo $get->id ?>, <?php echo ($get->sisa == null)?$get->req_pallet:$get->sisa ?>, $(this).attr('id'))">
          <i class="fa fa-arrow-up"></i>
        </button>
      </td>
    </tr>
      <?php
    }
    ?>
  </tbody>
</table>
</div>