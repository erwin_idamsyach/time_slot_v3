<?php

require_once APPPATH.'third_party/spout-master/src/Spout/Autoloader/autoload.php';
use swiftmailer\swiftmailer\swiftmailer;
class Schedule extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		//$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		$this->load->model('Schedule_model');
		isLogin();
	}

	public function index(){
		date_default_timezone_set("Asia/Bangkok");
		$arr = array();
		$arr_shift = array(
			array(
				'shift_name' => "Dinas Malam",
				"shift_alias" => "DM"
			),
			array(
				'shift_name' => "Dinas Pagi",
				"shift_alias" => "DP"
			),
			array(
				'shift_name' => "Dinas Sore",
				"shift_alias" => "DS"
			)
		);
		$dSchedule = array();
		$yearnow = date('Y');

		$ddate = date('Y-m-d');
		$date = new DateTime($ddate);
		$weeks = $date->format("W");

		$week = (null === $this->input->get('week'))?$weeks:$this->input->get('week');
		$year = (null === $this->input->get('year'))?$yearnow:$this->input->get('year');
		$arrDate = $this->getDateList($week, $year);
		$date = (null === $this->input->get('date'))?$arrDate[0]:$this->input->get('date');
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$user = $this->session->userdata('sess_role_no');
		$a = 0;
		$c_time = date('H:i:s');
		$getShift = $this->db->query("SELECT id,shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$shift = $getShift->shift;
		//echo json_encode($arrDate);

		while($a < count($arrDate)){
			$dSchedule[$arrDate[$a]] = array();

			if($user == 3){
				$vtype = $this->session->userdata("sess_vendor_type");

				if($vtype == "PM"){
					$d = $this->setSchedule($arrDate[$a], $vendor_code, $user, $shift, $date);
				}else{
					$d = $this->setScheduleWeighing($arrDate[$a], $vendor_code, $user, $shift, $date);
				}
			}else if($user == 1){
				//$d = $this->setScheduleWeighing($arrDate[$a], $user, $shift, $date);
			}else{
				//$d = $this->setSchedule($arrDate[$a], $vendor_code, $user);
			}
			$dataSchedule = $this->Schedule_model->getScheduleNew($arrDate[$a], $vendor_code, $user);
			array_push($dSchedule[$arrDate[$a]], $dataSchedule->result());
			$a++;

		}

		//echo "<pre>".json_encode($dSchedule, JSON_PRETTY_PRINT)."</pre>"; die();

		$dataTimeSlot = $this->Schedule_model->getTimeSlot();
		$dataTimeSlot2 = $this->Schedule_model->getTimeSlot2();
		$dataTimeSlot_adt = $this->Schedule_model->getTimeSlot_adt($date);

		$ts_sisa = $this->Schedule_model->getAdtTimeSlotWithSisa($date);
		$arr = json_encode($dSchedule, JSON_PRETTY_PRINT);
		/*echo "<pre>".$arr."</pre>";die();*/
		$get = json_decode($arr, true);
		
		$vendor = $this->db->query("SELECT vendor_code, vendor_alias, vendor_name FROM skin_master.ms_supplier");
		//echo "<pre>".json_encode($get, JSON_PRETTY_PRINT)."</pre>"; die();

		$total_booking = $this->db->query("SELECT COUNT(id) AS booking_count FROM tb_schedule_detail WHERE rdd BETWEEN '$arrDate[0]' AND '$arrDate[6]' AND status != '0' AND vendor_code LIKE '%$vendor_code%' ")->row()->booking_count;

		$booking_day = $this->db->query("SELECT COUNT(id) AS booking_count FROM tb_schedule_detail WHERE rdd = '$date' AND status != '0' AND vendor_code LIKE '%$vendor_code%'")->row()->booking_count;

		$total_material = $this->db->query("SELECT SUM(req_pallet) AS total_material FROM tb_rds_detail WHERE requested_delivery_date BETWEEN '$arrDate[0]' AND '$arrDate[6]' AND vendor_code LIKE '%$vendor_code%'")->row()->total_material;

		$material_day = $this->db->query("SELECT SUM(req_pallet) AS total_material FROM tb_rds_detail WHERE requested_delivery_date = '$date' AND vendor_code LIKE '%$vendor_code%'")->row()->total_material;

		$total_arrived = $this->db->query("SELECT COUNT(id) AS arrived_count FROM tb_schedule_detail WHERE rdd BETWEEN '$arrDate[0]' AND '$arrDate[6]' AND status >= '2' AND vendor_code LIKE '%$vendor_code%'")->row()->arrived_count;

		$arrived_day = $this->db->query("SELECT COUNT(id) AS arrived_count FROM tb_schedule_detail WHERE rdd = '$date' AND status >= '2' AND vendor_code LIKE '%$vendor_code%'")->row()->arrived_count;

		$total_received = $this->Schedule_model->total_received($arrDate[0], $arrDate[6]);
		$received_day = $this->Schedule_model->received_day($date);

		$role 						= $role = $this->session->userdata('sess_role_no');
		$data['week'] 				= $week;
		$data['year']				= $year;
		$data['vendor_code'] 		= $vendor_code;
		$data['vendor']				= $vendor;
		$data['data_time_slot'] 	= $dataTimeSlot;
		$data['data_time_slot2'] 	= $dataTimeSlot2;
		$data['date_list']			= $arrDate;
		$data['data_shift']			= $arr_shift;
		$data['data']		 		= $get;
		$data['c_date']				= $date;
		$data['c_shift']			= $shift;
		$data['time_slot_now']		= $getShift->id;
		$data['role']				= $role;
		$data['total_booking']		= $total_booking;
		$data['booking_day']		= $booking_day;
		$data['total_material']		= $total_material;
		$data['material_day']		= $material_day;
		$data['total_arrived']		= $total_arrived;
		$data['arrived_day']		= $arrived_day;
		$data['total_received']		= $total_received;
		$data['received_day']		= $received_day;
		$data['ts_sisa']			= $ts_sisa;
		
		getHTML('schedule/index', $data);
	}

	public function setSchedule($date, $vendor_code, $user, $shift_now, $datez){
		$arra = array();
		$datenow = date('Y-m-d');
		$arr_shift = array(
			array(
				'shift_name' => "Dinas Malam",
				"shift_alias" => "DM"
			),
			array(
				'shift_name' => "Dinas Pagi",
				"shift_alias" => "DP"
			),
			array(
				'shift_name' => "Dinas Sore",
				"shift_alias" => "DS"
			)
		);

		foreach($arr_shift as $as){
			$dataToday = $this->Schedule_model->getSchedule($date, $vendor_code, $user, $as['shift_alias']);
			
			//dump($dataToday->result());
			$counter = 0;
			$b = 0;
			$isSaved = false;
			if(count($dataToday->result()) > 0){
				$getLast = $this->getScheduleNumber();
				foreach($dataToday->result() as $get){
					if($counter >= $get->row_number){
						$getLast = $this->getScheduleNumber();
						$isSaved = false;
					}else{
						$getLast = $getLast;
					}
					$shift = $get->shift;
					$rdd = $get->requested_delivery_date;
					$sn = $getLast;
					if($vendor_code == $get->vendor_code){
						$arr = array(
							'id_schedule' => $get->id,
							'vendor_code' => $get->vendor_code,
							'schedule_number' => $getLast,
							'rdd' => $get->requested_delivery_date,
							'quantity' => $get->req_pallet
						);
						if($get->requested_delivery_date < $datenow){
							$this->db->insert('tb_scheduler', $arr);
						}else if($get->requested_delivery_date > $datenow){
							$this->db->insert('tb_scheduler', $arr);
						}else if($get->requested_delivery_date == $datenow){
							if($shift_now == 'DP' && $shift != 'DM'){
								$this->db->insert('tb_scheduler', $arr);
							}else if($shift_now == 'DS' && $shift == 'DS'){
								$this->db->insert('tb_scheduler', $arr);
							}else if($shift_now == 'DM'){
								$this->db->insert('tb_scheduler', $arr);
							}
						}else{

						}
					}

					if($vendor_code == $get->vendor_code){
						$this->db->where('id', $get->id);
		                $this->db->update('tb_rds_detail', array(
		                    'is_schedule' => 1
		                ));	
					}

	                if(!$isSaved){
	                	$time_slot_shift = '';
	                	$id_max = '';
						if($shift == 'DP'){
							$time_slot_shift = 6;
							$id_max = 10;
						}else if($shift == 'DM'){
							$time_slot_shift = 1;
							$id_max = 16;
						}else{
							$time_slot_shift = 12;
							$id_max = 15;
						}

						$getData = $this->db->query("SELECT tb_schedule_detail.id_time_slot, count(*) as COUNT FROM `tb_schedule_detail` inner join ms_time_slot on tb_schedule_detail.id_time_slot = ms_time_slot.slot_number WheRE tb_schedule_detail.rdd = '$rdd' AND ms_time_slot.shift = '$shift' GROUP BY tb_schedule_detail.id_time_slot ORDER BY ms_time_slot.ordering DESC")->row();

						if(isset($getData->id_time_slot)){
							if($getData->COUNT == 4){
								$time_slot_shift = $getData->id_time_slot + 1;
							}else{
								$time_slot_shift = $getData->id_time_slot;
							}
						}

						if($time_slot_shift > $id_max){
							if($shift == "DP"){
								$time_slot_shift = 11;
								$shift = "DS";
							}else if($shift == "DS"){
								$rdd = date('Y-m-d', strtotime($rdd."+1 day"));
								$time_slot_shift = 16;
								$shift ="DM";
							}else if($shift == "DM"){
								$time_slot_shift = 5;
								$shift ="DP";
							}

							$getData = $this->db->query("SELECT tb_schedule_detail.id_time_slot, count(*) as COUNT FROM `tb_schedule_detail` inner join ms_time_slot on tb_schedule_detail.id_time_slot = ms_time_slot.slot_number WheRE tb_schedule_detail.rdd = '$rdd' AND ms_time_slot.shift = '$shift' GROUP BY tb_schedule_detail.id_time_slot ORDER BY ms_time_slot.ordering DESC")->row();

							if(isset($getData->id_time_slot)){
								if($getData->COUNT >= 4){
									$time_slot_shift = $getData->id_time_slot + 1;
								}else{
									$time_slot_shift = $getData->id_time_slot;
								}
								if($time_slot_shift > 16){
									$time_slot_shift = 1;
								}
							}
						}
						$tsi = array(
							'vendor_code'		=> $get->vendor_code,
							'schedule_number' 	=> $getLast,
							'rdd' 				=> $rdd,
							'id_time_slot' 		=> $time_slot_shift,
						);
						$this->db->insert('tb_schedule_detail', $tsi);
						$isSaved = true;
	                }
					$counter = $get->row_number;
					$b++;
				}
			}else{

			}
		}
	}

	public function setScheduleWeighing($date, $vendor_code, $user, $shift_now, $date){
		$arra = array();

		$datenow = date('Y-m-d');
		$arr_shift = array(
				array(
					'shift_name' => "Dinas Malam",
					"shift_alias" => "DM"
				),
				array(
					'shift_name' => "Dinas Pagi",
					"shift_alias" => "DP"
				),
				array(
					'shift_name' => "Dinas Sore",
					"shift_alias" => "DS"
				)
			);


		foreach($arr_shift as $as){
			$dataToday = $this->Schedule_model->getScheduleWeighing($date, $vendor_code, $user, $as['shift_alias']);
			
			$counter = 0;
			$b = 0;
			if(count($dataToday->result()) > 0){
				foreach($dataToday->result() as $get){
					$isSaved = false;
					$getLast = $this->getScheduleNumber();
					$shift = $get->shift;
					$rdd = $get->requested_delivery_date;
					$sn = $getLast;
					if($vendor_code == $get->vendor_code){
						$arr = array(
							'id_schedule' => $get->id,
							'vendor_code' => $get->vendor_code,
							'schedule_number' => $getLast,
							'rdd' => $get->requested_delivery_date,
							'order_qty' => $get->qty
						);
						if($get->requested_delivery_date < $datenow){

						}else if($date > $datenow){
							$this->db->insert('tb_scheduler', $arr);
						}else if($date == $datenow){
							if($shift_now == 'DP' && $shift != 'DM'){
								$this->db->insert('tb_scheduler', $arr);
							}else if($shift_now == 'DS' && $shift == 'DS'){
								$this->db->insert('tb_scheduler', $arr);
							}else if($shift_now == 'DM'){
								$this->db->insert('tb_scheduler', $arr);
							}
						}
					}
					

					if($vendor_code == $get->vendor_code){
						$this->db->where('id', $get->id);
		                $this->db->update('tb_rds_detail', array(
		                    'is_schedule' => 1
		                ));	
					}

	                if(!$isSaved){
	                	$time_slot_shift = '';
	                	$id_max = '';
						if($shift == 'DP'){
							$time_slot_shift = 6;
							$id_max = 10;
						}else if($shift == 'DM'){
							$time_slot_shift = 1;
							$id_max = 16;
						}else{
							$time_slot_shift = 12;
							$id_max = 15;
						}

						$getData = $this->db->query("SELECT
										tb_schedule_detail.id_time_slot,
										count(*) AS COUNT
									FROM
										`tb_schedule_detail`
									INNER JOIN ms_time_slot ON tb_schedule_detail.id_time_slot = ms_time_slot.slot_number
									WHERE
										tb_schedule_detail.rdd = '$rdd'
									AND ms_time_slot.shift = '$shift'
									AND tb_schedule_detail.is_weighing = 1
									GROUP BY
										tb_schedule_detail.id_time_slot
									ORDER BY
										ms_time_slot.ordering DESC")->row();

						if(isset($getData->id_time_slot)){
							if($getData->COUNT == 1){
								$time_slot_shift = $getData->id_time_slot + 1;
							}else{
								$time_slot_shift = $getData->id_time_slot;
							}
						}

						if($time_slot_shift > $id_max){
							if($shift == "DP"){
								$time_slot_shift = 11;
								$shift = "DS";
							}else if($shift == "DS"){
								$rdd = date('Y-m-d', strtotime($rdd."+1 day"));
								$time_slot_shift = 16;
								$shift ="DM";
							}else if($shift == "DM"){
								$time_slot_shift = 5;
								$shift ="DP";
							}

							$getData = $this->db->query("SELECT
										tb_schedule_detail.id_time_slot,
										count(*) AS COUNT
									FROM
										`tb_schedule_detail`
									INNER JOIN ms_time_slot ON tb_schedule_detail.id_time_slot = ms_time_slot.slot_number
									WHERE
										tb_schedule_detail.rdd = '$rdd'
									AND ms_time_slot.shift = '$shift'
									AND tb_schedule_detail.is_weighing = 1
									GROUP BY
										tb_schedule_detail.id_time_slot
									ORDER BY
										ms_time_slot.ordering DESC")->row();

							if(isset($getData->id_time_slot)){
								if($getData->COUNT == 1){
									$time_slot_shift = $getData->id_time_slot + 1;
								}else{
									$time_slot_shift = $getData->id_time_slot;
								}
								if($time_slot_shift > 16){
									$time_slot_shift = 1;
								}
							}

						}

						$tsi = array(
							'vendor_code'		=> $get->vendor_code,
							'schedule_number' 	=> $getLast,
							'rdd' 				=> $rdd,
							'id_time_slot' 		=> $time_slot_shift,
							'is_weighing'		=> 1
						);
						$this->db->insert('tb_schedule_detail', $tsi);
						$isSaved = true;
	                }

					$b++;
				}
			}else{

			}
		}
	}

	public function setTimeFreezeOff($sn){
		$data = $this->db->query("SELECT id_schedule_group,created_at FROM tb_delivery_detail where id_schedule_group = '$sn' ")->row();
		if(isset($data)){

			/*if(Date('Y-m-d H:i:s') > $data->created_at ){
				$this->db->query("UPDATE tb_schedule_detail SET status='1' WHERE schedule_number = '$sn' ");
			}*/
		}

	}

	public function view_schedule(){
		$sn = $this->uri->segment(3);
		$getData = $this->db->query("SELECT a.vendor_code, a.rdd, a.id_time_slot, b.shift, a.is_weighing FROM tb_schedule_detail AS a INNER JOIN ms_time_slot AS b on a.id_time_slot = b.id WHERE schedule_number ='$sn' ")->row();
		$vendor_code = $getData->vendor_code;
		//$vendor_code = $this->session->userdata("sess_vendor_code");
		$req_data = $this->Schedule_model->getScheduleDetail($sn);
		$data['vendor_code'] = $vendor_code;
		$truck = $this->db->query("SELECT id,police_number, truck_name FROM ms_truck WHERE vendor_code = '$vendor_code' AND status = 0")->result();
		$driver = $this->db->query("SELECT id, driver_name FROM ms_driver WHERE vendor_code = '$vendor_code' AND status = 0 ")->result();
		if($getData->shift == 'DM'){
			$no_shift = 'M';
		}else if($getData->shift == 'DP'){
			$no_shift = 'P';
		}else{
			$no_shift = 'S';
		}
		$c_time = date('H:i:s');
		$getShift = $this->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row()->shift;
		$check_batch_null = $this->db->query("SELECT a.batch_number FROM tb_scheduler AS a INNER JOIN tb_schedule_detail AS b ON a.schedule_number = b.schedule_number INNER JOIN ms_time_slot AS c ON b.id_time_slot = c.id WHERE b.rdd = '$getData->rdd' AND RIGHT(batch_number,1) = '$no_shift' Order By a.batch_number DESC")->row();
		$shift = $getShift;
		$getFullPallet = $this->db->query("SELECT full_capacity FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->row()->full_capacity;

		$data['full_pallet'] = $getFullPallet;
		$data['shift'] = $shift;
		$data['truck'] = $truck;
		$data['driver'] = $driver;
		$data['sn'] = $sn;
		$data['data_list'] = $req_data['data_list'];
		$data['array_id'] = $req_data['array_id'];
		$data['is_weighing'] = $getData->is_weighing;
		$data["vendor_code"] = $vendor_code;
		$data['category'] = $this->getCategory_sch($sn);
		if($getData->is_weighing == 1){
			getHTML('schedule/view_weighing', $data);
		}else{
			getHTML('schedule/view_detail', $data);
		}
	}

	public function move_schedule(){
		$id = $this->input->get('id');
		$this->Schedule_model->moveSchedule($id);
		return true;
	}

	public function update_vendor_batch(){
		$value 	= $this->input->get('value');
		$id 	= $this->input->get('id');
		$update = $this->db->query("UPDATE tb_scheduler SET vendor_batch = '$value' WHERE id = '$id' ");
		if($update){
			return true;
		}else{
			return false;
		}
	}

	public function arrived(){
		date_default_timezone_set("Asia/Bangkok");
		$id = $this->input->post('id');
		$url = $this->input->post('url');
		$arrive_date = $this->input->post('arrive_date');
		$arrive_hour = $this->input->post('arrived_hour');
		$arrive_minute = $this->input->post('arrived_minute');
		$time = $arrive_hour.":".$arrive_minute;
		$arrived_id_time_slot = $this->input->post('arrived_id_time_slot');
		$get_material = $this->db->query("SELECT a.id FROM tB_rds_detail AS a INNER JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE b.schedule_number = '$id' ")->result();
		$user_id = $this->session->userdata('sess_id');
		$date_now = Date('Y-m-d H:i');

		foreach ($get_material as $get) {
			$this->db->query("UPDATE tb_rds_detail SET update_at = '$date_now', update_by = '$user_id' WHERE id = '$get->id' ");
		}

		$arrive_time = date('H:i:s',strtotime($time));
		$this->db->query("UPDATE tb_schedule_detail SET status = '2' WHERE schedule_number = '$id' ");
		$user_id = $this->session->userdata('sess_id');
		$data = array(
			"arrive_date"	=> $arrive_date,
			"arrive_time"	=> $arrive_time,
			"arrived_by"	=> $user_id,
			"arrived_id_time_slot" => $arrived_id_time_slot
		);

		$this->db->where("id_schedule_group",$id);
		$this->db->update("tb_delivery_detail", $data);

		$check_backlog = $this->db->query("SELECT id FROM tb_delivery_detail WHERE arrived_id_time_slot > id_time_slot ")->num_rows();
		if($check_backlog > 0 ){
			$data_backlog = array(
				"schedule_number" => $id,
				"update_by"		  => $this->session->userdata('sess_id'),
				"update_at"		  => date('Y-m-d H:i:s'),
				"rdd"			  => date('Y-m-d H:i:s')
			);
			$this->db->insert('tb_backlog', $data_backlog);
		}

		$getData = $this->db->query("SELECT b.vendor_code, a.receipt FROM tb_delivery_detail AS a INNER JOIN tb_schedule_detail AS b ON a.id_schedule_group = b.schedule_number WHERE id_schedule_group = '$id' ")->row();
		$receipt = $getData->receipt;
		$vendor_code = $getData->vendor_code;

		$get_item = $this->db->query("SELECT b.quantity, a.id, a.req_pallet ,a.po_number, a.po_line_item, a.material_code FROM tb_rds_detail AS a INNER JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE b.schedule_number = '$id' ")->result();
		foreach ($get_item as $get) {
			$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Arrive",
	  			"by_who"			=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_delivery_detail",
	  			"id_join"		=> $id,
	  			"value"			=> $receipt,
	  			"value2"		=> "AT ".date('d-m-Y',strtotime($arrive_date)).' '.date('H:i:s',strtotime($arrive_time)),
	  			"description"	=> "PO Number : ".$get->po_number.", Line : ".$get->po_line_item.", Material Code : ".$get->material_code,
	  			"select_join"	=> "arrive_date",
	  			"author"		=> 4,
	  			"vendor_code"	=> $vendor_code
		  	);
		  	$this->db->insert('tb_history', $history);

		  	$mm = array(
				"id_schedule" => $get->id,
				"quantity"	  => $get->quantity,
				"action"	  => "Arrived",
				"created_by"  => $this->session->userdata('sess_id')
			);
			add_material_movement($mm);
		}
		redirect('schedule'.$url);
	}

	public function received(){
		date_default_timezone_set("Asia/Bangkok");
		$id_return 				= $this->input->post('id_return');
		$value_return 			= $this->input->post('return_value');
		$id 					= $this->input->post('id');
		$id_schedule 			= $this->input->post('id_schedule');
		$url 					= $this->input->post('url');
		$reason 				= $this->input->post('reason');
		$po_number 				= $this->input->post('po_number');
		$po_line_item 			= $this->input->post('po_line_item');
		$material_code 			= $this->input->post('material_code');
		$received_amount 		= $this->input->post('received_amount');
		$received_date 			= $this->input->post('received_date');
		$received_hour 			= $this->input->post('received_hour');
		$received_minute 		= $this->input->post('received_minute');
		$time 					= $received_hour.":".$received_minute;
		$received_time 			= date('H:i',strtotime($time));

		$get_material = $this->db->query("SELECT a.id FROM tb_rds_detail AS a INNER JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE b.schedule_number = '$id' ")->result();
		$date_now = Date('Y-m-d H:i');
		$user_id = $this->session->userdata('sess_id');

		foreach ($get_material as $get) {
			$this->db->query("UPDATE tb_rds_detail SET update_at = '$date_now', update_by = '$user_id' WHERE id = '$get->id' ");
		}


		$config['allowed_types'] = 'jpg|jpeg|png|bmp';
		  	$config['max_size'] = 0;
			$config['upload_path'] = 'assets/upload/return';
			$receipt = $this->db->query("SELECT receipt FROM tb_delivery_detail WHERE id_schedule_group = '$id' ")->row()->receipt;

	  		$config['file_name'] = $receipt;
	  		$foto = '';
	  		$this->load->library('upload', $config);
		  	if ($this->upload->do_upload('reject_file')) {
		  		$data = $this->upload->data();
			  	$foto = $this->upload->data('file_name');

			}else{
			  	print_r($this->upload->display_errors());
			}
		$no=0;

		$getData = $this->db->query("SELECT b.vendor_code, a.receipt FROM tb_delivery_detail AS a INNER JOIN tb_schedule_detail AS b ON a.id_schedule_group	 = b.schedule_number WHERE a.id_schedule_group = '$id' ")->row();

		$receipt = $getData->receipt;
		$vendor_code = $getData->vendor_code;

		foreach ($id_return as $get) {

			$this->db->where('id',$get);
			$this->db->update('tb_scheduler', array( "receive_amount" => $received_amount[$no] ));

			$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Received",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_delivery_detail",
	  			"id_join"		=> $id,
	  			"value"			=> $receipt,
	  			"value2"		=> "AT ".date('d-m-Y',strtotime($received_date)).' '.date('H:i:s',strtotime($received_time)),
	  			"description"	=> "PO Number : ".$po_number[$no].", Line : ".$po_line_item[$no].", Material Code : ".$material_code[$no],
	  			"select_join"	=> "arrive_date",
	  			"author"		=> 4,
	  			"vendor_code"	=> $vendor_code
		  	);
		  	$this->db->insert('tb_history', $history);

		  	$mm = array(
				"id_schedule" => $id_schedule[$no],
				"quantity"	  => $received_amount[$no],
				"action"	  => "Received",
				"created_by"  => $this->session->userdata('sess_id')
			);
			add_material_movement($mm);

			if($value_return > 0){
				$mm = array(
					"id_schedule" => $id_schedule[$no],
					"quantity"	  => $value_return[$no],
					"action"	  => "Return_Material",
					"created_by"  => $this->session->userdata('sess_id')
				);
				add_material_movement($mm);
			}

			$no++;
		}

		$c_time = date('H:i:s');
    	$order = $this->db->query("SELECT id FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")
    	->row()->id;

		$data = array(
			"receive_date"	=> $received_date,
			"receive_time"	=> $received_time,
			"received_by"	=> $this->session->userdata('sess_id'),
			"reason"		=> $reason,
			"foto"			=> $foto,
			"received_id_time_slot" => $order
		);
		$this->db->where("id_schedule_group",$id);
		$this->db->update("tb_delivery_detail", $data);

		$this->db->query("UPDATE tb_schedule_detail SET status = '3' WHERE schedule_number = '$id' ");
		redirect('schedule'.$url);
	}

	public function adt_truck(){

		$date = $this->input->post('date');
		$reason = $this->input->post('reason');
		$time_slot = $this->input->post('time_slot');
		if($this->session->userdata('sess_role_no') == 3){
			$vendor = $this->session->userdata('sess_vendor_code');
		}else{
			$vendor = $this->input->post('vendor');
		}
		$week = $this->input->post('week');
		$last_id = $this->getScheduleNumber();
		$schedule_number = $last_id;
		$data = array(
			"vendor_code"		=> $vendor,
			"schedule_number"	=> $schedule_number,
			"rdd"				=> $date,
			"id_time_slot"		=> $time_slot,
			"status"			=> 0,
			"is_additional"		=> 1

		);

		$time = $this->db->query("SELECT start_time, end_time, shift FROM ms_time_slot WHERE id = '$time_slot' ")->row();
	  	$timez = $time->start_time.' - '.$time->end_time;
	  	$shift = $time->shift;
		$cek = $this->Schedule_model->checkSlotAva($time_slot, $date);
		$cek2 = $this->Schedule_model->checkSlotAva_not_plan($time_slot, $date);

		if( $cek == 2 ){
			if($cek2 > 0){

				$this->db->insert('tb_schedule_detail', $data);
				$get_last_id = $this->db->query("SELECT id FROM tb_schedule_detail ORDER BY id DESC")->row()->id;

				$get_another = $this->Schedule_model->get_another_schedule($time_slot, $date, $get_last_id, $shift);
				$change_schedule = $this->Schedule_model->checkSlotAva_change($time_slot, $date, $shift);

				$this->db->where('id', $get_another);
				$this->db->update('tb_schedule_detail', array("id_time_slot" => $change_schedule->id) );
			}else{

				$this->db->insert('tb_schedule_detail', $data);

			}

		}else{

			$this->db->insert('tb_schedule_detail', $data);
		}

		$adt = array(
			"schedule_number"	=> $schedule_number,
			"reason"			=> $reason
		);
		$this->db->insert('tb_additional_schedule',$adt);
		$last_id = $this->db->query("SELECT id FROM tb_schedule_detail ORDER BY id desc")->row()->id;
	  	$get_data = $this->db->query("SELECT a.vendor_code, a.rdd, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.slot_number WHERE a.id = '$last_id'")->row();

	  	$vc = $get_data->vendor_code;
	  	$rdd = $get_data->rdd;
	  	$time = $get_data->start_time.' '.$get_data->end_time;

  		$history = array(
  			"date" 			=> Date('Y-m-d'),
  			"time"			=> date('H:i:s'),
  			"action"		=> "Add Additional Truck",
  			"by_who"		=> $this->session->userdata('sess_id'),
  			"table_join"	=> "tb_schedule_detail",
  			"id_join"		=> $last_id,
  			"description"	=> 'Vendor Code :'.$vc." Rdd :".$rdd.' Time Slot:'.$time.' Reason :'.$reason,
  			"value"			=> "",
  			"value"			=> "",
  			"select_join"	=> "is_additional",
  			"author"		=> 3,
  			"vendor_code"	=> $vendor
  		);
  		$this->db->insert("tb_history",$history);

		redirect('schedule/view_schedule/'.$schedule_number.'/'.$date.'/'.$time_slot.'/'.$week.'/adt');
	}

	public function check_truck(){
		$min = $this->input->get('min_qty');
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$date = date('Y-m-d');
		$full_capacity = $this->db->query("SELECT full_capacity FROM skin_master.ms_supplier WHERE vendor_code = '$vendor_code' ")->row()->full_capacity;
		$check_pallet = $full_capacity - $min;

		if($this->session->userdata('sesss_no_role') == 1){
		$sum_pallet = $this->db->query("SELECT sum(a.quantity) as sum_qty FROM tb_scheduler as a LEFT JOIN tb_schedule_detail as b ON a.schedule_number = b.schedule_number  WHERE a.rdd='$date' AND b.status = 0 GROUP BY a.schedule_number HAVING sum_qty <= $check_pallet ")->row();
		}else{
			$sum_pallet = $this->db->query("SELECT sum(a.quantity) as sum_qty FROM tb_scheduler as a LEFT JOIN tb_schedule_detail as b ON a.schedule_number = b.schedule_number  WHERE b.rdd='$date' AND b.status = 0 AND b.vendor_code = '$vendor_code' GROUP BY a.schedule_number HAVING sum_qty <= $check_pallet ")->row();
		}
		$check = 0;
		if(isset($sum_pallet )){
				$check = $sum_pallet->sum_qty;
		}
		// $data = $this->db->query("SELECT sum(b.quantity) AS sum_qty FROM tb_schedule_detail as a INNER JOIN tb_scheduler as b ON a.schedule_number = b.schedule_number WHERE a.status = '0' AND a.rdd ='$date' AND a.vendor_code = '$vendor_code' GROUP BY a.schedule_number HAVING sum(b.quantity) < $check_pallet ")->row();

		echo json_encode($check);
	}

	public function get_schedule_list(){
		$sn = $this->input->get('sn');
		$batch_number = $this->input->get('batch_number');
		$vendor_code = $this->session->userdata("sess_vendor_code");
		$req_data = $this->Schedule_model->getScheduleDetail($sn);
		$c_time = date('H:i:s');
		$getData = $this->db->query("SELECT a.vendor_code, a.rdd, a.id_time_slot, b.shift FROM tb_schedule_detail AS a INNER JOIN ms_time_slot AS b on a.id_time_slot = b.id WHERE schedule_number ='$sn' ")->row();
		$getShift = $this->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		
		$shift = $getShift->shift;
		
		$data['batch_number'] = $batch_number;
		$data['shift'] = $shift;
		$data['vendor_code'] = $vendor_code;
		$data['sn'] = $sn;
		$data['data_list'] = $req_data['data_list'];
		$data['array_id'] = $req_data['array_id'];
		$data['category'] = $this->getCategory_sch($sn);
		$this->load->view('schedule/v_schedule_new', $data);

	}

	public function get_pool_schedule_list(){
		date_default_timezone_set('Asia/Jakarta');
		$shift = getShift();
		$c_minus = $this->input->get('cminus');
		$arr_vendor = array();
		$arr_id = array();
		$today  = date('Y-m-d');
		$this->load->model('outstanding/OS_Model');
		$role = $this->session->userdata('sess_role_no');
		$rdd = $this->input->get('rdd');
		$sn = $this->input->get('sn');
		$sess_no = $this->session->userdata('sess_no');
		$get_vendor_code =$this->db->query("SELECT vendor_code FROM tb_schedule_detail WHERE schedule_number = '$sn' ")->row()->vendor_code;

		if($this->session->userdata('sess_role_no') == 3){
			$vc = $this->session->userdata('sess_vendor_code');
		}else{
			$vc = $get_vendor_code;
		}

		$dataPool = $this->OS_Model->getMaterialVendor(0, 0, $sess_no, $vc, $sn, $rdd, $c_minus);
		foreach($dataPool->result() as $i){
			array_push($arr_id, $i->id);
		}
	
		if(count($arr_id) > 0){
			$imp = implode(",", $arr_id);
		}else{
			$imp = "";
		}

		$dateLess = $this->OS_Model->getDateLessVen(0,0, $imp, $sess_no, $vc, $sn, $c_minus);

		foreach($dateLess->result() as $i){
			array_push($arr_id, $i->id);
		}

		if(count($arr_id) > 0){
			$imp = implode(",", $arr_id);
		}else{
			$imp = "";
		}
		$shiftLate = $this->OS_Model->getShiftLateVen($today, $shift, $imp, 0, 0, $sess_no, $vc, $sn, $c_minus);
		foreach($shiftLate->result() as $i){
			array_push($arr_id, $i->id);
		}

		if(count($arr_id) > 0){
			$imp = implode(",", $arr_id);
		}else{
			$imp = "";
		}
		

		$pl = array_merge($dataPool->result(), $dateLess->result(), $shiftLate->result());
		//echo count($pl);
		/*echo "<pre>".json_encode($getDataAll, JSON_PRETTY_PRINT)."</pre>"; die();*/
		$data['data_list'] = $pl;
		$data['sn'] = $this->input->get('sn');
		$data['category'] = $this->getCategory_sch($sn);

		$this->load->view('schedule/data_material_pool', $data);
	}

	public function save_schedule(){
		$truck_no 		= $this->input->post('truck_no');
		$driver_name 	= $this->input->post('driver_name');
		$phone_no 		= $this->input->post('phone_number');
		$id_schedule 	= $this->input->post('id_schedule');
		$rdd 			= date('Y-m-d H:i');
		$time_slot 		= $this->input->post('id_time_slot');
		$coa_count 		= $this->input->post('coa_count');
		$coa_detail		= $this->input->post('coa_detail');
		$schedule_number = $this->input->post('slot_number');
		$dn_count		= $this->input->post('dn_count');
		$batch_number  = $this->input->post('batch_number');
		$checker = '';
		$checker2 = '';

	  	$rdg 	= date_create($rdd);
	  	$pyear 	= date_format($rdg, 'Y');
	  	$pweek 	= date_format($rdg, 'W');
	  	$pdate 	= date_format($rdg, 'Y-m-d');

		$shift 		= $this->db->query("SELECT shift FROM ms_time_slot where slot_number = '$time_slot' ")->row()->shift;
		$vendor_code = $this->db->query("SELECT vendor_code FROM tb_schedule_detail WHERE schedule_number ='$schedule_number' ")->row()->vendor_code;
	  	$vendor_name = $this->db->query("SELECT vendor_alias from skin_master.ms_supplier where vendor_code = '$vendor_code' ")->row()->vendor_alias;

	  	date_default_timezone_set("Asia/Bangkok");
	  	$date_created =  Date('Y/m/d H:i:s', strtotime('+1 hour +30 minutes'));
		$date = date('dmY');
		//invoice
		for($i=1;$i<=$dn_count;$i++){
			$do_number = $this->input->post('do_number'.$i);
			$inv_file = $this->input->post('file'.$i, TRUE);
			$config['upload_path'] = './assets/upload/invoice';
	  		$config['file_name'] = "DN_".$date."_".$shift."_".$vendor_name."_".$do_number;
	  		$config['allowed_types'] = 'docx|doc|pdf';
	  		$config['max_size'] = 0;

	  		$this->load->library('upload', $config);
		  	$this->upload->initialize($config);
		  	if ($this->upload->do_upload('file'.$i)) {
		  		$media = $this->upload->data();
		  		$dn_name = './assets/upload/invoice/'.$media['file_name'];
		  		$checker = 'True';
		  		$this->db->query("INSERT INTO tb_delivery_invoice(id,id_schedule, do_number, invoice) VALUES('','$id_schedule','$do_number','$dn_name')");
		  	}else{

				echo $this->upload->display_errors();
		   		$this->session->set_flashdata('ERR', $this->upload->display_errors()." allowed file type is .DOCX or .PDF");
		   		$this->db->query("DELETE FROM tb_delivery_invoice WHERE id_schedule = '$id_schedule' ");
		   		$ddd = date("Y-m-d", strtotime($rdd));
		   		redirect('schedule/view_schedule/'.$id_schedule.'/'.$ddd.'/'.$time_slot.'/'.$pweek);
		  	}
		}
	  	// End invoice

	  	// CoA
	  	for($i=1;$i<=$coa_count;$i++){
			$coa_number = $this->input->post('coa_number'.$i);
			$inv_file  = $this->input->post('coa'.$i, TRUE);
			$coas['upload_path'] = './assets/upload/coa';
	  		$coas['file_name'] = "CoA_".$date."_".$shift."_".$vendor_name."_".$do_number;
	  		$coas['allowed_types'] = 'docx|doc|pdf';
	  		$coas['max_size'] = 0;
	  		$this->load->library('upload', $coas);
		  	$this->upload->initialize($coas);
		  	if ($this->upload->do_upload('coa'.$i)) {

		  		$coa_media = $this->upload->data();
		  		$coa_name = './assets/upload/coa/'.$coa_media['file_name'];
		  		$checker2 = 'True';
		  		$this->db->query("INSERT INTO tb_delivery_coa(id,id_schedule, coa) VALUES('','$id_schedule','$coa_name')");
		  	}else{

				echo $this->upload->display_errors();
		   		$this->session->set_flashdata('ERR2', $this->upload->display_errors()." allowed file type is .DOCX or .PDF");
		   		$this->db->query("DELETE FROM tb_delivery_invoice WHERE id_schedule = '$id_schedule' ");
		   		$this->db->query("DELETE FROM tb_delivery_coa WHERE id_schedule = '$id_schedule' ");
		   		$ddd = date("Y-m-d", strtotime($rdd));
		   		redirect('schedule/view_schedule/'.$id_schedule.'/'.$ddd.'/'.$time_slot.'/'.$pweek);
		  	}
		}

	  	//End Coa
		$datez = date('Y-m-d');

		//diganti ke format 9003-ddmmyy-nomor receipt (limit puluhan)-nomor vendor

	  	$check_receipt = $this->db->query("SELECT receipt, delivery_date FROM tb_delivery_detail WHERE DATE(delivery_date) = '$datez' ORDER by id desc ")->row();


	  		if($check_receipt != null || $check_receipt != ''){
	  			$get_receipt = explode('-',$check_receipt->receipt);
	  			$receipt_ai =  $get_receipt[2];

	  			if($receipt_ai <= 9){
	  				$receipt_add = "0" . ($receipt_ai + 1);
	  				$receipt = "9003-".date('dmy').'-'.$receipt_add;
	  			}else{
	  				$receipt_add = $receipt_ai + 1;
	  				$receipt = "9003-".date('dmy').'-'.$receipt_add;
	  			}
	  			$array = array(
	  				'receipt'	  => $receipt
	  			);
	  		}
	  		$receipt_check = "9003-".date('dmy').'-'.'01';
	  		$check_first_receipt = $this->db->query("SELECT receipt FROM tb_delivery_detail WHERE receipt = '$receipt_check' ")->num_rows();

	  		if($check_first_receipt == 0){
	  			$receipt = "9003-".date('dmy').'-'.'01';
	  		}


	  	if($checker == 'True' && $checker2 == 'True'){
	  		$invoice_name = $config['file_name'];
	  		$coa = $coas['file_name'];
	  		$array = array(
	  			'id_schedule_group' => $id_schedule,
	  			'delivery_date' 	=> $rdd,
	  			'id_time_slot'		=> $time_slot,
	  			'id_truck'			=> $truck_no,
	  			'id_driver'			=> $driver_name,
	  			'phone_number'		=> $phone_no,
	  			'created_at'		=> $date_created,
	  			'receipt'			=> $receipt
	  		);

	   		$this->db->trans_start();
	   		$check_double_receipt = $this->db->query("SELECT receipt FROM tb_delivery_detail WHERE id_schedule_group = '$id_schedule' LIMIT 1")->num_rows();
	   		if($check_double_receipt == 0){
	   			$this->db->insert('tb_delivery_detail', $array);	
	   		}

	   		$this->db->trans_complete();
			$this->db->where('schedule_number', $schedule_number);
	   		$this->db->update('tb_schedule_detail', array(
	   			'status' => 1,
	   			'set_id_time_slot'	=> $time_slot,
	   			"set_rdd"			=> $rdd
	   		));

	   		$dcek = date_format(date_create($rdd), "Y-m-d");
	   		$check_batch_null = $this->db->query("SELECT
									CAST(SUBSTR(batch_number, 1, 8) AS int) as UNIQUE_CREATOR, 
									batch_number,
									substr(batch_number,7,2) as LAST_TWO,
									substr(batch_number,7,2)+1 as AFTER_INC
								FROM
									tb_scheduler
								WHERE
									rdd = '$dcek'
								ORDER BY
									AFTER_INC DESC
								LIMIT 0,1")->row();
	   		
	   		$shift_batch = "";
	   		if($shift == "DM"){
	   			$shift_batch = "M";
	   		}else if($shift == "DP"){
	   			$shift_batch = "P";
	   		}else{
	   			$shift_batch = "S";
	   		}

	   		if(null === $check_batch_null->LAST_TWO){
				$batch_number = date('ymd', strtotime($rdd) ).'01'.$shift_batch;
			}else{
				$batch = $check_batch_null->AFTER_INC;
				if($batch < 10){
					$batch_inc = "0".$batch;
				}else{
					$batch_inc = $batch;
				}
				
				$batch_number = date('ymd', strtotime($rdd) ).$batch_inc.$shift_batch;
			}

	  		$this->db->query("UPDATE tb_schedule_detail SET status='1' WHERE schedule_number='$schedule_number'");
	  		$this->db->query("UPDATE tb_scheduler SET status='1', batch_number = '$batch_number' WHERE schedule_number = '$schedule_number'");
	  		$this->db->query("UPDATE tb_rds_detail SET status='1' WHERE id IN (SELECT id_schedule FROM tb_scheduler WHERE schedule_number='$schedule_number') ");

	  		$this->load->library('infiQr');
			QRcode::png($receipt, 'assets/dist/img/'.$receipt.".png", "H", 10, 1);

	  		$get_schedule =  $this->db->query("SELECT a.id, b.start_time, b.end_time FROM tb_delivery_detail AS a INNER JOIN ms_time_slot AS b ON a.id_time_slot = b.id ORDER BY id DESC")->row();
	  		$last_id = $get_schedule->id;
	  		$time = $get_schedule->start_time.' - '.$get_schedule->end_time;

	  		$get_item = $this->db->query("SELECT a.id, b.quantity, a.po_number, a.po_line_item FROM tb_rds_detail AS a LEFT JOIN tb_scheduler AS b ON a.id = b.id_schedule WHERE b.schedule_number = '$schedule_number' ")->result();

	  		foreach ($get_item as $get) {
	  			$history = array(
		  			"date" 			=> Date('Y-m-d'),
		  			"time"			=> date('H:i:s'),
		  			"action"		=> "Set Plan",
		  			"by_who"		=> $this->session->userdata('sess_id'),
		  			"table_join"	=> "tb_delivery_detail",
		  			"id_join"		=> $last_id,
		  			"description"	=> "No Receipt :".$receipt.", PO:".$get->po_number.", Line : ".$get->po_line_item.", Week : ".$pweek.", Shift : ".$shift.", Time slot :".$time,
		  			"select_join"	=> "receipt",
		  			"author"		=> 3,
		  			"vendor_code"	=> $vendor_code
		  		);

		  		$this->db->insert('tb_history', $history);

		  		$mm = array(
						"id_schedule" => $get->id,
						"quantity"	  => $get->quantity,
						"action"	  => "Set Plan",
						"created_by"  => $this->session->userdata('sess_id')
					);
					//add_material_movement($mm);
	  		}

	  		redirect('schedule?year='.$pyear.'&week='.$pweek.'&date='.$pdate);
	  	}else{
	  		$this->session->set_flashdata('ERR', $this->upload->display_errors());
	  		redirect('schedule/view_schedule/'.$id_schedule.'/'.$rdd.'/'.$time_slot.'/'.$pweek);
	  	}
	}


	public function setStatus($schedule_number, $status){
		$array = array();
		$this->db->where('schedule_number', $schedule_number);
		$this->db->update('tb_scheduler', array(
			'status' => $status
		));

		$data = $this->db->query("SELECT id_schedule FROM tb_scheduler WHERE schedule_number='$schedule_number'");
		foreach($data->result() as $get){
			array_push($array, $get->id_schedule);
		}
		$js = json_encode($array);
		$js = substr($js, 1);
		$js = substr($js, 0, -1);
		$this->db->query("UPDATE tb_rds_detail SET status='1' WHERE id IN ($js)");
	}

	public function test(){
		$array = array();
		$data = $this->db->query("SELECT id_schedule FROM tb_scheduler WHERE schedule_number='2'");
		foreach($data->result() as $get){
			array_push($array, $get->id_schedule);
		}
		$js = json_encode($array);
		$js = substr($js, 1);
		$js = substr($js, 0, -1);
		echo $js;
	}

	public function get_detailed_information(){
		$key = $this->input->get('key');
		$status = $this->input->get('status');
		$count_do = $this->db->query("SELECT do_number FROM tb_delivery_invoice WHERE id_schedule = '$key' ")->num_rows();
		$sch_detail = $this->Schedule_model->getScheduleDetail($key);
		$del_detail = $this->Schedule_model->getDeliveryInfo($key);
		$dn_download = $this->Schedule_model->getdnDownload($key);
		$coa_download = $this->db->query("SELECT coa FROM tb_delivery_coa WHERE id_schedule = '$key' ");
		/*echo json_encode($del_detail->result()); die();*/

		$data['sch_detail'] = $sch_detail['data_list'];
		$data['del'] = $del_detail->row();
		$data['dn_download'] = $dn_download;
		$data['coa_download'] = $coa_download;

		if($status != 3){
			$this->load->view('schedule/modal_info', $data);
		}else{
			$this->load->view('schedule/modal_info_received', $data);
		}

	}

	public function get_DI(){
		$key = $this->input->get('key');
		$sch_detail = $this->Schedule_model->getScheduleDetail($key);
		$data['sch_detail'] = $sch_detail['data_list'];
		$this->load->view('schedule/modal_info_n', $data);
	}

	public function return_form(){
		$key = $this->input->get('key');
		$sch_detail = $this->Schedule_model->getScheduleDetail($key);
		$data['sch_detail'] = $sch_detail['data_list'];
		$this->load->view('schedule/return_form', $data);
	}

	public function get_suggestion(){
		$p_min = $this->input->get("pallet_minus");
		$sn = $this->input->get('sn');
		$rdd = $this->input->get('rdd');
		$vendor_codez = $this->db->query("SELECT vendor_code FROM tb_schedule_detail WHERE schedule_number = '$sn' ")->row()->vendor_code;
		$data_suggestion = $this->Schedule_model->getSuggestion($p_min, $sn, $rdd,$vendor_codez);
		$data['data_list'] = $data_suggestion;
		$data['p_min'] = $p_min;
		$data['sn'] = $sn;
		$data['category'] = $this->getCategory_sch($sn);

		$this->load->view('schedule/data_suggestion', $data);
	}

	public function add_material(){
		$id = $this->input->get('id');
		$sn = $this->input->get('sn');
		$this->db->where('id_schedule', $id);
		$this->db->update('tb_scheduler', array(
			"schedule_number" => $sn
		));

		$data = $this->db->query("SELECT a.id, a.req_pallet, a.vendor_code,a.po_number, a.material_code, a.requested_delivery_date , a.shift, b.quantity, a.po_line_item FROM tB_rds_detail as a INNER JOIN tb_scheduler as b ON a.id = b.id_schedule WHERE a.id = '$id' ")->row();
		$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Take",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_scheduler",
	  			"id_join"		=> $id,
	  			"description"	=> "From Same RDD :".$data->requested_delivery_date.", Shift :".$data->shift.", PO :".$data->po_number.", Line :".$data->po_line_item." Material Code :".$data->material_code.", Req Pallet :".$data->quantity,
	  			// "value"			=> "PO : ".$data->po_number. "Material Code : ".$data->material_code,
	  			// "value2"		=> "RDD : ".$data->rdd." shift : ". $data->shift,
	  			"select_join"	=> "id",
	  			"author"		=> 3,
	  			"vendor_code"	=> $data->vendor_code
	  	);

	  	$this->db->insert('tb_history', $history);

	  	$mm = array(
					"id_schedule" => $data->id,
					"quantity"	  => $data->req_pallet,
					"action"	  => "Take_From_Same_RDD",
					"created_by"  => $this->session->userdata('sess_id')
				);
				add_material_movement($mm);

		return true;
	}

	public function edit_schedule(){
		$id = $this->uri->segment(3);
		date_default_timezone_set("Asia/Bangkok");

		$truck_no 		= $this->input->post('truck_no');
		$driver_name 	= $this->input->post('driver_name');
		$phone_no 		= $this->input->post('phone_number');
		$do_number 		= $this->input->post('do_number');
		$coa_detail		= $this->input->post('coa_detail');
		$inv_file = $this->input->post('coa', TRUE);
		$dn_count		= $this->input->post('dn_count');
		$coa_count		= $this->input->post('coa_count');
		$time_slot 		= $this->input->post('id_time_slot');
		$vendor_code = $this->session->userdata('sess_vendor_code');
	  	$vendor_name = $this->db->query("SELECT vendor_name from skin_master.ms_supplier where vendor_code = '$vendor_code' ")->row()->vendor_name;
	  	$shift 		= $this->db->query("SELECT shift FROM ms_time_slot where slot_number = '$time_slot' ")->row()->shift;
	  	$date_created =  Date('Y/m/d H:m:s', strtotime("+90 minutes"));
		$date = date('dmY');
		$id_schedule = $this->db->query("SELECT id_schedule_group FROM tb_delivery_detail WHERE id = '$id' ")->row()->id_schedule_group;
		// $this->db->query("DELETE FROM tb_delivery_invoice WHERE id_schedule = '$id_schedule' ");
		// $this->db->query("DELETE FROM tb_delivery_coa WHERE id_schedule = '$id_schedule' ");
		// for($i=1;$i<=$dn_count;$i++){
		// 	$do_number = $this->input->post('do_number'.$i);
		// 	$inv_file = $this->input->post('file', TRUE);
		// 	$config['upload_path'] = './assets/upload/invoice';
	 //  		$config['file_name'] = "DN_".$date."_".$shift."_".$vendor_name."_".$do_number;
	 //  		$config['allowed_types'] = 'docx|doc|pdf';
	 //  		$config['max_size'] = 0;
	 //  		$this->load->library('upload', $config);
		//   	$this->upload->initialize($config);

		//   	if (!$this->upload->do_upload('file'.$i)) {
		//   		echo $this->upload->display_errors(); die();
		//    		$this->session->set_flashdata('ERR', $this->upload->display_errors());
		//   	}else{

		//   		$invoice_name = $config['file_name'];
		//   		$media = $this->upload->data();
		//   		$dn_name = './assets/upload/invoice/'.$media['file_name'];
		//   		$checker = 'True';
		//   		$this->db->query("INSERT INTO tb_delivery_invoice(id,id_schedule, do_number, invoice) VALUES('','$id_schedule','$do_number','$dn_name')");
		//   	}
		// }
	 //  	//End invoice

	 //  	//CoA
	 //  	for($i=1;$i<=$coa_count;$i++){
		// 	$coa_number = $this->input->post('coa_number'.$i);
		// 	$inv_file  = $this->input->post('coa', TRUE);
		// 	$coas['upload_path'] = './assets/upload/coa';
	 //  		$coas['file_name'] = "CoA_".$date."_".$shift."_".$vendor_name."_".$do_number;
	 //  		$coas['allowed_types'] = 'docx|doc|pdf';
	 //  		$coas['max_size'] = 0;
	 //  		$this->load->library('upload', $coas);
		//   	$this->upload->initialize($coas);
		//   	if (!$this->upload->do_upload('coa'.$i)) {
		//   		echo $this->upload->display_errors(); die();
		//    		$this->session->set_flashdata('ERR', $this->upload->display_errors());
		//   	}else{
		//   		echo "wakwaw";
		//   		$invoice_name = $config['file_name'];
		//   		$med = $this->upload->data();
		//   		$coa_name = './assets/upload/coa/'.$med['file_name'];
		//   		$checker2 = 'True';
		//   		$this->db->query("INSERT INTO tb_delivery_coa(id,id_schedule, coa) VALUES('','$id_schedule','$coa_name')");
		//   	}
		// }

		$array = array();
	  	$array = array(
		  			'id_truck'			=> $truck_no,
		  			'driver_name'		=> $driver_name,
		  			'phone_number'		=> $phone_no,
		  			'last_update'		=> $date_created
		  		);

	  	$this->db->where('id',$id);
	  	$this->db->update('tb_delivery_detail', $array);

	  	$dds = $this->db->query("SELECT delivery_date FROM tb_delivery_detail WHERE id=$id")->row();
	  	$rdg = date('Y-m-d',strtotime($dds->delivery_date));
  		$pyear = date('Y',strtotime($rdg));
  		$pweek = date('W',strtotime($rdg));

  		redirect('schedule?year='.$pyear.'&week='.$pweek.'&date='.$rdg);
	}

	public function add_material_pool(){
		$id = $this->input->get('id');
		$sn = $this->input->get('sn');
		$slot = $this->input->get('slot');
		$getData = $this->db->query("SELECT * FROM tb_schedule_pool where id_schedule = '$id' ")->row();
		$getrdd = strtotime($getData->rdd);
		$rdd = date('Y-m-d',$getrdd);

		$array = array(
				'id_schedule' => $id,
	  			'rdd' => $rdd,
	  			'vendor_code' => $getData->vendor_code,
	  			'schedule_number' => $getData->schedule_number

	  		);
		$this->db->insert('tb_scheduler', $array);
		// $this->db->query("INSERT INTO tb_schedule_detail(vendor_code, schedule_number, rdd, id_time_slot) VALUES ($getData->vendor_code,$getData->schedule_number,$getData->rdd, $slot) ");

		$this->db->where('id_schedule', $id);
		$this->db->delete('tb_schedule_pool');

		return true;
	}



	public function testArrayDot(){
		echo "-----------------------------------------<br>";
		$arr = array();
		$tt = (null === $this->input->get('date_start'))?'26/10/2018':$this->input->get('date_start');
		$tt = str_replace("/", "-", $tt);
		$today = date_create($tt);
		$today = date_format($today, "Y-m-d");
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$getLast = 1;
		$dataToday = $this->Schedule_model->getScheduleX($today, $vendor_code);
		if($dataToday->num_rows() > 0){
			$d 			= json_encode($dataToday->result());
			$arr 		= json_decode($d, TRUE);
			$pool 		= array();
			$a 			= 0;
			while($a < count($arr)){
				$counter 	= 0;
				while($counter <= 3120){
					$b 			= 0;
					$reqp = $arr[$a]['req_pallet'];
					$counter += $reqp;
					if($counter > 32){
						array_push($pool, $a);
						$counter 	-= $reqp;
						$kurang 	= 32-$counter;
						$f 			= array_search($kurang, array_column($arr, "req_pallet"));
						if($f != null){
							if($f > $a){
								$reqf = $arr[$f]['req_pallet'];
								$counter += $reqf;
								$b = $f;
							}
						}
					}else{
						$b = $a;
					}
				}
				echo $b." ".$counter;
				$a++;
			}
			echo "<br>";
		}
		echo "-----------------------------------------";
	}

	public function getDateList($week, $year) {
	  $dto = new DateTime();
	  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
	  $s = 1;
	  while($s <= 6){
	  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
	  	$s++;
	  }
	  return $ret;
	}

	public function change_schedule(){
		$id = $this->uri->segment(3);
		$rdd = $this->uri->segment(4);
		$a_ts = $this->uri->segment(5);
		if($this->session->userdata('sess_role_no') == 1){
			$shift = $this->uri->segment(7);
		}else{
			$shift = $this->uri->segment(6);
		}

		$b_ts = $this->input->post('ts');
		$yearz = $this->input->post('yearz');
		$weekz = $this->input->post('weekz');
		$date = $this->input->post('date');
		$cek = $this->Schedule_model->checkSlotAva($b_ts, $rdd);
		$cek2 = $this->Schedule_model->checkSlotAva_not_plan($b_ts, $rdd);
		if( $cek < 4 ){
			if( ($cek+$cek2) == 4 ){

				$get_another = $this->Schedule_model->get_another_schedule($b_ts, $rdd, $id, $shift);
		 		$this->db->where('id', $get_another);
		 		$this->db->update('tb_schedule_detail', array("id_time_slot" => $a_ts));

		 		$this->db->where('id', $id);
				$this->db->update('tb_schedule_detail', array(
					'id_time_slot' => $b_ts
				));

				$get_value = $this->db->query("SELECT start_time, end_time FROM ms_time_slot WHERE id = '$a_ts' ")->row();
				$value = $get_value->start_time." - ".$get_value->end_time;
				$get_value2 = $this->db->query("SELECT start_time, end_time FROM ms_time_slot WHERE id = '$b_ts' ")->row();
				$value2= $get_value2->start_time." - ".$get_value2->end_time;

				$history = array(
					"date"			=> Date('Y-m-d'),
					'time'			=> Date('H:i:s'),
					'Action' 		=> "Update",
					'by_who'			=> $this->session->userdata('sess_id'),
					'table_join' 	=> "ms_time_slot",
					'value'			=> $value,
					'value2'		=> ' To '.$value2,
					'description'	=> "Time Slot From",
					"author"		=> 3,
					"vendor_code"	=> $this->session->userdata('sess_vendor_code')
				);

				$this->db->insert('tb_history', $history);
			}else if($cek == 4){

			}else{

				$this->db->where('id', $id);
				$this->db->update('tb_schedule_detail', array(
					'id_time_slot' => $b_ts
				));

				$get_value = $this->db->query("SELECT start_time, end_time FROM ms_time_slot WHERE id = '$a_ts' ")->row();
				$value = $get_value->start_time." - ".$get_value->end_time;
				$get_value2 = $this->db->query("SELECT start_time, end_time FROM ms_time_slot WHERE id = '$b_ts' ")->row();
				$value2= $get_value2->start_time." - ".$get_value2->end_time;

				$history = array(
					"date"			=> Date('Y-m-d'),
					'time'			=> Date('H:i:s'),
					'Action' 		=> "Update",
					'by_who'			=> $this->session->userdata('sess_id'),
					'table_join' 	=> "ms_time_slot",
					'value'			=> $value,
					'value2'		=> ' To '.$value2,
					'description'	=> "Time Slot From",
					"author"		=> 3,
					"vendor_code"	=> $this->session->userdata('sess_vendor_code')
				);

				$this->db->insert('tb_history', $history);

			}

		}else{

		}

		 $arr = array("STATUS" => "SUCCESS");
		// $this->db->update('ms_user', array("is_schedule_changed" => 1) );
		$this->session->set_flashdata($arr);

		$user_id = $this->session->userdata('sess_id');

		$this->db->query("UPDATE tb_data_checker SET schedule_checker = 1 WHERE id != '$user_id' ");
		redirect('schedule?year='.$yearz.'&week='.$weekz.'&date='.$date);
	}

	public function Planned_PO(){
		$week = $this->input->get('week');
		$year = $this->input->get('year');
		$date_start = $this->input->get('date_start');
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$this->Schedule_model->Download_Excel($vendor_code, $week, $year, $date_start);
		//$this->Schedule_model->material_movement($vendor_code);

		// redirect('schedule');
	}

	public function material_movement(){
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$this->Schedule_model->material_movement($year, $week);
	}

	public function material_tracker(){
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory', 'PHPExcel/Worksheet/PHPExcel_Worksheet_MemoryDrawing'));
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$this->Schedule_model->material_tracker($year, $week);
	}

	public function receipt(){
		$this->Schedule_model->receipt_pdf();

	}

	public function receipt2(){
		$this->Schedule_model->receipt_tcpdf();
		
	}

	public function cut_pallet(){
		$id = $this->input->get('id');
		$pallet = $this->input->get('pallet');
		$quantity = $this->input->get('quantity');
		$idm = $this->input->get('id_material');
		$req_pallet = $this->db->query("SELECT req_pallet FROM tb_rds_detail WHERE id=$idm")->row();
		if($req_pallet->req_pallet > $pallet && $pallet != 0){
		$this->db->query("UPDATE tb_scheduler SET quantity = $pallet WHERE id=$id");

		$data = $this->db->query("SELECT a.vendor_code ,a.po_number, a.material_code, a.requested_delivery_date, a.shift FROM tb_rds_detail as a INNER JOIN tb_scheduler as b ON a.id = b.id_schedule WHERE a.id = '$idm' ")->row();

		$mm = array(
					"id_schedule" => $idm,
					"quantity"	  => $quantity-$pallet,
					"action"	  => "Cut_Material",
					"created_by"  => $this->session->userdata('sess_id')
				);
		add_material_movement($mm);

		$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Cut Material",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_rds_detail",
	  			"id_join"		=> $idm,
	  			"description"	=> "FROM RDD :".$data->requested_delivery_date.", Shift :".$data->shift.", PO :".$data->po_number.", Material Code :".$data->material_code.", Req Pallet :",
	  			"value"			=> $quantity,
	  			"value2"		=> $pallet,
	  			"select_join"	=> "receipt",
	  			"author"		=> 3,
	  			"vendor_code"	=> $data->vendor_code
	  		);
	  	$this->db->insert('tb_history', $history);

		return true;
		}
	}

	public function add_material_to_list(){
		$sn = $this->input->get('sno');
		$id = $this->input->get('id');
		$qty = $this->input->get('qty');

		$data = $this->db->query("SELECT a.id, a.req_pallet, a.vendor_code, a.po_number, a.material_code, a.requested_delivery_date, a.shift, a.po_line_item FROM tb_rds_detail as a LEFT JOIN tb_scheduler as b ON a.id = b.id_schedule WHERE a.id = '$id' ")->row();

		$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Take Material",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_material_pool",
	  			"id_join"		=> $id,
	  			"description"	=> "FROM RDD :".$data->requested_delivery_date.", Shift :".$data->shift.", PO :".$data->po_number.", Line : ".$data->po_line_item." Material Code :".$data->material_code.", Req Pallet :".$qty,
	  			"select_join"	=> "receipt",
	  			"author"		=> 3,
	  			"vendor_code"	=> $data->vendor_code
	  		);

	  	$this->db->insert('tb_history', $history);

	  	$mm = array(
					"id_schedule" => $data->id,
					"quantity"	  => $qty,
					"action"	  => "Take_Material",
					"created_by"  => $this->session->userdata('sess_id')
				);
				add_material_movement($mm);

		$cekData = $this->db->query("SELECT id FROM tb_scheduler WHERE id_schedule=$id AND schedule_number='$sn'")->num_rows();
		if($cekData == 1){
			$this->db->query("UPDATE tb_scheduler SET quantity = quantity+$qty WHERE id_schedule='$id' AND schedule_number='$sn'");
		}else{
			$cekDataB = $this->db->query("SELECT id FROM tb_scheduler WHERE id_schedule=$id AND status='0'")->num_rows();
			if($cekDataB == 1){
				$this->db->where('id_schedule', $id);
				$this->db->update('tb_scheduler', array(
					"schedule_number" => $sn
				));
			}else{
				$getData = $this->db->query("SELECT vendor_code, requested_delivery_date FROM tb_rds_detail WHERE id=$id")->row();
				$vendor_code = $getData->vendor_code;
				$rdd = $getData->requested_delivery_date;
				$arr = array(
					'id_schedule' => $id,
					'vendor_code' => $vendor_code,
					'schedule_number' => $sn,
					'rdd' => $rdd,
					'quantity' => $qty
				);
				$this->db->insert('tb_scheduler', $arr);

			}
		}
	}

	//Start schedule 2
	public function schedule2(){
		$role = $this->session->userdata('sess_role_no');
		if($role == 1 || $role == 2){

		}else{
			isRole();
		}

		$get_day = date('D');
		$time_freeze = $this->db->query("SELECT day, start_time, end_time FROM tb_time_freeze WHERE day ='$get_day' ");
		$table_select = $this->input->get('table');
		$category = $this->input->get('category');
		$year = (null === $this->input->get('year'))?date('Y'):$this->input->get('year');
		$week = (null === $this->input->get('week'))?date('Y'):$this->input->get('week');
		$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  a.created_at,
									  a.update_at,
									  b.vendor_alias ,
									  c.material_name,
									  a.bill_of_lading,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tb_rds_detail as a
									  LEFT JOIN skin_master.ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN skin_master.ms_material as c ON a.material_code = c.material_sku
									  WHERE a.category LIKE '%$category%' AND a.week = '$week' AND YEAR(requested_delivery_date) = '$year'
									  ORDER BY a.id ASC
									  ");

		$get_item_code = $this->db->query("SELECT material_code FROM tB_rds_detail where category = '$category' ");
		$get_supplier = $this->db->query("SELECT vendor_code, vendor_name, vendor_alias FROM skin_master.ms_supplier");
		$get_category = $this->db->query("SELECT * FROM ms_category");
		$data['category'] = $get_category;
		$data['data'] = $get_data;
		$data['supplier'] = $get_supplier;
		$data['item_code'] = $get_item_code;
		$data['time_freeze'] = $time_freeze;

		//dump($get_data->result());
		if(isset($table_select)){
			getHTML('schedule/schedules2',$data);
		}else{
			getHTML('schedule/schedule2',$data);
		}

	}

	public function schedule2_search(){
		$role = $this->session->userdata('sess_role_no');
		if($role == 1 || $role == 2){

		}else{
			isRole();
		}
		$get_day = date('D');
		$filter_text = $this->input->get('filter_text');
		$time_freeze = $this->db->query("SELECT day, start_time, end_time FROM tb_time_freeze WHERE day ='$get_day' ");
		$table_select = $this->input->get('table');
		$category = $this->input->get('category');
		$menu = $this->input->get('menu');
		$table = $this->input->get('table');
		if($menu != 'supplier'){
			$menu = $this->input->get('menu');
		}else{
			$menu ='';
		}

		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$item_list = $this->input->get('item_list');
		$get_supplier = $this->input->get('supplier');
		$get_data = '';

		if($get_supplier != '' AND $menu == 'material_name'){
			$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  b.vendor_alias ,
									  c.material_name,
									  d.nama,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tB_rds_detail as a
									  LEFT JOIN skin_master.ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN skin_master.ms_material as c ON a.material_code = c.material_sku
									  LEFT JOIN ms_user as d ON a.created_by = d.id
									  WHERE a.category LIKE '$category%' AND c.$menu LIKE '%$item_list%'
									  AND b.vendor_code = '$get_supplier'
									  AND YEAR(a.requested_delivery_date) = '$year' AND a.week = '$week'
									  ORDER BY a.id
									  ");
		}
		else if($get_supplier != '' AND $menu != ''){

			$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  b.vendor_alias ,
									  c.material_name,
									  d.nama,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tB_rds_detail as a
									  LEFT JOIN skin_master.ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN skin_master.ms_material as c ON a.material_code = c.material_sku
									  LEFT JOIN ms_user as d ON a.created_by = d.id
									  WHERE a.category LIKE '$category%' AND a.$menu LIKE '%$item_list%'
									  AND b.vendor_code = '$get_supplier' AND YEAR(a.requested_delivery_date) = '$year' AND a.week = '$week'
									  ORDER BY a.id
									  ");

		}else if($get_supplier != '')
		{
			$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  b.vendor_alias ,
									  c.material_name,
									  d.nama,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tB_rds_detail as a
									  LEFT JOIN skin_master.ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN skin_master.ms_material as c ON a.material_code = c.material_sku
									  LEFT JOIN ms_user as d ON a.created_by = d.id
									  WHERE a.category LIKE '$category%'
									  AND b.vendor_code = '$get_supplier' AND YEAR(a.requested_delivery_date) = '$year' AND a.week = '$week'
									  ORDER BY a.id
									  ");
		}else if($menu == 'material_name'){
			$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  b.vendor_alias ,
									  c.material_name,
									  d.nama,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tB_rds_detail as a
									  LEFT JOIN skin_master.ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN skin_master.ms_material as c ON a.material_code = c.material_sku
									  LEFT JOIN ms_user as d ON a.created_by = d.id
									  WHERE a.category LIKE '$category%' AND c.material_name LIKE '%$filter_text%' AND YEAR(a.requested_delivery_date) = '$year' AND a.week = '$week'
									  ORDER BY a.id
									  ");
		}else if($menu != ''){
			$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  b.vendor_alias ,
									  c.material_name,
									  d.nama,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tB_rds_detail as a
									  LEFT JOIN skin_master.ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN skin_master.ms_material as c ON a.material_code = c.material_sku
									  LEFT JOIN ms_user as d ON a.created_by = d.id
									  WHERE a.category LIKE '$category%' AND $menu LIKE '%$filter_text%' AND YEAR(a.requested_delivery_date) = '$year' AND a.week = '$week'
									  ORDER BY a.id
									  ");
		}else{
			$get_data = $this->db->query("SELECT
									  a.id,
									  a.status,
									  a.category,
									  a.po_number,
									  a.po_line_item,
									  a.material_code,
									  a.vendor_code,
									  a.qty,
									  a.uom,
									  a.requested_delivery_date,
									  a.shift,
									  a.uom_plt,
									  a.plt_truck,
									  a.req_pallet,
									  b.vendor_alias ,
									  c.material_name,
									  d.nama,
									  (SELECT nama FROM ms_user WHERE id = a.created_by) AS created_name,
                                      (SELECT nama FROM ms_user WHERE id = a.update_by) AS edit_name
									  FROM tB_rds_detail as a
									  LEFT JOIN skin_master.ms_supplier as b ON a.vendor_code = b.vendor_code
									  LEFT JOIN skin_master.ms_material as c ON a.material_code = c.material_sku
									  LEFT JOIN ms_user as d ON a.created_by = d.id
									  WHERE a.category LIKE '$category%' AND YEAR(a.requested_delivery_date) = '$year' AND a.week = '$week'
									  ORDER BY a.id
									  ");
		}
		$data['time_freeze'] = $time_freeze;
		$get_item_code = $this->db->query("SELECT material_code FROM tB_rds_detail where category='$category' ");
		$get_supplier = $this->db->query("SELECT vendor_code,vendor_name, vendor_alias FROM skin_master.ms_supplier");
		$get_category = $this->db->query("SELECT * FROM ms_category");
		$data['data'] = $get_data;
		$data['category'] = $get_category;
		$data['item_code'] = $get_item_code;
		$data['supplier'] = $get_supplier;
		if(isset($table_select)){
			getHTML('schedule/schedules2',$data);
		}else{
			getHTML('schedule/schedule2',$data);
		}

	}

	public function save_schedule_change(){
		$value = $this->input->post("value");
		$id = $this->input->post("id");
		$menu = $this->input->post("menu");
		if($menu == 'qty' || $menu == 'uom_plt'){
			if(isset($id)){
				$get_data = $this->db->query("SELECT qty,uom_plt FROM tb_rds_detail WHERE id='$id' ")->row();
				$qty 		= $get_data->qty;
				$uom_plt 	= $get_data->uom_plt;
				$req_plt    = '';
				if($menu =='qty'){
					$req_plt    = round($value/$uom_plt);
				}else{
					$req_plt    = round($qty/$value);
				}

				$status = $this->db->query("UPDATE tb_rds_detail SET $menu = '$value', req_pallet = '$req_plt' WHERE id='$id' ");
			}
		}else if($menu == 'requested_delivery_date'){
			if(isset($id)){
				$status = $this->db->query("UPDATE tb_rds_detail SET $menu = '$value', is_schedule = '0' WHERE id='$id' ");

				$schedule_number = $this->db->query("SELECT schedule_number FROM tb_scheduler WHERE id_schedule = '$id' ")->row()->schedule_number;
				$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule='$id' ");
			}
		}else{
			if(isset($id)){
				$status = $this->db->query("UPDATE tb_rds_detail SET $menu = '$value' WHERE id='$id' ");
			}
		}
		return true;
	}


	public function save_schedule2(){
		$arr_data = array();
		$c_date = date('Y-m-d');

		$yyy  = (null === $this->input->post('yyy'))?date('Y'):$this->input->post('yyy');
		$www  = (null === $this->input->post('www'))?date('W'):$this->input->post('www');

		if(null !== $this->input->get('category')){
			$category_uri = $this->input->get('category');
		}else{
			$category_uri = "FACE";
		}
		$i 						= 0;
		$arr_err 				= array();
		$arr_noact				= array();
		$arr_data_lack			= array();
		$arr_data_order			= array();
		$arr_sap_er				= array();
		$arr_sap_po				= array();
		$get_no 				= $this->input->post('no');
		$get_category 			= $this->input->post('category');
		$get_vendor 			= $this->input->post('vendor');
		$get_item_code 			= $this->input->post('item_code');
		$get_material_name 		= $this->input->post('material_name');
		$get_qty_per_palet 		= $this->input->post('qty_per_palet');
		$get_plan 				= $this->input->post('plan');
		$get_line 				= $this->input->post('line');
		$get_purchase_order 	= $this->input->post('purchase_order');
		$get_order_qty 			= $this->input->post('order_qty');
		$get_uom 				= $this->input->post('uom');
		$get_date 				= $this->input->post('date');
		$get_shift 				= $this->input->post('shift');
		$history['by_who']		= $this->session->userdata('sess_id');
		$history['date']		= Date('Y-m-d');
		$history['time']		= Date('H:i:s');
		$history['table_join']	= "tb_rds_detail";
		$history['select_join'] = "id";
		$history['author']		= 2;

		$get_bol				= $this->input->post('bill_of_lading');
		//echo json_encode($get_bol); die();
		$save = 0;
		$update = 0;
			foreach($get_category as $bru){
				if($bru == ""){continue;}
				$category 		= $bru;
				$vendor_split 	= explode(' ',$get_vendor[$i]);
				$vendor 		= $vendor_split[0];
				$item_code 		= $get_item_code[$i];
				$material_name 	= $get_material_name[$i];
				$qty_per_palet 	= (double)$get_qty_per_palet[$i];
				$plan 			= $get_plan[$i];
				$line 			= $get_line[$i];
				$purchase_order = $get_purchase_order[$i];
				$order_qty      = str_replace(",",".",$get_order_qty[$i]);
				$uom 			= $get_uom[$i];
				$bill_of_lading = $get_bol[$i];
				if($get_date != ''){
					$date 			= date('Y-m-d' ,strtotime($get_date[$i]));
				}else{
					$date       = '';
				}

				$shift 			= $get_shift[$i];
				$date_now 		= date('D' ,strtotime($get_date[$i]));
				$week 			= date('W' ,strtotime($get_date[$i]));

				$cekUomPlt = '';
				$getUom = $this->db->query("SELECT uom_pallet FROM skin_master.ms_material WHERE material_sku='$item_code'")->row();
				
				if($getUom != ''){
					$cekUomPlt = $getUom->uom_pallet;
				}

				$arr_rec = array(
									"category" => $category,
									"vendor" => $get_vendor[$i],
									"item_code" => $get_item_code[$i],
									"material_name" => $get_material_name[$i],
									"uom_plt" => $cekUomPlt,
									"plant" => $get_plan[$i],
									"line" => $get_line[$i],
									"po_number" => $get_purchase_order[$i],
									"order_qty" => $get_order_qty[$i],
									"uom" => $get_uom[$i],
									"date" => $get_date[$i],
									"shift" => $get_shift[$i],
									"bill_of_lading" => $get_bol[$i]
								);
				
				if($cekUomPlt == 0 || $cekUomPlt == ''){
					array_push($arr_data_lack, $item_code);
					$data_state = "3";

				}else{

					if( ($cekUomPlt != '') && ($order_qty != '') ){
						if(strpos(($order_qty/$cekUomPlt), ".")){

							$req_plt ="error";
							array_push($arr_data_order, $item_code);
							$data_state = "3";

						}else{
							$req_plt = $order_qty / $cekUomPlt;
						}
					}else{

						$req_plt = 'error';
						array_push($arr_data_lack, $item_code);
						$data_state = "3";
					}

					$data = array(
						"po_number" 			  => $purchase_order,
						"po_line_item" 			  => $line,
						"uom_plt" 			  	  => $cekUomPlt,
						"category" 				  => $category,
						"vendor_code"			  => $vendor,
						"material_code"			  => $item_code,
						"qty"					  => $order_qty,
						"qty_ori"				  => $order_qty,
						"uom"					  => $uom,
						"requested_delivery_date" => $date,
						"shift" 				  => $shift,
						"req_pallet" 			  => $req_plt,
						"plt_truck" 			  => $plan,
						"flag"					  => 0,
						"week"					  => $week,
						);

					if($category == "IBD"){
						$data['bill_of_lading'] = $bill_of_lading;
					}

					//echo "<pre>".json_encode($data,JSON_PRETTY_PRINT)."</pre>";
					//die();
					$history['vendor_code']	= $vendor;
					if($req_plt == 'error'){

					}

					elseif( ($line != null || $line != "") && ($purchase_order != null || $purchase_order != '') && ($category != null || $category != '') && ($vendor != null || $vendor != '') && ($item_code != null || $item_code != '') && ($order_qty != null || $order_qty != '') && ($uom != null || $uom != '') && ($date != null || $date != '') && ($date != null || $date != '') && ($shift != null || $shift != '') && ($plan != null || $plan != '')){

						$check_ava = $this->db->query("SELECT status, id, is_schedule, qty, req_pallet, requested_delivery_date, week, po_line_item, shift, material_code, uom_plt, sap_line, sap_stock_type, sap_sloc FROM tb_rds_detail WHERE po_number='$purchase_order' AND po_line_item='$line' AND material_code = '$item_code' ORDER BY id DESC");

						if($check_ava->num_rows() >= 1){
							$cek_row = $check_ava->row();
							$ids = $cek_row->id;
							$c_status = $cek_row->status;
							$c_sched = $cek_row->is_schedule;

							$this->db->where('id', $ids);
							$this->db->update('tb_rds_detail', array("flag" => 0));

							if($c_status != '1'){
								$data['update_by'] = $this->session->userdata('sess_id');
								$data['update_at'] = date('Y-m-d H:i:s');
								$data['is_schedule'] = 0;
								$get_check = $check_ava->row();
								if($get_check->qty == $order_qty && $get_check->week == $week && $get_check->shift == $shift && $get_check->requested_delivery_date == $date && $get_check->po_line_item == $line && $get_check->material_code == $item_code && $get_check->uom_plt == $cekUomPlt){
									$data_state = "4";

								}else{
									if($get_check->sap_line == ""){
										$cekpo = $this->cek_po_sap($purchase_order, $vendor, $item_code, $date, $order_qty);
										if($cekpo['status'] == true){
											$row = $cekpo['data'];
											$id_sap = $row->id;
											$qty_acc = $row->qty_acc;
											$data['sap_line'] = $row->item_number;
											$data['sap_stock_type'] = $row->stock_type;
											$data['sap_sloc'] = $row->storage_location;
											$data['sap_uom'] = $row->order_uom;
											$data['reference_po'] = $row->reference_po;

											$this->db->where('id', $ids);
											$this->db->update('tb_rds_detail', $data);
											$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule='$ids'");
											$mm = array(
												"id_schedule" => $get_check->id,
												"quantity"	  => $req_plt,
												"action"	  => "Update",
												"created_by"  => $this->session->userdata('sess_id')
											);
											add_material_movement($mm);
											$history['action']		= "Update";
											$history['value']		= '';
											$history['value2']		= '';
											$history['description'] = "RDS, PO :".$purchase_order.", line :".$line.", Material Code :".$item_code.", Qty :".$order_qty.", Req Pallet :".$req_plt." week :".$week;
											$this->db->insert('tb_history',$history);

											$acc = $qty_acc+$order_qty;
											$this->update_sap_qty($id_sap, $acc);
											$data_state = "1";
											$update++;
										}else{
											if($cekpo['msg'] == "PO_NOT_FOUND"){
												array_push($arr_sap_po, $purchase_order);
												$data_state = 5;
											}else{
												array_push($arr_sap_er, $purchase_order);
												$data_state = 6;
											}
										}
									}else{
										$this->db->where('id', $ids);
										$this->db->update('tb_rds_detail', $data);
										$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule='$ids'");
										$mm = array(
											"id_schedule" => $get_check->id,
											"quantity"	  => $req_plt,
											"action"	  => "Update",
											"created_by"  => $this->session->userdata('sess_id')
										);
										add_material_movement($mm);
										$history['action']		= "Update";
										$history['value']		= '';
										$history['value2']		= '';
										$history['description'] = "RDS, PO :".$purchase_order.", line :".$line.", Material Code :".$item_code.", Qty :".$order_qty.", Req Pallet :".$req_plt." week :".$week;
										$this->db->insert('tb_history',$history);
										$data_state = "1";
										$this->update_sap_qty($purchase_order, $vendor, $item_code, $date, $order_qty);
										$update++;
									}
								}
							}else{
									array_push($arr_noact, $purchase_order);
									$data_state = "2";
							}
						}else{
							$cekpo = $this->cek_po_sap($purchase_order, $vendor, $item_code, $date, $order_qty);
							if($cekpo['status'] == true){
								$row = $cekpo['data'];
								$id_sap = $row->id;
								$qty_acc = $row->qty_acc;
								$data['sap_line'] = $row->item_number;
								$data['sap_stock_type'] = $row->stock_type;
								$data['sap_sloc'] = $row->storage_location;
								$data['sap_uom'] = $row->order_uom;
								$data['reference_po'] = $row->reference_po;
								$data['created_by'] = $this->session->userdata('sess_id');
								$data['created_at'] = date('Y-m-d H:i:s');
								$data['is_schedule'] = 0;
								$this->db->insert('tb_rds_detail', $data);
								$last_id = $this->db->query("SELECT id FROM tb_rds_detail ORDER by id DESC")->row()->id;
								$mm = array(
												"id_schedule" => $last_id,
												"quantity"	  => $req_plt,
												"action"	  => "Add",
												"created_by"  => $this->session->userdata('sess_id')
								);
								add_material_movement($mm);
								$history['action']		= "Add";
								$history['id_join']		= $last_id;
								$history['description'] = "RDS, PO :".$purchase_order.", line :".$line.", Material Code :".$item_code.", Qty :".$order_qty.", Req Pallet :".$req_plt." week :".$week;
								$this->db->insert('tb_history',$history);
								$this->update_sap_qty($purchase_order, $vendor, $item_code, $date, $order_qty);

								$acc = $qty_acc+$order_qty;
								$this->update_sap_qty($id_sap, $acc);
								$data_state = "1";
								$save++;
							}else{
								if($cekpo['msg'] == "PO_NOT_FOUND"){
									array_push($arr_sap_po, $purchase_order);
									$data_state = 5;
								}else{
									array_push($arr_sap_er, $purchase_order);
									$data_state = 6;
								}
							}
						}
					}else{
						array_push($arr_err, $purchase_order);
						$data_state = "3";
					}
				}

				$arr_rec['data_state'] = $data_state;

				array_push($arr_data, $arr_rec);

				if($save > 0 || $update > 0){
					// echo "YES, WE HERE";
					$this->db->query("UPDATE tb_rds_detail SET is_schedule=0 WHERE requested_delivery_date='$date' AND status='0'");
					$this->db->query("DELETE FROM tb_schedule_detail WHERE rdd='$date' AND status=0");
					$this->db->query("DELETE FROM tb_scheduler WHERE rdd='$date' AND status='0'");
				}

				$i++;
			}

			if(count($arr_err) >= 1){
				$po_list = implode(", ", $arr_err);
				$this->session->set_flashdata('LIST_ERROR', $po_list);
			}

			if(count($arr_data_lack >= 1)){
				$data_lack = implode(", ", $arr_data_lack);
				$this->session->set_flashdata('LIST_DATA_LACK', $data_lack);
			}

			if(count($arr_data_order >= 1)){
				$data_order = implode(", ", $arr_data_order);
				$this->session->set_flashdata('LIST_DATA_ORDER', $data_order);
			}

			if(count($arr_noact) >= 1){
				$noact_list = implode(", ", $arr_noact);
				$this->session->set_flashdata('LIST_NOACT', $noact_list);
			}

			if(count($arr_sap_er) > 0){
				$sap_er = implode(", ", $arr_sap_er);
				$this->session->set_flashdata('SAP_ER', $sap_er);
			}

			if(count($arr_sap_po) > 0){
				$sap_po = implode(", ", $arr_sap_po);
				$this->session->set_flashdata('SAP_PO', $sap_po);
			}

			$this->db->query("UPDATE tb_data_checker AS a INNER JOIN ms_user AS b ON a.id_user = b.id SET a.data_checker =1 WHERE b.vendor_code != '' ");
			$this->session->set_flashdata('SAVED', "<b style='color:black'>Data berhasil tersimpan,</b> <b style='color:green'> Data Baru : ".$save." Data Update : ".$update."</b>");
		//echo json_encode($arr_data); die();
		$data['arr_data'] = $arr_data;
		$data['year'] = $yyy;
		$data['week'] = $www;
		getHTML('schedule/schedule_tbl_after_save', $data);
		//redirect('schedule/schedule2?category='.$category_uri."&year=".$yyy."&week=".$www);
	}

	public function delete_row_schedule2(){
		$id = $this->input->get('id');
		$category = $this->input->get('category');
		$get_scheduler = $this->db->query("SELECT schedule_number FROM tb_scheduler WHERE id_schedule = '$id' ")->row();
		if($get_scheduler == null){
			$schedule_number = $get_scheduler->schedule_number;
			$this->db->query("DELETE FROM tb_schedule_detail WHERE schedule_number='$schedule_number' ");
		}
		$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule='$id' ");
		$get_material = $this->db->query("SELECT material_code, material_name FROM tb_rds_detail as a INNER JOIN skin_master.ms_material as b on a.material_code = b.material_sku WHERE a.id='$id' ")->row();
		$material_code = $get_material->material_code;
		$material_name = $get_material->material_name;
		$this->db->query("DELETE FROM tb_rds_detail WHERE id='$id' ");
		$this->session->set_flashdata('DELETED', $material_code." : ".$material_name);
		return true;
		$this->db->query("DELETE FROM tb_rds_detail WHERE id='$id' ");
		$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule='$id'");

	}

	public function filter_menu(){
		$year = $this->input->post('year');
		$week = $this->input->post('wekk');
		$menu = $this->input->post('menu');
		$supplier = $this->input->post('supplier');
		$data = '';
		$category = $this->input->post('category');
		if($menu == 'supplier'){
			$data = $this->db->query("SELECT vendor_code, vendor_name, vendor_alias FROM skin_master.ms_supplier");
		}
		else if($menu != 'material_name'){
			$data = $this->db->query("SELECT $menu FROM tb_rds_detail WHERE category = '$category' AND YEAR(requested_delivery_date) = '$year' AND week = '$week' AND flag = 0 AND vendor_code = '$supplier' GROUP by $menu");
		}
		else{
			$data = $this->db->query("SELECT $menu FROM tb_rds_detail as a inner join skin_master.ms_material as b on a.material_code = b.material_sku WHERE a.category = '$category' AND YEAR(a.requested_delivery_date) = '$year' AND week = '$week' AND flag = 0 AND a.vendor_code = '$supplier' GROuP BY a.material_code");
		}

		$option = "<option value=''>--SELECT ITEM--</option>";
		if($menu != 'supplier'){
			foreach ($data->result() as $get) {
				$option = $option."<option value='".$get->$menu."'>".$get->$menu."</option>";
			}
		}else{
			foreach ($data->result() as $get) {
				$option = $option."<option value='".$get->vendor_code."'>".$get->vendor_alias.' - '.$get->vendor_name."</option>";
			}
		}
		echo $option;

		return true;
	}

	public function delete_schedule2(){

			$this->db->query("DELETE FROM tb_schedule_detail");
			$this->db->query("DELETE FROM tb_scheduler");
			$this->db->query("DELETE FROM tb_rds_detail");

		return true;

	}

	public function clear_flag(){
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$category = $this->input->get('category');
		$this->db->query("UPDATE tb_rds_detail SET flag='1' WHERE week = '$week' AND YEAR(requested_delivery_date) = '$year' AND category = '$category' ");
		$history = array(
				"date"			=> date('Y-m-d'),
				"time"			=> date('H:i:s'),
				"action"		=> "Clear",
				"by_who"		=> $this->session->userdata('sess_id'),
				"table_join" 	=> "tb_rds_detail",
				"id_join"		=> '',
				"description"	=> "RDS data table category :".$category.", Year :".$year.", Week :".$week,
				"value"			=> '',
				"value2"		=> '',
				"author"		=> 2
		);
			$this->db->insert('tb_history',$history);
	}

	public function check_delete(){
		$data = $this->input->get('check_item');
		$no = 0;

		foreach($data as $get){
			$check_item = $data[$no];
			$data_item = $check_item['check_item'];
			$dataz = $this->db->query("SELECT po_number,material_code, po_line_item, week, id, qty FROM tb_rds_detail WHERE id = '$data_item' AND status = '0' ");

			$purchase_order = $dataz->row()->po_number;
			$line = $dataz->row()->po_line_item;

			if($dataz->num_rows() > 0){
			$this->db->query("DELETE FROM tb_rds_detail WHERE id = '$data_item' ");
			$this->db->query("DELETE FROM tb_scheduler WHERE id_schedule = '$data_item' ");
			$history = array(
				"date"			=> date('Y-m-d'),
				"time"			=> date('H:i:s'),
				"action"		=> "Delete",
				"by_who"			=> $this->session->userdata('sess_id'),
				"table_join" 	=> "tb_rds_detail",
				"id_join"		=> $data_item,
				"description"	=> "RDS, PO :".$dataz->row()->po_number.", line :".$dataz->row()->po_line_item.", Material Code :".$dataz->row()->material_code.", Qty :".$dataz->row()->qty.", Req Pallet :".$req_plt." week :".$dataz->row()->week,
				"value"			=> '',
				"value2"		=> '',
				"author"		=> 2
			);

			$this->db->insert('tb_history',$history);
			}

			$no++;
		}
		if($no > 0){
			$this->session->set_flashdata('DELETED','Data telah dihapus');
		}

	}

	//End Schedule 2

	public function check_status_schedule(){
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$sn 		 = $this->input->get()['sn'];
		$shift       = $this->input->get('shift');
		$date 	 	 = $this->input->get('date');
		$check_rdd 	 = $this->db->query("SELECT rdd FROM tb_schedule_detail WHERE schedule_number='$sn' AND vendor_code = '$vendor_code' ")->row()->rdd;
		$datenow = date('Y-m-d');
		$checker_shift = '';
		$checker ='';

		$c_time = date('H:i:s');
		$getShift = $this->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row()->shift;
		$data1_1 = [];
		if($date > $datenow){

			if($getShift == 'DM'){
				$data1_1 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS e ON e.schedule_number = a.schedule_number  WHERE (SELECT count(c.id) FROM tb_scheduler AS c INNER JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number ) > 0 AND  a.rdd = '$datenow' AND a.vendor_code = '$vendor_code' AND a.status = 0 GROUP BY e.schedule_number")->result();
			}else if($getShift == 'DP'){
				$data1_1 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS e ON e.schedule_number = a.schedule_number  WHERE (SELECT count(c.id) FROM tb_scheduler AS c INNER JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number ) > 0 AND  a.rdd = '$datenow' AND a.vendor_code = '$vendor_code' AND a.status = 0 AND shift != 'DM' GROUP BY e.schedule_number")->result();
			}else{
				$data1_1 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS e ON e.schedule_number = a.schedule_number  WHERE (SELECT count(c.id) FROM tb_scheduler AS c INNER JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number ) > 0 AND  a.rdd = '$datenow' AND a.vendor_code = '$vendor_code' AND a.status = 0 AND shift = '$getShift' GROUP BY e.schedule_number")->result();
			}


		}
		$data1_2	 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS e ON e.schedule_number = a.schedule_number  WHERE (SELECT count(c.id) FROM tb_scheduler AS c INNER JOIN tb_schedule_detail AS d ON c.schedule_number = d.schedule_number ) > 0 AND  a.rdd  < '$check_rdd' AND a.rdd > '$datenow' AND a.vendor_code = '$vendor_code' AND a.status = 0 GROUP BY e.schedule_number")->result();

		$data1 = array_merge($data1_1, $data1_2);

		$data2 = [];


		//dump($datenow.' '.$check_rdd);
		if($datenow == $check_rdd){

			if($getShift == "DM" && $shift == "DP"){

				$data2 	 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS c ON c.schedule_number = a.schedule_number  WHERE a.rdd = '$check_rdd' AND a.vendor_code = '$vendor_code' AND a.status = 0 AND b.shift = 'DM' GROUP BY c.schedule_number ")->result();


			}else if($getShift == "DM" && $shift == "DS"){

				$data2 	 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS c ON c.schedule_number = a.schedule_number  WHERE a.rdd = '$check_rdd' AND a.vendor_code = '$vendor_code' AND a.status = 0 AND b.shift != 'DS' GROUP BY c.schedule_number ")->result();
			}else if($getShift == "DP" && $shift == "DS"){

				$data2 	 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS c ON c.schedule_number = a.schedule_number  WHERE a.rdd = '$check_rdd' AND a.vendor_code = '$vendor_code' AND a.status = 0 AND b.shift = 'DP' GROUP BY c.schedule_number ")->result();
			}

		}else{

			if($shift == 'DP'){
				$data2 	 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS c ON c.schedule_number = a.schedule_number  WHERE a.rdd = '$check_rdd' AND a.vendor_code = '$vendor_code' AND a.status = 0 AND b.shift = 'DM' GROUP BY c.schedule_number ")->result();

			}else if($shift == "DS"){
				$data2 	 = $this->db->query("SELECT a.rdd, b.shift, b.start_time, b.end_time FROM tb_schedule_detail as a INNER JOIN ms_time_slot as b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS c ON c.schedule_number = a.schedule_number  WHERE a.rdd = '$check_rdd' AND a.vendor_code = '$vendor_code' AND a.status = 0 AND b.shift != 'DS' GROUP BY c.schedule_number ")->result();
			}

		}


		$data_rdd = array_merge($data1,$data2);

		echo json_encode($data_rdd);
		return true;
	}

	public function freeze_time_update(){

		$id = $this->input->post('id');
		$start_time = $this->input->post('start_time');
		$end_time = $this->input->post('end_time');

		$array = array(
			"start_time" => $start_time,
			"end_time"	=> $end_time
		);

		$this->db->query("UPDATE tb_data_checker SET periode_checker = 1 ");

		$this->db->where('id', $id);
		$this->db->update("tb_time_freeze", $array);
		return true;
	}

	public function check_adt(){
		$role = $this->session->userdata('sess_role_no');
		$view ='';
		$getData = [];
		if($role == 1){
			$view = "view_admin = 0";
		}else if($role == 2){
			$view = "view_progressor = 0";
		}
		if($view != ''){
		$getData = $this->db->query("SELECT a.id, a.reason, b.vendor_code, b.rdd, e.shift, e.start_time, e.end_time FROM tb_additional_schedule 						 AS a
									 INNER JOIN tb_schedule_detail AS b ON a.schedule_number = b.schedule_number
									 LEFT JOIN tb_scheduler AS c ON c.schedule_number = b.schedule_number
									 INNER JOIN tb_rds_detail AS d ON c.id_schedule = d.id
									 INNER JOIN ms_time_slot AS e ON e.id = b.id_time_slot WHERE $view 
									 GROUP BY b.id")->result();
		}
		foreach ($getData as $get) {
				if($role == 1){
					$array = array(
						"view_admin" => 1
					);
				}else if($role == 2){
					$array = array(
						"view_progressor" => 1
					);
				}

			$this->db->where('id',$get->id);
			$this->db->update('tb_additional_schedule',$array);
		}
		echo json_encode($getData);

	}

	public function pop_event(){
		$url = $this->input->post('url');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$id_time_slot = $this->input->post('id_time_slot');
		$date_event = $this->input->post('date_event');

		$checker_event = $this->db->query("SELECT id FROM tb_schedule_event AS a WHERE a.date ='$date_event' AND a.id_time_slot = '$id_time_slot' ");
		$data = array(
				"title_event" 	=> $title,
				"id_time_slot" 	=> $id_time_slot,
				"date"			=> $date_event,
				"event"			=> $content
			);

		if($checker_event->num_rows() == 0){
			$this->db->insert('tb_schedule_event', $data);
			$this->db->query("UPDATE tb_data_checker SET freezing_event_checker = 1");
			$get_time = $this->db->query("SELECT start_time, end_time FROM ms_time_slot WHERE id = '$id_time_slot' ")->row();

			$start_time = $get_time->start_time;
			$end_time = $get_time->end_time;

			$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Freezing Time",
	  			"by_who"		=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "ms_user",
	  			"description"	=> "Freezing Time, Date :".$date_event.", Time Slot :".$start_time.'-'.$end_time,
	  			"author"		=> 1
	  	);

	  	$this->db->insert('tb_history', $history);

		}else{
			$this->db->where('id', $checker_event->row()->id);
			$this->db->update('tb_schedule_event', $data);
		}

		redirect('schedule'.$url);
	}

	public function cancel_event(){
		$id = $this->input->get('id');
		$url = '?year='.$this->input->get('year').'&week='.$this->input->get('week').'&date='.$this->input->get('date');
		$this->db->query("DELETE FROM tb_schedule_event WHERE id ='$id' ");
		redirect('schedule'.$url);
	}

	public function check_9shift(){
		$role = $this->session->userdata('sess_role_no');
		$vendor_code = $this->session->userdata('sess_vendor_code');
		if($role != 3){
			$last_shift = $this->db->query("SELECT b.shift FROM tb_schedule_detail AS a INNER JOIN ms_time_slot AS b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS c ON a.schedule_number = c.schedule_number WHERE a.status = 0 ORDER BY a.id ASC ")->row();
		}else{
			$last_shift = $this->db->query("SELECT b.shift FROM tb_schedule_detail AS a INNER JOIN ms_time_slot AS b ON a.id_time_slot = b.id INNER JOIN tb_scheduler AS c ON a.schedule_number = c.schedule_number WHERE a.vendor_code = '$vendor_code' AND a.status = 0 ORDER BY a.id ASC ")->row();
		}

		echo json_encode($last_shift);

	}

	public function freeze_time_update_new()
	{
		$data = json_decode($this->input->post('data'));
		$status = 0;

		foreach ($data as $key => $value) {

			if(isset($value->start_hour) && isset($value->start_minute)){
				$data_time = "{$value->start_hour}:{$value->start_minute}";
				$status = $this->db->where('id', $value->id)
					->update('tb_time_freeze',[
						'start_time' => $data_time
					]);
			}

			if(isset($value->end_hour) && isset($value->end_minute)){
				$data_time = "{$value->end_hour}:{$value->end_minute}";
				$status = $this->db->where('id', $value->id)
					->update('tb_time_freeze',[
						'end_time' => $data_time
					]);
			}
		}
		$this->db->query("UPDATE tb_data_checker AS a INNER JOIN ms_user AS b ON a.id_user = b.id SET a.periode_checker = 1 WHERE b.role = 2");
		echo $status;
	}

	public function check_outstanding(){
		$this->load->model('outstanding/OS_Model');
		date_default_timezone_set('Asia/Jakarta');
		$shift = getShift();
		$role = $this->session->userdata('sess_role_no');
		$arr_vendor = array();
		$arr_id = array();
		$today  = date('Y-m-d');
		$year = $this->input->get('year');
		$week = $this->input->get('week');
		$sess_no = $this->session->userdata('sess_role_no');
		$sess_id = $this->session->userdata('sess_id');
		$pl = [];
		$check_date_time = date('Y-m-d h:i:s');
		$get_last_login = $this->db->query("SELECT last_login FROM ms_user WHERE id = '$sess_id' ")->row()->last_login;
		$add_last_login = date('Y-m-d h:i:s', strtotime('+30 minutes',strtotime($get_last_login)));
		
		if( ((strtotime($check_date_time) - strtotime($add_last_login))/60) <= 30 ){
			$pl = [];
			$data['vendor_list'] = $this->db->query("SELECT vendor_code, vendor_alias FROM skin_master.ms_supplier")->result();
			$data['vendor'] = $this->input->get('vendor_code');
			$data['data_pool'] = $pl;
			echo "false";		
		}else{
			$this->db->query("UPDATE ms_user SET last_login = '$check_date_time' WHERE id = '$sess_id' ");
			$vc = $this->input->get('vendor_code');
			$imp = $vc;
			$dataPool = $this->OS_Model->getMaterialVendor(0, 0, $sess_no, $vc);
			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$dateLess = $this->OS_Model->getDateLessVen(0,0, 0, $sess_no, $vc);

			foreach($dataPool->result() as $i){
				array_push($arr_id, $i->id);
			}

			if(count($arr_id) > 0){
				$imp = implode(",", $arr_id);
			}else{
				$imp = "";
			}

			$shiftLate = $this->OS_Model->getShiftLateVen($today, $shift, $imp, 0, $week, $sess_no, $vc);

			$pl = array_merge($dataPool->result(), $dateLess->result(), $shiftLate->result());
			$data['vendor_list'] = $this->db->query("SELECT vendor_code, vendor_alias FROM skin_master.ms_supplier")->result();
			$data['vendor'] = $this->input->get('vendor_code');
			$data['data_pool'] = $pl;
			$this->load->view('schedule/modal_outstanding', $data);
		}
		
	}

	public function update_batch(){
		$data = array(
			"prod_date"   	=> $this->input->get('prod_date'),
			"exp_date"    	=> $this->input->get('exp_date')
		);
		$this->db->where('id', $this->input->get('id'));
		$this->db->update('tb_scheduler', $data);
		return true;
	}

	public function cek_po_sap($po_number, $vendor_code, $material_number, $delivery_date, $quantity){
		$arr = array();
		$cek_po = $this->db->query("SELECT * FROM sap_temp_db.tb_po_subpo WHERE po_number='$po_number' AND vendor_code='$vendor_code' AND material_number='$material_number'");

		if($cek_po->num_rows() >= 1){
			foreach($cek_po->result() as $data){
				$po_qty = $data->order_qty;
				$po_qty_acc = $data->qty_acc;
				$temp_qty = $po_qty_acc+$quantity;
				if($temp_qty > $po_qty){
					if($temp_qty > $po_qty){
						$arr = array("status" => false, "msg" => "QTY_EXCEEDED");
					}else{
						$arr = array("status" => true, "msg" => "PO_FOUND", "data" => $data);
						break;
					}
				}else{
					$arr = array("status" => true, "msg" => "PO_FOUND", "data" => $data);
					break;
				}
			}
		}else{
			$arr = array("status" => false, "msg" => "PO_NOT_FOUND");
		}
		return $arr;
	}

	public function update_sap_qty($id_sap, $qty){
		$cek_po = $this->db->query("UPDATE sap_temp_db.tb_po_subpo SET qty_acc=$qty WHERE id=$id_sap");		
	}

	public function getScheduleNumber(){
		$alias = $this->session->userdata("sess_vendor_alias");
		$getNumber = $this->db->query("SELECT
									IF (
										CHAR_LENGTH(schedule_number) = 4,
										SUBSTR(schedule_number, 1, 4)+1,
										SUBSTR(schedule_number, 4, 10)+1
									) as SN_AFTER_INC
									FROM
										`tb_schedule_detail`
									ORDER BY
										CAST(IF (
										CHAR_LENGTH(schedule_number) = 4,
									SUBSTR(schedule_number, 1, 4),
									 SUBSTR(schedule_number, 4, 10)
									) AS INT
									) DESC
									LIMIT 0, 1")->row();
		$number = $getNumber->SN_AFTER_INC;

		if($number < 10){
			$unique = "000000000".$number;
		}else if($number < 100){
			$unique = "00000000".$number;
		}else if($number < 1000){
			$unique = "0000000".$number;
		}else if($number < 10000){
			$unique = "000000".$number;
		}else if($number < 100000){
			$unique = "00000".$number;
		}else if($number < 1000000){
			$unique = "0000".$number;
		}else if($number < 10000000){
			$unique = "000".$number;
		}else if($number < 100000000){
			$unique = "00".$number;
		}else if($number < 1000000000){
			$unique = "0".$number;
		}else{
			$unique = $number;
		}

		$combine = $alias.$unique;
		return $combine;
	}

	public function check_planned(){
		$sn = $this->input->get('sn');
		//$sn = "SKL0000003831";

		$count = $this->db->query("SELECT status FROM tb_schedule_detail WHERE schedule_number='$sn'")->row();

		if($count->status >= 1){
			$arr = array("status" => "PLANNED");
		}else{
			$arr = array("status" => "NOT_PLANNED");
		}
		echo json_encode($arr);
	}

	public function getCategory_sch($sn){
		$data = $this->db->query("SELECT 
			category 
			FROM tb_rds_detail 
			INNER JOIN tb_scheduler ON tb_rds_detail.id = tb_scheduler.id_schedule WHERE schedule_number='$sn' LIMIT 0,1");
		if($data->num_rows() == 1){
			$data_cat = $data->row();
			return $data_cat->category;
		}else{
			return "FACE";
		}
	}
}
?>
