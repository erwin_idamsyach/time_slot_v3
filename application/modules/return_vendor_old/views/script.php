<script>
	
	$('#category').select2();

	function viewPODetail(po_number){
		$.ajax({
			"type" : "GET",
			"url"  : "<?php echo base_url(); ?>return_vendor/get_data_detail",
			"data" : "po_number="+po_number,
			success:function(resp){
				openModal("DETAIL PO NUMBER : "+po_number, resp);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		});
	}

	$("#table").DataTable({
		"scrollX": true,
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>',
		
	});

	function getDetailedInformation(key,status){
		$(".modal").modal('hide');
		setTimeout(function(){
			$.ajax({
				type : "GET",
				url : "<?php echo base_url() ?>schedule/get_detailed_information",
				data : {
					'key' : key,
					'status' : status
				},
				success:function(resp){
					openModal("Information", resp, "xl");
				},
				error:function(e){
					alert("Something wrong!");
					console.log(e);
				}
			})
			
		}, 200);	
	}

	function setWeek(week, state){
		/*alert(week+" "+state);*/
		var year = $('#year').val();
		if(state == "INC"){
			week = parseInt(week)+1;
			$('.week').val(week);
		}else{
			week = parseInt(week)-1;
			$('.week').val(week);
		}
		window.location=('<?php base_url() ?>return_vendor?year='+year+'&week='+week);
	}


	function setYear2(year, state){
			var year = $('#year').val();
			var week = $('.week').val();
			window.location=('<?php base_url() ?>return_vendor?year='+year+'&week='+week);
	}


	function setYear(year, state){
		/*alert(week+" "+state);*/
		var year = $('#year').val();
		var week = $('.week').val();
		if(state == "INC"){
			year = parseInt(year)+1;
			$('#year').val(year);
		}else{
			year = parseInt(year)-1;
			$('#year').val(year);
		}
		window.location=('<?php base_url() ?>return_vendor?year='+year+'&week='+week);
	}


	function getTruckDetail(truck_no, dudate){
		$("#title-truck").html('<label>Truck No <i class="fa fa-spinner fa-spin fa-temp"></i></label>');

		$.ajax({
			type : "GET",
			url : "<?php echo base_url() ?>return_vendor/get_truck_detail",
			data : {
				'truck_no' : truck_no,
				'date'	   : dudate
			},
			success:function(resp){
				$(".area-slot_ava").html("<h5>Slot Available : <b>"+resp+"</b></h5>")
				$(".fa-temp").remove();
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
				$(".fa-temp").remove();
			}
		})
	}

	function deliveryDetail(id){
		$.ajax({
			type : "GET",
			url : "<?php echo base_url(); ?>return_vendor/get_delivery_detail",
			data : {
				"id" : id
			},
			success:function(resp){
				openModal("Delivery Detail", resp);
			},
			error:function(e){
				console.log(e);
			}
		})
	}

	$("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 	});

 	function check_delete(){
 		if(($('input:checkbox:checked').length) == 1){
 			var confirmz = confirm('Are you sure you want to delete this?');
 		}else{
 			var confirmz = confirm('Are you sure you want to delete this all?');
 		}
 		
 		var check_item = [];
 		var a = 0;
            $.each($("input[name='check_item']:checked"), function(){            
                check_item.push({"check_item" : $(this).val()});
            	// check_item[a] = $(this).val();
            	// a += 1;
            });
            // alert("Item yang di check adalah: " + check_item[0].check_item);
            // console.log(check_item);

 		$.ajax({
 			type : "GET",
 			url  : "<?php echo base_url(); ?>return_vendor/check_delete",
 			data : {
 				"check_item" : check_item,
 			},
 			dataType : "JSON",
 			success:function(resp){
 				location.reload();
 			},
 			error: function(e){
 				location.reload();
 				console.log(e);
 			}
 		});
 		
 	}

 	function select_category(a){
 		var year = <?php echo $_GET['year']; ?>;
 		var week = <?php echo $_GET['week']; ?>;
 		window.location = "<?php echo base_url(); ?>return_vendor?year="+year+"&week="+week+"&category="+a;
 	}

 	var return_amountz = 0;

 	function openReturnModal(id, total, vendor_code, receipt, material_code, schedule_number, po_number){
 		return_amountz = total;
 		$("#myModal").modal('show');
 		$(".id_material").val(id);
 		$("#vendor_code").val(vendor_code);
 		$("#receipt").val(receipt);
 		$("#material_code").val(material_code);
 		$("#value1").val(total);
 		$("#po_number").val(po_number);
 		$(".total_material").val(total);
 		$('#receipt').val(receipt);
 		$('#schedule_number').val(schedule_number);
 		if(total == 0){
 			$('#return_button').attr("disabled",true);
 		}

 	}

 	function max_return(a){
 		
 		if(a > return_amountz){
 			alert("Return amount tidak dapat melebihi batas");
 			$('.total_material').val(return_amountz);
 			$('#return_button').attr("disabled",false);
 		}else if(a == 0){
 			$('#return_button').attr("disabled",true);
 		}else if(a < 0){
 			alert("Return amount tidak dapat kurang dari 0");
 			$('.total_material').val(return_amountz);
 			$('#return_button').attr("disabled",false);
 		}else{
 			$('#return_button').attr("disabled",false);
 		}
 	}

 		var year = "<?php echo $_GET['year'] ?>";
 		var week = "<?php echo $_GET['week'] ?>";
 	function select_menu(a){

 		
 		$('#item_list').empty();
 		if(a == 1 || a == 2){
 			
 			$.ajax({
 			type : "GET",
 			url  : "<?php echo base_url(); ?>return_vendor/filter",
 			data : {
 				"category" : category,
 				"menu"		: a,
 				"year"		: year,
 				"week"		: week
 			},
 			
 			success:function(resp){
 				$('#item_list').attr('disabled', false);
 				$('#item_list').append(resp);
 			},
 			error: function(e){
 				alert(e);
 				//location.reload();
 				console.log(e);
 			}
 		});
 			// });
 		}else{
 			
 			$('#item_list').attr('disabled', true);
 		}
 	}

 	function search_receipt(a,b){

 		if(a == 1){
 			window.location=("<?php echo base_url() ?>return_vendor?year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>&receipt="+b);
 		}else if(a == 2){
 			window.location=("<?php echo base_url() ?>return_vendor?year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>&supplier="+b);
 		}else{
 			window.location=("<?php echo base_url() ?>return_vendor?year=<?php echo $_GET['year'] ?>&week=<?php echo $_GET['week'] ?>");
 		}
 		
 	}

</script>