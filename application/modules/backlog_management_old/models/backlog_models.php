<?php

class backlog_models extends CI_Model{

	public function dataDN($rdd, $vendor_code){
		date_default_timezone_set("Asia/Bangkok");
		$c_time = date('H:i:s');
		$id_time_slot = $this->db->query("SELECT id FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$id_time = $id_time_slot->id;
		$data = [];
		if($rdd < date('Y-m-d')){
			$filter = "";
		}else if($rdd > date('Y-m-d')){
		
		}else{
			$filter = "AND (a.arrived_id_time_slot > b.id_time_slot OR d.end_time < '$c_time')";
		}
		if($rdd <= date('Y-m-d')){
			$data = $this->db->query("SELECT
									d.end_time,
									b.id as idz,
									b.status,
									c.vendor_code,
									c.vendor_name,
									c.vendor_alias,
									f.police_number AS id_truck,
									a.delivery_date,
									a.id_time_slot,
									a.arrive_time,
									a.arrive_date,
									e.do_number,
									a.receipt,
									a.id_schedule_group as sn,
									d.id,
									a.id_time_slot,
									d.ordering,
									CONCAT(d.start_time,' - ',d.end_time) as TIME_SLOT,
									(SELECT CONCAT(start_time,' - ',end_time) FROM ms_time_slot WHERE id =g.update_time_slot) AS UPDATE_TIME_SLOT
								FROM
									tb_delivery_detail a
								LEFT JOIN tb_schedule_detail b ON a.id_schedule_group = b.schedule_number
								LEFT JOIN tb_scheduler AS z ON b.schedule_number = z.schedule_number
								INNER JOIN skin_master.ms_supplier c ON b.vendor_code = c.vendor_code
								LEFT JOIN tb_backlog AS g ON b.schedule_number = g.schedule_number
								LEFT JOIN ms_time_slot d ON b.id_time_slot = d.id 
								LEFT JOIN ms_truck AS f ON a.id_truck = f.id 
								LEFT JOIN tb_delivery_invoice as e ON a.id_schedule_group = e.id_schedule
								WHERE b.rdd = '$rdd'
								AND b.status = 2 
								$filter
								GROUP BY a.receipt
								");	
		}
		return $data;
	}

	public function backlog_update($rdd){
		
	}


	public function dataTimeSlot($shift){
		$data = array();
		foreach($shift as $shf){
			$data[$shf] = $this->db->query("SELECT id,start_time, end_time 
								  FROM ms_time_slot WHERE shift = '$shf'
								  ORDER BY ordering
			")->result();
		}
		return $data;
	}

	public function dataSchedule($year, $date){
		$data = array();
		$dataz = $this->db->query("SELECT COUNT(id_time_slot) as total, id_time_slot, id FROM tb_schedule_detail WHERE rdd = '$date' GROUP BY id_time_slot")->result();
		foreach($dataz as $d){
			$data[$d->id_time_slot] = $d->total;
		}
		return $data;
	}

	public function dataScheduleMove($rdd, $shift, $rdd_future){
		$data1 = array();
		$data2 = [];

		if($shift == 'DM'){
			$filter_shift = "";
		}else if($shift == "DP"){
			$filter_shift = "AND e.shift != 'DM' ";

		}else if($shift == "DS"){
			$filter_shift = "AND e.shift = 'DS'";
		}

		$data = array();
		$no = 0;
		$time_slot = $this->db->query("SELECT * FROM ms_time_slot ORDER BY ordering");
		$total = $time_slot->num_rows();

		foreach($time_slot->result() as $ts){
			$data1[$rdd][$ts->shift][$no][$ts->id] = array(
				"id"			=> $ts->id,
				"start_time"	=> $ts->start_time,
				"end_time"		=> $ts->end_time,
				"shift"			=> $ts->shift
			);

			$data2[$rdd_future][$ts->shift][$no][$ts->id] = array(
				"id"			=> $ts->id,
				"start_time"	=> $ts->start_time,
				"end_time"		=> $ts->end_time,
				"shift"			=> $ts->shift
			);

			$data1[$rdd][$ts->shift][$no][$ts->id]["detail"]	= $this->db->query("SELECT a.id, a.vendor_code, b.vendor_alias, 
				a.id_time_slot, 
				a.schedule_number, 
				a.status,
				d.police_number,
				a.zara2,
				e.shift,
				c.gr_material_doc
				FROM tb_schedule_detail AS a 
				LEFT JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
				LEFT JOIN tb_scheduler AS z ON a.schedule_number = z.schedule_number 
				LEFT JOIN tb_delivery_detail AS c ON a.schedule_number = c.id_schedule_group
				LEFT JOIN ms_truck AS d ON c.id_truck = d.id
				LEFT JOIN ms_time_slot AS e ON a.id_time_slot = e.id
				WHERE a.rdd = '$rdd' AND a.id_time_slot = '$ts->id' 
				GROUP BY a.id")->result();

			$data2[$rdd_future][$ts->shift][$no][$ts->id]["detail"]	= $this->db->query("SELECT a.id, a.vendor_code, b.vendor_alias, 
				a.id_time_slot, 
				a.schedule_number, 
				a.status,
				d.police_number,
				a.zara2,
				e.shift,
				c.gr_material_doc
				FROM tb_schedule_detail AS a 
				LEFT JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code
				LEFT JOIN tb_scheduler AS z ON a.schedule_number = z.schedule_number  
				LEFT JOIN tb_delivery_detail AS c ON a.schedule_number = c.id_schedule_group
				LEFT JOIN ms_truck AS d ON c.id_truck = d.id
				LEFT JOIN ms_time_slot AS e ON a.id_time_slot = e.id
				WHERE a.rdd = '$rdd_future' AND a.id_time_slot = '$ts->id' 
				GROUP BY a.id")->result();

			$no++;
		}
		$data = array_merge($data1, $data2);
		return $data;
	}

	public function dataScheduleDetail($date){
		$data = array();
		$no = 0;
		$time_slot = $this->db->query("SELECT * FROM ms_time_slot ORDER BY ordering");
		$total = $time_slot->num_rows();
		for($i = 0; $i< $total; $i++){
			$countz[$i] = 0;
		}
		foreach($time_slot->result() as $ts){
			$data[$ts->shift][$no][$ts->id] = array(
				"id"			=> $ts->id,
				"start_time"	=> $ts->start_time,
				"end_time"		=> $ts->end_time,
				"shift"			=> $ts->shift
			);
			$data[$ts->shift][$no][$ts->id]["detail"]	= $this->db->query("SELECT a.id, a.vendor_code, b.vendor_alias, 
				a.id_time_slot, 
				a.schedule_number, 
				a.status,
				d.police_number,
				a.zara2
				FROM tb_schedule_detail AS a 
				LEFT JOIN skin_master.ms_supplier AS b ON a.vendor_code = b.vendor_code 
				LEFT JOIN tb_delivery_detail AS c ON a.schedule_number = c.id_schedule_group
				LEFT JOIN ms_truck AS d ON c.id_truck = d.id
				WHERE a.rdd = '$date' AND a.id_time_slot = '$ts->id' 
				GROUP BY a.id
				")->result();
			$no++;
		}
		return $data;
	}

	public function dataHistoryBacklog($rdd){
		$data = [];
		
		$data = $this->db->query("SELECT
									
									a.receipt,
									c.vendor_alias,
									f.police_number,
									g.id_time_slot,
									g.rdd,
									(SELECT CONCAT(start_time, ' - ', end_time ) as time_slot FROM ms_time_slot WHERE id = g.update_time_slot) AS update_time_slot,
									g.update_rdd,
									a.arrive_time,
									g.schedule_number,
									g.status,
									(SELECT CONCAT(start_time, ' - ', end_time ) as time_slot FROM ms_time_slot WHERE id = g.id_time_slot) AS time_slot
									FROM
										tb_delivery_detail a
									INNER JOIN tb_schedule_detail b ON a.id_schedule_group = b.schedule_number
									INNER JOIN skin_master.ms_supplier c ON b.vendor_code = c.vendor_code
									INNER JOIN tb_backlog AS g ON b.schedule_number = g.schedule_number
									INNER JOIN ms_time_slot d ON b.id_time_slot = d.id 
									LEFT JOIN ms_truck AS f ON a.id_truck = f.id 
									LEFT JOIN tb_delivery_invoice as e ON a.id_schedule_group = e.id_schedule
									WHERE g.rdd = '$rdd'
									GROUP BY g.id
									")->result();
		return $data;
	}
	

	
}

?>