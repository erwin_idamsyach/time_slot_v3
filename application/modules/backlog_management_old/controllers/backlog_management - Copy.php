<?php

class backlog_management extends CI_Controller{

	function __Construct(){
		parent::__Construct();
		$this->load->model('backlog_models');
		isLogin();
	}

	public function indexz(){
		$date = date('Y-m-d');
		$c_date = new DateTime($date);
		$rdd = $this->input->get('date');
		$year = $_GET['year'];
		$week = $_GET['week'];
		$shift = ["DM","DP","DS"];
		$data_shift = array();
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$dataDN = $this->backlog_models->dataDN($rdd, $vendor_code);
		$data_time_slot = $this->backlog_models->dataTimeSlot($shift);
		foreach ($shift as $shf) {
			$data_array[$shf] = $this->db->query("SELECT COUNT(id) as total FROM ms_time_slot WHERE shift ='$shf' ")->row()->total;
		}
		$data_schedule = $this->backlog_models->dataSchedule($year, $date);
		$data_schedule_detail = $this->backlog_models->dataScheduleDetail($date, $data_schedule);
		$date_list = $this->getDateList($week, $year);
		$this->load->model('schedule/Schedule_model');
		$data["data_schedule"] = $data_schedule;	
		$data["data_time_slot"]	= $data_time_slot;
		$data["data_shift"] = $data_array;
		$ts_sisa = $this->Schedule_model->getAdtTimeSlotWithSisa($date);
		$data['ts_sisa'] = $ts_sisa;
		$data['dn_list'] = $dataDN;
		$data['date_list'] = $date_list;
		$data['c_date'] = $c_date;
		getHTML('backlog_management/index', $data);
	}

	public function index(){
		$date = date('Y-m-d');
		$c_date = new DateTime($date);
		$rdd = $this->input->get('date');
		$year = $_GET['year'];
		$week = $_GET['week'];
		$shift = ["DM","DP","DS"];
		$data_shift = array();
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$dataDN = $this->backlog_models->dataDN($rdd, $vendor_code);
		$data_time_slot = $this->backlog_models->dataTimeSlot($shift);
		foreach ($shift as $shf) {
			$data_array[$shf] = $this->db->query("SELECT COUNT(id) as total FROM ms_time_slot WHERE shift ='$shf' ")->row()->total;
		}
		$data_schedule = $this->backlog_models->dataSchedule($year, $date);
		$data_schedule_detail = $this->backlog_models->dataScheduleDetail($date, $data_schedule);
		$date_list = $this->getDateList($week, $year);
		$this->load->model('schedule/Schedule_model');
		$data["data_schedule"] = $data_schedule;	
		$data["data_time_slot"]	= $data_time_slot;
		$data["data_shift"] = $data_array;
		$ts_sisa = $this->Schedule_model->getAdtTimeSlotWithSisa($date);
		$data['ts_sisa'] = $ts_sisa;
		$data['dn_list'] = $dataDN;
		$data['date_list'] = $date_list;
		$data['c_date'] = $c_date;
		dump("wkwk");
		getHTML('backlog_management/backlog_old', $data);
	}

	public function getDateList($week, $year) {
	  $dto = new DateTime();
	  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
	  $s = 1;
	  while($s <= 6){
	  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
	  	$s++;
	  }
	  return $ret;
	}

	public function getSchedule(){
		$sn = $this->input->get('sn');
		$getData = $this->db->query("SELECT b.id, b.start_time, b.end_time, a.schedule_number FROM tb_schedule_detail AS a INNER JOIN ms_time_slot AS b ON a.id_time_slot = b.id WHERE a.schedule_number = '$sn' ")->row();
		echo json_encode($getData);
	}

	public function changeSchedule(){
		$year = date('Y');
		$week = date('W');
		$sn = $this->input->get('sn');
		$urgent = $this->inpupt->get('urgent');
		$schedule_now = $this->input->get('schedule_now');
		$time_slot = $this->input->get('time_slot');
		$RDD = $this->input->get('date');
		$c_time = date('H:i:s');
		$getData = $this->db->query("SELECT id FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$id_time_slot = $getData->id;
		$this->db->where('schedule_number', $sn);
		$data = array(
			"id_time_slot" => $time_slot,
		);
		
		if($RDD != ''){
			$data['rdd'] = $rdd;
		}

		//$this->db->update('tb_schedule_detail', $data);
		redirect('backlog_management?year='.$year.'&week='.$week);
	}
}

?>