<?php

class backlog_management extends CI_Controller{

	function __Construct(){
		date_default_timezone_set("Asia/Bangkok");
		parent::__Construct();
		$this->load->model('backlog_models');
		isLogin();
	}

	public function index(){
		$date = date('Y-m-d');
		$c_date = new DateTime($date);
		$rdd = $this->input->get('date');
		$year = $_GET['year'];
		$week = $_GET['week'];
		$shift = ["DM","DP","DS"];
		$data_shift = array();
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$dataDN = $this->backlog_models->dataDN($rdd, $vendor_code);
		$data_time_slot = $this->backlog_models->dataTimeSlot($shift);
		$this->backlog_models->backlog_update($rdd);
		foreach ($shift as $shf) {
			$data_array[$shf] = $this->db->query("SELECT COUNT(id) as total FROM ms_time_slot WHERE shift ='$shf' ")->row()->total;
		}
		$data_schedule = $this->backlog_models->dataSchedule($year, $date);
		$data_schedule_detail = $this->backlog_models->dataScheduleDetail($rdd, $data_schedule);
		$data_schedule_dtnow = $this->backlog_models->dataScheduleDetail($date, $data_schedule);
		$date_list = $this->getDateList($week, $year);
		$this->load->model('schedule/Schedule_model');
		$data["data_schedule"] = $data_schedule;
		$data["data_schedule_detail"] = $data_schedule_detail;
		$data["data_time_slot"]	= $data_time_slot;
		$data["data_schedule_dtnow"]	= $data_schedule_dtnow;
		$data["data_shift"] = $data_array;
		$ts_sisa = $this->Schedule_model->getAdtTimeSlotWithSisa($date);
		$data["data_history_backlog"] = $this->backlog_models->dataHistoryBacklog($rdd); 
		$data['ts_sisa'] = $ts_sisa;
		$data['dn_list'] = $dataDN;
		$data['date_list'] = $date_list;
		$data['c_date'] = $c_date;
		getHTML('backlog_management/index', $data);
	}

	public function tabel_truck(){
		$rdd  							= $this->input->get('rdd');
		$year 							= $this->input->get('year');
		$week 							= $this->input->get('week');
		$c_time 						= date('H:i:s');
		$rdd_future = date('Y-m-d', strtotime($rdd."+1 day"));
		$getData 						= $this->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$shift 					= $getData->shift;
		$data_schedule_detail 			= $this->backlog_models->dataScheduleDetail($rdd);
		$data["data_shift"]				= $shift;
		$data["data_schedule_detail"] 	= $data_schedule_detail;
		$data["date"]				  	= $rdd;
		$this->load->view('backlog_management/tabel_truck', $data);
	}

	public function tabel_truck_move(){
		$rdd  							= $this->input->get('rdd');
		$year 							= $this->input->get('year');
		$week 							= $this->input->get('week');
		$c_time 						= date('H:i:s');
		$rdd_future = date('Y-m-d', strtotime($rdd."+1 day"));
		$getData 						= $this->db->query("SELECT shift FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$shift 					= $getData->shift;
		$data_schedule_move				= $this->backlog_models->dataScheduleMove($rdd, $shift, $rdd_future);
		$data["data_shift"]				= $shift;
		$data["data_schedule_detail"] 	= $data_schedule_move;
		$data["date"]				  	= $rdd;
		$data["rdd_future"]				= $rdd_future;

		$this->load->view('backlog_management/tabel_truck_move', $data);
	}

	public function indexz(){
		$date = date('Y-m-d');
		$c_date = new DateTime($date);
		$rdd = $this->input->get('date');
		$year = $_GET['year'];
		$week = $_GET['week'];
		$shift = ["DM","DP","DS"];
		$data_shift = array();
		$vendor_code = $this->session->userdata('sess_vendor_code');
		$dataDN = $this->backlog_models->dataDN($rdd, $vendor_code);

		$data_time_slot = $this->backlog_models->dataTimeSlot($shift);
		foreach ($shift as $shf) {
			$data_array[$shf] = $this->db->query("SELECT COUNT(id) as total FROM ms_time_slot WHERE shift ='$shf' ")->row()->total;
		}
		$data_schedule = $this->backlog_models->dataSchedule($year, $date);
		$data_schedule_detail = $this->backlog_models->dataScheduleDetail($date, $data_schedule);
		
		$date_list = $this->getDateList($week, $year);
		$this->load->model('schedule/Schedule_model');
		$data["data_schedule"] = $data_schedule;
		$data["data_schedule_detail"] = $data_schedule_detail;	
		$data["data_time_slot"]	= $data_time_slot;
		$data["data_shift"] = $data_array;
		$ts_sisa = $this->Schedule_model->getAdtTimeSlotWithSisa($date);
		$data["data_history_backlog"] = $this->backlog_models->dataHistoryBacklog($rdd); 
		$data['ts_sisa'] = $ts_sisa;
		$data['dn_list'] = $dataDN;
		$data['date_list'] = $date_list;
		$data['c_date'] = $c_date;
		getHTML('backlog_management/backlog_old', $data);
	}

	public function getDateList($week, $year) {
	  $dto = new DateTime();
	  $ret[0] = $dto->setISODate($year, $week)->format('Y-m-d');
	  $s = 1;
	  while($s <= 6){
	  	$ret[$s] = $dto->modify('+1 days')->format('Y-m-d');
	  	$s++;
	  }
	  return $ret;
	}

	public function getSchedule(){
		$sn = $this->input->get('sn');
		$getData = $this->db->query("SELECT b.id, b.start_time, b.end_time, a.id_schedule_group AS schedule_number 
			FROM tb_delivery_detail AS a 
			INNER JOIN ms_time_slot AS b ON 
			a.id_time_slot = b.id WHERE a.id_schedule_group = '$sn' ")->row();
		echo json_encode($getData);
	}

	public function changeSchedule(){
		$year = date('Y');
		$week = date('W');
		$sn = $this->input->get('sn');
		$urgent = $this->input->get('urgent');
		$schedule_now = $this->input->get('schedule_now');
		$time_slot = $this->input->get('time_slot');
		$RDD = $this->input->get('date');
		$c_time = date('H:i:s');
		$getData = $this->db->query("SELECT id FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
		$id_time_slot = $getData->id;
		$getData2 = [];
		$this->db->where('schedule_number', $sn);
		$data = array(
			"id_time_slot" => $time_slot,
		);
		if($RDD == ''){
			$datez = date('Y-m-d');
			$data['rdd'] = date('Y-m-d');
		}
		
		if($RDD != ''){
			$datez = $RDD;
			$data['rdd'] = $RDD;
		}

		$count_truck = $this->db->query("SELECT COUNT(id) As count FROM tb_schedule_detail WHERE id_time_slot = '$time_slot' AND rdd ='$datez' ")->row()->count;
		
		if($count_truck >= 4){
			$getData2 = $this->db->query("SELECT a.id, b.receipt FROM tb_schedule_detail AS a INNER JOIN tb_delivery_detail AS b ON a.schedule_number = b.id_schedule_group WHERE a.id_time_slot = '$time_slot' AND a.rdd = '$datez' ")->row();
			
			if(isset($getData2->id)){
				$data = $this->db->query("UPDATE tb_schedule_detail SET id_time_slot = 0 WHERE id = '$getData2->id' ");
				
				$this->db->query("DELETE FROM docking_management.tb_queue WHERE receipt = '$getData->receipt' ");
			}
		}
		$this->db->update('tb_schedule_detail', $data);
		$this->db->where('id_schedule_group', $sn);
		$this->db->update('tb_delivery_detail', array("id_time_slot" => $time_slot) );
		$status = 0;

		if($urgent != ''){
			$status = 1;
		}

		$getDataReceipt = $this->db->query("SELECT 
				a.receipt,
				a.id_truck,
				c.id_time_slot,
				e.police_number,
				a.id_driver,
				c.rdd,
				b.driver_name,
				c.vendor_code,
				d.vendor_name,
				c.schedule_number
				FROM tb_delivery_detail AS a 
				INNER JOIN ms_driver AS b ON a.id_driver = b.id
				INNER JOIN ms_truck AS e ON a.id_truck = e.id
				INNER JOIN tb_schedule_detail AS c ON c.schedule_number = a.id_schedule_group
				LEFT JOIN skin_master.ms_supplier AS d ON c.vendor_code = d.vendor_code
				WHERE id_schedule_group = '$sn'")->row();

			$dataz = array(
				"id_time_slot" => $getDataReceipt->id_time_slot,
				"rdd"		   => $getDataReceipt->rdd,
				"update_time_slot" => $id_time_slot,
				"update_rdd"	=> date('Y-m-d'),
				"schedule_number"	=> $getDataReceipt->schedule_number,
				"update_by"		=> $this->session->userdata('sess_id'),
				"update_at"		=> date('Y-m-d H:i:s') 
			);
		$data_backlog["id_time_slot"] = $getDataReceipt->id_time_slot;
		$data_backlog["schedule_number"] = $getDataReceipt->schedule_number;
		$data_backlog["status"] = $status;
		$dataz["status"] = $status;
		$this->db->insert("tb_backlog",$dataz);
		$this->db->query("UPDATE tb_data_checker SET schedule_checker = 1");

		redirect('backlog_management?year='.$year.'&week='.$week.'&date='.$datez);
	}

	public function ajax_select_date(){
		date_default_timezone_set("Asia/Bangkok");
		$date = $this->input->get("date");
    	$cdate = date('Y-m-d');
    	$c_time = ($date == $cdate)?date('H:i:s'):"00:01";
    	$get_order = $this->db->query("SELECT ordering, id FROM `ms_time_slot` WHERE time(end_time) >= time('$c_time')")->row();
    	$order = $get_order->id;
    	$ordering = $get_order->ordering;

    	$ts = $this->db->query("SELECT start_time, end_time, id, ordering, (5) AS sisa FROM ms_time_slot WHERE id >= '$order' ")->result();
    	
    	$get_data = [];
    	$no = 0;
    	foreach ($ts as $get) {
    		//dump($ts_sisa[$no]->id);
    		

    		$sisa = $this->db->query("SELECT (4-COUNT(a.id_time_slot)) as sisa FROM tb_schedule_detail AS a INNER JOIN ms_time_slot AS b ON a.id_time_slot = b.id 
    		WHERE a.status != 0
    		AND a.id_time_slot = '$get->id' 
    		AND a.rdd = '$date' 
    		AND (SELECT count(c.id) FROM tb_scheduler AS c INNER JOIN tb_schedule_detail as d ON c.schedule_number = d.schedule_number) > 0 
    		GROUP BY a.id_time_slot")->row();
    		if(!empty($sisa)){
    			$sisax = $sisa->sisa;
    		}else{
    			$sisax = "5";
    		}

    		if($ordering > 1 && $get->ordering > 1 ){
    			$get_data[$no] = Array(
	    			"id" 			=> $get->id,
	    			"start_time" 	=> $get->start_time,
	    			"end_time"		=> $get->end_time,
	    			"sisa"			=> $sisax
	    		);
    		}
    		
    		$no++;
    	}
    	$sisa = 5;
    	$data = '<select  required="true" name="time_slot" class="form-control"><option value="">--SELECT TIME SLOT--</option>';
        foreach($get_data as $d){
        	$status = "";
          $st = date_create($d["start_time"]);
          $et = date_create($d["end_time"]);
          $st = date_format($st, "H:i");
          $et = date_format($et, "H:i");
          if($d["sisa"] == 0){$status = "disabled";}
          	$data = $data."<option ".$status." value='".$d["id"]."'>".$st.' - '.$et."(".$d["sisa"]."slot tersisa)</option>";
        }
        $data = $data."</select>";
        echo $data;
    
	}

	public function update_time_slot(){
		date_default_timezone_set("Asia/Bangkok");
		$id_truck = $this->input->get('id_truck');
		$get_id_time_slot = $this->input->get('id_time_slot')+1;
		$rdd= $this->input->get('truck_date');
		$date = $this->input->get('rdd');
		$zara2= 0;
		
		if($get_id_time_slot > 5){
			$id_time_slot = ceil($get_id_time_slot/5)-1;
		}else{
			$id_time_slot = 16;
		}
		if( ($get_id_time_slot % 4) == 0){
			$zara2 = 1;
		}		
		//$rdd = $this->input->get('rdd');
		$date_now = date('Y-m-d');
		$update = false;
		$c_time = date('H:i:s');
		$getShift = $this->db->query("SELECT id, ordering FROM `ms_time_slot` WHERE time(start_time) <= time('$c_time') AND time(end_time) >= '$c_time' ")->row();

		$ordering = $getShift->ordering;
		$getData = $this->db->query("SELECT 
				a.receipt,
				a.id_truck,
				c.id_time_slot,
				c.rdd,
				e.police_number,
				a.id_driver,
				b.driver_name,
				c.vendor_code,
				d.vendor_name,
				c.schedule_number
				FROM tb_delivery_detail AS a 
				LEFT JOIN ms_driver AS b ON a.id_driver = b.id
				LEFT JOIN ms_truck AS e ON a.id_truck = e.id
				LEFT JOIN tb_schedule_detail AS c ON c.schedule_number = a.id_schedule_group
				LEFT JOIN skin_master.ms_supplier AS d ON c.vendor_code = d.vendor_code
				WHERE c.id = '$id_truck'")->row();
		$data_backlog = array(
			"update_time_slot"	=> $id_time_slot,
			"update_rdd"		=> $rdd,
			"update_by"			=> $this->session->userdata('sess_id'),
			"update_at"			=> date('Y-m-d H:i:s')
		);
		if($rdd < $date_now){
			echo "Hari telah terlampui coba hubungi pihak Unilever";
		}else{
			$check_data = $this->db->query("SELECT * FROM tb_backlog WHERE schedule_number = '$getData->schedule_number' ");
			$data_backlog["id_time_slot"] = $getData->id_time_slot;
			$data_backlog["rdd"]			= $getData->rdd;
			$data_backlog["schedule_number"] = $getData->schedule_number;

			$check_order = $this->db->query("SELECT ordering FROM ms_time_slot WHERE id = '$id_time_slot' ")->row()->ordering;
			if($check_order < $ordering && $rdd == $date){
				echo "Time slot telah terlampui coba hubungi pihak Unilever";
			}else if($check_data->num_rows() > 0){
				
				$this->db->insert("tb_backlog",$data_backlog);
				$update =$this->db->query("UPDATE tb_schedule_detail SET rdd = '$rdd', id_time_slot = '$id_time_slot', zara2 = '$zara2' WHERE id = '$id_truck' ");
				$update2 = $this->db->query("UPDATE tb_delivery_detail SET id_time_slot = '$id_time_slot' WHERE receipt = '$getData->receipt' ");

				$getEta = $this->db->query("SELECT start_time FROM ms_time_slot WHERE id = '$id_time_slot' ")->row()->start_time;
				$update3 = $this->db->query("UPDATE docking_management.tb_queue SET date = '$rdd', eta_dock = '$getEta' WHERE receipt = '$getData->receipt' ");
				echo "Data berhasil terupdate";
				$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Update",
	  			"by_who"			=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_schedule_detail",
	  			"id_join"		=> $id_truck,
	  			"value"			=> $getData->id_time_slot,
	  			"value2"		=> $id_time_slot,
	  			"description"	=> "Backlog Truck With Police Number : ".$getData->police_number.", Driver Name : ".$getData->driver_name,
	  			"select_join"	=> "tb_backlog",
	  			"author"		=> 1,
	  			"vendor_code"	=> $getData->vendor_code
		  	);
		  	$this->db->insert('tb_history', $history);
			}else{
				
				$update =$this->db->query("UPDATE tb_schedule_detail SET rdd = '$rdd', id_time_slot = '$id_time_slot', zara2 = '$zara2' WHERE id = '$id_truck' ");
				$update2 = $this->db->query("UPDATE tb_delivery_detail SET id_time_slot = '$id_time_slot' WHERE receipt = '$getData->receipt' ");
				$getEta = $this->db->query("SELECT start_time FROM ms_time_slot WHERE id = '$id_time_slot' ")->row()->start_time;
				$update3 = $this->db->query("UPDATE docking_management.tb_queue SET date = '$rdd', eta_dock = '$getEta' WHERE receipt = '$getData->receipt' ");
				$this->db->insert("tb_backlog",$data_backlog);
				echo "Data berhasil terupdate";

				$history = array(
	  			"date" 			=> Date('Y-m-d'),
	  			"time"			=> date('H:i:s'),
	  			"action"		=> "Update",
	  			"by_who"			=> $this->session->userdata('sess_id'),
	  			"table_join"	=> "tb_schedule_detail",
	  			"id_join"		=> $id_truck,
	  			"value"			=> $getData->id_time_slot,
	  			"value2"		=> $id_time_slot,
	  			"description"	=> "Backlog Truck With Police Number : ".$getData->police_number.", Driver Name : ".$getData->driver_name,
	  			"select_join"	=> "tb_backlog",
	  			"author"		=> 1,
	  			"vendor_code"	=> $getData->vendor_code
		  	);
		  	$this->db->insert('tb_history', $history);
			}
			
		}

		
		if($update){
			return true;
		}else{
			return false;
		}
	}

	
}

?>