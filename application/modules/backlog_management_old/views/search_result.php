<table class="table table-bordered table-striped table-hover data-table">
	<thead>
		<tr>
			<th class="text-center">Material Code</th>
			<th class="text-center">Material Name</th>
			<th class="text-center">Req. Qty.</th>
			<th class="text-center">UOM</th>
			<th class="text-center">RDD</th>
			<th class="text-center">Shift</th>
			<th class="text-center">Status</th>
			<th class="text-right">Action</th>
		</tr>
	</thead>
</table>