<script>
	var year = "<?php echo $_GET['year']; ?>";
	var week = "<?php echo $_GET['week']; ?>";
	var category = "<?php echo $_GET['category']; ?>";
	var c_get = "<?php if(isset($_GET['GET'])){echo "GET=".$_GET['GET'];}else{echo "GET=Weekly";} ?>";
	function setWeek(week, state){
		/*alert(week+" "+state);*/
		
		if(state == "INC"){
			week = parseInt(week)+1;
			$('.week').val(week);
		}else{
			week = parseInt(week)-1;
			$('.week').val(week);
		}

		var menu = $("#menu").val();
		var item_list = $("#item_list").val();
		var vendor_id = $("#vendor_id").val();
		var url=('<?php base_url() ?>rds?year='+year+'&week='+week+'&'+c_get);
		window.location=(`${url}&category=${category}&type_search_item=${menu}&type_search_value=${item_list}&vendor_code=${vendor_id}`);
	}

	function setYear(year, state){
		/*alert(week+" "+state);*/
		
		
		if(state == "INC"){
			year = parseInt(year)+1;
			$('#year').val(year);
		}else{
			year = parseInt(year)-1;
			$('#year').val(year);
		}
		var menu = $("#menu").val();
		var item_list = $("#item_list").val();
		var vendor_id = $("#vendor_id").val();
		var url=('<?php base_url() ?>rds?year='+year+'&week='+week+'&'+c_get);
		window.location=(`${url}&category=${category}&type_search_item=${menu}&type_search_value=${item_list}&vendor_code=${vendor_id}`);
	}

	$('.table-fix').on('scroll', function() {
	  $(this).find('thead th')
	  	.css('transform', 'translateY('+ this.scrollTop +'px)')
	  	.css({
	  		position: "relative",
	  		"background-color": "#31559F",
	  		"z-index" : 3
	  	});
	  $(this).find('tr td:nth-child(1)')
	  	.css('transform', 'translateX('+ $(this).scrollLeft() +'px)')
	  	.css({
	  		"z-index" : 2
	  	});
	});

	//$('.row.table-fix').perfectScrollbar();
	
	function filter_data() {
		
		var menu = $("#menu").val();
		var item_list = $("#item_list").val();
		var vendor_id = $("#vendor_id").val();
		var url=('<?php base_url() ?>rds?year='+year+'&week='+week+'&'+c_get);
		window.location=(`${url}&category=${category}&type_search_item=${menu}&type_search_value=${item_list}&vendor_code=${vendor_id}`);
	}

	function filter_data_daily() {
		
		var menu = $("#menu").val();	
		var date = $("#date_holder").val();
		var item_list = $("#item_list").val();
		var vendor_id = $("#vendor_id").val();
		var url=('<?php base_url() ?>rds?year='+year+'&week='+week+'&'+c_get);
		window.location=(`${url}&category=${category}&type_search_item=${menu}&type_search_value=${item_list}&vendor_code=${vendor_id}&Date=${date}`);
	}


	function select_menu(a){
		if(a == ''){
			$('#search_value').attr('disabled', true);
		}else{
			$('#search_value').attr('disabled', false);
		}
	}

</script>