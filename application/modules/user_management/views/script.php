<script>
	
	$(document).ready(function(){
		$('.alert').removeAttr('style');
		
		vendor_list();

		
	})

	$("#table").DataTable({
		
	});

	$("#imagez").change(function(){
        show_image(this);
    });

	function show_image(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#foto').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#tables").DataTable({
		"scrollX": true,
		"scrollY": true,
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
		
	});

	$("#tables2").DataTable({
		
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
		
	});

	$("#tables3").DataTable({
		ordering: false,
		"dom": '<"row justify-content-end"fl>rt<"row text-center align-items-center justify-content-end"ip>'
		
	});

	function validateForm_add(){
		var name = $('#name').val();
		var username = $('#username').val();
		var password = $('#password').val();
		var email = $('#email').val();
		var phone = $('#phone').val();
		var role = $('#role').val();

		if(role == 1 ){
			if(name != '' && username != '' && password != '' && email != '' && phone != '' && role != ''){
				$('#submit_form').attr('disabled',true);
				$('.form-field').submit();
			}else{
				$('#btn_submit').click();
			}
		}else{
			if(name != '' && username != '' && password != '' && email != '' && phone != '' && role != ''){
				$('#submit_form').attr('disabled',true);
				$('.form-field').submit();
			}else{
				$('#btn_submit').click();
			}
		}
	}

    function datatablez(){
		$('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search RDS",
        }
      });

      var table = $('#datatable').DataTable();

      // Edit record
      table.on('click', '.edit', function() {
        $tr = $(this).closest('tr');
        var data = table.row($tr).data();
        alert('You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.');
      });

      // Delete a record
      table.on('click', '.remove', function(e) {
        $tr = $(this).closest('tr');
        table.row($tr).remove().draw();
        e.preventDefault();
      });

      //Like record
      table.on('click', '.like', function() {
        alert('You clicked on Like button');
      });

	}

    function vendor_complete(vendor_code){
    	$.ajax({
    		type : "GET",
    		url  : "<?php echo base_url() ?>user_management/ajax_vendor",
    		dataType: "json",
    		data : {
				'vendor_code' : vendor_code
			},
			success:function(resp){

				var vendor_name = resp[0].vendor_name;
				var vendor_alias = resp[0].vendor_alias;
				
				$('#vendor_name').val(vendor_name);
				$('#vendor_alias').val(vendor_alias);
			},
			error:function(e){
				alert("Something Wrong!");
			}
    	});
    }


	function checkRole(role){
		if(role == 1){
			$("#vc").attr('disabled', true);
			$("#vn").attr('disabled', true);
			$("#category").attr('disabled', true);
		}else if(role == 2){
			$("#vc").attr('disabled', true);
			$("#vn").attr('disabled', true);
			$("#category").removeAttr('disabled');
		}else{
			$("#vc").removeAttr('disabled');
			$("#vn").removeAttr('disabled');
			$("#category").attr('disabled', true);
		}
	}

	function validateForm() {
	var isValid = false;
	  var vendor_code = $('#vendor_code').val();
	  var vendor_name = $('#vendor_name').val();
	  var vendor_alias = $('#vendor_alias').val();
	  var telephone = $('#telephone').val();

	  if(vendor_code != '' && vendor_name != '' && vendor_alias != '' && telephone != '' ){
	  	isValid = true;
	  }
	  
	  if(isValid == true){
	  	loading();
	  	$('#btn_save').attr('disabled', true);
	  	$('.form-field').submit();
	  }else{
	  	$('#btn_submit').click();
	  }
	}

	function vendor_list(){
		$('#vendor_list').empty();
		$.ajax({
			method : "POST",
			url    : "<?php echo base_url() ?>user_management/vendor_list",
			dataType : "html",
			success: function(result){
				$('#vendor_list').html(result);
			}
		});
	}

	function deletez(a){
		const swalWithBootstrapButtons = Swal.mixin({
			  customClass: {
			    confirmButton: 'btn btn-success',
			    cancelButton: 'btn btn-danger'
			  },
			  buttonsStyling: false,
			})

			swalWithBootstrapButtons.fire({
			  title: 'Apakah Anda Yakin?',
			  text: "Kamu tidak dapat mengembalikan data tersebut!",
			  type: 'warning',
			  showCancelButton: true,
			  confirmButtonText: 'Yes, Hapus!',
			  cancelButtonText: 'No, cancel!',
			  reverseButtons: true
			}).then((result) => {
			  if (result.value) {

			    swalWithBootstrapButtons.fire(
			      'Deleted!',
			      'Your file has been deleted.',
			      'success'
			    )
			    $.ajax({
	            method : "GET",
	            url : "<?php echo base_url() ?>user_management/delete",
	            data : {id:a},
	            dataType : "JSON",
	            success: function(result){ 
	            }
            }); 
			window.location=("<?php echo base_url() ?>user_management");
			  } else if (
			    // Read more about handling dismissals
			    result.dismiss === Swal.DismissReason.cancel
			  ) {
			    swalWithBootstrapButtons.fire(
			      'Cancelled',
			      'Data tersebut tidak dihapus',
			      'error'
			    )
			  }
			})

		
	}

	function delete_vendor(a,b){
		
		var confirmz = confirm("Apakah anda yakin akan menghapus vendor :"+b);
		if(confirmz){
			$.ajax({
			method : "POST",
			url    : "<?php echo base_url() ?>user_management/delete_vendor",
			data   : {id: a},
			
			success: function(result){
				vendor_list();
				alert("Vendor Code :"+b+" Berhasil dihapus");
			},error:function(result){
				alert("Something Error");
			}
		});
		}
		
	}

	function check_all(id_menu){
		if ($('#select'+id_menu).prop('checked')) {
			$('.sub'+id_menu).prop('checked', true);
		}else{
			$('.sub'+id_menu).prop('checked', false);
		}
	}

	//add menu authorization

	function vendor_check(a){
		if($("#"+a).prop("checked")){
			
		}else{
			alert("idih");
		}
	}

	function toggle_menu(a){
		if($('.sub'+a).hide()){
			$('.sub'+a).show();
		}else{
			$('.sub'+a).show();
		}
		
		var check = $('.menu'+a).prop("checked");
		if(!check){
			$('.submenu'+a).prop("checked", false);	
		}
	}

	function form_authorization(role){
	 	if(role != ''){ 	
		    $.ajax( {
		        url: "<?php echo base_url().'user_management/form_authorization'; ?>",
		        type: 'POST',
		        data: {
		          'role' : role
		        },
		        dataType:"html",
		        success:function(e){
		          $('.form_authorization').html(e);
		        },error:function(e){
		          alert("Something Error");
		        }
		      });
		    }else{
    			$('.form_authorization').empty();
    	}
    }



    function add_new_authorization(a){
    	$.ajax({
    		url: "<?php echo base_url() ?>user_management/add_role",
    		type:"POST",
    		data: {
    			'role_name' : a
    		},
    		success:function(e){
    			ajax_role();
    		},error:function(e){
    			alert("Something error");
    		}
    	});
    }

    function show_delete(a){
    	if(a != '' && (a != 1) && a!= 3 ){
    		$('.btn_delete').show();
    	}else{
    		$('.auth_action').prop('hidden', true);
    		$('.btn_delete').hide();
    	}
    }

    function delete_role(a){
    	$.ajax({
    		url: "<?php echo base_url() ?>user_management/delete_role",
    		type:"POST",
    		data: {
    			'role' : a
    		},
    		success:function(e){
    			alert("Data Berhasil di hapus");
    			ajax_role();
    		},error:function(e){
    			alert("Something error");
    		}
    	});
    }
</script>