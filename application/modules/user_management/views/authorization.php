<style>
  .nav-pills .nav-item .nav-link {
    line-height: 14px;
    text-transform: uppercase;
    font-size: 10px;
    font-weight: 500;
    min-width: 0px;
    text-align: center;
    color: #555;
    transition: all .3s;
    border-radius: 30px; 
    padding: 0px 2px;
}

@media and screen(min-width: 800px){
  .authorization{
    margin-top:-40px;
  }
}

.authorization{
    margin-top:-40px;
}

.nav-pills .nav-item i {
    display: block;
    font-size: 20px;
    padding: 15px 0;
}
</style>
<div class="content">
    <div class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-md-12">
            <div class="card ">
              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-check" style="font-size:30px"></i>
                </div>
                  <h4 class="card-title">Authorization User</h4>
              </div>
              <div class="card-body">
                <div class="row add_new">
                  <div class="col-1">
                    <button type="button" onclick="$('.add_new_form').show(),$('.add_new').hide(), $('.form_role').hide()" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add New</button>
                  </div>
                  <div class="col-3 form_role">
                    <select class="custom-select role" id="select_role" onchange="form_authorization(this.value), show_delete(this.value)" style="height:33px;margin-top:4px">
                      <option value="">
                        Select Role
                      </option>
                      
                    </select>
                  </div>
                  <div class="col-1 btn_action auth_action" hidden>
                    <button class="btn btn-info btn-sm "  onclick="$('#form_update_authorization').submit()">Save</button>
                  </div>
                  <div class="col-1 btn_action auth_action" hidden>
                    <button class="btn btn-warning btn-sm "  onclick="$('.auth_action').prop('hidden', true), form_authorization($('.role').val())">Cancel</button>
                  </div>
                  <div class="col-1 btn_action btn_delete" style="display:none">
                    <button class="btn btn-danger btn-sm" onclick="delete_role($('.role').val())">Delete</button>
                  </div>
                </div>
                <div class="row add_new_form" style="display:none">
                  <div class="col-2">
                     
                     <div class="form-group">
                      <label for="role" class="bmd-label-floating">Role Name</label>
                      <input type="text" name="role_name" class="form-control" id="role">
                    </div>
                  </div>

                  <div class="col">
                    <button type="submit" onclick="add_new_authorization($('#role').val()),$('.add_new_form').hide(),$('.add_new').show(),$('.form_role').show()" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Add New</button>
                    <button type="button" onclick="$('.add_new_form').hide(),$('.add_new').show(),$('.form_role').show()" class="btn btn-danger btn-sm"><i class="fas fa-window-close"></i> Cancel</button>
                    
                  </div>
                </div>
                
              </div>
            </div>
          </div>
      </div>
  

<!-- authorization -->
      <div class="form_authorization">
        
      </div>   
<!-- End authorization -->

      </div>
    </div>
  </div>

<script>
  ajax_role();

  function ajax_role(){
      $('.option_role').remove();
      $.ajax({
        url:"<?php echo base_url()?>user_management/ajax_role",
        dataType: "JSON",
        success:function(e){
          
          for(var i = 0; i < e.length; i++){
            $('#select_role').append("<option class='option_role' value='"+e[i].id+"'>"+e[i].role+"</option>");
          }

        },error:function(){
          alert("Something Error");
        }
      })
    }
</script>

