<style type="text/css">
  
</style>
<div class="content">
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card ">
                  <div class="card-header card-header-success card-header-icon">
                    <div class="card-icon">
                    <i class="fas fa-user-friends" style="font-size:30px"></i>
                    </div>
                    <h4 class="card-title">Add Supplier</h4>
                    </div>
                    
                    <div class="card-body ">
                      <form class="form-field" action="<?php echo base_url(); ?>user_management/add_action_vendor" method="POST">
                        <div class="container-fluid">
                          
                          <div class="row">
                            <div class="col-2">
                              <div class="form-group">
                                <label>Supplier Code</label>
                              
                              <input id="vendor_code" required type="text" class="form-control" name="vendor_code">
                              </div>
                            </div>

                            <div class="col-md-2">
                              <div class="form-group">
                                <label>Full Capacity</label>
                              
                              <input id="full_capacity" required type="number" class="form-control" name="full_capacity">
                              </div>
                            </div>

                            <div class="col-md-2">
                              <div class="form-group">
                                  <select required="true" name="vendor_type" class="form-control">
                                      <option>-- VENDOR TYPE --</option>
                                      <option value="RM"> RM </option>
                                      <option value="PM"> PM </option>
                                  </select>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col-md-4">
                              <div class="form-group">
                                <label>Supplier Name</label>
                              
                              <input type="text" id="vendor_name" required class="form-control" name="vendor_name">
                            </div>
                          </div>

                            <div class="col-md-2">
                              <div class="form-group">
                                <label>Supplier Alias</label>
                              
                              <input type="text" id="vendor_alias" required class="form-control" name="vendor_alias">
                            </div>
                          </div>

                             <div class="col-md-3">
                              <div class="form-group">
                                <label>Telephone</label>
                              
                              <input type="text" maxlength="13" required oninput="this.value=this.value.replace(/[^0-9]/g,'');" id="telephone" class="form-control" name="telephone">
                              </div>
                            </div>

                          </div>
                        </div>
                        <br>
                        <div class="row">
                          <button hidden id="btn_submit">submit</button>
                          <button onclick='validateForm()' id="btn_save" type="button" class="btn btn-primary" style="margin:20px"> Add Supplier</button>
                        </div>
                      </form>
                        
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>  