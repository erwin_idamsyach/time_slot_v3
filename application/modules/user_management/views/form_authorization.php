<form action="<?php echo base_url() ?>user_management/update_authorization_user" id="form_update_authorization" method="POST">
  <input type="hidden" name="role_id" value="<?php echo $role; ?>">
            <div class="card ">
              <div class="card-header card-header-primary card-header-icon">
                <div class="card-icon">
                  <i class="fas fa-user-lock" style="font-size:30px"></i>
                </div>
                  <h4 class="card-title">Form Authorization</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <ul class="nav nav-pills nav-pills-icons" role="tablist">
                      <?php foreach ($data_menu as $get) {
                        ?>
                          <li class="nav-item">
                            <a class="nav-link <?php if($get->menu == 'Schedule dashboard'){echo "active";} ?>" href="#<?php echo $get->menu;?>" role="tab" data-toggle="tab">
                                <i class="<?php echo $get->icon; ?>"></i>
                                <?php echo $get->menu; ?>
                            </a>
                        </li>
                        <?php 
                      } ?>                       
                    </ul>
                  </div>
                </div>

                <div class="row"  >
                  <div class="col-md-12">
                        <div class="tab-content tab-space">
                           <?php foreach ($data_menu as $get) {
                            $check_sub_menu = 0;
                          ?>
                            <div class="tab-pane <?php if($get->menu == 'Schedule dashboard'){echo "active";} ?>" id="<?php echo $get->menu; ?>">
                              <div class="container-fluid">
                                
                                <div class="row">
                                   
                                    <?php $data_sub_menu = sub_menu($get->id);
                                      foreach ($data_sub_menu->result() as $key) {
                                        if($key->sub_menu == "Menu"){
                                          if(in_array($get->id, $data_role_menu) ){
                                            $check_sub_menu = 1;
                                          }
                                         ?>
                                          <div class="col-3">
                                          <div class="form-check">
                                            <label class="form-check-label">
                                                <input 
                                                <?php 
                                                  if($key->sub_menu == 'Vendor'){
                                                    echo "onclick='vendor_check(this.id)'";
                                                  }
                                                ?> class="form-check-input menu<?php echo $get->id;?>" type="checkbox" <?php if(in_array($key->id, $data_role_menu)){echo "checked";} ?> id="<?php echo $key->id; ?>" value="<?php echo $key->id; ?>" onclick="toggle_menu('<?php echo $get->id; ?>')" name="menu[]">
                                                <?php echo $key->menu; ?>
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                          </div>
                                          </div>
                                        <?php 
                                        }else{
                                        ?>
                                        <div class="col-3 sub<?php echo $get->id;?>" 
                                          <?php 
                                          
                                          
                                          if(!in_array($key->id, $data_role_menu)){ 
                                           
                                            if($get->id != 11 && $check_sub_menu == 0){
                                              
                                            }

                                          } ?> 
                                          >
                                          <div class="form-check">
                                            <label class="form-check-label">
                                                <input <?php 
                                                if(in_array($key->id, $data_role_menu)){echo "checked='true'";} ?>
                                                <?php
                                                  if($key->sub_menu == 'Vendor'){
                                                    echo "onclick='vendor_check(this.id)'";
                                                  }
                                                ?> class="form-check-input submenu<?php echo $get->id; ?>" type="checkbox" id="<?php echo $key->id; ?>" value="<?php echo $key->id; ?>" name="menu[]">
                                                <?php echo $key->sub_menu; ?>
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                          </div>
                                          </div>
                                        <?php 
                                        }
                                      }
                                    ?>
                                  
                                </div>
                                
                              </div>

                            </div>
                          <?php } ?>
                        </div>
                    </div>
                </div>
              </div>
            </div>
</form>
<script type="text/javascript">
  $("input[type='checkbox']").change(function(){
    $('.auth_action').prop("hidden", false);
  });
</script>
          