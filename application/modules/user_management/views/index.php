<style type="text/css">
.nav-pills .nav-item .nav-link {
    line-height: 24px;
    text-transform: uppercase;
    font-size: 13px;
    font-weight: 500;
    min-width: 100px;
    text-align: center;
    color: #555;
    transition: all .3s;
    border-radius: 30px;
    padding: 0px 5px;
}

.table thead tr th {
    /* font-size: 1.063rem; */
    font-size: 14px;
}

@media screen and (min-width: 400px){
  
}

@media screen and (min-width: 800px){
  .action-button{
    position: absolute;
    display: inline-block;
    z-index: 9999;
  }

  .nav-pills{
    margin-left:-14px;
  }
}

td{
  white-space: nowrap;
}
</style>

<div class="content">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card ">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
                <i style="font-size:30px" class="fas fa-users-cog"></i>
              </div>
              <h4 class="card-title">User Management Unilever</h4>
            </div>
            <div class="card-body ">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-12">
                    <ul class="nav nav-pills nav-pills-icons" role="tablist">
                      <li class="nav-item">
                          <a class="nav-link active" href="#unilever" role="tab" data-toggle="tab">
                              <i><img src="<?php base_url(); ?>assets/images/icon.png" width="32" height="32"></i>
                              Unilever
                          </a>
                      </li>
                      <li class="nav-item">
                          <a class="nav-link" href="#supplier" role="tab" data-toggle="tab">
                              <i class="fas fa-users-cog"></i>
                              Supplier
                          </a>
                      </li>
                    </ul>
                  </div>
                </div>
                

                <div class="tab-content tab-space">
                
                  <div class="tab-pane active" id="unilever">
                    <div class="row">
                      <div class="col-12">
                        <h5><u>Data PIC Unilever</u></h5>
                      </div>
                    </div>                
                    <div class="row">
                      <div class="col-12">
                        <a href="<?php echo base_url(); ?>user_management/add" class="btn btn-primary btn-sm" style="position: absolute">
                          <i class="fa fa-plus"></i> ADD USER
                        </a>
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered table-hover" id="tables2" >
                            <thead style="color:white; background-color:#31559F;font-weight: 650;font-size:12px">
                              <tr>
                                <th class="text-center">No</th>
                                <th class="text-center">PIC</th>
                                <th class="text-center">Username</th>
                                <th class="text-center">Role</th>
                                <th class="text-center">Category</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">No Telepon</th>
                                
                                <?php
                                if($this->session->userdata('sess_role_no') == 1){
                                  ?>
                                <th class="text-center">Action</th>
                                  <?php
                                }
                                ?>
                              </tr>
                            </thead>
                            <tbody >
                              <?php
                              $a = 1;
                              foreach($data_user_uni->result() as $get){
                                ?>
                                <tr >
                                  <td><?php echo $a++; ?></td>
                                  <td><?php echo $get->nama; ?></td>
                                  <td><?php echo $get->username; ?></td>
                                  <td  align="center"><?php echo $get->role; ?></td>
                                  <td  align="center"><?php echo ($get->role=="Progressor")?$get->category:"-"; ?></td>
                                  <td><?php echo $get->email; ?></td>
                                  <td><?php echo $get->phone_number; ?></td>
                                  <?php
                                  if($this->session->userdata('sess_role_no') == 1){
                                    ?>
                                  <td class="text-center">
                                    <?php
                                    if($this->session->userdata('sess_role_no') == 1){
                                      ?>
                                      <a onclick='loading()' href="<?php echo base_url(); ?>profile?id=<?php echo $get->id; ?>"><button class="btn btn-just-icon btn-round btn-info btn-sm">
                                      <i class="fa fa-eye"></i>
                                    </button></a>
                                    <a onclick='loading()' href="<?php echo base_url(); ?>user_management/edit?id=<?php echo $get->id; ?>"><button class="btn btn-just-icon btn-round btn-warning btn-sm">
                                    <i class="fa fa-edit"></i>
                                    </button></a>
                                    <button class="btn btn-just-icon btn-round btn-danger btn-sm" onclick="deletez(<?php echo $get->id; ?>)">
                                      <i class="fa fa-trash"></i>
                                    </button>
                                      <?php
                                    }
                                    ?>
                                  </td>
                                    <?php
                                  }
                                  ?>
                                </tr>
                                <?php
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>           
                  </div>
                
                <div class="tab-pane" id="supplier">
                  <div class="row">
                    <div class="col-12">
                      <h5><u>Data PIC Supplier</u></h5>
                    </div>
                  </div>
                  <div class="row action-button">
                    <div class="col-12">
                      <a href="<?php echo base_url(); ?>user_management/add?sup" class="btn btn-sm btn-primary">
                        <i class="fa fa-plus"></i> ADD PIC SUPPLIER
                      </a>
                      <a href="<?php echo base_url(); ?>user_management/add_supplier" class="btn btn-warning btn-sm">
                        <i class="fa fa-plus"></i> ADD SUPPLIER
                      </a>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="tables3">
                          <thead style="color:white; background-color:#31559F;font-weight: 650; white-space: nowrap;">
                            <tr>
                              <th class="text-center" width="50">No</th>
                              <th class="text-center">PIC</th>
                              <th class="text-center">Username</th>
                              <th class="text-center">Supplier Code</th>
                              <th class="text-center">Supplier Name</th>
                              <th class="text-center">Supplier Alias</th>
                              <th class="text-center">Email</th>
                              <th class="text-center">No Telepon</th>
                             
                              <?php
                              if($this->session->userdata('sess_role_no') == 1){
                                ?>
                              <th width="200" class="text-center">Action</th>
                                <?php
                              }
                              ?>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $a = 1;
                            foreach($data_user_sup->result() as $get){
                              ?>
                              <tr>
                                <td><?php echo $a++; ?></td>
                                <td><?php echo $get->nama; ?></td>
                                <td><?php echo $get->username; ?></td>
                                <td><?php echo $get->vendor_code; ?></td>
                                <td><?php echo $get->vendor_name; ?></td>
                                <td><?php echo $get->vendor_alias; ?></td>
                                <td><?php echo $get->email; ?></td>
                                <td><?php echo $get->phone_number; ?></td>
                               
                                <?php
                                if($this->session->userdata('sess_role_no') == 1){
                                  ?>
                                <td class="text-center" width="100">
                                  <?php
                                  if($this->session->userdata('sess_role_no') == 1){
                                    ?>
                                     <a onclick='loading()' href="<?php echo base_url(); ?>profile?id=<?php echo $get->id; ?>"><button class="btn btn-just-icon btn-round btn-info btn-sm">
                                    <i class="fa fa-eye"></i>
                                  </button></a>
                                  <a onclick='loading()' href="<?php echo base_url(); ?>user_management/edit?id=<?php echo $get->id; ?>&sup"><button class="btn btn-just-icon btn-round btn-warning btn-sm">
                                    <i class="fa fa-edit"></i>
                                  </button></a>
                                  <button class="btn btn-danger btn-just-icon btn-round btn-sm" onclick="deletez(<?php echo $get->id; ?>)">
                                    <i class="fa fa-trash"></i>
                                  </button>
                                    <?php
                                  }
                                  ?>
                                </td>
                                  <?php
                                }
                                ?>
                              </tr>
                              <?php
                            }
                            ?>
                          </tbody>
                        </table>
                      </div>
                      <hr style="border-color:grey">
                      <h5><u>Data Supplier</u></h5>
                      <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="tables3">
                        <thead style="color:white; background-color:#31559F;font-weight: 650;">
                          <tr align="center">
                            <th style="font-size:13px">No</th>
                            <th style="font-size:13px">Supplier Code</th>
                            <th style="font-size:13px">Supplier Name</th>
                            <th style="font-size:13px">Supplier Alias</th>
                            <th style="font-size:13px">Full Capacity</th>
                            <th style="font-size:13px" width="100">Action</th>
                          </tr>
                        </thead>
                        <tbody id="vendor_list">
                          
                        </tbody>

                      </table>
                    </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
              
            

            </div>
          </div>      
        </div>
      </div>
    </div>

</div>
</div>
