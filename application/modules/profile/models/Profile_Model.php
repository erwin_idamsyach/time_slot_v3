<?php

class Profile_Model extends CI_Model{

	public function profile($id){
		$data = $this->db->query("SELECT a.*, b.role as role_name, c.vendor_name, c.vendor_alias, d.category as category_name FROM ms_user as a INNER JOIN ms_role as b ON b.id = a.role LEFT JOIN skin_master.ms_supplier as c ON a.vendor_code = c.vendor_code LEFT JOIN ms_category AS d ON a.category = d.id where a.id = '$id' ")->row();
		
		return $data;

	}

	public function history($id){
		$data = $this->db->query("SELECT a.*, b.username as user FROM tb_history as a INNER JOIN ms_user as b ON a.by_who = b.id WHERE a.by_who = '$id'  ORDER BY a.id DESC  ")->result();
		return $data;
	}
}

?>