
     $(function () {
         jsKeyboard.init("virtualKeyboardEdit");

         //first input focus
         var $firstInput = $(':input').first().focus();
         jsKeyboard.currentElement = $firstInput;
         jsKeyboard.currentElementCursorPosition = 0;
     });