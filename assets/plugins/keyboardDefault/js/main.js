
     $(function () {
         jsKeyboard.init("virtualKeyboard");
         jsKeyboard.init("virtualKeyboard2");
         jsKeyboard.init("virtualKeyboard3");
         jsKeyboard.init("virtualKeyboard4");
         jsKeyboard.init("virtualKeyboardEdit");
         jsKeyboard.init("keyboardForUser");

         //first input focus
         var $firstInput = $(':input').first().focus();
         jsKeyboard.currentElement = $firstInput;
         jsKeyboard.currentElementCursorPosition = 0;
     });