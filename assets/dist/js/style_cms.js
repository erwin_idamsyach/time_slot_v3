var url = "http://localhost/ml_skin/";
function getBreakdownDuration(){	
	console.log("REQUESTED");
	var date_start = $("#time_start").val();
	var date_done  = $("#time_done").val();
	var dStart = new Date('January 1, 2000 '+date_start).getTime();
	var dDone = new Date('January 1, 2000 '+date_done).getTime();
	var diff = (dDone-dStart)/60000;
	$("[name='bd_duration']").val(diff);
}
function scrool(jenis,div,hight,speed){
	if(jenis == 'up') {
		$(div).animate({ scrollTop: $(div).scrollTop() - hight }, 'slow');
	} else if(jenis == 'down') {		
		$(div).animate({ scrollTop: $(div).scrollTop() + hight }, 'slow');
	}
}
//paste this code under head tag or in a seperate js file.
// Wait for window load
$(window).load(function() {
	// Animate loader off screen
	$(".se-pre-con").fadeOut("slow");
	$('.alert-success').hide();
	$('.alert-danger').hide();
	$('.highcharts-credits').hide();
});
$('#editForm').hide();
$('#deleteForm').hide();

function openFile() {
	$("#goFile").trigger('click');
}

function addDecision(week,id,kode){
	$("#modal-execute").modal('show');
	$('.btn-direct').attr('onclick', "goExecute("+week+", "+id+", '"+kode+"')")
	$('.btn-desc').attr('onclick', "goExecutedesc("+week+", "+id+", '"+kode+"')")
}

function goExecute(week,id,kode){
	var r = confirm("Apakah anda yakin?");
	if (r == true) {
		var tahun = $('#tahun').val();
		$.ajax({
			type : "GET",
			url  : url+"dashboard/setExecute",
			data : {
				'week' 	: week,
				'id'	: id,
				'tahun'	: tahun,
				'kode'  : kode
			},
			success:function(html){
				$('.modal').modal('hide');
				$('#scn-'+week+'-'+id).html("<img src='"+url+"assets/images/loading.gif'>");
				setTimeout(function(){
					/*getcariUnit(kode)*/
					getSpecificRow(week, id, kode, tahun);
				}, 500)
			}
		});
	} else {
	    
	}
}

function goExecutedesc(week,id,kode){
	$('.modal').modal('hide');
	setTimeout(function(){
		var tahun = $('#tahun').val();
		$.ajax({
			type : "GET",
			url  : url+"dashboard/setExecutedesc",
			data : {
				'week' 	: week,
				'id'	: id,
				'tahun'	: tahun,
				'kode'  : kode
			},
			success:function(html){
				$('#myModal1').modal('show');
				$('.execute').html(html);			
				$(".dropify").dropify();
			}
		});
	}, 400);
}

function save_pm(week,id,kode,tahun){
	var r = confirm("Apakah anda yakin?");
	if (r == true) {
		var desc = $('#desc').val();
		var day = $("[name='day']").val();
		var date_pm = $("[name='date_pm']").val();
		var duration   = $("[name='bd_duration']").val();
		$.ajax({
			type : "GET",
			url  : url+"dashboard/setExecutepm",
			data : {
				'week' 	: week,
				'id'	: id,
				'tahun'	: tahun,
				'kode'  : kode,
				'desc'	: desc,
				'day'	: day,
				'date_pm' : date_pm,
				'duration'	 : duration
			},
			success:function(html){
				window.location.reload();
			}
		});
	} else {
	    
	}
}

function goFilex() {
	var r = confirm("Apakah anda yakin?");
	if (r == true) {
		$(".se-pre-con").fadeOut("slow");
	    $('#jos').submit();
	} else {
	    
	}
}

function getcariLineCalendar(){
	var sbu   = $('#sbu').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/cariLine",
		data : {
			'sbu'  : sbu,
		},
		success:function(html){
			$('#line').html(html);
		}
	});
}

function getcariMachineCalendar(){
	var sbu   = $('#sbu').val();
	var line   = $('#line').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/cariMachine",
		data : {
			'sbu'  : sbu,
			'line' : line
		},
		success:function(html){
			$('#machine').html(html);
		}
	});
}

function goFilex() {
	var r = confirm("Apakah anda yakin?");
	if (r == true) {
	    $('#jos').submit();
	} else {
	    
	}
}

function goTahun(menu){
	alert(menu)
	if(menu != "plan"){
		var tahun = $('#tahun').val();
		var menu = $('#menu').val();
		window.location=(url+'dashboard/index/'+menu+'?tahun='+tahun);
	}else{
		var tahun = $('#tahun').val();
		var menu = $('#sbu').val();
		window.location=(url+'plan/dashboard/'+menu+'/'+tahun);
	}
}

function goTahunPlan(){
	var tahun = $('#tahun').val();
	window.location=(url+'plan/dashboard/tube/'+tahun);
}

function getEquipment(){
	var machine = $('#machine').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/getEquipment",
		data : "machine="+machine,
		success:function(html){
			$('#equipment').html(html);
		}
	});
}

function change_week(week,id){
	$.ajax({
		type : "GET",
		url  : url+"calendar/getActual",
		data : {
			'week'    : week,
			'id_part' : id
		},
		success:function(html){
			$('#myModal').modal('show');
			$('.actual').html(html);
		}
	});
}

function change_week_machine(week,id){
	$.ajax({
		type : "GET",
		url  : url+"calendar/getAjaxActualMachine",
		data : {
			'week'    : week,
			'id_actual' : id
		},
		success:function(html){
			$('#myModal').modal('show');
			$('.actual').html(html);
		}
	});
}

function save_actual(week,id){
	$('#myModal').modal('hide');
	var machine = $('#machine').val();
	var equipment = $('#equipment').val();
	var actual = $('#actual').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/setActual",
		data : {
			'week'    	: week,
			'id_part' 	: id,
			'machine'   : machine,
			'equipment' : equipment,
			'actual'	: actual
		},
		success:function(html){
			$('#gogoBox').html(html);
		}
	});
}

function getcariLine(kode){
	var line = $('#line').val();
	var tahun = $('#tahun').val();
	$('#machine').val('');
	$('#unit').val('');
	$('#frekuensi').val('');
	$.ajax({
		type : "POST",
		url  : url+"dashboard/cariLine/"+kode,
		data : {
			'kode' : kode,
			'line' : line,
			'tahun': tahun
		},
		success:function(html){
			$('.table_data').html(html);
		}
	});
}

function getcariMesin(kode){
	var machine = $('#machine').val();
	var line 	= $('#line').val();
	var tahun 	= $('#tahun').val();
	$('#unit').val('');
	$('#frekuensi').val('');
	$.ajax({
		type : "POST",
		url  : url+"dashboard/cariMachine/"+kode,
		data : {
			'kode' 		: kode,
			'machine' 	: machine,
			'line' 		: line,
			'tahun'		: tahun
		},
		success:function(html){
			$('.table_data').html(html);
		}
	});
}

function getcariUnit(kode){
	/*$(".btn-search").html("")*/
	var sub 	= $('#sub-equipment').val();
	var unit 	= $('#equipment').val();
	var machine = $('#machine').val();
	var line 	= $('#line').val();
	var tahun 	= $('#tahun').val();
	var week 	= $("#week").val();
	$(".btn-state").html("<i class='fa fa-spinner fa-spin'></i>")
	/*$('#frekuensi').val('');*/
	$.ajax({
		type : "GET",
		url  : url+"dashboard/cariUnit/"+kode,
		data : {
			'kode' 		: kode,
			'machine' 	: machine,
			'line' 		: line,
			'unit' 		: unit,
			'sub'		: sub,
			'tahun'		: tahun,
			'week'      : week
		},
		success:function(html){
			$('.table_data').html(html);
			$(".btn-state").html("<i class='fa fa-search'></i> SEARCH")
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
			$(".btn-state").html("<i class='fa fa-search'></i> SEARCH")
		}
	});	
}

function getcariFrekuensi(kode){
	var tahun 	  = $('#tahun').val();
	var frekuensi = $('#frekuensi').val();
	$('#line').val('');
	$('#machine').val('');
	$('#unit').val('');
	$.ajax({
		type : "POST",
		url  : url+"dashboard/cariFrekuensi/"+kode,
		data : {
			'kode' 		: kode,
			'frekuensi' : frekuensi,
			'tahun'		: tahun
		},
		success:function(html){
			$('.table_data').html(html);
		}
	});
}

function page_atas(ket,kode){
	var page = parseInt($('#page2').val());
	var tahun 	= $('#tahun').val();
	if(page != 0){
		if(ket == "minus"){
			page = page-1;
		}else{
			page = page+1;
		}
		$.ajax({
			type : "GET",
			url  : url+"dashboard/page/"+kode,
			data : {
				'page' : page,
				'tahun': tahun
			},
			success:function(html){
				$('#page2').val(page);
				$('#page').val(page);
				$('.gogo').html(page);
				$('.table_data').html(html);
			}
		});
	}
}

function page_bawah(ket,kode){
	var page = parseInt($('#page').val());
	var tahun 	= $('#tahun').val();
	if(page != 0){
		if(ket == "minus"){
			page = page-1;
		}else{
			page = page+1;
		}
		$.ajax({
			type : "GET",
			url  : url+"dashboard/page/"+kode,
			data : {
				'page' : page,
				'tahun': tahun
			},
			success:function(html){
				$('#page').val(page);
				$('#page2').val(page);
				$('.gogo').html(page);
				$('.table_data').html(html);
			}
		});
	}
}

function page_number(kode){
	var page = parseInt($('#page').val());
	var tahun 	= $('#tahun').val();
	if(page != 0){
		$.ajax({
			type : "GET",
			url  : url+"dashboard/page/"+kode,
			data : {
				'page' : page,
				'tahun': tahun
			},
			success:function(html){
				$('#page').val(page);
				$('#page2').val(page);
				$('.gogo').html(page);
				$('.table_data').html(html);
			}
		});
	}
}

function page_number2(kode){
	var page = parseInt($('#page2').val());
	var tahun = $('#tahun').val();
	if(page != 0){
		$.ajax({
			type : "GET",
			url  : url+"dashboard/page/"+kode,
			data : {
				'page' : page,
				'tahun': tahun
			},
			success:function(html){
				$('#page').val(page);
				$('#page2').val(page);
				$('.gogo').html(page);
				$('.table_data').html(html);
			}
		});
	}
}

function getEquipment(){
	var machine = $('#machine').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/getEquipment",
		data : "machine="+machine,
		success:function(html){
			$('#equipment').html(html);
		}
	});
}

function getEquipmentsAtp(){
	var machine = $('#machine').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/getEquipment",
		data : "machine="+machine,
		success:function(html){
			$('#equipment').html(html);
		}
	});
}

function getPartAtp(){
	var machine = $('#machine').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/getPartAtp",
		data : "machine="+machine,
		success:function(html){
			$('#equipment').html(html);
		}
	});
}

function getSchedule(){
	var machine   = $('#machine').val();
	var equipment = $('#equipment').val();
	var sbu 	  = $('#sbu').val();
	var tahun 	  = $('#tahun').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/getSchedule",
		data : {
			'machine'   : machine,
			'equipment' : equipment,
			'sbu'		: sbu,
			'tahun'		: tahun
		},
		success:function(html){
			$('.table-calendar').html(html);
		}
	});
}

function getCounting(){
	var machine 	= $('#machine').val();
	var equipment 	= $('#equipment').val();
	var sbu 	  	= $('#sbu').val();
	var tahun 	  	= $('#tahun').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/getCounting",
		data : {
			'machine'   : machine,
			'equipment' : equipment,
			'sbu'		: sbu,
			'tahun'		: tahun
		},
		success:function(html){
			$('#area-actual_equipment').html(html);
			getSchedule();
		}
	});
}

function goTahungo(){
	var tahun = $('#tahun').val();
	window.location=(url+'calendar?tahun='+tahun);
}

function change_week(week,id){
	var sbu 	  	= $('#sbu').val();
	var tahun 	  	= $('#tahun').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/getActual",
		data : {
			'week'    : week,
			'id_part' : id,
			'sbu'	  : sbu,
			'tahun'   : tahun
		},
		success:function(html){
			$('#myModal').modal('show');
			$('.actual').html(html);
		}
	});
}

function save_actual(week,id){
	$('#myModal').modal('hide');
	var machine 	= $('#machine').val();
	var equipment 	= $('#equipment').val();
	var sbu 	  	= $('#sbu').val();
	var tahun 	  	= $('#tahun').val();
	var actual 		= $('#actual').val();
	var amount		= $('#amount').val();
	$.ajax({
		type : "GET",
		url  : url+"calendar/setActual",
		data : {
			'week'    	: week,
			'id_part' 	: id,
			'machine'   : machine,
			'equipment' : equipment,
			'sbu'	  	: sbu,
			'tahun'   	: tahun,
			'actual'	: actual,
			'amount'	: amount
		},
		success:function(html){
			$('#gogoBox').html(html);
		}
	});
}

function goTahun(){
	var tahun = $('#tahun').val();
	var menu  = $('#menu').val();
	window.location=(url+'dashboard/index/'+menu+'?tahun='+tahun);
}

function editData(){
	var kodePart = $('#kodePartValue').val();
	$('#editForm').slideDown();
	$('#deleteForm').slideUp();
}
function deleteData(){
	var kodePart = $('#kodePartValue').val();
	$('#editForm').slideUp();
	$('#deleteForm').slideDown();
}
function editDataInput(jenis){
	var jenisValue = $('#jenisValue').val();
	var year = $('#yearValue').val();
	var mesin = $('#mesinValue').val();
	var unit = $('#unitValue').val();
	var kodePart = $('#kodePartValue').val();
	var week = $('#weekValue').val();
	var hari = $('#hariValue').val();
	var shift = $('#shiftValue').val();
	if(jenis == 'schedule'){
		var yearNew = $('#yearNewS').val();
		var weekNew = $('#mingguNewS').val();
		var hariNew = $('#hariNewS').val();
		var shiftNew = $('#shiftNewS').val();
	} else if(jenis == 'execute'){
		var yearNew = $('#yearNewE').val();
		var weekNew = $('#mingguNewE').val();
		var hariNew = $('#hariNewE').val();
		var shiftNew = $('#shiftNewE').val();
	} else if(jenis == 'pergantian'){
		var yearNew = $('#yearNewPP').val();
		var weekNew = $('#mingguNewPP').val();
		var hariNew = $('#hariNewPP').val();
		var shiftNew = $('#shiftNewPP').val();
	} else if(jenis == 'improvement'){
		var yearNew = $('#yearNewI').val();
		var weekNew = $('#mingguNewI').val();
		var hariNew = $('#hariNewI').val();
		var shiftNew = $('#shiftNewI').val();
	} else if(jenis == 'breakdown'){		
		var yearNew = $('#yearNew').val();
		var weekNew = $('#mingguNew').val();
		var hariNew = $('#hariNew').val();
		var shiftNew = $('#shiftNew').val();
	}
	var downtimeNew = $('#downtimeNew').val();
	var ewoNew = $('#ewoNew').val();
	var rootCouseNew = $('#bdrootcauseNew').val();
	var month = $('#monthValue').val();
	$.ajax({
		type : "GET",
		url  : "<?php echo base_url(); ?>/proses/ajax/edit/data",
		data : {
			'jenisValue' : jenisValue,
			'jenis' : jenis,
			'year' 	: year,
			'mesin' : mesin,
			'unit' 	: unit,
			'kodePart' 	: kodePart,
			'week'  : week,
			'hari'  : hari,
			'shift' : shift,
			'month' : month,
			'yearNew'  : yearNew,
			'weekNew'  : weekNew,
			'hariNew'  : hariNew,
			'shiftNew' : shiftNew,
			'downtimeNew' : downtimeNew,
			'ewoNew' : ewoNew,
			'rootCouseNew' : rootCouseNew
		},
		success:function(){
			location.href= window.location.href;
		}
	});
}
function deleteDataInput(jenis){
	var jenisValue = $('#jenisValue').val();
	var year = $('#yearValue').val();
	var mesin = $('#mesinValue').val();
	var unit = $('#unitValue').val();
	var kodePart = $('#kodePartValue').val();
	var week = $('#weekValue').val();
	var hari = $('#hariValue').val();
	var shift = $('#shiftValue').val();
	var month = $('#monthValue').val();
	$.ajax({
		type : "GET",
		url  : "<?php echo base_url(); ?>/proses/ajax/delete/data",
		data : {
			'jenisValue' : jenisValue,
			'jenis' : jenis,
			'year'	: year,
			'mesin'	: mesin,
			'unit' 	: unit,
			'kodePart'	: kodePart,
			'week'	: week,
			'hari'	: hari,
			'shift'	: shift,
			'month'	: month
		},
		success:function(){
			location.href= window.location.href;
		}
	});
}
function saveData(jenis){
	var baseUrl = 'http://'+window.location.hostname+'/skin/cms';
	var str = $("form#"+jenis).serialize();
	var jeniss = $('#jenisSaveData').val();
	$.ajax({
		type : "GET",
		url  : "<?php echo base_url(); ?>/proses/ajax/save/"+jeniss,
		data : $("form#"+jenis).serialize(),
		success:function(){
			$('#btnclose'+jeniss).click();
			$("#box_table").load("<?php echo base_url(); ?>/ml/"+jeniss+" .response-cari");
		}
	});
}
function deleteMl(jenis, id){

}
$(".data-table").DataTable({
	"paging": true,
	"lengthChange": false,
	"searching": true,
	"ordering": false,
	"info": true,
	"autoWidth": true,
	"iDisplayLength": 10
});
$(".data-table-user").DataTable({
    "fnDrawCallback": function( oSettings ) {
      	if($("#checkAll").is(':checked')){
			$('input:checkbox[name="checkbox[]"]').prop('checked', true);
			$('#inputArray').val('all');
		} else {
			$('input:checkbox[name="checkbox[]"]').prop('checked', false);
			$('#inputArray').val('');
		}
    },
	"paging": true,
	"lengthChange": false,
	"searching": true,
	"ordering": false,
	"info": true,
	"autoWidth": true,
	"iDisplayLength": 10
});

function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
    // Get first day of year
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return [d.getUTCFullYear()];
}

function getWeekNumber2(d) {
    // Copy date so don't modify original
    d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
    // Get first day of year
    var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
    // Return array of year and week number
    return [weekNo];
}

(function() {
	$('#jos').hide();
	Date.prototype.getWeek = function() {
        var onejan = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
    }

    var d = new Date();
	var hour = d.getHours();
    var shift = 0;
    if(hour >= 22 || hour < 6)
    	shift = 1;
    else if(hour >= 6 && hour < 14)
    	shift = 2;
    else if(hour >= 14 && hour < 22)
    	shift = 3;
    var dayonshift = new Date();
    if(hour >= 22){
    	dayonshift.setDate(d.getDate() + 1);
    }else{
    	dayonshift = d;
    }

    var weekNumber  = getWeekNumber(new Date());
    var weekNumber2 = getWeekNumber2(new Date());
    var dayNames = ['Minggu','Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    
    var origOpen = XMLHttpRequest.prototype.open;
    XMLHttpRequest.prototype.open = function() {
        console.log('request started!');
        this.addEventListener('load', function() {
            $("#yearIni").val(weekNumber);
            $("#weekIni").val(weekNumber2);
            $("#hariIni").val(dayNames[dayonshift.getDay()]);
        });
        origOpen.apply(this, arguments);
    };
})();

$("#form-modal-freq").on('submit', function(){
	/*alert("Form submitted! Begin send data..");*/
	var equipment = $("#equipment").val();
	var tahun = $("#tahun").val();
	var freq = $("[name='freq_new']").val()
	var sbu = $("#sbu").val();
	/*alert("GET RESPONSE DATA : "+equipment+" "+tahun+" "+freq);*/

	$.ajax({
		type : "GET",
		url  : url+"calendar/changeFreqCalendar",
		data : {
			"equipment" : equipment,
			"tahun" 	: tahun,
			"freq"		: freq,
			"sbu"		: sbu
		},
		success:function(resp){
			/*window.location.reload();*/
			getCounting();
			scheduleFinishing(freq);
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
		}
	})
	return false;
});

function scheduleFinishing(freq){
	$('#modal-freq').modal('hide');
	$(".area-freq").text(freq);
}

$(".td-pm").on('click', function(){
	/*alert("TEST");*/
	var week = $(this).data('week');
	var id_part = $(this).data('id-part');
	var menu = $(this).data('menu');

	$.ajax({
		type : "GET",
		url : url+"ajax/get_pm_detail",
		data : {
			'week' : week,
			'id_part' : id_part,
			'menu' : menu
		},
		success:function(resp){
			$("#modal-any").modal('show');
			$("#modal-any-title").text("PM DETAIL");
			setModalSize('lg');
			$("#modal-any-body").html(resp);
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
		}
	})
});

function openFormUpload(sbu){
	$.ajax({
		type : "GET",
		url : url+"ajax/get_form_upload",
		success:function(resp){
			$("#modal-any").modal("show");
			$("#modal-any-title").text('Upload File SMP');
			setModalSize('sm');
			$("#modal-any-body").html(resp);
			$("#area-sbu").val(sbu);
			enableDropify();
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
		}
	})
}

function enableDropify(){
	$('.dropify').dropify();
}
function setModalSize(size){
	if(size == "lg"){
		$("#modal-any-type").removeClass('modal-sm')
		$("#modal-any-type").removeClass('modal-md')
		$("#modal-any-type").addClass('modal-lg')
	}else if(size == "md"){
		$("#modal-any-type").removeClass('modal-sm')
		$("#modal-any-type").removeClass('modal-lg')
		$("#modal-any-type").addClass('modal-md')
	}else{
		$("#modal-any-type").removeClass('modal-md')
		$("#modal-any-type").removeClass('modal-lg')
		$("#modal-any-type").addClass('modal-sm')
	}
}

function searchPlan(){
	var sbu = $('#sbu').val();
	var line = $('#line').val();
	var machine = $('#machine').val();
	var equipment = $('#equipment').val();
	var tahun = $("#tahun").val();
	var week = $("#week").val();
	$(".btn-state").html("<i class='fa fa-spinner fa-spin'></i>")
	$.ajax({
		type : "GET",
		url : url+"plan/search",
		data : {
			'sbu': sbu,
			'line': line,
			'machine': machine,
			'equipment': equipment,
			'tahun' : tahun,
			'week' : week
		},
		success:function(resp){
			$("#area-result").html(resp);
			$(".btn-state").html("<i class='fa fa-search'></i> SEARCH")
		},
		error:function(e){
			alert('Something wrong!');
			$(".btn-state").html("<i class='fa fa-search'></i> SEARCH");
			console.log(e);
		}
	})
}

function getActualMachine(){
	var tahun = $("#tahun").val();
	var sbu = $("#sbu").val();
	var line = $("#line").val();
	var machine = $("#machine").val();

	if(machine == ""){
		/*alert("SELECT MACHINE!");*/
	}else{
		$.ajax({
			type : "GET",
			url  : url+"calendar/getActualMachine",
			data : {
				'tahun' : tahun,
				'sbu'  : sbu,
				'line' : line,
				'machine' : machine
			},
			success:function(resp){
				$('#gogoBox').html(resp);
			},
			error:function(e){
				alert("Something wrong!");
				console.log(e);
			}
		});
	}
}

function modalActual(actual, week, id_actual){
	$("#modal-actual-machine").modal('show');
	$("#actual").val(actual);
	$(".btn-sub").attr('onclick', "save_actual_machine($('#actual').val(), "+week+", "+id_actual+")")
}

function save_actual_machine(actual_new, week, id_actual){
	$.ajax({
		type : "GET",
		url : url+"calendar/save_actual_machine",
		data : {
			'actual' : actual_new,
			'week' : week,
			'id_actual' : id_actual
		},
		success:function(resp){
			$("#modal-actual-machine").modal('hide');
			getActualMachine()
			getCounting();
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
		}
	})
}

function openTdPM(week, id_part, menu){
	/*var week = $(".td-pm").data('week');
	var id_part = $(".td-pm").data('id-part');
	var menu = $(".td-pm").data('menu');*/

	$.ajax({
		type : "GET",
		url : url+"ajax/get_pm_detail",
		data : {
			'week' : week,
			'id_part' : id_part,
			'menu' : menu
		},
		success:function(resp){
			$("#modal-any").modal('show');
			$("#modal-any-title").text("PM DETAIL");
			setModalSize('lg');
			$("#modal-any-body").html(resp);
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
		}
	})
}

function openDetail(id_part, week, year, type, sbu){
	/*alert(id_part);*/

	$.ajax({
		type : "GET",
		url : url+"ajax/getPmDetail",
		data : {
			'id_part' : id_part,
			'week' : week,
			'year' : year,
			'type' : type,
			'sbu' : sbu
		},
		success:function(resp){
			$("#modal-any").modal('show');
			if(type == "PM01"){
				$("#modal-any-title").text("DETAIL PM01");
				setModalSize('md');
			}else{
				$("#modal-any-title").text("DETAIL PM02/PM03");
				setModalSize('lg');
			}
			$("#modal-any-body").html(resp);
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
		}
	})
}

$('.td-scn').click(function(event){
	alert(event.which);
});

function getSpecificRow(week, id, kode, tahun){
	$.ajax({
		type : "GET",
		url : url+"dashboard/get_specific_row",
		data : {
			'week' 	: week,
			'id'	: id,
			'tahun'	: tahun,
			'kode'  : kode
		},
		success:function(resp){
			$("#tr-"+id).html(resp);
		},
		error:function(e){
			alert("Something wrong!");
			console.log(e);
		}
	})
}

$('.btn-edit-pm').on('click', function(){
	var t = $(this);
	$('#modal-edit-pm').modal('show');
	$('#line-edit > option').attr('selected', true).text(t.data('line'));
	$('#machine-edit > option').attr('selected', true).text(t.data('machine'));
	$('#equipment-edit > option').attr('selected', true).text(t.data('equipment'));
	$("#id-edit").val(t.data('id'));
	$('#no_umesc-edit').val(t.data('umesc'))
	$('#activity-edit').val(t.data('activity'))
	$('#date_pm-edit').val(t.data('date'))
	$('#time-edit').val(t.data('time'))
	$('#problem-edit').val(t.data('problem'))
	$('#causal_loss-edit').val(t.data('loss'))
	$('#root_cause-edit').val(t.data('root'))
	$('#no_pbk-edit').val(t.data('pbk'))
	$('#amount-edit').val(t.data('amount'))
});
/* $('select#year').selectpicker();
$('select#line').selectpicker();
$('select#mesin').selectpicker(); */
