var baseUrl = 'http://'+window.location.hostname+'/ml_skin/';
function sigin(){
	var idUser = $('.idUser').val();
	var pin = $('.pin').val();
	var mesin = $('.mesin').val();
	if(mesin == "" || mesin == "Mesin" || idUser == "" || pin == ""){
		if(idUser == ""){ $('.idUser').focus(); }
		else if(pin == ""){ $('.pin').focus(); }
		else if(mesin == ""){ $('.mesin').focus(); }
		$('#responseLogin').html('<div class="alert alert-danger" style="padding-top:15px;"><i class="fa fa-warning"></i> NO FIELD EMPTY!</div>')
		$('#responseLogin').fadeIn(200).delay(2500).fadeOut(200);
	}else{
		$.ajax({
			type : "GET",
			url  : baseUrl+"proses-login",
			data : {
				'idUser' 	: idUser,
				'pin'  : pin,
				'mesin'  : mesin
			},
			success:function(html){
				if(html == "gagal"){
					$('#responseLogin').html('<div class="alert alert-danger" style="padding-top:15px;"><h6 style="font-size:12px;"><i class="fa fa-warning"></i> ACCOUNT NOT EXIST!</h6></div>')
					$('#responseLogin').fadeIn(200).delay(2500).fadeOut(200);
				}else if(html == "success"){
					window.location=(baseUrl);
				}
			}
		});
	}
}
function chechlist(divId){
	var status = $("#"+divId).hasClass("unitactive").toString();
	if(status == 'true') {
		$("#"+divId).removeClass("unitactive");
		$('#check'+divId).prop('checked', false);
	} else if(status == 'false') {
		$("#"+divId).addClass("unitactive");
		$('#check'+divId).prop('checked', true);
	}
}
function sendAjax(response, link, type, dataType, redirect='')
{
	if(redirect == '')
	{
		$.ajax({
			url 	: link,
			type 	: type,
			dataType: dataType,
			success : function(data)
			{
				$(response).html(data);
			}
		});
	} else {
		$.ajax({
			url 	: link,
			type 	: type,
			dataType: dataType,
			success : function(data)
			{
				if(data == 'redirect')
				{
					window.location.href = redirect;
				} else {
					$(response).html(data);
				}
			}
		});
	}
}

function load_view(jenis,url){
	if(jenis == 'Sign Out') {
		window.location.href = url;
	} else {		
		$("#load").html('<div class="se-pre-con">');
		$.ajax({
			url 	: url,
			type 	: 'get',
			dataType: 'html',
			success : function(data)
			{
				$("#load").html(data);
			}
		});
	}
}
function undisabled(){	
	if($('#idMaintenanceField').val() == 'imp') {
		$('#improvement').removeClass('disable');		
	} else {		
		$('#pergantianpart').removeClass('disable');
		$('#inspectionpart').removeClass('disable');
		$('#improvement').removeClass('disable');
		$('#brokenpart').removeClass('disable');
		$('#machinefaiure').removeClass('disable');	
	}
}
function disabled(){	
	$('#pergantianpart').addClass('disable');
	$('#inspectionpart').addClass('disable');
	$('#improvement').addClass('disable');
	$('#brokenpart').addClass('disable');
	$('#machinefaiure').addClass('disable');	
	$('#minggu').attr('disabled', true);
	$('#hari').attr('disabled', true);
	$('#shift').attr('disabled', true);
	$('#deskripsi').attr('disabled', true);
	$('#downtime').attr('disabled', true);
	$('#noewo').attr('disabled', true);
	$('#bdrootcause').attr('disabled', true);	
}
function cariPart(data, response){
	var part = $('#'+data).val();
	var unitId = $('#idUnitField').val();
	var subUnitId = $('#idSubUnitField').val();
	$.ajax({
		type : "get",
		url  : baseUrl+"proses/ajax/cari/data/part",
		data : "part="+part+"&unitId="+unitId+"&subUnitId="+subUnitId,
		success:function(data){
			$('#responsePart').html(data);
		}
	});
}
function check(div) {
	$('input:checkbox[name="checkMenu"]').prop('checked', false);
	$('#check'+div).prop('checked', true);
	if(div == 'brokenpart' || div == 'machinefaiure') {
		$('#extraField').slideDown();
		$('#deskripsi').attr('required', true);
		$('#downtime').attr('required', true);
		$('#noewo').attr('required', true);
		$('#bdrootcause').attr('required', true);
	} else {
		$('#extraField').slideUp();
		$('#deskripsi').removeAttr('required');
		$('#downtime').removeAttr('required');
		$('#noewo').removeAttr('required');
		$('#bdrootcause').removeAttr('required');
	}		
	
	if($('input:checkbox[name="checkMenu"]').is(':checked')){
		removeDisableForm();
	} else {
		addDisableForm();
	}		
	
	$('#idSubMenuField').val(div);
}
function removeDisableForm(){
	$('#minggu').removeAttr('disabled');
	$('#minggu').removeAttr('disabled');
	$('#hari').removeAttr('disabled');
	$('#shift').removeAttr('disabled');
	$('#deskripsi').removeAttr('disabled');
	$('#downtime').removeAttr('disabled');
	$('#noewo').removeAttr('disabled');
	$('#bdrootcause').removeAttr('disabled');
}
function addDisableForm(){
	$('#minggu').attr('disabled', true);
	$('#hari').attr('disabled', true);
	$('#shift').attr('disabled', true);
	$('#deskripsi').attr('disabled', true);
	$('#downtime').attr('disabled', true);
	$('#noewo').attr('disabled', true);
	$('#bdrootcause').attr('disabled', true);
}
function show_alert(status,message){
	$('.message').html(message);
	$('.alert-'+status).show(500).delay(500).hide(500);
}
function page_item(data,menu_active){
	$.ajax({
		type : "get",
		url  : baseUrl+"page_waste",
		data : "data="+data+'&menu_active='+menu_active,
		success:function(html){
			$('.response-cari').html(html);
		}
	});
}
function pagination_item(ke,cari,menu_active){
	$.ajax({
		type : "get",
		url  : baseUrl+"pagination_waste",
		data : "cari="+cari+"&data="+ke+'&menu_active='+menu_active,
		success:function(html){
			$('.response-cari').html(html);
		}
	});
}