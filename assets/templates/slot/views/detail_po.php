<?php 
$not_plan = "style='background: #FF2525; color: #FFF'";
$plan = "style='background: #25FF3F; color: #000'";
?>
<div class="content-wrapper" style="background:#EEEEEE;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
      <a href="<?php echo base_url()."slot" ?>" class="btn btn-info">
        <i class="fa fa-arrow-left"></i> BACK
      </a><br>
        DETAIL PO : <?php echo $po_number; ?>
      </h1>
      <ol class="breadcrumb" style="color: #fff">
        <li><a href="#"> </a></li>
      </ol>
    </section>

    <section class="content" style="padding:20px">
      <div class="row">
        <div class="col-md-8">
          <div class="box">
            <div class="box-header">INFORMATION</div>
            <div class="box-body">
              <table border='1' class="table table-bordered">
                <tr>
                  <th>PO Number#</th>
                  <td><?php echo $info->po_number; ?></td>
                </tr>
                <tr>
                  <th>Category</th>
                  <td><?php echo $info->category; ?></td>
                </tr>
                <tr>
                  <th>Vendor Code</th>
                  <td><?php echo $info->vendor_code; ?></td>
                </tr>
                <tr>
                  <th>Vendor Name</th>
                  <td><?php echo $info->vendor_name." ($info->vendor_alias)"; ?></td>
                </tr>
                <tr>
                  <th>Week</th>
                  <td><?php echo date('W',strtotime($info->requested_delivery_date)); ?></td>
                </tr>
                <tr>
                  <th>Shift</th>
                  <td><?php echo $info->shift; ?></td>
                </tr>

              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="box">
        <div class="box-header">ITEM LIST</div>
        <div class="box-body">
          <table class="table table-striped table-hover">
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">Material Code</th>
              <th class="text-center">Material Description</th>
              <th class="text-center">UOM PLT</th>
              <th class="text-center">Quantity</th>
              <th class="text-center">UOM</th>
              
              <th class="text-center">RDD</th>
              <th class="text-center">Status</th>
              <th class="text-right">Action</th>
            </tr>
            <?php
            $a = 1;
            foreach($item_list->result() as $get){
            $plan_status = $get->status;
              ?>
            <tr>
              <td class="text-center">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="<?php echo $get->id; ?>">
                  </label>
                </div>
              </td>
              <td><?php echo $get->material_code ?></td>
              <td><?php echo $get->material_name; ?></td>
              <td><?php echo $get->uom_plt; ?></td>
              <td class="text-right"><?php echo number_format($get->qty, 3, ',', ','); ?></td>
              <td align="center"><?php echo $get->uom; ?></td>
              <td><?php 
              $date = date_create($get->requested_delivery_date); 
              $df = date_format($date, "D, d M Y");
              echo $df;
              ?></td>
              <td align="center">
                <?php LegendIcon($get->status); ?>
              </td>
              <td class="pull-right">
                <?php
                $sess_vendor = $this->session->userdata('sess_role_no');
                getButton($sess_vendor, $plan_status, $get->po_number, $get->id);
                ?>
              </td>
            </tr>
              <?php
            }
            ?>
          </table>
        </div>
      </div>
    </section>
</div>