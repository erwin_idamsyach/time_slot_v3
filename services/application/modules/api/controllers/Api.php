<?php
use Laravie\Parser\Xml\Reader;
use Laravie\Parser\Xml\Document;

class Api extends CI_Controller{

	public function index(){
		echo "XML PARSER IS HERE";
	}

	public function xmlParser(){
		$status = "";
		if(isset($_FILES['file_contents'])){
			$filename = basename($_FILES['file_contents']['name']);
			if(strpos($filename, "SPO") !== false){
				$folder = "po";
				$status = "YES";
			}else if(strpos($filename, "MAT") !== false){
				$folder = "material";
				$status = "YES";
			}else if(strpos($filename, "ASN") !== false){
				$folder = "acknowledge";
				$status = "YES";
			}else if(strpos($filename, "TSB") !== false){
				$folder = "ibd";
				$status = "YES";
			}else{
				echo "FILE TIDAK DIKETAHUI";
				$status = "NO";
			}

			$target_location = "./assets/xml/".$folder."/".$filename;

			if($status == "YES"){
				if(move_uploaded_file($_FILES['file_contents']['tmp_name'], $target_location)){
					if($folder == "po"){
						$this->xml($target_location, $folder);
					}else if($folder == "acknowledge"){
						$this->xml_gr_acknowledge($target_location, $folder);
					}else if($folder == "ibd"){
						$this->xml_ibd($target_location, $folder);
					}
					echo "DONE";
				}else{
					echo "ERROR";
				}
			}
		}
	}

	public function xml($file_path, $folder){
		$data = array();
		$asd = array();
		$xml = simplexml_load_file($file_path, null, null, 'ns2', true);
		$xml->registerXPathNamespace('ns0', "http://www.unece.org/cefact/namespaces/SBDH");
		$xml->registerXPathNamespace('ns1', "urn:ean.ucc:2");
		$xml->registerXPathNamespace('ns2', "urn:ean.ucc:deliver:2");

		foreach ($xml->xpath('//ns2:despatchAdvice') as $key) {
			//echo "<pre>".json_encode($key, JSON_PRETTY_PRINT)."</pre>";
			$ar1 = json_encode($key, JSON_PRETTY_PRINT);
			$arr = json_decode($ar1, TRUE);

			$vendor_code			= $arr['shipper']['gln'];
			$po_number				= $arr['purchaseOrder']['uniqueCreatorIdentification'];
			$d_date 				= $arr['despatchInformation']['actualShipping']['actualShipDateTime'];
			$exp 					= str_split($d_date);
			$fix_date 				= $exp[0].$exp[1].$exp[2].$exp[3]."-".$exp[4].$exp[5]."-".$exp[6].$exp[7];
			$delivery_date		    = $fix_date;

			$converter = $this->db->query("SELECT * FROM tb_vendor_conversion");
			$get = json_encode($converter->result());
			$arr = json_decode($get, TRUE);
			$search = array_search($vendor_code, array_column($arr, "vendor_code"));
			if($search != null){
				$old_vencode = $vendor_code;
				$vendor_code = $arr[$search]['convert_with'];
			}else{
				echo $vendor_code;
			}

			$data['material_detail'] = array();
			//echo "<pre>".json_encode($arr['despatchAdviceLogisticUnitLineItem'], JSON_PRETTY_PRINT)."</pre>"; die();
			foreach($xml->xpath('//despatchAdviceLogisticUnitLineItem/despatchAdviceItemContainmentLineItem') as $data){
				//echo "<pre>".json_encode($data, JSON_PRETTY_PRINT)."</pre>";
				$g = json_encode($data);
				$xm = json_decode($g, TRUE);
				$line_it = $xm['@attributes']['number'];
				$asd['po_number'] = $po_number;
				$asd['vendor_code'] = $vendor_code;
				$asd['delivery_date'] = $delivery_date;
				$asd['item_number'] = $xm['@attributes']['number'];
				$asd['material_number'] = $xm['containedItemIdentification']['additionalTradeItemIdentification'][0]['additionalTradeItemIdentificationValue'];
				$asd['stock_type'] = $xm['containedItemIdentification']['additionalTradeItemIdentification'][1]['additionalTradeItemIdentificationValue'];
				$asd['storage_location'] = $xm['containedItemIdentification']['additionalTradeItemIdentification'][4]['additionalTradeItemIdentificationValue'];
				$asd['delivery_tolerance'] = $xm['containedItemIdentification']['additionalTradeItemIdentification'][2]['additionalTradeItemIdentificationValue'];

				$asd['order_qty'] = $xm['quantityContained']['value'];
				$asd['order_uom'] = $xm['quantityContained']['unitOfMeasure']['measurementUnitCodeValue'];
				echo "<pre>".json_encode($asd, JSON_PRETTY_PRINT)."</pre>";

				$cek = $this->db->query("SELECT * FROM tb_po_subpo WHERE po_number='$po_number' AND item_number='$line_it'");

				if($cek->num_rows() != 1){
					$this->db->insert("tb_po_subpo", $asd);
				}else{
					$this->db->where('po_number', $po_number);
					$this->db->where('item_number', $line_it);
					$this->db->update('tb_po_subpo', $asd);

					$this->db->where('po_number', $po_number);
					$this->db->where('item_number', $line_it);
					$this->db->update('tb_po_subpo', array(
						'status' => 0
					));
				}
			}
			//echo "<pre>".json_encode($data, JSON_PRETTY_PRINT)."</pre>";
		}
	}

	public function xml_gr_acknowledge($target_location = "./assets/xml/acknowledge/8999999017002_ASN_5080171617_20190729092042385.xml.xml", $folder){
		#PLEASE CHANGE THIS IF YOU IN PRODUCTION
		$curr_db = "db_time_slot";
		#---------------------------------------
		$data = array();
		$item = array();
		$item['material_code'] = array();
		$item['quantity'] = array();
		$item['batch'] = array();
		$xml = simplexml_load_file($target_location, null, null, 'ns0', true);
		$xml->registerXPathNamespace('ns0', "urn:gs1:ecom:inventory_report:xsd:3");
		$xml->registerXPathNamespace('ns1', "http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader");
		foreach($xml->xpath('//ns0:inventoryReportMessage') as $data){
			echo "<pre>".json_encode($data, JSON_PRETTY_PRINT)."</pre>";

			$encode = json_encode($data, JSON_PRETTY_PRINT);
			$get = json_decode($encode, TRUE);

			$item['material_document'] = $get['inventoryReport']['inventoryReportIdentification']['entityIdentification'];
			//echo "<hr/>";
			foreach($xml->xpath("//inventoryReport/inventoryItemLocationInformation") as $gett){
				echo "<hr/><hr/>";
				echo "<pre>".json_encode($gett, JSON_PRETTY_PRINT)."</pre>";

				$encode_two = json_encode($gett, JSON_PRETTY_PRINT);
				$get = json_decode($encode_two, TRUE);

				$material_code = $get['transactionalTradeItem']['additionalTradeItemIdentification'];
				$quantity = $get['inventoryActivityLineItem']['inventoryActivityQuantitySpecification']['quantityOfUnits'];
				$batch = $get['inventoryActivityLineItem']['inventoryActivityQuantitySpecification']['transactionalItemData']['batchNumber'];
				
				$mc_new = substr($material_code, 10, 8);
				echo $mc_new." ".$quantity." ".$batch."<br>";

				array_push($item['material_code'], $mc_new);
				array_push($item['quantity'], $quantity);
				array_push($item['batch'], "'".$batch."'");

				echo "<pre>".json_encode($item, JSON_PRETTY_PRINT)."</pre>";
			}

			$imp_mc = implode(",", $item['material_code']);
			$imp_qty = implode(",", $item['quantity']);
			$imp_batch = implode(",", $item['batch']);
			$material_doc = $item['material_document'];
			$c_time = date("Y-m-d H:i:s");
			/*echo $imp_mc."<br>";
			echo $imp_qty."<br>";
			echo $imp_batch."<br>";*/

			$get_sn = $this->db->query("SELECT
							c.schedule_number
						FROM
							$curr_db.tb_scheduler a
						INNER JOIN $curr_db.tb_rds_detail b ON a.id_schedule = b.id
						INNER JOIN $curr_db.tb_schedule_detail c ON a.schedule_number = c.schedule_number
						INNER JOIN $curr_db.tb_delivery_detail d ON c.schedule_number = d.id_schedule_group
						WHERE
							b.material_code IN ($imp_mc) 
						AND
						 ROUND((a.receive_amount*b.uom_plt),3) IN($imp_qty)
						AND a.batch_number IN($imp_batch)
						AND d.gr_material_doc IS NULL OR d.gr_material_doc=''
						GROUP BY a.schedule_number");
			if($get_sn->num_rows() > 0){
				$get_sn = $get_sn->row();
				$sn = $get_sn->schedule_number;

				$this->db->query("UPDATE $curr_db.tb_delivery_detail SET gr_material_doc='$material_doc', gr_success_time='$c_time' WHERE id_schedule_group='$sn'");

				$status = 1;
			}else{
				$get_sn_w = $this->db->query("SELECT
							*,
							(a.receive_amount*b.uom_plt) as SEND_QTY
						FROM
							$curr_db.tb_scheduler a
						INNER JOIN $curr_db.tb_rds_detail b ON a.id_schedule = b.id
						INNER JOIN $curr_db.tb_schedule_detail c ON a.schedule_number = c.schedule_number
						INNER JOIN $curr_db.tb_delivery_detail d ON c.schedule_number = d.id_schedule_group
						WHERE
							b.material_code IN ($imp_mc) 
						AND
						 d.receive_quantity IN($imp_qty)
						AND a.batch_number IN($imp_batch)
						GROUP BY a.schedule_number");
				if($get_sn_w->num_rows() > 0){
					$get_sn_w = $get_sn_w->row();
					$sn = $get_sn_w->schedule_number;

					$this->db->query("UPDATE $curr_db.tb_delivery_detail SET gr_material_doc='$material_doc', gr_success_time='$c_time' WHERE id_schedule_group='$sn'");

					$status = 1;
				}else{
					$status = 0;
				}
			}
			$this->db->insert("tb_gr_acknowledge", array(
					"json_data" => json_encode($item),
					"status" => $status
				));
		}
	}

	public function xml_ibd($target_location = "./assets/xml/TSB__0223957996_20190719135523816.xml.xml", $folder){
		$curr_db = "db_time_slot";
		#---------------------------------------
		$data = array();
		$item = array();
		$item['material_code'] = array();
		$item['quantity'] = array();
		$item['batch'] = array();
		$xml = simplexml_load_file($target_location, null, null, 'ns2', true);
		$xml->registerXPathNamespace('ns0', "http://www.unece.org/cefact/namespaces/SBDH");
		$xml->registerXPathNamespace('ns1', "urn:ean.ucc:2");
		$xml->registerXPathNamespace('ns2', "urn:ean.ucc:deliver:2");

		foreach($xml->xpath('//ns2:despatchAdvice') as $data){
			echo "<pre>".json_encode($data, JSON_PRETTY_PRINT)."</pre>";

			$encode = json_encode($data, JSON_PRETTY_PRINT);
			$get 	= json_decode($encode, TRUE);
			
			$ibd_number  = $get['despatchAdviceIdentification']['uniqueCreatorIdentification']+1-1;
			$vendor_code = $get['shipper']['additionalPartyIdentification'][0]['additionalPartyIdentificationValue']+1-1;
			$vendor_name =  $get['shipper']['additionalPartyIdentification'][1]['additionalPartyIdentificationValue'];
			
			$converter = $this->db->query("SELECT * FROM tb_vendor_conversion");
			$get = json_encode($converter->result());
			$arr = json_decode($get, TRUE);
			$search = array_search($vendor_code, array_column($arr, "vendor_code"));
			if($search != null){
				$old_vencode = $vendor_code;
				$vendor_code = $arr[$search]['convert_with'];
			}else{
				echo $vendor_code;
			}

			foreach($xml->xpath('//ns2:despatchAdvice/despatchAdviceItemContainmentLineItem') as $item){
				$arr = array();
				echo "<pre>".json_encode($item, JSON_PRETTY_PRINT)."</pre>";

				$encode = json_encode($item);
				$get = json_decode($encode, TRUE);

				$item_number = $get['deliveryNote']['@attributes']['number']+1-1;
				$material_code = $get['containedItemIdentification']['additionalTradeItemIdentification'][0]['additionalTradeItemIdentificationValue']+1-1;
				$delivery_date = date_format(date_create($get['deliveryNote']['reference']['referenceDateTime']), "Y-m-d");
				$order_qty = $get['quantityContained']['value'];
				$uom = $get['quantityContained']['unitOfMeasure']['measurementUnitCodeValue'];

				$po_number = $get['purchaseOrder']['documentReference']['uniqueCreatorIdentification'];
				//echo $po_number;

				$arr = array(
					"po_number" => $ibd_number,
					"vendor_code" => $vendor_code,
					"item_number" => $item_number,
					"material_number" => $material_code,
					"storage_location" => "9001",
					"order_qty" => $order_qty,
					"order_uom" => $uom, 
					"delivery_date" => $delivery_date,
					"reference_po"  => $po_number
				);

				$cek = $this->db->query("SELECT * FROM tb_po_subpo WHERE po_number='$ibd_number' AND item_number='$item_number'");

				if($cek->num_rows() != 1){
					$this->db->insert("tb_po_subpo", $arr);
					//echo "YOU WAS HERE, NEW THING";
				}else{
					$this->db->where('po_number', $ibd_number);
					$this->db->where('item_number', $item_number);
					$this->db->update('tb_po_subpo', $arr);
					//echo "YOU WAS HERE, UPDATED THING";
				}
			}
		}
	}
}
?>